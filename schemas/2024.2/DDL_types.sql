CREATE TYPE "project-name"."sphn_AdverseEvent__sphn_hasConsequences__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CongenitalAbnormality',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Death',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HospitalisationOrProlongation',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LifeThreatening',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NoneOfTheConsequencesMentioned',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PermanentDamageOrDisability',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#TemporarilySeriousImpactMedicallyImportant'
);
CREATE TYPE "project-name"."sphn_AdverseEvent__sphn_hasOutcome__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Fatal',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NotRecovered',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Recovered',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Recovering',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#RecoveringWithSequelae'
);
CREATE TYPE "project-name"."sphn_Age__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_Age__sphn_hasQuantity__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-resource/ucum/a',
    'https://biomedit.ch/rdf/sphn-resource/ucum/d',
    'https://biomedit.ch/rdf/sphn-resource/ucum/h',
    'https://biomedit.ch/rdf/sphn-resource/ucum/min',
    'https://biomedit.ch/rdf/sphn-resource/ucum/mo',
    'https://biomedit.ch/rdf/sphn-resource/ucum/wk'
);
CREATE TYPE "project-name"."sphn_AllergyEpisode__sphn_hasDuration__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_AntimicrobialSusceptibilityLabTestEvent__sphn_hasLabTest__sphn_hasChemicalAgent__Drug__sphn_hasActiveIngredient__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_AntimicrobialSusceptibilityLabTestEvent__sphn_hasLabTest__sphn_hasChemicalAgent__Drug__sphn_hasInactiveIngredient__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_AntimicrobialSusceptibilityLabTestEvent__sphn_hasLabTest__sphn_hasChemicalAgent__Drug__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_AntimicrobialSusceptibilityLabTestEvent__sphn_hasLabTest__sphn_hasChemicalAgent__Substance__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_AntimicrobialSusceptibilityLabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasNumericalReference__ReferenceRange__sphn_hasLowerLimit__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_AntimicrobialSusceptibilityLabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasNumericalReference__ReferenceRange__sphn_hasUpperLimit__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_AntimicrobialSusceptibilityLabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasNumericalReference__ReferenceValue__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_AntimicrobialSusceptibilityLabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_AssessmentEvent__sphn_hasAssessment__Assessment__sphn_hasComponent__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_AssessmentEvent__sphn_hasAssessment__Assessment__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_AssessmentEvent__sphn_hasAssessment__TumorGradeAssessment__sphn_hasComponent__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_AssessmentEvent__sphn_hasAssessment__TumorGradeAssessment__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_AssessmentEvent__sphn_hasAssessment__TumorStageAssessment__sphn_hasComponent__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_AssessmentEvent__sphn_hasAssessment__TumorStageAssessment__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_BilledDiagnosis__sphn_hasRank__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Complementary',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Principal',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Secondary'
);
CREATE TYPE "project-name"."sphn_BilledProcedure__sphn_hasRank__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Principal',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Supplementary'
);
CREATE TYPE "project-name"."sphn_BirthDate__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_BloodPressureMeasurement__sphn_hasResult__sphn_hasDiastolicPressure__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_BloodPressureMeasurement__sphn_hasResult__sphn_hasDiastolicPressure__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-resource/ucum/mmsblHgsbr'
);
CREATE TYPE "project-name"."sphn_BloodPressureMeasurement__sphn_hasResult__sphn_hasMeanPressure__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_BloodPressureMeasurement__sphn_hasResult__sphn_hasMeanPressure__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-resource/ucum/mmsblHgsbr'
);
CREATE TYPE "project-name"."sphn_BloodPressureMeasurement__sphn_hasResult__sphn_hasSystolicPressure__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_BloodPressureMeasurement__sphn_hasResult__sphn_hasSystolicPressure__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-resource/ucum/mmsblHgsbr'
);
CREATE TYPE "project-name"."sphn_BodyHeightMeasurement__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_BodyMassIndex__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_BodyMassIndex__sphn_hasQuantity__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-resource/ucum/kgperm2'
);
CREATE TYPE "project-name"."sphn_BodySurfaceArea__sphn_hasCalculationMethod__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#DuBois',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Mosteller',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Other'
);
CREATE TYPE "project-name"."sphn_BodySurfaceArea__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_BodySurfaceArea__sphn_hasQuantity__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM ('https://biomedit.ch/rdf/sphn-resource/ucum/m2');
CREATE TYPE "project-name"."sphn_BodyTemperatureMeasurement__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_BodyWeightMeasurement__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_CardiacIndex__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_CardiacIndex__sphn_hasQuantity__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-resource/ucum/Lperminperm2'
);
CREATE TYPE "project-name"."sphn_CardiacOutputMeasurement__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_CircumferenceMeasurement__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_DataFile__sphn_hasEncoding__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ASCII',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ISO88591',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#UTF16',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#UTF8'
);
CREATE TYPE "project-name"."sphn_DataFile__sphn_hasHash__sphn_hasAlgorithm__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#MD5',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SHA256',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SHA512'
);
CREATE TYPE "project-name"."sphn_DataProcessing__sphn_hasQualityControlMetric__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_DataProvider__sphn_hasCategory__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Company',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ExternalLaboratory',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#FederalOffice',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HealthInsurance',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Hospital',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Pharmacy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PrivatePractice',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ResearchOrganization',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ServiceProvider',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#University'
);
CREATE TYPE "project-name"."sphn_DrugAdministrationEvent__sphn_hasDrug__sphn_hasActiveIngredient__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_DrugAdministrationEvent__sphn_hasDrug__sphn_hasInactiveIngredient__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_DrugAdministrationEvent__sphn_hasDrug__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_DrugAdministrationEvent__sphn_hasDuration__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_DrugPrescription__sphn_hasDrug__sphn_hasActiveIngredient__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_DrugPrescription__sphn_hasDrug__sphn_hasInactiveIngredient__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_DrugPrescription__sphn_hasDrug__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_DrugPrescription__sphn_hasFrequency__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_ElectrocardiographicProcedure__sphn_hasNumberOfLeads__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_Exposure__sphn_hasDuration__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_Exposure__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_FluidBalance__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_FluidInputOutput__sphn_hasSubstance__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_GestationalAgeAtBirth__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_GestationalAgeAtBirth__sphn_hasQuantity__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM ('https://biomedit.ch/rdf/sphn-resource/ucum/d');
CREATE TYPE "project-name"."sphn_HealthcareEncounter__sphn_hasTherapeuticArea__sphn_hasSpecialtyName__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AddictionMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AllergologyAndClinicalImmunology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Anaesthesiology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Angiology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CardiacAndThoracicVascularSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Cardiology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ChildAndAdolescentPsychiatryAndPsychotherapy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ChildAndAdolescentPsychology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ChiropracticSpecialist',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ClinicalPharmacologyAndToxicology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ClinicalPsychology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CommunityPharmacy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#DermatologyAndVenereology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#EndocrinologyAndDiabetology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ForensicMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Gastroenterology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GeneralInternalMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GeneralMedicalPractitioner',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GeneralMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GeriatricMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GynaecologyAndObstetrics',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Haematology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HandSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HealthPsychology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HospitalPharmacy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Infectology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#IntensiveCareMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#InternalMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#MedicalGenetics',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#MedicalOncology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Nephrology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Neurology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Neuropsychology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Neurosurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NuclearMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OccupationalMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Ophthalmology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OralAndMaxillofacialSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OralSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Orthodontics',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OrthopaedicSurgeryAndTraumatologyOfTheLocomotorApparatus',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Other',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Otorhinolaryngology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PaediatricSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Paediatrics',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Pathology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Periodontics',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PharmaceuticalMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PhysicalMedicineAndRehabilitation',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PlasticReconstructiveAndAestheticSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Pneumology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PreventionAndPublicHealth',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PsychiatryAndPsychotherapy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Psychotherapy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#RadioOncologyAndRadiotherapy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Radiology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ReconstructiveDentistry',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Rheumatology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Surgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ThoracicSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#TropicalAndTravelMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Urology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#VascularSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#VisceralMedicine'
);
CREATE TYPE "project-name"."sphn_HeartRateMeasurement__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_HomeAddress__sphn_hasSwissSocioEconomicPosition__sphn_hasDistance__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_HomeAddress__sphn_hasSwissSocioEconomicPosition__sphn_hasDistance__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM ('https://biomedit.ch/rdf/sphn-resource/ucum/m');
CREATE TYPE "project-name"."sphn_InhaledOxygenConcentration__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_InhaledOxygenConcentration__sphn_hasQuantity__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-resource/ucum/percent'
);
CREATE TYPE "project-name"."sphn_InsuranceStatus__sphn_hasCoverageType__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Foreign',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#General',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#None',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Other',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Private',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SemiPrivate'
);
CREATE TYPE "project-name"."sphn_Isolate__sphn_hasFixationType__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AlcoholBased',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AldehydeBased',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AllprotectTissueReagent',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HeatStabilization',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NeutralBufferedFormalin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonaldehydeBasedWithoutAceticAcid',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonaldehydeWithAceticAcid',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonbufferedFormalin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OptimumCuttingTemperatureMedium',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Other',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneTissue',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#RNALater',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SnapFreezing',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#VacuumTechnologyStabilization'
);
CREATE TYPE "project-name"."sphn_Isolate__sphn_hasPrimaryContainer__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AcidCitrateDextrose',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AldehydeBasedStabilizerForCTCs',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CellPreparationTubeCitrate',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CellPreparationTubeHeparin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ChemicalAdditivesStabilizers',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CitratePhosphateDextrose',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#EDTAAndGel',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Glass',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Hirudin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LithiumHeparin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LithiumHeparinAndGel',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LithiumHeparinAndRubberPlug',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonAldehydeBasedStabilizerForCellFreeNucleicAcids',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OrageneCollectionContainerOrEquivalent',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Other',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneBloodDNA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneBloodRNAplus',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneBoneMarrowRNA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PhysicalFiltrationSystem',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PolyethyleneTubeSterile',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PolypropyleneTubeSterile',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PotassiumEDTA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ProteaseInhibitors',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#RNALater',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#S8820ProteaseInhibitorTabletsOrEquivalent',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SerumSeparatorTubeWithClotActivator',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SerumTubeWithoutClotActivator',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumCitrate',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumEDTA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumFluoridePotassiumOxalate',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumHeparin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#StoolCollectionContainerWithDNAStabilizer',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#TempusTube',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#TraceElementsTube'
);
CREATE TYPE "project-name"."sphn_LabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasNumericalReference__ReferenceRange__sphn_hasLowerLimit__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_LabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasNumericalReference__ReferenceRange__sphn_hasUpperLimit__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_LabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasNumericalReference__ReferenceValue__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_LabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_LibraryPreparation__sphn_hasIntendedInsertSize__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_LibraryPreparation__sphn_hasQualityControlMetric__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_Measurement__sphn_hasResult__AssessmentResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_Measurement__sphn_hasResult__BloodPressure__sphn_hasDiastolicPressure__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_Measurement__sphn_hasResult__BloodPressure__sphn_hasDiastolicPressure__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-resource/ucum/mmsblHgsbr'
);
CREATE TYPE "project-name"."sphn_Measurement__sphn_hasResult__BloodPressure__sphn_hasMeanPressure__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_Measurement__sphn_hasResult__BloodPressure__sphn_hasMeanPressure__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-resource/ucum/mmsblHgsbr'
);
CREATE TYPE "project-name"."sphn_Measurement__sphn_hasResult__BloodPressure__sphn_hasSystolicPressure__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_Measurement__sphn_hasResult__BloodPressure__sphn_hasSystolicPressure__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-resource/ucum/mmsblHgsbr'
);
CREATE TYPE "project-name"."sphn_Measurement__sphn_hasResult__BodyHeight__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_Measurement__sphn_hasResult__BodyTemperature__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_Measurement__sphn_hasResult__BodyWeight__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_Measurement__sphn_hasResult__CardiacOutput__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_Measurement__sphn_hasResult__Circumference__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_Measurement__sphn_hasResult__HeartRate__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_Measurement__sphn_hasResult__OxygenSaturation__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_Measurement__sphn_hasResult__OxygenSaturation__sphn_hasQuantity__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-resource/ucum/percent'
);
CREATE TYPE "project-name"."sphn_Measurement__sphn_hasResult__RespiratoryRate__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_Measurement__sphn_hasResult__TumorGradeAssessmentResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_Measurement__sphn_hasResult__TumorStageAssessmentResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_MicrobiologyBiomoleculePresenceLabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasNumericalReference__ReferenceRange__sphn_hasLowerLimit__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_MicrobiologyBiomoleculePresenceLabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasNumericalReference__ReferenceRange__sphn_hasUpperLimit__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_MicrobiologyBiomoleculePresenceLabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasNumericalReference__ReferenceValue__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_MicrobiologyBiomoleculePresenceLabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_MicrobiologyMicroscopyLabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasCellMorphology__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CurvedRodShaped',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Elongated',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Filamentous',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Other',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Ovoid',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#RodShaped',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Round',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Spiral'
);
CREATE TYPE "project-name"."sphn_MicrobiologyMicroscopyLabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasCellOrganization__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#EntireCell',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Multicellular',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Unicellular'
);
CREATE TYPE "project-name"."sphn_MicrobiologyMicroscopyLabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasNumericalReference__ReferenceRange__sphn_hasLowerLimit__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_MicrobiologyMicroscopyLabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasNumericalReference__ReferenceRange__sphn_hasUpperLimit__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_MicrobiologyMicroscopyLabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasNumericalReference__ReferenceValue__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_MicrobiologyMicroscopyLabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_MicroorganismIdentificationLabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasNumericalReference__ReferenceRange__sphn_hasLowerLimit__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_MicroorganismIdentificationLabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasNumericalReference__ReferenceRange__sphn_hasUpperLimit__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_MicroorganismIdentificationLabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasNumericalReference__ReferenceValue__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_MicroorganismIdentificationLabTestEvent__sphn_hasLabTest__sphn_hasResult__sphn_hasTimeToPositivity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_NutritionIntake__sphn_hasEnergyQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_NutritionIntake__sphn_hasSubstance__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_OxygenSaturationMeasurement__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_OxygenSaturationMeasurement__sphn_hasResult__sphn_hasQuantity__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-resource/ucum/percent'
);
CREATE TYPE "project-name"."sphn_RadiotherapyProcedure__sphn_hasFractionsNumber__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_RadiotherapyProcedure__sphn_hasRadiationQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_RadiotherapyProcedure__sphn_hasRadiationQuantity__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-resource/ucum/Gy',
    'https://biomedit.ch/rdf/sphn-resource/ucum/MBq',
    'https://biomedit.ch/rdf/sphn-resource/ucum/cGy',
    'https://biomedit.ch/rdf/sphn-resource/ucum/mCi'
);
CREATE TYPE "project-name"."sphn_RespiratoryRateMeasurement__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_SampleProcessing__sphn_hasQualityControlMetric__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_Sample__sphn_hasFixationType__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AlcoholBased',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AldehydeBased',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AllprotectTissueReagent',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HeatStabilization',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NeutralBufferedFormalin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonaldehydeBasedWithoutAceticAcid',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonaldehydeWithAceticAcid',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonbufferedFormalin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OptimumCuttingTemperatureMedium',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Other',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneTissue',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#RNALater',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SnapFreezing',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#VacuumTechnologyStabilization'
);
CREATE TYPE "project-name"."sphn_Sample__sphn_hasPrimaryContainer__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AcidCitrateDextrose',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AldehydeBasedStabilizerForCTCs',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CellPreparationTubeCitrate',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CellPreparationTubeHeparin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ChemicalAdditivesStabilizers',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CitratePhosphateDextrose',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#EDTAAndGel',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Glass',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Hirudin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LithiumHeparin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LithiumHeparinAndGel',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LithiumHeparinAndRubberPlug',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonAldehydeBasedStabilizerForCellFreeNucleicAcids',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OrageneCollectionContainerOrEquivalent',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Other',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneBloodDNA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneBloodRNAplus',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneBoneMarrowRNA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PhysicalFiltrationSystem',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PolyethyleneTubeSterile',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PolypropyleneTubeSterile',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PotassiumEDTA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ProteaseInhibitors',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#RNALater',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#S8820ProteaseInhibitorTabletsOrEquivalent',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SerumSeparatorTubeWithClotActivator',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SerumTubeWithoutClotActivator',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumCitrate',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumEDTA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumFluoridePotassiumOxalate',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumHeparin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#StoolCollectionContainerWithDNAStabilizer',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#TempusTube',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#TraceElementsTube'
);
CREATE TYPE "project-name"."sphn_SequencingAnalysis__sphn_hasQualityControlMetric__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_SequencingAssay__sphn_hasIntendedReadDepth__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_SequencingAssay__sphn_hasIntendedReadLength__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_SequencingAssay__sphn_hasSequencingRun__sphn_hasAverageInsertSize__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_SequencingAssay__sphn_hasSequencingRun__sphn_hasAverageReadLength__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_SequencingAssay__sphn_hasSequencingRun__sphn_hasMeanReadDepth__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_SequencingAssay__sphn_hasSequencingRun__sphn_hasQualityControlMetric__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_SequencingAssay__sphn_hasSequencingRun__sphn_hasReadCount__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_SourceSystem__sphn_hasCategory__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Biobank',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CaseReportForm',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ClinicalDataPlatform',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ClinicalRegistry',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Cohort',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#DataRepository',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HealthcareInformationSystem',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OMICSFacility',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ResearchLaboratory'
);
CREATE TYPE "project-name"."sphn_SourceSystem__sphn_hasPurpose__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Billing',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PatientCare',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#QualityControl',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Research'
);
CREATE TYPE "project-name"."sphn_TimeSeriesDataFile__sphn_hasEncoding__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ASCII',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ISO88591',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#UTF16',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#UTF8'
);
CREATE TYPE "project-name"."sphn_TimeSeriesDataFile__sphn_hasEntryCount__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_TimeSeriesDataFile__sphn_hasEntryCount__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-resource/ucum/cblnbcbr'
);
CREATE TYPE "project-name"."sphn_TimeSeriesDataFile__sphn_hasHash__sphn_hasAlgorithm__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#MD5',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SHA256',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SHA512'
);
CREATE TYPE "project-name"."sphn_TobaccoExposure__sphn_hasDuration__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_TobaccoExposure__sphn_hasDuration__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM ('https://biomedit.ch/rdf/sphn-resource/ucum/a');
CREATE TYPE "project-name"."sphn_TobaccoExposure__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_TobaccoExposure__sphn_hasQuantity__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM ('https://biomedit.ch/rdf/sphn-resource/ucum/a');
CREATE TYPE "project-name"."sphn_TumorGradeAssessmentEvent__sphn_hasAssessment__sphn_hasComponent__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_TumorGradeAssessmentEvent__sphn_hasAssessment__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_TumorSpecimen__sphn_hasFixationType__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AlcoholBased',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AldehydeBased',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AllprotectTissueReagent',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HeatStabilization',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NeutralBufferedFormalin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonaldehydeBasedWithoutAceticAcid',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonaldehydeWithAceticAcid',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonbufferedFormalin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OptimumCuttingTemperatureMedium',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Other',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneTissue',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#RNALater',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SnapFreezing',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#VacuumTechnologyStabilization'
);
CREATE TYPE "project-name"."sphn_TumorSpecimen__sphn_hasPrimaryContainer__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AcidCitrateDextrose',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AldehydeBasedStabilizerForCTCs',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CellPreparationTubeCitrate',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CellPreparationTubeHeparin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ChemicalAdditivesStabilizers',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CitratePhosphateDextrose',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#EDTAAndGel',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Glass',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Hirudin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LithiumHeparin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LithiumHeparinAndGel',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LithiumHeparinAndRubberPlug',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonAldehydeBasedStabilizerForCellFreeNucleicAcids',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OrageneCollectionContainerOrEquivalent',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Other',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneBloodDNA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneBloodRNAplus',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneBoneMarrowRNA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PhysicalFiltrationSystem',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PolyethyleneTubeSterile',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PolypropyleneTubeSterile',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PotassiumEDTA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ProteaseInhibitors',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#RNALater',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#S8820ProteaseInhibitorTabletsOrEquivalent',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SerumSeparatorTubeWithClotActivator',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SerumTubeWithoutClotActivator',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumCitrate',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumEDTA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumFluoridePotassiumOxalate',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumHeparin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#StoolCollectionContainerWithDNAStabilizer',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#TempusTube',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#TraceElementsTube'
);
CREATE TYPE "project-name"."sphn_TumorSpecimen__sphn_hasTumorPurity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_TumorSpecimen__sphn_hasTumorPurity__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-resource/ucum/percent'
);
CREATE TYPE "project-name"."sphn_TumorStageAssessmentEvent__sphn_hasAssessment__sphn_hasComponent__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_TumorStageAssessmentEvent__sphn_hasAssessment__sphn_hasResult__sphn_hasQuantity__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_VariantDescriptor__sphn_hasGeneticVariation__CopyNumberVariation__sphn_hasGenomicPosition__sphn_hasCoordinateConvention__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#InterResidueCoordinates',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ResidueCoordinates'
);
CREATE TYPE "project-name"."sphn_VariantDescriptor__sphn_hasGeneticVariation__CopyNumberVariation__sphn_hasTotalCopyNumber__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_VariantDescriptor__sphn_hasGeneticVariation__CopyNumberVariation__sphn_hasTotalCopyNumber__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-resource/ucum/cblcopycbr'
);
CREATE TYPE "project-name"."sphn_VariantDescriptor__sphn_hasGeneticVariation__GeneticVariation__sphn_hasGenomicPosition__sphn_hasCoordinateConvention__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#InterResidueCoordinates',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ResidueCoordinates'
);
CREATE TYPE "project-name"."sphn_VariantDescriptor__sphn_hasGeneticVariation__GenomicDeletion__sphn_hasGenomicPosition__sphn_hasCoordinateConvention__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#InterResidueCoordinates',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ResidueCoordinates'
);
CREATE TYPE "project-name"."sphn_VariantDescriptor__sphn_hasGeneticVariation__GenomicDeletion__sphn_hasSequenceLength__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_VariantDescriptor__sphn_hasGeneticVariation__GenomicDeletion__sphn_hasSequenceLength__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-resource/ucum/cblbase_paircbr'
);
CREATE TYPE "project-name"."sphn_VariantDescriptor__sphn_hasGeneticVariation__GenomicInsertion__sphn_hasGenomicPosition__sphn_hasCoordinateConvention__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#InterResidueCoordinates',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ResidueCoordinates'
);
CREATE TYPE "project-name"."sphn_VariantDescriptor__sphn_hasGeneticVariation__GenomicInsertion__sphn_hasSequenceLength__sphn_hasComparator__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual'
);
CREATE TYPE "project-name"."sphn_VariantDescriptor__sphn_hasGeneticVariation__GenomicInsertion__sphn_hasSequenceLength__sphn_hasUnit__sphn_hasCode__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-resource/ucum/cblbase_paircbr'
);
CREATE TYPE "project-name"."sphn_VariantDescriptor__sphn_hasGeneticVariation__SingleNucleotideVariation__sphn_hasGenomicPosition__sphn_hasCoordinateConvention__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#InterResidueCoordinates',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ResidueCoordinates'
);
CREATE TYPE "project-name"."supporting__sphn_Interpretation__sphn_hasInput__valueset_iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ASCII',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AcidCitrateDextrose',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AddictionMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AlcoholBased',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AldehydeBased',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AldehydeBasedStabilizerForCTCs',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AllergologyAndClinicalImmunology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AllprotectTissueReagent',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Anaesthesiology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Angiology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Billing',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Biobank',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CardiacAndThoracicVascularSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Cardiology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CaseReportForm',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CellPreparationTubeCitrate',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CellPreparationTubeHeparin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ChemicalAdditivesStabilizers',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ChildAndAdolescentPsychiatryAndPsychotherapy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ChildAndAdolescentPsychology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ChiropracticSpecialist',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CitratePhosphateDextrose',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ClinicalDataPlatform',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ClinicalPharmacologyAndToxicology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ClinicalPsychology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ClinicalRegistry',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Cohort',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CommunityPharmacy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Company',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Complementary',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CongenitalAbnormality',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CurvedRodShaped',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#DataRepository',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Death',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#DermatologyAndVenereology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#DuBois',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#EDTAAndGel',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Elongated',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#EndocrinologyAndDiabetology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#EntireCell',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ExternalLaboratory',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Fatal',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#FederalOffice',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Filamentous',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Foreign',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ForensicMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Gastroenterology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#General',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GeneralInternalMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GeneralMedicalPractitioner',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GeneralMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GeriatricMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Glass',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GynaecologyAndObstetrics',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Haematology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HandSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HealthInsurance',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HealthPsychology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HealthcareInformationSystem',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HeatStabilization',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Hirudin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Hospital',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HospitalPharmacy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HospitalisationOrProlongation',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ISO88591',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Infectology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#IntensiveCareMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#InterResidueCoordinates',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#InternalMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LifeThreatening',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LithiumHeparin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LithiumHeparinAndGel',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LithiumHeparinAndRubberPlug',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#MD5',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#MedicalGenetics',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#MedicalOncology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Mosteller',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Multicellular',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Nephrology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Neurology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Neuropsychology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Neurosurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NeutralBufferedFormalin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonAldehydeBasedStabilizerForCellFreeNucleicAcids',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonaldehydeBasedWithoutAceticAcid',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonaldehydeWithAceticAcid',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonbufferedFormalin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#None',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NoneOfTheConsequencesMentioned',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NotRecovered',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NuclearMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OMICSFacility',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OccupationalMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Ophthalmology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OptimumCuttingTemperatureMedium',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OrageneCollectionContainerOrEquivalent',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OralAndMaxillofacialSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OralSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Orthodontics',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OrthopaedicSurgeryAndTraumatologyOfTheLocomotorApparatus',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Other',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Otorhinolaryngology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Ovoid',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneBloodDNA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneBloodRNAplus',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneBoneMarrowRNA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneTissue',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PaediatricSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Paediatrics',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Pathology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PatientCare',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Periodontics',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PermanentDamageOrDisability',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PharmaceuticalMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Pharmacy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PhysicalFiltrationSystem',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PhysicalMedicineAndRehabilitation',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PlasticReconstructiveAndAestheticSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Pneumology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PolyethyleneTubeSterile',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PolypropyleneTubeSterile',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PotassiumEDTA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PreventionAndPublicHealth',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Principal',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Private',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PrivatePractice',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ProteaseInhibitors',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PsychiatryAndPsychotherapy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Psychotherapy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#QualityControl',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#RNALater',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#RadioOncologyAndRadiotherapy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Radiology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ReconstructiveDentistry',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Recovered',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Recovering',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#RecoveringWithSequelae',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Research',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ResearchLaboratory',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ResearchOrganization',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ResidueCoordinates',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Rheumatology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#RodShaped',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Round',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#S8820ProteaseInhibitorTabletsOrEquivalent',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SHA256',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SHA512',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Secondary',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SemiPrivate',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SerumSeparatorTubeWithClotActivator',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SerumTubeWithoutClotActivator',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ServiceProvider',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SnapFreezing',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumCitrate',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumEDTA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumFluoridePotassiumOxalate',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumHeparin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Spiral',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#StoolCollectionContainerWithDNAStabilizer',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Supplementary',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Surgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#TemporarilySeriousImpactMedicallyImportant',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#TempusTube',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ThoracicSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#TraceElementsTube',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#TropicalAndTravelMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#UTF16',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#UTF8',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Unicellular',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#University',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Urology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#VacuumTechnologyStabilization',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#VascularSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#VisceralMedicine'
);
CREATE TYPE "project-name"."supporting__sphn_Interpretation__sphn_hasOutput__valueset_iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ASCII',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AcidCitrateDextrose',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AddictionMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AlcoholBased',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AldehydeBased',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AldehydeBasedStabilizerForCTCs',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AllergologyAndClinicalImmunology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#AllprotectTissueReagent',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Anaesthesiology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Angiology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Billing',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Biobank',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CardiacAndThoracicVascularSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Cardiology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CaseReportForm',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CellPreparationTubeCitrate',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CellPreparationTubeHeparin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ChemicalAdditivesStabilizers',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ChildAndAdolescentPsychiatryAndPsychotherapy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ChildAndAdolescentPsychology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ChiropracticSpecialist',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CitratePhosphateDextrose',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ClinicalDataPlatform',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ClinicalPharmacologyAndToxicology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ClinicalPsychology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ClinicalRegistry',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Cohort',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CommunityPharmacy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Company',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Complementary',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CongenitalAbnormality',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#CurvedRodShaped',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#DataRepository',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Death',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#DermatologyAndVenereology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#DuBois',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#EDTAAndGel',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Elongated',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#EndocrinologyAndDiabetology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#EntireCell',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ExternalLaboratory',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Fatal',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#FederalOffice',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Filamentous',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Foreign',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ForensicMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Gastroenterology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#General',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GeneralInternalMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GeneralMedicalPractitioner',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GeneralMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GeriatricMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Glass',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GreaterThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#GynaecologyAndObstetrics',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Haematology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HandSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HealthInsurance',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HealthPsychology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HealthcareInformationSystem',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HeatStabilization',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Hirudin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Hospital',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HospitalPharmacy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#HospitalisationOrProlongation',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ISO88591',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Infectology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#IntensiveCareMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#InterResidueCoordinates',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#InternalMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThan',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LessThanOrEqual',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LifeThreatening',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LithiumHeparin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LithiumHeparinAndGel',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#LithiumHeparinAndRubberPlug',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#MD5',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#MedicalGenetics',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#MedicalOncology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Mosteller',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Multicellular',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Nephrology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Neurology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Neuropsychology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Neurosurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NeutralBufferedFormalin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonAldehydeBasedStabilizerForCellFreeNucleicAcids',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonaldehydeBasedWithoutAceticAcid',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonaldehydeWithAceticAcid',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NonbufferedFormalin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#None',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NoneOfTheConsequencesMentioned',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NotRecovered',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#NuclearMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OMICSFacility',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OccupationalMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Ophthalmology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OptimumCuttingTemperatureMedium',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OrageneCollectionContainerOrEquivalent',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OralAndMaxillofacialSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OralSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Orthodontics',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#OrthopaedicSurgeryAndTraumatologyOfTheLocomotorApparatus',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Other',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Otorhinolaryngology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Ovoid',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneBloodDNA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneBloodRNAplus',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneBoneMarrowRNA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PAXgeneTissue',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PaediatricSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Paediatrics',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Pathology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PatientCare',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Periodontics',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PermanentDamageOrDisability',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PharmaceuticalMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Pharmacy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PhysicalFiltrationSystem',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PhysicalMedicineAndRehabilitation',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PlasticReconstructiveAndAestheticSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Pneumology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PolyethyleneTubeSterile',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PolypropyleneTubeSterile',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PotassiumEDTA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PreventionAndPublicHealth',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Principal',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Private',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PrivatePractice',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ProteaseInhibitors',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PsychiatryAndPsychotherapy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Psychotherapy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#QualityControl',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#RNALater',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#RadioOncologyAndRadiotherapy',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Radiology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ReconstructiveDentistry',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Recovered',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Recovering',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#RecoveringWithSequelae',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Research',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ResearchLaboratory',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ResearchOrganization',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ResidueCoordinates',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Rheumatology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#RodShaped',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Round',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#S8820ProteaseInhibitorTabletsOrEquivalent',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SHA256',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SHA512',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Secondary',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SemiPrivate',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SerumSeparatorTubeWithClotActivator',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SerumTubeWithoutClotActivator',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ServiceProvider',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SnapFreezing',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumCitrate',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumEDTA',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumFluoridePotassiumOxalate',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#SodiumHeparin',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Spiral',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#StoolCollectionContainerWithDNAStabilizer',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Supplementary',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Surgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#TemporarilySeriousImpactMedicallyImportant',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#TempusTube',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#ThoracicSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#TraceElementsTube',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#TropicalAndTravelMedicine',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#UTF16',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#UTF8',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Unicellular',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#University',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Urology',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#VacuumTechnologyStabilization',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#VascularSurgery',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#VisceralMedicine'
);
CREATE TYPE "project-name"."supporting__sphn_SemanticMapping__sphn_hasPurpose__iri_type" AS ENUM (
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Billing',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#PatientCare',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#QualityControl',
    'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Research'
);