# Update from release 1.6.1 to 1.7.0

## Introduction
We here report the manual changes necessary to migrate from SPHN Connector release 1.6.1 to release 1.7.0 without dropping the Docker volumes. If the Docker volumes are dropped (all data is lost) no migration process is required. In case you encounter any issues please contact the support team at the email [fair-data-team@sib.swiss](mailto:fair-data-team@sib.swiss).

**Disclaimer**: the process described here is related only to the update 1.6.1 --> 1.7.0. We did not test the migration for other releases.

## Breaking changes
- Airflow metadata is now written in PostgreSQL in a new database named `airflow_postgres`. This database is not present in release 1.6.1 and must therefore be created manually after the startup of the SPHN Connector.

## Update procedure

Small adaptations might be needed depending on the users permissions (e.g. usage of `sudo` might be required)

1. Stop the SPHN Connector. **DO NOT** remove the volumes when turning down the SPHN Connector or the data will be lost.
    ```bash
    docker compose down
    ```

2. Checkout to the new release branch
    ```bash
    git checkout 1.7.0
    ```

3. Execute the configuration script and fix any issue that is raised. Substitute `<connector_repository_folder>` with the path to the SPHN Connector repository. If you are executing it from the same directory where the file is located, simply replace it with `.`
    ```bash
    <connector_repository_folder>/setup.sh
    ```

4. Start the SPHN Connector
    ```bash
    docker compose up -d
    ```

5. You might observe that `connector` container did not start property and it's restarting. This is an expected behavior. 
    ```bash
    docker ps
    ```

6. Execute the migration script `release-update/update_from_1.6.1_to_1.7.0.sh`. Substitute `<connector_repository_folder>` with the path to the SPHN Connector repository.  If you are executing it from the repository directory, simply replace it with `.`. The script contains the necessary steps for creating the missing database.
    ```bash
    <connector_repository_folder>/release-update/update_from_1.6.1_to_1.7.0.sh
    ```

7. Monitor the status of the `connector` container and make sure that it restarts successfully
    ```bash
    docker ps
    ```
    ```bash
    docker logs connector -f
    ```