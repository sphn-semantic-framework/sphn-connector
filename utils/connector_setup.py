#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'connector_setup.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
from typing import Union
import requests
import json
import urllib3
import glob
import os
import random
import string
import argparse
import csv
from pathlib import Path 
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
urllib3.disable_warnings(urllib3.exceptions.DependencyWarning)

class ConnectorSetup(object):
    """Class containing all configuration variables and methods for running the script
    """
    def __init__(self, 
                 api_user: str, 
                 api_password: str, 
                 host: str, 
                 projects: int, 
                 users: int, 
                 sphn_schema: str, 
                 project_schema: str, 
                 ext_terminology_path: str,
                 output_path: str):
        """Class constructor

        Args:
            api_user (str): API user
            api_password (str): API password
            host (str): host name
            projects (int): number of projects to create
            users (int): number of users to create
            sphn_schema (str): SPHN schema
            project_schema (str): project specific schema
            ext_terminology_path (str): path to folder containing external terminologies
            output_path (str): output path to store created credentials
        """
        self.url = f"https://{host}:1443"
        self.projects = projects
        self.users = users
        self.api_user, self.api_password = ConnectorSetup._get_credentials(api_user=api_user, api_password=api_password)
        self.sphn_schema = sphn_schema
        self.project_schema = project_schema
        self.ext_terminologies = ext_terminology_path
        self.output_path = output_path
        intro = f"""{'-'*50}
Start SPHN Connector configuration
{'-'*50}
    Number of projects to create: {self.projects}
    Number of users to create: {self.users}
    API user: {self.api_user}
    SPHN schema path: {self.sphn_schema}"""
        if self.project_schema is not None:
            intro += f"""
Project specific schema path: {self.project_schema}"""
        if ext_terminology_path is not None:
            intro += f"""
    External terminologies load from '{self.ext_terminologies}': {", ".join(os.listdir(self.ext_terminologies))}"""
        print(intro)

    @staticmethod
    def _get_credentials(api_user: str, api_password: str) -> Union[str, str]:
        """Get API credentials

        Args:
            api_user (str): API user
            api_password (str): API password

        Returns:
            Union[str, str]: API credentials
        """
        parent_directory = os.path.dirname(os.path.dirname(Path( __file__ ).absolute()))
        env_file = os.path.join(parent_directory, ".env")
        with open(env_file, 'r', encoding='utf-8') as in_file:
            for line in in_file:
                if line:
                    sline = line.split('=')
                    if api_user is None and sline[0] == 'API_USER':
                        api_user = sline[1].split('# Definition')[0].strip().strip('"')
                    if api_password is None and sline[0] == 'API_PASSWORD':
                        api_password = sline[1].split('# Definition')[0].strip().strip('"')
        return api_user, api_password

    def get_response(self) -> object:
        """Response of a request with user credentials.

        Returns:
            object: requests response
        """
        return requests.post(
            self.url + "/token",
            data={
                "username": self.api_user,
                "password": self.api_password,
            },
            verify=False,
            allow_redirects=False,
            timeout=(15, None),
        )

    def get_header(self) -> dict:
        """Header information including access token.

        Returns:
            dict: headers
        """
        response = self.get_response()
        token = json.loads(response.text)
        header = {"Authorization": "Bearer " + token["access_token"]}
        return header

    def initialize(self):
        """Start the creation of objects
        """
        self.create_projects()
        self.upload_external_files()
        self.create_users()
        print(f"\n{'-'*50}\nSPHN Connector configured\n{'-'*50}")

    def create_projects(self):
        """Create projects
        """
        if self.projects != 0:
            print(f"{'-'*50}\nStart creation of {self.projects} projects\n{'-'*50}")
            with open(self.sphn_schema, encoding='utf-8') as sphn_schema:
                schema_files = {"sphn_schema": (os.path.basename(self.sphn_schema), sphn_schema.read())}
                if self.project_schema is not None:
                    with open(self.project_schema, encoding='utf-8') as project_schema:
                        schema_files['project_specific_schema'] = (os.path.basename(self.project_schema), project_schema.read())
                for i in range(1, self.projects + 1):
                    print(f"Creating project 'project-{i}'")
                    response = requests.post(self.url + "/Create_project", params={"project": f"project-{i}"}, files=schema_files, headers=self.get_header(), verify=False, timeout=300)
                    print(response.text)
            print()

    def upload_external_files(self):
        """Upload external terminologies for created projects
        """
        if self.projects != 0 and self.ext_terminologies is not None:
            print(f"{'-'*50}\nStart upload of external terminologies\n{'-'*50}")
            external_files = []
            for file in glob.glob(os.path.join(self.ext_terminologies, '*')):
                with open(file, encoding='utf-8') as terminology:
                    external_files.append(("files", (os.path.basename(file), terminology.read())))

            for i in range(1, self.projects + 1):
                print(f"Uploading external files into project 'project-{i}'")
                response = requests.post(self.url + "/Upload_external_files", params={"project": f"project-{i}", "files_type": "External Terminology"}, files=external_files, headers=self.get_header(), verify=False, timeout=300)
                print(response.text)
            print()

    @staticmethod
    def _generate_password() -> str:
        """Genrate random password

        Returns:
            str: password
        """
        length = 16
        characters = string.ascii_letters + string.digits
        return ''.join(random.choice(characters) for _ in range(length))

    def create_users(self):
        """Create standard users and store the credentials in current folder
        """
        if self.users != 0:
            print(f"{'-'*50}\nStart creation of standard {self.users} users\n{'-'*50}")
            credentials = {}
            user_type = "Standard"
            for i in range(1, self.users + 1):
                username = f"user_{i}"
                print(f"Creating standard user '{username}'")
                password = ConnectorSetup._generate_password()
                credentials[username] = password
                response = requests.post(self.url + "/Create_new_user", data={"user": username, "password": password}, params={"user_type": user_type}, headers=self.get_header(), verify=False, timeout=10)
                print(response.text)
            self.print_report(credentials=credentials)
    
    def print_report(self, credentials: dict):
        """Print report

        Args:
            credentials (dict): credentials
        """
        if self.output_path is not None:
            with open(self.output_path, "w", newline="", encoding='utf-8') as file:
                writer = csv.writer(file)
                writer.writerow(["User", "Password"])
                for key, value in credentials.items():
                    writer.writerow([key, value])
        else:
            print(f"\n{'-'*50}\nCreated credentials (user, password)\n{'-'*50}")
            for key, value in credentials.items():
                print(f"{key}, {value}")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Workshop setup')
    parser.add_argument("sphn_schema", help="SPHN schema path")
    parser.add_argument("-ext", "--ext_terminologies", action="store", default=None, help="Folder path to external terminologies")
    parser.add_argument("-np", "--projects", type=int, action="store", default=1, help="Number of projects to create")
    parser.add_argument("-nu", "--users", type=int, action="store", default=0, help="Number of users to create")
    parser.add_argument("-ho", "--host", action="store", default="localhost", help="Host name of the machine")
    parser.add_argument("-u", "--api_user", action="store", default=None, help="Username for API access. If not specified taken from the .env")
    parser.add_argument("-p", "--api_password", action="store", default=None)
    parser.add_argument("-ps", "--project_schema", action="store", default=None, help="Project schema path")
    parser.add_argument("-o", "--output_path", action="store", default=None, help="Output path for created credentials list")
    args = parser.parse_args()
    connector_setup = ConnectorSetup(api_user=args.api_user, api_password=args.api_password, host=args.host, projects=args.projects, 
                                     users=args.users, sphn_schema=args.sphn_schema, project_schema=args.project_schema,
                                     ext_terminology_path=args.ext_terminologies, output_path=args.output_path)
    connector_setup.initialize()