#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'generate_config_file.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

import argparse
import json
import os
import logging
import hashlib
import secrets
from pathlib import Path
logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s', level=logging.getLevelName('INFO'), datefmt='%Y-%m-%d %H:%M:%S')

def get_api_secret_key(api_secret_key: str) -> str:
    """Return hexadecimal secret key for API

    Args:
        api_secret_key (str): input secret key

    Returns:
        str: hexadecimal secret key
    """
    if not api_secret_key:
        return secrets.token_hex(32)
    else:
        return hashlib.sha256(api_secret_key.encode()).hexdigest()


def main():
    """Main method
    """
    environment_variables = dict(os.environ)    
    api_secret_key = get_api_secret_key(api_secret_key=environment_variables.get('API_SECRET_KEY'))

    config_json = {
        "data_provider_id": environment_variables['DATA_PROVIDER_ID'],
        "api": {
            "secret_key": api_secret_key,
            "user": environment_variables['API_USER'],
            "password": environment_variables['API_PASSWORD']
        },
        "minio": {
            "access_key": environment_variables.get('MINIO_ACCESS_KEY'),
            "secret_key": environment_variables.get('MINIO_SECRET_KEY'),
            "compression": environment_variables.get('MINIO_COMPRESSION', 'OFF')
        },
        "external_s3":{
            "activated": environment_variables.get("USE_EXTERNAL_S3", False),
            "access_key": environment_variables.get("EXTERNAL_S3_ACCESS_KEY"),
            "secret_key": environment_variables.get("EXTERNAL_S3_SECRET_KEY"),
            "URL": environment_variables.get("EXTERNAL_S3_URL"),   
            "connector_bucket": environment_variables.get('EXTERNAL_CONNECTOR_S3_BUCKET', 'connector'),
            "einstein_bucket": environment_variables.get('EXTERNAL_EINSTEIN_S3_BUCKET', 'einstein')
        },
        "postgres": {
            "user": environment_variables['POSTGRES_USER'],
            "password": environment_variables['POSTGRES_PASSWORD'],
            "ui_email": environment_variables['POSTGRES_UI_EMAIL'],
            "ui_password": environment_variables['POSTGRES_UI_PASSWORD']
        },
        "airflow": {
            "user": environment_variables['AIRFLOW_USER'],
            "password": environment_variables['AIRFLOW_PASSWORD'],
            "firstname": environment_variables['AIRFLOW_FIRSTNAME'],
            "lastname": environment_variables['AIRFLOW_LASTNAME'],
            "email": environment_variables['AIRFLOW_EMAIL']
        },
        "grafana": {
            "user": environment_variables['GRAFANA_ADMIN_USER'],
            "password": environment_variables['GRAFANA_ADMIN_PASSWORD'],
            "postgres_user": environment_variables['GRAFANA_POSTGRES_USER'],
            "postgres_user_password": environment_variables['GRAFANA_POSTGRES_USER_PASSWORD'],
        },

        "machine": {
            "memory": environment_variables.get('SPHN_CONNECTOR_MEMORY'),
            "cpus": environment_variables.get('SPHN_CONNECTOR_CPUS')
        },
        "parallelization": {
            "parallel_projects": environment_variables.get('PARALLEL_PROJECTS', 3) if environment_variables.get('PARALLEL_PROJECTS', 3) else 3,
        },
        "token_timeout": {
            "access_token_timeout" : environment_variables.get('ACCESS_TOKEN_TIMEOUT', 3) if environment_variables.get('ACCESS_TOKEN_TIMEOUT', 3) else 3
        },
        "einstein": {
            "activated": environment_variables.get('EINSTEIN', False),
            "endpoint": environment_variables.get('EINSTEIN_ENDPOINT'),
            "api_user": environment_variables.get('EINSTEIN_API_USER'),
            "api_password": environment_variables.get('EINSTEIN_API_PASSWORD')
        }
    }

    parent_directory = os.path.dirname(os.path.dirname(Path(__file__).absolute()))
    version_file = os.path.join(parent_directory, "version.txt")
    if os.path.exists(version_file):
        version_variables= {}
        with open(version_file, 'r', encoding='utf-8') as in_file:
            for line in in_file:
                if line:
                    sline = line.split('=')
                    version_variables[sline[0]] = sline[1].strip().strip('"')
        
        config_json["version"]= {
            "commit_hash": version_variables['GIT_HASH'],
            "is_tag": version_variables['IS_TAG'],
            "version_name": version_variables['VERSION_NAME'],
            "commit_timestamp": version_variables['COMMIT_TIMESTAMP']
        }

    config_file = os.path.join(parent_directory, "config.json")
    with open(config_file, "w", encoding='utf-8') as out_file:
        out_file.write(json.dumps(config_json, indent=4))


if __name__ == '__main__':
    logging.info("Generate configuration file config.json")
    parser = argparse.ArgumentParser(description='Generate configuration file config.json')
    args = parser.parse_args()
    main()