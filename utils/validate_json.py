#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'validate_json.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from jsonschema import Draft202012Validator
from jsonschema import FormatChecker
import json
import argparse
import re
import logging

def date_checker(value: str) -> bool:
    """Check date

    Args:
        value (str): date value

    Returns:
        bool: date value checked successfully
    """
    date_regex = r"^\d\d\d\d-(0?[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])?(Z|[+-](?:[0][0-9]|[1][0-4]):[0-5][0-9])?$"
    return bool(re.match(date_regex, value))

def time_checker(value: str) -> bool:
    """Check time

    Args:
        value (str): time value

    Returns:
        bool: time value checked successfully
    """
    time_regex = r"^(?:[01]\d|2[0-4]):(?:[0-5]\d):(?:[0-5]\d)(?:[.][0-9]{3})?(Z|[+-](?:[0][0-9]|[1][0-4]):[0-5][0-9])?$"
    return bool(re.match(time_regex, value))

def date_time_checker(value: str):
    """Check date-time

    Args:
        value (str): date-time value

    Returns:
        bool: date-time value checked successfully
    """
    date_time_regex = r"^(?:[0-9]{4}-[0-9]{2}-[0-9]{2})(?:[T])(?:[01]\d|2[0-4]):(?:[0-5]\d):(?:[0-5]\d)(?:[.][0-9]{3})?(Z|[+-](?:[0][0-9]|[1][0-4]):[0-5][0-9])?"
    return bool(re.match(date_time_regex, value))

def validate_data(schema_file: str, data_file: str, check_formats: bool):
    """Validates patient data in JSON format against JSON schema

    Args:
        schema_file (str): file path to the JSON schema
        data_file (str): file path to the JSON data file
        check_formats (bool): validate formats
    """
    logging.info(f"Execute validation for patient file '{data_file}' against JSON schema defined by file '{schema_file}'")
    try:
        schema = json.load(open(schema_file))
        patient_data = json.load(open(data_file))
        if check_formats:
            format_checker = FormatChecker()
            format_checker.checkers['date'] = (date_checker, ValueError)
            format_checker.checkers['time'] = (time_checker, ValueError)
            format_checker.checkers['date-time'] = (date_time_checker, ValueError)
            v = Draft202012Validator(schema, format_checker=format_checker)
            errs=v.iter_errors(patient_data)
        else:
            v = Draft202012Validator(schema)
            errs=v.iter_errors(patient_data)
        if errs is None:
            logging.info("Validation successful")
        else:
            logging.info("Validation failed")
            for err in errs:
                logging.info(f"JSON Path: {err.json_path}")
                logging.error(err.message)
                print()
    except Exception as exc:
        logging.error(f"Validation failed: {exc}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='JSON schema validator')
    parser.add_argument("schema_file", action="store", help="Path to the JSON schema file")
    parser.add_argument("data_file", action="store", help="Path to the JSON file to validate")
    parser.add_argument("-f", "--check_formats", action="store_true", default=False, help="Activate validation of JSON formats")
    args = parser.parse_args()
    logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s', level="INFO", datefmt='%Y-%m-%d %H:%M:%S')
    validate_data(schema_file=args.schema_file, data_file=args.data_file, check_formats=args.check_formats)
