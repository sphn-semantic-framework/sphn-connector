# Utility scripts
This documents briefly describes the utility scripts available to the user in the repository folder `/utils`.

## Connector setup `/utils/connector_setup.py`
The script `/utils/connector_setup.py` can be used to automate the creation of projects and users in the SPHN Connector.

### Usage

The script requires the path to the SPHN schema. By default the script generates a single project. The creation of users must be configured explicitly by setting the number of users to create via `-nu` flag. Usage overview of the script:
```
usage: connector_setup.py [-h] [-ext EXT_TERMINOLOGIES] [-np PROJECTS] [-nu USERS] [-ho HOST] [-u API_USER] [-p API_PASSWORD] [-ps PROJECT_SCHEMA] [-o OUTPUT_PATH] sphn_schema

Workshop setup

positional arguments:
  sphn_schema         SPHN schema path

optional arguments:
  -h, --help            show this help message and exit
  -ext EXT_TERMINOLOGIES, --ext_terminologies EXT_TERMINOLOGIES
                        Folder path to external terminologies
  -np PROJECTS, --projects PROJECTS
                        Number of projects to create
  -nu USERS, --users USERS
                        Number of users to create
  -ho HOST, --host HOST
                        Host name of the machine
  -u API_USER, --api_user API_USER
                        Username for API access. If not specified taken from the .env
  -p API_PASSWORD, --api_password API_PASSWORD
  -ps PROJECT_SCHEMA, --project_schema PROJECT_SCHEMA
                        Project schema path
  -o OUTPUT_PATH, --output_path OUTPUT_PATH
                        Output path for created credentials list
```

### Example

In the SPHN Connector root folder we create a folder `schema` and we store the latest schema file `sphn_schema_2024.2.ttl`. We create a second folder `external-terminologies` and we store all the external terminology files. Then we execute

```
python ./utils/connector_setup.py ./schema/sphn_schema_2024.2.ttl -ext ./external-terminologies -np 1 -nu 1
```

The command creates one project and a new user. The output of the execution is

```
--------------------------------------------------
Start SPHN Connector configuration
--------------------------------------------------
    Number of projects to create: 1
    Number of users to create: 1
    API user: user
    SPHN schema path: ./schema/sphn_schema_2024.2.ttl
    External terminologies load from ./external-terminologies/: sphn_ucum_2024-1.ttl, sphn_chop_2024-2016-1.ttl, sphn_so_20211122-1.ttl, sphn_icd-10-gm_2024-2015-1.ttl, sphn_loinc_2.74-1.ttl, sphn_geno_20220810-1.ttl, sphn_hgnc_20221107-1.ttl, sphn_atc_2024-2016-1.ttl, snomed-ct-20221231.ttl

--------------------------------------------------
Start creation of 1 projects
--------------------------------------------------
Creating project 'project-1'
Project 'project-1' successfully created

--------------------------------------------------
Start upload of external terminologies
--------------------------------------------------
Uploading external files into project 'project-1'
Files 'sphn_ucum_2024-1.ttl', 'sphn_chop_2024-2016-1.ttl', 'sphn_so_20211122-1.ttl', 'sphn_icd-10-gm_2024-2015-1.ttl', 'sphn_loinc_2.74-1.ttl', 'sphn_geno_20220810-1.ttl', 'sphn_hgnc_20221107-1.ttl', 'sphn_atc_2024-2016-1.ttl', 'snomed-ct-20221231.ttl' successfully ingested as external terminology files

--------------------------------------------------
Start creation of standard 1 users
--------------------------------------------------
Creating standard user 'user_1'
API user 'user_1' successfully configured

--------------------------------------------------
Created credentials (user, password)
--------------------------------------------------
user_1, q6zK3Eh5TfLCNNca

--------------------------------------------------
SPHN Connector configured
--------------------------------------------------
```
## JSON file validation `/utils/validate_json.py`
This script can be used to validate a JSON file against a JSON schema. The user provides the path to the JSON schema and to the JSON file and executes the script. Optionally they can also check for JSON formats. The output of the validation is printed to the console.
### Usage
Helper message
```
usage: validate_json.py [-h] [-f] schema_file data_file

JSON schema validator

positional arguments:
  schema_file          Path to the JSON schema file
  data_file            Path to the JSON file to validate

optional arguments:
  -h, --help           show this help message and exit
  -f, --check_formats  Activate validation of JSON formats
```

### Example
In the JSON file `patient_data_input.json` we have a property `sphn:invalidProperty`
```json
{
  "sphn:SubjectPseudoIdentifier": {
        "id": "patient_1",
        "sphn:hasIdentifier": "c43ea5d6-7c6c-4d86-805c-841be7b0f2ae",
        "sphn:invalidProperty": "abc"
    }
}
```
which is not accepted by the JSON schema `json_schema.json`
```json
{
  "sphn:SubjectPseudoIdentifier": {
            "type": "object",
            "description": "SPHN Concept 'SubjectPseudoIdentifier'",
            "properties": {
                "id": {
                    "type": "string",
                    "description": "ID of SPHN Concept 'SubjectPseudoIdentifier'"
                },
                "sphn:hasIdentifier": {
                    "description": "Value for 'hasIdentifier' property",
                    "type": "string"
                }
            },
            "required": [
                "id",
                "sphn:hasIdentifier"
            ],
            "additionalProperties": false
        }
}
```
Therefore, executing the command 
```
python ./utils/validate_json.py json_schema.json patient_data_input.json
```
gives the following output
```
2023-10-16 14:45:04 INFO     Execute validation for patient file 'patient_data_input.json' against JSON schema defined by file 'json_schema.json'
2023-10-16 14:45:04 INFO     Validation failed
2023-10-16 14:45:04 INFO     JSON Path: $.sphn:SubjectPseudoIdentifier
2023-10-16 14:45:04 ERROR    Additional properties are not allowed ('sphn:invalidProperty' was unexpected)
```

## Database ingestion `/utils/ingest_into_database.py`
The legacy script `/utils/ingest_into_database.py` was used to import CSV files extracted from another database into the SPHN Connector database.

### Usage

Helper message
```
usage: ingest_into_database.py [-h] [-u USER] [-p PASSWORD] [-prj PROJECT] [-t TABLES_FOLDER] [-cm CSV_MAPPING_FILE] [-em EXCEL_MAPPING_FILE]

Database ingestion

optional arguments:
  -h, --help            show this help message and exit
  -u USER, --user USER  Postgres database user
  -p PASSWORD, --password PASSWORD
                        Postgres database password
  -prj PROJECT, --project PROJECT
                        Project name
  -t TABLES_FOLDER, --tables_folder TABLES_FOLDER
                        Local folder path with tables data
  -cm CSV_MAPPING_FILE, --csv_mapping_file CSV_MAPPING_FILE
                        Path to CSV mapping file
  -em EXCEL_MAPPING_FILE, --excel_mapping_file EXCEL_MAPPING_FILE
                        Path to Excel mapping file
```

The script parameters are:
- Credentials for accessing the `sphn_tables` Postgres database.
- Name of the project in which we want to ingest the data.
- Path to the folder containing CSV files with data extracted from the source database.
- The mapping file which reports the mapping between the column names in the source database and in the target database.
- Excel mapping file (same as previous point but in Excel format).

Example of CSV mapping file:
```
new_usz_label;sphn_connector
id;id
Code__id;sphn_hasCode__id
Code__hasCodingSystemAndVersion;sphn_hasCode__sphn_hasCodingSystemAndVersion
Code__hasIdentifier;sphn_hasCode__sphn_hasIdentifier
Code__hasName;sphn_hasCode__sphn_hasName
Code__termid;sphn_hasCode__termid
Code__iri;sphn_hasCode__iri
```

## Generate configuration file `/utils/generate_config_file.py`
The script `/utils/generate_config_file.py` is used internally by the SPHN Connector during the start up of the application. It is not meant to be used by the standard users.
The script extracts the values of the parameters defined in the `.env` file and generates a JSON file `config.json`.

### Usage
To execute the script locally in a development environment just execute the command from the root directory of the cloned repository
```
python ./utils/generate_config_file.py
```

Helper message
```
usage: generate_config_file.py [-h]

Generate configuration file config.json

optional arguments:
  -h, --help  show this help message and exit
```

## Generate test certificates `/utils/generate_certs.sh`
The script `/utils/generate_certs.sh` can be used to generate certificates `root-certificate.cert`, `reverse-proxy.cert`, `reverse-proxy.key` for testing Connector and Einstein connection.

### Usage

The user should change directory into the `/utils` folder first. Then they should modify the parameters `PASSPHRASE`, `IP`, and `DNS`. `PASSPHRASE` is the password of the generated `root-certificate.cert`. If the host machine that uses the generated `reverse-proxy.cert` certificate is accessible via an IP address, then the `IP` variable should be specified. On the other hand, if it is accessible via custom DNS, then the `DNS` variable should be specified. Other parameters can also be modified if wished but this is not required.
```
cd ./utils
./generate_certs.sh
```

If no other parameters are changed, then the subfolder `/certs` is created. The following files are generated:
```
reverse-proxy.cert
reverse-proxy.csr
reverse-proxy.ext
reverse-proxy.key
root-certificate.cert
root-certificate.key
root-certificate.srl
```
The files `root-certificate.cert`, `reverse-proxy.cert`, `reverse-proxy.key` can then be copied in the Connector folder `/data-transfer`. To issue another certificate for Einstein under the same root certificate, the user should update the parameters (e.g. IP address, if necessary) and then retrigger the script. Assuming that the `root-certificate.cert` has not been removed, it will create new reverse-proxy files that should then be copied, together with the used `root-certificate.cert` in the folder `/data-transfer` in Einstein.

### Example

Configure parameters, for example:
```
PASSPHRASE="my-root-password"
IP="172.28.154.13"
DNS="" 
```

Create root certificate and issue reverse-proxy certificates

```
cd ./utils
./generate_certs.sh
```

Output

```
Root CA certificate not found. Creating a new one...
Generating RSA private key, 2048 bit long modulus (2 primes)
........................+++++
........................................+++++
e is 65537 (0x010001)
Generating RSA private key, 2048 bit long modulus (2 primes)
................................................................+++++
...+++++
e is 65537 (0x010001)
Signature ok
subject=C = CH, ST = ZH, L = Zurich, O = SIB, OU = SPHN, CN = Reverse proxy certificate
Getting CA Private Key
```

Copy generated files into Connector `/data-transfer` folder:
```
cp ./certs/root-certificate.cert ../data-transfer
cp ./certs/reverse-proxy.cert ../data-transfer
cp ./certs/reverse-proxy.key ../data-transfer
```

Update parameters if necessary and generate reverse-proxy files for Einstein:
```
./generate_certs.sh
```

Output:
```
Root CA certificate already exists. Skipping creation.
Generating RSA private key, 2048 bit long modulus (2 primes)
........................................................+++++
.............................................................+++++
e is 65537 (0x010001)
Signature ok
subject=C = CH, ST = ZH, L = Zurich, O = SIB, OU = SPHN, CN = Reverse proxy certificate
Getting CA Private Key
```

Copy generated files into Einstein `/data-transfer` folder:
```
cp ./certs/root-certificate.cert /einstein-repo-path/data-transfer
cp ./certs/reverse-proxy.cert /einstein-repo-path/data-transfer
cp ./certs/reverse-proxy.key /einstein-repo-path/data-transfer
```

## Scan images vulnerabilities `/utils/scan_images.sh`
The script `/utils/scan_images.sh` is a tool that can be used by the development team to check for images vulnerabilities. The Docker images are built and scanned with the `trivy` package. This script should not be used by the data providers. The following images are scanned:
```
sphn-connector_api:latest
sphn-connector_data_handler:latest
sphn-connector_minio:latest
sphn-connector_connector:latest
sphn-connector_postgres:latest
sphn-connector_pgadmin:latest
sphn-connector_reverse-proxy:latest
sphn-connector_grafana:latest
sphn-connector_setup:latest
```
The script only returns `HIGH`, `CRITICAL` severity vulnerabilities that are marked as fixable.
### Usage
Scan local images without building them:
```
./utils/scan_images.sh
```
Build Docker images and scan them:
```
./utils/scan_images.sh build
```

### Example
The following commands
```
./utils/scan_images.sh
```
gives (example)
```
====================================================================
           Scanning: sphn-connector_api:latest
====================================================================
2024-10-04T09:27:05+02:00       INFO    Vulnerability scanning is enabled
2024-10-04T09:27:06+02:00       INFO    Detected OS     family="alpine" version="3.20.3"
2024-10-04T09:27:06+02:00       INFO    [alpine] Detecting vulnerabilities...   os_version="3.20" repository="3.20" pkg_num=46
2024-10-04T09:27:06+02:00       INFO    Number of language-specific files       num=2
2024-10-04T09:27:06+02:00       INFO    [gobinary] Detecting vulnerabilities...
2024-10-04T09:27:06+02:00       INFO    [python-pkg] Detecting vulnerabilities...

sphn-connector_api:latest (alpine 3.20.3)

Total: 0 (HIGH: 0, CRITICAL: 0)


====================================================================
           Scanning: sphn-connector_data_handler:latest
====================================================================
2024-10-04T09:27:06+02:00       INFO    Vulnerability scanning is enabled
2024-10-04T09:27:06+02:00       INFO    Detected OS     family="alpine" version="3.20.3"
2024-10-04T09:27:06+02:00       INFO    [alpine] Detecting vulnerabilities...   os_version="3.20" repository="3.20" pkg_num=15
2024-10-04T09:27:06+02:00       INFO    Number of language-specific files       num=0

sphn-connector_data_handler:latest (alpine 3.20.3)

Total: 0 (HIGH: 0, CRITICAL: 0)
...
```