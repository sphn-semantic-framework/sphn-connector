#!/bin/bash
#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'scan_images.sh' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Check if "build" argument is provided
if [[ "$1" == "build" ]]; then
    DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
    echo "Build argument detected. Executing build steps..."
    docker-compose -f "$DIR/../docker-compose-build.yaml" build
    docker build -t sphn-connector_setup:latest "$DIR/../setup/"
else
    echo "No build argument detected. Skipping build steps..."
fi

# Define colors
RED="\033[1;31m"
GREEN="\033[1;32m"
RESET="\033[0m"  # Reset color

# List of Docker images
images=(
  "sphn-connector_api"
  "sphn-connector_data_handler"
  "sphn-connector_minio"
  "sphn-connector_connector"
  "sphn-connector_postgres"
  "sphn-connector_pgadmin"
  "sphn-connector_reverse-proxy"
  "sphn-connector_grafana"
  "sphn-connector_setup"
)

# Loop over each image
for image in "${images[@]}"; do
  # Print a dynamic title for each image
  echo -e "${GREEN}====================================================================${RESET}"
  echo -e "${RED}           Scanning: $image:latest${RESET}"
  echo -e "${GREEN}====================================================================${RESET}"

  # Execute the trivy command
  trivy image "$image:latest" --scanners vuln --severity HIGH,CRITICAL --ignore-unfixed

  # Add a newline between each image for better readability
  echo ""
done
