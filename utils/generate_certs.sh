#!/bin/bash
#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'generate_certs.sh' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Set variables
CERTS_DIR="certs"
ROOT_CA_KEY="$CERTS_DIR/root-certificate.key"
ROOT_CA_CERT="$CERTS_DIR/root-certificate.cert"
ROOT_CA_SERIAL="$CERTS_DIR/root-certificate.srl"
SERVER_KEY="$CERTS_DIR/reverse-proxy.key"
SERVER_CSR="$CERTS_DIR/reverse-proxy.csr"
SERVER_CERT="$CERTS_DIR/reverse-proxy.cert"
EXT_FILE="$CERTS_DIR/reverse-proxy.ext"
DAYS_VALID=365
KEY_SIZE=2048
PASSPHRASE="password" # Update password of root certificate
IP=""  # Update this with the actual IP address or leave empty. For example for local testing on Linux the IP address of the host where the Connector/Einstein is running (hostname -I)
DNS="" # Update this with the actual DNS or leave empty. For example if the Connector/Einstein services are accessible via a customized DNS

# Ensure the certs directory exists
mkdir -p "$CERTS_DIR"

# Function to create Root CA
create_root_ca() {
    # Create Root CA private key
    openssl genrsa -des3 -out $ROOT_CA_KEY -passout pass:$PASSPHRASE $KEY_SIZE 

    # Create a self-signed Root CA certificate
    openssl req -x509 -new -nodes -key $ROOT_CA_KEY -sha256 -days $DAYS_VALID -out $ROOT_CA_CERT -passin pass:$PASSPHRASE -subj "/C=CH/ST=ZH/L=Zurich/O=SIB/OU=SPHN/CN=Root certificate"
}

# Function to issue a reverse proxy certificate
issue_certificate() {

    # Check if IP and DNS are defined
    if [ -z "$IP" ] && [ -z "$DNS" ]; then
        echo "Error: Either IP or DNS must be defined."
        exit 1
    fi

    # Create reverse proxy private key
    openssl genrsa -out $SERVER_KEY $KEY_SIZE

    # Create a CSR for the reverse proxy
    openssl req -new -key $SERVER_KEY -out $SERVER_CSR -subj "/C=CH/ST=ZH/L=Zurich/O=SIB/OU=SPHN/CN=Reverse proxy certificate"

    # Create a configuration file for the certificate extension
    echo "authorityKeyIdentifier=keyid,issuer" > $EXT_FILE
    echo "basicConstraints=CA:FALSE" >> $EXT_FILE
    echo "keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment" >> $EXT_FILE
    echo "subjectAltName = @alt_names" >> $EXT_FILE
    echo "[alt_names]" >> $EXT_FILE
    
    if [ -n "$IP" ]; then
        echo "IP.1 = $IP" >> $EXT_FILE
    fi
    
    if [ -n "$DNS" ]; then
        echo "DNS.1 = $DNS" >> $EXT_FILE
    fi

    # Sign the reverse proxy CSR with the Root CA certificate
    openssl x509 -req -in $SERVER_CSR -CA $ROOT_CA_CERT -CAkey $ROOT_CA_KEY -CAcreateserial \
    -out $SERVER_CERT -days $DAYS_VALID -sha256 -extfile $EXT_FILE -passin pass:$PASSPHRASE

    # Set permissions for the reverse proxy key
    chmod 644 $SERVER_KEY
}

# Check if Root CA certificate already exists
if [ ! -f "$ROOT_CA_CERT" ]; then
    echo "Root CA certificate not found. Creating a new one..."
    create_root_ca
else
    echo "Root CA certificate already exists. Skipping creation."
fi

issue_certificate