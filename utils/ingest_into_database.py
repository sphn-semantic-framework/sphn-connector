#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'ingest_into_database.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

import traceback
import psycopg2
import pandas as pd
import io
import glob
import os
import argparse


def ingest_data(user: str, password: str, project: str, tables_folder: str, csv_mapping_file: str, excel_mapping_file: str):
    """Ingest data into database

    Args:
        user (str): user for database connection
        password (str): password for database connection
        project (str): name of the initialized project
        tables_folder (str): local folder path containing SPHN tables csv files
        csv_mapping_file (str): local path to csv mapping file with column mappings
        excel_mapping_file (str): local path to excel mapping file with column mappings
    """
    conn = psycopg2.connect(host='localhost', database='sphn_tables', user=user, password=password, port='5432')
    conn.autocommit = True
    cursor = conn.cursor()
    if excel_mapping_file:
        df = pd.read_excel(excel_mapping_file)
    else:
        df = pd.read_csv(csv_mapping_file, sep=';')
    columns_map = {}
    for i,j in zip(df.new_usz_label.tolist(), df.sphn_connector.tolist()):
        if str(i) != 'nan' or str(j) != 'nan':
            columns_map[str(i)] = str(j)

    for csv_file in glob.glob(os.path.join(tables_folder, '*')):
        towrite = io.BytesIO()
        table_name = os.path.basename(csv_file).replace('.csv', '')
        df = pd.read_csv(csv_file, sep=';')
        renamed_df = df.rename(columns=columns_map)
        renamed_df.to_csv(towrite, index=False)
        towrite.seek(0)
        with towrite as file:
            columns = ','.join(file.readline().decode().strip().split(';'))
            try:
                cursor.copy_expert(f"COPY \"{project}\".{table_name} ({columns}) FROM STDIN WITH CSV HEADER", file=file)
                print(f"{'*'*100}\nTable name: {table_name.upper()}\n{'*'*100}\nData ingested\n\n")
            except Exception:
                print(f"{'*'*100}\nTable name: {table_name.upper()}\n{'*'*100}\n{traceback.format_exc()}\n\n")
    conn.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Database ingester')
    parser.add_argument("-u", "--user", action="store", default="user", help="Postgres database user")
    parser.add_argument("-p", "--password", action="store", default="password", help="Postgres database password")
    parser.add_argument("-prj", "--project", action="store", default='123', help="Project name")
    parser.add_argument("-t", "--tables_folder", action="store", default="Tables/", help="Local folder path with tables data")
    parser.add_argument("-cm", "--csv_mapping_file", action="store", default="Mapping_tablenames.csv", help="Path to CSV mapping file")
    parser.add_argument("-em", "--excel_mapping_file", action="store", default=None, help="Path to Excel mapping file")
    args = parser.parse_args()
    ingest_data(user=args.user, password=args.password, project=args.project, tables_folder=args.tables_folder, csv_mapping_file=args.csv_mapping_file, excel_mapping_file=args.excel_mapping_file)