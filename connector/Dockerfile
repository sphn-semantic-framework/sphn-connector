#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'Dockerfile' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

FROM registry.dcc.sib.swiss/sphn-semantic-framework/sphn-connector/python:3.10.15-slim

ENV DEBIAN_PKG="bash dos2unix wget curl git maven=3.8.7-1 sudo libc6 libc-dev-bin libde265-0 libgnutls30 libaom3=3.6.0-1+deb12u1 netcat-openbsd libexpat1=2.5.0-1+deb12u1 libcups2=2.4.2-3+deb12u8 libsqlite3-0=3.40.1-2+deb12u1"
# Folder definitions
ENV QUALITY_HOME=/home/sphn/quality
ENV QUALITY_HOME_TMP=/home/sphn/quality_tmp
ENV STATISTICS_HOME=/home/sphn/statistics
ENV AIRFLOW_HOME=/home/sphn/airflow
ENV AIRFLOW_DAGS=/home/sphn/airflow/dags
ENV UTILS_HOME=/home/sphn/code/utils
ENV SHACLER=/home/sphn/shacler
ENV SPARQLER=/home/sphn/sparqler
ENV CONVERSION=/home/sphn/conversion
ENV CONVERSION_TMP=/home/sphn/conversion_tmp
ENV TZ=Europe/Zurich
#WARNING - When upgrading to a newer Airflow version the constraints.txt needs to be updated accordingly
ENV AIRFLOW_VERSION=2.10.1
ENV PYTHON_VERSION=3.10
ENV CONSTRAINT_URL="https://raw.githubusercontent.com/apache/airflow/constraints-${AIRFLOW_VERSION}/constraints-${PYTHON_VERSION}.txt"
# Get Maven
ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"

# Get Java via the package manager
RUN apt-get update \
    && apt-get install ${DEBIAN_PKG} -y \
    && apt-get install openjdk-17-jdk -y \
    && echo "JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64/" >> /etc/environment

# Create the sphn user (uid 1000)
#Create sphn user & Group with GID = 1000 and UID = 1000
RUN groupadd -g 1000 sphn \
    && useradd -u 1000 -m -d /home/sphn sphn -g 1000

WORKDIR ${AIRFLOW_HOME}
USER root
COPY ./connector/app/requirements.txt ${AIRFLOW_HOME}/requirements.txt
COPY ./connector/app/airflow.sh ${AIRFLOW_HOME}/airflow.sh 

# folder creation for apache installation
# install apache airflow and some python dependencies 
RUN bash -c "mkdir -p /home/sphn/airflow/dags /home/sphn/airflow/plugins /home/sphn/airflow/logs" \
    && sudo pip install "apache-airflow==${AIRFLOW_VERSION}" --constraint "${CONSTRAINT_URL}" \
    && pip install "apache-airflow==${AIRFLOW_VERSION}"  -r ${AIRFLOW_HOME}/requirements.txt \
    && mkdir -p /shared/logs \
    && chmod 777 /shared/logs

RUN dos2unix ${AIRFLOW_HOME}/airflow.sh 
RUN chmod +x ${AIRFLOW_HOME}/airflow.sh

RUN chown -R sphn:sphn ${AIRFLOW_HOME} \
&& chmod -R 755 ${AIRFLOW_HOME}

# Then use user sphn
USER sphn

WORKDIR ${QUALITY_HOME_TMP}
RUN git clone https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-rdf-quality-check-tool.git ${QUALITY_HOME_TMP}/ \
    && git checkout 76b0485ab77c90f7ce1f06a53f893be5553b7bbe \
    && bash -c "rm -rf ./hospital-properties ./ontologies ./queries ./shapes" \
    && mvn clean package \
    && mkdir -p ${QUALITY_HOME}/target \
    && cp ${QUALITY_HOME_TMP}/target/sphn-quality-check-tool-v2.3-full.jar ${QUALITY_HOME}/target \
    && rm -rf ${QUALITY_HOME_TMP} \
    && mkdir -p ${STATISTICS_HOME} \
    && rm -rf /home/sphn/.m2/repository

WORKDIR ${SHACLER}
RUN git clone https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-shacl-generator.git ${SHACLER}/ \
    && git checkout c3e4d6a3c720a5d2cbd2d1a646a63af32a7599cf \
    && pip install -r requirements.txt

WORKDIR ${SPARQLER}
RUN git clone https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema-doc.git ${SPARQLER}/ \
    && git checkout c95bf326842280b2834dae01cbe2c23c6eadcc09 \
    && pip install -r requirements.txt \
    && cp ${SPARQLER}/sparqler/sparqler.py ${AIRFLOW_DAGS}/sparqler_external.py \
    && cp ${SPARQLER}/sparqler/*_connector.rq ${AIRFLOW_DAGS}/ \
    && rm -rf ${SPARQLER}

WORKDIR ${CONVERSION_TMP}
RUN git clone https://sphn-connector-2025:glpat-iLqsCBKtWGsZhrsS3zxW@git.dcc.sib.swiss/hospfair/rmlmapper-java.git ${CONVERSION_TMP}/ \
    && git checkout c100f6a7b6440682434238f55c9068174d7c3187  \
    && mvn install -DskipTests=true -P no-buildnumber \
    && mkdir -p ${CONVERSION} \
    && cp ${CONVERSION_TMP}/target/rmlmapper-6.2.1-r0-all.jar ${CONVERSION} \
    && rm -rf ${CONVERSION_TMP} \
    && rm -rf /home/sphn/.m2/repository

USER root
RUN apt-get remove dos2unix wget curl git maven=3.8.7-1 openssh-client -y \
    && apt autoremove -y \
    && apt-get clean

USER sphn
# Add the generate config
RUN mkdir -p ${UTILS_HOME}
WORKDIR ${UTILS_HOME}
COPY ./utils/generate_config_file.py ${UTILS_HOME}/generate_config_file.py

# Copy python scripts
RUN mkdir -p ${AIRFLOW_DAGS}
COPY ./lib/config.py ${AIRFLOW_DAGS}/config.py
COPY ./lib/database.py ${AIRFLOW_DAGS}/database.py
COPY ./lib/connector.py ${AIRFLOW_DAGS}/connector.py
COPY ./lib/shacler_rules.py ${AIRFLOW_DAGS}/shacler_rules.py
COPY ./lib/database_extractor.py ${AIRFLOW_DAGS}/database_extractor.py
COPY ./lib/backup_restore.py ${AIRFLOW_DAGS}/backup_restore.py
COPY ./lib/enums.py ${AIRFLOW_DAGS}/enums.py
COPY ./lib/rml_generator.py ${AIRFLOW_DAGS}/rml_generator.py
COPY ./lib/pre_checks.py ${AIRFLOW_DAGS}/pre_checks.py
COPY ./lib/de_identification.py ${AIRFLOW_DAGS}/de_identification.py
COPY ./api/app/api.py ${AIRFLOW_DAGS}/api.py
COPY ./connector/app/dags/* ${AIRFLOW_DAGS}/ 
COPY ./lib/rdf_parser.py ${AIRFLOW_DAGS}/rdf_parser.py
COPY ./lib/airflow_lib.py ${AIRFLOW_DAGS}/airflow_lib.py
COPY ./lib/minio_policy.py ${AIRFLOW_DAGS}/minio_policy.py
COPY ./lib/rml_generator_lib.py ${AIRFLOW_DAGS}/rml_generator_lib.py
COPY ./lib/sparqler.py ${AIRFLOW_DAGS}/sparqler.py
COPY ./lib/grafana_counts.py ${AIRFLOW_DAGS}/grafana_counts.py

RUN mkdir -p ${CONVERSION}
COPY ./lib/execute_rml_mapper.sh ${CONVERSION}/execute_rml_mapper.sh

USER sphn

EXPOSE 8080