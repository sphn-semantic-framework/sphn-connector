#!/bin/sh
#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'airflow.sh' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
export AIRFLOW__DATABASE__SQL_ALCHEMY_CONN="postgresql+psycopg2://${POSTGRES_USER}:${POSTGRES_PASSWORD}@postgres:5432/airflow_postgres"
python /home/sphn/code/utils/generate_config_file.py
airflow db migrate
airflow users create --username "${AIRFLOW_USER}" --firstname "${AIRFLOW_FIRSTNAME}" --lastname "${AIRFLOW_LASTNAME}" --role Admin --password "${AIRFLOW_PASSWORD}" --email "${AIRFLOW_EMAIL}"
airflow webserver --port 8080 & airflow scheduler