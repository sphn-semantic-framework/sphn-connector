#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# This file: 'run_grafana_count.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from datetime import datetime
from airflow.decorators import task, dag
from airflow.models.dagrun import DagRun
from grafana_counts import update_grafana_realtime_count

@dag(schedule="*/5 * * * *",start_date=datetime(2024, 1, 1), description="executes real time count",  render_template_as_native_obj=True, is_paused_upon_creation=True, catchup=False)
def run_grafana_count():
    """
    Defines DAG 'run_grafana_count'.
    """
    @task
    def count_patients_for_grafana(dag_run: DagRun | None = None):
        update_grafana_realtime_count()
    # Set the order of execution:
    count_patients_for_grafana()

run_grafana_count()
