#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'run_database_extraction.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from airflow.decorators import task, dag
from airflow.models.dagrun import DagRun
from datetime import datetime
from database_extractor import DatabaseExtractor

@dag(schedule=None,start_date=datetime(2024, 1, 1), description="extracts data from database",  render_template_as_native_obj=True)
def run_database_extraction():

    @task
    def cleanup_before_extraction(dag_run: DagRun | None = None):
        DatabaseExtractor._cleanup_before_extraction(str(dag_run.conf["project_name"]))

    @task
    def extract_database_data(dag_run: DagRun | None = None):
        DatabaseExtractor._extract_database_data(project_name=str(dag_run.conf["project_name"]), run_uuid=dag_run.conf["run_uuid"])

    @task
    def update_metadata(dag_run: DagRun | None = None):
        DatabaseExtractor._update_database_metadata(str(dag_run.conf["project_name"]))

    cleanup_before_extraction() >> extract_database_data() >> update_metadata()

run_database_extraction()
