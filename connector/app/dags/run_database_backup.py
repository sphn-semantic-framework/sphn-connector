#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# This file: 'run_database_backup.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from datetime import datetime
from airflow.decorators import task, dag
from airflow.models.dagrun import DagRun
from backup_restore import BackupRestore

backup = BackupRestore()

@dag(schedule=None,
     start_date=datetime(2024, 1, 1),
     description="Backup for the SPHN Connector database")
def run_database_backup():
    """ Dag for the database backup task"""

    @task
    def execute_backup_database(dag_run: DagRun | None = None):
        backup.backup_database(api_config=dag_run.conf)

    execute_backup_database()

run_database_backup()
