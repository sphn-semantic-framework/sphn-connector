#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# This file: 'run_whole_pipeline.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from datetime import datetime
from airflow.decorators import dag
from airflow.operators.trigger_dagrun import TriggerDagRunOperator

PROJECT_NAME = "{{dag_run.conf['project_name']}}"
VALIDATION_LOG_LEVEL = "{{dag_run.conf['validation_log_level']}}"
PATIENT_ID = "{{dag_run.conf['patient_id']}}"
RUN_UUID = "{{dag_run.conf['run_uuid']}}"
SEVERITY_LEVEL = "{{dag_run.conf['severity_level']}}"
REVALIDATE = "{{dag_run.conf['revalidate']}}"

@dag(schedule=None,start_date=datetime(2024, 1, 1),
     description="Run all essential SPHN Connector steps sequentially",  render_template_as_native_obj=True)
def run_whole_pipeline():

    trigger_pre_check_and_de_id = TriggerDagRunOperator(
        task_id="execute_pre_check_and_de_identification",
        trigger_dag_id="run_pre_check_and_de_identification",
        wait_for_completion=True,
        conf={"project_name":  PROJECT_NAME, "patient_id": PATIENT_ID, "run_uuid": RUN_UUID},
        poke_interval=1)

    trigger_integration = TriggerDagRunOperator(
        task_id="execute_integration",
        trigger_dag_id="run_integration",
        wait_for_completion=True,
        conf={"project_name":  PROJECT_NAME, "patient_id": PATIENT_ID, "run_uuid": RUN_UUID},
        poke_interval=1)

    trigger_validation = TriggerDagRunOperator(
        task_id="execute_validation",
        trigger_dag_id="run_validation",
        wait_for_completion=True,
        conf={"project_name":  PROJECT_NAME, "validation_log_level": VALIDATION_LOG_LEVEL,
              "patient_id": PATIENT_ID, "run_uuid": RUN_UUID, "severity_level": SEVERITY_LEVEL, "revalidate": REVALIDATE},
        poke_interval=1)
    
    trigger_notify_einstein = TriggerDagRunOperator(
        task_id="excute_notify_einstein",
        trigger_dag_id="run_notify_einstein",
        wait_for_completion=True,
        conf={"project_name":  PROJECT_NAME, "patient_id": PATIENT_ID, "run_uuid": RUN_UUID},
        poke_interval=1)
    
    # Set the order of execution:
    trigger_pre_check_and_de_id >> trigger_integration >> trigger_validation >> trigger_notify_einstein

run_whole_pipeline()
