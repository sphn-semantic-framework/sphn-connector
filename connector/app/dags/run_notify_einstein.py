#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# This file: 'run_notify_einstein.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from datetime import datetime
from airflow.decorators import task, dag
from airflow.models.dagrun import DagRun
from connector import Connector

@dag(schedule=None, start_date=datetime(2024, 1, 1), max_active_runs=1, description="Notifies new data to Einstein tool")
def run_notify_einstein():
    """
    Defines DAG 'run_notify_einstein'.
    """
    @task
    def run_notification_task(dag_run: DagRun | None = None):
        Connector._run_notify_einstein(project_name = str(dag_run.conf["project_name"]), run_uuid = dag_run.conf["run_uuid"], dag_run_config = dag_run.conf)

    run_notification_task()

run_notify_einstein()
