#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# This file: 'run_validation.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

import os
from datetime import datetime
from airflow.decorators import task, dag
from airflow.models.dagrun import DagRun
from connector import Connector
from enums import QCSeverityLevel, Steps, DataPersistenceLayer
from grafana_counts import update_processed_patients_with_history, save_file_sizes

@dag(schedule=None,start_date=datetime(2024, 1, 1),
     description="triggers the validation step of the SPHN Connector",  render_template_as_native_obj=True)
def run_validation():
    """
    Defines DAG 'run_validation'.
    """

    @task
    def initial_data_cleanup(dag_run: DagRun | None = None):
        Connector._cleanup_data(folder = os.path.join('/home/sphn/quality', str(dag_run.conf["project_name"])))

    @task
    def create_folders_structure(dag_run: DagRun | None = None):
        Connector._create_folder_structure(project_name = str(dag_run.conf["project_name"]), step = Steps.VALIDATION)

    @task
    def download_data(dag_run: DagRun | None = None):
        Connector._download_data(project_name = str(dag_run.conf["project_name"]), step = Steps.VALIDATION)

    @task
    def generate_QC_properties_file(dag_run: DagRun | None = None):
        Connector._generate_properties_file(project_name = str(dag_run.conf["project_name"]), validation_log_level = dag_run.conf["validation_log_level"], step = Steps.VALIDATION)

    @task
    def build_tdb2_model(dag_run: DagRun | None = None):
        Connector._build_tdb2_model(project_name = str(dag_run.conf["project_name"]), run_uuid = dag_run.conf["run_uuid"] , validation_log_level = dag_run.conf["validation_log_level"], step = Steps.VALIDATION, dag_run_config = dag_run.conf)
    @task.bash
    def start_count_for_grafana():
        return "airflow dags unpause run_grafana_count"
    @task
    def run_QAF_validation(dag_run: DagRun | None = None):
        Connector._run_validation(project_name = str(dag_run.conf["project_name"]), run_uuid = dag_run.conf["run_uuid"], validation_log_level = dag_run.conf["validation_log_level"], 
                                  severity_level = QCSeverityLevel(dag_run.conf["severity_level"]), dag_run_config = dag_run.conf,
                                  revalidate=dag_run.conf['revalidate'])
    @task.bash
    def stop_count_for_grafana():
        return "airflow dags pause run_grafana_count"
    @task
    def update_final_count_from_history(dag_run: DagRun | None = None):
        update_processed_patients_with_history(project_name = str(dag_run.conf["project_name"]), step = Steps.VALIDATION)
        
    @task
    def update_zone_file_sizes(dag_run: DagRun | None = None):
        save_file_sizes(project_name = str(dag_run.conf["project_name"]), zone = DataPersistenceLayer.RELEASE_ZONE, step = Steps.VALIDATION)
    
    @task
    def final_data_cleanup(dag_run: DagRun | None = None):
        Connector._cleanup_data(folder = os.path.join('/home/sphn/quality', str(dag_run.conf["project_name"])))

    # Set the order of execution:
    initial_data_cleanup() >> create_folders_structure() >> download_data() >> generate_QC_properties_file() >> build_tdb2_model() >> start_count_for_grafana() >> run_QAF_validation() >> stop_count_for_grafana() >> update_final_count_from_history() >> update_zone_file_sizes() >> final_data_cleanup()

run_validation()
