#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# This file: 'run_shacler.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
import os
from datetime import datetime
from airflow.decorators import task, dag
from airflow.models.dagrun import DagRun
from shacler_rules import Shacl

@dag(schedule=None,start_date=datetime(2024, 1, 1), description="Runs the SHACLer during the project creation.",  render_template_as_native_obj=True)
def run_shacler():
    """
    Defines DAG 'run_shacler'.
    """

    @task()
    def initial_data_cleanup(dag_run: DagRun | None = None):
        Shacl._cleanup_data(folder= os.path.join("/home/sphn/shacler", str(dag_run.conf["project_name"])))

    @task()
    def download_shacler_files(dag_run: DagRun | None = None):
        Shacl._download_shacler_files(str(dag_run.conf["project_name"]))

    @task()
    def execute_shacler(dag_run: DagRun | None = None):
        Shacl._generate_shacl(str(dag_run.conf["project_name"]))

    @task()
    def final_data_cleanup(dag_run: DagRun | None = None):
        Shacl._cleanup_data(folder= os.path.join("/home/sphn/shacler", str(dag_run.conf["project_name"])))

    # Set the order of execution:
    initial_data_cleanup() >> download_shacler_files() >> execute_shacler() >> final_data_cleanup()

    
run_shacler()
