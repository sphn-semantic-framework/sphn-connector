#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# This file: 'run_integration.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from datetime import datetime
from airflow.decorators import task, dag
from database import Database
from database import Database
from connector import Connector
from airflow.models.dagrun import DagRun
import os
from enums import Steps, DataPersistenceLayer
from grafana_counts import update_processed_patients_with_history, save_file_sizes


@dag(schedule=None,start_date=datetime(2024, 1, 1), description="executes the SPHN Connector integration phase.",  render_template_as_native_obj=True)
def run_integration():
    """
    Defines DAG 'run_integration'.
    """

    @task
    def initial_data_cleanup(dag_run: DagRun | None = None):
        Connector._cleanup_data(folder = os.path.join("/home/sphn/conversion",
                                str(dag_run.conf["project_name"])))

    @task
    def create_folders_structure(dag_run: DagRun | None = None):
        Connector._create_folder_structure(project_name=str(dag_run.conf["project_name"]),
                                           step = Steps.INTEGRATION)

    @task
    def generate_properties_file(dag_run: DagRun | None = None):
        Connector._generate_rmlmapper_properties_file(project_name = str(dag_run.conf["project_name"]))

    @task
    def download_data(dag_run: DagRun | None = None):
        Connector._download_data(project_name = str(dag_run.conf["project_name"]),
                                 step = Steps.INTEGRATION )
    @task.bash
    def start_count_for_grafana():
        return "airflow dags unpause run_grafana_count"

    @task
    def run_integration_task(dag_run: DagRun | None = None):
        Connector._run_integration(project_name = str(dag_run.conf["project_name"]),
                                    run_uuid= dag_run.conf["run_uuid"],
                                    dag_run_config=dag_run.conf)
    @task.bash
    def stop_count_for_grafana():
        return "airflow dags pause run_grafana_count"
    
    @task
    def update_final_count_from_history(dag_run: DagRun | None = None):
        update_processed_patients_with_history(project_name = str(dag_run.conf["project_name"]), step = Steps.INTEGRATION)
    
    @task
    def update_zone_file_sizes(dag_run: DagRun | None = None):
        save_file_sizes(project_name = str(dag_run.conf["project_name"]), zone = DataPersistenceLayer.GRAPH_ZONE, step = Steps.INTEGRATION)
    
    @task
    def final_data_cleanup(dag_run: DagRun | None = None):
        Connector._cleanup_data(
            folder = os.path.join("/home/sphn/conversion", str(dag_run.conf["project_name"])))
    
    # Set the order of execution:
    initial_data_cleanup() >> create_folders_structure() >>  generate_properties_file() >> download_data() >> start_count_for_grafana() >> run_integration_task() >> stop_count_for_grafana() >> update_final_count_from_history() >> update_zone_file_sizes() >> final_data_cleanup()

run_integration()
