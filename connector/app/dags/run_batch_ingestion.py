#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'run_database_extraction.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from datetime import datetime
from airflow.decorators import task, dag
from airflow.models.dagrun import DagRun
from api import upload_batch_data

@dag(schedule=None,start_date=datetime(2024, 1, 1),
      description="triggers run_batch_ingestion",  render_template_as_native_obj=True)
def run_batch_ingestion():
    """
    Defines DAG 'run_batch_ingestion'.
    """

    @task
    def ingest_batch(dag_run: DagRun | None = None):
        upload_batch_data(project = str(dag_run.conf["project_name"]))
        

    # Set the order of execution:
    ingest_batch()

run_batch_ingestion()
