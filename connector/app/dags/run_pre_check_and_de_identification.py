#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# This file: 'run_pre_check_and_de_identification.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from datetime import datetime
from airflow.decorators import task, dag
from airflow.models.dagrun import DagRun
from connector import Connector
from grafana_counts import save_file_sizes
from enums import DataPersistenceLayer, Steps

@dag(schedule=None,start_date=datetime(2024, 1, 1),
      description="triggers PreCheck/De-ID",  render_template_as_native_obj=True)
def run_pre_check_and_de_identification():
    """
    Defines DAG 'run_pre_check_and_de_identification'.
    """
    @task
    def update_landing_and_refined_file_sizes(dag_run: DagRun | None = None):
        save_file_sizes(project_name = str(dag_run.conf["project_name"]),zone = DataPersistenceLayer.LANDING_ZONE, step=Steps.PRE_CHECK_DE_ID)
    @task
    def run_pre_check_and_de_id(dag_run: DagRun | None = None):
        Connector._run_pre_check_and_de_identification(project_name = str(dag_run.conf["project_name"]),
                                                       run_uuid = dag_run.conf["run_uuid"],
                                                       dag_run_config=dag_run.conf)
    # Set the order of execution:
    update_landing_and_refined_file_sizes() >> run_pre_check_and_de_id()

run_pre_check_and_de_identification()
