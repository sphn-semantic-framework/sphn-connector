#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# This file: 'run_sparqler.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from datetime import datetime
from airflow.decorators import task, dag
from airflow.models.dagrun import DagRun
from sparqler import SparqlGenerator

@dag(schedule=None,start_date=datetime(2024, 1, 1),
      description="triggers SPARQLer to create the statistics queries",  render_template_as_native_obj=True)
def run_sparqler():

    @task
    def cleanup_before_generation(dag_run: DagRun | None = None):
        SparqlGenerator._cleanup_data(project_name=str(dag_run.conf["project_name"]))

    @task
    def download_schemas(dag_run: DagRun | None = None):
        SparqlGenerator._download_schemas(project_name=str(dag_run.conf["project_name"]))

    @task
    def generate_sparql_queries(dag_run: DagRun | None = None):
        SparqlGenerator._generate_sparql_queries(project_name=str(dag_run.conf["project_name"]), sphn_base_iri=dag_run.conf["sphn_base_iri"])

    @task
    def cleanup_after_generation(dag_run: DagRun | None = None):
        SparqlGenerator._cleanup_data(project_name=str(dag_run.conf["project_name"]))

    cleanup_before_generation() >> download_schemas() >>  generate_sparql_queries() >> cleanup_after_generation()

run_sparqler()
