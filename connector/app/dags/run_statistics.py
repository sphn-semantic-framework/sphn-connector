#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# This file: 'run_statistics.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
import os
from datetime import datetime
from airflow.decorators import task, dag
from airflow.models.dagrun import DagRun
from connector import Connector
from enums import Steps
from grafana_counts import update_processed_patients_with_history

@dag(schedule=None,start_date=datetime(2024, 1, 1), description="triggers the execution of the SPARQL statistics scripts",  render_template_as_native_obj=True)
def run_statistics():
    """
    Defines task for the 'run_statistics' DAG.
    """

    @task
    def initial_data_cleanup(dag_run: DagRun | None = None):
        Connector._cleanup_data(folder = os.path.join('/home/sphn/statistics', str(dag_run.conf["project_name"])))

    @task
    def create_folders_structure(dag_run: DagRun | None = None):
        Connector._create_folder_structure(project_name = str(dag_run.conf["project_name"]), step = Steps.STATISTICS)

    @task
    def download_data(dag_run: DagRun | None = None):
        Connector._download_data(project_name = str(dag_run.conf["project_name"]), step = Steps.STATISTICS, profile = dag_run.conf["profile"])

    @task
    def generate_QC_properties_file(dag_run: DagRun | None = None):
        Connector._generate_properties_file(project_name = str(dag_run.conf["project_name"]), validation_log_level = "Compact", step = Steps.STATISTICS)

    @task
    def build_tdb2_model(dag_run: DagRun | None = None):
        Connector._build_tdb2_model(project_name = str(dag_run.conf["project_name"]),validation_log_level="Compact",step =Steps.STATISTICS, run_uuid = dag_run.conf["run_uuid"], dag_run_config = dag_run.conf)

    @task.bash
    def start_count_for_grafana():
        return "airflow dags unpause run_grafana_count"
    
    @task
    def run_statistics_task(dag_run: DagRun | None = None):
        Connector._run_statistics(project_name = str(dag_run.conf["project_name"]), run_uuid = dag_run.conf["run_uuid"], profile = dag_run.conf["profile"])
    
    @task.bash
    def stop_count_for_grafana():
        return "airflow dags pause run_grafana_count"
    @task
    def update_final_count_from_history(dag_run: DagRun | None = None):
        update_processed_patients_with_history(project_name = str(dag_run.conf["project_name"]), step = Steps.STATISTICS)
    
    @task
    def final_data_cleanup(dag_run: DagRun | None = None):
        Connector._cleanup_data(folder = os.path.join('/home/sphn/statistics', str(dag_run.conf["project_name"])))

    initial_data_cleanup() >> create_folders_structure() >> download_data() >> generate_QC_properties_file() >> build_tdb2_model() >> start_count_for_grafana() >> run_statistics_task() >> stop_count_for_grafana() >> update_final_count_from_history() >> final_data_cleanup()

run_statistics()
