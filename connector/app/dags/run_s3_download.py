#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'run_s3_download.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from datetime import datetime
from airflow.decorators import task, dag
from airflow.models.dagrun import DagRun
from database import Database

@dag(schedule=None,start_date=datetime(2024, 1, 1), description="triggers the S3 download")
def run_s3_download():
    """
    Defines DAG 'run_s3_download'.
    """

    @task
    def download_bundle(dag_run: DagRun | None = None):
        Database._trigger_s3_download(project_name = str(dag_run.conf["project_name"]),
                                         compression_type=dag_run.conf["compression_type"],
                                         skip_downloaded = dag_run.conf["skip_downloaded"],
                                         patients_only=dag_run.conf["patients_only"],
                                         dag_run_config= dag_run.conf)

    # Set the order of execution:
    download_bundle()

run_s3_download()
