#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'minio/Dockerfile' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

# Base image: minio/minio:RELEASE.2025-01-20T14-49-07Z
FROM registry.dcc.sib.swiss/sphn-semantic-framework/sphn-connector/minio/minio:01.2025

# Create a group and user
RUN echo "sphn:x:1000:" >> /etc/group && \
    echo "sphn:x:1000:1000:/bin/sh" >> /etc/passwd

WORKDIR /data
COPY minio/entrypoint.sh /usr/local/bin/entrypoint.sh

# give the sphn user the rights to the working directory
RUN chown -R sphn:sphn /data \
    && chmod -R 755 /data \
    && mkdir -p /shared/logs \
    && chmod 777 /shared/logs \
    && chmod +x /usr/local/bin/entrypoint.sh \
    && chown sphn:sphn /usr/local/bin/entrypoint.sh

# Set the user to run the application
USER sphn

# Entrypoint command
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]