#!/bin/bash
#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'setup.sh' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
# 

ENV_FILE=".env"
TAG_NAME="latest"

if [ -f "$ENV_FILE" ]; then
  source "$ENV_FILE"
else
  echo "ERROR -- Configuration file '$ENV_FILE' does not exist. Please create it (copy sample file .env_sample to .env and update it with your configurations)"
  exit 1
fi

if [ -n "${REGISTRY}" ]; then
  if [[ "${REGISTRY}" != */ ]]; then
    echo "ERROR -- Value of 'REGISTRY' parameter '$REGISTRY' must end with a trailing slash"
    exit 1
  fi
else
  REGISTRY="registry.dcc.sib.swiss/sphn-semantic-framework/sphn-connector/"
fi

# Check if port is occupied
check_for_linux=$(uname)

if [ ${check_for_linux} = "Linux" ]; then
    nginx_port=$(ss -tunlp | grep ":${NGINX_PORT:-1443} ")
    nginx_port_minio_api=$(ss -tunlp | grep ":${NGINX_PORT_MINIO_API:-1444} ")
elif [ ${check_for_linux} = "Darwin" ]; then
    nginx_port=$(netstat -a -n | grep 'LISTEN ' | grep ".${NGINX_PORT:-1443} ")
    nginx_port_minio_api=$(netstat -a -n | grep 'LISTEN ' | grep ".${NGINX_PORT_MINIO_API:-1444} ")
else
    nginx_port=$(netstat -aon | gawk " $2~/:${NGINX_PORT:-1443} / { print $5 } ")
    nginx_port_minio_api=$(netstat -aon | gawk " $2~/:${NGINX_PORT_MINIO_API:-1444} / { print $5 } ")
fi

if [ ! -z "${nginx_port}" ]; then
    echo "ERROR  -- Port '${NGINX_PORT:-1443}' is already occupied. Please free up the port"
    exit 1
fi

if [ ! -z "${nginx_port_minio_api}" ]; then
    echo "ERROR  -- Port '${NGINX_PORT_MINIO_API:-1444}' is already occupied. Please free up the port"
    exit 1
fi

DATA_TRANSFER_DIR="${BASE_PATH:-.}/data-transfer"
DEFAULT_CERT_DIR="${BASE_PATH:-.}/default-certificates"

# Ensure the directory exists
if [ ! -d "$DATA_TRANSFER_DIR" ]; then
    echo "ERROR -- Directory $DATA_TRANSFER_DIR does not exist."
    exit 1
fi

# Ensure the directory exists
if [ ! -d "$DEFAULT_CERT_DIR" ]; then
    echo "ERROR -- Directory $DEFAULT_CERT_DIR does not exist."
    exit 1
fi

check_and_copy_cert() {
    local cert_file="$1"
    if [ ! -f "$DATA_TRANSFER_DIR/$cert_file" ]; then
        echo "WARNING -- Certificate file $cert_file not found in $DATA_TRANSFER_DIR."
        cp "$DEFAULT_CERT_DIR/$cert_file" "$DATA_TRANSFER_DIR/"
        echo "WARNING -- Default certificate $cert_file copied into $DATA_TRANSFER_DIR."

        if [ $? -ne 0 ]; then
            echo "ERROR -- Failed to copy certificate file $cert_file."
            exit 1
        fi
    fi
}

check_and_copy_cert "sql.cert"
check_and_copy_cert "sql.key"
check_and_copy_cert "reverse-proxy.cert"
check_and_copy_cert "reverse-proxy.key"
check_and_copy_cert "root-certificate.cert"

# Pull setup container image
docker pull ${REGISTRY}sphn-connector_setup:${TAG_NAME}

# Test command for integrating local changes in setup.py
# docker build -t ${REGISTRY}sphn-connector_setup:${TAG_NAME} setup/

# Run the setup.py script inside the Docker container
docker run --name sphn-connector_setup --env-file .env --rm -v ${DATA_TRANSFER_DIR}:/data-transfer ${REGISTRY}sphn-connector_setup:${TAG_NAME} python /setup/setup.py

# Capture the exit status of the previous command
exit_status=$?

# Trigger the appropriate docker-compose command based on the exit status
if [ $exit_status -eq 0 ]; then
    echo ""
    docker logout registry.dcc.sib.swiss
    docker pull ${REGISTRY}sphn-connector_data_handler:${TAG_NAME}
    docker pull ${REGISTRY}sphn-connector_api:${TAG_NAME}
    docker pull ${REGISTRY}sphn-connector_minio:${TAG_NAME}
    docker pull ${REGISTRY}sphn-connector_connector:${TAG_NAME}
    docker pull ${REGISTRY}sphn-connector_postgres:${TAG_NAME}
    docker pull ${REGISTRY}sphn-connector_pgadmin:${TAG_NAME}
    docker pull ${REGISTRY}sphn-connector_reverse-proxy:${TAG_NAME}
    docker pull ${REGISTRY}sphn-connector_grafana:${TAG_NAME}
    echo "INFO  Please check in directory /release-update if there are any necessary steps to execute before updating the release"
else
    exit 1
fi