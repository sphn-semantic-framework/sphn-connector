#!/bin/bash
#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'install-docker-rootless.sh' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

# before starting check user input
installation_successful=true

# feedback function in case of failed installation / deactivation
installation_deactivation_failed() {
    package_or_service="${1}"
    disabled_indication="${2}"
    if [ -z "$disabled_indication" ]; then
        installation_successful=false
        echo "ERROR -- installation of '$package_or_service' failed "  && exit 1
    elif [ "$disabled_indication" = "not disabled" ]; then
        installation_successful=false
        echo "ERROR -- deactivation of '$package_or_service' failed '$disabled_indication' " && exit 1
    else
        installation_successful=false
        echo "ERROR -- invalid parameter '$disabled_indication'" && exit 1
    fi
}

# feedback function in case of successful installation / deactivation
installation_deactivation_successful() {
    package_or_service="${1}"
    disabled_indication="${2}"
    if [ -z "$disabled_indication" ]; then
        echo "INFO -- installation of '$package_or_service' successful"
    elif [ "$disabled_indication" = "disabled" ]; then
        echo "INFO -- deactivation of '$package_or_service' successful '$disabled_indication'"
    else
        installation_successful=false
        echo "ERROR -- invalid parameter '$disabled_indication'" && exit 1
    fi
}

echo "WARNING: This script will install dependencies on your system"
echo to enable a Docker rootless installation
echo "WARNING: Do not execute this script in a productive environment"
echo "before you checked the installation steps"
echo "INFO: This installation has only been tested on Ubuntu"
echo "if you want to continue confirm with 'y' for yes"
echo "if you do not wish to continue respond with 'n' for no"
echo "INFO: sudo privileges are needed for the setup"

# continue only if the user provided positive feedback
while true; do
    read -p "Do you want to continue (y|n)?: " response
    case $response in
        yes|Yes|YES|y|Y)
        echo "setup continues"; break;;
        n|N|No|no|NO)
        echo "setup will be terminated" && exit 1 ;;
        * ) echo "Please answer: Do you want to continue (y|n)?";;
    esac
done

echo "Docker installation starts"

# the installation follows the guide provided by the docker documentation
# for more information please visit
# https://docs.docker.com/engine/install/ubuntu/

# update package index
echo update package index
if  sudo apt-get update -y; then
    installation_deactivation_successful "apt-get update"
else 
    installation_deactivation_failed "apt-get update"
fi

# in case an older installation is found it will be removed
# safeguarding for the installation has been removed since the script 
# would fail if it can't find one of the specified packages to remove
echo remove possible older installation 
sudo apt-get remove docker docker-engine docker.io containerd runc

# install dependencies
echo install dependencies
if sudo apt-get install ca-certificates curl gnupg; then
    installation_deactivation_successful "docker dependencies (ca-certificates curl gnupg)"
else 
    installation_deactivation_failed "docker dependencies (ca-certificates curl gnupg)"
fi

# add Dockers official GPG key
if sudo install -m 0755 -d /etc/apt/keyrings  \
    && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg \
    && sudo chmod a+r /etc/apt/keyrings/docker.gpg; then 
    installation_deactivation_successful "dockers GPG key"
else 
    installation_deactivation_failed "dockers GPG key"
fi

# setup repository
if echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null; then
    installation_deactivation_successful "repo setup"
else 
    installation_deactivation_failed "repo setup"
fi

# Select version to install
# depending on the setup the user will have multiple version to chose from.
if sudo apt-get update && apt-cache madison docker-ce | awk '{ print $3 }';then
    echo "Select Version to install":
    while true; do 
        read -p "Which version do you want to install " docker
        if [ -z $docker ]; then
            echo "Please set version string"
        else
            VERSION_STRING=$docker
            echo "docker version '$VERSION_STRING' will be installed";
            break
        fi
    done
else
    echo failed to extract docker version && exit 1 
fi
        

# the installation instruction is directly copied from the docker documentation:
# https://docs.docker.com/engine/install/ubuntu/

echo "install docker.io and docker-compose-plugin docker-buildx-plugin docker-compose-plugin in version '$VERSION_STRING'"
if  sudo apt-get install docker-ce=$VERSION_STRING docker-ce-cli=$VERSION_STRING docker-buildx-plugin docker-compose-plugin;  then
    installation_deactivation_successful "docker rootless '$VERSION_STRING'"
else 
    installation_deactivation_failed "docker rootless '$VERSION_STRING'"
fi

# for version 18.04 we need to install 
echo Check Ubuntu version
ubuntu_version=$(lsb_release -a | grep 'Release:' | grep -Eo '^[0-9]{1,2}')
# since ubuntu version 19.04 is available as a package
version='19'

if [ "$((ubuntu_version))" -le "$((version))" ]; then
    echo installing  slirp4netns
    (cd /usr/local/bin && sudo curl -o slirp4netns --fail -L https://github.com/rootless-containers/slirp4netns/releases/download/v1.1.12/slirp4netns-$(uname -m))
    if sudo chmod +x /usr/local/bin/slirp4netns && sudo apt-get install selinux-utils ; then 
        installation_deactivation_successful "installation of slirp4netns for ubuntu '$ubuntu_version'"
    else
        installation_deactivation_failed "installation of slirp4netns for ubuntu '$ubuntu_version'"
    fi
else
    echo "no additional installs necessary (slirp4netns & selinux-utils)"
fi


echo "Docker rootless installation starts"

# installation of rootless docker
# for more information have a look at:
# https://docs.docker.com/engine/security/rootless/


echo install uidmap and dbuser-user-session:
if  sudo apt-get install -y uidmap dbus-user-session ; then 
    installation_deactivation_successful "uidmap"
else
    installation_deactivation_failed "uidmap"
fi

echo disable docker:
if  sudo systemctl disable --now docker.service docker.socket ; then
    installation_deactivation_successful "docker" "disabled"
else 
    installation_deactivation_failed "docker" "not disabled"
fi

# the rootless installation installation is can be installed via the dockerd-rootless-setuptool.sh 
# provided by docker
echo install docker rootless:
if  dockerd-rootless-setuptool.sh install; then
    installation_deactivation_successful "docker rootless"
else 
    echo check your docker installation
    installation_deactivation_failed "docker rootless"
fi

echo "Export of env_vars PATH & DOCKER_HOST"
echo "Please double check if these are added to your ENV"
user_name=$(echo $USER)
export PATH=/home/"$user_name"/bin:$PATH
export DOCKER_HOST=unix:///run/user/1000/docker.sock

if [ "$installation_successful" = false ] ; then
    echo 'ERROR -- Installation of Docker rootless for the SPHN connector failed'
    exit 1
else
    echo 'INFO -- Installation ended'
    echo "INFO -- to start docker rootless execute: systemctl --user start docker "
fi
