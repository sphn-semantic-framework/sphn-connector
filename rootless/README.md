# Rootless installation remarks

If you want to use the SPHN Connector in a rootless mode please take into account the remarks below. For **Docker-rootless** installation you can either use the pre-build images [docker-compose.yaml](./docker-compose.yaml) or build them locally with [docker-compose-build.yaml](./docker-compose-build.yaml).
Note: SPHN Connector for rootless installations will run by default on higher ports (check the compose files for more details).

## **Docker rootless**

The installation script assumes a "clean" environment. So at first Docker an its dependencies will be installed. In case you want to install the docker rootless env in an already existing & used environment please review & verify the commands first. In that case it is probably safer to install the dependencies manually than with the provided script. The installation start at line 57.
