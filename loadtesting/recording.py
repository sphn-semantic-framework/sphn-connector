""" 
Module to execute all necessary SPHN Connector steps (project creation, data ingestion).
Afterwards evaluation pipelines are executed. Includes activation of resource monitoring
"""

#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# This file: 'recording.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from datetime import datetime
from threading import Thread, Event
import logging
import time
from loadtesting_lib.machine_analyzer import monitor_system
import pytz
import plac

from loadtesting_lib.insert import (
    create_project,
    ingest_bulk,
    dag_run_batch_ingestion,
    start_pipeline,
    upload_external_files,
    wait_for_project,
    call_delete_project,
    _create_machine_stats,
    write_to_postgres,
    write_env_vars,
    add_run_uuid,
    dag_status
)


LOCAL = pytz.timezone("Europe/Zurich")
TIME = datetime.now(tz=LOCAL)
TIME_OF_EXECUTION = TIME.strftime("%Y_%m_%d-%H%M")
file_name = f"{TIME_OF_EXECUTION}"


@plac.pos(arg="project_name", help="Name of the project", type=str)
@plac.flg(
    arg="project_schema", help="flag for including project schema file", abbrev="pro"
)
@plac.flg(arg="exception", help="flag for including exception files", abbrev="ecpt")
@plac.flg(
    arg="external_terminologies",
    help="flag for including terminology files",
    abbrev="ext",
)
@plac.flg(
    arg="pre_check", help="flag for including custom pre-checks files", abbrev="pre"
)
@plac.flg(
    arg="delete_project", help="flag for deleting project after run", abbrev="del"
)
def ingest(
    project_name: str,
    project_schema: bool = False,
    exception: bool = False,
    external_terminologies: bool = False,
    pre_checks: bool = False,
    delete_project: bool = False,
    filename=file_name,
) -> None:
    """
    ingest predefined (mock) patients into the sphn-connector

    ATTENTION
    files like external Terminologies, project specific schema etc. are by default excluded.
    To include them set the specific flag.

    example module call:
    python recording.py sphn-setup -ext -ecpt

    This call start the ingestion of json mock-patients into the sphn-connector
    The project will be called "sphn-setup"                                             [sphn-setup]
    The external terminologies placed in the folder external-terminologies will be used [-ext]
    The external terminologies placed in the folder external-terminologies will be used [-ecpt]

    Args:
        project_name (str): name of the project
        external (bool, optional): if external files should be uploaded. Defaults to False.
        exception (bool, optional): if exception files should be uploaded. Defaults to False.
    """
    event = Event()
    background_thread = Thread(
        target=monitor_system, args=[project_name, filename, event]
    )
    background_thread.start()

    # * [step 1]: setup up logging
    timestmp = datetime.now()
    timestmp_formatted = timestmp.strftime("%Y_%m_%d-%H%M")
    logging.basicConfig(
        filename=f"loadtesting/logfiles/{timestmp_formatted}_{project_name}.log",
        format="%(asctime)s %(levelname)-8s %(message)s",
        level=logging.INFO,
        datefmt="%Y-%m-%d %H:%M:%S",
    )
    logging.info("Mock Data Workflow started")
    logging.info("started at %s", timestmp.strftime("%Y-%m-%d %H:%M:%S"))
    logging.info("Start Project setup")

    # * [step 3]: Create the project and upload external files (if flag has been set)
    create_project(
        project_name=project_name,
        project_schema=project_schema,
        exception_file=exception,
    )
    upload_external_files(
        project_name=project_name,
        external_terminologies=external_terminologies,
        pre_checks=pre_checks,
    )

    # * [step 4]: gather meta data about the files and upload them in a bulk fashion
    logging.info("ingestion for project: %s started", project_name.upper())
    ingest_bulk(directory=project_name, project=project_name)

    # * [step 5]: wait for the bulk ingestion to finish
    dag_run_batch_ingestion(project=project_name)
    ingestion = dag_status("run_batch_ingestion")
    wait_for_project(
        start_response=ingestion,
        project_name=project_name,
        start_time=timestmp,
        dag="run_batch_ingestion"
    )
    # * [step 6]: start "run_whole_pipeline"
    # * so the pre_check, integration and validation step
    process_start = start_pipeline(project=project_name)
    logging.info("pipeline for project: %s started", project_name.upper())
    time.sleep(30)

    # * [step 7]: wait for the execution of the "run_whole_pipeline" dag and record some metrics
    wait_for_project(
        start_response=process_start["run_uuid"],
        project_name=project_name,
        start_time=timestmp,
        dag="run_whole_pipeline"
    )

    # * [step 8]: stop machine metrics recording
    event.set()

    # * [step 9]: upload report to postgres db
    write_to_postgres(filenname=f"loadtesting/system_stats/{filename}_{project_name}.csv")
    env_data = _create_machine_stats(
        project_name=project_name, uuid=process_start["run_uuid"]
    )
    write_env_vars(env_data, project=project_name, timestamp=TIME)
    add_run_uuid(project=project_name, uuid=process_start["run_uuid"])

    # * [step 10]: Delete Files
    
    call_delete_project(project=project_name, remove_project=delete_project)


if __name__ == "__main__":
    plac.call(ingest)
