""" ENUMS used in the loadtesting script """
#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# This file: 'loadtesting_enums.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from enum import Enum, auto


class FileClass(Enum):
    """
    Enum class defining the class of the file (i.e. what the file represents)
    """

    PATIENT_DATA = "Patient Data"
    SCHEMA = "Schema"
    EXT_SCHEMA = "External Schema"
    EXT_TERMINOLOGY = "External Terminology"
    EXT_SHACL = "External SHACL"
    EXT_QUERIES = "External Queries"
    EXCEPTIONS = "Exception File"
    RML = "RML"
    PRE_CHECK_CONFIG = "Pre-Check Config"
    DE_IDENTIFICATION_CONFIG = "De-Identification Config"
    REPORT_SPARQL_QUERY = "Report SPARQL Query"


class FileType(Enum):
    """
    Enum class defining the file type
    """

    RDF = "Rdf"
    JSON = "Json"
    ZIP = "Zip"


class Endpoints(Enum):
    """
    Enum class for inter Container networking
    """

    API_URL = "http://api:8000"
    AIRFLOW_URL = "http://connector:8080/airflow"


class PGConnection(Enum):
    """
    Enum class for connection string definition
    """

    POSTGRES = auto()
    GRAFANA = auto()


class AirflowDags(Enum):
    """ "
    Enum class for defined Airflow Dags
    """

    DATABASE_EXTRACTION = "run_database_extraction"
    INTEGRATION = "run_integration"
    PRE_CHECK = "run_pre_check_and_de_identification"
    SHACLER = "run_shacler"
    SPARQLER = "run_sparqler"
    STATISTICS = "run_statistics"
    VALIDATION = "run_validation"
    WHOLE_PIPELINE = "run_whole_pipeline"


class ConnectorEndpoints(Enum):
    """
    Enum class for the Connector Endpoints
    """

    CREATE_PROJECT = "Create_project"
    UPLOAD_EXTERNAL_FILES = "Upload_external_files"


class ProjectFilePaths(Enum):
    """
    Enum class for project files
    """

    SPHN_SCHEMA = "loadtesting/project/sphn-schema"
    EXCEPTION_FILE = "loadtesting/project/exceptions"
    EXTERNAL_TERMINOLOGIES = "loadtesting/project/external-terminologies"
    PRE_CHECKS_PATH = "loadtesting/project/pre-checks"
    PROJECT_SPECIFIC_SCHEMA = "loadtesting/project/project-schema"