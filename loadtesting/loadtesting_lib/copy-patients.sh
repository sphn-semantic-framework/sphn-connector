#!/bin/sh

#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'copy-patients.sh' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#


# example call
# sh lib/copy-patients.sh 250 mock_patient_files/sphn-testrun  mock_patient_files/12mb.json
# mock_patient_files/sphn-testrun   directory where the copied patients should be placed
# mock_patient_files/12mb.json      location of the file to be copied

n_patients=$1;
folder=$2;
patient_file_name=$3;


for f in $(eval echo "{1..$n_patients}")
do
    cp $patient_file_name "$folder/mock-patient-$f".json
done