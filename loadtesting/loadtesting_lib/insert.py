""" basic functionality for creation of and monitoring projects within the sphn connector """

#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# This file: 'insert.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

import json
import logging
import os
import time
from datetime import timedelta
from datetime import datetime
import sys
from .time_decorator import timer
import requests
import urllib3
import pytz
import calendar
from typing import Sequence
from uuid import UUID
import psutil
from .loadtesting_enums import (
    FileClass,
    ProjectFilePaths,
    ConnectorEndpoints,
    Endpoints,
    AirflowDags,
)
from enum import Enum
from csv import DictReader
import psycopg2
from psycopg2.extensions import AsIs
sys.path.insert(1, '/home/sphn/code/api/app/')
from config import Config

config = Config()

logging.getLogger().setLevel(logging.INFO)
urllib3.disable_warnings()

authentication_airflow = (config.airflow_user, config.airflow_password)


def get_response():
    """
    Response of a request with user credentials.

    Returns:
        Response
    """

    return requests.post(
        url=f"{Endpoints.API_URL.value}/token",
        data={
            "username": config.api_user,
            "password": config.api_password,
        },
        verify=False,
        allow_redirects=False,
        timeout=(15, None),
    )


def get_header():
    """
    Header information including access token.
    Returns:
        dict: authorization information
    """
    response = get_response()
    token = json.loads(response.text)
    header = {"Authorization": "Bearer " + token["access_token"]}
    return header


@timer
def create_project(
    project_name: str, project_schema: bool = False, exception_file: bool = False
):
    """
    create_project creates a project inside of the SPHN Connector

    Args:
        project_name (str): name of the project
        process_id (int): process id for tracking docker stats
        project_schema (bool, optional): boolean to include project specific schema. Defaults to False.
        exception_file (bool, optional): boolean to include exception file. Defaults to False.
    """

    sphn_schema = _load_init_files(file_location=ProjectFilePaths.SPHN_SCHEMA)

    if project_schema:
        external_schema = _load_init_files(
            file_location=ProjectFilePaths.PROJECT_SPECIFIC_SCHEMA
        )

    if exception_file:
        exception = _load_init_files(file_location=ProjectFilePaths.EXCEPTION_FILE)

    create_project_endpoint = (
        f"{Endpoints.API_URL.value}/{ConnectorEndpoints.CREATE_PROJECT.value}"
    )

    if project_schema and exception_file:
        response = requests.post(
            create_project_endpoint,
            files=[sphn_schema[1], external_schema[1], exception[1]],
            params={"project": project_name, "purge_before_upload": True},
            headers=get_header(),
            verify=False,
            timeout=(15, None),
        )
        _code_check(
            response=response, file_type="All schema files", project_name=project_name
        )
        logging.info("%s: %s", project_name.upper(), response.text)
        logging.info(
            "files uploaded for %s: '%s', '%s', '%s' ",
            project_name.upper(),
            sphn_schema[0],
            external_schema[0],
            exception[0],
        )

    elif project_schema and not exception_file:
        response = requests.post(
            create_project_endpoint,
            files=[sphn_schema[1], external_schema[1]],
            params={"project": project_name, "purge_before_upload": True},
            headers=get_header(),
            verify=False,
            timeout=(15, None),
        )
        _code_check(
            response=response, file_type="project_schema", project_name=project_name
        )
        logging.info("%s: %s", project_name.upper(), response.text)
        logging.info(
            "files uploaded for %s: '%s', '%s' ",
            project_name.upper(),
            sphn_schema[0],
            external_schema[0],
        )

    elif exception_file and not project_schema:
        response = requests.post(
            create_project_endpoint,
            files=[sphn_schema[1], exception[1]],
            params={"project": project_name, "purge_before_upload": True},
            headers=get_header(),
            verify=False,
            timeout=(15, None),
        )
        _code_check(response=response, file_type="exception", project_name=project_name)
        logging.info("%s: %s", project_name.upper(), response.text)
        logging.info(
            "files uploaded for %s: '%s', '%s' ",
            project_name.upper(),
            sphn_schema[0],
            exception[0],
        )

    else:
        response = requests.post(
            create_project_endpoint,
            files=[sphn_schema[1]],
            params={"project": project_name, "purge_before_upload": True},
            headers=get_header(),
            verify=False,
            timeout=(15, None),
        )
        _code_check(
            response=response,
            file_type="minimal setup (SPHN schema only)",
            project_name=project_name,
        )
        logging.info("%s: %s", project_name.upper(), response.text)
        logging.info(
            "files uploaded for %s: '%s'", project_name.upper(), sphn_schema[0]
        )


def _load_external_files(file_location: Enum):
    """
    _load_external_files loads external terminology files and prepares them for the post request.
    iterates of the directory in which external terminology files are stored.

    Args:
        file_location (Enum): Enum for external file location

    Returns:
        list of tuples: list of tuples that include all the terminology files for upload
    """
    files = os.listdir(file_location.value)

    if len(files) == 0:
        logging.info("ERROR: no files found please review settings or project setup")
        sys.exit(1)

    upload_files = [
        ("files", (file, open(os.path.join(file_location.value, file), "rb")))
        for file in files
    ]
    return upload_files


@timer
def upload_external_files(
    project_name: str,
    external_terminologies: bool = False,
    pre_checks: bool = False,
) -> None:
    """
    upload_external_files makes post request to upload terminologies and or pre checks in the SPHN connector.

    Args:
        project_name (str): name of the project
        process_id (int): process id of the docker stats process
        external_terminologies (bool, optional): boolean to include external terminologies. Defaults to False.
        pre_checks (bool, optional): boolean to include pre-check file. Defaults to False.
    """

    ingest_external_url = f"{Endpoints.API_URL.value}/Upload_external_files"
    if external_terminologies:
        external_terminology_files = _load_external_files(
            ProjectFilePaths.EXTERNAL_TERMINOLOGIES
        )
        request = requests.post(
            ingest_external_url,
            params={
                "project": project_name,
                "files_type": FileClass.EXT_TERMINOLOGY.value,
                "purge_before_upload": True,
            },
            files=external_terminology_files,
            timeout=(600, None),
            headers=get_header(),
            verify=False,
        )
        _code_check(
            response=request,
            project_name=project_name,
            file_type=FileClass.EXT_TERMINOLOGY.value,
        )
    if pre_checks:
        file_precheck = _load_init_files(file_location=ProjectFilePaths.PRE_CHECKS_PATH)
        request = requests.post(
            ingest_external_url,
            params={
                "project": project_name,
                "files_type": FileClass.PRE_CHECK_CONFIG.value,
                "purge_before_upload": True,
            },
            files=[("files", file_precheck[1])],
            headers=get_header(),
            verify=False,
            timeout=(15, None),
        )
        _code_check(
            response=request,
            project_name=project_name,
            file_type=FileClass.PRE_CHECK_CONFIG.value,
        )
    else:
        logging.info(
            "No external files selected for upload for project: %s",
            project_name.upper(),
        )


def _load_init_files(file_location: Enum):
    """
    _load_init_files load necessary files to initialize a project inside of the SPHN Connector

    Args:
        file_location (Enum): Enum value for file location

    Returns:
        tuple: tuple consisting of (filename, (file type, file))
    """

    file = os.listdir(file_location.value)
    if len(file) > 1:
        logging.info("ERROR: too many files provided. Review project setup")
        sys.exit(1)
    try:
        path = os.path.join(file_location.value, file[0])
    except IndexError as excp:
        logging.info("%s: please revisited settings or project setup", excp)
        sys.exit(1)

    return (file[0], (file_location.name.lower(), open(path, mode="rb")))


def start_pipeline(project: str):
    """
    calls the 'Start_Pipeline endpoint of the connector

    Args:
        project (str): name of the project

    Returns:
        response: json representation of response
    """

    endpoint = f"{Endpoints.API_URL.value}/Start_pipeline"
    resp = requests.post(
        endpoint,
        params={"project": project},
        headers=get_header(),
        timeout=(30, None),
        verify=False,
    )
    logging.info("%s", resp.text)
    return resp.json()


def check_pipeline_status(project_name: str, uuid: UUID, dag: str):
    """
    checks the status for a given dag

    Args:
        project_name (str): name of the project
        uuid (_type_): id that belongs to a specific pipeline run

    Returns:
        tuple: status, start date and end date of dagrun
    """
    session = requests.Session()
    session.auth = authentication_airflow
    endpoint = f"{Endpoints.AIRFLOW_URL.value}/api/v1/dags/{dag}/dagRuns"
    response = session.get(endpoint, verify=False)
    result = response.json()
    if dag == "run_whole_pipeline":
        for i in range(len(result["dag_runs"])):
            if (
                result["dag_runs"][i]["conf"]["run_uuid"] == uuid
                and result["dag_runs"][i]["conf"]["project_name"] == project_name
            ):
                status = result["dag_runs"][i]["state"].upper()
                start_date = result["dag_runs"][i]["start_date"]
                end_date = result["dag_runs"][i]["end_date"]

                if start_date:
                    start_date = datetime.strptime(start_date, "%Y-%m-%dT%H:%M:%S.%f%z")
                if end_date:
                    end_date = datetime.strptime(end_date, "%Y-%m-%dT%H:%M:%S.%f%z")

                return status, start_date, end_date
    elif dag == "run_batch_ingestion":
        for i in range(len(result["dag_runs"])):
            if (
                result["dag_runs"][i]["dag_run_id"] == uuid["dag_runs"][i]["dag_run_id"]
                and result["dag_runs"][i]["conf"]["project_name"] == project_name
            ):
                status = result["dag_runs"][i]["state"].upper()
                start_date = result["dag_runs"][i]["start_date"]
                end_date = result["dag_runs"][i]["end_date"]

                if start_date:
                    start_date = datetime.strptime(start_date, "%Y-%m-%dT%H:%M:%S.%f%z")
                if end_date:
                    end_date = datetime.strptime(end_date, "%Y-%m-%dT%H:%M:%S.%f%z")

                return status, start_date, end_date


def wait_for_project(start_response, project_name: str, start_time, dag:str):
    """
    Waits for the pipeline to succeed / fail before its stops the recording of the data

    Args:
        start_response (uuid): uuid from the pipeline that should be monitored
        project_name (str): name of the project
        process_id (int): process id of the process that extracts the docker stats info
    """

    status = check_pipeline_status(project_name=project_name, uuid=start_response, dag=dag)[0]

    while status is not None and status not in ["SUCCESS", "FAILED"]:
        time.sleep(120)
        status = check_pipeline_status(project_name=project_name, uuid=start_response, dag=dag)[
            0
        ]
        if status in ["SUCCESS", "FAILED"]:
            break
    logging.info("project %s ended with status: %s", project_name.upper(), status)
    end = datetime.now()
    duration = end - start_time
    logging.info(
        "Process ended at %s, duration: %s",
        end.strftime("%Y-%m-%d %H:%M:%S"),
        datetime.strptime(str(duration).split(".", maxsplit=1)[0], "%H:%M:%S").time(),
    )
    time.sleep(10)


@timer
def ingest_bulk(directory: str, project: str) -> None:
    """
    Sends posts request to the connector endpoint 'Ingest_json' to ingest json data

    data is ingested in bulk (batch_ingestion is set to true)
    you will need to trigger the batch_ingestion endpoint to process the data further.
    @timer decorator monitors the execution time of the function

    Args:
        directory (str): directory with patient data in json format
        project (str): name of the project
    """

    ingest_json_url = f"{Endpoints.API_URL.value}/Ingest_json"
    files = os.listdir(f"loadtesting/mock_patient_files/{directory}")

    for patient in files:
        # get upload file
        upload_file = {
            "file": open(
                f"loadtesting/mock_patient_files/{directory}/{patient}", "rb"
            )
        }

        # extract number from filename
        splitted = patient.split("-")
        pid = splitted[-1][0:-5]

        # send post request
        requests.post(
            ingest_json_url,
            files=upload_file,
            params={"project": project, "patient_id": pid, "batch_ingestion": True},
            headers=get_header(),
            verify=False,
            timeout=(15, None),
        )


def call_delete_project(project: str, remove_project: bool = False):
    """
    call_delete_project delete project

    triggers the Airflow DAG 'delete_project'

    Args:
        project (str): name of the project

    Returns:
        response: response of the endpoint
    """

    if remove_project:
        endpoint = f"{Endpoints.API_URL.value}/Delete_project"
        resp = requests.delete(
            endpoint,
            params={"project": project},
            headers=get_header(),
            timeout=(30, None),
            verify=False,
        )
        logging.info("%s", resp.text)
        return resp
    else:
        logging.info(
            "project %s is kept and not automatically deleted", project.upper()
        )


def dag_run_batch_ingestion(project: str):
    """
    dag_run_batch_ingestion start batch ingestion

    triggers the Airflow DAG 'Trigger_batch_ingestion'

    Args:
        project (str): name of the project

    Returns:
        response: response of the endpoint
    """

    endpoint = f"{Endpoints.API_URL.value}/Trigger_batch_ingestion"
    resp = requests.post(
        endpoint,
        params={"project": project},
        headers=get_header(),
        timeout=(30, None),
        verify=False,
    )
    logging.info("%s", resp.text)
    return resp


def dag_status(dag:str):
    "checks the status of a given Airflow DAG"

    session = requests.Session()
    session.auth = authentication_airflow
    endpoint = f"{Endpoints.AIRFLOW_URL.value}/api/v1/dags/{dag}/dagRuns"
    response = session.get(endpoint, verify=False)
    result = response.json()

    return result

def _code_check(response, file_type, project_name: str):
    """
    _code_check checks the response of the request made to the connector.
    Terminates in case there is a "not ok" response

    Args:
        response (_type_): response of the connector.
        file_type (_type_): file type uploaded
        project_name (str): name of the project
    """

    if response.status_code != 200:
        logging.info(
            "UPLOAD FAILED: '%s' for project: '%s' status: %s",
            file_type,
            project_name.upper(),
            response.text,
        )
        sys.exit(1)
    else:
        logging.info(
            "Upload '%s' for project: '%s' status: %s",
            file_type,
            project_name.upper(),
            response.text,
        )


def _create_machine_stats(project_name: str, uuid: UUID):
    """
    _create_machine_stats combines runtime for the different DAGS with machine statistics.

    creates a dictionary out of it that is later on added into the respective postgres table.

    Args:
        project_name (str): _description_
        uuid (UUID): UUID of the process to monitor

    Returns:
        dict: key value pair of machine stats and dag execution times for a given project & process
    """

    machine_stats = dict()
    START_POINT, END_POINT, DURATION = 0, 1, 2
    checks = get_execution_times(
        project_name=project_name, uuid=uuid, pipeline=AirflowDags.PRE_CHECK
    )
    integration = get_execution_times(
        project_name=project_name, uuid=uuid, pipeline=AirflowDags.INTEGRATION
    )
    validation = get_execution_times(
        project_name=project_name, uuid=uuid, pipeline=AirflowDags.VALIDATION
    )
    whole_pipeline = get_execution_times(
        project_name=project_name, uuid=uuid, pipeline=AirflowDags.WHOLE_PIPELINE
    )

    machine_stats["uuid"] = str(uuid)
    machine_stats["CPUs"] = psutil.cpu_count()
    machine_stats["RAM_GB"] = int(psutil.virtual_memory().total / 1024 / 1024 / 1024)
    machine_stats["DISK_GB"] = int(psutil.disk_usage("/").total / 1024 / 1024 / 1024)
    machine_stats["PARALLEL_PROJECTS"] = config.parallel_projects
    machine_stats["PROJECT_NAME"] = project_name
    machine_stats.update(
        {
            "start_checks": checks[START_POINT],
            "end_checks": checks[END_POINT],
            "duration_checks": checks[DURATION],
        }
    )
    machine_stats.update(
        {
            "start_integration": integration[START_POINT],
            "end_integration": integration[END_POINT],
            "duration_integration": integration[DURATION],
        }
    )
    machine_stats.update(
        {
            "start_validation": validation[START_POINT],
            "end_validation": validation[END_POINT],
            "duration_validation": validation[DURATION],
        }
    )
    machine_stats.update(
        {
            "start_whole_pipeline": whole_pipeline[START_POINT],
            "end_whole_pipeline": whole_pipeline[END_POINT],
            "duration_whole_pipeline": whole_pipeline[DURATION],
        }
    )
    return machine_stats

#! Functions provides not defaults / safeguarding
def get_execution_times(project_name: str, uuid: UUID, pipeline: Enum):
    """
    get_execution_times calls the Airflow API and extracts information about the pipeline runs
    for a given project and process.

    For a predefined Airflow DAG function will extract the start time , end time and
    will calculate the execution time of the DAG. Microseconds are cut off from the result.

    Args:
        project (str): name of the project
        uuid (UUID): UUID of the process to monitor
        pipeline (Enum): ENUM class of the DAG

    Returns:
        tuple: of three datetimes: Start, End and Duration
    """
    session = requests.Session()
    session.auth = authentication_airflow
    endpoint = f"{Endpoints.AIRFLOW_URL.value}/api/v1/dags/{pipeline.value}/dagRuns"
    response = session.get(endpoint, verify=False)
    result = response.json()

    for i in range(len(result["dag_runs"])):
        if (
            result["dag_runs"][i]["conf"]["run_uuid"] == uuid
            and result["dag_runs"][i]["conf"]["project_name"] == project_name
        ):
            start_date = result["dag_runs"][i]["start_date"]
            end_date = result["dag_runs"][i]["end_date"]

            if start_date:
                start_date = datetime.strptime(start_date, "%Y-%m-%dT%H:%M:%S.%f%z")
                start_utc = start_date.astimezone(pytz.utc)
            if end_date:
                end_date = datetime.strptime(end_date, "%Y-%m-%dT%H:%M:%S.%f%z")
                end_utc = end_date.astimezone(pytz.utc)

            duration_temp = end_utc - start_utc

            duration = duration_temp - timedelta(
                microseconds=duration_temp.microseconds
            )

            return start_utc, end_utc, duration


def _define_column_types(columns: Sequence[str]):
    """
    _define_column_types simplistic function to add column types according to the column names.

    columns with the name 'time' will be of type 'bigint', with the name 'project_name' will be
    of type 'varchar(200)'. Everything else is considered to be of type 'float4'

    Args:
        columns (Sequence[str]): names of the columns that need to be added to the postgres table

    Returns:
        str: SQL style syntax that includes column names and their associated types
    """

    schema = dict()
    for i in columns:
        if i == "time":
            schema[i] = "bigint"
        elif i == "project_name":
            schema[i] = "varchar(200)"
        else:
            schema[i] = "float4"

    column_definitions = ", ".join(f"{key} {schema[key]}" for key in schema.keys())
    column_definitions += ", uuid varchar(300)"
    return column_definitions


def _create_schema_statement(column_definitions: str, table: str) -> str:
    """
    _create_schema_statement combines the column definitions with 'beginning'
    and 'ending' to create a single postgres table according to user input.

    Args:
        column_definitions (str): column names an der types, comma separated
        table (str): name of the table to be created.

    Returns:
        str: SQL statement for table creation.
    """
    sql = f'CREATE TABLE IF NOT EXISTS "public"."{table}" (' + column_definitions + ");"
    return sql


def write_to_postgres(filenname: str) -> None:
    """
    write_to_postgres extracts the machine monitoring and writes results to postgres

    During loadtesting machine statistics are written to a .csv file. Function
    'write_to_postgres' reads the dict in after the run has completed and inserts it into the postgres table.
    Afterwards the .csv file is removed from the container.

    Args:
        filenname (str): name of the file
    """

    conn = psycopg2.connect(
        database="performance",
        user=config.postgres_user,
        password=config.postgres_password,
        host="postgres",
        port=5432,
    )
    conn.autocommit = True
    cursor = conn.cursor()
    with open(file=filenname, encoding="utf-8") as file:
        reader = DictReader(file)
        columns = _define_column_types(columns=reader.fieldnames)
        sql_table = _create_schema_statement(
            column_definitions=columns, table="machine_statistics"
        )
        cursor.execute(sql_table)
        next(file, None)
        cursor.copy_from(
            file=file,
            table="machine_statistics",
            sep=",",
            columns=[i.lower() for i in reader.fieldnames],
        )
    cursor.close()
    try:
        os.remove(path=filenname)
    except OSError as error:
        print(f"File can't be removed: {error}")


def write_env_vars(env_data: dict, project: str, timestamp) -> None:
    """
    write_env_vars creates the necessary tables (if not exists) and adds values for them

    Args:
        env_data (dict): recorded environmental data
        project (str): name of the project
        timestamp (_type_): time of recording
    """
    # connect to the database, create cursor and add project name & time to env dict
    conn = psycopg2.connect(
        database="performance",
        user=config.postgres_user,
        password=config.postgres_password,
        host="postgres",
        port=5432,
    )
    conn.autocommit = True
    cursor = conn.cursor()
    current_time = calendar.timegm(timestamp.utctimetuple())
    env_data.update({"PROJECT_NAME": project, "time": current_time})

    # define types for the columns of table 'environment'
    columns = """PROJECT_NAME varchar(200), CPUs double precision, RAM_GB double precision, DISK_GB double precision,
        PARALLEL_PROJECTS double precision, time bigint, start_checks timestamptz, end_checks timestamptz, duration_checks interval, start_integration timestamptz,
        end_integration timestamptz,duration_integration interval,  start_validation timestamptz, end_validation timestamptz, duration_validation interval,
        start_whole_pipeline timestamptz, end_whole_pipeline timestamptz, duration_whole_pipeline interval, uuid varchar(300) """

    create_statement = _create_schema_statement(
        column_definitions=columns, table="environment"
    )
    cursor.execute(create_statement)

    column_names = env_data.keys()
    column_values = [env_data[key] for key in column_names]
    insert_statement = "INSERT INTO environment (%s) VALUES %s;"
    cursor.execute(
        insert_statement, (AsIs(",".join(column_names)), tuple(column_values))
    )
    cursor.close()


def add_run_uuid(project: str, uuid: UUID):
    """
    adds uuid into postgres database table 'machine_statistics'

    Args:
        env_data (str): recorded environmental data
        project (str): name of the project
    """
    sql = """UPDATE machine_statistics SET uuid=%s WHERE project_name=%s AND uuid is NULL;"""
    conn = psycopg2.connect(
        database="performance",
        user=config.postgres_user,
        password=config.postgres_password,
        host="postgres",
        port=5432,
    )
    conn.autocommit = True
    cursor = conn.cursor()
    cursor.execute(sql, (str(uuid), str(project)))
    cursor.close()
