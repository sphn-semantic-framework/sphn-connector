""" Functions to extract and write out host system statistics"""
#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# This file: 'machine_analyzer.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

import os
import datetime
import time
import csv
import calendar
from threading import Event
import psutil


def _get_cpu_information(interval: float) -> dict[str, float]:
    """
    _get_cpu_information extract CPU statistics from host.

    Returns:
        dict[str, float]: key, value pairs with CPU infos.
    """

    # monitors changes in the interval
    cpu_times_percent, all_cpus, cpus_utilization = (
        psutil.cpu_times_percent(interval=interval),
        psutil.cpu_percent(interval=interval),
        psutil.cpu_percent(interval=interval, percpu=True),
    )
    # create dictionary to map values for the cpus to appropriate keys
    cpu_keys = [f"CPU_{number}" for number in range(1, len(cpus_utilization) + 1)]
    per_cpu = dict(zip(cpu_keys, cpus_utilization))

    cpu_dict = {"All_cpus": all_cpus, "IO_wait": cpu_times_percent.iowait}
    cpu_dict.update(per_cpu)

    return cpu_dict


def _get_memory_information() -> dict[str, float]:
    """
    _get_memory_information extracts memory statistics from host system.

    Returns:
        dict[str, float]: key, value pairs with memory related statistics.
    """

    memory = psutil.virtual_memory()
    swap_memory = psutil.swap_memory()
    memory_used = memory.total - memory.available

    (
        memory_total_gb,
        memory_free_gb,
        memory_available_gb,
        memory_used_gb,
        swap_memory_total_gb,
        swap_memory_free_gb,
        swap_memory_used_gb,
    ) = (
        _convert_bytes_to_gbytes(variable_in_bytes=memory.total),
        _convert_bytes_to_gbytes(variable_in_bytes=memory.free),
        _convert_bytes_to_gbytes(variable_in_bytes=memory.available),
        _convert_bytes_to_gbytes(variable_in_bytes=memory_used),
        _convert_bytes_to_gbytes(variable_in_bytes=swap_memory.total),
        _convert_bytes_to_gbytes(variable_in_bytes=swap_memory.free),
        _convert_bytes_to_gbytes(variable_in_bytes=swap_memory.used),
    )
    return {
        "Memory": memory_total_gb,
        "Memory_free": memory_free_gb,
        "Memory_available": memory_available_gb,
        "Memory_used": memory_used_gb,
        "Swap_memory": swap_memory_total_gb,
        "Swap_memory_free": swap_memory_free_gb,
        "Swap_memory_used": swap_memory_used_gb,
    }


def _get_disk_information() -> dict[str, float]:
    """
    _get_disk_information  extracts disk statistics from host system

    Returns:
        dict[str, float]: key, value pairs with disk related statistics.
    """

    disk_usage = psutil.disk_usage("/")
    disk_io = psutil.disk_io_counters()

    read_bytes = getattr(disk_io, "read_bytes")
    write_bytes = getattr(disk_io, "write_bytes")

    read_gbytes, write_gbytes = _convert_bytes_to_gbytes(
        variable_in_bytes=read_bytes
    ), _convert_bytes_to_gbytes(variable_in_bytes=write_bytes)

    return {
        "Disk_used": disk_usage.percent,
        "Disk_free": 100 - disk_usage.percent,
        "Read_Gbytes": read_gbytes,
        "Write_Gbytes": write_gbytes,
    }


def _convert_bytes_to_gbytes(variable_in_bytes: int) -> float:
    """
    _convert_bytes_to_gbytes converts bytes into GB

    Args:
        variable_in_bytes (int): byte value to be converted

    Returns:
        float: variable in GB
    """

    transformed = variable_in_bytes / 1024**3
    return round(transformed, 3)


def _write_to_file(filename: str, data: dict) -> None:
    """
    _write_to_file writes host system stats to file.

    Args:
        filename (str): name of the csv file
        data (dict): data dictonary with host system statistics.
    """

    file_path = os.path.join("loadtesting/system_stats", filename)

    file_exists = os.path.isfile(file_path)

    with open(file_path, mode="a", encoding="utf-8") as file:
        data_writer = csv.DictWriter(file, delimiter=",", fieldnames=data.keys())
        if not file_exists:
            data_writer.writeheader()
        data_writer.writerow(data)


def monitor_system(project_name: str, prefix: str, event: Event):
    """
    monitor_system watcher function that monitors host system and creates an entry every
    five seconds.

    Collects CPU, Memory, and disk data that will be later on written to predefined .csv file.
    Stays idle for five seconds and creates another entry. Stops when the main program (recording.py)
    Gives feedback that the processing for a given project has stopped.

    Args:
        project_name (str): name of the project
        prefix (str): prefix for filename
        event (Event): Event to stop monitoring.
    """

    starttime = time.monotonic()

    filename = f"{prefix}_{project_name}.csv"

    while True:
        system_time = {
            "time": calendar.timegm(datetime.datetime.utcnow().utctimetuple())
        }
        disks_before = psutil.disk_io_counters()

        cpu_data = _get_cpu_information(interval=0.1)
        memory_data = _get_memory_information()
        disk_data = _get_disk_information()

        system_data = {**system_time, **cpu_data, **memory_data, **disk_data}

        disks_before = psutil.disk_io_counters()
        # we want to ensure that data is evenly recorded every 5s
        time.sleep(5.0 - ((time.monotonic() - starttime) % 5.0))

        disks_after = psutil.disk_io_counters()
        # calculate the amount of data written & read in the last ~ 5s
        disks_read_per_sec = disks_after.read_bytes - disks_before.read_bytes
        disks_write_per_sec = disks_after.write_bytes - disks_before.write_bytes
        system_data.update(
            {
                "Disk_read_per_sec": _convert_bytes_to_gbytes(
                    variable_in_bytes=disks_read_per_sec
                ),
                "Disk_write_per_second": _convert_bytes_to_gbytes(
                    variable_in_bytes=disks_write_per_sec
                ),
                "project_name": project_name,
            }
        )
        _write_to_file(filename=filename, data=system_data)

        if event.is_set():
            print("Monitoring terminates")
            break
