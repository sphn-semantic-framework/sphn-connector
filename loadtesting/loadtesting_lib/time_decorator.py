""" Decorator function to track function execution for logging (loadtesting)"""
#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'time_decorator.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#


from datetime import datetime
import logging


def timer(func):
    """
    timer decorator for function execution monitoring
    Args:
        func (Callable): function to track
    """
    def time_wrapper(*args, **kwargs):
        start_time = datetime.now()
        func(*args, **kwargs)
        end_time = datetime.now()
        duration = end_time - start_time
        logging.info("Execution of function %s took %s",
                     func.__name__, datetime.strptime(str(duration).split(
                         ".", maxsplit=1)[0], '%H:%M:%S').time())
    return time_wrapper
