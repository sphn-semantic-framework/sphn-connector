#! /bin/sh

#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'start-performance-test.sh' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#


# script to trigger the creation of mock patient of different sizes
# and to run the pipelines afterwards

start_mock_project(){
    echo "starting mock project '$1'"
    # copying files to designated folder
    mkdir -p loadtesting/mock_patient_files/$1

    if [[ $3 =~ ^[0-9]+$ ]]; then
        sh loadtesting/loadtesting_lib/copy-patients.sh $3 loadtesting/mock_patient_files/$1 loadtesting/mock_patient_files/mock-patients/$2
        # start recording system and docker starts
        echo "files copied for mock project '$1' starting recording"
        python3 loadtesting/recording.py $1
    else
        sh loadtesting/loadtesting_lib/copy-patients.sh $4 loadtesting/mock_patient_files/$1 loadtesting/mock_patient_files/mock-patients/$2
        # start recording system and docker starts
        echo "files copied for mock project '$1' starting recording"
        python3 loadtesting/recording.py $1 $3
    fi

    # remove not needed project files    
    echo "recording ended: removing files"
    rm -rf mock_patient_files/$1
    rm -rf loadtesting/$2
    
}

# extract files
tar -xvzf loadtesting/mock_patient_files/mock-patients.tar.gz -C loadtesting/mock_patient_files

#list elements in directory
patients=$(ls loadtesting/mock_patient_files/mock-patients)

# extract the filename and use it as a project name inside the SPHN Connector

if [[ $1 =~ ^[0-9]+$ ]]; then
    for patient in $patients
        do
            start_mock_project ${patient%%.*} ${patient} $1
        done
else
    for patient in $patients
    do
        start_mock_project ${patient%%.*} ${patient} $1 $2
    done
fi


rm -rf loadtesting/system_stats/*