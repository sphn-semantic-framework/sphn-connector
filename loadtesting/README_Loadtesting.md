# Loadtesting in the SPHN Connector

This is brief description on how to handle the performance assessment tool of the SPHN Connector.
For more details please review the [User Guide](../../docs/SPHN-Connector_User-Guide.pdf) and the [Requirements & Installation guide](../../docs/SPHN-Connector_Installation-requirements-guide.pdf) of the SPHN Connector.

Before you are able to use the loadtesting capability of the SPHN Connector you need to activate it first.
You can do that by enabling the import and installation steps in the [API DOCKERFILE](../api/Dockerfile) and remove the comments in line 38 up to and including line 43.
Afterwards you need to build the images locally with:
```shell
docker compose -f docker-compose-build.yaml up --build -d
```

After doing so you can move on with the rest of the document.

## Short introduction for the impatient

The following command starts the performance assessment for the SPHN Connector.
Projects are deleted after successful execution.

```shell
docker exec -it api sh /home/sphn/code/loadtesting/start-performance-test.sh -del 50
```

If you omit the "-del" flag the mock projects created for the assessment will not be deleted:

```shell
docker exec -it api sh /home/sphn/code/loadtesting/start-performance-test.sh 50
```

The integer value 50 indicates that 50 mock patients will be created per project.
Any positive integer value is accepted and can be used. For the graphics it is beneficial to use values >= 50.


## Introduction

The performance assessment of the SPHN Connector is an additional service for the infrastructure admin user to determine the optimal settings for the SPHN Connector.
The performance assessment triggers the current SPHN Connector installation with different “loads” (see section Mock patients) and records host machine statistics that are later used for visualizations. This should help the infrastructure admin user to determine if the settings chosen by them can be fine-tuned. This should help to ensure that the performance of the SPHN Connector is in an acceptable range for the end user.


## Workflow

Logically the following steps are executed when the assessment is triggered. (Note: project deletion is not explicitly mentioned). That is fully dependant on the user. A simplified execution flow is presented below:

```mermaid
graph LR;
Start --> extraction["extract mock patient files"]
extraction --> loop["loop over mock data"];
loop["loop over files"] --> copy_files;
copy_files --> create_project;
create_project --> upload_mock_data;
upload_mock_data --> start_recording;
start_recording --> finished_successfully;
finished_successfully --> upload_report;
upload_report --> remove_local_files;
remove_local_files --> dec{all processed?};
dec == No ==> loop;
dec == Yes ==> E[End];
```

## Adding custom data
In the /mock-patients directory in the /data-transfer directory three mock patients have been placed:

- 01-sphn-small.json (approx. 1.1 MB)
- 02-sphn-medium.json (approx. 7.2 MB)
- 03-sphn-big.json (approx. 12 MB)

These are packaged into the *mock-patients.tar.gz* file. If you want to add your own test data for the evaluation follow the following steps:

1) Delete mock-patients.tar.gz* (if it exists)
2) Place your files into the /mock-patients directory
3) Execute setup.sh
4) Start the SPHN Connector as usual

It is advisable to use a Connector instance that is currently not used by other user. Furthermore, it is recommended to delete the *mock-patients.tar.gz* that contains custom data locally as soon as the containers are up and running to avoid confusion later on.
**ATTENTION**: currently you can only provide data in JSON format to the performance assessment.

## Reviewing results

The recorded data is written to Postgres in batches. Once a project has been successfully validated all the data that has been recorded to the run is written to the Postgres instance and available for review in the Grafana dashboard "Performance". In case you used the option to delete the projects after a run you can directly alter the settings and retrigger the run with the same data. In case you opted out on this option you will manually need to delete all the projects created for the performance assessment.
