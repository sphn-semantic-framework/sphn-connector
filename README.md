# <img src="api/app/static/sphn-connector-logo.png" width="10%"> SPHN Connector

[![Releases](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-connector/-/badges/release.svg)](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-connector/-/releases)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

## Overview

The SPHN Connector is a dockerized solution that allows data-providing institutions to build a pipeline that converts their data from relational or JSON sources into graph data based on an RDF schema conforming to the SPHN Framework. The ingested data is converted into RDF and validated to check its conformity with the schema. Optionally, data providers that do not have an in-house de-identification functionality, can activate the module for de-identification in the SPHN Connector.

## Description

The SPHN Connector integrates a variety of tools developed or distributed by the DCC like the [SHACLer](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-shacl-generator/) or the [Quality Assurance Framework](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-quality-check-tool) to simplify the production of high quality data. In the context of SPHN, the SPHN Connector is intended to and can be used by any data provider for an easier creation and validation of data in RDF.

The SPHN Connector is build with flexibility and simplicity in mind. It expects only two elements: The input data is provided on a patient level and the base schema which can be the SPHN RDF Schema and optionally a project-specific RDF Schema.

Almost everything else can be adapted by the user to fit its needs, skills and the working environment. One example is the bandwidth of input data that is supported by the connector. A user can upload JSON files, RDF files or setup a specific database import. The setup of the validation is also user centric. Experienced users can provided their own SHACL file for validation while others can simply use the file that is created by the connector via the SHACLer.

## Installation options

The SPHN Connector offers different installation modes for various environments. These include Docker in a rootfull/rootless mode
Depending on your system you might need to use the specific docker-compose file for that environment:

| Filename                      |      Usage                                            |
|------------------------------ |:-------------------------------------------------------|
| `docker-compose-build.yaml`   | Integrate local changes into the Connector             |
| `docker-compose.yaml`         | Use the Connector in a **Docker rootfull/rootless** environment |

## Quick installation

These steps describe the installation of the latest released SPHN Connector in a development environment on a Linux machine. To install the SPHN Connector in a production environment, please follow the detailed steps in the [SPHN Connector installation guide](docs/SPHN-Connector_Installation-requirements-guide.pdf).

- **Hardware requirements**: Linux on x86/amd64 CPU with a rather modern OS, 16GB RAM, 4 CPUs, 500GB disk.
- **Prerequisites**: internet connection, git, docker (2.3.X or higher), docker-compose plugin (2.15.X or higher).

### Installation steps

1. Clone SPHN Connector locally (replace `$RELEASE_TAG` with the release version you want to install, e.g. `1.5.0`)
```
git clone -b $RELEASE_TAG https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-connector.git
```
2. Copy `.env_sample` to `.env` and configure mandatory properties (detailed description of available parameters can be found [here](.env_sample.md))
```
DATA_PROVIDER_ID=""                 # Definition:  ID of the data provider
API_USER=""                         # Definition:  Admin API user
API_PASSWORD=""                     # Definition:  Admin API password
MINIO_ACCESS_KEY=""                 # Definition:  Access key to the MinIO object storage
MINIO_SECRET_KEY=""                 # Definition:  Secret key for the MinIO object storage
POSTGRES_USER=""                    # Definition:  User for PostgreSQL database (requires 'docker compose down -v' before reset)
POSTGRES_PASSWORD=""                # Definition:  Password for PostgreSQL database (requires 'docker compose down -v' before reset)
POSTGRES_UI_EMAIL=""                # Definition:  Email address for pgAdmin interface (requires 'docker compose down -v' before reset)
POSTGRES_UI_PASSWORD=""             # Definition:  Password for pgAdmin interface (requires 'docker compose down -v' before reset)
AIRFLOW_USER=""                     # Definition:  Airflow username
AIRFLOW_PASSWORD=""                 # Definition:  Airflow password
AIRFLOW_FIRSTNAME=""                # Definition:  Airflow admin first name
AIRFLOW_LASTNAME=""                 # Definition:  Airflow admin last name
AIRFLOW_EMAIL=""                    # Definition:  Airflow admin email
GRAFANA_ADMIN_USER=""               # Definition:  Grafana admin user name. Example: admin
GRAFANA_ADMIN_PASSWORD=""           # Definition:  Password for the Grafana admin user. Minimum no. of characters: 12. Example: bYsK2HSA86uE
GRAFANA_POSTGRES_USER=""            # Definition:  Grafana postgres user with admin rights on the Grafana database but read only rights on all other databases & tables. Additional user for security reasons
GRAFANA_POSTGRES_USER_PASSWORD=""   # Definition:  Password for the Grafana postgres user. Minimum no. of characters: 12. Example: aFBPCiutwmc2
```
**OPTIONAL:** Alter the Entrypoint for the SPHN Connector.
Via the optional parameter `NGINX_PORT` in the `.env` file you can alter the entrypoint for the SPHN Connector. As a default port 1443 is used.
Keep in mind that if you install the Connector via rootless Docker you need to either keep the default port or choose a custom value > 1000.

```
NGINX_PORT="1443"
```
3. Execute `setup.sh` configuration script in the root folder
```
./setup.sh
```
4. Launch SPHN Connector.
For more information about the different modes please review section `Installation options` or the [SPHN Connector installation guide](docs/SPHN-Connector_Installation-requirements-guide.pdf)
```
docker compose up -d
or: 
docker compose -f NAME_OF_THE_COMPOSE_FILE up -d
```

5. Access the SPHN Connector entry page (if not modified, server name will be `localhost`) (if specified use the configured port `NGINX_PORT`)
```
https://{your-server}:1443
```

## Documentation

[Installation guide](docs/SPHN-Connector_Installation-requirements-guide.pdf)

[User guide](docs/SPHN-Connector_User-Guide.pdf)

[Troubleshooting guide](https://docs.google.com/document/d/1ocLUZ8m0QqkN6l8EVssaRit3l7dWoISNidwrROhK_vM/edit#heading=h.berlpb7fz5qo)

Visual user guides: [API endpoints](https://docs.google.com/presentation/d/1LWNsi4rpJopSzCma8dtVo5vcX6sIePmGboUZ82fgHaA/edit#slide=id.p1), [MinIO](https://docs.google.com/presentation/d/1BH9Yxcy6RtyiYFTRFFukh-XbVyZMnpudetI4YxktYM4/edit#slide=id.p1), [pgAdmin](https://docs.google.com/presentation/d/1bnsrqJk8sxH2t-7yodbxqD-Em4l3LhzjHnQrVMasbQw/edit#slide=id.p1), [Airflow](https://docs.google.com/presentation/d/1h3VqNoJUMvGMZBgjxG0SGPZz8yM379PNwHaoGSNNXiA/edit#slide=id.p1), [Grafana](https://docs.google.com/presentation/d/1lBIyh7cmvjIkMCIjScHKrnvLKZ9xmrmjt5kN2EFk5ww/edit?usp=drive_link)

[Readthedocs](https://sphn-semantic-framework.readthedocs.io/en/latest/sphn_framework/sphn_connector.html)

## Contact

For any question or issue related to the SPHN Connector, please contact the support team [fair-data-team@sib.swiss](mailto:fair-data-team@sib.swiss).

## Pipelines

CI/CD pipelines can be monitored [here](https://gitlab.com/biomedit/fair/sphn-connector/-/pipelines?page=1&scope=all&ref=main).

## Releases

Public releases of the SPHN Connector can be found on the [releases page](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-connector/-/releases)

The SPHN Connector software follows semantic versioning, represented as:
```
X.Y.Z
```
- Major version (`X`): Incremented for breaking changes. Upgrading between major versions requires a fresh installation, as no data migration is supported. Existing Docker volumes must be removed before reinstalling.

- Minor version (`Y`): Incremented for changes that may require manual configuration. Manual intervention might be needed to migrate data from the previous version.

- Patch version (`Z`): Incremented for non-breaking changes and bug fixes. Upgrades within the same major and minor version can be performed without data loss or additional configuration.

**Overview of Potential Changes**:
- Breaking changes: These include modifications to the PostgreSQL database schema that cannot be resolved through manual intervention (e.g., adding a non-nullable column, introducing a new table requiring pre-populated data for processing), functional changes that conflict with existing data, alterations to the S3 folder structure, or the introduction of new mandatory files.

- Breaking changes requiring manual intervention: These involve PostgreSQL schema modifications that can be addressed manually (e.g., adding a nullable column, creating a new table that can remain empty, removing a table or column, or adding or dropping a database schema).

- Non-breaking changes: These include bug fixes, the addition of new features that do not conflict with the existing data model, and the introduction of new configuration parameters.

**Important**:
Upgrade procedures have been tested only for consecutive releases. Skipping intermediate releases during the upgrade process is not guaranteed to work.

**Update guidelines**
In the repository folder `release-update/` you can find all the necessary guidelines for the migrations. Especially for minor changes, we provide migration scripts or migration step-by-step guides.

**Recommendation**:
This semantic versioning applies from release 1.7.0 onward. Upgrade procedures for earlier releases are not guaranteed. Before upgrading, back up your PostgreSQL database and S3 data to safeguard against potential data loss.

## License
&copy; Copyright SIB Swiss Institute of Bioinformatics 2025 under [GNU GPL v3.0](https://www.gnu.org/licenses/gpl-3.0). See `LICENSE.txt` and `THIRD-PARTY-MATERIALS.md`.

