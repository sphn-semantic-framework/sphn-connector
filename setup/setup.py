#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'setup.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
# 
import argparse
import os
import tarfile
import platform
import logging
import boto3
from botocore.client import Config as botocoreConfig
import requests
logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s', level=logging.getLevelName('INFO'), datefmt='%Y-%m-%d %H:%M:%S')

def check_load_testing_files(successfully_configured: bool) -> bool:
    """Check load testing files

    Args:
        successfully_configured (bool): keep track of configuration status

    Returns:
        bool: successful check
    """
    if os.path.isfile('/data-transfer/mock-patients.tar.gz'):
        logging.info("Mock patients already zipped: '/data-transfer/mock-patients.tar.gz'. Checking content...")
        if not check_tar_contents():
            successfully_configured = False
    else:
        mock_patient_dir = os.listdir('/data-transfer/mock-patients')
        if not mock_patient_dir:
            logging.info("Mock patient directory is empty")
            successfully_configured = False
        else:
            logging.info("Mock patients zipped: '/data-transfer/mock-patients.tar.gz'")
            with tarfile.open('/data-transfer/mock-patients.tar.gz', 'w:gz') as tar:
                tar.add('/data-transfer/mock-patients', arcname='mock-patients')
    return successfully_configured

def check_integer_value(var_value: str, var_name: str) -> bool:
    """Check that the value is an integer

    Args:
        var_value (str): variable value
        var_name (str): variable name

    Returns:
        bool: value is an integer
    """
    if not var_value.isdigit() or int(var_value) <= 0:
        logging.error(f"Set valid value for variable '{var_name}' in .env file. Only positive integers are allowed")
        return False
    return True

def check_length(input_var: str, min_length: int, var_name: str) -> bool:
    """Check length of value

    Args:
        input_var (str): variable value
        min_length (int): minimum length expected
        var_name (str): variable name

    Returns:
        bool: value is longer than minimum length
    """
    if len(input_var) < min_length:
        logging.error(f"Variable '{var_name}' must have at least {min_length} characters")
        return False
    return True

def check_credentials_for_invalid_chars(var_value: str, var_name: str) -> bool:
    """Check credentials for invalid characters

    Args:
        var_value (str): variable value
        var_name (str): variable name

    Returns:
        bool: value does not have invalid characters
    """
    invalid_chars = "\"/'!#@\\$%"
    for char in invalid_chars:
        if char in var_value:
            logging.error(f"Variable '{var_name}' contains an invalid character: '{char}'")
            return False
    return True

def check_reserved_names(var_value: str, var_name: str) -> bool:
    """Check if value uses reserved names

    Args:
        var_value (str): variable value
        var_name (str): variable name

    Returns:
        bool: value does not use reserved names
    """
    if var_value.lower().startswith('pg_'):
        logging.error(f"Variable '{var_name}' cannot start with 'pg_' as these are reserved words")
        return False
    if var_value.lower() == 'airflow_postgres':
        logging.error(f"Variable '{var_name}' cannot be used as this is a reserved word")
        return False
    return True

def check_email_for_invalid_chars(var_value: str, var_name: str) -> bool:
    """Check email for invalid characters

    Args:
        var_value (str): variable value
        var_name (str): variable name

    Returns:
        bool: email does not contain invalid characters
    """
    invalid_chars = "/'!#\\$%"
    for char in invalid_chars:
        if char in var_value:
            logging.error(f"Variable '{var_name}' contains an invalid character: '{char}'")
            return False
    return True

def check_for_on_or_off(var_value: str, var_name: str) -> bool:
    """Check that the value is either 'on' or 'off'

    Args:
        var_value (str): variable value
        var_name (str): variable name

    Returns:
        bool: value in ['on', 'off']
    """
    var_value_uppercase = var_value.upper()
    if var_value_uppercase not in ["ON", "OFF"]:
        logging.error(f"Invalid '{var_name}' setting: '{var_value}' only 'ON' or 'OFF' allowed")
        return False
    return True

def check_boolean(var_value: str, var_name: str) -> bool:
    """Check that the value is a boolean

    Args:
        var_value (str): variable value
        var_name (str): variable name

    Returns:
        bool: value is a boolean
    """
    var_value_uppercase = var_value.upper()
    if var_value_uppercase not in ["TRUE", "FALSE"]:
        logging.error(f"Invalid '{var_name}' setting: '{var_value}' only 'True' or 'False' allowed")
        return False
    return True

def check_tar_contents() -> bool:
    """Check tar content for load testing

    Returns:
        bool: content exists
    """
    successfully_configured = True
    file_to_package = os.listdir('/data-transfer/mock-patients')
    with tarfile.open('/data-transfer/mock-patients.tar.gz', 'r:gz') as tar:
        tar_contents = tar.getnames()
        for patient in file_to_package:
            if f"mock-patients/{patient}" not in tar_contents:
                logging.error(f"\tFile: '{patient}' missing in mock-patients.tar.gz")
                successfully_configured = False
            else:
                logging.info(f"\tFile: '{patient}' found in mock-patients.tar.gz")
    return successfully_configured

def check_nginx_port(var_value: str, var_name: str):
    """Check nginx port limit

    Args:
        var_value (str): variable value
        var_name (str): variable name

    Returns:
        bool: value is correct
    """
    if var_value.isdigit():
        if int(var_value) < 1000:
            logging.warning(f"For Docker rootless installation it is recommended to set variable '{var_name}' to a value higher than 1000")
        return True
    else:
        logging.error(f"Variable '{var_name}' must be an integer")
        return False
    
def check_ports(nginx_port: str, nginx_port_minio_api: str, successfully_configured: bool):
    """Check that NGINX port and NGINX Minio API port differs
    Args:
        nginx_port (str): NGINX port
        nginx_port_minio_api (str): NGINX Minio API port
        successfully_configured (bool): track configuration status

    Returns:
        bool: ports differs
    """
    if nginx_port == nginx_port_minio_api:
        logging.error("Values of variables 'NGINX_PORT' and 'NGINX_PORT_MINIO_API' must differ")
        return False
    else:
        return successfully_configured

def check_https_protocol(var_value: str, var_name: str):
    """Check for HTTPS protocol in value

    Args:
        var_value (str): variable value
        var_name (str): variable name

    Returns:
        bool: HTTPS protocol is used
    """
    if not var_value.startswith('https://'):
        logging.error(f"Variable '{var_name}' with value '{var_value}' must adhere to HTTPS protocol")
        return False
    return True

def strip_quotes(value: str) -> str:
    """Strip leading and trailing quotes from the string

    Args:
        value (str): variable value

    Returns:
        str: stripped value
    """
    if value and isinstance(value, str):
        return value.split('# Definition')[0].strip().strip('"')
    return value

def load_env_file() -> dict:
    """Load environmental variables into memory

    Returns:
        dict: environmental variables map
    """
    dot_env = {}
    for key, value in os.environ.items():
        dot_env[key] = strip_quotes(value)
    return dot_env

def check_base_path(var_value: str, var_name: str) -> bool:
    """Check base path

    Args:
        var_value (str): variable value
        var_name (str): variable name

    Returns:
        bool: base path is defined
    """
    check_for_linux = platform.system()
    if check_for_linux not in ["Linux", "Darwin"]:
        if var_value == 'RequiredForWindowsInstallation':
            logging.error(f"Variable '{var_name}' missing or empty in .env file. Variable must be configured on Windows machine.")
            return False
    return True

def check_einstein_api(endpoint: str, user: str, password: str):
    """Check that Einstein API is reachable

    Args:
        endpoint (str): Einstein endpoint
        user (str): Einstein user
        password (str): Einstein password
    """
    root_certificate = '/data-transfer/root-certificate.cert'
    try:
        if os.path.exists(root_certificate):
            response = requests.post(f"{endpoint}/token", data={"username": user, "password": password}, verify=root_certificate)
        else:
            response = requests.post(f"{endpoint}/token", data={"username": user, "password": password})
        headers = {'Authorization': 'Bearer ' + response.json()['access_token']}
        if os.path.exists(root_certificate):
            response = requests.get(f'{endpoint}/api', verify=root_certificate, headers=headers)
        else:
            response = requests.get(f'{endpoint}/api', headers=headers)

        if response.status_code != 200:
            logging.warning(f"Before processing data with the SPHN Connector, make sure that Einstein is reachable at '{endpoint}/api'")
    except Exception:
        logging.warning(f"Before processing data with the SPHN Connector, make sure that Einstein is reachable at '{endpoint}/api'")

def test_bucket_permissions(s3_client: boto3.client, bucket_name: str, successfully_configured: bool) -> bool:
    """Test permissions on S3 bucket

    Args:
        s3_client (boto3.client): S3 client connection object
        bucket_name (str): name of the bucket
        successfully_configured (bool): track configuration status

    Returns:
        bool: configuration successful
    """
    test_key = 'setup_test_file.txt'

    # Write an empty file
    try:
        s3_client.put_object(Bucket=bucket_name, Key=test_key, Body=b'')
    except Exception:
        logging.error(f"Write permissions missing on bucket '{bucket_name}'")
        successfully_configured = False
    
    # Read the empty file
    try:
        s3_client.get_object(Bucket=bucket_name, Key=test_key)
    except Exception:
        logging.error(f"Read permissions missing on bucket '{bucket_name}'")
        successfully_configured = False
    
    # Delete the empty file
    try:
        s3_client.delete_object(Bucket=bucket_name, Key=test_key)
    except Exception:
        logging.error(f"Delete permissions missing on bucket '{bucket_name}'")
        successfully_configured = False

    return successfully_configured

def check_external_s3_availability(endpoint: str, 
                                   user: str, 
                                   password: str, 
                                   successfully_configured: bool, 
                                   einstein_activated: bool, 
                                   einstein_bucket: str, 
                                   connector_bucket: str) -> bool:
    """Check external S3 availability

    Args:
        endpoint (str): S3 endpoint
        user (str): S3 user
        password (str): S3 password
        successfully_configured (bool): track configuration status
        einstein_activated (bool): Einstein is activated
        einstein_bucket (str): name of Einstein bucket
        connector_bucket (str): name of Connector bucket

    Returns:
        bool: configuration successful
    """
    root_certificate = '/data-transfer/root-certificate.cert'
    try:
        if os.path.exists(root_certificate):
            client = boto3.client('s3', endpoint_url=endpoint, aws_access_key_id=user, aws_secret_access_key=password, config=botocoreConfig(signature_version='s3v4'),
                                  region_name='eu-central-1', verify=root_certificate)
        else:
            client = boto3.client('s3', endpoint_url=endpoint, aws_access_key_id=user, aws_secret_access_key=password, config=botocoreConfig(signature_version='s3v4'),
                                  region_name='eu-central-1')
        response = client.list_buckets()

        einstein_exists = einstein_activated
        connector_exists = True

        # Check buckets existence
        if response.get('Buckets'):
            bucket_names = [obj['Name'] for obj in response.get('Buckets')]
            if einstein_activated and einstein_bucket not in bucket_names:
                logging.error(f"Einstein configured bucket '{einstein_bucket}' does not exist in S3 storage. Please create it.")
                einstein_exists = False
                successfully_configured = False
            if connector_bucket not in bucket_names:
                logging.error(f"Connector configured bucket '{connector_bucket}' does not exist in S3 storage. Please create it.")
                connector_exists = False
                successfully_configured = False
        else:
            if einstein_activated:
                logging.error(f"Einstein configured bucket '{einstein_bucket}' does not exist in S3 storage. Please create it.")
                einstein_exists = False
            logging.error(f"Connector configured bucket '{connector_bucket}' does not exist in S3 storage. Please create it.")
            connector_exists = False
            successfully_configured = False

        # Test user permissions
        if einstein_exists:
            successfully_configured = test_bucket_permissions(client, einstein_bucket, successfully_configured)

        if connector_exists:
            successfully_configured = test_bucket_permissions(client, connector_bucket, successfully_configured)
    except Exception as exc:
        logging.error(f"Failed to connect to external S3 at endpoint '{endpoint}': {exc}")
        successfully_configured = False
    return successfully_configured

def check_variables(variables: dict, dot_env: dict, mandatory: bool, successfully_configured: bool, optional_vars_default_values: dict = {}) -> bool:
    """Check variables

    Args:
        variables (dict): map of variables
        dot_env (dict): envirnomental variables map
        mandatory (bool): is variable mandatory
        successfully_configured (bool): track configuration status
        optional_vars_default_values (dict, optional): default variables values. Defaults to {}.

    Returns:
        bool: configuration successful
    """
    for var, checks in variables.items():
        if mandatory:
            value = dot_env.get(var)
        else:
            value = dot_env.get(var, optional_vars_default_values.get(var))
        if value is None:
            if mandatory:
                logging.error(f"Variable '{var}' missing or empty in .env file. Variable must be configured")
                successfully_configured = False
        elif checks:
            if not isinstance(checks, list):
                checks = [checks]
            for check in checks:
                if not check(value, var):
                    successfully_configured = False
    return successfully_configured

def main():
    """Main method
    """
    logging.info("Start test of SPHN Connector configuration")
    dot_env = load_env_file()
    successfully_configured = True

    # # Check tar contents
    # Loadtesting commented out from pushed API Docker image
    # successfully_configured = check_load_testing_files(successfully_configured=successfully_configured)

    # Check for required environment variables
    required_vars = {
        "DATA_PROVIDER_ID": None,
        "API_USER": check_credentials_for_invalid_chars,
        "API_PASSWORD": [(lambda v, var: check_length(v, 12, var)), check_credentials_for_invalid_chars],
        "POSTGRES_USER": [check_credentials_for_invalid_chars, check_reserved_names],
        "POSTGRES_PASSWORD": [(lambda v, var: check_length(v, 12, var)), check_credentials_for_invalid_chars],
        "POSTGRES_UI_EMAIL": check_email_for_invalid_chars,
        "POSTGRES_UI_PASSWORD": [(lambda v, var: check_length(v, 12, var)), check_credentials_for_invalid_chars],
        "AIRFLOW_USER": check_credentials_for_invalid_chars,
        "AIRFLOW_PASSWORD": [(lambda v, var: check_length(v, 12, var)), check_credentials_for_invalid_chars],
        "AIRFLOW_FIRSTNAME": None,
        "AIRFLOW_LASTNAME": None,
        "AIRFLOW_EMAIL": check_email_for_invalid_chars,
        "GRAFANA_ADMIN_USER": check_credentials_for_invalid_chars,
        "GRAFANA_ADMIN_PASSWORD": [(lambda v, var: check_length(v, 12, var)), check_credentials_for_invalid_chars],
        "GRAFANA_POSTGRES_USER": [check_credentials_for_invalid_chars, check_reserved_names],
        "GRAFANA_POSTGRES_USER_PASSWORD": [(lambda v, var: check_length(v, 12, var)), check_credentials_for_invalid_chars]
    }

    successfully_configured = check_variables(variables=required_vars, dot_env=dot_env, mandatory=True, successfully_configured=successfully_configured)

    # Check for optional environment variables
    optional_vars = {
        "BASE_PATH": check_base_path,
        "NGINX_PORT": check_nginx_port,
        "NGINX_PORT_MINIO_API": check_nginx_port,
        "API_SECRET_KEY": [(lambda v, var: check_length(v, 32, var)), check_credentials_for_invalid_chars],
        "PARALLEL_PROJECTS": check_integer_value,
        "ACCESS_TOKEN_TIMEOUT": check_integer_value,
        "SPHN_CONNECTOR_MEMORY": check_integer_value,
        "SPHN_CONNECTOR_CPUS": check_integer_value,
        "EINSTEIN": check_boolean,
        "USE_EXTERNAL_S3": check_boolean
    }

    optional_vars_default_values = {
        "BASE_PATH": "RequiredForWindowsInstallation",
        "NGINX_PORT": "1443",
        "NGINX_PORT_MINIO_API": "1444"
    }

    successfully_configured = check_variables(variables=optional_vars, dot_env=dot_env, mandatory=False, successfully_configured=successfully_configured, 
                                              optional_vars_default_values=optional_vars_default_values)

    # Check that NGINX_PORT and NGINX_PORT_MINIO_API differs
    successfully_configured = check_ports(nginx_port=dot_env.get('NGINX_PORT', '1443'), nginx_port_minio_api=dot_env.get('NGINX_PORT_MINIO_API', '1444'), successfully_configured=successfully_configured)
    
    # Check for Einstein required environment variables
    einstein_activated = str(dot_env.get("EINSTEIN", False)).upper() == 'TRUE'
    if einstein_activated:
        einstein_required_vars = {}
        einstein_required_vars['EINSTEIN_ENDPOINT'] = check_https_protocol
        einstein_required_vars['EINSTEIN_API_USER'] = check_credentials_for_invalid_chars
        einstein_required_vars['EINSTEIN_API_PASSWORD'] = [(lambda v, var: check_length(v, 12, var)), check_credentials_for_invalid_chars]
        einstein_configured_successfully = check_variables(variables=einstein_required_vars, dot_env=dot_env, mandatory=True, successfully_configured=True)
        successfully_configured = successfully_configured and einstein_configured_successfully
        if einstein_configured_successfully:
            check_einstein_api(endpoint=dot_env.get('EINSTEIN_ENDPOINT'), user=dot_env.get('EINSTEIN_API_USER'), password=dot_env.get('EINSTEIN_API_PASSWORD'))
    
    external_storage = str(dot_env.get("USE_EXTERNAL_S3", False)).upper()

    # Check for required environment variables when USE_EXTERNAL_S3=False
    if external_storage == 'FALSE':
        
        # Mandatory
        internal_s3_vars = {}
        internal_s3_vars['MINIO_ACCESS_KEY'] = check_credentials_for_invalid_chars
        internal_s3_vars['MINIO_SECRET_KEY'] = [(lambda v, var: check_length(v, 12, var)), check_credentials_for_invalid_chars]
        successfully_configured = check_variables(variables=internal_s3_vars, dot_env=dot_env, mandatory=True, successfully_configured=successfully_configured)

        # Optional
        internal_s3_vars = {"MINIO_COMPRESSION": check_for_on_or_off}
        successfully_configured = check_variables(variables=internal_s3_vars, dot_env=dot_env, mandatory=False, successfully_configured=successfully_configured)

    # Check for required environment variables when USE_EXTERNAL_S3=True
    if external_storage == 'TRUE':
        external_s3_vars = {}
        external_s3_vars['EXTERNAL_S3_URL'] = check_https_protocol
        external_s3_vars['EXTERNAL_S3_ACCESS_KEY'] = check_credentials_for_invalid_chars
        external_s3_vars['EXTERNAL_S3_SECRET_KEY'] = None
        external_s3_successfully_configured = check_variables(variables=external_s3_vars, dot_env=dot_env, mandatory=True, successfully_configured=True)
        successfully_configured = successfully_configured and external_s3_successfully_configured
        if external_s3_successfully_configured:
            successfully_configured = check_external_s3_availability(endpoint=dot_env.get('EXTERNAL_S3_URL'), user=dot_env.get('EXTERNAL_S3_ACCESS_KEY'), 
                                                                     password=dot_env.get('EXTERNAL_S3_SECRET_KEY'), successfully_configured=successfully_configured,
                                                                     einstein_activated=einstein_activated,
                                                                     einstein_bucket=dot_env.get('EXTERNAL_EINSTEIN_S3_BUCKET', 'einstein'),
                                                                     connector_bucket=dot_env.get('EXTERNAL_CONNECTOR_S3_BUCKET', 'connector'))

    if successfully_configured:
        logging.info("Environment configuration finished successfully.")
        logging.info("Pulling SPHN Connector images from GitLab Registry...")
    else:
        logging.error("SPHN Connector setup failed. Please fix described errors and re-run the setup script.")
        exit(1)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Check SPHN Connector configuration')
    main()
