# How to install and use the sphn-connector-installer script

(C) Copyright SIB Swiss Institute of Bioinformatics 2024 under GNU GPL v3.0 See LICENSE and THIRD-PARTY-MATERIALS
## Prerequisites
In order to use the `install-sphn-connector.sh` script the following prerequisites are assumed:
1. There is a specific user under which the installation is done.
2. The `{installation-user}` has either root rights or the necessary rights to run the commands used in the script, especially `docker`.
3. All installation specific parameters are set in `.env-install` (described below).
4. The `{installation-user}` is a dedicated user that can install and run the SPHN Connector.
5. The `{installation-user}` has a home directory and the `install-sphn-connector.sh` is installed into this directory
6. The following assumption regarding directory structure is made

```
# Base directory where the SPHN Connector will be installed (BASE_DIR needs to be configured in .env-install).
/{base-dir}

# Directory where the sphn connector will be installed. The {installation-user} needs to be owner of the directory and the group owner is preferably set to an administrator group or the group the {installation-user} is in.
/{base-dir}/sphn-connector

# Home directory where the directory of {installation-user} is located.
/{home-dir}

# The install-sphn-connector.sh script and .env-install files will be presumed to be installed in the home directory of the user running it.
/{home-dir}/{installation-user}  

# Directory that stores/savekeeps the .env file, and the key-pairs for api and reverse proxy from the SPHN Connector which will replace the default installed files.
/{home-dir}/{installation-user}/sphn-connector-config   
```
## Install the scripts
Copy the following files into the users home directory and create the sphn-connector-config directory:

```
cp .env-install ~/
cp install-sphn-connector.sh ~/
chmod 700 ~/install-sphn-connector.sh
mkdir ~/sphn-connector-config
```

Copy the key-pairs for the api and the reverse proxy into `~/sphn-connector-config` from either the previous installation (`/{base-dir}/sphn-connector/data-transfer`) or from where you obtained the official key-pairs. The files owner should be the `{installation-user}` and the group owner the same as used for the sphn-connector directory.
Also copy the `.env` file from the SPHN Connector into `~/sphn-connector-config` from either the previous installation or directly from the [repository](.env).
Finally make sure to edit the `.env` to fit your installation, if necessary (see also the official [installation guide](https://docs.google.com/document/d/1KzxE0SSp7u5Dmz8QyjM4y42DS4nJ3oYW/edit#heading=h.s10uipfef6a2)).

## Note
If any of the commands in the `install-sphn-connector.sh` script fails due to access rights, do not change to use sudo before the command as this causes other issues. It should be sufficient to adjust the rights of the directory and files.

## Config file
The config file `.env_install` must reside in the home directory of the installing user.
The following parameters are supported:

### GIT_USER_EMAIL
The git username and email must be specified in order to use git. This is usually stored in the `.gitconfig` in the home directory but for consistency reason it will be set very time the script runs.

### GIT_USER
The git username and email must be specified in order to use git. This is usually stored in the `.gitconfig` in the home directory but for consistency reason it will be set very time the script runs.

### HTTP_PROXY
If your network is configured to use proxies for git then this parameter contains the URL and PORT of the proxy.


### HTTP_SSLCAINFO 
If your installation requires a certificate to be able to get through a proxy to the git repository this must be specified with full path and file name /path/path/file.extension

### GIT_TAG
If specified this tag will be checked out and the installation will be done with this version. If this parameter is not present the main branch is used to setup the sphn-connector.

### BASE_DIR
The directory where the sphn-connector directory is located.

## Note
If you do not use a certain parameter like `HTTP_PROXY`, you don't have to delete the line, just leave `HTTP_PROXY=` in the config. Also it is suggested to enclose the parameter values with ''.
# Running the installer
The installer can be executed as simply as running the following command `~/install-sphn-connector.sh` it is assumed the `.env-install` file is correctly setup`

# Troubleshooting
- Make sure the correct rights are set to execute the script `chmod 700 ~/install-sphn-connector`
- Make sure the user-owner and group-owner of the sphn-connector directory and the key-pairs for api and reverse proxy are set correctly as described above.
- User should be able to run docker if not you might want to add the user to the docker group by `sudo usermod -aG docker $USER`
- Check that all docker services are up and running `docker ps`
