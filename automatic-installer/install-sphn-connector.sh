#!/bin/bash

#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'install-sphn-connector.sh' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
# 

source .env-install
SUCCESSFULLY_CONFIGURED=true
USE_PROXY=true
USE_HTTP_SSL=true
CHECKOUT_TAG=true
DIR=$BASE_DIR/sphn-connector

missing_variable_warning(){
    echo "WARNING -- Variable '$1' missing or empty in .env file. Variable must be configured"
}
missing_sphn_connector_dir(){
    echo "ERROR -- sphn-connector directory does not exist!"
}

failed_copy_warning() {
    if [ $? -ne 0 ]; then
        echo "ERROR --  Copy failed. Issue with file: '$1'"
        exit 1
    fi
}

if [ -z ${GIT_USER_EMAIL} ]; then
    missing_variable_warning "GIT_USER_EMAIL"
    SUCCESSFULLY_CONFIGURED=false
fi
if [ -z "${GIT_USER}" ]; then
    missing_variable_warning "GIT_USER"
    SUCCESSFULLY_CONFIGURED=false
fi
if [ -z "${HTTP_SSLCAINFO}" ]; then
    USE_HTTP_SSL=false
fi
if [ -z "${HTTP_PROXY}" ]; then
    USE_PROXY=false
fi
if [ -z "${GIT_TAG}" ]; then
    CHECKOUT_TAG=false
fi
if [ ! -d "$DIR" ]; then
    missing_sphn_connector_dir
    SUCCESSFULLY_CONFIGURED=false
fi

if [ "$SUCCESSFULLY_CONFIGURED" = false ] ; then
    echo 'ERROR -- SPHN Connector install failed. Please take described steps and re-run the install script'
    exit 1
else

    git config --global user.email $GIT_USER_EMAIL && git config --global user.name $GIT_USER
    
    cd $BASE_DIR/sphn-connector && docker compose down --volumes --rmi all

    cd $BASE_DIR/sphn-connector && rm -rf *
    cd $BASE_DIR/sphn-connector && rm -rf .*

    # git checkout main

    if  [ "$USE_HTTP_SSL" = true ] ; then
        git config --global http.sslcainfo $HTTP_SSLCAINFO
    fi
    if  [ "$USE_PROXY" = true ] ; then
        git clone https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-connector.git $BASE_DIR/sphn-connector --config http.proxy=$HTTP_PROXY
    else
        git clone https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-connector.git $BASE_DIR/sphn-connector
    fi
    if  [ "$CHECKOUT_TAG" = true ] ; then
     	cd $BASE_DIR/sphn-connector && git checkout $GIT_TAG
    fi

    # manipulate config
    cp ~/sphn-connector-config/.env $BASE_DIR/sphn-connector || failed_copy_warning ".env"
    
    cp ~/sphn-connector-config/*.cert $BASE_DIR/sphn-connector/data-transfer/ || failed_copy_warning "certificate(s)"
    
    cp ~/sphn-connector-config/*.key $BASE_DIR/sphn-connector/data-transfer/ || failed_copy_warning "key(s)"
    
    # run setup & docker
    cd $BASE_DIR/sphn-connector && ./setup.sh && docker compose up -d

fi
