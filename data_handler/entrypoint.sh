#!/bin/sh
#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'entrypoint.sh' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
# 

cp /data-transfer-certificates/sql.cert /data-transfer-postgres-certs-volume/sql.cert
cp /data-transfer-certificates/sql.key /data-transfer-postgres-certs-volume/sql.key
cp /data-transfer-postgres/00_create_tables.sql /data-transfer-postgres-init-volume/00_create_tables.sql
cp /data-transfer-postgres/01_init_postgres.sh /data-transfer-postgres-init-volume/01_init_postgres.sh
cp /data-transfer-postgres/02_create_grafana_user.sh /data-transfer-postgres-init-volume/02_create_grafana_user.sh

# For postgres we need permission 644 for sql.cert and 640 for sql.key
# and the sql.key needs to be owned by postgres (0:70). Otherwise the postgres db won't startup

chown 0:70 /data-transfer-postgres-certs-volume/sql.key
chmod 640 /data-transfer-postgres-certs-volume/sql.key
chmod 644 /data-transfer-postgres-certs-volume/sql.cert

# make the creation of initial postgres tables & users executable

chmod 777 /data-transfer-postgres-init-volume/00_create_tables.sql
chmod 777 /data-transfer-postgres-init-volume/01_init_postgres.sh
chmod 777 /data-transfer-postgres-init-volume/02_create_grafana_user.sh

# Safeguard shell scripts an ensure correct encoding
dos2unix /data-transfer-postgres-init-volume/*.sh

# use root certificate if available
if [ -f /data-transfer-certificates/root-certificate.cert ]; then
    cp /data-transfer-certificates/root-certificate.cert /etc/ssl/certs/root-certificate.crt
    cp /data-transfer-certificates/root-certificate.cert /usr/local/share/ca-certificates/root-certificate.crt 
    chmod 777 /etc/ssl/certs/root-certificate.crt
    chmod 777 /usr/local/share/ca-certificates/root-certificate.crt 
    update-ca-certificates
fi

cp /etc/ssl/certs/* /data-transfer-ssl-certificates-volume-api
cp /etc/ssl/certs/* /data-transfer-ssl-certificates-volume-connector

# Copy NGINX reverse-proxy certificates
cp /data-transfer-certificates/reverse-proxy.cert /data-transfer-proxy-volume/reverse-proxy.cert
cp /data-transfer-certificates/reverse-proxy.key /data-transfer-proxy-volume/reverse-proxy.key
chmod 777 /data-transfer-proxy-volume/reverse-proxy.cert
chmod 777 /data-transfer-proxy-volume/reverse-proxy.key

# Copy NGINX configuration file
cp /data-transfer-nginx-config/nginx.conf /data-transfer-nginx-volume/nginx.conf
chmod 777 /data-transfer-nginx-volume/nginx.conf

# Copy loadtesting mock patients and update permissions
addgroup sphn
adduser -u 1000 -D -h /home/sphn sphn -G sphn

cp /data-transfer-certificates/mock-patients.tar.gz  /data-transfer-mock-patients-volume/mock-patients.tar.gz
chown -R sphn:sphn /data-transfer-mock-patients-volume
chmod -R 755 /data-transfer-mock-patients-volume