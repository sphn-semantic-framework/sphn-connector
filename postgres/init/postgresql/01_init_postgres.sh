#!/bin/bash

#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'rdf_parser.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#


echo "local all all md5"> /var/lib/postgresql/data/pg_hba.conf
echo "hostssl all all all md5">> /var/lib/postgresql/data/pg_hba.conf
