--
-- Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
--
-- This file: 'rdf_parser.py' is part of the SPHN Connector.
-- This file is subject to  the terms and conditions defined in the file "License.txt"
-- which is part of the source code package. See the "License.txt" file 
-- distributed with this work for additional information regarding copyright ownership.
-- You may not use this file except in compliance with the licence. 

-- You should have received a copy of the GNU General Public License
-- along with this program; if not, see <http://www.gnu.org/licenses/>.
--


CREATE TABLE IF NOT EXISTS configuration (
    project_name varchar(200),
    data_provider_id varchar(200),
    api_user varchar(200),
    api_password varchar(200),
    s3_access_key varchar(200),
    s3_secret_key varchar(200),
    postgres_user varchar(200),
    postgres_password varchar(200),
    postgres_ui_email varchar(200),
    postgres_ui_password varchar(200), 
    airflow_user varchar(200),
    airflow_password varchar(200),
    airflow_firstname varchar(200),
    airflow_lastname varchar(200),
    airflow_email varchar(200),
    initialized boolean,
    sphn_base_iri varchar(100),
    output_format varchar(10),
    compression boolean,
    share_with_einstein boolean,
    project_extended_tables_prefix varchar(100),
    PRIMARY KEY(project_name, data_provider_id)
    );

CREATE TABLE IF NOT EXISTS landing_zone (
    project_name varchar(200),
    patient_id varchar(200),
    data_provider_id varchar(200),
    object_name varchar(1000),
    file_type varchar(5),
    timestmp timestamp,
    ingestion_type varchar(50),
    processed boolean,
    creation_time timestamptz,
    size numeric,
    PRIMARY KEY(project_name, patient_id, data_provider_id)
    );

CREATE TABLE IF NOT EXISTS refined_zone (
    project_name varchar(200),
    patient_id varchar(200),
    data_provider_id varchar(200),
    object_name varchar(1000),
    timestmp timestamp,
    processed boolean,
    size numeric,
    PRIMARY KEY(project_name, patient_id, data_provider_id)
    );

CREATE TABLE IF NOT EXISTS graph_zone (
    project_name varchar(200),
    patient_id varchar(200),
    data_provider_id varchar(200),
    object_name varchar(1000),
    timestmp timestamp,
    processed boolean,
    size numeric,
    PRIMARY KEY(project_name, patient_id, data_provider_id)
    );

CREATE TABLE IF NOT EXISTS release_zone (
    project_name varchar(200),
    patient_id varchar(200),
    data_provider_id varchar(200),
    object_name varchar(1000),
    validated_ok boolean,
    timestmp timestamp,
    downloaded boolean,
    size numeric,
    PRIMARY KEY(project_name, patient_id, data_provider_id)
    );

CREATE TABLE IF NOT EXISTS api_credentials (
    api_user varchar(200) PRIMARY KEY,
    api_password varchar(200),
    user_type varchar(20)
    );

CREATE TABLE IF NOT EXISTS logging (
    project_name varchar(200),
    patient_id varchar(200),
    data_provider_id varchar(200),
    step varchar(200),
    status varchar(50),
    message text,
    timestmp timestamp,
    PRIMARY KEY (project_name, patient_id, step, data_provider_id, status)
);

CREATE TABLE IF NOT EXISTS shacler_logging (
    project_name varchar(200),
    data_provider_id varchar(200),
    status varchar(50),
    message text,
    timestmp timestamp,
    PRIMARY KEY (project_name, data_provider_id, status)
);

CREATE TABLE IF NOT EXISTS ddl_generation (
    project_name varchar(200),
    data_provider_id varchar(200),
    ddl_statements text,
    object_type varchar(100),
    timestmp timestamp
);

CREATE TABLE IF NOT EXISTS rml_json_logs (
    project_name varchar(200),
    data_provider_id varchar(200),
    status varchar(50),
    message text,
    timestmp timestamp,
    PRIMARY KEY (project_name, data_provider_id, status)
);

CREATE TABLE IF NOT EXISTS pipeline_history (
    project_name varchar(300),
    data_provider_id varchar(200),
    pipeline_step varchar(100),
    pipeline_status varchar(100),
    patients_processed integer,
    patients_with_warnings integer,
    patients_with_errors integer,
    execution_time timestamp,
    elapsed_time integer,
    run_uuid varchar(300),
    PRIMARY KEY (project_name, data_provider_id, pipeline_step, run_uuid)
);

CREATE TABLE IF NOT EXISTS de_identification (
    project_name varchar(200),
    data_provider_id varchar(200),
    patient_id varchar(200),
    input_field varchar(3000),
    rule varchar(200),
    rule_name varchar(200),
    date_shift integer,
    substituted boolean,
    old_value varchar(500),
    new_value varchar(500),
    timestmp timestamptz,
    PRIMARY KEY (project_name, data_provider_id, patient_id, input_field, rule, rule_name, timestmp)
);

CREATE TABLE IF NOT EXISTS names_tracker (
    project_name varchar(200),
    data_provider_id varchar(200),
    file_class varchar(100),
    object_name varchar(300),
    original_name varchar(300),
    PRIMARY KEY (project_name, data_provider_id, file_class, object_name)
);

CREATE TABLE config_files_history (
    project_name varchar(200),
    data_provider_id varchar(200),
    filename varchar(500),
    file_class varchar(50),
    file_action varchar(20),
    timestmp timestamp,
    PRIMARY KEY (project_name, data_provider_id, filename, timestmp)
);

CREATE TABLE de_identification_shifts (
    project_name varchar(200),
    data_provider_id varchar(200),
    patient_id varchar(200),
    shift integer, 
    PRIMARY KEY (project_name, data_provider_id, patient_id)
);

CREATE TABLE de_identification_scrambling (
    project_name varchar(200),
    data_provider_id varchar(200),
    patient_id varchar(200),
    key_path varchar(1000),
    new_value varchar(50), 
    anonymized boolean,
    PRIMARY KEY (project_name, data_provider_id, patient_id, key_path)
);

CREATE TABLE execution_errors (
    project_name varchar(200),
    data_provider_id varchar(200),
    patient_id varchar(200),
    run_uuid varchar(300),
    step varchar(50),
    error_level varchar(10),
    timestmp timestamp,
    PRIMARY KEY (project_name, data_provider_id, patient_id, run_uuid, step, error_level, timestmp)
);

CREATE TABLE tdb2_status (
    project_name varchar(200),
    data_provider_id varchar(200),
    up_to_date boolean,
    PRIMARY KEY (project_name, data_provider_id)
);

CREATE TABLE external_terminologies (
    project_name varchar(200),
    data_provider_id varchar(200),
    terminology_name varchar(200),
    version_iri varchar(200),
    PRIMARY KEY (project_name, data_provider_id, terminology_name)
);

CREATE TABLE sparqler_count_instances (
    project_name varchar(200),
    data_provider_id varchar(200),
    patient_id varchar(200),
    timestmp timestamp,
    concept varchar(200),
    count integer,
    PRIMARY KEY(project_name, data_provider_id, patient_id, concept)
);

CREATE TABLE sparqler_min_max_predicates_extensive (
    project_name varchar(200),
    data_provider_id varchar(200),
    patient_id varchar(200),
    timestmp timestamp,
    concept varchar(200),
    attribute varchar(1000),
    min varchar(200),
    max varchar(200),
    value varchar(300),
    PRIMARY KEY(project_name, data_provider_id, patient_id, concept, attribute, min, max, value)
);

CREATE TABLE sparqler_min_max_predicates_numeric_extensive (
    project_name varchar(200),
    data_provider_id varchar(200),
    patient_id varchar(200),
    timestmp timestamp,
    concept varchar(200),
    attribute varchar(1000),
    min double precision,
    max double precision,
    value double precision,
    PRIMARY KEY(project_name, data_provider_id, patient_id, concept, attribute, min, max, value)
);

CREATE TABLE sparqler_min_max_predicates_fast (
    project_name varchar(200),
    data_provider_id varchar(200),
    patient_id varchar(200),
    timestmp timestamp,
    concept varchar(200),
    attribute varchar(1000),
    min varchar(200),
    max varchar(200),
    count integer,
    PRIMARY KEY(project_name, data_provider_id, patient_id, concept, attribute, min, max, count)
);

CREATE TABLE sparqler_min_max_predicates_numeric_fast (
    project_name varchar(200),
    data_provider_id varchar(200),
    patient_id varchar(200),
    timestmp timestamp,
    concept varchar(200),
    attribute varchar(1000),
    min double precision,
    max double precision,
    count integer,
    PRIMARY KEY(project_name, data_provider_id, patient_id, concept, attribute, min, max, count)
);

CREATE TABLE sparqler_has_code (
    project_name varchar(200),
    data_provider_id varchar(200),
    patient_id varchar(200),
    timestmp timestamp,
    concept varchar(200),
    attribute varchar(1000),
    code varchar(1000),
    count integer,
    PRIMARY KEY(project_name, data_provider_id, patient_id, concept, attribute, code)
);

CREATE TABLE sparqler_administrative_case_encounters (
    project_name varchar(200),
    data_provider_id varchar(200),
    patient_id varchar(200),
    timestmp timestamp,
    concept varchar(200),
    administrative_case varchar(300),
    PRIMARY KEY(project_name, data_provider_id, patient_id, concept, administrative_case)
);

CREATE TABLE stats_individual_reports (
    project_name varchar(200),
    data_provider_id varchar(200),
    patient_id varchar(200),
    query_name varchar(200),
    timestmp timestamp,
    header varchar(3000),
    query_output text,
    PRIMARY KEY(project_name, data_provider_id, patient_id, query_name)
);

CREATE TABLE quality_checks (
    project_name varchar(200),
    data_provider_id varchar(200),
    patient_id varchar(200),
    successful_validation boolean,
    severity varchar(50),
    result_path varchar(300),
    source_constraint_component varchar(300),
    source_shape varchar(300),
    count integer,
    focus_node varchar(300),
    result_message varchar(2000),
    value varchar(300)
    );

CREATE TABLE supporting_concepts (
    data_provider_id varchar(200),
    project_name varchar(200),
    table_name varchar(300),
    PRIMARY KEY(data_provider_id, project_name, table_name)
);

CREATE TABLE real_time_processing (
    data_provider_id varchar(200),
    project_name varchar(200),
    step varchar(50),
    patients_processed integer,
    PRIMARY KEY(data_provider_id, project_name, step)
);

CREATE TABLE grafana_real_time_report (
    data_provider_id varchar(200),
    project_name varchar(200),
    step varchar(50),
    patients_processed integer,
    project_size BIGINT, 
    PRIMARY KEY(data_provider_id, project_name, step)
);

CREATE TABLE einstein_data_sharing (
    data_provider_id varchar(200),
    project_name varchar(200),
    file_identifier varchar(200),
    file_graph varchar(300),
    object_name varchar(300),
    status varchar(100),
    hash varchar(300),
    PRIMARY KEY (data_provider_id, project_name, file_identifier)
);

CREATE DATABASE SPHN_TABLES;

CREATE DATABASE Grafana;

CREATE DATABASE Performance;

CREATE DATABASE airflow_postgres;