#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'rml_generator_lib.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

import logging
import os
import io
from pathlib import Path
import re
from typing import Union
from rdflib import OWL, RDF, RDFS, Graph, SKOS
from rdflib import collection as rdflibCollection
from rdflib.term import URIRef, BNode
from collections import defaultdict
from rdflib.graph import Collection
import copy
from pyoxigraph import Store, Quad, NamedNode
from copy import deepcopy

def niceify_prefix(prefixable_iri: str) -> Union[str, str]:
    """Tries to make a simple string out of a possibly prefixable IRI

    Args:
        prefixable_iri (str): IRI to niceify

    Returns:
        Union[str, str]: pseudoprefix and concept name
    """
    if (prefixable_iri.count(":") == 1) & (prefixable_iri.count("/") == 0):
        # this is the case when we really have a prefixable iri here like sphn:Frequency
        return prefixable_iri.split(":", 1)[0], prefixable_iri.split(":", 1)[1]
    else:
        # this is the case when we just have an IRI. Then we need to reverse engineer some name for it.
        # e.g <https://biomedit.ch/rdf/sphn-schema/psss#Microorganism>
        prefixable_iri = prefixable_iri.strip("<>")
        if '#' in str(prefixable_iri):
            iri_splitted = prefixable_iri.split("#", 1)
            pseudoprefix = iri_splitted[0].rsplit('/', 1)[1]
            name = iri_splitted[1]
        else:
            # Assumption: the second last element indicates the pseudoprefix
            iri_splitted = prefixable_iri.rsplit('/', -1)
            pseudoprefix = iri_splitted[-2]
            name = iri_splitted[-1]
        return pseudoprefix, name

def filter_classrefs(classiri: URIRef, obj: URIRef, restrictions: dict, classrefs: list) -> list:
    """When not in the Code, Terminology case, it's possible that a restriction on a concept is defined (e.g. sphn:hasResult from sphn:ReferenceRangeInterpretation can only be
       a sphn:LabResult). In this case the restriction will have the sphn:LabResult in the value set and here we return the classes that are present in the classrefs but that are only
       defined in the value set. If no value set is defined we return all the collected classes.

    Args:
        classiri (URIRef): main class
        obj (URIRef): object property
        restrictions (dict): restrictions map
        classrefs (list): list of classes in range

    Returns:
        list: filtered classes in range
    """
    if 'valueset' in restrictions[classiri].get(str(obj), {}):
        values = list(restrictions[classiri][str(obj)]['valueset'].values())
        filtered_classrefs = [URIRef(ref) for ref in classrefs if str(ref) in values]
        if not filtered_classrefs:
            # To be sure in case we do not find matches we return the original classes in range
            return classrefs
        else:
            return filtered_classrefs
    else:
        return [URIRef(x) for x in classrefs]

def get_restrictions_from_map(restrictions_key: str, restrictions_map: dict, sphn_base_iri: str, project_iri: str) -> list:
    """Extract restriction from restrictions map

    Args:
        restrictions_key (str): transversed path for restrictions check
        restrictions_map (str): map paths and restrictions
        sphn_base_iri (str): SPHN base IRI
        project_iri (str): project base IRI

    Returns:
        list: list of classes in range
    """
    if any(val.startswith(sphn_base_iri) for val in restrictions_map[restrictions_key]):
        if restrictions_map[restrictions_key] == [f'{sphn_base_iri}#Code']:
            return [URIRef(f'{sphn_base_iri}#Code')]
        elif f'{sphn_base_iri}#Code' in restrictions_map[restrictions_key] and f'{sphn_base_iri}#Terminology' in restrictions_map[restrictions_key]:
            return [URIRef(f'{sphn_base_iri}#Code'), URIRef(f'{sphn_base_iri}#Terminology')]
        elif f'{sphn_base_iri}#Code' in restrictions_map[restrictions_key]:
            return [URIRef(f'{sphn_base_iri}#Code'), URIRef(f'{sphn_base_iri}#Terminology')]
        elif f'{sphn_base_iri}#Code' not in restrictions_map[restrictions_key] and f'{sphn_base_iri}#Terminology' in restrictions_map[restrictions_key]:
            return [URIRef(f'{sphn_base_iri}#Terminology')]
        else:
            return [URIRef(val) for val in restrictions_map[restrictions_key]]
    elif project_iri is not None and any(val.startswith(project_iri) for val in restrictions_map[restrictions_key]):
        return [URIRef(val) for val in restrictions_map[restrictions_key]]
    else:
        return [URIRef(f'{sphn_base_iri}#Terminology')]

def get_root_nodes_prefixes(root_nodes: set) -> dict:
    """Retuns map of root nodes prefixes

    Args:
        root_nodes (set): root nodes

    Returns:
        list: map of prefixes
    """
    prefixes = {}
    for root_node in root_nodes:
        prefix, value = niceify_prefix(prefixable_iri=root_node)
        prefixes[prefix] = str(root_node).rstrip(value)
    return prefixes

def get_range(classiri: URIRef, 
              schema_graph: Graph, 
              obj: URIRef, 
              restrictions: dict, 
              restrictions_key: str, 
              restrictions_map: dict, 
              sphn_base_iri: str, 
              is_legacy: bool,
              root_nodes: set, 
              project_iri: str) -> Union[list, bool]:
    """Returns range for a specific object proprety

    Args:
        classiri (URIRef): main concept class
        schema_graph (Graph): schema graph
        obj (URIRef): object to consider
        restrictions (dict): map of SPHN classes restrictions
        restrictions_key (str): transversed path for restrictions check
        restrictions_map (str): map paths and restrictions
        sphn_base_iri (str): SPHN base IRI
        is_legacy (bool): legacy schema
        root_nodes (set): root nodes
        project_iri (str): project base IRI

    Returns:
        Union[list, bool]: list of classes in the object range, is derived from a root concept
    """
    # Check if there is a restriction on the full transversed path, if sphn:Code is not available in the valueset and we don't have additional SPHN classes, 
    # we assume it's restricted on sphn:Terminology
    classrefs = None
    classiriprefix, classiriname = niceify_prefix(prefixable_iri=classiri)
    root_concept = False
    if is_legacy:
        source_class_path = str(classiri) + str(obj)
    else:
        source_class_path = str(classiri) + f"[{classiriprefix}{classiriname}]" + str(obj)
    if restrictions_key in restrictions_map:
        restriction_values = get_restrictions_from_map(restrictions_key=restrictions_key, restrictions_map=restrictions_map, sphn_base_iri=sphn_base_iri, project_iri=project_iri)
        if URIRef(f'{sphn_base_iri}#Code') in restriction_values or URIRef(f'{sphn_base_iri}#Terminology') in restriction_values:
            return restriction_values, root_concept
        else:
            classrefs = []
            if any(root_node in restriction_values for root_node in root_nodes):
                root_concept = True
            for value in restriction_values:
                classrefs += get_all_subclasses(graph=schema_graph, concept=value, sphn_namespace=sphn_base_iri, root_nodes=root_nodes)
                if value not in classrefs and value not in root_nodes:
                    classrefs += [value]
            classrefs = list(set(classrefs))
    elif source_class_path in restrictions_map:
        restriction_values = get_restrictions_from_map(restrictions_key=source_class_path, restrictions_map=restrictions_map, sphn_base_iri=sphn_base_iri, project_iri=project_iri)
        if URIRef(f'{sphn_base_iri}#Code') in restriction_values or URIRef(f'{sphn_base_iri}#Terminology') in restriction_values:
            return restriction_values, root_concept
        else:
            classrefs = []
            if any(root_node in restriction_values for root_node in root_nodes):
                root_concept = True
            for value in restriction_values:
                classrefs += get_all_subclasses(graph=schema_graph, concept=value, sphn_namespace=sphn_base_iri, root_nodes=root_nodes)
                if value not in classrefs and value not in root_nodes:
                    classrefs += [value]
            classrefs = list(set(classrefs))
    # If there is no restriction on the full path, we check the object property range
    if classrefs is None:
        classrefs = []
        classrefs_tmp = list(schema_graph.objects(obj, RDFS.range))
        if not classrefs_tmp:
            equivalent_property = list(schema_graph.objects(obj, OWL.equivalentProperty))
            classrefs_tmp = list(schema_graph.objects(equivalent_property[0], RDFS.range))
        # Expand classrefs in case no restrictions found (relevant especially for project specific properties)
        if any(root_node in classrefs_tmp for root_node in root_nodes):
            root_concept = True
        for value in classrefs_tmp:
            classrefs += get_all_subclasses(graph=schema_graph, concept=value, sphn_namespace=sphn_base_iri, root_nodes=root_nodes)
            if value not in classrefs and value not in root_nodes:
                classrefs += [value]
        classrefs = list(set(classrefs))
    multi_range = len(classrefs) > 1
    
    # Check if there is more than one class in range
    if multi_range:
        # Check if only sphn:Terminology is allowed in valueset of the classiri/object property restrictions
        if has_terminology_range_only(classiri=classiri, obj=obj, restrictions=restrictions, classrefs=classrefs, sphn_base_iri=sphn_base_iri, root_nodes=root_nodes):
            classrefs = [URIRef(f'{sphn_base_iri}#Terminology')]
        # Check if only sphn:Code is allowed in valueset of classiri/object property restrictions
        elif has_code_range_only(classiri=classiri, obj=obj, restrictions=restrictions, sphn_base_iri=sphn_base_iri):
            classrefs = [URIRef(f'{sphn_base_iri}#Code')]
        # Assume we have multi-classes that are not only sphn:Code and sphn:Terminology
        elif URIRef(f'{sphn_base_iri}#Code') not in classrefs or root_concept:
            # This logic is defined to rule out a specific class from the range if there is a restriction at the lower level. The only example we have so far (2023.2) is
            # the case sphn:AccessDevicePresence/sphn:hasMedicalDevice which has in range sphn:LabAnalyzer and sphn:MedicalDevice, but the restricion on 
            # sphn:hasMedicalDevice/sphn:hasTypeCode allows only sphn:Terminology and therefore the class sphn:LabAnalyzer can be ruled out as sphn:LabAnalyzer/sphn:hasTypeCode
            # allows only sphn:Code.
            if is_legacy and 'valueset' in restrictions[classiri][str(obj)] and 'prop_pipeline' in restrictions[classiri][str(obj)]['valueset']:
                values = copy.deepcopy(restrictions[classiri][str(obj)]['valueset'])
                values.pop('prop_pipeline')
                values = list(values.values())
                prop_pipeline = restrictions[classiri][str(obj)]['valueset']['prop_pipeline']
                tmp_classrefs = []
                classiri_key = str(classiri) + str(obj) + ''.join([prop for prop in prop_pipeline.values()])
                classiri_range = get_restrictions_from_map(restrictions_key=classiri_key, restrictions_map=restrictions_map, sphn_base_iri=sphn_base_iri, project_iri=project_iri)
                for classref in classrefs:
                    classref_key = str(classref) + ''.join([prop for prop in prop_pipeline.values()])
                    if classref_key in restrictions_map:
                        classref_range = get_restrictions_from_map(restrictions_key=classref_key, restrictions_map=restrictions_map, sphn_base_iri=sphn_base_iri, project_iri=project_iri)
                        # Here we assume that len(classiri_range) == 1, i.e. the extracted restriction returns a single value which should be either Code or Terminology
                        if set(classiri_range) == set(classref_range) or classiri_range[0] in classref_range:
                            tmp_classrefs.append(classref)
                    else:
                        tmp_classrefs.append(classref)
                classrefs = tmp_classrefs if tmp_classrefs else [URIRef(x) for x in classrefs]
                classrefs = filter_classrefs(classiri=classiri, obj=obj, restrictions=restrictions, classrefs=classrefs)
            else:
                classrefs = [URIRef(x) for x in classrefs]
            if not is_legacy:
                # Extract SKOS.scopeNote to check sub-classes that should be excluded from the range
                scope_notes = [str(x) for x in list(schema_graph.objects(classiri, SKOS.scopeNote))]
                pattern = re.compile(r'For\s+([^,]+),\s+instances of\s+(.*?)\s+are not allowed')
                if scope_notes:
                    obj_prefix, obj_name = niceify_prefix(prefixable_iri=obj)
                    object_to_match = f"{obj_prefix}:{obj_name}"
                    prefixes_map = get_root_nodes_prefixes(root_nodes=root_nodes)
                    for scope_note in scope_notes:
                        # Assuming we have one string in the list for the case where subclasses not allowed are specified, e.g. "For sphn:hasMedicalDevice, instances of sphn:AccessDevice, sphn:LabAnalyzer are not allowed"
                        match = pattern.search(scope_note)
                        if match:
                            object_property = match.group(1)
                            if object_property == object_to_match:
                                excluded_classes_str = [x.strip() for x in match.group(2).split(',')]
                                excluded_classes = []
                                for ex_class in excluded_classes_str:
                                    for prefix, iri in prefixes_map.items():
                                        ex_class = ex_class.replace(f'{prefix}:', iri)
                                    excluded_classes.append(URIRef(ex_class))
                                filtered_classrefs = [item for item in classrefs if item not in excluded_classes]
                                classrefs = filtered_classrefs
        # Range is composed of sphn:Code and sphn:Terminology
        else:
            classrefs = [URIRef(f'{sphn_base_iri}#Code'), URIRef(f'{sphn_base_iri}#Terminology')]
    elif niceify_prefix(prefixable_iri=classrefs[0]) == (None, None):
        classrefs = [URIRef(f'{sphn_base_iri}#Terminology')]
    return classrefs, root_concept

def export_graph(graph: Graph, output: str):
    """Exports graph to file

    Args:
        graph (Graph): graph to export
        output (str): output path of the exported file
    """
    logging.info(f"Exporting RML graph to file '{output}'")
    prefixes = set()
    with open(str(output), "w", encoding='utf-8') as out:
        for prefix, namespace in graph.namespace_manager.namespaces():
            if prefix:
                prefix_def = f"@prefix {prefix}: <{namespace}> .\n"
                out.write(prefix_def)
                prefixes.add(prefix_def.strip('\n'))
        serialized_lines = graph.serialize(format='ttl').splitlines()
        serialized_data = []
        for line in serialized_lines:
            if line not in prefixes:
                serialized_data.append(line)
        out.write('\n'.join(serialized_data))

def get_root_nodes(sphn: Graph, project: Graph = None) -> set:
    """Get root nodes

    Args:
        sphn (Graph): SPHN graph
        project (Graph, optional): project graph. Defaults to None.

    Returns:
        set: root nodes set
    """
    def getClasses(graph):
        classes = set()
        for s, p, o in graph.triples((None, RDF.type, OWL.Class)):
            if not isinstance(s, BNode):
                classes.add(s)
        return classes

    def getPrefix(subject, prefixes):
        for uri, prefix in prefixes:
            if subject.startswith(uri):
                return uri, prefix
        return None, None

    classes = getClasses(sphn)
    rootNodes = []
    for s in classes:
        if not list(sphn.triples((s, RDFS.subClassOf, None))) and s not in [URIRef("https://biomedit.ch/rdf/sphn-ontology/sphn#Deprecated"), URIRef("https://biomedit.ch/rdf/sphn-schema/sphn#Deprecated")]:
          rootNodes.append(s)

    # Logic is: for all the classes 
    #       for all the root Nodes in SPHN
    #       check whether all descendants have a different prefix and then add it to the root Nodes
    classes = getClasses(project)
    prefixes = [(name, pre) for pre, name in project.namespaces()]
    for s in classes:
        for root in rootNodes:
            for candidate, _, _ in list(project.triples((s, RDFS.subClassOf, root))):
                if not candidate.startswith(getPrefix(root, prefixes)[0]):
                    rootNodes.append(candidate)
    return rootNodes

def delete_roots_from_properties(graph: Graph, rootNodes: set):
    """From the domains before the visualization delete the root concepts of both project and sphn
       sphn delete it by default, however for the project only delete if the domain would not be empty afterwards

    Args:
        graph (Graph): data graph
        rootNodes (set): set of root nodes
    """
    def list_insertion(g, code_list, baseNode):
        # codes = [URIRef(code) for code in code_list]
        # domain_node = BNode()

        if len(code_list) > 1:
            bn1, bn2 = BNode(), BNode()
            Collection(g, bn1, code_list)
            graph.add((baseNode, RDFS.domain, bn2))
            graph.add((bn2, RDF.type, OWL.Class))
            graph.add((bn2, OWL.unionOf, bn1))
        else:
            graph.add((baseNode, RDFS.domain, code_list[0]))
        return

    Query = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX ex: <http://example.com/>
    SELECT ?s ?o2
    WHERE {
            ?s rdfs:domain ?o .
            ?o owl:unionOf/rdf:rest*/rdf:first ?o2 .
        }
    """
    response = graph.query(Query)
    dictionary = defaultdict(set)
    to_delete = defaultdict(set)
    for key, domain in response:
        dictionary[key].add(domain)
    if len(rootNodes) == 1:
        for key, value in dictionary.items():
            if len(value) > 1:
                    if rootNodes[0] in value: 
                        value.remove(rootNodes[0])
                        to_delete[key].add(rootNodes[0])
    else:
        for key, value in dictionary.items():
            if rootNodes[1] in value: 
                value.remove(rootNodes[1])
                to_delete[key].add(rootNodes[1])

            if len(value) > 1:
                if rootNodes[0] in value: 
                    value.remove(rootNodes[0])
                    to_delete[key].add(rootNodes[0])
    
    Query = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX ex: <http://example.com/>
    DELETE {
        ?property rdfs:domain ?blank_node_of_union .
    }
    WHERE {
        ?property rdfs:domain ?blank_node_of_union .
        FILTER(isblank(?blank_node_of_union))
    }
    """
    response = graph.update(Query)


    for key, value in dictionary.items():
        list_insertion(graph, list(value), key)
    return

def simplify_schema_graph(sphn_graph: Graph, project_graph: Graph) -> Union[set, Graph]:
    """Simplify schema graph by inheriting propeties of superclasses only, if there is not more specific subproperty in the class

    Args:
        sphn_graph (Graph): SPHN graph
        project_graph (Graph): project specific graph

    Returns:
        Union[set, Graph]: root nodes and simplified graph
    """
    logging.info("Simplifying schema graph")
    
    # Extract root nodes
    root_nodes = get_root_nodes(sphn=sphn_graph, project=project_graph)
    # Pre-process project graph to remove changes on SPHN properties on extended SPHN Concepts
    project_graph.update("""
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX sphn: <https://biomedit.ch/rdf/sphn-schema/sphn#>
        DELETE {
            ?concept rdfs:subClassOf ?potentialBlankNodeToDelete .
        }
        WHERE {
            ?concept rdf:type owl:Class .
            FILTER (STRSTARTS(str(?concept), "https://biomedit.ch/rdf/sphn-schema/sphn#"))   # do that only for sphn concepts
            ?concept rdfs:subClassOf+ sphn:SPHNConcept .
            ?concept rdfs:subClassOf ?potentialBlankNodeToDelete .
            {
                ?potentialBlankNodeToDelete owl:intersectionOf/rdf:rest*/rdf:first/owl:onProperty ?someProp .
            }
            UNION
            {
                ?potentialBlankNodeToDelete owl:onProperty ?someProp .
            }
            FILTER(STRSTARTS(str(?someProp), "https://biomedit.ch/rdf/sphn-schema/sphn#"))
        }
        """)
    
    # Unify graphs into a single graph
    sphn_graph += project_graph

    # Delete root nodes from properties' domains
    delete_roots_from_properties(graph=sphn_graph, rootNodes=root_nodes)

    # Define pyoxigraph store
    store = Store()
    sphn_graph_data = sphn_graph.serialize(format="turtle").encode("utf-8")
    store.bulk_load(input=sphn_graph_data, mime_type="text/turtle")

    # Extract properties
    query = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        SELECT DISTINCT ?property ?o2
        WHERE {
            ?property rdfs:domain ?o.
            ?o owl:unionOf/rdf:rest*/rdf:first ?o2 .
            OPTIONAL {
                ?subProperty rdfs:subPropertyOf ?property .
                ?subProperty rdfs:domain/owl:unionOf/rdf:rest*/rdf:first ?o2 .
            }
            OPTIONAL {
                ?subProperty rdfs:subPropertyOf ?property .
                ?subProperty rdfs:domain ?o2 .
            }
            OPTIONAL {
                ?o2 rdfs:subClassOf/owl:intersectionOf/rdf:rest*/rdf:first/owl:onProperty ?prop .
                FILTER(?prop = ?property)
            }                        

            FILTER (!bound(?subProperty) || bound(?prop))
        }
        """
    # Now insert the new triples
    for row in store.query(query):
        store.add(Quad(row['property'], NamedNode(str(RDFS.domain)), row['o2']))

    # Now also make all OWL Properties beeing also instances of RDF:Property
    store.update("""
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX ex: <http://example.com/>
        INSERT {
            ?s rdf:type rdf:Property .
        }WHERE {
            ?s rdf:type ?objType .
            FILTER(?objType IN (owl:ObjectProperty, owl:FunctionalProperty, owl:DatatypeProperty, owl:AnnotationProperty))
        }""")

    # Workaround for issue where range is a blankNode with some list
    store.update("""
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        DELETE {
            ?property rdfs:range ?blank_node_of_union .
        }
        INSERT {
            ?property rdfs:range ?o2 .
        }
        WHERE {
            ?property rdfs:range ?blank_node_of_union .
            ?blank_node_of_union owl:unionOf ?o.
            ?o (rdf:rest*/rdf:first) ?o2.
        }
        """)

    # Explicitly push inherited property down ==> But like on https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology-documentation-visualization/-/issues/29 
    # but only push where there is no more precise subproperty which is applicable
    store.update("""
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        INSERT {
            ?prop rdfs:domain ?sub .
        }
        WHERE {
            ?sub rdfs:subClassOf+ ?super .
            OPTIONAL {
                ?prop rdfs:domain ?super.
            }
            OPTIONAL {
                ?prop rdfs:domain/owl:unionOf/rdf:rest*/rdf:first ?super.
            }
            FILTER (bound(?prop))
            FILTER NOT EXISTS {
                ?subprop rdfs:subPropertyOf+ ?prop .
                ?subprop rdfs:domain ?sub .
            }
        }
        """)

    # Explicitly push down property usage (range) to children. e.g. SingleNucleotideVariation (SNV) is a child of GeneticVariation. hasGeneticVariation only has rdfs:range GeneticVariation.
    # in RDFS this means, every instance of an SNV is also an instance of a GeneticVariation. Therfore hasGeneticVariation can also point to rdfs:range sphn:SingleNucleotideVariation
    store.update("""

        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

        INSERT {
            ?prop rdfs:range ?sub .
        }
        WHERE {
            ?sub rdfs:subClassOf+ ?super .
            OPTIONAL {
                ?prop rdfs:range ?super.
            }
            OPTIONAL {
                ?prop rdfs:range/owl:unionOf/rdf:rest*/rdf:first ?super.
            }
            FILTER (bound(?prop))
            FILTER(?super != <https://biomedit.ch/rdf/sphn-ontology/sphn#Terminology>)
            FILTER(?super != <https://biomedit.ch/rdf/sphn-schema/sphn#Terminology>)
            FILTER(?super != <https://biomedit.ch/rdf/sphn-ontology/sphn#SPHNConcept>)
            FILTER(?super != <https://biomedit.ch/rdf/sphn-schema/sphn#SPHNConcept>)
            FILTER NOT EXISTS {
                ?subprop rdfs:subPropertyOf+ ?prop .
                ?subprop rdfs:range ?sub .
            }
        }

        """)
    
    store_data = io.BytesIO()
    store.dump(store_data, "text/turtle")
    pre_processed_graph = Graph()
    store_data.seek(0)
    pre_processed_graph.parse(store_data)

    # Collect and bind prefixes to pre-processed graph
    prefixes = {}
    for prefix, namespace in project_graph.namespace_manager.namespaces():
        if namespace not in prefixes or not prefixes[namespace]:
            prefixes[namespace] = prefix
        
    for prefix, namespace in sphn_graph.namespace_manager.namespaces():
        if namespace not in prefixes or not prefixes[namespace]:
            prefixes[namespace] = prefix

    for namespace, prefix in prefixes.items():
        pre_processed_graph.namespace_manager.bind(prefix, namespace)
    return root_nodes, pre_processed_graph
    
def load_graph(path: Path, schema_file_type: str):
    """Loads schema graph into memory

    Args:
        path (Path): local path to graph file
        schema_file_type (str): type of file, e.g. 'turtle'

    Returns:
        Graph: Graph object representing the loaded graph
    """
    logging.info(f"Loading schema file '{path}'")
    g = Graph()
    if schema_file_type is None:
        try:
            g.parse(str(path))
        except Exception:
            error_msg = "The file type guessed from the extension seems to be wrong. Try to explicitly state the file type of the schema."
            logging.error(error_msg)
            raise Exception(error_msg)
    else:
        try:
            if not os.path.exists(path):
                error_msg = f"Schema file '{path}' does not exist"
                logging.error(error_msg)
                raise Exception(error_msg)
            logging.debug("Trying to parse: " + str(path) + " in format " + schema_file_type)
            g.parse(str(path), format=schema_file_type)          
        except Exception:
            error_msg = f"Parsing of file '{path}' failed. File type could be wrong"
            logging.error(error_msg)
            raise Exception(error_msg)
    return g

def has_value_set(classiri: URIRef, obj: URIRef, schema_graph: Graph, restrictions: dict) -> bool:
    """Checks if an object property is defined as a ValueSet

    Args:
        classiri (URIRef): concept class
        obj (URIRef): object property
        schema_graph (Graph): schema graph
        restrictions (dict): map of SPHN classes restrictions

    Returns:
        bool: class has ValueSet
    """
    if 'valueset' in restrictions[classiri][str(obj)]:
        valueset_values = restrictions[classiri][str(obj)]['valueset']
        for valueset_value in valueset_values.values():
            subclass = URIRef(valueset_value)
            parents = (schema_graph.objects(subclass, RDFS.subClassOf))
            parents = [x for x in parents]
            if parents and isinstance(parents[0], URIRef):
                _, parent_name = niceify_prefix(parents[0])
                if parent_name == 'ValueSet':
                    return True
        return False
    else:
        return False

def has_ucums(classiri: URIRef, obj: URIRef, restrictions: dict) -> bool:
    """Checks if an object property is defining UCUM values

    Args:
        classiri (URIRef): concept class
        obj (URIRef): object property
        restrictions (dict): map of SPHN classes restrictions

    Returns:
        bool: has UCUM values
    """
    if 'valueset' in restrictions[classiri][str(obj)]:
        valueset_values = restrictions[classiri][str(obj)]['valueset']
        for valueset_value in valueset_values.values():
            if valueset_value == 'https://biomedit.ch/rdf/sphn-resource/ucum/UCUM':
                return True
            else:
                return False
    return False

def get_value_set(classiri: URIRef, obj: URIRef, schema_graph: Graph, restrictions: dict) -> Union[list, list, URIRef]:
    """For ValueSet object, returns values, additional classes and name of subclass

    Args:
        classiri (URIRef): concept class
        obj (URIRef): object property
        schema_graph (Graph): schema graph
        restrictions (dict): map of SPHN classes restrictions

    Returns:
        Union[list, list, URIRef]: value sets, classes, subclass instance
    """
    valueset_values = restrictions[classiri][str(obj)]['valueset']
    classes = []
    values = []
    for valueset_value in valueset_values.values():
        subclass = URIRef(valueset_value)
        parents = (schema_graph.objects(subclass, RDFS.subClassOf))
        parents = [x for x in parents]
        if parents and isinstance(parents[0], URIRef):
            _, parent_name = niceify_prefix(parents[0])
            if parent_name == 'ValueSet':
                values =(schema_graph.subjects(RDF.type, subclass))
                values = [x for x in values]
                subclass = subclass
                values = values
            elif (subclass, RDF.type, OWL.Class):
                classes.append(subclass)
        elif (subclass, RDF.type, OWL.Class):
            classes.append(subclass)
    return values, classes, subclass

def has_higher_cardinality(classiri: URIRef, obj: URIRef, restrictions: dict, sphn_base_iri: str) -> bool:
    """Defines if the object must be defined as an array

    Args:
        classiri (URIRef): class object
        obj (URIRef): object property
        restrictions (dict): restrictions map
        sphn_base_iri (str): SPHN base IRI

    Returns:
        bool: object has higher cardinality
    """
    # For PSSS for example, we do not have any cardinalities defined in our restrictions, therefore we do not want to define the objects as arrays.
    # Case when multiple owl:someValuesFrom are defined on a property
    if isinstance(restrictions[classiri][str(obj)], list):
        obj_restrictions = restrictions[classiri][str(obj)][0]
    else:
        obj_restrictions = restrictions[classiri][str(obj)]
    min_cardinality = obj_restrictions.get('minCardinality')
    max_cardinality = obj_restrictions.get('maxCardinality')
    return min_cardinality is not None and max_cardinality not in ['0', '1'] and obj != URIRef(f'{sphn_base_iri}#hasSubjectPseudoIdentifier')

def has_terminology_range_only(classiri: URIRef, 
                               obj: URIRef, 
                               restrictions: dict, 
                               classrefs: list, 
                               sphn_base_iri: str, 
                               root_nodes: set) -> bool:
    """Checks if object needs only Terminology range definition in JSON schema

    Args:
        classiri (URIRef): concept class
        obj (URIRef): object property
        restrictions (dict): map of SPHN classes restrictions
        classrefs (list): list of class references
        sphn_base_iri (str): SPHN base IRI
        root_nodes (set): root nodes

    Returns:
        bool: has terminology range only
    """
    if 'valueset' in restrictions[classiri].get(str(obj), {}):
        values = list(restrictions[classiri][str(obj)]['valueset'].values())
        if values and URIRef(f'{sphn_base_iri}#Terminology') in classrefs and f'{sphn_base_iri}#Code' not in values and not any(str(root_concept) in values for root_concept in root_nodes):
            return True
    return False

def has_code_range_only(classiri: URIRef, obj: URIRef, restrictions: dict, sphn_base_iri: str) -> bool:
    """Checks if object needs only Code range definition in JSON schema

    Args:
        classiri (URIRef): concept class
        obj (URIRef): object property
        restrictions (dict): map of SPHN classes restrictions
        sphn_base_iri (str): SPHN base IRI

    Returns:
        bool: has Code range only
    """
    if 'valueset' in restrictions[classiri].get(str(obj), {}):
        values = list(restrictions[classiri][str(obj)]['valueset'].values())
        if values == [f'{sphn_base_iri}#Code']:
            return True
    return False

def add_repeated_concepts(sphn_base_iri: str, core_concepts: set, sphn_classes: list, is_legacy: bool):
    """From 2024 schema on, we add some concepts to the core concepts artificially. This is to reduce the size of the generated schema from the fact these concepts are
       repeatedly invoked.

    Args:
        sphn_base_iri (str): SPHN base IRI
        core_concepts (set): core concepts
        sphn_classes (str): available classes
        is_legacy (bool): legacy schema
    """
    if is_legacy:
        return
    else:
        concept_uri = URIRef(f'{sphn_base_iri}#SourceSystem')
        if concept_uri in sphn_classes:
            core_concepts.add(concept_uri)

def get_core_concepts(schema_graph: Graph, sphn_classes: list, sphn_base_iri: str, data_provider_class: str, is_legacy: bool) -> set:
    """Gets SPHN core concepts

    Args:
        schema_graph (Graph): schema graph
        sphn_classes (list): list of SPHN concept classes
        sphn_base_iri (str): SPHN base IRI
        data provider class (str): data provider class name
        is_legacy (bool): legacy schema

    Returns:
        set: core concepts
    """
    core_concepts = set()
    all_dataproperties = list(schema_graph.subjects(RDF.type, OWL.DatatypeProperty))
    for classiri in sphn_classes:
        _, classname = niceify_prefix(prefixable_iri=classiri)
        if classname in [data_provider_class, 'SubjectPseudoIdentifier']:
            # Add DataProvider and SubjectPseudoIdentifier as artificial core concepts
            core_concepts.add(classiri)
        else:
            domain_class = list(schema_graph.subjects(RDFS.domain, classiri))
            objectproperties_for_class = list(set(domain_class) - set(all_dataproperties))
            for obj in objectproperties_for_class:
                _, obj_name = niceify_prefix(prefixable_iri=obj)
                if obj_name== "hasSubjectPseudoIdentifier":
                    # All the concepts that define the properts prefix:hasSubjectPseudoIdentifier are added to the core concept list
                    core_concepts.add(classiri)
    
    # Add additional artificial concepts to the core concepts list (e.g. sphn:SourceSystem)
    add_repeated_concepts(sphn_base_iri=sphn_base_iri, core_concepts=core_concepts, sphn_classes=sphn_classes, is_legacy=is_legacy)
    return sorted(core_concepts)


def _add_cardinality_to_dict(G: Graph, in_domain_of: dict, subclass: object, atLeastOneWith: bool = False) -> dict:
    """Add cardinality to map

    Args:
        G (Graph): data graph
        in_domain_of (dict): collected restrictions
        subclass (object): subclass object
        atLeastOneWith (bool, optional): at least one condition. Defaults to False.

    Returns:
        dict: collected restrictions
    """
    for prop in list(G.objects(subject=subclass, predicate=OWL.onProperty)):
        if str(prop) not in in_domain_of :
            in_domain_of[str(prop)] = {}
        
        if isinstance(in_domain_of[str(prop)], list):
            return in_domain_of
        
        max = list(G.objects(subject=subclass, predicate=OWL.maxCardinality))
        if not atLeastOneWith: 
            min = list(G.objects(subject=subclass, predicate=OWL.minCardinality))
        else: 
            min = [0]

        if bool(min):
            # If a min cardinality restriction is already defined, we keep the highest value
            if "minCardinality" not in in_domain_of[str(prop)] or "minCardinality" in in_domain_of[str(prop)] and int(in_domain_of[str(prop)]["minCardinality"]) < int(min[0]):
                in_domain_of[str(prop)]["minCardinality"] = {}
                in_domain_of[str(prop)]["minCardinality"] = str(min[0])

        if bool(max):
            # If a max cardinality restriction is already defined, we keep the lowest value
            if "maxCardinality" not in in_domain_of[str(prop)] or "maxCardinality" in in_domain_of[str(prop)] and int(in_domain_of[str(prop)]["maxCardinality"]) > int(max[0]):
                in_domain_of[str(prop)]["maxCardinality"] = {}
                in_domain_of[str(prop)]["maxCardinality"] = str(max[0])
    
    return in_domain_of

def _get_valueset_vals(G: Graph, in_domain_of: map, prop: URIRef, subclass: BNode, predicate: URIRef, cnt: int) -> Union[dict, int]:
    """Find values from valuesets

    Args:
        G (Graph): data graph
        in_domain_of (map): collected restrictions
        prop (URIRef): proprety object
        subclass (BNode): subclass object
        predicate (URIRef): predicate object
        cnt (int): count

    Returns:
        Union[dict, int]: collected restrictions and count
    """
    # find values from valueset
    vals = list(G.objects(subject=subclass, predicate=predicate))
    # add valueset to dict
    for range in vals:
        _add_valueset_to_dict(in_domain_of, prop, range, cnt)
        cnt += 1
    return in_domain_of, cnt

def _add_valueset_to_dict(in_domain_of: map, prop: URIRef, range: URIRef, cnt: int) -> dict:
    """Add value to dict

    Args:
        in_domain_of (map): collected restrictions
        prop (URIRef): property object
        range (URIRef): range object
        cnt (int): count

    Returns:
        _type_: collected restrictions
    """
    # add value to dict
    in_domain_of[str(prop)]["valueset"][cnt] = str(range)
    # check if value is from snomed/chop/loinc or other
    # if any(x in str(range) for x in ["snomed", "chop", "loinc"]):
    #     in_domain_of[str(prop)]["valueset"]["SPHNvalueset"] = True
    # else:
    #     in_domain_of[str(prop)]["valueset"]["SPHNvalueset"] = False
    return in_domain_of


def _retrieve_valueset_rest(G: Graph, in_domain_of: dict, prop: URIRef, node: BNode):
    """Retrieve valueset restriction

    Args:
        G (Graph): data graph
        in_domain_of (dict): collected restrictions
        prop (URIRef): property object
        node (BNode): node object
    """
    if isinstance(in_domain_of[str(prop)], list):
        return in_domain_of
        #check that it is not a cardinality restriction
    multiple_restrictions = []
    # New pattern in the 2024.1 RDF schema allows the definition of multiple restrictions on a property. For example sphn:TumorStageAssessment/sphn:hasResult
    # has a restriction on the range (sphn:Result) and a restriction at the lower level, i.e. sphn:hasResult/sphn:hasCode. To avoid overwriting the "valueset"
    # object we store the current restrictions on the property and define it afterwards as a list where only the "valueset" component will be different.
    if "valueset" in in_domain_of[str(prop)] and in_domain_of[str(prop)]["valueset"] != {}: 
        multiple_restrictions.append(deepcopy(in_domain_of[str(prop)]))
    if bool(list(G.objects(subject=node, predicate=OWL.someValuesFrom))):
        in_domain_of[str(prop)]["valueset"] = {}

    # find all restrictions (different patterns), start owl:someValuesFrom
    for rest in G.objects(subject=node, predicate=OWL.someValuesFrom):

        # owl:Class 
        if(rest, RDF.type, OWL.Class) in G:
            # owl:unionOf, one or more values
            for element in G.objects(subject=rest, predicate=OWL.unionOf):
                collection = list(rdflibCollection.Collection(G, element))
                cnt = 0

                for val in collection:
                    in_domain_of = _add_valueset_to_dict(in_domain_of, prop=prop, range=val, cnt=cnt)
                    cnt += 1
            # no owl:unionOf, only one value
            if in_domain_of[str(prop)]["valueset"] == {}:
                in_domain_of = _add_valueset_to_dict(in_domain_of, prop=prop, range=rest, cnt=0)

        # owl:Restriction, path of more than one property
        elif(rest, RDF.type, OWL.Restriction) in G:
            in_domain_of[str(prop)]["valueset"]["prop_pipeline"] = {}
            prop_pipeline = list(G.objects(subject=rest, predicate=OWL.onProperty))

            for p in prop_pipeline:
                in_domain_of[str(prop)]["valueset"]["prop_pipeline"][0] = str(p)

            extract_list_of_restrictions(G=G, rest=rest, prop=prop, in_domain_of=in_domain_of)
        else:
            in_domain_of, cnt = _get_valueset_vals(G, in_domain_of, prop=prop, subclass=node, predicate=OWL.someValuesFrom, cnt=0)
    if multiple_restrictions:
        # Add restrictions to the list only if the restriction has not been collected yet
        if in_domain_of[str(prop)] not in multiple_restrictions:
            in_domain_of[str(prop)] = multiple_restrictions + [in_domain_of[str(prop)]]

    return in_domain_of


def extract_list_of_restrictions(G: Graph, rest: BNode, prop: URIRef, in_domain_of: dict):
    """Extract restrictions on someValuesFrom recursively

    Args:
        G (Graph): data graph
        rest (BNode): rest node
        prop (URIRef): property object
        in_domain_of (dict): collected restrictions
    """
    for element in G.objects(subject=rest, predicate=OWL.someValuesFrom):
        # owl:Class
        if(element, RDF.type, OWL.Class) in G:
            # owl:unionOf
            for restriction in G.objects(subject=element, predicate=OWL.unionOf):
                collection = list(rdflibCollection.Collection(G, restriction))
                cnt = 0

                for node in collection:
                    prop_pipeline = list(G.objects(subject=node, predicate=OWL.onProperty))

                    # one or more values in owl:hasValue, path of three properties
                    if bool(prop_pipeline):
                        for p in prop_pipeline:
                            prop_path_ind = len(in_domain_of[str(prop)]["valueset"]["prop_pipeline"])
                            in_domain_of[str(prop)]["valueset"]["prop_pipeline"][prop_path_ind] = str(p)
                        in_domain_of, cnt = _get_valueset_vals(G, in_domain_of, prop=prop, subclass=node, predicate=OWL.hasValue, cnt=cnt)

                    # more than one value, path of two properties
                    else:
                        in_domain_of = _add_valueset_to_dict(in_domain_of, prop=prop, range=node, cnt=cnt)
                        cnt += 1

        # owl:Restriction, more than one value in owl:hasValue, path of two or three properties
        elif(element, RDF.type, OWL.Restriction) in G:
            prop_pipeline = list(G.objects(subject=element, predicate=OWL.onProperty))
            for p in prop_pipeline:
                prop_path_ind = len(in_domain_of[str(prop)]["valueset"]["prop_pipeline"])
                in_domain_of[str(prop)]["valueset"]["prop_pipeline"][prop_path_ind] = str(p)
            if list(G.objects(element, OWL.hasValue)):
                in_domain_of, cnt = _get_valueset_vals(G, in_domain_of, prop=prop, subclass=element, predicate=OWL.hasValue, cnt=0)
            else:
                extract_list_of_restrictions(G=G, rest=element, prop=prop, in_domain_of=in_domain_of)
        # one value, path of two properties
        else:
            in_domain_of = _add_valueset_to_dict(in_domain_of, prop=prop, range=element, cnt=0)

def get_restrictions_map(restrictions: dict) -> dict:
    """Get map of properties paths and attached valuesets
       It generates a dictionary that defines full property paths with classes in range and the corresponding valueset restrictions.
       For example:
       {
            "https://biomedit.ch/rdf/sphn-schema/sphn#BodyTemperature[sphnBodyTemperature]https://biomedit.ch/rdf/sphn-schema/sphn#hasDataFile[sphnTimeSeriesDataFile]https://biomedit.ch/rdf/sphn-schema/sphn#hasFormatCode": [
            "http://edamontology.org/format_1915"
            ],
            "https://biomedit.ch/rdf/sphn-schema/sphn#BodyTemperature[sphnBodyTemperature]https://biomedit.ch/rdf/sphn-schema/sphn#hasDataFile[sphnTimeSeriesDataFile]https://biomedit.ch/rdf/sphn-schema/sphn#hasHash": [
                "https://biomedit.ch/rdf/sphn-schema/sphn#Hash"
            ],
            "https://biomedit.ch/rdf/sphn-schema/sphn#BodyTemperature[sphnBodyTemperature]https://biomedit.ch/rdf/sphn-schema/sphn#hasDataFile": [
                "https://biomedit.ch/rdf/sphn-schema/sphn#TimeSeriesDataFile"
            ], ...
       } 

    Args:
        restrictions (dict): restrictions

    Returns:
        dict: restrictions map
    """
    restrictions_map = {}
    for concept, concept_restriction in restrictions.items():
        for object, obj_component in concept_restriction.items():
            # Case when multiple owl:someValuesFrom are defined on a property
            if not isinstance(obj_component, list):
                obj_restrictions = [obj_component]
            else:
                obj_restrictions = obj_component
            for obj_restriction in obj_restrictions:
                restriction_key = str(concept) + str(object)
                if 'valueset' in obj_restriction:
                    values_map = copy.deepcopy(obj_restriction['valueset'])
                    if 'prop_pipeline' in obj_restriction['valueset']:
                        for prop in obj_restriction['valueset']['prop_pipeline'].values():
                            restriction_key += str(prop)
                        values_map.pop('prop_pipeline')
                    restrictions_map[restriction_key] = list(values_map.values())
    return restrictions_map

def get_restrictions_map_with_range(restrictions: dict, schema_graph: Graph, sphn_base_iri: str, root_nodes: set, path: str = "") -> dict:
    """Get map of properties paths and attached valuesets

    Args:
        restrictions (dict): reastrictions map
        schema_graph (Graph): full graph
        sphn_base_iri (str): SPHN base IRI
        root_nodes (set): list of root nodes
        path (str, optional): collected path. Defaults to "".

    Returns:
        dict: restrictions map
    """
    restrictions_map = {}
    for concept, concept_restriction in restrictions.items():
        conceptprefix, conceptname = niceify_prefix(prefixable_iri=str(concept))
        for object, obj_component in concept_restriction.items():
            if object in path:
                return restrictions_map
            # Case when multiple owl:someValuesFrom are defined on a property
            if not isinstance(obj_component, list):
                obj_restrictions = [obj_component]
            else:
                obj_restrictions = obj_component
            for obj_restriction in obj_restrictions:
                restriction_key = str(concept) + f"[{conceptprefix}{conceptname}]" + str(object)
                if 'valueset' in obj_restriction:
                    values_map = copy.deepcopy(obj_restriction['valueset'])
                    property_keys = ""
                    if 'prop_pipeline' in obj_restriction['valueset']:
                        number_of_paths = len(obj_restriction['valueset']['prop_pipeline'].values())
                        for i_prop, prop in enumerate(obj_restriction['valueset']['prop_pipeline'].values(), start=1):
                            prop_range = list(schema_graph.objects(URIRef(prop), RDFS.range))
                            if set(prop_range) == set([URIRef(f'{sphn_base_iri}#Code'), URIRef(f'{sphn_base_iri}#Terminology')]):
                                prop_range = [URIRef(f'{sphn_base_iri}#Code')]
                            propprefix, propname = niceify_prefix(prefixable_iri=prop_range[0])
                            if i_prop < number_of_paths:
                                property_keys += str(prop) + f"[{propprefix}{propname}]"
                            else:
                                property_keys += str(prop) 
                        values_map.pop('prop_pipeline')

                    restriction_keys = []
                    if property_keys:
                        # It's possible that the object as multiple classes in range and we therefore need to define additional restrictions maps
                        obj_range = list(schema_graph.objects(URIRef(object), RDFS.range))
                        for object_class in obj_range:
                            objprefix, objname = niceify_prefix(prefixable_iri=object_class)
                            object_restriction_key = restriction_key + f'[{objprefix}{objname}]' + property_keys
                            restriction_keys.append(object_restriction_key)
                    else:
                        restriction_keys.append(restriction_key)
                    
                    for restriction_key in restriction_keys:
                        for value in values_map.values():
                            if URIRef(value) in restrictions:
                                classrefs = [URIRef(value)]
                                children = get_all_subclasses(graph=schema_graph, concept=URIRef(value), sphn_namespace=sphn_base_iri, root_nodes=root_nodes)
                                classrefs += children
                                for classref in classrefs:
                                    if classref in restrictions:
                                        result = get_restrictions_map_with_range(restrictions={classref: restrictions[classref]}, schema_graph=schema_graph, sphn_base_iri=sphn_base_iri, 
                                                                        root_nodes=root_nodes, path=restriction_key)
                                        for el_key, el_value in result.items():
                                            new_key = restriction_key + el_key.replace(str(classref), '')
                                            if new_key in restrictions_map:
                                                restrictions_map[new_key] = list(set(restrictions_map[new_key] + el_value))
                                            else:
                                                restrictions_map[new_key] = el_value
                        restrictions_map[restriction_key] = list(values_map.values())
    return restrictions_map

def get_restrictions(G: Graph, sphn_classes: list) -> dict:
    """Navigates through SPHN classes and extract restrictions information
       It creates a dictionary that maps the SPHN/project classes and properties to classes, cardinalities, and valuesets.
       For example:
       {
            "https://biomedit.ch/rdf/sphn-schema/sphn#BodyTemperature": {
                "https://biomedit.ch/rdf/sphn-schema/sphn#hasDataFile": {
                    "class": {
                        "0": "https://biomedit.ch/rdf/sphn-schema/sphn#DataFile",
                        "1": "https://biomedit.ch/rdf/sphn-schema/sphn#TimeSeriesDataFile"
                    },
                    "minCardinality": "0",
                    "maxCardinality": "1",
                    "valueset": {
                        "0": "https://biomedit.ch/rdf/sphn-schema/sphn#TimeSeriesDataFile"
                    }
                },  ...
            }
        }

    Args:
        G (Graph): schema graph
        sphn_classes (list): list of SPHN concept classes

    Returns:
        dict: map of SPHN classes restrictions
    """
    # Similar as before with 'prop_pipeline', taken from https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology-documentation-visualization/-/blob/master/pylode/modified/profiles/ontdoc.py?ref_type=heads
    restrictions = {}
    for s in sphn_classes:
        # retrieve the datatypes when it is a DatatypeProperty or ObjectProperty
        in_domain_of = {}
        for o in G.subjects(predicate=RDFS.domain, object=s):
            in_domain_of[str(o)] = {}
            #datatypes from DataProperty
            if (o, RDF.type, OWL.DatatypeProperty) in G :
                ranges = G.objects(subject=o, predicate=RDFS.range)
                range_list = list(ranges)
                in_domain_of[str(o)]["datatype"] = {}
                cnt = 0
                if (len(range_list) >= 1):
                    for range in range_list:
                        in_domain_of[str(o)]["datatype"][cnt] = str(range)
                        cnt += 1

            #datatypes from ObjectProperty
            if (o, RDF.type, OWL.ObjectProperty) in G :
                ranges = G.objects(subject=o, predicate=RDFS.range)
                range_list = list(ranges)
                cnt = 0
                if (len(range_list) >= 1):
                    in_domain_of[str(o)]["class"] = {}
                    for range in range_list:
                        in_domain_of[str(o)]["class"][cnt] = str(range)
                        cnt += 1

        #retrieve the restrictions (cardinalities and valuesets)
        for subclass in G.objects(subject=s, predicate=RDFS.subClassOf):
            if(subclass, RDF.type, OWL.Class) in G:

                # standard format: intersectionOfs
                for intersection in G.objects(subject=subclass, predicate=OWL.intersectionOf):
                    collection = list(rdflibCollection.Collection(G, intersection))
                    for node in collection:
                        if(node, RDF.type, OWL.Restriction) in G:

                            # find and add cardinality restrictions
                            in_domain_of = _add_cardinality_to_dict(G, in_domain_of, subclass=node)

                            # find and add valueset restrictions
                            for prop in G.objects(subject=node, predicate=OWL.onProperty):
                                in_domain_of = _retrieve_valueset_rest(G, in_domain_of, prop=prop, node=node)

                # at least one prop groups (unionOf in addition to intersectionOf)
                for union in G.objects(subject=subclass, predicate=OWL.unionOf):
                    collection = list(rdflibCollection.Collection(G, union))
                    propArray = []
                    first_prop = True

                    # append all props from one AtLeastOne group to array
                    for intersection in collection:
                        for ints in G.objects(subject=intersection, predicate=OWL.intersectionOf):
                            collection2 = list(rdflibCollection.Collection(G, ints))
                            for node in collection2:
                                if (node , RDF.type, OWL.Restriction) in G:
                                    for prop in list(G.objects(subject=node, predicate=OWL.onProperty)):
                                        propArray.append(prop)      

                    # now iterate through atLeastOne group again
                    for intersection in collection:
                        for ints in G.objects(subject=intersection, predicate=OWL.intersectionOf):
                            collection2 = list(rdflibCollection.Collection(G, ints))
                            for node in collection2:
                                if(node, RDF.type, OWL.Restriction) in G:
                                    for prop in G.objects(subject=node, predicate=OWL.onProperty):

                                        # add AtLeastOneWith key for identification
                                        if first_prop:
                                            propArray = list(set(propArray))
                                            propArray.remove(prop)
                                            in_domain_of[str(prop)]["atLeastOneWith"] = []
                                            in_domain_of[str(prop)]["atLeastOneWith"] = propArray
                                            first_prop = False
                                        elif prop in propArray:
                                            in_domain_of[str(prop)]["atLeastOneWith"] = []
                                            in_domain_of[str(prop)]["atLeastOneWith"].append("Yes")

                                        # find and add valueset restrictions
                                        in_domain_of = _retrieve_valueset_rest(G, in_domain_of, prop=prop, node=node)

                                    # find and add cardinality restriction
                                    in_domain_of = _add_cardinality_to_dict(G, in_domain_of, subclass=node, atLeastOneWith=True)

            # only one valueset and no cardinalities OR one cardinality and no other restrictions
            elif(subclass, RDF.type, OWL.Restriction) in G:
                # find and add cardinality restrictions
                in_domain_of = _add_cardinality_to_dict(G, in_domain_of, subclass=subclass)
                # find and add valueset restrictions
                for prop in list(G.objects(subject=subclass, predicate=OWL.onProperty)):
                    in_domain_of = _retrieve_valueset_rest(G, in_domain_of, prop=prop, node=subclass)

        restrictions[s] = in_domain_of
    return restrictions

def get_prefix_objects_map(objectproperties_for_class: list) -> dict:
    """Stores object names per prefixes in a map

    Args:
        objectproperties_for_class (list): list of objects URIRefs

    Returns:
        dict: map of prefix and list of objects
    """
    objmap = {}
    for obj in objectproperties_for_class:
        prefix, name = niceify_prefix(prefixable_iri=obj)
        if prefix not in objmap:
            objmap[prefix] = []
        objmap[prefix].append(name)
    return objmap

def sort_keys_recursively(data: dict) -> dict:
    """Sort dictionary keys recursively

    Args:
        data (dict): data to sort

        dict: sorted data
    """
    if isinstance(data, dict):
        return {key: sort_keys_recursively(value) for key, value in sorted(data.items())}
    elif isinstance(data, list):
        if isinstance(data[0], dict):
            return [sort_keys_recursively(item) for item in sorted(data, key=lambda x: x.get('description', ''))]
        else:
            return sorted(data)
    else:
        return data
    
def sort_json_schema(json_schema: dict, data_provider_class: str):
    """Sort JSON schema alphabetically

    Args:
        json_schema (dict): JSON schema
        data_provider_class (str): data provider class
    """
    json_schema['properties']['content'] = sort_keys_recursively(json_schema['properties']['content'])
    if 'supporting_concepts' in json_schema['properties']:
        json_schema['properties']['supporting_concepts'] = sort_keys_recursively(json_schema['properties']['supporting_concepts'])
    json_schema['properties']['sphn:DataRelease'] = sort_keys_recursively(json_schema['properties']['sphn:DataRelease'])
    json_schema['properties'][f'sphn:{data_provider_class}'] = sort_keys_recursively(json_schema['properties'][f'sphn:{data_provider_class}'])
    json_schema['properties']['sphn:SubjectPseudoIdentifier'] = sort_keys_recursively(json_schema['properties']['sphn:SubjectPseudoIdentifier'])

def check_invocation(obj: URIRef, 
                     classiri: URIRef, 
                     graph: Graph, 
                     filtered_class: list, 
                     all_dataproperties: list, 
                     restrictions: dict, 
                     restrictions_key: str, 
                     restrictions_map: dict, 
                     sphn_base_iri: str, 
                     is_legacy: bool, 
                     root_nodes: set, 
                     project_iri: str, 
                     source_concept: URIRef, 
                     path: str) -> bool:
    """Check invocation of concept starting from core concepts

    Args:
        obj (URIRef): object property
        classiri (URIRef): upper class
        graph (Graph): schema graph
        filtered_class (list): non-core concept to check
        all_dataproperties (list): all properties
        restrictions (dict): restrictions
        restrictions_key (str): transversed path for restrictions check
        restrictions_map (str): map paths and restrictions
        sphn_base_iri (str): SPHN base IRI
        is_legacy (bool): legacy schema
        root_nodes (set): root nodes
        project_iri (str): project base IRI
        source_concept (URIRef): initial core concept
        path (str): full path of property

    Returns:
        bool: is invoked in the graph
    """
    invoked = False
    classrefs, _ = get_range(classiri=classiri, schema_graph=graph, obj=obj, restrictions=restrictions, restrictions_key=restrictions_key, restrictions_map=restrictions_map, sphn_base_iri=sphn_base_iri, is_legacy=is_legacy, root_nodes=root_nodes,
                          project_iri=project_iri)
    if filtered_class in classrefs:
        return True
    else:
        for classref in classrefs:
            classrefprefix, classrefname = niceify_prefix(prefixable_iri=classref)
            if classiri in classrefs:
                return False
            if source_concept == classref:
                # Avoid looping into source core concept again
                return False
            domain_class = list(graph.subjects(RDFS.domain, classref))
            objectproperties_for_class = list(set(domain_class) - set(all_dataproperties))
            for inner_object in objectproperties_for_class:
                if str(inner_object) in path:
                    return False
                invoked = check_invocation(obj=inner_object, classiri=classref, graph=graph, filtered_class=filtered_class, all_dataproperties=all_dataproperties, restrictions=restrictions, restrictions_key=restrictions_key + f"[{classrefprefix}{classrefname}]" + str(inner_object), restrictions_map=restrictions_map, sphn_base_iri=sphn_base_iri, 
                                           is_legacy=is_legacy, root_nodes=root_nodes, project_iri=project_iri, source_concept=source_concept, path=path + str(inner_object))
                if invoked:
                    return invoked
    return invoked

def get_supporting_concepts(graph: Graph, 
                            core_concepts: list, 
                            sphn_classes: list, 
                            restrictions: dict, 
                            restrictions_map: dict, 
                            sphn_base_iri: str, 
                            root_nodes: list, 
                            is_legacy: bool,
                            project_iri: str, 
                            is_2024_schema: bool) -> set:
    """Get list of concepts not referenced from core concepts

    Args:
        graph (Graph): schema graph
        core_concepts (list): list of core concepts
        sphn_classes (list): list of all classes
        restrictions (dict): restrictions
        restrictions_map (str): map paths and restrictions
        sphn_base_iri (str): SPHN base IRI
        root_nodes (list): root nodes
        is_legacy (bool): legacy schema
        project_iri (str): project base IRI
        is_2024_schema (bool): is related to 2024 RDF schema

    Returns:
        set: list of supporting concepts
    """
    if is_legacy:
        return set()
    # Additional concepts to exclude
    deprecated_concepts = [x for x in (graph.subjects(RDFS.subClassOf, URIRef(f"{sphn_base_iri}#Deprecated"))) if isinstance(x, URIRef)]
    valuesets = [x for x in (graph.subjects(RDFS.subClassOf, URIRef(f"{sphn_base_iri}#ValueSet"))) if isinstance(x, URIRef)]
    
    filtered_concepts = []
    for concept in sorted([x for x in sphn_classes]):
        # Filter all the concepts that are not in the lists of excluded concepts
        if concept not in core_concepts + deprecated_concepts + valuesets + root_nodes + [URIRef(f"{sphn_base_iri}#Deprecated")]:
            filtered_concepts.append(concept)
    all_dataproperties = list(graph.subjects(RDF.type, OWL.DatatypeProperty))
    supporting_concepts = set()

    if not is_legacy and is_2024_schema:
        # Hardcode sphn:ReferenceInterpretationResult as supporting concept from 2024 onwards
        supporting_concepts.add(URIRef('https://biomedit.ch/rdf/sphn-schema/sphn#ReferenceInterpretationResult'))

    # Loop over all concepts and check if they are invoked by some other core concept
    for classiri in filtered_concepts:
        invoked = False
        for core_concept in core_concepts:
            core_conceptprefix, core_conceptname = niceify_prefix(prefixable_iri=core_concept)
            domain_class = list(graph.subjects(RDFS.domain, core_concept))
            objectproperties_for_class = list(set(domain_class) - set(all_dataproperties))
            for obj in objectproperties_for_class:
                invoked = check_invocation(obj=obj, classiri=core_concept, graph=graph, filtered_class=classiri, all_dataproperties=all_dataproperties, 
                                           restrictions=restrictions, restrictions_key=str(core_concept) + f"[{core_conceptprefix}{core_conceptname}]" + str(obj), restrictions_map=restrictions_map, sphn_base_iri=sphn_base_iri, is_legacy=is_legacy,
                                           root_nodes=root_nodes, project_iri=project_iri, source_concept=core_concept, path=str(core_concept) + str(obj))
                if invoked:
                    break
            if invoked:
                break
            
        if not invoked:
            # If a concept is not invoked by any other concept derived from core concepts, then we define it as supporting concept
            _, name = niceify_prefix(prefixable_iri=classiri)
            if name not in ['DataRelease', 'ValueSet']:
                supporting_concepts.add(classiri)
    return sorted(supporting_concepts)

def get_all_subclasses(graph: Graph, concept: URIRef, sphn_namespace: str, root_nodes: set, all_children_list: set = None) -> list:
    """Extract all direct/indirect subclasses of a concept

    Args:
        graph (Graph): graph
        concept (URIRef): root concept to process
        root_nodes (set): root concept to process
        sphn_namespace (str): SPHN namespace
        all_children_list (set, optional): collection of all the children. Defaults to set().

    Returns:
        list: collection of children of root concept
    """
    if concept == URIRef(f'{sphn_namespace}#Terminology'):
        # What about the direct child of Terminology defined by the projects, e.g. spo:Terminology????
        return []
    if all_children_list is None:
        all_children_list = set()
    children = list(graph.subjects(RDFS.subClassOf, concept))
    for child in children:
        if child not in root_nodes:
            all_children_list.add(child)
        if child == URIRef(f'{sphn_namespace}#Terminology'):
            continue
        else:
            get_all_subclasses(graph=graph, concept=child, sphn_namespace=sphn_namespace, root_nodes=root_nodes, all_children_list=all_children_list)
    return list(all_children_list)

def group_target_concepts(list_of_properties: list) -> list:
    """Group classes in range that describe the same properties into a single object with multiple target_concept values

    Args:
        list_of_properties (list): list of oneOf target objects

    Returns:
        list: grouped list of target objects
    """
    collected_objects = defaultdict(lambda: {'original_objects': [], 'target_concepts': [], 'cleaned_object': None})
    for element in list_of_properties:
        element_copy = deepcopy(element)
        points_to_core_concept = set(list(element_copy['properties'].keys())) == set(['id', 'target_concept'])
        if 'id' in element_copy['properties']:
            sphn_concept = element_copy['properties']['id']['description'].startswith('ID of SPHN Concept')
        else:
            sphn_concept = element_copy['description'].startswith('SPHN Concept') 
        target_defined = 'target_concept' in element_copy['properties']
        if points_to_core_concept or sphn_concept or not target_defined:
            collected_objects[str(element_copy)]['original_objects'] = [element_copy]
            collected_objects[str(element_copy)]['target_concepts'] = None
        else:
            element_copy.pop('description')
            element_copy['properties']['id'].pop('description')
            target_concept = element_copy['properties'].pop('target_concept')
            collected_objects[str(element_copy)]['original_objects'].append(element)
            collected_objects[str(element_copy)]['target_concepts'] += target_concept['enum']
            collected_objects[str(element_copy)]['cleaned_object'] = element_copy

    update_one_of_block = []
    for value in collected_objects.values():
        if value['target_concepts'] is not None:
            if len(value['target_concepts']) == 1:
                update_one_of_block.append(value['original_objects'][0])
            else:
                new_object = deepcopy(value['cleaned_object'])
                concepts = []
                for target in sorted(value['target_concepts']):
                    prefix, name = niceify_prefix(prefixable_iri=target)
                    concepts.append(f"{prefix}:{name}")
                new_object['description'] = f'Target concept {"/".join([concept for concept in concepts])}'
                new_object['properties']['id'] = {"type" : "string", "description": "ID of the target concept"}
                new_object['properties']['target_concept'] = {"type" : "string", "description": "IRI for unified Concepts", "enum": sorted(value['target_concepts'])}
                update_one_of_block.append(new_object)
        else:
            update_one_of_block.append(value['original_objects'][0])
    return update_one_of_block
 