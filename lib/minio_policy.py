#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'minio_policy.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

import subprocess
import os
import sys

sys.path.append(os.path.abspath(os.path.dirname(__file__)))
from enums import UserType


class MinioPolicy(object):

    def __init__(self, database: object):
        """Class constructor

        Args:
            database (object): Database object
        """
        self.database = database
        self.access_key = self.database.config.s3_access_key
        self.secret_key = self.database.config.s3_secret_key
        self.minio_endpoint = self.database.config.s3_endpoint
        self.use_external_s3 = self.database.config.use_external_s3
        self.api_user = self.database.config.api_user
        self.compression = self.database.config.s3_compression

    def set_alias(self):
        """Set alias
        """
        MinioPolicy._execute_command(cmd=f'/home/sphn/minio-binaries/mc alias set sib {self.minio_endpoint} {self.access_key} {self.secret_key}')
        # all compressible datatypes should be compressed
        if self.compression.upper() == "ON":
            MinioPolicy._execute_command(cmd="/home/sphn/minio-binaries/mc admin config set sib compression extensions= mime_types=")
        else:
            pass

    def create(self, user: str = None, password: str = None, user_type: UserType = None, project: str = None):
        """Setup MinIO policies for project and users

        Args:
            user (str, optional): user to set. Defaults to None.
            password (str, optional): password for the given user. Defaults to None.
            user_type (UserType, optional): type of the user. Defaults to None.
            project (str, optional): name of the created project. Defaults to None.
        """
        if not self.use_external_s3:
            self.set_alias()
            bucket_name = self.database.config.s3_connector_bucket
            if project is not None:
                users_map = self.database.get_users_map(current_user=self.api_user)
                MinioPolicy._execute_command(cmd='/bin/sh /home/sphn/code/api/app/scripts/create-admin-groups.sh')
                MinioPolicy._execute_command(cmd=f'/bin/sh /home/sphn/code/api/app/scripts/create-standard-groups.sh {bucket_name} {project}/Input/batch_ingestion')
                MinioPolicy._execute_command(cmd=f'/bin/sh /home/sphn/code/api/app/scripts/create-ingestion-groups.sh {bucket_name} {project}/Input/batch_ingestion')

                for user in users_map['admin_users']:
                    if user != self.access_key:
                        groups = ["ADMIN_RO", "ADMIN_RW", "ADMIN_RWD"]
                        for group in groups:
                            MinioPolicy._execute_command(cmd=f"/home/sphn/minio-binaries/mc admin group add sib {group} {user}")
                for user in users_map['standard_users']:
                    groups = [f"{bucket_name}_{project}_Input_batch_ingestion_STANDARD_RO", f"{bucket_name}_{project}_Input_batch_ingestion_STANDARD_RW", f"{bucket_name}_{project}_Input_batch_ingestion_STANDARD_RWD"]
                    for group in groups:
                        MinioPolicy._execute_command(cmd=f"/home/sphn/minio-binaries/mc admin group add sib {group} {user}")
                for user in users_map['ingestion_users']:
                    groups = [f"{bucket_name}_{project}_Input_batch_ingestion_INGESTION_RO", f"{bucket_name}_{project}_Input_batch_ingestion_INGESTION_RW", f"{bucket_name}_{project}_Input_batch_ingestion_INGESTION_RWD"]
                    for group in groups:
                        MinioPolicy._execute_command(cmd=f"/home/sphn/minio-binaries/mc admin group add sib {group} {user}")
            else:
                MinioPolicy._execute_command(cmd=f'/home/sphn/minio-binaries/mc admin user add sib {user} {password}')
                project_names = self.database.get_initialized_projects()
                for project_name in project_names:
                    if user_type == UserType.ADMIN:
                        groups = ["ADMIN_RO", "ADMIN_RW", "ADMIN_RWD"]
                    elif user_type == UserType.STANDARD:
                        groups = [f"{bucket_name}_{project_name}_Input_batch_ingestion_STANDARD_RO", f"{bucket_name}_{project_name}_Input_batch_ingestion_STANDARD_RW", f"{bucket_name}_{project_name}_Input_batch_ingestion_STANDARD_RWD"]
                    else:
                        groups = [f"{bucket_name}_{project_name}_Input_batch_ingestion_INGESTION_RO", f"{bucket_name}_{project_name}_Input_batch_ingestion_INGESTION_RW", f"{bucket_name}_{project_name}_Input_batch_ingestion_INGESTION_RWD"]
                    for group in groups:
                        MinioPolicy._execute_command(cmd=f"/home/sphn/minio-binaries/mc admin group add sib {group} {user}")

    def delete(self, project: str):
        """Delete MinIO policies/groups

        Args:
            project (str): name of the project
        """
        if not self.use_external_s3:
            self.set_alias()
            bucket_name = self.database.config.s3_connector_bucket
            users_map = self.database.get_users_map(current_user=self.api_user)
            groups = [f"{bucket_name}_{project}_Input_batch_ingestion_STANDARD_RO", f"{bucket_name}_{project}_Input_batch_ingestion_STANDARD_RW", f"{bucket_name}_{project}_Input_batch_ingestion_STANDARD_RWD"]
            for group in groups:
                for user in users_map['standard_users']:
                    MinioPolicy._execute_command(cmd=f"/home/sphn/minio-binaries/mc admin group remove sib {group} {user}")
                MinioPolicy._execute_command(cmd=f"/home/sphn/minio-binaries/mc admin group remove sib {group}")
                MinioPolicy._execute_command(cmd=f"/home/sphn/minio-binaries/mc admin policy remove sib {group}")
            groups = [f"{bucket_name}_{project}_Input_batch_ingestion_INGESTION_RO", f"{bucket_name}_{project}_Input_batch_ingestion_INGESTION_RW", f"{bucket_name}_{project}_Input_batch_ingestion_INGESTION_RWD"]
            for group in groups:
                for user in users_map['ingestion_users']:
                    MinioPolicy._execute_command(cmd=f"/home/sphn/minio-binaries/mc admin group remove sib {group} {user}")
                MinioPolicy._execute_command(cmd=f"/home/sphn/minio-binaries/mc admin group remove sib {group}")
                MinioPolicy._execute_command(cmd=f"/home/sphn/minio-binaries/mc admin policy remove sib {group}")

    def delete_user(self, user: str):
        """Remove user from MinIO

        Args:
            user (str): user to remove
        """
        if not self.use_external_s3:
            self.set_alias()
            MinioPolicy._execute_command(cmd=f"/home/sphn/minio-binaries/mc admin user remove sib {user}")

    def set_user_password(self, user: str, password: str):
        """Set the user password if there is a change

        Args:
            user (str): name of the user
            password (str): password to set
        """
        if not self.use_external_s3:
            self.set_alias()
            MinioPolicy._execute_command(cmd=f"/home/sphn/minio-binaries/mc admin user add sib {user} {password}")

    def _execute_command(cmd: str):
        """Execute bash command

        Args:
            cmd (str): command string to execute
        """
        p = subprocess.Popen(cmd.split(' '), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        _, error = p.communicate()
        if error:
            msg = error.decode('utf-8').strip('\n')
            print(f"Error when executing Minio client command '{cmd}':\n{msg}")
