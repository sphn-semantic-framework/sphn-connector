#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'airflow_lib.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from requests.auth import HTTPBasicAuth
import requests
from datetime import timedelta
from fastapi import HTTPException, status, Response
import time
import logging
import uuid
import sys
import os

sys.path.append(os.path.abspath(os.path.dirname(__file__)))
from database import Database
from enums import AirflowDag, CompressionType, PipelineAction, QCSeverityLevel, StatisticsProfile, Steps, LogLevel
from config import Config

logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s', level=logging.getLevelName('INFO'), datefmt='%Y-%m-%d %H:%M:%S')


class Airflow(object):
    """
    Class containing information and methods about Airflow pipelines. It is used by the API to interact with the Airflow infrastructure
    """
    def __init__(self):
        """
        Class constructor
        """
        self.config = Config()
        self.base_url = "http://connector:8080/airflow"
        self.authentication = HTTPBasicAuth(self.config.airflow_user, self.config.airflow_password)

    # Alternatively we need to check the keys individually in the case the "conf" value in the DAG contains other parameters
    def _get_dag_conf(project: str, 
                      dag_id: str, 
                      patient_id: str = None, 
                      validation_log_level: LogLevel = LogLevel.COMPACT, 
                      run_uuid: str = None, 
                      profile: StatisticsProfile = StatisticsProfile.FAST, 
                      validation_status: str = None, 
                      compression_type: CompressionType = None, 
                      skip_downloaded: bool = None, 
                      sphn_base_iri: str = None, 
                      step: Steps = None, 
                      patients_only: bool = False,
                      severity_level: QCSeverityLevel = None, 
                      revalidate: bool = False,
                      config: Config = None) -> dict:
        """Gets the DAG conf parameters

        Args:
            project (str): name of the project
            dag_id (str): ID of the DAG
            patient_id (str, optional): unique ID of the patient. Defaults to None.
            validation_log_level (LogLevel, optional): logging level for QAF validation. Defaults to LogLevel.COMPACT.
            run_uuid (str, optional): UUID of the run. Defaults to None.
            profile (StatisticsProfile, optional): statistics profile. Defaults to StatisticsProfile.FAST.
            validation_status (str, optional): validation status. Defaults to None.
            compression_type (CompressionType, optional): compression type of download bundle. Defaults to None.
            skip_downloaded (bool, optional): skip downloaded files. Defaults to None.
            sphn_base_iri (str, optional): SPHN base IRI. Defaults to None.
            step (Steps, optional): pipeline step. Defaults to None.
            patients_only (bool, optional): download only patient data. Defaults to False.
            severity_level (QCSeverityLevel, optional): QC severity level. Defaults to None.
            revalidate (bool, optional): revalidate all the data. Defaults to False.
            config (Config, optional): configuration object. Defaults to None.

        Returns:
            dict: DAG configurazion JSON
        """
        if dag_id in ['run_shacler', 'run_batch_ingestion']:
            return {"conf": {'project_name': project}}
        elif dag_id == 'run_database_extraction':
            return {"conf": {'project_name': project, "run_uuid": run_uuid}}
        elif dag_id == 'run_notify_einstein':
            return {"conf": {"project_name": project, "patient_id": patient_id, "run_uuid": run_uuid}}
        elif dag_id == 'run_sparqler':
            return {"conf": {'project_name': project, 'sphn_base_iri': sphn_base_iri}}
        elif dag_id == 'run_statistics':
            return {"conf": {"project_name": project, "validation_log_level": validation_log_level.value, "patient_id": patient_id, "run_uuid": run_uuid, "profile": profile.value}}
        elif dag_id == 'run_s3_download':
            return {"conf": {"project_name": project, "patient_id": patient_id, "validation_status": validation_status, "compression_type": compression_type.value, "skip_downloaded": skip_downloaded, "patients_only": patients_only}}
        elif dag_id == 'run_database_backup':
            return {"conf": {'commit_hash': config.commit_hash, 'is_tag': config.is_tag, 'version_name': config.version_name,
                             'commit_timestamp': config.commit_timestamp.strftime("%Y-%m-%d %H:%M:%S %z")}}
        elif dag_id == 'run_real_time_count':
            return {"conf": {'project_name': project, 'step': step.value}}
        else:
            return {"conf": {"project_name": project, "validation_log_level": validation_log_level.value, "patient_id": patient_id, "run_uuid": run_uuid,
                             "severity_level": severity_level.value, "revalidate": revalidate}}

    def get_latest_dag(self, project: str, dag_id: str, patient_id: str = None) -> dict:
        """Return latest ran/running dag

        Args:
            project (str): name of the project
            dag_id (str): ID of the DAG
            patient_id (str, optional): unique ID of the patient. Defaults to None.

        Returns:
            dict: latest Airflow DAG
        """
        endpoint = "{base_url}/api/v1/dags/{dag_id}/dagRuns".format(base_url=self.base_url, dag_id=dag_id)
        response = requests.get(endpoint, auth=self.authentication, params={"order_by": "-execution_date"})
        dags = response.json()["dag_runs"]
        if dags:
            for dag in dags:
                dag_conf = dag["conf"]
                dag_project = dag_conf.get('project_name')
                dag_patient_id = dag_conf.get('patient_id') if dag_conf.get('patient_id') is not None else None
                if str(project) == str(dag_project):
                    if patient_id is None:
                        return dag
                    elif str(patient_id) == str(dag_patient_id):
                        return dag
        return {}

    def is_dag_running(self, dag_id: str, project: str = None):
        """Check if DAG is running

        Args:
            dag_id (str): DAG ID
            project (str, optional): Name of the project. Defaults to None.

        Raises:
            HTTPException: DAG still running
        """
        dag = self.get_latest_dag(project=project, dag_id=dag_id)
        dag_state = dag.get('state')
        if dag_state in ['running', 'queued']:
            if dag_id == 'run_s3_download':
                raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=f"S3 download for project '{project}' is still running")
            elif dag_id == 'run_database_backup':
                raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail="Backup of SPHN Connector database is still running")

    def check_dag_status(self, dag_id: str, project: str = None) -> str:
        """Check the status of the S3 download

        Args:
            dag_id (str): DAG ID
            project (str, optional): Name of the project. Defaults to None.

        Raises:
            HTTPException: extraction still running

        Returns:
            str: no download running
        """
        dag = self.get_latest_dag(project=project, dag_id=dag_id)
        dag_state = dag.get('state')
        if dag_state in ['running', 'queued']:
            if dag_id == 'run_s3_download':
                raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=f"S3 download for project '{project}' is still running")
            elif dag_id == 'run_database_backup':
                raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail="Backup of SPHN Connector database is still running")
            elif dag_id == 'run_database_extraction':
                raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=f"Data extraction for project '{project}' is still running")
            elif dag_id == 'run_batch_ingestion':
                raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=f"Batch ingestion of data for project '{project}' is still running")
        elif dag_state == 'failed':
            if dag_id == 'run_s3_download':
                raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"S3 download for project '{project}' failed. Check Airflow logs")
            elif dag_id == 'run_database_backup':
                raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Backup of SPHN Connector database failed")
            elif dag_id == 'run_database_extraction':
                raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail="Database data extraction failed")
            elif dag_id == 'run_batch_ingestion':
                raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=f"Batch ingestion of data for project '{project}' failed")
        else:
            if dag_id == 'run_s3_download':
                return f"No data download running for project '{project}'"
            elif dag_id == 'run_database_backup':
                return "No backup running"
            elif dag_id == 'run_database_extraction':
                return f"No data extraction running for project '{project}'"
            elif dag_id == 'run_batch_ingestion':
                return f"No batch data upload running for project '{project}'"

    def is_pipeline_running(self, project: str = None) -> bool:
        """Checks if pipelines are running for a specific project

        Args:
            project (str, optional): name of the project. Defaults to None.

        Raises:
            HTTPException: pipeline is running

        Returns:
            bool: pipeline is running or not
        """
        
        dag_ids = ['run_whole_pipeline', 'run_pre_check_and_de_identification', 'run_integration', 'run_validation', 'run_database_extraction', 'run_batch_ingestion']
        for dag_id in dag_ids:
            endpoint = "{base_url}/api/v1/dags/{dag_id}/dagRuns".format(base_url=self.base_url, dag_id=dag_id)
            response = requests.get(endpoint, auth=self.authentication, params={"order_by": "-execution_date"})
            dags = response.json()["dag_runs"]
            for dag in dags:
                dag_project = dag["conf"].get('project_name')
                if (project == dag_project or project is None) and dag['state'] in ['running', 'queued']:
                    if project is None:
                        return_message = "Cannot trigger action because there are some running pipelines"
                    else:
                        return_message = f"Cannot trigger action because project '{project}' has some running pipelines"
                    raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=return_message)
        return False

    def is_sparqler_running(self, project: str):
        """Check if SPARQLer DAG is still running

        Args:
            project (str): name of the project
        """
        dag_id = "run_sparqler"
        endpoint = "{base_url}/api/v1/dags/{dag_id}/dagRuns".format(base_url=self.base_url, dag_id=dag_id)
        response = requests.get(endpoint, auth=self.authentication, params={"order_by": "-execution_date"})
        dags = response.json()["dag_runs"]
        for dag in dags:
            dag_project = dag["conf"].get('project_name')
            if project == dag_project and dag['state'] in ['running', 'queued']:
                return_message = f"SPARQLer is still running for project '{project}'. Wait few minutes and retry..."
                raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=return_message)

    def get_dag(self, dag_id: str, dag_run_id: str) -> str:
        """Gets Airflow DAG

        Args:
            dag_id (str): ID of the DAG
            dag_run_id (str): ID of the DAG run

        Returns:
            str: Airflow DAG
        """
        endpoint = "{base_url}/api/v1/dags/{dag_id}/dagRuns/{dag_run_id}".format(base_url=self.base_url, dag_id=dag_id, dag_run_id=dag_run_id)
        response = requests.get(endpoint, auth=self.authentication)
        return response.json()
    
    @staticmethod
    def _get_error_message(project: str, patient_id: str = None, step: Steps = Steps.ALL, dag_id: str = None) -> str:
        """Returns error message for /Get_status and /Stop_pipeline

        Args:
            project (str): name of the project
            patient_id (str): unique patient ID
            step (Steps): name of the step
            dag_id (str): DAG ID

        Returns:
            str: error message
        """
        if dag_id == 'run_database_backup':
            return "No backup of SPHN Connector database running"
        else:
            message = f"No pipelines found for project '{project}'"
            if patient_id:
                message += f", patient id '{patient_id}'"
            if step != Steps.ALL:
                message += f", step '{step.value}'"
            return message

    def get_return_message(self, 
                           project: str, 
                           pipeline_action: PipelineAction, 
                           patient_id: str = None, 
                           step: Steps = Steps.ALL,
                           run_uuid: str = None, 
                           dag_identifier: str = None) -> str:
        """Gets return message based on pipeline action specified and step specified

        Args:
            project (str): name of the project
            pipeline_action (PipelineAction): pipeline action
            patient_id (str, optional): unique patient ID. Defaults to None.
            step (Steps, optional): name of the step. Defaults to Steps.ALL.
            run_uuid (str, optional): UUID of the run. Defaults to None.
            dag_identifier (str, optional): ID of the DAG. Defaults to None.

        Returns:
            str: return message
        """
        if pipeline_action == PipelineAction.STOPPED:
            if dag_identifier == 'run_database_extraction':
                return f"Extraction of database data for project '{project}' stopped"
            elif dag_identifier == 'run_batch_ingestion':
                return f"Batch upload of data for project '{project}' stopped"
            elif dag_identifier == 'run_s3_download':
                return f"Data download into S3 for project '{project}' stopped"
            elif dag_identifier == 'run_database_backup':
                return "Backup of SPHN Connector database stopped"
            else:
                return f"Processing of data for project '{project}' stopped"
        else:
            if step == step.STATISTICS:
                return {"run_uuid": run_uuid, "message": f"Statistics of project '{project}' {pipeline_action.value}"}
            elif step != Steps.ALL:
                if patient_id is not None:
                    return {"run_uuid": run_uuid, "message": f"Processing of data for project '{project}', patient '{patient_id}', and step '{step.value}' {pipeline_action.value}"}
                else:
                    return {"run_uuid": run_uuid, "message": f"Processing of data for project '{project}' and step '{step.value}' {pipeline_action.value}"}
            else:
                if patient_id is not None:
                    return {"run_uuid": run_uuid, "message": f"Processing of data for project '{project}' and patient '{patient_id}' {pipeline_action.value}"}
                else:
                    return {"run_uuid": run_uuid, "message": f"Processing of data for project '{project}' {pipeline_action.value}"}


    def get_status(self, project: str, step: Steps, database: Database) -> list:
        """Get status of pipelines

        Args:
            project (str): name of the project
            step (Steps): name of the step
            database (Database): database object

        Raises:
            HTTPException: exception when no pipelines are found

        Returns:
            list: list of pipelines with execution details
        """
        pipelines = []
        records = database.get_status(project=project, step=step)
        if records:
            for record in records:
                execution_time = record[7].strftime("%Y-%m-%d %H:%M:%S")
                patients_processed = record[4] if record[4] is not None else 0
                elapsed_time = str(timedelta(seconds=record[8])) if record[8] is not None else None
                if record[3] == 'RUNNING':
                    if step == Steps.PRE_CHECK_DE_ID:
                        patients_processed = database.get_pre_check_de_id_processed_patients(project=project, execution_time=record[7])
                    elif step == Steps.NOTIFY_EINSTEIN:
                        patients_processed = None
                    elif step in [Steps.INTEGRATION, Steps.VALIDATION, Steps.STATISTICS]:
                        self.trigger_dag(project=project, dag_id='run_real_time_count', step=step)
                        patients_processed += database.get_patients_count(project=project, step=step)
                    else:
                        if Steps(record[2]) == Steps.PRE_CHECK_DE_ID:
                            patients_processed = database.get_pre_check_de_id_processed_patients(project=project, execution_time=record[7])
                        elif Steps(record[2]) == Steps.NOTIFY_EINSTEIN:
                            patients_processed = None
                        elif Steps(record[2]) == Steps.INGEST_FROM_DATABASE:
                            patients_processed = None
                        else:
                            self.trigger_dag(project=project, dag_id='run_real_time_count', step=Steps(record[2]))
                            patients_processed += database.get_patients_count(project=project, step=Steps(record[2]))
                    pipelines.append({'run_uuid': record[0], 'project_name': record[1], 'pipeline_step': record[2], 'pipeline_status': record[3], 'patients_processed': patients_processed, 
                                      'patients_with_warnings': record[5], 'patients_with_errors': record[6], 'execution_time': execution_time, "elapsed_time": elapsed_time})
                else:
                    if Steps(record[2]) == Steps.INGEST_FROM_DATABASE:
                        pipelines.append({'run_uuid': record[0], 'project_name': record[1], 'pipeline_step': record[2], 'pipeline_status': record[3],'patients_processed':patients_processed , 'execution_time': execution_time, "elapsed_time": elapsed_time})
                    else:
                        pipelines.append({'run_uuid': record[0], 'project_name': record[1], 'pipeline_step': record[2], 'pipeline_status': record[3], 'patients_processed': patients_processed,
                                'patients_with_warnings': record[5], 'patients_with_errors': record[6], 'execution_time': execution_time, "elapsed_time": elapsed_time})
        if pipelines:
            return pipelines
        else:
            error_message = f"No pipelines found for project '{project}'"
            if step != Steps.ALL:
                error_message += f" and step '{step.value}'"
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=error_message)

    def _get_dag_id(dag_id: str, step: Steps) -> str:
        """Gets DAG ID

        Args:
            dag_id (str): ID of the DAG
            step (Steps): pipeline step

        Raises:
            HTTPException: step not found

        Returns:
            str: ID of the DAG
        """
        if dag_id is not None:
            return dag_id
        else:
            if step == Steps.ALL:
                return 'run_whole_pipeline'
            elif step == Steps.PRE_CHECK_DE_ID:
                return 'run_pre_check_and_de_identification'
            elif step == Steps.INTEGRATION:
                return 'run_integration'
            elif step == Steps.VALIDATION:
                return 'run_validation'
            elif step == Steps.NOTIFY_EINSTEIN:
                return 'run_notify_einstein'
            elif step == Steps.STATISTICS:
                return 'run_statistics'
            else:
                raise HTTPException(status_code=status.HTTP_405_METHOD_NOT_ALLOWED, detail=f"Step '{step.value}' not yet implemented")

    def wait_pipeline_execution(self, project: str, dag_id: str):
        """Wait for pipeline execution and check result

        Args:
            project (str): name of the project
            dag_id (str): ID of the dag

        Raises:
            HTTPException: exception if Airflow task failed
        """
        latest_dag = self.get_latest_dag(project=project, dag_id=dag_id)
        pipeline_status = latest_dag.get('state')
        while pipeline_status is not None and pipeline_status not in ["success", "failed"]:
            time.sleep(1)
            latest_dag = self.get_latest_dag(project=project, dag_id=dag_id)
            pipeline_status = latest_dag.get("state")
        if pipeline_status == "failed":
            if dag_id == 'run_shacler':
                raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail=f"Execution of '{dag_id}' for project '{project}' failed")
        elif pipeline_status == "success":
            if dag_id == 'run_shacler':
                print(f"SHACLER for project '{project}' successfully executed")

    def trigger_dag(self, 
                    project: str, 
                    dag_id: str = None, 
                    patient_id: str = None, 
                    step: Steps = Steps.ALL, 
                    validation_log_level: LogLevel = LogLevel.COMPACT, 
                    profile: StatisticsProfile = StatisticsProfile.FAST, 
                    validation_status: str = None, 
                    compression_type: CompressionType = None, 
                    skip_downloaded: bool = None, 
                    sphn_base_iri: str = None, 
                    patients_only: bool = False, 
                    severity_level: QCSeverityLevel = None, 
                    revalidate: bool = False,
                    config: Config = None) -> str:
        """Trigger Airflow DAG

        Args:
            project (str): name of the project
            dag_id (str, optional): ID of the DAG. Defaults to None.
            patient_id (str, optional): unique ID of the patient. Defaults to None.
            step (Steps, optional): step to trigger. Defaults to Steps.ALL.
            validation_log_level (LogLevel, optional): logging level for QAF validation. Defaults to LogLevel.COMPACT.
            profile (StatisticsProfile, optional): statistics profile. Defaults to StatisticsProfile.FAST.
            validation_status (str, optional): validation status. Defaults to None.
            compression_type (CompressionType, optional): compression type of download bundle. Defaults to None.
            skip_downloaded (bool, optional): skip downloaded files. Defaults to None.
            sphn_base_iri (str, optional): SPHN base IRI. Defaults to None.
            patients_only (bool, optional): download only patient data. Defaults to False.
            severity_level (QCSeverityLevel, optional): QC severity level. Defaults to None.
            revalidate (bool, optional): revalidate all the data. Defaults to False.
            config (Config, optional): configuration object. Defaults to None.

        Returns:
            str: triggering message
        """
        dag_id = Airflow._get_dag_id(dag_id=dag_id, step=step)
        run_uuid = str(uuid.uuid4())
        endpoint = "{base_url}/api/v1/dags/{dag_id}/dagRuns".format(base_url=self.base_url, dag_id=dag_id)
        configs = Airflow._get_dag_conf(project=project, dag_id=dag_id, patient_id=patient_id, validation_log_level=validation_log_level, run_uuid=run_uuid, profile=profile, 
                                        validation_status=validation_status, compression_type=compression_type, skip_downloaded=skip_downloaded, sphn_base_iri=sphn_base_iri,
                                        step=step, patients_only=patients_only, severity_level=severity_level, revalidate=revalidate, config=config)
        requests.post(endpoint, auth=self.authentication, json=configs)
        if dag_id in ['run_shacler', 'run_real_time_count']:
            return self.wait_pipeline_execution(project=project, dag_id=dag_id)
        elif dag_id == 'run_database_extraction':
            return {"run_uuid": run_uuid, "message": f"Extraction of database data for project '{project}' triggered" }     
        elif dag_id == 'run_batch_ingestion':
            return f"Batch ingestion of data for project '{project}' triggered"
        elif dag_id == 'run_s3_download':
            return f"Download of data in S3 bucket triggered. Object will be stored with prefix '{project}/Download/{project}{compression_type.value}'"
        elif dag_id == 'run_database_backup':
            return "Backup of SPHN Connector database triggered. Backup will be stored under 'sphn-connector-backup' prefix in Connector bucket in S3"
        elif dag_id == 'run_sparqler':
            pass
        else:
            return self.get_return_message(project=project, patient_id=patient_id, step=step, pipeline_action=PipelineAction.TRIGGERED, run_uuid=run_uuid)


    def stop_dag(self, project: str = None, dag_identifier: str = None) -> str:
        """Stops Airflow DAG

        Args:
            project (str, optional): name of the project. Defaults to None.
            dag_identifier (str, optional): ID of the DAG. Defaults to None.

        Raises:
            HTTPException: stop process failed

        Returns:
            str: stopping message
        """
        if dag_identifier is not None:
            dag_ids = [dag_identifier]
        else:
            dag_ids = ['run_whole_pipeline', 'run_pre_check_and_de_identification', 'run_integration', 'run_validation', 'run_notify_einstein']
        stopped = False
        for dag_id in dag_ids:
            latest_dag = self.get_latest_dag(project=project, dag_id=dag_id)
            if latest_dag:
                if latest_dag.get('state') in ['running','queued']:
                    endpoint = "{base_url}/api/v1/dags/{dag_id}/dagRuns/{dag_run_id}".format(base_url=self.base_url, dag_id=dag_id, dag_run_id=latest_dag["dag_run_id"])
                    requests.patch(endpoint, auth=self.authentication, json={"state": "failed"})
                    stopped = True
        if stopped:
            return self.get_return_message(project=project, pipeline_action=PipelineAction.STOPPED, dag_identifier=dag_identifier)
        else:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=Airflow._get_error_message(project=project, dag_id=dag_identifier))

    def get_extra_running_projects_count(self, project: str) -> int:
        """Get number of concurrent projects running

        Args:
            project (str): name of the project

        Returns:
            int: number of concurrent projects
        """
        concurrent_projects = set()
        dag_ids = ['run_whole_pipeline', 'run_pre_check_and_de_identification', 'run_integration', 'run_validation', 'run_statistics']
        for dag_id in dag_ids:
            endpoint = "{base_url}/api/v1/dags/{dag_id}/dagRuns".format(base_url=self.base_url, dag_id=dag_id)
            response = requests.get(endpoint, auth=self.authentication, params={"order_by": "-execution_date"})
            dags = response.json()["dag_runs"]
            for dag in dags:
                dag_project = dag["conf"].get('project_name')
                if dag_project is not None and str(project) != str(dag_project) and dag['state'] in ['running', 'queued']:
                    concurrent_projects.add(str(dag_project))
        return len(list(concurrent_projects))

    def check_concurrent_projects(self, project: str):
        """Check if the number of running/queued projects already is at its maximum

        Args:
            project (str): name of the project

        Raises:
            HTTPException: exception if max numer of projects are running
        """
        number_of_concurrent_projects = self.get_extra_running_projects_count(project=project)
        if number_of_concurrent_projects >= self.config.parallel_projects:
            raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=f"Project '{project}' cannot be triggered becuase there are already {self.config.parallel_projects} projects in running/queued state")
    
    def get_concurrent_projects_msg(self, project: str) -> str:
        """Extract message if there are other projects currently running

        Args:
            project (str): name of the project

        Returns:
            str: concurrent projects warning message
        """
        concurrent_projects = self.get_extra_running_projects_count(project=project)
        if concurrent_projects == 0:
            return None
        elif concurrent_projects == 1:
            return f"WARNING -- Step triggered while {str(concurrent_projects)} other project is in running state. Running concurrent projects could lead to performance and memory issues."
        else:
            return f"WARNING -- Step triggered while {str(concurrent_projects)} other projects are in running state. Running concurrent projects could lead to performance and memory issues."

    def get_logs(self, project: str, dag: AirflowDag) -> Response:
        """Get Airflow logs for debugging

        Args:
            project (str): name of the project
            dag (AirflowDag): specific DAG

        Returns:
            Response: log file
        """
        if dag == AirflowDag.ALL:
            dag_ids = ['run_batch_ingestion', 'run_database_backup', 'run_database_extraction', 'run_grafana_count', 'run_integration', 'run_s3_download', 
                       'run_notify_einstein', 'run_pre_check_and_de_identification', 'run_real_time_count', 'run_shacler', 'run_sparqler', 'run_statistics', 
                       'run_validation', 'run_whole_pipeline']
        else:
            dag_ids = [dag.value]
        airflow_logs = []
        for dag_id in dag_ids:
            airflow_logs.append("#"*100 + "\n")
            airflow_logs.append(f"Logs for dag_id '{dag_id}'\n")
            dags = requests.get(f"{self.base_url}/api/v1/dags/{dag_id}/dagRuns", auth=self.authentication, params={"order_by": "-execution_date"}).json()["dag_runs"]
            dag_exists = False
            for dag in dags:
                dag_project = dag["conf"].get('project_name')
                if str(dag_project) == str(project):
                    dag_exists = True
                    dag_run_id = dag['dag_run_id']
                    state = dag['state']
                    airflow_logs.append(f"Dag run: '{dag_run_id}'\n")
                    airflow_logs.append(f"Dag run state: '{state}'\n")
                    airflow_logs.append("#"*100 + "\n\n")
                    task_instances = requests.get(f"{self.base_url}/api/v1/dags/{dag_id}/dagRuns/{dag_run_id}/taskInstances", auth=self.authentication).json()['task_instances']
                    filtered_task_instances = [item for item in task_instances if item['queued_when'] is not None]
                    for task in sorted(filtered_task_instances, key=lambda x: x['queued_when']):
                        task_id = task['task_id']
                        task_try_number = task['try_number']
                        airflow_logs.append("*"*100 + "\n")
                        airflow_logs.append(f"Task id: '{task_id}'\n")
                        response = requests.get(f"{self.base_url}/api/v1/dags/{dag_id}/dagRuns/{dag_run_id}/taskInstances/{task_id}/logs/{task_try_number}", 
                                                auth=self.authentication)
                        airflow_logs.append("Logs: " + response.text.lstrip('connector').rstrip("\n") + "\n")
                        airflow_logs.append("*"*100 + "\n\n")
                    break
            if not dag_exists:
                airflow_logs.append("#"*100 + "\n\n")
        headers = {'Content-Disposition': f'attachment; filename="{project}_airflow_logs.log"'}
        return Response("".join(airflow_logs), headers=headers)