#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'rdf_parser.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from typing import Union
from rdflib import URIRef
from rdflib.namespace import OWL, RDF
import lightrdf
from fastapi import HTTPException, status
import io
from enums import OutputFormat


def validate_rdf_file(content: bytes, filename: str = None):
    """Validates RDF file

    Args:
        content (bytes): RDF file content
        filename (str): name of the file

    Raises:
        HTTPException: exception for invalid RDF file
    """
    try:
        parser = lightrdf.Parser()
        for _ in parser.parse(io.BytesIO(content), base_iri=None, format="turtle"):
            continue
    except Exception:
        if filename is not None:
            raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=f"Invalid RDF file provided '{filename}'")
        else:
            raise

def get_schema_iri(content: bytes) -> str:
    """Return schema IRI

    Args:
        content (bytes): schema content

    Returns:
        str: schema IRI
    """
    doc = lightrdf.RDFDocument(io.BytesIO(content), parser=lightrdf.turtle.PatternParser)
    for o, _, _ in doc.search_triples(None, RDF.type, OWL.Ontology):
        return str(o).lstrip('<').rstrip('>')

def validate_external_terminology(content: bytes, filename: str) -> str:
    """Validate terminology file and update table in database

    Args:
        content (bytes): RDF file content
        filename (str): name of the file

    Raises:
        HTTPException: exception for invalid RDF file

    Returns:
        str: version IRI of the external terminology
    """
    
    try:
        version_iri = None
        doc = lightrdf.RDFDocument(io.BytesIO(content), parser=lightrdf.turtle.PatternParser)
        for _, _, o in doc.search_triples(None, OWL.versionIRI, None):
            version_iri = str(o)
        return version_iri
    except Exception:
        raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=f"Invalid RDF file provided '{filename}'")
    
def check_sphn_import(content: bytes) -> str:
    """Check that SPHN ontology is in the list of OWL:Imports

    Args:
        content (bytes): project schema data

    Raises:
        HTTPException: SPHN import missing

    Returns:
        str: SPHN IRI
    """
    doc = lightrdf.RDFDocument(io.BytesIO(content), parser=lightrdf.turtle.PatternParser)
    for _, _, o in doc.search_triples(None, OWL.imports, None):
        iri = str(o).lstrip('<').rstrip('>')
        if iri.startswith('https://biomedit.ch/rdf/sphn-ontology/sphn/') or iri.startswith('https://biomedit.ch/rdf/sphn-schema/sphn/'):
            return iri
    raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail="SPHN schema IRI is not in the list of owl:imports of the project schema")

def check_imports_match(content: bytes, sphn_imported_iri: str):
    """Check that the IRI listed in the owl:imports matches the SPHN owl:versionIRI

    Args:
        content (bytes): SPHN schema data
        sphn_imported_iri (str): SPHN imported IRI

    Raises:
        HTTPException: mismatch between IRIs
    """
    doc = lightrdf.RDFDocument(io.BytesIO(content), parser=lightrdf.turtle.PatternParser)
    for _, _, o in doc.search_triples(None, OWL.versionIRI, None):
        iri = str(o).lstrip('<').rstrip('>')
        if iri == sphn_imported_iri:
            return
    raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=f"Imported schema version '{sphn_imported_iri}' does not match SPHN schema owl:versionIRI")

def get_named_graph_uri(content: bytes, sphn_base_iri: str, data_provider_id: str, patient_id: str) -> str:
    """Get patient identifier to use as named graph URI

    Args:
        content (bytes): patient data
        sphn_base_iri (str): SPHN schema base IRI
        data_provider_id (str): data provider ID
        patient_id (str): ID of the patient

    Returns:
        str: named graph URI (sphn:SubjectPseudoIdentifier)
    """
    doc = lightrdf.RDFDocument(io.BytesIO(content), parser=lightrdf.turtle.PatternParser)
    named_graph_uri = None
    for triple in doc.search_triples(None, RDF.type, URIRef(f'{sphn_base_iri}#SubjectPseudoIdentifier')):
        return str(triple[0])
    if named_graph_uri is None:
        return f'<https://biomedit.ch/rdf/sphn-resource/{data_provider_id}-sphn-SubjectPseudoIdentifier-{patient_id}>'

def convert_to_quads(content: bytes, sphn_base_iri: str, output_format: OutputFormat, data_provider_id: str, patient_id: str) -> Union[io.BytesIO, str]:
    """Convert turtle file in nquads

    Args:
        content (bytes): patient data
        sphn_base_iri (str): SPHN schema base IRI
        output_format (OutputFormat): project output format
        data_provider_id (str): data provider ID
        patient_id (str): ID of the patient

    Returns:
        Union[io.BytesIO, str]: patient data serialized in nquads or trig, graph named IRI
    """
    named_graph_uri = get_named_graph_uri(content=content, sphn_base_iri=sphn_base_iri, data_provider_id=data_provider_id, patient_id=patient_id)
    output_graph = io.BytesIO()
    if output_format == OutputFormat.NQUADS:
        parser = lightrdf.Parser()
        for s, p, o in parser.parse(io.BytesIO(content), format='turtle'):
            output_graph.write(f"{s} {p} {o} {named_graph_uri} .\n".encode('utf-8'))
    else:
        # Find the last instantiated prefix line
        last_prefix_index = content.rfind(b'@prefix')

        # Set prefix index to 0 if no prefixes found on the file
        if last_prefix_index == -1:
            last_prefix_index = 0

        # Extract the last prefix line
        last_prefix_line = content[last_prefix_index:content.find(b'\n', last_prefix_index)].strip()
        
        # Use 'resource:' prefix if defined in the input file
        if b'@prefix resource: <https://biomedit.ch/rdf/sphn-resource/> .' in content:
            named_graph_uri_cleaned = named_graph_uri.strip('<>').replace('https://biomedit.ch/rdf/sphn-resource/', 'resource:').encode('utf-8')
        else:
            named_graph_uri_cleaned = named_graph_uri.encode('utf-8')
        
        # Insert named graph after the last prefix and closing curly bracket at the end
        trig_content = (content[:last_prefix_index]
                        + last_prefix_line 
                        + b'\n\n'
                        + named_graph_uri_cleaned
                        + b' {'
                        + content[last_prefix_index + len(last_prefix_line):]
                        + b'\n}')

        output_graph.write(trig_content)

    return output_graph, named_graph_uri.lstrip('<').rstrip('>')