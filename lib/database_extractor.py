#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'database_extractor.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from copy import deepcopy
import json
from typing import Union
from fastapi import HTTPException, status
from datetime import datetime
import io
import os
import sys
from psycopg2 import sql
from concurrent.futures import ProcessPoolExecutor, as_completed

sys.path.append(os.path.abspath(os.path.dirname(__file__)))
from database import Database
from enums import ConceptType, DataPersistenceLayer, FileClass, FileType, IngestionType, Steps, PipelineStep
from rml_generator_lib import niceify_prefix
from api import get_object_name

class DatabaseExtractor(Database):
    """
    Class defining methods and logic to extract data from database and convert it to JSON format
    """
    def __init__(self, project: str):
        """Class constructor

        Args:
            project (str): name of the project
        """
        super().__init__(project=project)
        self.project = project
        self.tables_definition = {}
        self.supporting_tables_definition = {}
        self.supporting_tables_with_dpi = []
        self.project_extended_prefix = self.get_project_extended_prefix(project=project)

    def get_patients_to_process(self, data_provider_class: str) -> list:
        """Extract from table "project".sphn_DataRelease' the ids of the patients which are ready to be processed

        Args:
            project (str): project name
            data_provider_class (str): data provider class

        Returns:
            list: list of patients in the format (patient_id, creation_time)
        """
        self.create_connection(database_name='sphn_tables')
        query = sql.SQL('SELECT "sphn_hasSubjectPseudoIdentifier__id", "sphn_hasExtractionDateTime" FROM {project}.{table_name} WHERE {data_provider} = %s')\
                .format(project=sql.Identifier(self.project), table_name=sql.Identifier("sphn_DataRelease"), data_provider=sql.Identifier(f"sphn_has{data_provider_class}__id"))
        self.cursor.execute(query, (self.config.data_provider_id, ))
        records = self.cursor.fetchall()
        released_patients = []
        for record in records:
            released_patients.append((record[0], record[1]))
        self.close_connection()
        self.create_connection()
        query = sql.SQL("SELECT patient_id, creation_time FROM {layer} WHERE project_name=%s and processed=True and creation_time IS NOT NULL").format(layer=sql.Identifier(DataPersistenceLayer.LANDING_ZONE.value))
        self.cursor.execute(query, (self.project, ))
        records = self.cursor.fetchall()
        processed_patients = []
        for record in records:
            processed_patients.append((record[0], record[1]))
        self.close_connection()
        patients_to_process = []
        for patient in released_patients:
            if patient not in processed_patients:
                patients_to_process.append(patient)
        return patients_to_process

    def check_patients_data(self, project_name: str):
        """Checks that patient data exists, if not raises exception

        Args:
            project_name (str): name of the project

        Raises:
            HTTPException: no patient data available, pull won't be triggered
        """
        sphn_base_iri = self.get_sphn_base_iri(project=project_name)
        data_provider_class = Database._get_data_provider_class(sphn_base_iri=sphn_base_iri)
        patients_to_process = self.get_patients_to_process(data_provider_class=data_provider_class)
        if not patients_to_process:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"No new patients to process found on '{project_name}.sphn_DataRelease' table for project '{project_name}'")

    @staticmethod
    def _cleanup_before_extraction(project_name: str):
        """Cleanup landing zones before extraction and ingestion

        Args:
            project_name (str): name of the project
        """
        database_extractor = DatabaseExtractor(project=project_name)
        sphn_base_iri = database_extractor.get_sphn_base_iri(project=project_name)
        data_provider_class = Database._get_data_provider_class(sphn_base_iri=sphn_base_iri)
        database_extractor.project = project_name
        patients_to_process = database_extractor.get_patients_to_process(data_provider_class=data_provider_class)
        database_extractor.create_connection()
        for patient in patients_to_process:
            database_extractor.cleanup_before_ingestion(project_name=project_name, patient_id=patient[0], parallel=True)
        database_extractor.close_connection()

    def extract_patient(self, patient_id: str, data_provider_class: str, is_legacy: bool):
        """Extract patient data and load it into S3

        Args:
            patient_id (str): ID of the patient
            data_provider_class (str): data provider class
            is_legacy (bool): schema is older than 2024
        """
        bucket = self.config.s3_connector_bucket
        json_data = self.extract_json_data(patient_id=patient_id, data_provider_class=data_provider_class, is_legacy=is_legacy)
        content=json.dumps(json_data, indent=4).encode()
        file_size = io.BytesIO(content).getbuffer().nbytes
        object_name = get_object_name(project_name=self.project, data_provider_id=self.config.data_provider_id, file_class=FileClass.PATIENT_DATA, patient_id=patient_id, file_type=FileType.JSON, filename=IngestionType.DATABASE)
        self.config.s3_client.put_object(Bucket=bucket, Key=object_name, Body=io.BytesIO(content), ContentLength=file_size)

    def chunks(self, patient_ids):
        """Split patient IDs into chunks

        Args:
            patient_ids (list): List of patient IDs

        Yields:
            dict: Chunk data with chunk ID and list of patients
        """
        patient_ids = [patient[0] for patient in patient_ids]
        if len(patient_ids) % self.config.machine_cpus == 0:
            size = int(len(patient_ids) / self.config.machine_cpus)
        else:
            size = int(len(patient_ids) / self.config.machine_cpus) + 1
        for i in range(1, len(patient_ids) + 1, size):
            chunk_patients = patient_ids[i - 1:i - 1 + size]
            yield chunk_patients


    @staticmethod
    def _extract_database_data(project_name: str, run_uuid: str):
        """Extract data from database and convert it to JSON

        Args:
            project_name (str): name of the project
            run_uuid (str): UUID of the run
        """
        database_extractor = DatabaseExtractor(project=project_name)
        sphn_base_iri = database_extractor.get_sphn_base_iri(project=project_name)
        data_provider_class = Database._get_data_provider_class(sphn_base_iri=sphn_base_iri)
        is_legacy = Database._is_legacy_schema(sphn_base_iri=sphn_base_iri)
        database_extractor.project = project_name
        start_timestamp = datetime.now()
        patients_to_process = database_extractor.get_patients_to_process(data_provider_class=data_provider_class)
        database_extractor.update_history_table(project_name=project_name, pipeline_step=Steps.INGEST_FROM_DATABASE, pipeline_status=PipelineStep.RUNNING, patients_processed=None, patients_with_warnings=None, patients_with_errors=None, 
                                       execution_time=start_timestamp, elapsed_time=None, run_uuid=run_uuid)
        if not patients_to_process:
            database_extractor.update_history_table(project_name=project_name, pipeline_step=Steps.INGEST_FROM_DATABASE, pipeline_status=PipelineStep.SKIPPED, patients_processed=None, patients_with_warnings=None, patients_with_errors=None, 
                                       execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)
        chunks = database_extractor.chunks(patient_ids=patients_to_process)
        tables_definition, supporting_tables_definition = database_extractor.generate_tables_definition(data_provider_class=data_provider_class, is_legacy=is_legacy)
        supporting_tables_with_dpi = Database._get_tables_with_dpi(project_name=project_name)
        with ProcessPoolExecutor(max_workers=database_extractor.config.machine_cpus) as executor:
            futures = []
            for _, chunk in enumerate(chunks):
                futures.append(executor.submit(extract_chunks_data, chunk, project_name, tables_definition, supporting_tables_definition, supporting_tables_with_dpi, data_provider_class, is_legacy))
            for future in as_completed(futures):
                future.result()
        database_extractor.update_history_table(project_name=project_name, pipeline_step=Steps.INGEST_FROM_DATABASE, pipeline_status=PipelineStep.SUCCESS, patients_processed=len(patients_to_process), patients_with_warnings=None, patients_with_errors=None, 
                                       execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)

    def update_patient_metadata(self, patient_id: str, creation_time: datetime):
        """Updates patient metadata

        Args:
            patient_id (str): ID of the patient
            creation_time (datetime): creation time of the patient
        """
        object_name = get_object_name(project_name=self.project, data_provider_id=self.config.data_provider_id, file_class=FileClass.PATIENT_DATA, patient_id=patient_id, file_type=FileType.JSON, filename=IngestionType.DATABASE)
        query = sql.SQL("INSERT INTO {layer} (project_name, patient_id, data_provider_id, object_name, file_type, timestmp, ingestion_type, processed, creation_time) VALUES (%s, %s, %s, %s, %s, %s, %s, False, %s) \
                ON CONFLICT (project_name, patient_id, data_provider_id) DO UPDATE SET object_name=EXCLUDED.object_name, file_type=EXCLUDED.file_type, timestmp=EXCLUDED.timestmp, ingestion_type=EXCLUDED.ingestion_type, processed=EXCLUDED.processed, creation_time=EXCLUDED.creation_time") \
        .format(layer=sql.Identifier(str(DataPersistenceLayer.LANDING_ZONE.value)))
        values = (self.project, patient_id, self.config.data_provider_id, object_name, FileType.JSON.value, datetime.now(), IngestionType.DATABASE.value, creation_time)
        self.cursor.execute(query, values)


    @staticmethod
    def _update_database_metadata(project_name: str):
        """Updates landing_zone table and creation time of the uploaded patients

        Args:
            project_name (str): name of the project
        """
        database_extractor = DatabaseExtractor(project=project_name)
        sphn_base_iri = database_extractor.get_sphn_base_iri(project=project_name)
        data_provider_class = Database._get_data_provider_class(sphn_base_iri=sphn_base_iri)
        database_extractor.project = project_name
        patients_to_process = database_extractor.get_patients_to_process(data_provider_class=data_provider_class)
        database_extractor.create_connection()
        for patient in patients_to_process:
            database_extractor.update_patient_metadata(patient_id=patient[0], creation_time=patient[1])
        database_extractor.close_connection()

    @staticmethod
    def _update_influenced_tables(targets: dict, table_definition: list):
        """Update influenced tables

        Args:
            targets (dict): targets map
            table_definition (list): table definition list
        """
        sphn_base_iri = 'https://biomedit.ch/rdf/sphn-schema/sphn#'
        if targets:
            filtered_targets = {}
            for element_col, element_targets in targets.items():
                sphn_targets = [el for el in element_targets if el.startswith(sphn_base_iri)]
                if len(sphn_targets) == 1:
                    filtered_targets[element_col] = sphn_targets[0]

            for filtered_col, filtered_target in filtered_targets.items():
                for definition in table_definition:
                    if definition['json_column_name'].startswith(filtered_col) and len(definition['json_column_name'].replace(f"{filtered_col}::", "").split('::')) == 1:
                        definition['target_concept'] = filtered_target

    def generate_tables_definition(self, data_provider_class: str, is_legacy: bool) -> Union[dict, dict]:
        """Generate tables definition map based on JSON schema of the project

        Args:
            data_provider_class (str): data provider class
            is_legacy (bool): schema is older than 2024

        Returns:
            Union[dict, dict]: tables definition maps
        """
        tables_definition = {}
        supporting_tables_definition = {}
        sphn_concepts, supporting_concepts, SPHN_ONLY_sphn_concepts = self.get_sphn_concepts(data_provider_class=data_provider_class)
        if SPHN_ONLY_sphn_concepts:
            standard_concepts = SPHN_ONLY_sphn_concepts
            project_extended_concepts = sphn_concepts
        else:
            standard_concepts = sphn_concepts
            project_extended_concepts = []

        influenced_core_targets = {}

        for core_content in project_extended_concepts:
            table_definition = []
            if core_content not in standard_concepts:
                for core_key, core_value in core_content.items():
                    core_targets = {}
                    DatabaseExtractor._extract_columns(db_col_names=[], json_col_names=[], property_map=core_value, table_definition=table_definition, table_name=core_key, 
                                                    concept_type=ConceptType.CORE, is_legacy=is_legacy, table_targets=core_targets)
                    if core_key.split(':')[-1] not in [data_provider_class, 'SubjectPseudoIdentifier', 'DataRelease']:
                        tables_definition[self.project_extended_prefix + core_key] = table_definition
                    else:
                        tables_definition[core_key] = table_definition
                    influenced_core_targets[core_key] = core_targets

        for core_content in standard_concepts:
            table_definition = []
            for core_key, core_value in core_content.items():
                DatabaseExtractor._extract_columns(db_col_names=[], json_col_names=[], property_map=core_value, table_definition=table_definition, table_name=core_key,
                                                concept_type=ConceptType.CORE, is_legacy=is_legacy)
                if project_extended_concepts:
                    DatabaseExtractor._update_influenced_tables(targets=influenced_core_targets.get(core_key, {}), table_definition=table_definition)
                tables_definition[core_key] = table_definition

        for concept in supporting_concepts:
            table_definition = []
            for core_key, core_value in concept.items():
                supporting_targets = {}
                DatabaseExtractor._extract_columns(db_col_names=[], json_col_names=[], property_map=core_value, table_definition=table_definition, table_name=core_key,
                                                   concept_type=ConceptType.SUPPORTING, is_legacy=is_legacy, table_targets=supporting_targets)
                supporting_tables_definition[core_key] = table_definition

        return tables_definition, supporting_tables_definition

    def get_sphn_concepts(self, data_provider_class: str) -> Union[list, list]:
        """Extract SPHN concepts from JSON schema

        Args:
            data_provider_class (str): data provider class

        Returns:
            list: list of SPHN concept maps and supporting concepts
        """
        bucket = self.config.s3_connector_bucket
        json_schema_name = f'{self.project}/RML/{self.project}_json_schema.json'
        response = self.config.s3_client.get_object(Bucket=bucket, Key=json_schema_name)['Body'].read().decode('utf8')
        json_schema = json.loads(response)
        sphn_concepts = []
        supporting_concepts = []
        SPHN_ONLY_sphn_concepts = []
        for property in json_schema['properties']:
            if property.split(':')[-1] in [data_provider_class, 'SubjectPseudoIdentifier', 'DataRelease']:
                sphn_concepts += [{property: json_schema['properties'][property]}]
        for key, value in json_schema['properties']['content']['properties'].items():
            sphn_concepts += [{key: value['items']}]
        if 'supporting_concepts' in json_schema['properties']:
            for key, value in json_schema['properties']['supporting_concepts']['properties'].items():
                supporting_concepts += [{key: value['items']}]

        try:
            SPHN_ONLY_json_schema_name = f'{self.project}/RML/SPHN_ONLY_{self.project}_json_schema.json'
            SPHN_ONLY_response = self.config.s3_client.get_object(Bucket=bucket, Key=SPHN_ONLY_json_schema_name)['Body'].read().decode('utf8')
            SPHN_ONLY_json_schema = json.loads(SPHN_ONLY_response)
            sphn_only = True
        except Exception:
            sphn_only = False

        if sphn_only:
            for key, value in SPHN_ONLY_json_schema['properties']['content']['properties'].items():
                SPHN_ONLY_sphn_concepts += [{key: value['items']}]
        return sphn_concepts, supporting_concepts, SPHN_ONLY_sphn_concepts

    @ staticmethod
    def _extract_columns(db_col_names: list, 
                         json_col_names: list, 
                         property_map: dict, 
                         table_definition: list, 
                         table_name: str, 
                         concept_type: ConceptType, 
                         is_legacy: bool, 
                         table_targets: dict = {}, 
                         in_array_field: bool = False, 
                         target_concept: str = None, 
                         in_one_of: bool = False, 
                         datatype_array: bool = False):
        """Extract concepts columns definition

        Args:
            db_col_names (list): column names for the DB table
            json_col_names (list): column names of the JSON output data
            property_map (dict): map of the property
            table_definition (list): table definition of properties
            table_name (str): name of the table
            concept_type (ConceptType): concept type
            is_legacy (bool): schema is older than 2024
            table_targets (dict): target concepts collection
            in_array_field (bool, optional): field is boolean. Defaults to False.
            target_concept (str, optional): target concept value. Defaults to None.
            datatype_array (bool, optional): property defines an array of datatypes values. Defaults to False.
        """
        if property_map['type'] == 'object':
            if 'oneOf' in property_map:
                for element in property_map['oneOf']:
                    DatabaseExtractor._extract_columns(db_col_names=db_col_names, json_col_names=json_col_names, property_map=element, table_definition=table_definition, 
                                                       table_name=table_name, concept_type=concept_type, is_legacy=is_legacy, table_targets=table_targets)
            else:
                target_concept_defined = "target_concept" in property_map["properties"]
                one_of_in_array = in_array_field and in_one_of
                if target_concept_defined:
                    enums = property_map["properties"]["target_concept"]["enum"]
                    target_json_col_name = '::'.join([col for col in json_col_names])
                    if target_json_col_name not in table_targets:
                        table_targets[target_json_col_name] = []
                    table_targets[target_json_col_name] += enums
                    table_targets[target_json_col_name] = list(set(table_targets[target_json_col_name]))
                    pseudoprefix, classref = niceify_prefix(enums[0])
                    target_reference = classref if pseudoprefix == 'sphn' else pseudoprefix + classref
                for key, value in property_map['properties'].items():
                    if target_concept_defined and len(enums) == 1:
                        if key != "target_concept":
                            DatabaseExtractor._extract_columns(db_col_names=db_col_names + [target_reference] + [key], json_col_names=json_col_names + [key], property_map=value, table_definition=table_definition, 
                                                               table_name=table_name, concept_type=concept_type, is_legacy=is_legacy, table_targets=table_targets, target_concept=enums[0], in_array_field=one_of_in_array)
                    else:
                        DatabaseExtractor._extract_columns(db_col_names=db_col_names + [key], json_col_names=json_col_names + [key], property_map=value, table_definition=table_definition,
                                                           table_name=table_name, concept_type=concept_type, is_legacy=is_legacy, table_targets=table_targets, in_array_field=one_of_in_array)
        elif property_map['type'] == 'array':
            if 'oneOf' in property_map['items']:
                for element in property_map['items']['oneOf']:
                    DatabaseExtractor._extract_columns(db_col_names=db_col_names, json_col_names=json_col_names, property_map=element, table_definition=table_definition,
                                                       table_name=table_name, concept_type=concept_type, is_legacy=is_legacy, table_targets=table_targets, in_array_field=True, in_one_of=True)
            else:
                if property_map['items']['type'] == 'object':
                    for key, value in property_map['items']['properties'].items():
                        DatabaseExtractor._extract_columns(db_col_names=db_col_names + [key], json_col_names=json_col_names + [key], property_map=value, table_definition=table_definition,
                                                           table_name=table_name, concept_type=concept_type, is_legacy=is_legacy, table_targets=table_targets, in_array_field=True)
                else:
                    DatabaseExtractor._extract_columns(db_col_names=db_col_names, json_col_names=json_col_names, property_map=property_map['items'], table_definition=table_definition,
                                                       table_name=table_name, concept_type=concept_type, is_legacy=is_legacy, table_targets=table_targets, in_array_field=True, datatype_array=True)
        else:
            name_of_column = '::'.join([col for col in db_col_names])
            if concept_type == ConceptType.CORE and name_of_column.endswith('::sourceConceptID'):
                return
            data_name_of_column = '::'.join([col for col in json_col_names])
            if 'format' in property_map:
                column_type = property_map['format']
            else:
                column_type = property_map['type']
            if not is_legacy and concept_type == ConceptType.CORE and 'description' in property_map and property_map['description'] in ["Unique ID for the given IRI. String format follows convention: <coding_system>-<identifier>", "ID of SPHN Concept 'Code'"]:
                # Define sourceConceptID column only for non-legacy core concepts
                source_concept_id = '::'.join([column for column in name_of_column.split('::')[:-2]])
                if source_concept_id:
                    source_concept_id += '::id'
                else:
                    source_concept_id = 'id'
                json_source_concept_id_column = '::'.join([column for column in data_name_of_column.split('::')[:-1]]) + '::sourceConceptID'
            else:
                source_concept_id = None
                json_source_concept_id_column = None
            table_definition.append({'db_column_name': name_of_column, "json_column_name": data_name_of_column,"column_type": column_type, "array": in_array_field, "target_concept": target_concept,
                                     'pattern': property_map.get('pattern'), 'source_concept_id': source_concept_id, 'json_source_concept_id_column': json_source_concept_id_column,
                                     "datatype_array": datatype_array})

    def unify_subject_pseudo_identifier(self, json_data: dict):
        """Unify sphn:SubjectPseudoIdentifier into a single dictionary

        Args:
            json_data (dict): patient data
        """
        table_name = 'sphn:SubjectPseudoIdentifier'
        json_table_name = table_name.replace(self.project_extended_prefix, '')
        subject_pseudo_data = deepcopy(json_data.get(json_table_name, []))
        if subject_pseudo_data:
            shared_ids = []
            for subject_element in subject_pseudo_data:
                if subject_element.get('sphn:hasSharedIdentifier'):
                    shared_ids += subject_element.get('sphn:hasSharedIdentifier')
            json_data[json_table_name] = subject_pseudo_data[0]
            if shared_ids:
                json_data[json_table_name]['sphn:hasSharedIdentifier'] = shared_ids
            
    def extract_json_data(self, patient_id: str, data_provider_class: str, is_legacy: bool) -> dict:
        """Extract data and generate JSON content

        Args:
            patient_id (str): ID of the patient
            data_provider_class (str): data provider class
            is_legacy (bool): schema is older than 2024

        Returns:
            dict: JSON data
        """
        json_data = {"content": {}}
        tables = list(self.tables_definition.keys())
        supporting_tables = list(self.supporting_tables_definition.keys())
        for table_name in tables:
            json_table_name = table_name.replace(self.project_extended_prefix, '')
            data_records = self.get_column_values(table_name=table_name, patient_id=patient_id, concept_type=ConceptType.CORE, data_provider_class=data_provider_class, is_legacy=is_legacy)
            for data_record in data_records:
                if table_name.split(':')[-1] == 'SubjectPseudoIdentifier':
                    if json_table_name not in json_data:
                        json_data[json_table_name] = []
                    json_data[json_table_name].append(DatabaseExtractor._get_json_data(data_record=data_record, table_name=table_name))
                elif table_name.split(':')[-1] not in [data_provider_class, 'DataRelease']:
                    if json_table_name not in json_data['content']:
                        json_data['content'][json_table_name] = []
                    json_data['content'][json_table_name].append(DatabaseExtractor._get_json_data(data_record=data_record, table_name=table_name))
                else:
                    # Assumption: only one record for these tables
                    json_data[json_table_name] = DatabaseExtractor._get_json_data(data_record=data_record, table_name=table_name)

        if supporting_tables:
            json_data["supporting_concepts"] = {}
        for table_name in supporting_tables:
            json_table_name = table_name
            data_records = self.get_column_values(table_name=table_name, patient_id=patient_id, concept_type=ConceptType.SUPPORTING, data_provider_class=data_provider_class, is_legacy=is_legacy)
            for data_record in data_records:
                if table_name.split(':')[-1] not in [data_provider_class, 'SubjectPseudoIdentifier', 'DataRelease']:
                    if json_table_name not in json_data['supporting_concepts']:
                        json_data['supporting_concepts'][json_table_name] = []
                    json_data['supporting_concepts'][json_table_name].append(DatabaseExtractor._get_json_data(data_record=data_record, table_name=table_name))
                else:
                    # Assumption: only one record for these tables
                    json_data[json_table_name] = DatabaseExtractor._get_json_data(data_record=data_record, table_name=table_name)
        
        self.unify_subject_pseudo_identifier(json_data=json_data)

        return json_data

    def get_column_values(self, table_name: str, patient_id: str, concept_type: ConceptType, data_provider_class: str, is_legacy: bool) -> list:
        """Get values from database for table's columns

        Args:
            table_name (str): name of the table
            patient_id (str): ID of the patient
            concept_type (ConceptType): concept tpe
            data_provider_class (str): data provider class
            is_legacy (bool): schema is older than 2024

        Returns:
            list: columns values
        """
        if concept_type == ConceptType.CORE:
            columns = self.tables_definition[table_name]
        else:
            columns = self.supporting_tables_definition[table_name]
        col_list = sql.SQL(', ').join([sql.Identifier(col['db_column_name'].replace(':', '_')) for col in columns])
        if table_name.replace(self.project_extended_prefix, '') == f'sphn:{data_provider_class}':
            query = sql.SQL("SELECT {col_list} FROM {project}.{table_name} WHERE id=%s").format(col_list=col_list, 
                                                                                         project=sql.Identifier(self.project),
                                                                                         table_name=sql.Identifier(table_name.replace(':', '_')))
            self.cursor.execute(query, (self.config.data_provider_id, ))
        elif table_name.replace(self.project_extended_prefix, '') == 'sphn:SubjectPseudoIdentifier':
            query = sql.SQL("SELECT {col_list} FROM {project}.{table_name} WHERE id=%s and {data_provider}=%s").format(col_list=col_list, 
                                                                                         project=sql.Identifier(self.project),
                                                                                         table_name=sql.Identifier(table_name.replace(':', '_')),
                                                                                         data_provider=sql.Identifier(f"sphn_has{data_provider_class}__id"))
            self.cursor.execute(query, (patient_id, self.config.data_provider_id))
        else:
            if concept_type == ConceptType.CORE:
                if is_legacy:
                    query = sql.SQL("SELECT {col_list} FROM {project}.{table_name} WHERE \"sphn_hasSubjectPseudoIdentifier__id\"=%s and {data_provider}=%s").format(col_list=col_list, 
                                                                                                 project=sql.Identifier(self.project),
                                                                                                 table_name=sql.Identifier(table_name.replace(':', '_')),
                                                                                                 data_provider=sql.Identifier(f"sphn_has{data_provider_class}__id"))
                    self.cursor.execute(query, (patient_id, self.config.data_provider_id))
                else:
                    if table_name.replace(self.project_extended_prefix, '').replace(':', '_') == 'sphn_SourceSystem':
                        query = sql.SQL("SELECT {col_list} FROM {project}.{table_name} WHERE {data_provider}=%s AND (patient_id =%s OR patient_id IS NULL)").format(col_list=col_list, 
                                                                                                project=sql.Identifier(self.project),
                                                                                                table_name=sql.Identifier(table_name.replace(':', '_')),
                                                                                                data_provider=sql.Identifier(f"sphn_has{data_provider_class}__id"))
                        self.cursor.execute(query, (self.config.data_provider_id, patient_id))
                    elif table_name.replace(self.project_extended_prefix, '').replace(':', '_') == 'sphn_DataRelease':
                        query = sql.SQL("SELECT {col_list} FROM {project}.{table_name} WHERE \"sphn_hasSubjectPseudoIdentifier__id\"=%s and {data_provider}=%s").format(col_list=col_list, 
                                                                                                 project=sql.Identifier(self.project),
                                                                                                 table_name=sql.Identifier(table_name.replace(':', '_')),
                                                                                                 data_provider=sql.Identifier(f"sphn_has{data_provider_class}__id"))
                        self.cursor.execute(query, (patient_id, self.config.data_provider_id))
                    else:
                        query = sql.SQL("SELECT {col_list} FROM {project}.{table_name} WHERE \"sphn_hasSubjectPseudoIdentifier__id\"=%s").format(col_list=col_list, 
                                                                                                 project=sql.Identifier(self.project),
                                                                                                 table_name=sql.Identifier(table_name.replace(':', '_')))
                        self.cursor.execute(query, (patient_id, ))
            else:
                if table_name.replace(self.project_extended_prefix, '').replace(':', '_') in self.supporting_tables_with_dpi:
                    query = sql.SQL("SELECT {col_list} FROM {project}.{table_name} WHERE {data_provider}=%s AND (patient_id =%s OR patient_id IS NULL)").format(col_list=col_list, 
                                                                                                project=sql.Identifier(self.project),
                                                                                                table_name=sql.Identifier("supporting__" + table_name.replace(':', '_')),
                                                                                                data_provider=sql.Identifier(f"sphn_has{data_provider_class}__id"))
                    self.cursor.execute(query, (self.config.data_provider_id, patient_id))
                else:
                    query = sql.SQL("SELECT {col_list} FROM {project}.{table_name} WHERE patient_id =%s OR patient_id IS NULL").format(col_list=col_list, 
                                                                                            project=sql.Identifier(self.project),
                                                                                            table_name=sql.Identifier("supporting__" + table_name.replace(':', '_')))
                    self.cursor.execute(query, (patient_id, ))

        records = self.cursor.fetchall()
        table_data = []
        for record in records:
            column_values = []
            record_map = dict(zip( [col['db_column_name'] for col in columns], record))
            for col, value in zip(columns, record):
                if value is not None:
                    if col['source_concept_id'] is not None:
                        # Add extra column to defined sourceConceptID field
                        column_values.append({'json_column_name': col['json_source_concept_id_column'],'column_value': record_map[col['source_concept_id']], 'array': col['array'], 'target_concept': col['target_concept'],
                                              'datatype_array': col['datatype_array']})
                    if col['column_type'] == 'date-time':
                        value = str(value).replace(' ', 'T')
                    elif col['column_type'] in ['time', 'date']:
                        value = str(value)
                    elif col['column_type'] == 'number':
                        value = float(value)
                    elif col['pattern'] is not None:
                        value = DatabaseExtractor._get_value_with_pattern(value=value, pattern=col['pattern'])
                    column_values.append({'db_column_name': col['db_column_name'], 'json_column_name': col['json_column_name'],'column_value': value, 'array': col['array'], 'target_concept': col['target_concept'],
                                          'datatype_array': col['datatype_array']})
            table_data.append(column_values)

        return table_data

    @staticmethod
    def _get_json_data(data_record: list, table_name : str) -> dict:
        """
        Extract data in JSON format. The JSON structure is constructed
        recursively by generating and accessing the nesting on a Python dictionary

        Args:
            data_record (list): database data record
            table_name (str): name of the table

        Returns:
            dict: JSON data
        """
        table_dict = {table_name: {}}
        for col_map in data_record:
            # Define the nested key from the column name
            keys = col_map['json_column_name'].split('::')
            # Define the max level of nesting by counting the extracted keys
            last_key = len(keys)
            # Set pointer to table key before processing the data record
            current = table_dict[table_name]
            for i_key, key in enumerate(keys, start=1):
                if i_key == last_key and col_map['column_value'] is not None:
                    # When the last level of nesting is reached, assign the value to that key

                    if col_map['datatype_array']:
                        if key not in current:
                            current[key] = []
                        current[key].append(col_map['column_value'])
                    else:
                        current[key] = col_map['column_value']
                    if 'target_concept' in col_map and col_map['target_concept'] is not None:
                        current['target_concept'] = col_map['target_concept']
                else:
                    if key not in current:
                        # Define the key in the map if not already defined
                        if col_map['array']:
                            # For key related to an array field define the key as list with a map inside
                            current[key] = [{}]
                        else:
                            # For non-array key, define the key as a map
                            current[key] = {}
                    # Recursively shift to the next level of nesting by assigning the current map pointer to the key value
                    if isinstance(current[key],list):
                        current = current[key][0]
                    else:
                        current = current[key]
        return table_dict[table_name]
    
    @staticmethod
    def _get_value_with_pattern(value: int, pattern: str) -> str:
        """Return string value for properties with pattern defined

        Args:
            value (int): numeric value
            pattern (str): pattern

        Returns:
            str: string value reflecting pattern
        """
        # Values inserted into the database should be positive integers, for security we take the absolute value here
        str_value = str(int(abs(value)))
        if pattern == "^(-)?[0-9][0-9][0-9][0-9](Z|[+-]14:00|[+-](?:[0][0-9]|[1][0-3]):[0-5][0-9])?$":
            return "-" + str_value.zfill(4) if value < 0 else str_value.zfill(4)
        elif pattern == "^(--[0][1-9]|--[1][0-2])(Z|[+-]14:00|[+-](?:[0][0-9]|[1][0-3]):[0-5][0-9])?$":
            return "--"  + str_value.zfill(2)
        elif pattern == "^(---[0][1-9]|---[1][0-9]|---[2][0-9]|---[3][0-1])(Z|[+-]14:00|[+-](?:[0][0-9]|[1][0-3]):[0-5][0-9])?$":
            return "---" + str_value.zfill(2)


def extract_chunks_data(chunk: list, 
                        project_name: str, 
                        tables_definition: dict, 
                        supporting_tables_definition: dict, 
                        supporting_tables_with_dpi: list, 
                        data_provider_class: str, 
                        is_legacy: bool):
    """Extract chunk data from database

    Args:
        chunk (list): chunk of patient IDs
        project_name (str): name of the project
        tables_definition (dict): tables definition map
        supporting_tables_definition (dict): supporting tables definition map
        supporting_tables_with_dpi (list): list of supporting concepts with property sphn:hasDataProvider
        data_provider_class (str): data provider class
        is_legacy (bool): schema is older than 2024
    """
    database_extractor = DatabaseExtractor(project=project_name)
    database_extractor.project = project_name
    database_extractor.tables_definition = tables_definition
    database_extractor.supporting_tables_definition = supporting_tables_definition
    database_extractor.supporting_tables_with_dpi = supporting_tables_with_dpi
    database_extractor.create_connection(database_name='sphn_tables')
    for patient in chunk:
        database_extractor.extract_patient(patient_id=patient, data_provider_class=data_provider_class, is_legacy=is_legacy)
    database_extractor.close_connection()

