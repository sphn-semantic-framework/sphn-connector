#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'sparqler.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

import glob
import os.path
import shutil
from config import Config
import io
from pathlib import Path
from sparqler_external import main as SPARQLer
from database import bucket_exists, Database

class SparqlGenerator(object):
    """
    Class containing information and methods about generation logic for SPARQL queries
    """
    def __init__(self):
        """
        Class constructor
        """
        self.config = Config()

    @staticmethod
    def _cleanup_data(project_name: str):
        """Cleanup data before starting validation

        Args:
            project_name (str): name of the project
        """
        folder = os.path.join("/home/sphn/sparqler", project_name)
        if os.path.exists(folder):
            shutil.rmtree(folder)

    @staticmethod
    def _download_schemas(project_name: str):
        """Download schema files

        Args:
            project_name (str): name of the project
        """
        sparql_generator = SparqlGenerator()
        database = Database(project=project_name)
        bucket = sparql_generator.config.s3_connector_bucket
        project_folder = os.path.join("/home/sphn/sparqler", project_name)
        os.makedirs(project_folder, exist_ok=True)
        if bucket_exists(bucket=bucket, config=sparql_generator.config):
            schema_file = f"{project_name}/Schema/{project_name}_schema.ttl"
            external_schemas = database.list_objects_keys(bucket=bucket, prefix=f"{project_name}/QAF/Schemas/")
            sparql_generator.config.s3_client.download_file(Bucket=bucket, Key=schema_file, Filename=os.path.join(project_folder,'project_schema.ttl'))
            for external_schema in external_schemas:
                sparql_generator.config.s3_client.download_file(Bucket=bucket, Key=external_schema, Filename=os.path.join(project_folder,'external_schema.ttl'))
                break

    @staticmethod 
    def _get_encounters_query(sphn_base_iri: str) -> str:
        """Returns AdministrativeCase encounters query

        Args:
            sphn_base_iri (str): SPHN base IRI
        
        Returns:
            str: SPARQL query
        """
        return f'''PREFIX sphn: <{sphn_base_iri}#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
SELECT ?concept ?administrativeCase
WHERE {{
    ?resource sphn:hasAdministrativeCase ?administrativeCase .
    ?resource rdf:type ?concept .
}}
GROUP BY ?concept ?administrativeCase
'''

    @staticmethod
    def _generate_sparql_queries(project_name: str, sphn_base_iri: str):
        """Generate SPARQL queries

        Args:
            project_name (str): name of the project
            sphn_base_iri (str): SPHN base IRI
        """
        sparql_generator = SparqlGenerator()
        bucket = sparql_generator.config.s3_connector_bucket
        project_folder = os.path.join("/home/sphn/sparqler", project_name)
        project_schema = os.path.join(project_folder, 'project_schema.ttl')
        external_schema = os.path.join(project_folder, 'external_schema.ttl')
        if os.path.exists(external_schema):
            SPARQLer(FileName=[Path(external_schema), Path(project_schema)], output=project_folder, sphn_connector=True)
        else:
            SPARQLer(FileName=[Path(project_schema)], output=project_folder, sphn_connector=True)
        sparql_queries_prefix = f"{project_name}/Statistics/Queries"
        for directory in ["hasCode", "count_instances", "min_max_predicates"]:
            for file in glob.glob(os.path.join(project_folder, directory, '*')):
                object_name = os.path.join(sparql_queries_prefix, os.path.basename(file))
                with open(file, 'rb') as query_file:
                    bytes_content = io.BytesIO(query_file.read())
                    bytes_content.seek(0)
                    file_size = bytes_content.getbuffer().nbytes
                    sparql_generator.config.s3_client.put_object(Bucket=bucket, Key=object_name, Body=bytes_content, ContentLength=file_size)
        object_name = os.path.join(sparql_queries_prefix, "administrative-case-encounters.rq")
        bytes_content= io.BytesIO(bytes(SparqlGenerator._get_encounters_query(sphn_base_iri=sphn_base_iri), 'utf-8'))
        bytes_content.seek(0)
        file_size = bytes_content.getbuffer().nbytes
        sparql_generator.config.s3_client.put_object(Bucket=bucket, Key=object_name, Body=bytes_content, ContentLength=file_size)
