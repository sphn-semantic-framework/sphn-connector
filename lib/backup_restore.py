#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'backup_restore.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

import decimal
import json
import os
import io
import sys
import traceback
import zipfile
from psycopg2 import sql
from fastapi import HTTPException, status
from datetime import datetime, date, time
from io import BytesIO
from enums import UserType
from api import create_s3_bucket

sys.path.append(os.path.abspath(os.path.dirname(__file__)))
from minio_policy import MinioPolicy
from database import Database


class BackupRestore(object):
    """
    Class containing information and methods to backup/restore SPHN Connector
    """
    def __init__(self) -> None:
        """Class constructor
        """
        self.database = Database()
        self.minio_policy = MinioPolicy(database=self.database)
    
    def backup_database(self, api_config: dict):
        """Backup Postgres database data

        Args:
            api_config (dict): configuration object from API container
        """
        data_found = False
        self.database.create_connection()
        sql = "SELECT project_name FROM configuration"
        self.database.cursor.execute(sql)
        projects = [project[0] for project in self.database.cursor.fetchall()]
        self.database.close_connection()

        bytes_buffer = BytesIO()
        zip_file = zipfile.ZipFile(bytes_buffer, mode='w', compression=zipfile.ZIP_DEFLATED)
        
        for i_project, project in enumerate(projects):
            sphn_tables_insert_statement = self.get_sphn_tables_insert_statements(project=project)
            if sphn_tables_insert_statement:
                zip_file.writestr(f'sphn_tables_{project}.sql', sphn_tables_insert_statement.encode())
                data_found = True
            if i_project == 0:
                internal_tables = self.get_internal_tables_insert_statements()
                if internal_tables:
                    zip_file.writestr('internal_tables.sql', internal_tables.encode())
                    data_found = True
        
        # Extract DDL creation statements
        ddl_statements = [f'CREATE SCHEMA "{project_name}";' for project_name in projects]
        self.database.create_connection()
        
        types_sql = "SELECT ddl_statements FROM ddl_generation WHERE object_type='TYPE'"
        self.database.cursor.execute(types_sql)
        types = self.database.cursor.fetchall()
        if types:
            ddl_statements += [record[0] for record in types]
        
        tables_sql = "SELECT ddl_statements FROM ddl_generation WHERE object_type='TABLE'"
        self.database.cursor.execute(tables_sql)
        tables = self.database.cursor.fetchall()
        if tables:
            ddl_statements += [record[0] for record in tables]

        if ddl_statements:
            zip_file.writestr('ddl_creation_statements.sql', '\n'.join(ddl_statements))
            data_found = True
        self.database.close_connection()

        version_details = {'commit_hash': api_config['commit_hash'], 'is_tag': api_config['is_tag'], 'version_name': api_config['version_name'],
                           'commit_timestamp': api_config['commit_timestamp']}
        zip_file.writestr('version.json', json.dumps(version_details, indent=4))

        zip_file.close()

        if data_found:
            backup_data = bytes_buffer.getvalue()
            current_datetime = datetime.now()
            formatted_datetime = current_datetime.strftime("%Y-%m-%dT%H:%M:%S")
            zip_filename = f"sphn-connector-backup/SPHN_Connector_Database_Backup-{formatted_datetime}.zip"
            file_size = io.BytesIO(backup_data).getbuffer().nbytes
            self.database.config.s3_client.put_object(Bucket=self.database.config.s3_connector_bucket, Key=zip_filename, Body=io.BytesIO(backup_data), ContentLength=file_size)
        else:
            print("No data found in SPHN Connector database")
    
    def check_migration_steps(self, buffer: io.BytesIO):
        """Check if there are any necessary migration steps needed

        Args:
            buffer (io.BytesIO): backup file data

        Raises:
            HTTPException: restore not possible
        """
        with zipfile.ZipFile(buffer) as zip_ref:
            for zipinfo in zip_ref.infolist():
                zip_filename = zipinfo.filename.split('/', 1)[-1]
                if zip_filename == 'version.json':
                    with zip_ref.open(zipinfo) as extracted_file:
                        file_content = extracted_file.read()
                        backup_version_data = json.loads(file_content.decode('utf-8'))
                        break
        commit_datetime = datetime.strptime(backup_version_data["commit_timestamp"], "%Y-%m-%d %H:%M:%S %z")
        print(f"SPHN Connector current commit timestamp: {self.database.config.commit_timestamp}")
        print(f"Backup commit timestamp: {commit_datetime}")
        if commit_datetime > self.database.config.commit_timestamp:
            raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=f"SPHN Connector current commit '{self.database.config.commit_timestamp}' is older than backup commit '{commit_datetime}'. Restore not possible...")
        else:
            pass

    def restore_tables(self, zipinfo: zipfile.ZipInfo, zip_ref: zipfile.ZipFile):
        """Restore database tables from ZIP data

        Args:
            zipinfo (zipfile.ZipInfo): ZIP info object
            zip_ref (zipfile.ZipFile): ZIP ref object
        """
        with zip_ref.open(zipinfo) as extracted_file:
            file_content = extracted_file.read()
            self.database.cursor.execute(file_content)
    
    def restore_database(self, content: bytes) -> str:
        """Restore database data

        Args:
            content (bytes): backup data

        Returns:
            str: completion message
        """
        self.is_connector_empty()
        bytes_buffer = io.BytesIO(content)
        self.check_migration_steps(buffer=bytes_buffer)
        with zipfile.ZipFile(bytes_buffer) as zip_ref:
            for zipinfo in zip_ref.infolist():
                zip_filename = zipinfo.filename.split('/', 1)[-1]
                if zip_filename == 'ddl_creation_statements.sql':
                    self.database.create_connection(database_name='sphn_tables')
                    self.restore_tables(zipinfo=zipinfo, zip_ref=zip_ref)
                    self.database.close_connection()

            for zipinfo in zip_ref.infolist():
                zip_filename = zipinfo.filename.split('/', 1)[-1]
                if zip_filename == 'internal_tables.sql':
                    self.database.create_connection()
                    self.restore_tables(zipinfo=zipinfo, zip_ref=zip_ref)
                    self.database.close_connection()

            for zipinfo in zip_ref.infolist():
                zip_filename = zipinfo.filename.split('/', 1)[-1]
                if zip_filename.startswith('sphn_tables_'):
                    self.database.create_connection(database_name='sphn_tables')
                    self.restore_tables(zipinfo=zipinfo, zip_ref=zip_ref)
                    self.database.close_connection()
        
        create_s3_bucket(bucket=self.database.config.s3_connector_bucket, config=self.database.config)
        create_s3_bucket(bucket=self.database.config.s3_einstein_bucket, config=self.database.config)
        self.setup_users_access()

        return "SPHN Connector database restored"

    def setup_users_access(self):
        """Configure S3 and DB access roles for imported users
        """
        self.database.create_connection()
        sql = "SELECT project_name FROM configuration"
        self.database.cursor.execute(sql)
        projects = [project[0] for project in self.database.cursor.fetchall()]
        self.database.close_connection()

        try:
            for user, password in self.database.get_credentials(user_type=UserType.INGESTION).items():
                if user != self.database.config.api_user:
                    user_type = self.database.get_user_type(user=user)
                    self.minio_policy.create(user=user, password=password, user_type=user_type)
                    self.database.grant_permissions(user=user, password=password, user_type=user_type)

            for project in projects:
                try:
                    self.database.grant_permissions(project=project)
                    self.minio_policy.create(project=project)
                except Exception:
                    print(f"Exception for project '{project}':")
                    traceback.print_exc()
        except Exception:
            print("Exception when configurig user accesses to S3 and DB:")
            traceback.print_exc()


    def get_sphn_tables_insert_statements(self, project: str) -> str:
        """Generates insert statements for data on SPHN_TABLES database for a given project

        Args:
            project (str): name of the project

        Returns:
            str: insert statements
        """
        table_names = self.get_sphn_table_names(project=project)
        self.database.create_connection(database_name='sphn_tables')
        insert_statements = []
        for table_name in table_names:
            self.database.cursor.execute(sql.SQL("SELECT * FROM {project}.{table_name}").format(project=sql.Identifier(project), table_name=sql.Identifier(table_name)))
            records = self.database.cursor.fetchall()
            if records:
                column_names = [desc[0] for desc in self.database.cursor.description]
                for record in records:
                    record = [BackupRestore._convert_type(rec) for rec in record]
                    filtered_record = []
                    filtered_columns = []
                    for rec, col in zip(record, column_names):
                        if rec is not None:
                            filtered_record.append(rec)
                            filtered_columns.append(f'"{col}"')
                    insert_statements.append(f'INSERT INTO "{project}"."{table_name}" ({",".join(filtered_columns)}) VALUES {tuple(filtered_record)};')
        self.database.close_connection()
        return '\n'.join(insert_statements)

    def get_internal_tables_insert_statements(self) -> str:
        """Generates insert statements for SPHN Connector internal tables on PostgreSQL

        Returns:
            str: insert statements for internal tables
        """
        table_names = self.get_internal_table_names()
        self.database.create_connection()
        insert_statements = []
        for table_name in table_names:
            self.database.cursor.execute(sql.SQL('SELECT * FROM "public".{table_name}').format(table_name=sql.Identifier(table_name)))
            records = self.database.cursor.fetchall()
            if records:
                column_names = [desc[0] for desc in self.database.cursor.description]
                for record in records:
                    record = [BackupRestore._convert_type(rec) for rec in record]
                    filtered_record = ""
                    filtered_columns = ""
                    for rec, col in zip(record, column_names):
                        if rec is not None:
                            if isinstance(rec, str):
                                rec = rec.replace("'", "''")
                                rec = f"'{rec}'"
                            filtered_record += f"{rec},"
                            filtered_columns += f"{col},"
                    insert_statements.append(f'INSERT INTO "public".{table_name} ({filtered_columns.rstrip(",")}) VALUES ({filtered_record.rstrip(",")});')
        self.database.close_connection()
        return '\n'.join(insert_statements)

    def get_sphn_table_names(self, project: str) -> list:
        """Extract table names from SPHN_TABLES database for a given project

        Args:
            project (str): name of the project

        Returns:
            list: list of table names
        """
        self.database.create_connection(database_name='sphn_tables')
        sql = "SELECT table_name FROM information_schema.tables WHERE table_schema = %s"
        self.database.cursor.execute(sql, (project, ))
        table_names = [table_name[0] for table_name in self.database.cursor.fetchall()]
        self.database.close_connection()
        return table_names

    def get_internal_table_names(self):
        """Extract table names from SPHN Connector internal tables on PostgreSQL. Table 'api_credentials' is skipped.

        Returns:
            list: list of table names
        """
        self.database.create_connection()
        sql = "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'"
        self.database.cursor.execute(sql)
        table_names = [table_name[0] for table_name in self.database.cursor.fetchall()]
        self.database.close_connection()
        return table_names

    @staticmethod
    def _convert_type(value: object) -> object:
        """Convert value to correct type

        Args:
            value (object): value of column

        Returns:
            object: converted value
        """
        if isinstance(value, datetime) or isinstance(value, date) or isinstance(value, time):
            return str(value)
        elif isinstance(value, decimal.Decimal):
            return float(value)
        else:
            return value

    def is_connector_empty(self):
        """Check that no data is present on the SPHN Connector

        Raises:
            HTTPException: exception raised when data is present
        """
        table_names = self.get_internal_table_names() + ['api_credentials']
        self.database.create_connection()
        for table_name in table_names:
            self.database.cursor.execute(sql.SQL("SELECT * FROM {table_name}").format(table_name=sql.Identifier(table_name)))
            records = self.database.cursor.fetchall()
            if records:
                raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=f"SPHN Connector database is not empty. Internal table '{table_name}' contains data. SPHN Connector database must be completely empty in order to be restored")
        self.database.close_connection()