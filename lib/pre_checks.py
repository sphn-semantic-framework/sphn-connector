#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'pre_checks.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from copy import deepcopy
import traceback
import json
import re
from typing import Union
from jsonschema import Draft202012Validator
from jsonschema import FormatChecker
import sys
import os
import rfc3987
from datetime import datetime
sys.path.append(os.path.abspath(os.path.dirname(__file__)))
from enums import PreCheckLevel, PreCheckStatus, PreCheckType
from rdf_parser import validate_rdf_file
from config import Config

class PreCheck(object):
    """
    Super class for the Pre-Checks
    """
    def __init__(self, project: str, config: Config, level: PreCheckLevel, check_name: str):
        """Class constructor

        Args:
            project (str): name of the project
            config (Config): config object
            level (PreCheckLevel): pre-check level
            check_name (str): check name
        """
        self.project = project
        self.config = config
        self.level = level
        self.check_name = check_name

    @staticmethod
    def _date_checker(value: str) -> bool:
        """Check regex for string with date format

        Args:
            value (str): field value

        Returns:
            bool: match found
        """
        date_regex = r"^\d\d\d\d-(0?[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])?(Z|[+-](?:[0][0-9]|[1][0-4]):[0-5][0-9])?$"
        return bool(re.match(date_regex, value))

    @staticmethod
    def _time_checker(value: str) -> bool:
        """Check regex for string with time format

        Args:
            value (str): field value

        Returns:
            bool: match found
        """
        time_regex = r"^(?:[01]\d|2[0-4]):(?:[0-5]\d):(?:[0-5]\d)(?:[.][0-9]{3})?(Z|[+-](?:[0][0-9]|[1][0-4]):[0-5][0-9])?$"
        return bool(re.match(time_regex, value))

    @staticmethod
    def _date_time_checker(value: str) -> bool:
        """Check regex for string with date-time format

        Args:
            value (str): field value

        Returns:
            bool: match found
        """
        date_time_regex = r"^(?:[0-9]{4}-[0-9]{2}-[0-9]{2})(?:[T])(?:[01]\d|2[0-4]):(?:[0-5]\d):(?:[0-5]\d)(?:[.][0-9]{3})?(Z|[+-](?:[0][0-9]|[1][0-4]):[0-5][0-9])?$"
        return bool(re.match(date_time_regex, value))
    
    @staticmethod
    def _double_checker(value: str) -> bool:
        """Check regex for string representing double value

        Args:
            value (str): field value

        Returns:
            bool: match found
        """
        try:
            float(value)
            return True
        except:
            return False

    @staticmethod
    def _g_day_checker(value: str) -> bool:
        """Check regex for gDay string

        Args:
            value (str): field value

        Returns:
            bool: match found
        """
        g_day_regex = r"^(---[0][1-9]|---[1][0-9]|---[2][0-9]|---[3][0-1])(Z|[+-]14:00|[+-](?:[0][0-9]|[1][0-3]):[0-5][0-9])?$"
        return bool(re.match(g_day_regex, value))

    @staticmethod
    def _g_month_checker(value: str) -> bool:
        """Check regex for gMonth string

        Args:
            value (str): field value

        Returns:
            bool: match found
        """
        g_month_regex = r"^(--[0][1-9]|--[1][0-2])(Z|[+-]14:00|[+-](?:[0][0-9]|[1][0-3]):[0-5][0-9])?$"
        return bool(re.match(g_month_regex, value))

    @staticmethod
    def _g_year_checker(value: str) -> bool:
        """Check regex for gYear string

        Args:
            value (str): field value

        Returns:
            bool: match found
        """
        g_year_regex = r"^(-)?[0-9][0-9][0-9][0-9](Z|[+-]14:00|[+-](?:[0][0-9]|[1][0-3]):[0-5][0-9])?$"
        return bool(re.match(g_year_regex, value))

    @staticmethod
    def _transverse_data(key_path: str, data: dict) -> Union[dict, str]:
        """Transverse data object

        Args:
            key_path (str): key path
            data (dict): data object

        Returns:
            Union[dict, str]: map of the last key, last key name
        """
        current = data
        extracted_keys = key_path.split('/')
        for key in extracted_keys[:-1]:
            if isinstance(current, list):
                key = int(key)
            current = current[key]
        return current, extracted_keys[-1]

class PreCheckResult(object):
    """
    Class defining Pre-Check Result object
    """
    def __init__(self, status: PreCheckStatus, check: PreCheck, start_time: datetime, execution_time: int, message: str = None):
        """Class constructor

        Args:
            status (PreCheckStatus): status of the exectution
            check (PreCheck): check object
            start_time (datetime): start time of the check
            execution_time (int): execution time in seconds
            message (str, optional): check exit message. Defaults to None.
        """
        self.status = status
        self.level = check.level
        if self.status == PreCheckStatus.SUCCESS:
            if message is not None:
                self.message = f"Pre-Check '{check.check_name}' of type '{check.type.value}' PASSED. Start time: {start_time}. Execution time: {execution_time}. {message}"
            else:
                self.message = f"Pre-Check '{check.check_name}' of type '{check.type.value}' PASSED. Start time: {start_time}. Execution time: {execution_time}"
        else:
            self.message = f"Pre-Check '{check.check_name}' of type '{check.type.value}' FAILED with severity level '{self.level.value.lower()}'. Start time: {start_time}. Execution time: {execution_time}. Error message: {message}"


class ReplaceCharsCheck(PreCheck):
    """
    Class defining check of type 'replaceCharsCheck'
    """
    def __init__(self, applies_to_field: str, replacement_map: dict, **kwargs):
        """Class constructor

        Args:
            applies_to_field (str): field to which the check is applied
            replacement_map (dict): replacement map
        """
        super().__init__(**kwargs)
        self.applies_to_field = applies_to_field
        self.replacement_map = replacement_map
        self.type = PreCheckType.REPLACE_CHARS_CHECK

    def execute(self, patient_data: dict, keys: list) -> PreCheckResult:
        """Execute check

        Args:
            patient_data (dict): patient data
            keys (list): list of data keys

        Returns:
            PreCheckResult: object with results of execution
        """
        try:
            start_time = datetime.now()
            filtered_keys = [key for key in keys if re.sub(r"(\/[0-9]*\/)", "/", key).endswith(self.applies_to_field)]
            self.replace_characters(data=patient_data, key_paths=filtered_keys)
            return PreCheckResult(status=PreCheckStatus.SUCCESS, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds)
        except Exception:
            return PreCheckResult(status=PreCheckStatus.FAILED, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds, message=traceback.format_exc())

    def replace_characters(self, data: dict, key_paths: list):
        """Loop through the data and replace characters

        Args:
            data (dict): patient data
            key_paths (list): list of key paths to update
            patient_id (str): patient id
        """
        for key_path in key_paths:
            current, last_key = PreCheck._transverse_data(key_path=key_path, data=data)
            for char_to_replace, replacement in self.replacement_map.items():
                if isinstance(current[last_key], list):
                    current[last_key] = [element.replace(char_to_replace, replacement) for element in current[last_key]]
                else:
                    current[last_key] = current[last_key].replace(char_to_replace, replacement)

class ReplaceRegexCheck(PreCheck):
    """
    Class defining check of type 'replaceRegexCheck'
    """
    def __init__(self, applies_to_field: str, regex: str, replacement: str, **kwargs):
        """Class constructor

        Args:
            applies_to_field (str): field to which the check is applied
            regex (str): regex expression of chars to replace
            replacement (str): character replacement
        """
        super().__init__(**kwargs)
        self.applies_to_field = applies_to_field
        self.regex = regex
        self.replacement = replacement
        self.type = PreCheckType.REPLACE_REGEX_CHECK

    def execute(self, patient_data: dict, keys: list) -> PreCheckResult:
        """Execute check

        Args:
            patient_data (dict): patient_data
            keys (list): list of data keys

        Returns:
            PreCheckResult: object with results of execution
        """
        try:
            start_time = datetime.now()
            filtered_keys = [key for key in keys if re.sub(r"(\/[0-9]*\/)", "/", key).endswith(self.applies_to_field)]
            self.replace_characters(data=patient_data, key_paths=filtered_keys)
            return PreCheckResult(status=PreCheckStatus.SUCCESS, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds)
        except Exception:
            return PreCheckResult(status=PreCheckStatus.FAILED, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds, message=traceback.format_exc())

    def replace_characters(self, data: dict, key_paths: list):
        """Loop through the data and replace according to regex expression

        Args:
            data (dict): patient data
            key_paths (list): list of key paths to update
        """
        for key_path in key_paths:
            current, last_key = PreCheck._transverse_data(key_path=key_path, data=data)
            if isinstance(current[last_key], list):
                current[last_key] = [re.sub(self.regex, self.replacement, element) for element in current[last_key]]
            else:
                current[last_key] = re.sub(self.regex, self.replacement, current[last_key])

class RegexCheck(PreCheck):
    """
    Class defining check of type 'regexCheck'

    """
    def __init__(self, applies_to_field: str, regex: str, **kwargs):
        """Class constructor

        Args:
            applies_to_field (str): field to which the check is applied
            regex (str): regex expression to check
        """        
        super().__init__(**kwargs)
        self.applies_to_field = applies_to_field
        self.regex = regex
        self.type = PreCheckType.REGEX_CHECK
        self.check_result = None

    def execute(self, patient_data: dict, keys: list) -> PreCheckResult:
        """Execute check

        Args:
            patient_data (dict): patient_data
            keys (list): list of data keys

        Returns:
            PreCheckResult: object with results of execution
        """
        self.check_result = None
        try:
            start_time = datetime.now()
            filtered_keys = [key for key in keys if re.sub(r"(\/[0-9]*\/)", "/", key).endswith(self.applies_to_field)]
            self.apply_regex(data=patient_data, key_paths=filtered_keys, start_time=start_time)
            if self.check_result is not None:
                return self.check_result
            else:
                return PreCheckResult(status=PreCheckStatus.SUCCESS, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds)
        except Exception:
            return PreCheckResult(status=PreCheckStatus.FAILED, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds, message=traceback.format_exc())

    def apply_regex(self, data: dict, key_paths: list, start_time: datetime):
        """Loop through data and check the regex expression

        Args:
            data (dict): patient data
            key_paths (list): list of key paths to update
            start_time (datetime): start time of the check
        """
        for key_path in key_paths:
            current, last_key = PreCheck._transverse_data(key_path=key_path, data=data)
            if isinstance(current[last_key], list):
                for element in current[last_key]:
                    if not bool(re.match(rf"^{self.regex}$", element)):
                        self.check_result = PreCheckResult(status=PreCheckStatus.FAILED, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds, 
                                                        message=f"Value '{element}' of field '{key_path}' does not match regex '{self.regex}'")
                        return 
            else:
                if not bool(re.match(rf"^{self.regex}$", current[last_key])):
                    self.check_result = PreCheckResult(status=PreCheckStatus.FAILED, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds, 
                                                       message=f"Value '{current[last_key]}' of field '{key_path}' does not match regex '{self.regex}'")
                    return

class ValidationCheck(PreCheck):
    """
    Class defining check of type 'validationCheck'
    """
    def __init__(self, check_formats: bool, **kwargs):
        """Class constructor

        Args:
            check_formats (bool): check time formats
        """
        super().__init__(**kwargs)
        self.check_formats = check_formats
        self.type = PreCheckType.VALIDATION_CHECK
        self.schema = None
        self._load_schema()
    
    def _load_schema(self):
        """Load JSON schema once per checks' creation
        """
        bucket = self.config.s3_connector_bucket
        json_schema = self.config.s3_client.get_object(Bucket=bucket, Key=f'{self.project}/RML/{self.project}_json_schema.json')
        self.schema = json.loads(json_schema["Body"].read())

    def execute(self, patient_data: dict) -> PreCheckResult:
        """Execute check

        Args:
            patient_data (dict): patient_data

        Returns:
            PreCheckResult: object with results of execution
        """
        try:
            start_time = datetime.now()
            if self.check_formats:
                format_checker = FormatChecker()
                format_checker.checkers['date'] = (PreCheck._date_checker, ValueError)
                format_checker.checkers['time'] = (PreCheck._time_checker, ValueError)
                format_checker.checkers['date-time'] = (PreCheck._date_time_checker, ValueError)
                v = Draft202012Validator(schema=self.schema, format_checker=format_checker)
                errs=v.iter_errors(patient_data)
            else:
                v = Draft202012Validator(schema=self.schema)
                errs=v.iter_errors(patient_data)
            errors = [err for err in errs]
            if not errors:
                return PreCheckResult(status=PreCheckStatus.SUCCESS, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds)
            else:
                error_message = ""
                for err in errors:
                    error_message += f"\nJSON Path: {err.json_path} -- Error: {err.message}"
                return PreCheckResult(status=PreCheckStatus.FAILED, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds, message=error_message)
        except Exception:
            return PreCheckResult(status=PreCheckStatus.FAILED, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds, message=traceback.format_exc())


class DataTypeCheck(PreCheck):
    """
    Class defining check of type 'dataTypeCheck'

    """
    def __init__(self, **kwargs):
        """Class constructor
        """        
        super().__init__(**kwargs)
        self.type = PreCheckType.DATA_TYPE_CHECK

    def execute(self, patient_data: bytes) -> PreCheckResult:
        """Execute check

        Args:
            patient_data (bytes): patient data

        Returns:
            PreCheckResult: object with results of execution
        """
        try:
            start_time = datetime.now()
            error_message = self.check_data_type(data=patient_data)
            if error_message is not None:
                return PreCheckResult(status=PreCheckStatus.FAILED, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds, message=error_message)
            else:
                return PreCheckResult(status=PreCheckStatus.SUCCESS, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds)
        except Exception:
            return PreCheckResult(status=PreCheckStatus.FAILED, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds, message=traceback.format_exc())

    def check_data_type(self, data: bytes) -> str:
        """Check data types

        Args:
            data (bytes): patient data

        Returns:
            str: error message
        """
        error_messages = {}
        data_types = ['xsd:dateTime', 'http://www.w3.org/2001/XMLSchema#dateTime',
                      'xsd:date', 'http://www.w3.org/2001/XMLSchema#date',
                      'xsd:time', 'http://www.w3.org/2001/XMLSchema#time',
                      'xsd:double', 'http://www.w3.org/2001/XMLSchema#double',
                      'xsd:gDay', 'http://www.w3.org/2001/XMLSchema#gDay',
                      'xsd:gMonth', 'http://www.w3.org/2001/XMLSchema#gMonth',
                      'xsd:gYear', 'http://www.w3.org/2001/XMLSchema#gYear']
        for data_type in data_types:
            regex = rf"\"(.*)\"\^\^{data_type}[\s]*[\.;]"
            matches = re.findall(regex, data.decode())
            for match in matches:
                if data_type in ['xsd:dateTime', 'http://www.w3.org/2001/XMLSchema#dateTime'] and not PreCheck._date_time_checker(value=match) or \
                   data_type in ['xsd:date', 'http://www.w3.org/2001/XMLSchema#date'] and not PreCheck._date_checker(value=match) or \
                   data_type in ['xsd:time', 'http://www.w3.org/2001/XMLSchema#time'] and not PreCheck._time_checker(value=match) or \
                   data_type in ['xsd:double', 'http://www.w3.org/2001/XMLSchema#double'] and not PreCheck._double_checker(value=match) or \
                   data_type in ['xsd:gDay', 'http://www.w3.org/2001/XMLSchema#gDay'] and not PreCheck._g_day_checker(value=match) or \
                   data_type in ['xsd:gMonth', 'http://www.w3.org/2001/XMLSchema#gMonth'] and not PreCheck._g_month_checker(value=match) or \
                   data_type in ['xsd:gYear', 'http://www.w3.org/2001/XMLSchema#gYear'] and not PreCheck._g_year_checker(value=match):
                    if data_type not in error_messages:
                        error_messages[data_type] = []
                    error_messages[data_type].append(match)
        if error_messages:
            message = ""
            for data_type, values in error_messages.items():
                values_list = [f"'{value}'" for value in values]
                message += f"\n\tValues not matching data type '{data_type}': [{', '.join(values_list)}]"
            return message


class IRIValidationCheck(PreCheck):
    """
    Class defining check of type 'IRIValidationCheck'
    """
    def __init__(self, **kwargs):
        """
        Class constructor
        """
        super().__init__(**kwargs)
        self.type = PreCheckType.IRI_VALIDATION_CHECK

    def execute(self, patient_data: bytes, keys: list) -> PreCheckResult:
        """Execute IRI validation check

        Args:
            patient_data (bytes): patient data
            keys (list): list of data keys

        Returns:
            PreCheckResult: object with results of execution
        """
        try:
            start_time = datetime.now()
            filtered_keys = [key for key in keys if key.split('/')[-1] == 'iri']
            error_messages = self.validate_IRIs(data=patient_data, keys=filtered_keys)
            if error_messages:
                return PreCheckResult(status=PreCheckStatus.FAILED, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds, message="\n" + "\n".join(error_messages))
            else:
                return PreCheckResult(status=PreCheckStatus.SUCCESS, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds)
        except Exception:
            return PreCheckResult(status=PreCheckStatus.FAILED, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds, message=traceback.format_exc())

    def validate_IRIs(self, data: dict, keys: list) -> list:
        """Validate IRIs values

        Args:
            data (dict): patient data
            keys (list): keys to check

        Returns:
            list: list of error messages
        """
        error_messages = []
        for key_path in keys:
            current, last_key = PreCheck._transverse_data(key_path=key_path, data=data)
            try:
                if isinstance(current[last_key], list):
                    for element in current[last_key]:
                        rfc3987.parse(element, rule="IRI")
                else:
                    rfc3987.parse(current[last_key], rule="IRI")
            except Exception:
                error_messages.append(f"\t\tValue '{current[last_key]}' of field '{key_path}' is not a valid IRI")
        return error_messages

class RdfValidationCheck(PreCheck):
    """
    Class defining check of type 'rdfValidationCheck'
    """
    def __init__(self, **kwargs):
        """Class constructor
        """        
        super().__init__(**kwargs)
        self.type = PreCheckType.RDF_VALIDATION_CHECK

    def execute(self, patient_data: bytes) -> PreCheckResult:
        """Execute RDF validation check

        Args:
            patient_data (bytes): patient data

        Returns:
            PreCheckResult: object with results of execution
        """
        try:
            start_time = datetime.now()
            validate_rdf_file(content=patient_data)
            return PreCheckResult(status=PreCheckStatus.SUCCESS, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds)
        except Exception as exc:
            return PreCheckResult(status=PreCheckStatus.FAILED, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds, message=exc)

class ReplaceUnversionedCodes(PreCheck):
    """Class to replace unversioned codes with versioned codes
    """
    def __init__(self, sphn_version_iri: str, **kwargs):
        """Class constructor

        Args:
            sphn_version_iri (str): SPHN schema version IRI
        """
        super().__init__(**kwargs)
        self.type = PreCheckType.REPLACE_UNVERSIONED_CODES
        self.current_year = sphn_version_iri.split('/')[-2]

    def execute(self, patient_data: bytes, keys: set, code_term_paths: set) -> PreCheckResult:
        """Execute changes on patient data for defined versioning of unversioned codes

        Args:
            patient_data (bytes): patient data
            keys (list): list of data keys
            code_term_paths (set): collection of paths to codes

        Returns:
            PreCheckResult: object with results of execution
        """
        try:
            original_patient_data = deepcopy(patient_data)
            start_time = datetime.now()
            filtered_keys = [key for key in keys if re.sub(r"(\/[0-9]*\/)", "/", key) in code_term_paths and key.endswith('/termid')]
            filtered_keys_1 = [key.replace('/termid', '/iri') for key in filtered_keys if key in keys]
            self.replace_unversioned_codes(data=patient_data, key_paths=filtered_keys_1)
            if original_patient_data != patient_data:
                message = f"At least one one unversioned code has been found where versioned codes can be used, the codes have been reassigned to the versioned codes"
            else:
                 message = "No unversioned codes have been reassigned"
            return PreCheckResult(status=PreCheckStatus.SUCCESS, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds, message=message)
        except Exception:
            return PreCheckResult(status=PreCheckStatus.FAILED, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds, message=traceback.format_exc())

    def replace_unversioned_codes(self, data: dict, key_paths: list):
        """Replace unversioned codes

        Args:
            data (dict): patient data
            key_paths (list): list of key paths to update
        """
        for key_path in key_paths:
            current, last_key = PreCheck._transverse_data(key_path=key_path, data=data)
            if current[last_key].startswith('https://www.whocc.no/atc_ddd_index/?code='):
                current[last_key] = current[last_key].replace('https://www.whocc.no/atc_ddd_index/?code=', f'https://www.whocc.no/atc_ddd_index/?year={self.current_year}&code=')
            elif current[last_key].startswith('https://biomedit.ch/rdf/sphn-resource/icd-10-gm/'):
                if not ReplaceUnversionedCodes._is_icd_10_versioned(url=current[last_key]):
                    current[last_key] = current[last_key].replace('https://biomedit.ch/rdf/sphn-resource/icd-10-gm/', f'https://biomedit.ch/rdf/sphn-resource/icd-10-gm/{self.current_year}/')
            elif current[last_key].startswith('https://biomedit.ch/rdf/sphn-resource/chop/'):
                if not ReplaceUnversionedCodes._is_chop_versioned(url=current[last_key]):
                    current[last_key] = current[last_key].replace('https://biomedit.ch/rdf/sphn-resource/chop/', f'https://biomedit.ch/rdf/sphn-resource/chop/{self.current_year}/')

    @staticmethod
    def _is_icd_10_versioned(url: str) -> bool:
        """Check if the gived IRI is versioned according to ICD-10 standards

        Args:
            url (str): IRI of the code

        Returns:
            bool: it is a versioned ICDM-10 IRI
        """
        pattern = r"^https://biomedit\.ch/rdf/sphn-resource/icd-10-gm/\d{4}/.+$"
        return bool(re.match(pattern, url))
    
    @staticmethod
    def _is_chop_versioned(url: str) -> bool:
        """Check if the gived IRI is versioned according to CHOP standards

        Args:
            url (str): IRI of the code

        Returns:
            bool: it is a versioned CHOP IRI
        """
        pattern = r"^https://biomedit\.ch/rdf/sphn-resource/chop/\d{4}/.+$"
        return bool(re.match(pattern, url))
    
class JSONPreProcessing(PreCheck):
    """
    Class defining check of type 'jsonPreProcessing'
    """

    def __init__(self,  **kwargs):
        """Class constructor
        """
        super().__init__(**kwargs)
        self.type = PreCheckType.JSON_PRE_PROCESSING
        json_schema = self.config.s3_client.get_object(Bucket=self.config.s3_connector_bucket, Key=f'{self.project}/RML/{self.project}_json_schema.json')
        self.schema = json.loads(json_schema["Body"].read())

    def execute(self, patient_data: bytes, keys: set, code_term_paths: set) -> PreCheckResult:
        """Execute concept ID pushdown and arrays expansion for supporting concepts

        Args:
            patient_data (bytes): patient data
            keys (list): list of data keys
            code_term_paths (set): collection of paths to codes

        Returns:
            PreCheckResult: object with results of execution
        """
        try:
            start_time = datetime.now()

            # Exclude supporting concepts (for Terminology and Codes the user MUST ALWAYS specify the source concept)
            # We only keep one key per concept, i.e. either the 'id' or 'termid'
            filtered_keys_1 = [key for key in keys if not key.startswith('supporting_concepts/') and (key.endswith('/id') or key.endswith('/termid'))]
            
            # If sourceConceptID already define on the code, remove the key from the list to speed up the process
            filtered_keys_2 = []
            for filtered_key in filtered_keys_1:
                if filtered_key.replace('/id', '/sourceConceptID').replace('/termid', '/sourceConceptID') not in keys:
                    filtered_keys_2.append(filtered_key)

            # Filter the keys to keep only Code/Terminology related paths
            filtered_keys_3 = [key.replace('/id', '/sourceConceptID').replace('/termid', '/sourceConceptID').split('/') for key in filtered_keys_2 if re.sub(r"(\/[0-9]*\/)", "/", key) in code_term_paths]
            
            # Push down sourceConcept ID and type
            for key_path in filtered_keys_3:
                JSONPreProcessing._add_source_concept_id(extracted_keys=key_path, data=patient_data)
            
            # Expand array fields in supporting concepts
            JSONPreProcessing._expand_supporting_array_fields(data=patient_data)
            return PreCheckResult(status=PreCheckStatus.SUCCESS, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds)
        except Exception:
            return PreCheckResult(status=PreCheckStatus.FAILED, check=self, start_time=start_time, execution_time=(datetime.now() - start_time).seconds, message=traceback.format_exc())
    
    @staticmethod
    def _add_source_concept_id(extracted_keys: list, data: dict):
        """Transverse data object

        Args:
            extracted_keys (list): key paths
            data (dict): data object

        Returns:
            Union[dict, str]: map of the last key, last key name
        """
        for key in extracted_keys[:-1]:
            if isinstance(data, list):
                key = int(key)
            else:
                source_concept_id = data.get('id')
            data = data[key]
        data[extracted_keys[-1]] = source_concept_id

    @staticmethod
    def _extract_array_fields_map(data: dict) -> dict:
        """Extract a map {'property1': [{...}, ...], ...} with the properties in supporting concepts that are defined as arrays and the current values

        Args:
            data (dict): concept data

        Returns:
            dict: array fields map
        """
        array_map= {}
        for key, value in data.items():
            if isinstance(value, list):
                if len(value) > 1:
                    if key not in array_map:
                        array_map[key] = []
                    for element in value:
                        array_map[key].append(element)
        return array_map
    
    @staticmethod
    def _find_longest_list_size(input_dict: dict) -> int:
        """Find the maximum length of array values for a concept

        Args:
            input_dict (dict): map of array fields

        Returns:
            int: maximum length
        """
        max_list_size = 0
        for value in input_dict.values():
            max_list_size = max(max_list_size, len(value))
        return max_list_size

    @staticmethod
    def _expand_supporting_array_fields(data: dict):
        """Expand supporting concepts array fields

        Args:
            data (dict): patient data
        """
        if 'supporting_concepts' in data:
            for concept_key, concept_list in data['supporting_concepts'].items():
                collected_items = []
                for distinct_concept in concept_list:
                    array_map = JSONPreProcessing._extract_array_fields_map(data=distinct_concept)
                    if array_map:
                        max_iteration = JSONPreProcessing._find_longest_list_size(input_dict=array_map)
                        iteration = 0
                        copied_el = deepcopy(distinct_concept)
                        while (iteration < max_iteration):
                            # Iterate as many times as the longest list of values for an array field in the distinct concept
                            for key, elements_list in array_map.items():
                                if iteration >= len(elements_list):
                                    # In case the list of values is smaller than the maximum we replicate the first element
                                    copied_el[key] = [elements_list[0]]
                                else:
                                    copied_el[key] = [elements_list[iteration]]
                            if copied_el not in collected_items:
                                collected_items.append(deepcopy(copied_el))
                            iteration += 1
                    else:
                        if distinct_concept not in collected_items:
                            collected_items.append(deepcopy(distinct_concept))
                data['supporting_concepts'][concept_key] = collected_items


def get_default_pre_checks() -> dict:
    """Gets default checks definition

    Returns:
        dict: default checks
    """
    default_checks = {
        "validationCheck": {
            "defaultValidationCheck": {
                "level": "warn"
                }
        },
        "IRIValidationCheck": {
            "defaultIRIValidationCheck": {
                "level": "fail"
            }
        },
        "replaceCharsCheck": {
            "defaultIDReplaceCharsCheck": {
                "applies_to_field": "id",
                "_comment" : "Replace characters in ID field",
                "replacement_map": {
                    " ": "_",
                    "/": "_",
                    ":": "_",
                    "ä": "ae",
                    "Ä": "Ae",
                    "ü": "ue",
                    "Ü": "Ue",
                    "ö": "oe",
                    "Ö": "Oe",
                    "ß": "ss",
                    "à": "a",
                    "á": "a",
                    "â": "a",
                    "À": "A",
                    "Á": "A",
                    "Â": "A",
                    "è": "e",
                    "é": "e",
                    "ê": "e",
                    "É": "E",
                    "È": "E",
                    "Ê": "E",
                    "ì": "i",
                    "í": "i",
                    "î": "i",
                    "Í": "I",
                    "Ì": "I",
                    "Î": "I",
                    "ò": "o",
                    "ó": "o",
                    "ô": "o",
                    "Ó": "O",
                    "Ò": "O",
                    "Ô": "O",
                    "ú": "u",
                    "ù": "u",
                    "û": "u",
                    "Ú": "U",
                    "Ù": "U",
                    "Û": "U"
                }
            },
            "defaultTermIDReplaceCharsCheck":{
                "applies_to_field": "termid",
                "_comment" : "Replace characters in termid field",
                "replacement_map": {
                    " ": "_",
                    "/": "_",
                    ":": "_"
                }
            }
       }
    }
    return default_checks

def get_applies_to_field(field: str) -> str:
    """Returns applied_to_field with slash added if needed

    Args:
        field (str): applies_to_field defined in configuration file

    Returns:
        str: updated field
    """
    if field is None:
        return field
    elif field.startswith('/') or field.startswith('content/') or field.startswith('sphn:DataRelease/') or field.startswith('sphn:DataProviderInstitute/') or field.startswith('sphn:DataProvider/') or field.startswith('sphn:SubjectPseudoIdentifier/'):
        return field
    elif field.startswith('/content/') or field.startswith('/sphn:DataRelease/') or field.startswith('/sphn:DataProviderInstitute/') or field.startswith('/sphn:DataProvider/') or field.startswith('/sphn:SubjectPseudoIdentifier/'):
        return field[1:]
    else:
        return "/" + field


def create_pre_checks(project: str, config: Config, is_legacy: bool, sphn_version_iri: str) -> list:
    """Reads the configuration file for Pre-Checks and creates the PreChecks objects accordingly

    Args:
        project (str): name of the project
        config (Config): configuration object
        is_legacy (bool): is legacy schema
        sphn_version_iri (str): SPHN schema version IRI

    Returns:
            list: list of PreCheck objects
    """
    bucket = config.s3_connector_bucket
    try:
        checks_config = config.s3_client.get_object(Bucket=bucket, Key=f'{project}/Pre-Check/{project}_checks_config.json')
        checks_json = json.loads(checks_config["Body"].read())
    except Exception:
        print("No Pre-Check configuration file uploaded. Setting up default Pre-Checks")
        checks_json = get_default_pre_checks()
    replace_checks = []
    pre_checks = []
    for check_type, checks in checks_json.items():
        check_type = PreCheckType(check_type)
        for check_name, check in checks.items():
            if check_type == PreCheckType.REGEX_CHECK:
                pre_checks.append(RegexCheck(project=project, 
                                             config=config, 
                                             level=PreCheckLevel(check.get('level', 'fail').upper()), 
                                             check_name=check_name, 
                                             applies_to_field=get_applies_to_field(field=check.get('applies_to_field')), 
                                             regex=check.get('regex'))
                                             )
            elif check_type == PreCheckType.VALIDATION_CHECK:
                pre_checks.append(ValidationCheck(project=project, 
                                                  config=config, 
                                                  level=PreCheckLevel(check.get('level', 'fail').upper()), 
                                                  check_name=check_name,
                                                  check_formats=check.get('check_formats', False))
                                                  )
            elif check_type == PreCheckType.DATA_TYPE_CHECK:
                pre_checks.append(DataTypeCheck(project=project, 
                                                config=config, 
                                                level=PreCheckLevel(check.get('level', 'fail').upper()), 
                                                check_name=check_name)
                                                )
            elif check_type == PreCheckType.IRI_VALIDATION_CHECK:
                pre_checks.append(IRIValidationCheck(project=project,
                                                     config=config,
                                                     level=PreCheckLevel(check.get('level', 'fail').upper()), 
                                                     check_name=check_name))
            elif check_type == PreCheckType.RDF_VALIDATION_CHECK:
                pre_checks.append(RdfValidationCheck(project=project,
                                                     config=config,
                                                     level=PreCheckLevel(check.get('level', 'fail').upper()),
                                                     check_name=check_name)
                                 )
            elif check_type == PreCheckType.REPLACE_CHARS_CHECK:
                replace_checks.append(ReplaceCharsCheck(project=project, 
                                                        config=config, 
                                                        level=PreCheckLevel.WARN, 
                                                        check_name=check_name, 
                                                        applies_to_field=get_applies_to_field(field=check.get('applies_to_field')), 
                                                        replacement_map=check.get('replacement_map'))
                                                        )
            elif check_type == PreCheckType.REPLACE_REGEX_CHECK:
                replace_checks.append(ReplaceRegexCheck(project=project, 
                                                        config=config, 
                                                        level=PreCheckLevel.WARN, 
                                                        check_name=check_name, 
                                                        applies_to_field=get_applies_to_field(field=check.get('applies_to_field')), 
                                                        regex=check.get('regex'),
                                                        replacement=check.get('replacement', '_'))
                                                        )
    if not is_legacy:
        # Add concept ID pushdown from 2024.1 schema for Code and Terminology concepts and flattening of arrays
        pre_checks.append(JSONPreProcessing(project=project,
                                            config=config,
                                            level=PreCheckLevel.FAIL,
                                            check_name='jsonPreProcessing'))
    
    pre_checks.append(ReplaceUnversionedCodes(project=project,
                                              config=config,
                                              level=PreCheckLevel.WARN,
                                              check_name='replaceUnversionedCodes',
                                              sphn_version_iri=sphn_version_iri))
    return replace_checks + pre_checks

def get_replace_id_checks(project: str, config: Config, pre_checks: list) -> list:
    """Create additional checks for patients extracted from the database, where we apply the replace logic to sourceConceptID
    fields the same way as the configured checks

    Args:
        project (str): name of the project
        config (Config): configuration object
        pre_checks (list): list of configured pre-checks

    Returns:
        list: list of new pre-checks to replace sourceConceptID field
    """
    replace_checks = []
    for check in pre_checks:
        if check.type == PreCheckType.REPLACE_CHARS_CHECK and check.applies_to_field == '/id':
            replace_checks.append(ReplaceCharsCheck(project=project, 
                                                    config=config, 
                                                    level=PreCheckLevel.WARN, 
                                                    check_name=check.check_name + 'SourceConceptID', 
                                                    applies_to_field="/sourceConceptID", 
                                                    replacement_map=check.replacement_map))
        elif check.type == PreCheckType.REPLACE_REGEX_CHECK and check.applies_to_field == '/id':
            replace_checks.append(ReplaceRegexCheck(project=project, 
                                                    config=config, 
                                                    level=PreCheckLevel.WARN, 
                                                    check_name=check.check_name + 'SourceConceptID', 
                                                    applies_to_field="/sourceConceptID", 
                                                    regex=check.regex,
                                                    replacement=check.replacement)
                                                    )
    return replace_checks


def validate_configuration_file(content: bytes) -> str:
    """Validates Pre-Checks configuration file

    Args:
        content (bytes): checks configuration file content

    Returns:
        str: error messages
    """
    checks_json = json.loads(content)
    pre_conditions_list = []
    for check_type, checks in checks_json.items():
        if check_type not in [type.value for type in PreCheckType]:
            pre_conditions_list.append(f"Defined type '{check_type}' is not a valid Pre-Check type. Allowed values: [{', '.join([type.value for type in PreCheckType])}]")
            continue
        check_type = PreCheckType(check_type)
        for check_name, check in checks.items():
            if not isinstance(check, dict):
                pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{str(check)}' is not a valid entry. Entry must be defined as a dictionary with parameters according to check type")
                continue
            if check_type == PreCheckType.REGEX_CHECK:
                check_level = check.get('level', 'fail')
                regex = check.get('regex')
                applies_to_field = check.get('applies_to_field')
                if check_level.upper() not in ['WARN', 'FAIL']:
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{check_level}' is not a valid entry for field 'level'. Allowed values: ['WARN', 'FAIL']")
                if regex is None:
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: 'regex' field is mandatory")
                elif not isinstance(regex, str):
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{str(regex)}' is not a valid entry for field 'regex'. Entry must be of type string")
                else:
                    try:
                        re.compile(regex)
                    except Exception:
                        pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{str(regex)}' is not a valid regex expression for 'regex' field")
                if applies_to_field is None:
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: 'applies_to_field' field is mandatory")
                elif not isinstance(applies_to_field, str):
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{str(applies_to_field)}' is not a valid entry for 'applies_to_field' field. Entry must be of type string")
            elif check_type == PreCheckType.VALIDATION_CHECK:
                check_level = check.get('level', 'fail')
                check_formats = check.get('check_formats', False)
                if check_level.upper() not in ['WARN', 'FAIL']:
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{check_level}' is not a valid entry for field 'level'. Allowed values: ['WARN', 'FAIL']")
                if not isinstance(check_formats, bool):
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{check_formats}' is not a valid entry for field 'check_formats'. Entry must be of type boolean")
            elif check_type == PreCheckType.DATA_TYPE_CHECK:
                check_level = check.get('level', 'fail')
                if check_level.upper() not in ['WARN', 'FAIL']:
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{check_level}' is not a valid entry for field 'level'. Allowed values: ['WARN', 'FAIL']")
            elif check_type == PreCheckType.IRI_VALIDATION_CHECK:
                check_level = check.get('level', 'fail')
                if check_level.upper() not in ['WARN', 'FAIL']:
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{check_level}' is not a valid entry for field 'level'. Allowed values: ['WARN', 'FAIL']")
            elif check_type == PreCheckType.RDF_VALIDATION_CHECK:
                check_level = check.get('level', 'fail')
                if check_level.upper() not in ['WARN', 'FAIL']:
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{check_level}' is not a valid entry for field 'level'. Allowed values: ['WARN', 'FAIL']")
            elif check_type == PreCheckType.REPLACE_CHARS_CHECK:
                check_level = check.get('level', 'fail')
                applies_to_field = check.get('applies_to_field')
                replacement_map = check.get('replacement_map')
                if check_level.upper() not in ['WARN', 'FAIL']:
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{check_level}' is not a valid entry for field 'level'. Allowed values: ['WARN', 'FAIL']")
                if applies_to_field is None:
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: 'applies_to_field' field is mandatory")
                elif not isinstance(applies_to_field, str):
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{str(applies_to_field)}' is not a valid entry for 'applies_to_field' field. Entry must be of type string")
                if replacement_map is None:
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: 'replacement_map' field is mandatory")
                elif not isinstance(replacement_map, dict):
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{str(replacement_map)}' is not a valid entry for 'replacement_map' field. Entry must be of type dict")
                elif not replacement_map:
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{str(replacement_map)}' is not a valid entry for 'replacement_map' field. The map cannot be empty")
                elif not all(isinstance(item, str) for item in replacement_map.values()):
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{str(replacement_map)}' is not a valid entry for 'replacement_map' field. Entry must be a dict with string values")
                elif not all(isinstance(item, str) for item in replacement_map.keys()):
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{str(replacement_map)}' is not a valid entry for 'replacement_map' field. Entry must be a dict with string keys")
            elif check_type == PreCheckType.REPLACE_REGEX_CHECK:
                check_level = check.get('level', 'fail')
                applies_to_field = check.get('applies_to_field')
                regex = check.get('regex')
                replacement = check.get('replacement', '_')
                if check_level.upper() not in ['WARN', 'FAIL']:
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{check_level}' is not a valid entry for field 'level'. Allowed values: ['WARN', 'FAIL']")
                if applies_to_field is None:
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: 'applies_to_field' field is mandatory")
                elif not isinstance(applies_to_field, str):
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{str(applies_to_field)}' is not a valid entry for 'applies_to_field' field. Entry must be of type string")
                if regex is None:
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: 'regex' field is mandatory")
                elif not isinstance(regex, str):
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{str(regex)}' is not a valid entry for field 'regex'. Entry must be of type string")
                else:
                    try:
                        re.compile(regex)
                    except Exception:
                        pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{str(regex)}' is not a valid regex expression for 'regex' field")
                if not isinstance(replacement, str):
                    pre_conditions_list.append(f"Check '{check_name}' is misconfigured: '{str(replacement)}' is not a valid entry for 'replacement' field. Entry must be of type string")
    if pre_conditions_list:
        return ', '.join(pre_conditions_list)