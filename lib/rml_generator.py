#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'rml_generator.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

import argparse
import json
import logging
import os
from copy import deepcopy
from pathlib import Path
import sys
from rdflib import OWL, RDF, RDFS, Graph, Namespace
from rdflib.term import BNode, Literal, URIRef

sys.path.append(os.path.abspath(os.path.dirname(__file__)))
from database import Database
from enums import RML_JSON_LOGS, ConceptType
from rml_generator_lib import (export_graph, get_all_subclasses, get_core_concepts, get_prefix_objects_map,
                               get_range, get_restrictions, get_restrictions_map, get_restrictions_map_with_range, get_supporting_concepts, get_value_set, has_higher_cardinality,
                               has_terminology_range_only, has_ucums,
                               has_value_set, load_graph, niceify_prefix, group_target_concepts,
                               simplify_schema_graph, sort_json_schema)

NAMESPACE_RML = ('rml', URIRef('http://semweb.mmlab.be/ns/rml#'))
NAMESPACE_RR = ('rr', URIRef('http://www.w3.org/ns/r2rml#'))
NAMESPACE_QL = ('ql', URIRef('http://semweb.mmlab.be/ns/ql#'))
NAMESPACE_RML_BASE = ('', URIRef('https://biomedit.ch/rdf/sphn-rml/sphn#'))
NAMESPACE_RESOURCE = ('resource', URIRef("https://biomedit.ch/rdf/sphn-resource/"))
NAMESPACE_DCT = ('dct', URIRef("http://purl.org/dc/terms/"))

RML = Namespace(NAMESPACE_RML[1])
RR =  Namespace(NAMESPACE_RR[1])
QL =  Namespace(NAMESPACE_QL[1])
BASE = Namespace(NAMESPACE_RML_BASE[1])
RESOURCE = Namespace(NAMESPACE_RESOURCE[1])
DCT = Namespace(NAMESPACE_DCT[1])


class JSONSchemaGenerator(object):
    """
    Class containing details and methods about the JSON schema generation
    """

    def __init__(self, 
                 schema_graph: Graph, 
                 restrictions: dict, 
                 output_path: str, 
                 project_name: str, 
                 database: Database, 
                 restrictions_map: dict, 
                 sphn_base_iri: str,
                 core_concepts: set, 
                 supporting_concepts: set, 
                 root_nodes: set, 
                 project_iri: str,
                 do_not_restrict_unit: bool):
        """Class constructor

        Args:
            schema_graph (Graph): schema graph
            restrictions (dict): map of SPHN classes restrictions
            output_path (str): output path of the JSON schema file
            project_name (str): name of the project
            database (Database): database object
            restrictions_map (dict): restrictions map
            sphn_base_iri (str): SPHN base IRI
            core_concepts (set): core concepts
            supporting_concepts (set): supporting concepts
            root_nodes (set): root nodes
            project_iri (str): project base IRI
            do_not_restrict_unit (bool): do not restrict sphn:hasUnit values
        """ 
        self.schema = {}
        self.schema_graph = schema_graph
        self.restrictions = restrictions
        self.output_path = output_path
        self.sphn_base_iri = sphn_base_iri
        self.data_provider_class = Database._get_data_provider_class(sphn_base_iri=sphn_base_iri)
        self.data_provider_property = f"has{self.data_provider_class}"
        self.get_base_json_schema()
        self.project_name = project_name
        self.database = database
        self.restrictions_map = restrictions_map
        self.cleanup_supporting_concepts()
        self.core_concepts = core_concepts
        self.supporting_concepts = supporting_concepts
        self.is_legacy = Database._is_legacy_schema(sphn_base_iri=self.sphn_base_iri)
        self.root_nodes = root_nodes
        self.project_iri = project_iri
        self.valuesets = get_all_subclasses(graph=self.schema_graph, concept=URIRef(f"{self.sphn_base_iri}#ValueSet"), sphn_namespace=self.sphn_base_iri, root_nodes=self.root_nodes)
        self.do_not_restrict_unit = do_not_restrict_unit

    def get_base_json_schema(self) -> dict:
        """Returns the base format of the JSON schema which will be filled by the Schema

        Returns:
            dict: JSON schema base structure
        """
        self.schema = {
            "$schema": "https://json-schema.org/draft/2020-12/schema",
            "type": "object",
            "properties": {
                "id": {
                    "type": "string"
                },
                "schema": {
                    "type": "string"
                },
                "title": {
                    "type": "string"
                },
                "targetType": {
                    "type": "string"
                },
                "license": {
                    "type": "string"
                },
                "sphn:DataRelease":{
                    "type": "object",
                    "description": "sphn:DataRelease properties",
                    "properties": {
                        "id": {
                            "type": "string",
                            "description": "ID of 'sphn:DataRelease'. To ensure the uniqueness of a DataRelease instance ID (i.e. the dataset identifier), a UNIX Epoch timestamp should ideally be concatenated to it as a suffix"
                            },
                        "sphn:hasExtractionDateTime": {
                            "type": "string",
                            "description": "Value for the sphn:hasExtractionDateTime of the data release",
                            "format": "date-time"
                    }
                },
                "required": ["id", "sphn:hasExtractionDateTime"]
            },
            },
            "required": ["sphn:DataRelease", "sphn:SubjectPseudoIdentifier", f"sphn:{self.data_provider_class}", "content"],
            "additionalProperties": False
        }

    def generate_schema(self):
        """Generates JSON schema 
                Understanding the RML mappings creation is more tricky then the JSON schema creation.
                For the JSON schema we simply loop though the schema graph starting from the core concepts and supporting concepts.
                We then extract all the data-type properties which we map directly to fields, and the object properties which we map recursively.
                When an object points to a core concepts, then we report only the referncing 'id' field and we stop the recursion.
                When extracting the classes in range of a properties, in case there are multiple classes available, we then allow multiple definition objects
                for the object property. Those object are sometimes define by a 'target_concept' field which defines the class in range they are defining.
                Restrictions on valuesets and UCUM values are defined in string fields by the 'enum' field. Properties that have a cardinality which can be 
                higher than one are represented as arrays in the JSON schema.
                These are the main key points for the JSON schema generation, for more details check-out the user guide and follow the code step by step.

        """
        core_concept_dict = dict()
        supporting_concept_dict = dict()
        logging.info("Generating JSON schema")
        if self.database is not None:
            self.database.update_rml_json_logging(project_name = self.project_name, status=RML_JSON_LOGS.JSON_SCHEMA_GENERATOR, message = "Generating JSON schema")

        # Generate schema for core concepts
        for classiri in self.core_concepts:
            self.json_core_concept(classiri=classiri, core_concept_dict=core_concept_dict, concept_type=ConceptType.CORE)

        # Generate schema for supporting concepts
        for classiri in self.supporting_concepts:
            self.json_core_concept(classiri=classiri, core_concept_dict=supporting_concept_dict, concept_type=ConceptType.SUPPORTING)
        
        # Unify results
        for key, value in core_concept_dict.items():
            if key.split(':')[-1] in [self.data_provider_class, 'SubjectPseudoIdentifier']:
                self.schema["properties"][key] = value
        
        self.schema['properties']['content'] = {"type": "object", "description": "List of SPHN core concepts", "properties": {}, "additionalProperties": False}
        self.schema['properties']['supporting_concepts'] = {"type": "object", "description": "List of SPHN supporting concepts", "properties": {}, "additionalProperties": False}
        
        # Finalize structure
        for key, value in core_concept_dict.items():
            if key.split(':')[-1] not in [self.data_provider_class, 'SubjectPseudoIdentifier']:
                self.schema["properties"]["content"]["properties"][key] = {"type": "array", "description": f"List of '{key}' concepts", "items": value}
        
        for key, value in supporting_concept_dict.items():
            if key.split(':')[-1] not in [self.data_provider_class, 'SubjectPseudoIdentifier']:
                self.schema["properties"]["supporting_concepts"]["properties"][key] = {"type": "array", "description": f"List of '{key}' concepts", "items": value}
        
        if not self.schema["properties"]["supporting_concepts"]["properties"]:
            # Delete 'supporting_concepts' key if no supporting concepts defined
            self.schema["properties"].pop("supporting_concepts")
        
        # Save JSON schema to file
        logging.info(f"Exporting JSON schema to file '{self.output_path}'")

        # Sort JSON schema keys
        sort_json_schema(json_schema=self.schema, data_provider_class=self.data_provider_class)
        
        with open(self.output_path, 'w', encoding='utf-8') as f:
            json.dump(self.schema, f, ensure_ascii=False, indent=4)

        if self.database is not None:
            self.database.update_rml_json_logging(project_name=self.project_name, status=RML_JSON_LOGS.JSON_SCHEMA_GENERATOR_Export, message = f"Exporting JSON schema to file '{self.output_path}'")
  
    def json_core_concept(self, classiri: URIRef, core_concept_dict: dict, concept_type: ConceptType, on_extra_class: bool = False):
        """Generates JSON schema for core concept

        Args:
            classiri (URIRef): SPHN class
            core_concept_dict (dict): map to fill with schema information for the class
            concept_type (ConceptType): type of the concept
            on_extra_class (bool, optional): special case when processing an additional class after a ValueSet. Defaults to False.
        """
        pseudoprefix, name = niceify_prefix(prefixable_iri=classiri)
        property_key = pseudoprefix + ":" + name

        if property_key not in core_concept_dict:
            # Define core concept in the schema if not already there
            id_entry = dict({"id" : {"type" : "string", "description": f"ID of {pseudoprefix.upper()} Concept '{name}'"}})
            core_concept_dict[property_key] = dict({"type": "object", "description": JSONSchemaGenerator._get_description(prop_class=classiri), "properties" : id_entry, "required": ["id"], "additionalProperties": False})
        
        # Extract all data-type and object properties for the core concept
        all_dataproperties = list(self.schema_graph.subjects(RDF.type, OWL.DatatypeProperty))
        domain_class = list(self.schema_graph.subjects(RDFS.domain, classiri))
        dataproperties_for_class = list(set(all_dataproperties) & set(domain_class))
        objectproperties_for_class = list(set(domain_class) - set(all_dataproperties))

        # Generate JSON schema entried for data-type properties
        for prop in dataproperties_for_class:
            self.json_datatype_property(classiri=classiri, prop=prop, property_dict=core_concept_dict[property_key], concept_type=concept_type, top_level=True)
        
        # Generate JSON schema entries for object properties
        for obj in objectproperties_for_class:
            _, obj_name = niceify_prefix(prefixable_iri=obj)
            
            if obj_name == self.data_provider_property and concept_type == ConceptType.SUPPORTING:
                # Keep track of supporting concepts related to sphn:DataProvider for later use
                self.track_supporting_dpi_property(property_key=property_key)
            
            # Generate JSON schema entry for the object property
            if self.is_legacy:
                next_restrictions_key = str(classiri) + str(obj)
            else:
                next_restrictions_key = str(classiri) + f"[{pseudoprefix}{name}]" + str(obj)
            self.json_object_property(classiri=classiri, obj=obj, object_dict=core_concept_dict[property_key], concept_type=concept_type, on_extra_class=on_extra_class, restrictions_key=next_restrictions_key)

    def json_object_property(self, 
                             classiri: URIRef, 
                             obj: URIRef, 
                             object_dict: dict, 
                             restrictions_key: str, 
                             concept_type: ConceptType, 
                             on_extra_class: bool = False, 
                             break_next: bool = False):
        """Generates JSON schema for object property

        Args:
            classiri (URIRef): concept class
            obj (URIRef): object property
            object_dict (dict): object property map to fill with schema information
            restrictions_key (str): transversed path for restrictions check
            concept_type (ConceptType): concept type
            on_extra_class (bool, optional): special case when processing an additional class after a ValueSet. Defaults to False.
            break_next (bool, optional): break second iteration in a loop. Defaults to False.
        """
        pseudoprefix, obj_name = niceify_prefix(prefixable_iri=obj)
        # Case when multiple owl:someValuesFrom are defined on a property
        if isinstance(self.restrictions[classiri][str(obj)], list):
            is_field_required = self.restrictions[classiri][str(obj)][0].get('minCardinality') == "1"
        else:
            is_field_required = self.restrictions[classiri][str(obj)].get('minCardinality') == "1"
        is_array = has_higher_cardinality(classiri=classiri, obj=obj, restrictions=self.restrictions, sphn_base_iri=self.sphn_base_iri)

        if not on_extra_class and has_value_set(classiri=classiri, obj=obj, schema_graph=self.schema_graph, restrictions=self.restrictions):
            # Define JSON schema for object properties defined by sphn:ValueSet
            values, additional_classes,_ = get_value_set(classiri=classiri, obj=obj, schema_graph=self.schema_graph, restrictions=self.restrictions)
            sub_object_key = pseudoprefix + ":" + obj_name

            if sub_object_key not in object_dict['properties']:
                value_set_schema = JSONSchemaGenerator._get_value_set_schema(obj_name=obj_name, enums=[str(value) for value in values])
                object_dict["properties"][sub_object_key] = dict({"type": "object"})

                if is_field_required:
                    object_dict['required'].append(sub_object_key)

                if not additional_classes:
                    object_dict["properties"][sub_object_key]['description'] = value_set_schema['description']
                    object_dict["properties"][sub_object_key]['properties'] = value_set_schema['properties']
                    object_dict["properties"][sub_object_key]['required'] = value_set_schema['required']
                    object_dict["properties"][sub_object_key]['additionalProperties'] = value_set_schema['additionalProperties']
                else:
                    # Potential legacy logic to describe properties with additional classes
                    object_dict["properties"][sub_object_key]['description'] = f"Schema definition for '{obj_name}' property"
                    object_dict["properties"][sub_object_key]["oneOf"]= [value_set_schema]
                    for additional_class in additional_classes:
                        tmp_json = dict()
                        self.json_core_concept(classiri=additional_class, core_concept_dict=tmp_json, concept_type=concept_type, on_extra_class=True)
                        pseudoprefix, additional_class_name = niceify_prefix(prefixable_iri=additional_class)
                        properties = tmp_json[f"{pseudoprefix}:{additional_class_name}"]['properties']
                        required_fields = tmp_json[f"{pseudoprefix}:{additional_class_name}"]['required']
                        description = f"{pseudoprefix.upper()} Concept '{additional_class_name}' for '{obj_name}' property"
                        additional_json = {"type": "object", "description": description, "properties": properties, "required": required_fields, "additionalProperties": False}
                        object_dict["properties"][sub_object_key]['oneOf'].append(additional_json)

        elif has_ucums(classiri=classiri, obj=obj, restrictions=self.restrictions):
            # Define JSON schema for object properties defining UCUM values
            sub_object_key = pseudoprefix + ":" + obj_name
            if sub_object_key not in object_dict['properties']:
                if restrictions_key in self.restrictions_map and not self.do_not_restrict_unit:
                    ucum_schema = self.get_ucum_schema(obj_name=obj_name, enum_values=self.restrictions_map[restrictions_key])
                else:
                    ucum_schema = self.get_ucum_schema(obj_name=obj_name)
                
                if is_field_required:
                    object_dict['required'].append(sub_object_key)

                object_dict["properties"][sub_object_key] = dict({"type": "object"})
                object_dict["properties"][sub_object_key]['description'] = ucum_schema['description']
                object_dict["properties"][sub_object_key]['properties'] = ucum_schema['properties']
                object_dict["properties"][sub_object_key]['required'] = ucum_schema['required']
                object_dict["properties"][sub_object_key]['additionalProperties'] = ucum_schema['additionalProperties']
        else:
            # Define JSON schema object properties

            if obj_name in [self.data_provider_property, 'hasSubjectPseudoIdentifier']:
                # Special concepts are not mapped inside other core concepts
                return
            
            # Extract classes in range of the object property
            classrefs, root_concept = get_range(classiri=classiri, schema_graph=self.schema_graph, obj=obj, restrictions=self.restrictions, restrictions_key=restrictions_key, restrictions_map=self.restrictions_map, 
                                                sphn_base_iri=self.sphn_base_iri, is_legacy=self.is_legacy, root_nodes=self.root_nodes, project_iri=self.project_iri)       
            multi_range = len(classrefs) > 1
            sub_object_key = pseudoprefix + ":" + obj_name

            if sub_object_key not in object_dict['properties']:

                # Generate description field for object property entry
                if multi_range:
                    classrefs_descriptions = sorted([JSONSchemaGenerator._get_description(prop_class=ref) for ref in classrefs])
                    obj_description = "/".join(classrefs_descriptions)
                else:
                    obj_description = JSONSchemaGenerator._get_description(prop_class=classrefs[0])
                
                classrefprefix, classrefname = niceify_prefix(prefixable_iri=classrefs[0]) # I think here we assume that the first classref is Code in the case of multi_range
                id_entry = dict({"id" : {"type" : "string", "description": f"ID of {classrefprefix.upper()} Concept '{classrefname}'"}})
                object_dict["properties"][sub_object_key] = dict({"type": "object", "description": obj_description, "properties" : id_entry, "required": ["id"], "additionalProperties": False})
                
                if is_field_required:
                    object_dict['required'].append(sub_object_key)

                if self.is_legacy and classrefname == 'AdministrativeCase':
                    # For legacy RDF schema the property pointing to sphn:AdministrativeCase is mapped only with the ID field
                    if is_array:
                        object_dict["properties"][sub_object_key] = {"type": "array", "description": f"List of '{obj_name}' properties", "items": object_dict["properties"][sub_object_key]}
                    return
                
            # Check if object property is restricted to only allow sphn:Terminology values
            terminology_only_condition = multi_range and has_terminology_range_only(classiri=classiri, obj=obj, restrictions=self.restrictions, classrefs=classrefs, sphn_base_iri=self.sphn_base_iri, root_nodes=self.root_nodes) or not multi_range and classrefs[0] == URIRef(f'{self.sphn_base_iri}#Terminology')
            
            if terminology_only_condition:
                # Ad-hoc schema for sphn:Terminology
                terminology_schema = self.get_terminology_schema(concept_type=concept_type)
                object_dict["properties"][sub_object_key]['required'] = terminology_schema['required']
                object_dict["properties"][sub_object_key]['description'] = terminology_schema['description']
                object_dict["properties"][sub_object_key]['properties'] = terminology_schema['properties']
            else:
                list_of_properties = []
                valueset_internal_map = deepcopy(object_dict['properties'][sub_object_key])
                valueset_enum_values = set()
                
                # Loop over the object property's classes in range
                for classref in classrefs:

                    if classref == URIRef(f"{self.sphn_base_iri}#ValueSet"):
                        # Skip parent class ValueSet
                        continue

                    internal_map = deepcopy(object_dict['properties'][sub_object_key])

                    if classref == URIRef(f"{self.sphn_base_iri}#Terminology") and multi_range:
                        # Ad-hoc schema for sphn:Terminology with multiple classes in range
                        list_of_properties.append(self.get_terminology_schema(concept_type=concept_type, break_next=break_next, root_concept=root_concept))
                    else:
                        if (concept_type == ConceptType.SUPPORTING or break_next or (classref in self.core_concepts and not self.is_legacy)) and classref != URIRef(f"{self.sphn_base_iri}#Code"):
                            # Skip generation of schema entries for supporting concepts, properties leading to a direct cycle, and properties pointing towards a core concepts (sphn:Code is an exception)
                            if not multi_range:
                                if is_array:
                                    # Update schema if property is an array
                                    object_dict["properties"][sub_object_key] = {"type": "array", "description": f"List of '{obj_name}' properties", "items": object_dict["properties"][sub_object_key]}
                                return
                        else:
                            # Extract all data-type and object properties for the core concept
                            all_dataproperties = list(self.schema_graph.subjects(RDF.type, OWL.DatatypeProperty))
                            domain_class = list(self.schema_graph.subjects(RDFS.domain, classref))
                            dataproperties_for_class = list(set(all_dataproperties) & set(domain_class))
                            objectproperties_for_class = list(set(domain_class) - set(all_dataproperties))

                            if classiri in classrefs:    
                                # Condition for a direct cycle in the graph (only 'id' field mapped)
                                break_next = True
                            
                            # Generate JSON schema entries for data-type properties
                            for prop in dataproperties_for_class:
                                self.json_datatype_property(classiri=classref, prop=prop, property_dict=internal_map, concept_type=concept_type)

                            # Generate JSON schema entries for object properties
                            for obj in objectproperties_for_class:        
                                if str(obj) in restrictions_key:
                                    break_next = True        
                                if self.is_legacy:
                                    next_restrictions_key = restrictions_key + str(obj)
                                else:
                                    classrefprefix, classrefname = niceify_prefix(prefixable_iri=classref)    
                                    next_restrictions_key = restrictions_key + f"[{classrefprefix}{classrefname}]" + str(obj)
                                self.json_object_property(classiri=classref, obj=obj, object_dict=internal_map, restrictions_key=next_restrictions_key, concept_type=concept_type, break_next=break_next)

                            if classref == URIRef("https://biomedit.ch/rdf/sphn-schema/sphn#Code"):
                                if concept_type == ConceptType.SUPPORTING:
                                    # For supporting concepts properties with sphn:Code in range, define artificial fields to keep track of the source concept details
                                    JSONSchemaGenerator._define_source_concept_fields(object_dict=internal_map)
                                elif not self.is_legacy:
                                    internal_map['properties']['sourceConceptID'] = {"type": "string", "description": "The ID of the SPHN/Project concept where this code has been used, e.g. 123"}

                        if multi_range:
                            if classref in self.valuesets:
                                # Update the list of collected valuesets values
                                valueset_values = list(self.schema_graph.subjects(RDF.type, classref))
                                valueset_enum_values.update([str(value) for value in valueset_values])
                            else:
                                # Update list of properties collected
                                internal_map["description"] = JSONSchemaGenerator._get_description(prop_class=classref)
                                if "properties" in internal_map and "id" in internal_map["properties"]:
                                    multiclassrefprefix, multiclassrefname = niceify_prefix(prefixable_iri=classref)
                                    internal_map["properties"]["id"]["description"] = f"ID of {multiclassrefprefix.upper()} Concept '{multiclassrefname}'"
                                    # in one of scenarios we need a distinct identifier to get the correct concepts:
                                    if classref != URIRef(f"{self.sphn_base_iri}#Code") or root_concept:                                 
                                        internal_map["properties"]["target_concept"] = {"type" : "string", "description":f"IRI for Concept '{multiclassrefname}'","enum": [f"{classref}"]}
                                        internal_map["required"].append("target_concept")
                                list_of_properties.append(internal_map)
                
                if valueset_enum_values:
                    # Define JSON schema entry for sphn:ValueSet and list all the available values
                    value_set_schema = JSONSchemaGenerator._get_value_set_schema(obj_name=obj_name, enums=list(valueset_enum_values), supporting_concept=True)
                    valueset_internal_map["description"] = JSONSchemaGenerator._get_description(prop_class=URIRef(f"{self.sphn_base_iri}#ValueSet"))
                    valueset_internal_map['properties'] = value_set_schema['properties']
                    valueset_internal_map['required'] = value_set_schema['required']
                    valueset_internal_map['additionalProperties'] = value_set_schema['additionalProperties']
                    list_of_properties.append(valueset_internal_map)
                
                # Finalize the schema entries for the object property
                if multi_range:
                    if set(classrefs) != set([URIRef(f'{self.sphn_base_iri}#Terminology'), URIRef(f'{self.sphn_base_iri}#Code')]):
                        list_of_properties = group_target_concepts(list_of_properties=list_of_properties)  
                    if len(list_of_properties) == 1:
                        object_dict["properties"][sub_object_key] = list_of_properties[0]
                    else:
                        object_dict["properties"][sub_object_key] = {"type": "object", "description": object_dict["properties"][sub_object_key]["description"], 'oneOf': list_of_properties}
                else:
                    object_dict["properties"][sub_object_key] = internal_map
        
        if is_array:
            object_dict["properties"][sub_object_key] = {"type": "array", "description": f"List of '{obj_name}' properties", "items": object_dict["properties"][sub_object_key]}

    def json_datatype_property(self, 
                               classiri: URIRef, 
                               prop: URIRef, 
                               property_dict: dict, 
                               concept_type: ConceptType, 
                               top_level: bool = False):
        """Generates JSON schema for datatype property

        Args:
            classiri (URIRef): concept class
            prop (URIRef): datatype property
            property_dict (dict): datatype property map to fill with schema information
            concept_type (ConceptType): concept type
            top_level (bool, optional): object property from a top level class. Defaults to False.
        """
        pseudoprefix, prop_name = niceify_prefix(prefixable_iri=prop)
        is_field_required = self.restrictions[classiri][str(prop)].get('minCardinality') == "1"
        is_array = self.restrictions[classiri][str(prop)].get('maxCardinality') != "1"
        range_of_datatypes = list(self.schema_graph.objects(prop, RDFS.range))
        if len(range_of_datatypes) > 1:
            if 'valueset' in self.restrictions[classiri][str(prop)]:
                datatype = URIRef(self.restrictions[classiri][str(prop)]['valueset'][0])
            else:
                class_prefix, class_name = niceify_prefix(prefixable_iri=classiri)
                if self.database is not None:
                    self.database.update_rml_json_logging(project_name=self.project_name, status=RML_JSON_LOGS.JSON_DATATYPE_WARNING, message="Missing restrictions for some objects. String type is enforced")
                logging.warning(f"JSON generation -- No restrictions found on type for object '{pseudoprefix}:{prop_name}' for class '{class_prefix}:{class_name}'. Enforcing string type")
                datatype = URIRef('http://www.w3.org/2001/XMLSchema#string')
        else:
            datatype = range_of_datatypes[0]
        _, datatype_name = niceify_prefix(prefixable_iri=datatype)
        sub_property_key = pseudoprefix + ":" + prop_name
        
        if is_field_required:
            if concept_type == ConceptType.CORE or concept_type == ConceptType.SUPPORTING and top_level:
                if "oneOf" in property_dict.keys():
                    property_dict["oneOf"][0]["required"].append(sub_property_key)
                else:
                    property_dict["required"].append(sub_property_key)
        if "oneOf" in property_dict.keys():
            property_dict["oneOf"][0]["properties"][sub_property_key] = {"description": JSONSchemaGenerator._get_description(prop_name=prop_name)}
            JSONSchemaGenerator._set_property_type(property_dict=property_dict["oneOf"][0]["properties"][sub_property_key], datatype=datatype_name)
        else:
            property_dict["properties"][sub_property_key] = {"description": JSONSchemaGenerator._get_description(prop_name=prop_name)}
            JSONSchemaGenerator._set_property_type(property_dict=property_dict["properties"][sub_property_key], datatype=datatype_name)

        if is_array:
            if "oneOf" in property_dict.keys():
                property_dict["oneOf"][0]["properties"][sub_property_key] = {"type": "array", "description": f"List of '{prop_name}' properties", "items": property_dict["properties"][sub_property_key]}
            else:
                property_dict["properties"][sub_property_key] = {"type": "array", "description": f"List of '{prop_name}' properties", "items": property_dict["properties"][sub_property_key]}


    @staticmethod
    def _set_property_type(property_dict: dict, datatype: str):
        """Sets property type in the JSON schema definition

        Args:
            property_dict (dict): dictionary of the property
            datatype (str): name of the data type
        """
        if datatype == "dateTime":
            property_dict["type"] = "string"
            property_dict["format"] = "date-time"
        elif datatype == 'date':
            property_dict["type"] = "string"
            property_dict["format"] = "date"
        elif datatype == "time":
            property_dict["type"] = "string"
            property_dict["format"] = "time"
        elif datatype == "double":
            property_dict["type"] = "number"
        elif datatype == "gYear":
            property_dict["type"] = "string"
            property_dict["pattern"] = "^(-)?[0-9][0-9][0-9][0-9](Z|[+-]14:00|[+-](?:[0][0-9]|[1][0-3]):[0-5][0-9])?$"
        elif datatype == "gMonth":
            property_dict["type"] = "string"
            property_dict["pattern"] = "^(--[0][1-9]|--[1][0-2])(Z|[+-]14:00|[+-](?:[0][0-9]|[1][0-3]):[0-5][0-9])?$"
        elif datatype == "gDay":
            property_dict["type"] = "string"
            property_dict["pattern"] = "^(---[0][1-9]|---[1][0-9]|---[2][0-9]|---[3][0-1])(Z|[+-]14:00|[+-](?:[0][0-9]|[1][0-3]):[0-5][0-9])?$"
        else:
            property_dict["type"] = "string"

    @staticmethod
    def _get_description(prop_name: str = None, prop_class: URIRef = None) -> str:
        """Get description for property block

        Args:
            prop_name (str, optional): name of the property. Defaults to None.
            prop_class (URIRef, optional): class of the property. Defaults to None.

        Returns:
            str: converted string
        """
        if prop_class:
            pesudoprefix, name = niceify_prefix(prefixable_iri=prop_class)
            return f"{pesudoprefix.upper()} Concept '{name}'"
        else:
            return f"Value for '{prop_name}' property"

    
    @staticmethod
    def _get_value_set_schema(obj_name: str, enums: list, supporting_concept: bool = False) -> dict:
        """Get schema for a given ValueSet

        Args:
            obj_name (str): object property name
            enums (list): list of values of the ValueSet
            supporting_concept (bool, optional): special logic for supporting concept. Defaults to False.

        Returns:
            dict: schema of the ValueSet
        """
        if supporting_concept:
            value_set_schema = {
            "type": "object", 
            "description": f"Value Set for property '{obj_name}'",
            "properties": {
                "valueset_iri": {"type": "string", "description": f"IRI for '{obj_name}' property", "enum": enums}
                },
                "required": ["valueset_iri"],
                "additionalProperties": False
            }
        else:
            value_set_schema = {
                "type": "object", 
                "description": f"Value Set for property '{obj_name}'",
                "properties": {
                    "iri": {"type": "string", "description": f"IRI for '{obj_name}' property", "enum": enums}
                    },
                    "required": ["iri"],
                    "additionalProperties": False
                }
        return value_set_schema
    
    def get_ucum_schema(self, obj_name: str, enum_values: list = []) -> dict:
        """Get schema for property with UCUM values

        Args:
            obj_name (str): object property name
            enum_values (list, optional): list of values restricted. Defaults to [].

        Returns:
            dict: UCUM schema
        """
        if self.is_legacy:
            ucum_schema = {
                "type": "object", 
                "description": f"Schema for property '{obj_name}'",
                "properties": {
                    "iri": {"type": "string", "description": f"UCUM IRI for '{obj_name}' property"}
                    },
                    "required": ["iri"],
                    "additionalProperties": False
                }
        else:
            ucum_schema = {
                "type": "object", 
                "description": f"Schema for property '{obj_name}'",
                "properties": {
                    "termid": {"type": "string", "description": "Unique ID for the given IRI. String format follows convention: <coding_system>-<identifier>"}, 
                    "iri": {"type": "string", "description": f"UCUM IRI for '{obj_name}' property"},
                    "sourceConceptID": {"type": "string", "description": "The ID of the SPHN/Project concept where this code has been used, e.g. 123"}
                    },
                    "required": ["termid", "iri"],
                    "additionalProperties": False
                }
            if enum_values:
                ucum_schema['properties']['iri']['enum'] = enum_values
        return ucum_schema
    
    @staticmethod
    def _define_source_concept_fields(object_dict: dict):
        """Create artificial properties 'sourceCodeType' and 'sourceCodeID' for supporting concepts properties of type Code/Terminology

        Args:
            object_dict (dict): map of the property
        """
        object_dict['properties']['sourceConceptType'] = {
            "description": "The SPHN/Project concept type where this code has been used, e.g. sphn-Consent. Naming convention: <prefix>-<conceptName>",
            "type": "string"
        }
        object_dict['properties']['sourceConceptID'] = {
            "description": "The ID of the SPHN/Project concept where this code has been used, e.g. 123",
            "type": "string"
        }
        object_dict["required"].append('sourceConceptType')
        object_dict["required"].append('sourceConceptID')
        
    def get_terminology_schema(self, concept_type: ConceptType, break_next: bool = False, root_concept: bool = False) -> dict:
        """Gets schema of Terminology class
        
        Args:
            concept_type (ConceptType): concept type
            break_next (bool, optional): break second iteration of concept in loop. Defaults to False.
            root_concept (bool, optional): class in range derived from root concept. Defaults to False.

        Returns:
            dict: Terminology class schema
        """
        if concept_type == ConceptType.SUPPORTING or break_next:
            if root_concept:
                required_fields = ['target_concept', 'termid']
            else:
                required_fields = ['termid']
        else:
            required_fields = ['termid', 'iri']
        
        terminology_schema = {
            "type": "object", 
            "description": "SPHN Concept 'Terminology'",
            "properties": {
                "termid": {"type": "string", "description": "Unique ID for the given IRI. String format follows convention: <coding_system>-<identifier>"}, 
                "iri": {"type": "string", "description": "IRI of SPHN Concept 'Terminology'"}
                },
            "required": required_fields,
            "additionalProperties": False
            }
        if root_concept:
            terminology_schema['properties']["target_concept"] = {
                "description": "IRI for Concept 'Terminology'",
                "enum": ["https://biomedit.ch/rdf/sphn-schema/sphn#Terminology"],
                "type": "string"
                }
        if not self.is_legacy:
            terminology_schema['properties']['sourceConceptID'] = {"type": "string", "description": "The ID of the SPHN/Project concept where this code has been used, e.g. 123"}
        if concept_type == ConceptType.SUPPORTING:
            JSONSchemaGenerator._define_source_concept_fields(object_dict=terminology_schema)
        return terminology_schema

    def cleanup_supporting_concepts(self):
            """Cleanup supporting_concepts before RML generation
            """
            if self.database is not None:
                self.database.create_connection()
                query = "DELETE FROM supporting_concepts WHERE data_provider_id=%s AND project_name=%s"
                self.database.cursor.execute(query, (self.database.config.data_provider_id, self.project_name))
                self.database.close_connection()

    def track_supporting_dpi_property(self, property_key: str):
        """Track supporting concepts that have property sphn:hasDataProvider

        Args:
            property_key (str): property key
        """
        if self.database is not None:
            self.database.create_connection()
            table_name = property_key.replace(':', '_')
            query =  "INSERT INTO supporting_concepts (data_provider_id, project_name, table_name) VALUES (%s, %s, %s) ON CONFLICT DO NOTHING"
            self.database.cursor.execute(query, (self.database.config.data_provider_id, self.project_name, table_name))
            self.database.close_connection()

class RMLGenerator(object):
    """Class containing methods and variables for the RML mapping generation
    """

    def __init__(self, 
                 schema_graph: Graph, 
                 restrictions: dict, 
                 input_path: str, 
                 output_path: str, 
                 data_provider_id: str, 
                 project_name: str, 
                 database : Database, 
                 project_version_iri: str, 
                 restrictions_map: dict, 
                 sphn_base_iri: str, 
                 core_concepts: set, 
                 supporting_concepts: set, 
                 root_nodes: set, 
                 project_iri: str):
        """Class constructor

        Args:
            schema_graph (Graph): schema graph
            restrictions (dict): map of SPHN classes restrictions
            input_path (str): path of the input JSON file with patient data
            output_path (str): path of the RML mapping file to generate
            data_provider_id (str): data provider ID
            project_name (str): name of the project
            database (Database): database object
            project_version_iri (str): project/sphn owl:versionIRI
            restrictions_map (dict): restrictions map
            sphn_base_iri (str): SPHN base IRI
            core_concepts (set): core concepts
            supporting_concepts (set): supporting concepts
            root_nodes (set): root nodes
            project_iri (str): project base IRI
        """
        self.schema_graph = schema_graph
        self.rml_graph = Graph()
        self.restrictions = restrictions
        self.input_path = input_path
        self.output_path = output_path
        self.data_provider_id = data_provider_id
        self.project_name = project_name
        self.database = database
        self.sphn_base_iri = sphn_base_iri
        self.data_provider_class = Database._get_data_provider_class(sphn_base_iri=sphn_base_iri)
        self.data_provider_property = f"has{self.data_provider_class}"
        self.apply_namespace()
        self.map_data_release(project_version_iri=project_version_iri)
        self.restrictions_map = restrictions_map
        self.core_concepts = core_concepts
        self.supporting_concepts = supporting_concepts
        self.is_legacy = Database._is_legacy_schema(sphn_base_iri=self.sphn_base_iri)
        self.root_nodes = root_nodes
        self.project_iri = project_iri
        self.valuesets = get_all_subclasses(graph=self.schema_graph, concept=URIRef(f"{self.sphn_base_iri}#ValueSet"), sphn_namespace=self.sphn_base_iri, root_nodes=self.root_nodes)
        
    def generate_rml_mapping(self):
        """Generates RML mapping for core concept classes
        """
        logging.info("Generating RML mapping")
        if self.database is not None:
            self.database.update_rml_json_logging(project_name=self.project_name, status=RML_JSON_LOGS.RMLGenerator, message="Generating RML mapping")
        
        # Extract RML mapping for core/supporting concepts
        for classiri in self.core_concepts:
            self.map_core_concept(classiri=classiri, concept_type=ConceptType.CORE)
        for classiri in self.supporting_concepts:
            self.map_core_concept(classiri=classiri, concept_type=ConceptType.SUPPORTING)
        
        export_graph(graph=self.rml_graph, output=self.output_path)
        if self.database is not None:
            self.database.update_rml_json_logging(project_name=self.project_name, status=RML_JSON_LOGS.EXPORT_GRAPH, message = f"Exporting RML graph to file: {self.output_path}")

    def map_core_concept(self, classiri: URIRef, concept_type: ConceptType):
        """Generates RML mapping for core concept class

        Args:
            classiri (URIRef): SPHN class
            concept_type (ConceptType): concept type
        """
        all_dataproperties = list(self.schema_graph.subjects(RDF.type, OWL.DatatypeProperty))
        pseudoprefix, name = niceify_prefix(prefixable_iri=classiri)

        # Define JSON path and mapping name for the concept
        if name in [self.data_provider_class, 'SubjectPseudoIdentifier']:
            # These concepts are defined at the top level of the JSON schema
            json_content_path = "$."
            parent_path = json_content_path + pseudoprefix + ":" + name
        else:
            # All other concepts are defined either under the 'content' field or the 'supporting_concepts' field
            json_content_path = f"$.{concept_type.value}."
            parent_path = json_content_path + pseudoprefix + ":" + name + "[*]"
        
        mapping_name = pseudoprefix + name
        mapping_uri = URIRef(BASE + mapping_name)

        # Define the mapping for the concept
        # Beside the code we will put examples of the effect of the commands on the RML mapping file. 
        # Please refere to the generated RML mapping file for more details. We use the sphn:Allergy concept as referece.

        # Define triples map --> :sphnAllergy a rr:TriplesMap ;
        self.rml_graph.add((mapping_uri, RDF.type, RR.TriplesMap))

        # Define triple --> :sphnAllergy rml:logicalSource [ ] ;
        logical_source_node = BNode()
        self.rml_graph.add((mapping_uri, RML.logicalSource, logical_source_node))

        # Add (rml:source "patient_data.json") to logical source
        self.rml_graph.add((logical_source_node, RML.source, Literal(self.input_path)))

        # Add (rml:referenceFormulation ql:JSONPath) to logical source
        self.rml_graph.add((logical_source_node, RML.referenceFormulation, QL.JSONPath))

        if name in ["SubjectPseudoIdentifier", self.data_provider_class]:   
            # Add (rml:iterator "$.sphn:SubjectPseudoIdentifier") to logical source               
            self.rml_graph.add((logical_source_node, RML.iterator, Literal(json_content_path + pseudoprefix + ":" + name)))
        else:
            # Add (rml:iterator "$.content.sphn:Allergy[*]") to logical source
            self.rml_graph.add((logical_source_node, RML.iterator, Literal(json_content_path + pseudoprefix + ":" + name + "[*]")))
        
        # Define subject map --> :sphnAllergy rr:subjectMap [].
        subject_map_node = BNode()
        self.rml_graph.add((mapping_uri, RR.subjectMap, subject_map_node))

        # Add (rr:template "https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-sphn-Allergy-{id}") to subject map
        self.rml_graph.add((subject_map_node, RR.template, Literal(RESOURCE+ f"{self.data_provider_id}-{pseudoprefix}-{name}-{{id}}")))

        # Add (rr:class sphn:Allergy) to subject map
        self.rml_graph.add((subject_map_node, URIRef(str(RR)+"class"), classiri))

        # Extract all data-type properties and generate the mappings
        domain_class = list(self.schema_graph.subjects(RDFS.domain, classiri))
        dataproperties_for_class = list(set(all_dataproperties) & set(domain_class))
        for prop in dataproperties_for_class:
            self.map_datatype_property(classiri=classiri, prop=prop, mapping_uri=mapping_uri)
        
        # Extract all object properties and generate the mappings
        objectproperties_for_class = list(set(domain_class) - set(all_dataproperties))
        logging.debug(f"Class '{name}' has {len(objectproperties_for_class)} object properties")
        objmap = get_prefix_objects_map(objectproperties_for_class=objectproperties_for_class)
        
        # Loop over each object to map
        for obj in objectproperties_for_class:

            # Generate recursive mapping for the object property
            if self.is_legacy:
                next_restrictions_key = str(classiri) + str(obj)
            else:
                next_restrictions_key = str(classiri) + f"[{pseudoprefix}{name}]" + str(obj)
            self.map_object_property(classiri=classiri, obj=obj, mapping_uri=mapping_uri, parent_path=parent_path, parent_obj=name, restrictions_key=next_restrictions_key, concept_type=concept_type)
            
            # Check if the object property is an array, i.e. it is defined with max-cardinality > 1
            is_array = has_higher_cardinality(classiri=classiri, obj=obj, restrictions=self.restrictions, sphn_base_iri=self.sphn_base_iri)
            obj_pseudoprefix, obj_name = niceify_prefix(prefixable_iri=obj)
            
            # Create ad-hoc mappings for special concepts and predicate maps
            if is_array or (not has_value_set(classiri=classiri, obj=obj, schema_graph=self.schema_graph, restrictions=self.restrictions) and not has_ucums(classiri=classiri, obj=obj, restrictions=self.restrictions)):
                if obj_name in ['hasSubjectPseudoIdentifier', self.data_provider_property]:
                    skip_mapping = pseudoprefix != 'sphn' and obj_pseudoprefix == 'sphn' and pseudoprefix in objmap and obj_name in objmap[pseudoprefix]
                    self.map_join_condition(classiri=classiri, obj=obj, mapping_uri=mapping_uri, skip_mapping=skip_mapping, restrictions_key=next_restrictions_key)
                elif self.is_legacy:
                    if obj_name != 'hasAdministrativeCase':
                        self.create_predicate_object_map(classiri=classiri, obj=obj, mapping_uri=mapping_uri, is_array=is_array, restrictions_key=next_restrictions_key, concept_type=concept_type)
                else:
                    self.create_predicate_object_map(classiri=classiri, obj=obj, mapping_uri=mapping_uri, is_array=is_array, restrictions_key=next_restrictions_key, concept_type=concept_type)

    def map_object_property(self, 
                            classiri: URIRef, 
                            obj: URIRef, 
                            mapping_uri: URIRef, 
                            restrictions_key: str, 
                            concept_type: ConceptType, 
                            parent_path: str = None, 
                            parent_obj: str = None, 
                            break_next: bool = False):
        """Generates RML mapping for object property

        Args:
            classiri (URIRef): concept class
            obj (URIRef): object property
            mapping_uri (URIRef): mapping URI
            restrictions_key (str): transversed path for restrictions check
            concept_type (ConceptType): type of the concept
            parent_path (str, optional): path to parent object. Defaults to None.
            parent_obj (str, optional): parent object. Defaults to None.
            break_next (bool, optional): break second iteration of concept in loop. Defaults to False.
        """
        _, upper_mapping_name = niceify_prefix(prefixable_iri=mapping_uri)
        pseudoprefix, obj_name = niceify_prefix(prefixable_iri=obj)
        classiri_pseudoprefix, classiri_name = niceify_prefix(prefixable_iri=classiri)
        
        # Define prefix for mapping name
        if classiri_name in [self.data_provider_class, 'SubjectPseudoIdentifier']:
            content_path_prefix = f"{classiri_pseudoprefix}:{classiri_name}"
        else:
            content_path_prefix = concept_type.value
        
        # Check cardinality of object property an define if it can be defined as an array
        is_array = has_higher_cardinality(classiri=classiri, obj=obj, restrictions=self.restrictions, sphn_base_iri=self.sphn_base_iri)
        
        if has_value_set(classiri=classiri, obj=obj, schema_graph=self.schema_graph, restrictions=self.restrictions):
            # Special logic to map object properties defined as sphn:ValueSet
            _, additional_classes, _ = get_value_set(classiri=classiri, obj=obj, schema_graph=self.schema_graph, restrictions=self.restrictions)
            if additional_classes:
                for additional_class in additional_classes:
                    self.map_extra_class_join_condition(classiri=additional_class, obj=obj, mapping_uri=mapping_uri, restrictions_key=restrictions_key, concept_type=concept_type)
            self.map_value_set(obj=obj, mapping_uri=mapping_uri)
        elif has_ucums(classiri=classiri, obj=obj, restrictions=self.restrictions) and self.is_legacy:
            # Special logic for legacy RDF schemas to map properties with UCUM values 
            self.map_value_set(obj=obj, mapping_uri=mapping_uri)
        else:
            if obj_name in [self.data_provider_property, 'hasSubjectPseudoIdentifier']:
                # Special core concepts are always referenced by the concept mapping and therefore not mapped at the lower level
                return
            elif self.is_legacy and obj_name == 'hasAdministrativeCase':
                # Special logic for legacy RDF schema to map property referencing sphn:AdministrativeCase
                self.create_predicate_object_map(classiri=classiri, obj=obj, mapping_uri=mapping_uri, is_array=is_array, restrictions_key=restrictions_key + str(obj), concept_type=concept_type)
                return
            else:
                # Extract list of classes in the object property's range
                classrefs, root_concept = get_range(classiri=classiri, schema_graph=self.schema_graph, obj=obj, restrictions=self.restrictions, restrictions_key=restrictions_key, restrictions_map=self.restrictions_map, sphn_base_iri=self.sphn_base_iri,
                                                    is_legacy=self.is_legacy, root_nodes=self.root_nodes, project_iri=self.project_iri)
                code_and_term_list = [URIRef(f'{self.sphn_base_iri}#Terminology'), URIRef(f'{self.sphn_base_iri}#Code')]
                multi_range = len(classrefs) > 1

                if concept_type == ConceptType.SUPPORTING or break_next:
                    # Stop the recursivity for supporting concepts' properties or properties leading to a direct cycle
                    classrefprefix, classrefname = niceify_prefix(prefixable_iri=classrefs[0])
                    if not multi_range and classrefname not in ['Code', 'Terminology']:
                        if self.is_legacy:
                            new_restrictions_key = restrictions_key + str(obj)
                        else:
                            new_restrictions_key = restrictions_key + f"[{classrefprefix}{classrefname}]" + str(obj)
                        self.create_predicate_object_map(classiri=classiri, obj=obj, mapping_uri=mapping_uri, is_array=is_array, restrictions_key=new_restrictions_key, concept_type=concept_type)
                        return
                    if break_next:
                        return
                    
                is_valueset_mapping_defined = False
                class_mapping_uri = mapping_uri

                for classref in classrefs:
                    # Loop over all the classes in the property's range
                    
                    if classref == URIRef(f"{self.sphn_base_iri}#ValueSet"):
                        # Skip parent class ValueSet
                        continue

                    if classref in self.valuesets:
                        # Map ValueSet children
                        if not is_valueset_mapping_defined and root_concept:
                            self.map_value_set(obj=obj, mapping_uri=class_mapping_uri, supporting_concept=True)
                            is_valueset_mapping_defined = True
                        continue

                    classref_pseudoprefix, classref_name = niceify_prefix(prefixable_iri=classref) 

                    if not self.is_legacy and not multi_range and classref in self.core_concepts and classref not in code_and_term_list:
                        # From RDF schema 2024.2, all the object properties referring to a core concept define only the reference ID field 
                        # and therefore no additional mapping of the properties is necessary
                        return
                    
                    if classiri in classrefs:    
                        # Condition for a direct cycle in the graph (only 'id' field mapped)
                        break_next = True
                    
                    # Define JSON path of the object property
                    if classref_name in [self.data_provider_class, 'SubjectPseudoIdentifier']:
                        json_content_path = f"$.{pseudoprefix}:{classref}"
                    else:
                        if is_array:
                            if parent_obj:
                                json_content_path = f"{parent_path}.{pseudoprefix}:{obj_name}[*]"
                            else:
                                # Apparently never matching this condition
                                json_content_path = f"$.{content_path_prefix}.{pseudoprefix}:{obj_name}[*]"
                        else:
                            if parent_obj:
                                json_content_path = f"{parent_path}.{pseudoprefix}:{obj_name}"
                            else:
                                # Apparently never matching this condition
                                json_content_path = f"$.{content_path_prefix}.{pseudoprefix}:{obj_name}"

                    if multi_range and (classref not in code_and_term_list or root_concept):
                        # When an object has multiple classes in range we define the 'target_concept' field in the JSON schema. The JSON path should be then defined accordingly
                        # Re-use upper path defined above and add the condition. Strip '[*]' if the object is an array as it doesn't make a difference for the condition
                        json_content_path = f"{json_content_path.rstrip('[*]')}[?(@.target_concept=='{classref}')]"

                    # Define mapping name and URI and skip the mapping generation if a mapping with the same name is already available inside the RML graph
                    # For example for sphn:Allergy/sphn:hasAllergy we define the mapping 'sphnAllergy_sphnhasAllergen_rangesphnAllergen'
                    mapping_name = f"{upper_mapping_name}_{pseudoprefix}{obj_name}_range{classiri_pseudoprefix}{classref_name}"
                    mapping_uri = URIRef(BASE + mapping_name)
                    if (mapping_uri, None, None) in self.rml_graph:
                        continue

                    if not self.is_legacy and classref not in code_and_term_list:
                        # From RDF schema 2024.2 we do not map the full core concepts but only the ID field. Same logic is applied to supporting concepts
                        if (multi_range and classref in self.core_concepts) or (concept_type == ConceptType.SUPPORTING):
                            continue

                    # Generate the RML mapping of the object property
                    # Define triples map of the referenced object property class (e.g. let's consider sphn:Allergy/sphn:hasAllergen) -->  :sphnAllergy_sphnhasAllergen_rangesphnAllergen a rr:TriplesMap ;            
                    self.rml_graph.add((mapping_uri, RDF.type, RR.TriplesMap))

                    # Define logical source --> :sphnAllergy_sphnhasAllergen_rangesphnAllergen rml:logicalSource [ ];
                    logical_source_node = BNode()
                    self.rml_graph.add((mapping_uri, RML.logicalSource, logical_source_node))

                    # Add (rml:source "patient_data.json" ) to logical source
                    self.rml_graph.add((logical_source_node, RML.source, Literal(self.input_path)))

                    # Add (rml:referenceFormulation ql:JSONPath) to logical source
                    self.rml_graph.add((logical_source_node, RML.referenceFormulation, QL.JSONPath))

                    # Add (rml:iterator "$.content.sphn:Allergy[*].sphn:hasAllergen") to logical source
                    self.rml_graph.add((logical_source_node, RML.iterator, Literal(json_content_path)))

                    # Define subject map --> :sphnAllergy_sphnhasAllergen_rangesphnAllergen rr:subjectMap [ ]
                    subject_map_node = BNode()
                    self.rml_graph.add((mapping_uri, RR.subjectMap, subject_map_node))

                    if classref_name == 'Terminology':
                        if not self.is_legacy:
                            if concept_type == ConceptType.SUPPORTING:
                                # Add (rr:template "https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-{sourceConceptType}-{sourceConceptID}-sphn-Code-{termid}") to subject map
                                self.rml_graph.add((subject_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{{sourceConceptType}}-{{sourceConceptID}}-{classref_pseudoprefix}-Code-{{termid}}")))
                            else:
                                # Add (rr:template "https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-sphn-AllergyEpisode-{sourceConceptID}-sphn-Code-{termid}") to subject map
                                self.rml_graph.add((subject_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{classiri_pseudoprefix}-{classiri_name}-{{sourceConceptID}}-{classref_pseudoprefix}-Code-{{termid}}")))
                        else:
                            # This case is not available in the current SPHN schema but the logic would be similar as the above examples
                            self.rml_graph.add((subject_map_node, RR.template, Literal(RESOURCE + classref_pseudoprefix + "-Code-{termid}")))
                    else:
                        if classref_name == 'Code' and not self.is_legacy:
                            if concept_type == ConceptType.SUPPORTING:
                                # Add (rr:template "https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-{sourceConceptType}-{sourceConceptID}-sphn-Code-{id}") to subject map
                                self.rml_graph.add((subject_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{{sourceConceptType}}-{{sourceConceptID}}-{classref_pseudoprefix}-{classref_name}-{{id}}")))
                            else:
                                # Add ("https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-sphn-Allergen-{sourceConceptID}-sphn-Code-{id}") to subject map
                                self.rml_graph.add((subject_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{classiri_pseudoprefix}-{classiri_name}-{{sourceConceptID}}-{classref_pseudoprefix}-{classref_name}-{{id}}")))
                        else:
                            # Add (rr:template "https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-sphn-Allergen-{id}") to subject map
                            self.rml_graph.add((subject_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{classref_pseudoprefix}-{classref_name}-{{id}}")))
                        
                        # Add (rr:class sphn:Allergen) to subject map
                        self.rml_graph.add((subject_map_node, URIRef(str(RR)+"class"), classref))
                    
                    if multi_range and classref not in code_and_term_list and self.is_legacy:
                        # Legacy logic to map properties related to a specific 'target_concept'
                        self.safeguard_relationship_mapping(classiri=classiri, classref=classref, obj=obj, mappingname=mapping_name, content_path_prefix=content_path_prefix)
                        
                    # Extract all data-type properties of the object's class in range
                    all_dataproperties = list(self.schema_graph.subjects(RDF.type, OWL.DatatypeProperty))
                    domain_class = list(self.schema_graph.subjects(RDFS.domain, classref))
                    dataproperties_for_class = list(set(all_dataproperties) & set(domain_class))
                    logging.debug(f"Class '{classref_name}' has {len(dataproperties_for_class)} data properties")
                    
                    if classref_name == 'Terminology':
                        # Ad-hoc mapping rules for sphn:Terminology class
                        # Define predicate object map --> :sphnExposure_sphnhasAgentCode_rangesphnTerminology rr:predicateObjectMap []
                        predicate_object_map_node = BNode()
                        self.rml_graph.add((mapping_uri, RR.predicateObjectMap, predicate_object_map_node))

                        # Add (rr:predicate rdf:type) to predicate object map
                        self.rml_graph.add((predicate_object_map_node, RR.predicate, RDF.type))
                        
                        # Define object map inside predicate object map --> rr:objectMap []
                        object_map_node = BNode()
                        self.rml_graph.add((predicate_object_map_node, RR.objectMap, object_map_node))

                        # Add (rml:reference "iri") to object map
                        self.rml_graph.add((object_map_node, RML.reference, Literal("iri")))

                        # Add (rr:termType rr:IRI) to object map
                        self.rml_graph.add((object_map_node, RR.termType, RR.IRI))

                    # Map all the data-type properties
                    for prop in dataproperties_for_class:
                        self.map_datatype_property(classiri=classref, prop=prop, mapping_uri=mapping_uri)

                    # Extract all object properties of the object's class in range
                    objectproperties_for_class = list(set(domain_class) - set(all_dataproperties))
                    objmap = get_prefix_objects_map(objectproperties_for_class=objectproperties_for_class)
                    logging.debug(f"Class '{classref_name}' has {len(objectproperties_for_class)} object properties")
                    
                    # Loop over the class in range objects and generate RML mappings
                    for inner_obj in objectproperties_for_class:
                        inner_obj_pseudoprefix, inner_obj_name = niceify_prefix(prefixable_iri=inner_obj)
                        inner_obj_is_array = has_higher_cardinality(classiri=classref, obj=inner_obj, restrictions=self.restrictions, sphn_base_iri=self.sphn_base_iri)
                        # Create an object mapping of the current object such that all objects are mapped. This must be explicitly stated otherwise only object mappings of objects 
                        # defined at the top level of a core concept will be instantiated
                        
                        # Generate recursive mapping for the object property
                        if str(inner_obj) in restrictions_key:
                            break_next = True  
                        if self.is_legacy:
                            next_restrictions_key = restrictions_key + str(inner_obj)
                        else:
                            next_restrictions_key = restrictions_key + f"[{classref_pseudoprefix}{classref_name}]" + str(inner_obj)
                        self.map_object_property(classiri=classref, obj=inner_obj, mapping_uri=mapping_uri, parent_path=json_content_path, parent_obj=obj_name, restrictions_key=next_restrictions_key, concept_type=concept_type, break_next=break_next) 
                        
                        # Create ad-hoc mappings for special concepts and predicate maps
                        if inner_obj_is_array or (not has_value_set(classiri=classref, obj=inner_obj, schema_graph=self.schema_graph, restrictions=self.restrictions) and not has_ucums(classiri=classref, obj=inner_obj, restrictions=self.restrictions)):
                            if inner_obj_name in ['hasSubjectPseudoIdentifier', self.data_provider_property]:
                                skip_mapping = classref_pseudoprefix != 'sphn' and inner_obj_pseudoprefix == 'sphn' and classref_pseudoprefix in objmap and obj_name in objmap[classref_pseudoprefix]
                                self.map_join_condition(classiri=classref, obj=inner_obj, mapping_uri=mapping_uri, skip_mapping=skip_mapping, restrictions_key=next_restrictions_key)
                            else:
                                self.create_predicate_object_map(classiri=classref, obj=inner_obj, mapping_uri=mapping_uri, is_array=inner_obj_is_array, restrictions_key=next_restrictions_key, concept_type=concept_type)
                        elif has_ucums(classiri=classref, obj=inner_obj, restrictions=self.restrictions) and not self.is_legacy:
                            self.create_predicate_object_map(classiri=classref, obj=inner_obj, mapping_uri=mapping_uri, is_array=inner_obj_is_array, restrictions_key=next_restrictions_key, concept_type=concept_type)
        
    def safeguard_relationship_mapping(self, 
                                       classiri: URIRef, 
                                       classref:URIRef, 
                                       obj: URIRef, 
                                       mappingname: str, 
                                       content_path_prefix: str):
        """In case we encounter a one of relationship in the json we add an additonal mapping to only generate prediate_object relationships when they are needed
           An example for this would be in the version 2023 of the sphn schema the hasIndicationToStart property of the DrugPrescription core concept.
           hasIndicationtoStart can link to a Diagnosis (and its children NursingDiagnosis, FOPHDiagnosis etc. ) and Intent.
           To only create a hasIndication mapping for a relationship if it is actually there this function has been introduced
        
        Args:
            classiri (URIRef): concept class
            classref (URIRef): object to be modeled
            prop (URIRef): datatype property
            mapping_uri (URIRef): mapping URI
            content_path_prefix (str): content JSON path prefix
        """
        # This is legacy code which was at some point relevant for 2023.2 SPHN schema. It is possible that it can be completely removed.
        prefix_core, core_concept = niceify_prefix(classiri)
        concept_prefix, concept = niceify_prefix(classref)
        pseudoprefix, obj_name = niceify_prefix(obj)
        
        json_content_path = f"$.{content_path_prefix}..{prefix_core}:{core_concept}[?(@.{pseudoprefix}:{obj_name}.target_concept=='{classref}')]"
        
        specific_mapping_name = f"{mappingname}_Availability"
        specific_mapping_uri = URIRef(BASE + specific_mapping_name)
        
        self.rml_graph.add((specific_mapping_uri, RDF.type, RR.TriplesMap))
        additional_source_node = BNode()
        self.rml_graph.add((specific_mapping_uri, RML.logicalSource, additional_source_node))
        self.rml_graph.add((additional_source_node, RML.source, Literal(self.input_path)))
        self.rml_graph.add((additional_source_node, RML.referenceFormulation, QL.JSONPath))
        self.rml_graph.add((additional_source_node, RML.iterator, Literal(json_content_path))) 
                        
        additional_predicate_object_map_node = BNode()
        self.rml_graph.add((specific_mapping_uri, RR.predicateObjectMap, additional_predicate_object_map_node))
        self.rml_graph.add((additional_predicate_object_map_node, RR.predicate, obj))
        
        additional_object_map_node = BNode() 
        self.rml_graph.add((additional_predicate_object_map_node, RR.objectMap, additional_object_map_node))
        if concept == 'Terminology':
            self.rml_graph.add((additional_object_map_node, RR.template, Literal(RESOURCE + f"{concept_prefix}-Code-{{{pseudoprefix}:{obj_name}.termid}}")))
        else:
            self.rml_graph.add((additional_object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{concept_prefix}-{concept}-{{{pseudoprefix}:{obj_name}.id}}")))
        
        subject_map_node = BNode()
        self.rml_graph.add((specific_mapping_uri, RR.subjectMap, subject_map_node))
        self.rml_graph.add((subject_map_node, RR.template, Literal(RESOURCE+ f"{self.data_provider_id}-{prefix_core}-{core_concept}-{{id}}")))
        self.rml_graph.add((subject_map_node, URIRef(str(RR)+"class"), classiri))
    
                        
    def map_datatype_property(self, classiri: URIRef, prop: URIRef, mapping_uri: URIRef):
        """Generates RML mapping for datatype property

        Args:
            classiri (URIRef): concept class
            prop (URIRef): datatype property
            mapping_uri (URIRef): mapping URI
        """
        pseudoprefix, prop_name = niceify_prefix(prefixable_iri=prop)
        
        # Extract the range of the data-type property
        range_of_datatypes = list(self.schema_graph.objects(prop, RDFS.range))

        # is_array = self.restrictions[classiri][str(prop)].get('maxCardinality') != "1"
        if len(range_of_datatypes) > 1:
            if 'valueset' in self.restrictions[classiri][str(prop)]:
                # If a valueset is defined for the data-type property, use the first element (usually only one is available)
                datatype = URIRef(self.restrictions[classiri][str(prop)]['valueset'][0])
            else:
                class_prefix, class_name = niceify_prefix(prefixable_iri=classiri)
                if self.database is not None:
                    self.database.update_rml_json_logging(project_name=self.project_name, status=RML_JSON_LOGS.RML_DATATYPE_WARNING, message="Missing restrictions for some objects. String type is enforced")
                logging.warning(f"RML mapping -- No restrictions found on type for object '{pseudoprefix}:{prop_name}' for class '{class_prefix}:{class_name}'. Enforcing string type")
                datatype = URIRef('http://www.w3.org/2001/XMLSchema#string')
        else:
            datatype = range_of_datatypes[0]

        # Define predicate object map --> :sphnAllergy rr:predicateObjectMap [ ];
        predicate_object_map_node = BNode()
        self.rml_graph.add((mapping_uri, RR.predicateObjectMap, predicate_object_map_node))

        # Add (rr:predicate sphn:hasLastReactionDateTime) to predicate object map
        self.rml_graph.add((predicate_object_map_node, RR.predicate, prop))

        # Define object map inside predicate object map  --> rr:objectMap []
        object_map_node = BNode()
        self.rml_graph.add((predicate_object_map_node, RR.objectMap, object_map_node))

        # In case the maxCardinality != 1 then we should define it as an array. Nevertheless, rml:reference "sphn:hasSharedIdentifier" is equivalent to 
        # rml:reference "sphn:hasSharedIdentifier" when a list of objects is passed 
        # We should eventually add
        # is_array = self.restrictions[classiri][str(prop)].get('maxCardinality') != "1"
        # if is_array:
        #   self.rml_graph.add((object_map_node, RML.reference, Literal(pseudoprefix + ":" + prop_name + '[*]')))

        # Add (rml:reference "sphn:hasLastReactionDateTime") to object map
        self.rml_graph.add((object_map_node, RML.reference, Literal(pseudoprefix + ":" + prop_name)))

        # Add (rr:datatype xsd:dateTime) to object map
        self.rml_graph.add((object_map_node, RR.datatype, datatype))

    def create_predicate_object_map(self, 
                                    classiri: URIRef, 
                                    obj: URIRef, 
                                    mapping_uri: URIRef, 
                                    is_array: bool, 
                                    restrictions_key: str, 
                                    concept_type: ConceptType):
        """Generate predicate object map for a given object. This is the mapping that from an object property defines the associated resource. That resource is then
           defined elsewhere by its mapping rules.

        Args:
            classiri (URIRef): class IRI
            obj (URIRef): object IRI
            mapping_uri (URIRef): mapping IRI
            is_array (bool): is an array property
            restrictions_key (str): transversed path for restrictions check
            concept_type (ConceptType): concept type
        """
        pseudoprefix, obj_name = niceify_prefix(prefixable_iri=obj)
        prefix_class_where_is_used, class_where_is_used = niceify_prefix(prefixable_iri=classiri)
        classrefs, root_concept = get_range(classiri=classiri, schema_graph=self.schema_graph, obj=obj, restrictions=self.restrictions, restrictions_key=restrictions_key, restrictions_map=self.restrictions_map, sphn_base_iri=self.sphn_base_iri,
                                            is_legacy=self.is_legacy, root_nodes=self.root_nodes, project_iri=self.project_iri)
        code_and_term_list = [URIRef(f'{self.sphn_base_iri}#Terminology'), URIRef(f'{self.sphn_base_iri}#Code')]
        multi_range = len(classrefs) > 1
        for classref in classrefs:

            if classref == URIRef(f"{self.sphn_base_iri}#ValueSet"):
                # Skip mapping if the class in range is equal to sphn:ValueSet
                continue
            
            if classref in self.valuesets and root_concept:
                # Skip mapping if the class in range is a child of sphn:ValueSet and a root concept is in the range of the object property
                continue

            if self.is_legacy and multi_range and classref not in code_and_term_list:
                # Skip mapping for legacy RDF schema when there are multiple classes in range but the class in range 
                # processed is not sphn:Code or sphn:Terminology
                continue

            classref_prefix, classref_name = niceify_prefix(prefixable_iri=classref)

            # Define predicate object map --> :sphnAllergy rr:predicateObjectMap [ ];
            predicate_object_map_node = BNode()
            self.rml_graph.add((mapping_uri, RR.predicateObjectMap, predicate_object_map_node))

            # Add (rr:predicate sphn:hasSourceSystem) to predicate object map
            self.rml_graph.add((predicate_object_map_node, RR.predicate, obj))

            # Define object map inside predicate object map  --> rr:objectMap []
            object_map_node = BNode()
            self.rml_graph.add((predicate_object_map_node, RR.objectMap, object_map_node))

            if classref_name == 'Terminology':
                # Mapping logic for class in range of type sphn:Terminology
                if not self.is_legacy and root_concept:
                    if concept_type == ConceptType.SUPPORTING:
                        if is_array:
                            # Special logic for sphn:Interpretation/sphn:hasInput/hasOutput where Code/Terminology are inside array field and have condition on target_concept
                            # Add ("https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-{sphn:hasOutput[*].sourceConceptType}-{sphn:hasOutput[*].sourceConceptID}-sphn-Code-{sphn:hasOutput[?(@.target_concept=='https://biomedit.ch/rdf/sphn-schema/sphn#Terminology')].termid}") to object map
                            self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{{{pseudoprefix}:{obj_name}[*].sourceConceptType}}-{{{pseudoprefix}:{obj_name}[*].sourceConceptID}}-{classref_prefix}-Code-{{{pseudoprefix}:{obj_name}[?(@.target_concept=='{classref}')].termid}}")))
                        else:
                            # Add (rr:template "https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-{sphn:hasMethodCode.sourceConceptType}-{sphn:hasMethodCode.sourceConceptID}-sphn-Code-{sphn:hasMethodCode.termid}") to object map
                            self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{{{pseudoprefix}:{obj_name}.sourceConceptType}}-{{{pseudoprefix}:{obj_name}.sourceConceptID}}-{classref_prefix}-Code-{{{pseudoprefix}:{obj_name}[?(@.target_concept=='{classref}')].termid}}")))
                    else:
                        # This case is not available in the current SPHN schema but the logic would be similar as the above examples
                        self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{prefix_class_where_is_used}-{class_where_is_used}-{{id}}-{classref_prefix}-Code-{{{pseudoprefix}:{obj_name}[?(@.target_concept=='{classref}')].termid}}")))
                elif is_array:
                    if not self.is_legacy:
                        if concept_type == ConceptType.SUPPORTING:
                            # As of 2024.1 this condition is not entered. We in any case use the same logic as above for the array fields and Code/Terminology. Structure is similar as previous examples.
                            self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{{{pseudoprefix}:{obj_name}[*].sourceConceptType}}-{{{pseudoprefix}:{obj_name}[*].sourceConceptID}}-{classref_prefix}-Code-{{{pseudoprefix}:{obj_name}[*].termid}}")))
                        else:
                            # Add (rr:template "https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-sphn-TumorSpecimen-{id}-sphn-Code-{sphn:hasMaterialTypeCode[*].termid}") to object map
                            self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{prefix_class_where_is_used}-{class_where_is_used}-{{id}}-{classref_prefix}-Code-{{{pseudoprefix}:{obj_name}[*].termid}}")))
                    else:
                        # This case is not available in the current SPHN schema but the logic would be similar as the above examples
                        self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{classref_prefix}-Code-{{{pseudoprefix}:{obj_name}[*].termid}}")))
                else:
                    if not self.is_legacy:
                        if concept_type == ConceptType.SUPPORTING:
                            # Add (rr:template "https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-{sphn:hasCode.sourceConceptType}-{sphn:hasCode.sourceConceptID}-sphn-Code-{sphn:hasCode.termid}") to object map
                            self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{{{pseudoprefix}:{obj_name}.sourceConceptType}}-{{{pseudoprefix}:{obj_name}.sourceConceptID}}-{classref_prefix}-Code-{{{pseudoprefix}:{obj_name}.termid}}")))
                        else:
                            # Add (rr:template "https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-sphn-Organism-{id}-sphn-Code-{sphn:hasCode.termid}") to object map
                            self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{prefix_class_where_is_used}-{class_where_is_used}-{{id}}-{classref_prefix}-Code-{{{pseudoprefix}:{obj_name}.termid}}")))
                    else:
                        # This case is not available in the current SPHN schema but the logic would be similar as the above examples
                        self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{classref_prefix}-Code-{{{pseudoprefix}:{obj_name}.termid}}")))
            elif is_array:
                # Mapping logic for objects with higher cardinality
                if not self.is_legacy and multi_range and set(classrefs) != set(code_and_term_list):
                    if classref_name == 'Code':
                        if concept_type == ConceptType.SUPPORTING:
                            # Special logic for sphn:Interpretation/sphn:hasInput/hasOutput where Code/Terminology are inside array field and have condition on target_concept
                            # Add (rr:template "https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-{sphn:hasOutput[*].sourceConceptType}-{sphn:hasOutput[*].sourceConceptID}-sphn-Code-{sphn:hasOutput[?(@.target_concept=='https://biomedit.ch/rdf/sphn-schema/sphn#Code')].id}") to object map
                            self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{{{pseudoprefix}:{obj_name}[*].sourceConceptType}}-{{{pseudoprefix}:{obj_name}[*].sourceConceptID}}-{classref_prefix}-{classref_name}-{{{pseudoprefix}:{obj_name}[?(@.target_concept=='{classref}')].id}}")))
                        else:
                            # This case is not available in the current SPHN schema but the logic would be similar as the above examples
                            self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{prefix_class_where_is_used}-{class_where_is_used}-{{id}}-{classref_prefix}-{classref_name}-{{{pseudoprefix}:{obj_name}[?(@.target_concept=='{classref}')].id}}")))
                    else:
                        # Add (rr:template "https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-sphn-LabTestEvent-{sphn:hasOutput[?(@.target_concept=='https://biomedit.ch/rdf/sphn-schema/sphn#LabTestEvent')].id}") to object map
                        self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{classref_prefix}-{classref_name}-{{{pseudoprefix}:{obj_name}[?(@.target_concept=='{classref}')].id}}")))
                else:
                    if not self.is_legacy and classref_name == 'Code':
                        if concept_type == ConceptType.SUPPORTING:
                            # This case is not available in the current SPHN schema but the logic would be similar as the above examples
                            self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{{{pseudoprefix}:{obj_name}.sourceConceptType}}-{{{pseudoprefix}:{obj_name}.sourceConceptID}}-{classref_prefix}-{classref_name}-{{{pseudoprefix}:{obj_name}[*].id}}")))
                        else:
                            # Add (rr:template "https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-sphn-OncologyDiagnosis-{id}-sphn-Code-{sphn:hasCode[*].id}") to object map
                            self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{prefix_class_where_is_used}-{class_where_is_used}-{{id}}-{classref_prefix}-{classref_name}-{{{pseudoprefix}:{obj_name}[*].id}}")))
                    else:
                        # Add (rr:template "https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-sphn-SourceSystem-{sphn:hasSourceSystem[*].id}") to object map
                        self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{classref_prefix}-{classref_name}-{{{pseudoprefix}:{obj_name}[*].id}}")))
            else:
                # Mapping logic for all the other cases
                if not self.is_legacy and multi_range and set(classrefs) != set(code_and_term_list):
                    if not self.is_legacy and classref_name == 'Code':
                        if concept_type == ConceptType.SUPPORTING:
                            # This case is not available in the current SPHN schema but the logic would be similar as the above examples
                            self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{{{pseudoprefix}:{obj_name}.sourceConceptType}}-{{{pseudoprefix}:{obj_name}.sourceConceptID}}-{classref_prefix}-{classref_name}-{{{pseudoprefix}:{obj_name}[?(@.target_concept=='{classref}')].id}}")))
                        else:
                            # This case is not available in the current SPHN schema but the logic would be similar as the above examples
                            self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{prefix_class_where_is_used}-{class_where_is_used}-{{id}}-{classref_prefix}-{classref_name}-{{{pseudoprefix}:{obj_name}[?(@.target_concept=='{classref}')].id}}")))
                    else:
                        # Add (rr:template "https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-sphn-ReferenceRange-{sphn:hasNumericalReference[?(@.target_concept=='https://biomedit.ch/rdf/sphn-schema/sphn#ReferenceRange')].id}") to object map
                        self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{classref_prefix}-{classref_name}-{{{pseudoprefix}:{obj_name}[?(@.target_concept=='{classref}')].id}}")))
                else:
                    if not self.is_legacy and classref_name == 'Code':
                        if concept_type == ConceptType.SUPPORTING:
                            # Add (rr:template "https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-{sphn:hasCode.sourceConceptType}-{sphn:hasCode.sourceConceptID}-sphn-Code-{sphn:hasCode.id}") to object map
                            self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{{{pseudoprefix}:{obj_name}.sourceConceptType}}-{{{pseudoprefix}:{obj_name}.sourceConceptID}}-{classref_prefix}-{classref_name}-{{{pseudoprefix}:{obj_name}.id}}")))
                        else:
                            # Add (rr:template "https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-sphn-Organism-{id}-sphn-Code-{sphn:hasCode.id}") to object map
                            self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{prefix_class_where_is_used}-{class_where_is_used}-{{id}}-{classref_prefix}-{classref_name}-{{{pseudoprefix}:{obj_name}.id}}")))
                    else:
                        # Add (rr:template "https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-sphn-Quantity-{sphn:hasQuantity.id}") to object map
                        self.rml_graph.add((object_map_node, RR.template, Literal(RESOURCE + f"{self.data_provider_id}-{classref_prefix}-{classref_name}-{{{pseudoprefix}:{obj_name}.id}}")))

    def map_value_set(self, obj: URIRef, mapping_uri: URIRef, supporting_concept: bool = False):
        """Generates RML mapping for ValueSet

        Args:
            obj (URIRef): object property
            mapping_uri (URIRef): mapping URI
            supporting_concept (bool, optional): special logic for supporting concepts. Defaults to False.
        """
        pseudoprefix, obj_name = niceify_prefix(prefixable_iri=obj)

        # Define predicate object map --> :sphnDataProvider rr:predicateObjectMap [];
        predicate_object_map_node = BNode()
        self.rml_graph.add((mapping_uri, RR.predicateObjectMap, predicate_object_map_node))

        # Add (rr:predicate sphn:hasCategory) to predicate object map
        self.rml_graph.add((predicate_object_map_node, RR.predicate, obj))

        # Define object map inside predicate object map  --> rr:objectMap [];
        object_map_node = BNode()
        self.rml_graph.add((predicate_object_map_node, RR.objectMap, object_map_node))

        if supporting_concept:
            # Add (rml:reference "sphn:hasInput[*].valueset_iri") to object map
            self.rml_graph.add((object_map_node, RML.reference, Literal(f"{pseudoprefix}:{obj_name}[*].valueset_iri")))
        else:
            # Add (rml:reference "sphn:hasCategory.iri") to object map
            self.rml_graph.add((object_map_node, RML.reference, Literal(f"{pseudoprefix}:{obj_name}.iri")))

        # Add (rr:termType rr:IRI) to object map
        self.rml_graph.add((object_map_node, RR.termType, RR.IRI))

    def map_extra_class_join_condition(self, 
                                       classiri: URIRef, 
                                       obj: URIRef, 
                                       mapping_uri: URIRef, 
                                       restrictions_key: str, 
                                       concept_type: ConceptType):
        """Generates RML mapping join condition for extra class related to ValueSet

        Args:
            classiri (URIRef): extra concept class
            obj (URIRef): object property
            mapping_uri (URIRef): mapping URI
            restrictions_key (str): transversed path for restrictions check
            concept_type (ConceptType): type of the concept
        """
        # THIS IS PROBABLY LEGACY CODE
        pseudoprefix, obj_name = niceify_prefix(prefixable_iri=obj)
        refmappingname = f"{obj_name}ClassMapping"
        if pseudoprefix != 'sphn':
            refmappingname = pseudoprefix + refmappingname
        refmapping_uri = URIRef(BASE + refmappingname)
        predicate_object_map_node = BNode()
        self.rml_graph.add((mapping_uri, RR.predicateObjectMap, predicate_object_map_node))
        self.rml_graph.add((predicate_object_map_node, RR.predicate, obj))
        object_map_node = BNode()
        self.rml_graph.add((predicate_object_map_node, RR.objectMap, object_map_node))
        self.rml_graph.add((object_map_node, RR.parentTriplesMap, refmapping_uri))   
        joinConditionNode = BNode()
        self.rml_graph.add((object_map_node, RR.joinCondition, joinConditionNode))
        self.rml_graph.add((joinConditionNode, RR.child, Literal(pseudoprefix + ":" + obj_name + ".id")))
        self.rml_graph.add((joinConditionNode, RR.parent, Literal("id")))
        self.map_extra_class_object_property(classiri=classiri, obj=obj, mappingname=refmappingname, restrictions_key=restrictions_key, concept_type=concept_type)

    def map_extra_class_object_property(self, 
                                        classiri: URIRef, 
                                        obj: URIRef, 
                                        mappingname: URIRef, 
                                        restrictions_key: str, 
                                        concept_type: ConceptType):
        """Generates RML mapping for object property for extra class

        Args:
            classiri (URIRef): extra concept class
            obj (URIRef): object property
            mappingname (URIRef): mapping name
            restrictions_key (str): transversed path for restrictions check
            concept_type (ConceptType): type of the concept
        """
        # THIS IS PROBABLY LEGACY CODE
        pseudoprefix, obj_name = niceify_prefix(prefixable_iri=obj)
        extraclass_pseudoprefix, extraclass_name = niceify_prefix(prefixable_iri=classiri)
        json_content_path = f"$..{pseudoprefix}:{obj_name}"
        mapping_uri = URIRef(BASE + mappingname)
        if (mapping_uri, None, None) in self.rml_graph:
            return
        
        self.rml_graph.add((mapping_uri, RDF.type, RR.TriplesMap))
        logical_source_node = BNode()
        self.rml_graph.add((mapping_uri, RML.logicalSource, logical_source_node))
        self.rml_graph.add((logical_source_node, RML.source, Literal(self.input_path)))
        self.rml_graph.add((logical_source_node, RML.referenceFormulation, QL.JSONPath))
        self.rml_graph.add((logical_source_node, RML.iterator, Literal(json_content_path)))
        subject_map_node = BNode()
        self.rml_graph.add((mapping_uri, RR.subjectMap, subject_map_node))
        self.rml_graph.add((subject_map_node, RR.template, Literal(RESOURCE+ f"{self.data_provider_id}-{extraclass_pseudoprefix}-{extraclass_name}-{{id}}")))
        self.rml_graph.add((subject_map_node, URIRef(str(RR)+"class"), classiri))

        all_dataproperties = list(self.schema_graph.subjects(RDF.type, OWL.DatatypeProperty))
        domain_class = list(self.schema_graph.subjects(RDFS.domain, classiri))
        dataproperties_for_class = list(set(all_dataproperties) & set(domain_class))

        for prop in dataproperties_for_class:
            self.map_datatype_property(classiri=classiri, prop=prop, mapping_uri=mapping_uri)

        objectproperties_for_class = list(set(domain_class) - set(all_dataproperties))
        objmap = get_prefix_objects_map(objectproperties_for_class=objectproperties_for_class)
        for obj in objectproperties_for_class:
            self.map_object_property(classiri=classiri, obj=obj, mapping_uri=mapping_uri, restrictions_key=restrictions_key + str(obj), concept_type=concept_type)
            if not has_value_set(classiri=classiri, obj=obj, schema_graph=self.schema_graph, restrictions=self.restrictions) and not has_ucums(classiri=classiri, obj=obj, restrictions=self.restrictions):
                obj_pseudoprefix, obj_name = niceify_prefix(prefixable_iri=obj)
                if obj_name in ['hasSubjectPseudoIdentifier', self.data_provider_property]:
                    skip_mapping = extraclass_pseudoprefix != 'sphn' and obj_pseudoprefix == 'sphn' and extraclass_pseudoprefix in objmap and obj_name in objmap[extraclass_pseudoprefix]
                    self.map_join_condition(classiri=classiri, obj=obj, mapping_uri=mapping_uri, skip_mapping=skip_mapping, restrictions_key=restrictions_key + str(obj))
                else:
                    self.create_predicate_object_map(classiri=classiri, obj=obj, mapping_uri=mapping_uri, is_array=False, restrictions_key=restrictions_key + str(obj), concept_type=concept_type)   # Current version doesn't tackle array fields in extra classes as for SPHN Connector we only have one such class

    def map_join_condition(self, classiri: URIRef, obj: URIRef, mapping_uri: URIRef, skip_mapping: bool, restrictions_key: str):
        """Generates RML mapping join condition for object properties hasDataProvider and hasSubjectPseudoIdentifier

        Args:
            classiri (URIRed): concept class
            obj (URIRef): object property
            mapping_uri (URIRef): mapping URI
            skip_mapping (bool): skip join condition mapping when class prefix != 'sphn' and object prefix = 'sphn'
            restrictions_key (str): transversed path for restrictions check
        """
        if skip_mapping:
            return
        classrefs, _ = get_range(classiri=classiri, schema_graph=self.schema_graph, obj=obj, restrictions=self.restrictions, restrictions_key=restrictions_key, restrictions_map=self.restrictions_map, sphn_base_iri=self.sphn_base_iri,
                              is_legacy=self.is_legacy, root_nodes=self.root_nodes, project_iri=self.project_iri)
        for classref in classrefs:
            classprefix, classname = niceify_prefix(prefixable_iri=classref)
            
            # Define reference mapping rules, i.e. sphnDataProvider and sphnSubjectPseudoIdentifier
            refmappingname = f"{classprefix}{classname}"
            refmapping_uri = URIRef(BASE + refmappingname)

            # Define predicate object map --> :sphnAllergy rr:predicateObjectMap [ ]
            predicate_object_map_node = BNode()
            self.rml_graph.add((mapping_uri, RR.predicateObjectMap, predicate_object_map_node))

            # Add (rr:predicate sphn:hasSubjectPseudoIdentifier) to predicate object map
            self.rml_graph.add((predicate_object_map_node, RR.predicate, obj))

            # Define object map inside predicate object map --> rr:objectMap [ ]
            object_map_node = BNode()
            self.rml_graph.add((predicate_object_map_node, RR.objectMap, object_map_node))

            # Add reference to other mapping rule (rr:parentTriplesMap :sphnSubjectPseudoIdentifier) to object map
            self.rml_graph.add((object_map_node, RR.parentTriplesMap, refmapping_uri))        

    def map_data_release(self, project_version_iri: str):
        """Generate RML mapping for DataRelease. It generates the following mapping (on 2024.2)

            :DataRelease a rr:TriplesMap ;
                rml:logicalSource [ rml:iterator "$.sphn:DataRelease" ;
                        rml:referenceFormulation ql:JSONPath ;
                        rml:source "patient_data.json" ] ;
                rr:predicateObjectMap [ rr:objectMap [ rml:reference "sphn:hasExtractionDateTime" ;
                                rr:datatype xsd:dateTime ] ;
                        rr:predicate sphn:hasExtractionDateTime ],
                    [ rr:objectMap [ rr:parentTriplesMap :sphnDataProvider ] ;
                        rr:predicate sphn:hasDataProvider ],
                    [ rr:objectMap [ rr:template "https://biomedit.ch/rdf/sphn-schema/sphn/2024/2" ] ;
                        rr:predicate dct:conformsTo ] ;
                rr:subjectMap [ rr:class sphn:DataRelease ;
                        rr:template "https://biomedit.ch/rdf/sphn-resource/DATA-PROVIDER-ID-sphn-DataRelease-{id}" ] .

        Args:
            project_version_iri (str): project/sphn owl:versionIRI
        """
        NAMESPACE_SPHN = ('sphn', URIRef(self.sphn_base_iri + '#'))
        SPHN = Namespace(NAMESPACE_SPHN[1])
        json_content_path = "$.sphn:DataRelease"
        mapping_uri = URIRef(BASE + "DataRelease")

        # Create RML mapping        
        self.rml_graph.add((mapping_uri, RDF.type, RR.TriplesMap))
        logical_source_node = BNode()
        self.rml_graph.add((mapping_uri, RML.logicalSource, logical_source_node))
        self.rml_graph.add((logical_source_node, RML.source, Literal(self.input_path)))
        self.rml_graph.add((logical_source_node, RML.referenceFormulation, QL.JSONPath))
        self.rml_graph.add((logical_source_node, RML.iterator, Literal(json_content_path)))
        subject_map_node = BNode()
        self.rml_graph.add((mapping_uri, RR.subjectMap, subject_map_node))
        self.rml_graph.add((subject_map_node, RR.template, Literal(RESOURCE+ f"{self.data_provider_id}-sphn-DataRelease-{{id}}")))
        self.rml_graph.add((subject_map_node, URIRef(str(RR)+"class"), URIRef(SPHN + 'DataRelease')))

        # Create extraction date mapping
        predicate_object_map_node = BNode()
        self.rml_graph.add((mapping_uri, RR.predicateObjectMap, predicate_object_map_node))
        self.rml_graph.add((predicate_object_map_node, RR.predicate, URIRef(SPHN + 'hasExtractionDateTime')))
        object_map_node = BNode()
        self.rml_graph.add((predicate_object_map_node, RR.objectMap, object_map_node))
        self.rml_graph.add((object_map_node, RML.reference, Literal('sphn:hasExtractionDateTime')))
        self.rml_graph.add((object_map_node, RR.datatype, URIRef('http://www.w3.org/2001/XMLSchema#dateTime')))

        # Created version IRI mapping
        predicate_object_map_node = BNode()
        self.rml_graph.add((mapping_uri, RR.predicateObjectMap, predicate_object_map_node))
        self.rml_graph.add((predicate_object_map_node, RR.predicate, URIRef(DCT + 'conformsTo')))
        object_map_node = BNode()
        self.rml_graph.add((predicate_object_map_node, RR.objectMap, object_map_node))
        self.rml_graph.add((object_map_node, RR.template, Literal(project_version_iri)))

        # Create DataProvider mapping
        dataprovider_mapping = f"sphn{self.data_provider_class}"
        refmapping_uri = URIRef(BASE + dataprovider_mapping)
        predicate_object_map_node = BNode()
        self.rml_graph.add((mapping_uri, RR.predicateObjectMap, predicate_object_map_node))
        self.rml_graph.add((predicate_object_map_node, RR.predicate, URIRef(SPHN + self.data_provider_property)))
        object_map_node = BNode()
        self.rml_graph.add((predicate_object_map_node, RR.objectMap, object_map_node))
        self.rml_graph.add((object_map_node, RR.parentTriplesMap, refmapping_uri))

    def apply_namespace(self):
        """Applies namespaces to RML graph
        """
        schema_namespaces = list(self.schema_graph.namespace_manager.namespaces())
        NAMESPACE_SPHN = ('sphn', URIRef(self.sphn_base_iri + '#'))
        for (prefix, uri) in schema_namespaces:
            self.rml_graph.namespace_manager.bind(prefix, uri, override=True) 
        for (prefix, uri) in [NAMESPACE_RML, NAMESPACE_RR, NAMESPACE_QL, NAMESPACE_SPHN, NAMESPACE_RML_BASE, NAMESPACE_RESOURCE, NAMESPACE_DCT]:
            self.rml_graph.namespace_manager.bind(prefix, uri, override=True) 


def main(input_data: str, 
         schema_file: str, 
         json_schema_output_path: str, 
         rml_mapping_output_path: str, 
         data_provider_id: str, 
         project_name: str,
         database: Database = None, 
         ext_schemas: list = [],
         do_not_restrict_unit: bool = False):
    """Main method

    Args:
        input_data (str): input JSON file with patient data
        schema_file (str): schema
        json_schema_output_path (str): path of the JSON schema output file
        rml_mapping_output_path (str): path of the RML mapping output file
        data_provider_id (str): data provider ID
        project_name (str): name of the project
        database (Database, optional): Database object. Defaults to None.
        ext_schemas (list, optional): list of external schemas. Defaults to [].
        do_not_restrict_unit (bool, optional): do not restrict sphn:hasUnit values. Defaults to False.
    """
    # Load schema files and extract base IRIs
    schema_dir = os.path.dirname(schema_file)
    simplified_schema = os.path.join(schema_dir, os.path.basename(schema_file).replace('.ttl', '_simplified.ttl'))
    
    if ext_schemas:
        # If an external schema is provided load it as 'sphn_graph' and load the project specific schema as 'project_graph'
        sphn_graph = load_graph(path=Path(ext_schemas[0]), schema_file_type="turtle")
        project_graph = load_graph(path=Path(schema_file), schema_file_type="turtle")
        if database is not None:
            database.update_rml_json_logging(project_name=project_name, status=RML_JSON_LOGS.SCHEMA_FILE, message = "Loading SPHN schema and project specific schema")
            sphn_base_iri = database.get_sphn_base_iri(project=project_name)
        else:
            sphn_base_iri = [str(x) for x in sphn_graph.subjects(RDF.type, OWL.Ontology)][0]
        project_iri = [x for x in project_graph.subjects(RDF.type, OWL.Ontology)][0]
        project_version_iri = [str(x) for x in project_graph.objects(project_iri, OWL.versionIRI)][0]
        sphn_schema_iri = [str(x) for x in sphn_graph.objects(URIRef(sphn_base_iri), OWL.versionIRI)][0]
    else:
        # If not external schema is provided load the SPHN schema as 'sphn_graph'
        sphn_graph = load_graph(path=Path(schema_file), schema_file_type="turtle")
        project_graph = Graph()
        if database is not None:
            database.update_rml_json_logging(project_name=project_name, status=RML_JSON_LOGS.SCHEMA_FILE, message = "Loading SPHN schema")
            sphn_base_iri = database.get_sphn_base_iri(project=project_name)
        else:
            sphn_base_iri = [str(x) for x in sphn_graph.subjects(RDF.type, OWL.Ontology)][0]
        project_iri = None
        project_version_iri = [str(x) for x in sphn_graph.objects(URIRef(sphn_base_iri), OWL.versionIRI)][0]
        sphn_schema_iri = project_version_iri

    using_legacy_schema = Database._is_legacy_schema(sphn_base_iri=sphn_base_iri)
    is_2024_schema = Database._is_2024_schema(sphn_schema_iri=sphn_schema_iri)
    
    # Pre-process provided schemas and store it locally
    root_nodes, preprocessed_graph = simplify_schema_graph(sphn_graph, project_graph)    
    
    if database is not None:
        database.update_rml_json_logging(project_name=project_name, status=RML_JSON_LOGS.EXPORT_SIMPLE, message = f"Exporting RML graph to file {simplified_schema}")

    # Export pre-processed unified schema into local file with '_simplified.ttl' suffix
    export_graph(graph=preprocessed_graph, output=simplified_schema)

    if database is not None:
        database.update_rml_json_logging(project_name=project_name, status=RML_JSON_LOGS.LOAD_SIMPLE, message = f"Loading schema file {simplified_schema}") 

    # Reload the pre-processed schema as 'full_graph'
    full_graph = load_graph(path=Path(simplified_schema), schema_file_type="turtle")   

    # Extract all classes, i.e. all concepts defined as rdf:type owl:Class
    sphn_classes = [x for x in (full_graph.subjects(RDF.type, OWL.Class)) if isinstance(x, URIRef)]

    # Extract restrictions JSON file
    restrictions = get_restrictions(G=full_graph, sphn_classes=sphn_classes)

    # Extract restricion map that defines restrictions on full property paths
    if using_legacy_schema:
        restrictions_map = get_restrictions_map(restrictions=restrictions)
    else:
        restrictions_map = get_restrictions_map_with_range(restrictions=restrictions, schema_graph=full_graph, sphn_base_iri=sphn_base_iri, root_nodes=root_nodes)
    
    # Extract core concepts
    core_concepts = get_core_concepts(schema_graph=full_graph, sphn_classes=sphn_classes, sphn_base_iri=sphn_base_iri, data_provider_class=Database._get_data_provider_class(sphn_base_iri=sphn_base_iri),
                                      is_legacy=using_legacy_schema)
    
    # Extract supporting concepts (from 2024.2)
    supporting_concepts = get_supporting_concepts(graph=full_graph, core_concepts=core_concepts, sphn_classes=sphn_classes, restrictions=restrictions, restrictions_map=restrictions_map, sphn_base_iri=sphn_base_iri,
                                                  root_nodes=root_nodes, is_legacy=using_legacy_schema, project_iri=project_iri, is_2024_schema=is_2024_schema)
    
    # Generate RML mapping file based on provided RDF schemas
    rml_generator = RMLGenerator(schema_graph=full_graph, restrictions=restrictions, input_path=input_data, output_path=rml_mapping_output_path, data_provider_id=data_provider_id, 
                                project_name=project_name, database=database, project_version_iri=project_version_iri, restrictions_map=restrictions_map, sphn_base_iri=sphn_base_iri,
                                core_concepts=core_concepts, supporting_concepts=supporting_concepts, root_nodes=root_nodes, project_iri=project_iri)    
    rml_generator.generate_rml_mapping()

    # Generate JSON schema based on provided RDF schemas
    json_generator = JSONSchemaGenerator(schema_graph=full_graph, restrictions=restrictions, output_path=json_schema_output_path, project_name=project_name, database=database, restrictions_map=restrictions_map, 
                                         sphn_base_iri=sphn_base_iri, core_concepts=core_concepts, supporting_concepts=supporting_concepts, root_nodes=root_nodes, project_iri=project_iri,
                                         do_not_restrict_unit=do_not_restrict_unit)
    json_generator.generate_schema()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='RML mappings generator')
    parser.add_argument("-f", "--input_file", action="store", default="patient_data.json")
    parser.add_argument("-o", "--schema_file", action="store", default="sphn_rdf_schema_2024.1.ttl")
    parser.add_argument("-oj", "--output_json", action="store", default='json_schema.json')
    parser.add_argument("-om", "--output_mapping", action="store", default="rml_generated_mapping.ttl")
    parser.add_argument("-exo", "--external_schema", action="store", default=None)
    parser.add_argument("-l", "--log_level", action="store", choices=['INFO', 'DEBUG'], default="INFO")
    parser.add_argument("-pro", "--project_name", action="store", default=None)
    args = parser.parse_args()
    logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s', level=logging.getLevelName(args.log_level), datefmt='%Y-%m-%d %H:%M:%S')
    logging.info("Welcome to the RML generator")

    # Input files
    schema_file = args.schema_file
    input_file = args.input_file
    external_schemas = [args.external_schema] if args.external_schema is not None else []

    # Output files
    json_schema_output_path = args.output_json
    rml_mapping_output_path = args.output_mapping

    project_name = args.project_name
    
    main(input_data=input_file, schema_file=schema_file, json_schema_output_path=json_schema_output_path, rml_mapping_output_path=rml_mapping_output_path, data_provider_id='DATA-PROVIDER-ID', project_name=project_name, ext_schemas=external_schemas,
         do_not_restrict_unit=True)
