#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# This file: 'connector.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

import glob
import os
from datetime import datetime, timedelta
import sys
import subprocess
import shutil
from typing import Union, Set, Dict
import json
from itertools import islice
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor, as_completed
import csv
import re
import io
import requests
import time

sys.path.append(os.path.abspath(os.path.dirname(__file__)))
from enums import DagRun, DataPersistenceLayer, DeIdentificationStatus, ErrorLevel, FileType, IngestionType, LogLevel, OutputFormat, PipelineStep, PreCheckLevel, PreCheckStatus, PreCheckType, QCSeverityLevel, StatisticsProfile, Steps
from de_identification import create_de_identification_rules
from config import Config
from database import Database, bucket_exists
from pre_checks import create_pre_checks, get_replace_id_checks
from rml_generator_lib import niceify_prefix
from airflow_lib import Airflow

class Connector(object):
    """
    Class containing information and methods about Airflow Connector. It contains the methods used from the Airflow container to execute the pipelines
    """
    def __init__(self, project: str):
        """Class constructor

        Args:
            project (str): name of the project
        """
        self.config = Config()
        self.database = Database(project=project)
        self.config.data_provider_id = self.database.config.data_provider_id

    def update_logging(self, 
                       project_name: str, 
                       patient_id: str, 
                       step: Steps, 
                       dag_run_status: DagRun, 
                       message: str = None, 
                       report: str = None, 
                       validation_ok: bool = None, 
                       parallel: bool = False):
        """Updates logging table with validation results

        Args:
            project_name (str): name of the project
            patient_id (str): ID of the patient
            step (Steps): pipeline step
            dag_run_status (DagRun): status of the DAG run
            message (str, optional): status message. Defaults to None.
            report (str, optional): validation report. Defaults to None.
            validation_ok (bool, optional): validation status. Defaults to None.
            parallel (bool, optional): running in parallel. Defaults to False.
        """
        if not parallel:
            self.database.create_connection()
        timestmp = datetime.now()
        if message is None:
            if dag_run_status ==  DagRun.BACKLOG:
                logging_message = f"Patient '{patient_id}' for project '{project_name}' currently in the backlog"
            elif dag_run_status ==  DagRun.LOADING:
                logging_message = f"Patient '{patient_id}' for project '{project_name}' queued"
            elif dag_run_status == DagRun.VALIDATION_RESULT:
                if validation_ok:
                    logging_message = f"SUCCESSFUL SHACL validation of patient id '{patient_id}' and project '{project_name}'. Validation report:{report}"
                else:
                    logging_message = f"FAILED SHACL validation of patient id '{patient_id}' and project '{project_name}'. Validation report:{report}"
            elif dag_run_status == DagRun.STATS_SUCCESSFUL:
                logging_message = "Statistics successfully executed"
        else:
            logging_message = message
        sql = "INSERT INTO logging (project_name, patient_id, data_provider_id, step ,status, message, timestmp) VALUES \
                (%s, %s, %s, %s, %s, %s, %s) \
                ON CONFLICT (project_name, patient_id, step, data_provider_id, status) DO UPDATE SET timestmp=EXCLUDED.timestmp, message=EXCLUDED.message"
        self.database.cursor.execute(sql, (project_name, patient_id, self.config.data_provider_id, step.value, dag_run_status.value, logging_message, timestmp))
        if not parallel:
            self.database.close_connection()

    def update_quality_checks(self, project_name: str, patient_id: str, report: str, validation_ok: bool):
        """Update quality_checks table

        Args:
            project_name (str): name of the project
            patient_id (str): ID of the patient
            report (str): validation report
            validation_ok (bool): successful validation
        """
        pattern = r'\| (.*?) \| (.*?) \| (.*?) \| (.*?) \| (.*?) \| (.*?) \| (.*?) \| (.*?) \|'
        matches = re.findall(pattern, report)
        values = matches[1:]
        cleanup_query = "DELETE FROM quality_checks WHERE project_name=%s AND data_provider_id=%s AND patient_id=%s"
        self.database.cursor.execute(cleanup_query, (project_name, self.config.data_provider_id, patient_id))
        query = "INSERT INTO quality_checks (project_name, data_provider_id, patient_id, successful_validation, severity, result_path, source_constraint_component, source_shape, count, focus_node, result_message, value) \
                 VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        value_lists = [[project_name, self.config.data_provider_id, patient_id, validation_ok] + [val.strip() for val in value] for value in values]
        self.database.cursor.executemany(query, value_lists)

    def cleanup_logging(self, project: str, patient_ids: list, step: Steps):
        """Cleanup logs for patient for the current step

        Args:
            project (str): name of the project
            patient_id (list): list of patient IDs
            step (Steps): pipeline step
        """
        query = "DELETE FROM logging WHERE project_name=%s AND data_provider_id=%s AND patient_id=%s AND step=%s"
        for patient_id in patient_ids:
            self.database.cursor.execute(query, (project, self.config.data_provider_id, patient_id, step.value))

    def delete_missing_schema_warning(self, project_name: str, patient_id: str, step: Steps, status: DagRun, parallel: bool = False):
        """
        deletes missing schema warning from logging table.

        Args:
            project_name (str): name of the project
            patient_id (str): id of the patient
            step (Steps): pipeline step
            dag_run_status (DagRun): dag run status
            parallel (bool, optional): parallel execution. Defaults to False.
        """
        if not parallel:
            self.database.create_connection()
        sql = "DELETE FROM logging WHERE project_name=%s AND data_provider_id=%s AND patient_id=%s AND step=%s AND status=%s"
        self.database.cursor.execute(sql, (project_name, self.config.data_provider_id, patient_id, step.value, status.value))
        if not parallel:
            self.database.close_connection()


    def _cleanup_data(folder: str):
        """
        Cleanup data before starting validation

        Args:
            folder (str): name of the folder

        """



        if os.path.exists(folder):
            shutil.rmtree(folder)

    @staticmethod
    def _create_folder_structure(project_name: str, step: Steps):
        """
        Creates local folder structure

        Args:
            project_name (str): name of the project
            step (Steps): name of the step
        """
        if step == Steps.INTEGRATION:
            conversion_data_folder = os.path.join("/home/sphn/conversion", project_name)
            os.makedirs(conversion_data_folder, exist_ok=True)
        elif step in [Steps.VALIDATION, Steps.STATISTICS]:
            if step == Steps.VALIDATION:
                project_folder = os.path.join("/home/sphn/quality", project_name)
            else:
                project_folder = os.path.join("/home/sphn/statistics", project_name)
            os.makedirs(project_folder, exist_ok=True)
            schema_folder = os.path.join(project_folder,"schemas")
            os.makedirs(schema_folder, exist_ok=True)
            shacl_folder = os.path.join(project_folder, "shapes")
            os.makedirs(shacl_folder, exist_ok=True)
            queries_folder = os.path.join(project_folder, "queries")
            os.makedirs(queries_folder, exist_ok=True)
            external_resources = os.path.join(project_folder,"external-resources")
            os.makedirs(external_resources, exist_ok=True)
            test_data_folder = os.path.join(project_folder, "data/test_data")
            os.makedirs(test_data_folder, exist_ok=True)
            logs_folder = os.path.join(project_folder, "log")
            os.makedirs(logs_folder, exist_ok=True)
            report_query_folder = os.path.join(project_folder, "report-query")
            os.makedirs(report_query_folder, exist_ok=True)
            sparql_queries_folder = os.path.join(project_folder, "sparqler-queries")
            os.makedirs(sparql_queries_folder, exist_ok=True)

    @staticmethod
    def _download_data(project_name: str, step: Steps, profile: str = StatisticsProfile.FAST):
        """Download necessary data to execute the step

        Args:
            project_name (str): name of the project
            step (Steps): name of the step
            profile (str): statistics profile
        """
        connector = Connector(project=project_name)
        bucket = connector.config.s3_connector_bucket
        if step == Steps.INTEGRATION:
            conversion_data_folder = os.path.join("/home/sphn/conversion", project_name)
            rml_mapping = f'{project_name}_rml_mapping.ttl'
            rml_mapping_path = os.path.join(conversion_data_folder, rml_mapping)
            with open(rml_mapping_path, 'w', encoding='utf-8') as fp:
                # Create empty file to avoid tests failure
                pass
            if connector.object_exists(object_name=f'{project_name}/RML/{rml_mapping}'):
                connector.config.s3_client.download_file(Bucket=bucket, Key=f'{project_name}/RML/{rml_mapping}', Filename=rml_mapping_path)
        elif step in [Steps.VALIDATION, Steps.STATISTICS]:
            if step == Steps.VALIDATION:
                project_folder = os.path.join("/home/sphn/quality", project_name)
            else:
                project_folder = os.path.join("/home/sphn/statistics", project_name)
            schema_folder = os.path.join(project_folder,"schemas")
            shacl_folder = os.path.join(project_folder, "shapes")
            queries_folder = os.path.join(project_folder, "queries")
            external_resources = os.path.join(project_folder,"external-resources")
            report_query_folder = os.path.join(project_folder, "report-query")
            if bucket_exists(bucket = bucket, config=connector.config):
                schema_file = f"{project_name}/Schema/{project_name}_schema.ttl"
                connector.config.s3_client.download_file(Bucket=bucket, Key=schema_file, Filename=os.path.join(schema_folder, os.path.basename(schema_file)))
                external_schemas = connector.database.list_objects_keys(bucket=bucket, prefix=f"{project_name}/QAF/Schemas/")
                queries = connector.database.list_objects_keys(bucket=bucket, prefix=f"{project_name}/QAF/Queries/")
                print("External schemas")
                print(external_schemas)
                for external_schema in external_schemas:
                    print("External schema:", external_schema)
                    file_path = os.path.join(schema_folder, external_schema.split('/')[-1])
                    print("File path", file_path)
                    print("Directory exists", os.path.exists(os.path.dirname(file_path)))
                    connector.config.s3_client.download_file(Bucket=bucket, Key=external_schema, Filename=file_path)
                for query in queries:
                    file_path = os.path.join(queries_folder, query.split('/')[-1])
                    connector.config.s3_client.download_file(Bucket=bucket, Key=query, Filename=file_path)
                if step == Steps.STATISTICS:
                    connector.pull_sparqler_queries(project=project_name, project_folder=project_folder, profile=profile)
                if step == Steps.VALIDATION:
                    shacls = connector.database.list_objects_keys(bucket=bucket, prefix=f"{project_name}/QAF/SHACL/")
                    terminologies = connector.database.list_objects_keys(bucket=bucket, prefix=f"{project_name}/QAF/Terminologies/")
                    report_queries = connector.database.list_objects_keys(bucket=bucket, prefix=f"{project_name}/QAF/Report-Queries/report_sparql_query.rq", file_path=True)
                    for shacl in shacls:
                        file_path = os.path.join(shacl_folder, shacl.split('/')[-1])
                        connector.config.s3_client.download_file(Bucket=bucket, Key=shacl, Filename=file_path)
                    for terminology in terminologies:
                        file_path = os.path.join(external_resources, terminology.split('/')[-1])
                        connector.config.s3_client.download_file(Bucket=bucket, Key=terminology, Filename=file_path)
                    for report_query in report_queries:
                        file_path = os.path.join(report_query_folder, report_query.split('/')[-1])
                        connector.config.s3_client.download_file(Bucket=bucket, Key=report_query, Filename=file_path)
                        break

    def pull_sparqler_queries(self, project: str, project_folder: str, profile: str):
        """Pull SPARQLer queries for validation

        Args:
            project (str): name of the project
            project_folder (str): project folder
            profile (str): statistics profile
        """
        profile = StatisticsProfile(profile)
        bucket = self.config.s3_connector_bucket
        sparqler_queries = self.database.list_objects_keys(bucket=bucket, prefix=f"{project}/Statistics/Queries/")
        for query in sparqler_queries:
            filename = query.split("/")[-1]
            if profile == StatisticsProfile.EXTENSIVE and filename == 'min-max-predicates-fast.rq':
                continue
            if profile == StatisticsProfile.FAST and filename == 'min-max-predicates-extensive.rq':
                continue
            file_path = os.path.join(f"{project_folder}/sparqler-queries", filename)
            self.config.s3_client.download_file(Bucket=bucket, Key=query, Filename = file_path)

    def _generate_properties_file(project_name: str, validation_log_level: str, step: Steps):
        """Generates properties file if not existing

        Args:
            project_name (str): name of the project
            validation_log_level (str): logging level for QAF validation
            step (Steps): pipeline step
        """
        validation_log_level = LogLevel(validation_log_level)
        tdb2_folder = os.path.join('/home/sphn/quality/tdb2', project_name, '0')
        if step == Steps.VALIDATION:
            project_folder = os.path.join('/home/sphn/quality', project_name)
        else:
            project_folder = os.path.join('/home/sphn/statistics', project_name)
        properties_file = os.path.join(project_folder, f"{project_name}-QC_{validation_log_level.value.upper()}.properties")
        if not os.path.exists(properties_file):
            log_level = "true" if validation_log_level == LogLevel.VERBOSE else "false"
            data = f"""schema.dir={project_folder}/schemas
shapes.dir={project_folder}/shapes
queries.dir={project_folder}/queries
sparqler-queries.dir={project_folder}/sparqler-queries
sparqler-output.dir={project_folder}/sparqler-output
qc-queries-output.dir={project_folder}/qc-queries-output
resources.dir={project_folder}/data/test_data
external_resources.dir={project_folder}/external-resources
logs.dir={project_folder}/log
reportSPARQL.dir={project_folder}/report-query
shapes.logfulldetails={log_level}
tdb2prefix={tdb2_folder}
severity=INFO"""
            with open(properties_file, "w", encoding='utf-8') as file:
                file.write(data)

    def _generate_rmlmapper_properties_file(project_name: str):
        """
        Generates properties file if not existing

        Args:
            project_name (str): name of the project
        """
        project_folder = os.path.join('/home/sphn/conversion', project_name)
        properties_file = os.path.join(project_folder, f"{project_name}.properties")
        rml_mapping = os.path.join(project_folder, f'{project_name}_rml_mapping.ttl')
        input_folder = os.path.join(project_folder, 'json')
        output_folder = os.path.join(project_folder, 'converted')
        if not os.path.exists(properties_file):
            data = f"""mappingfile={rml_mapping}
serialization=turtle
inputfolder={input_folder}
outputfolder={output_folder}
"""
            with open(properties_file, "w", encoding='utf-8') as file:
                file.write(data)

    def get_patient_data(self, project_name: str, patient_id: str, layer: DataPersistenceLayer, step: Steps = Steps.ALL, revalidate: bool = False) -> dict:
        """Gets the patient files names stored in the landing zone

        Args:
            project_name (str): name of the project
            patient_id (str): unique ID of the patient
            layer (DataPersistenceLayer): data persistence layer
            step (Steps, optional): pipeline step. Defaults to Steps.ALL.
            revalidate (bool, optional): revalidate all data. Defaults to False.

        Returns:
            dict: map of patient data
        """
        self.database.create_connection()
        if step == Steps.STATISTICS:
            sql = "SELECT object_name, patient_id, size FROM {} WHERE project_name = %s AND data_provider_id = %s".format(layer.value)
            self.database.cursor.execute(sql, (project_name, self.config.data_provider_id))
        else:
            if revalidate:
                where_condition = ""
            else:
                where_condition = "AND processed=False"
            if layer == DataPersistenceLayer.LANDING_ZONE:
                if patient_id is not None:
                    sql = f"SELECT object_name, patient_id, size, ingestion_type FROM {layer.value} WHERE project_name = %s AND patient_id = %s AND data_provider_id = %s {where_condition}"
                    self.database.cursor.execute(sql, (project_name, patient_id, self.config.data_provider_id))
                else:
                    sql = f"SELECT object_name, patient_id, size, ingestion_type FROM {layer.value} WHERE project_name = %s AND data_provider_id = %s {where_condition}"
                    self.database.cursor.execute(sql, (project_name, self.config.data_provider_id))
            else:
                if patient_id is not None:
                    sql = f"SELECT object_name, patient_id, size FROM {layer.value} WHERE project_name = %s AND patient_id = %s AND data_provider_id = %s {where_condition}"
                    self.database.cursor.execute(sql, (project_name, patient_id, self.config.data_provider_id))
                else:
                    sql = f"SELECT object_name, patient_id, size FROM {layer.value} WHERE project_name = %s AND data_provider_id = %s {where_condition}"
                    self.database.cursor.execute(sql, (project_name, self.config.data_provider_id))
        records = self.database.cursor.fetchall()
        self.database.close_connection()
        patient_data = {}
        for record in records:
            if layer == DataPersistenceLayer.LANDING_ZONE:
                patient_data[record[1]] = {'object_name': record[0], 'size': float(record[2]) if record[2] is not None else None, 'ingestion_type': IngestionType(record[3])}
            else:
                patient_data[record[1]] = {'object_name': record[0], 'size': float(record[2]) if record[2] is not None else None}
        return patient_data

    def object_exists(self, object_name: str) -> bool:
        """Checks if object exists in S3 storage

        Args:
            object_name (str): prefix of the object in S3

        Returns:
            bool: object exists
        """
        head, _ = os.path.split(object_name)
        object_names = self.database.list_objects_keys(bucket=self.config.s3_connector_bucket, prefix=head + "/")
        if object_names:
            return object_name in object_names
        else:
            return False


    def update_execution_errors_table(self, 
                                      project_name: str, 
                                      patient_id: str, 
                                      run_uuid: str, 
                                      step: Steps, 
                                      error_level: ErrorLevel, 
                                      parallel: bool):
        """Update execution_errors table

        Args:
            project_name (str): name of the project
            patient_id (str): patient ID
            run_uuid (str): unique identifier of the DAG run
            step (Steps): step name
            error_level (ErrorLevel): level of the execution error
            parallel (bool): parallel execution
        """
        if not parallel:
            self.database.create_connection()
        sql = "INSERT INTO execution_errors (project_name, data_provider_id, patient_id, run_uuid, step, error_level, timestmp) VALUES (%s, %s, %s, %s, %s, %s, %s)"
        self.database.cursor.execute(sql, (project_name, self.config.data_provider_id, patient_id, run_uuid, step.value, error_level.value, datetime.now()))
        if not parallel:
            self.database.close_connection()

    @staticmethod
    def _get_keys(data: Dict, enumerated: bool) -> Set[str]:
        """Extract all key paths from the patient data

        Args:
            data (dict): patient data
            enumerated (bool): add enumeration at the end of the path if list

        Returns:
            list: list of key paths
        """
        result = set()
        for key, value in data.items():
            if isinstance(value, dict):
                new_keys = Connector._get_keys(data=value, enumerated=enumerated)
                for innerkey in new_keys:
                    result.add(f'{key}/{innerkey}')
            elif isinstance(value, list):
                for i_obj, obj in enumerate(value):
                    if isinstance(obj, dict):
                        new_keys = Connector._get_keys(data=obj, enumerated=enumerated)
                        for innerkey in new_keys:
                            result.add(f'{key}/{i_obj}/{innerkey}')
                    else:
                        if enumerated:
                            result.add(f'{key}/{i_obj}/')
                        else:
                            result.add(key)
                            break
            else:
                result.add(key)
        return result

    def execute_pre_checks(self, 
                           patient_data: dict, 
                           pre_checks: list, 
                           file_type: FileType, 
                           keys: set, 
                           code_term_paths: set, 
                           ingestion_type: IngestionType,
                           replace_id_checks: list, 
                           i_chunk: int, 
                           patient_id: str) -> Union[bool, bool, str]:
        """Execute Pre-Checks for a patient

        Args:
            patient_data (dict): patient data
            pre_checks (list): list of defined Pre-Checks
            file_type (FileType): patient file type
            keys (list): list of patient data paths
            code_term_paths (set): collection of paths to codes
            ingestion_type (IngestionType): ingestion type of the patient
            replace_id_checks (list): checks to apply to sourceConceptID field
            i_chunk (int): chunk number
            patient_id (str): ID of the patient

        Returns:
            Union[bool, bool, str]: checks failed, stop file, return message
        """
        checks_failed = False
        stop_file = False
        check_messages = []
        for pre_check in pre_checks:
            print(f"{{'chunk': '{i_chunk}', 'patient': '{patient_id}'}}: Execute pre-check '{pre_check.check_name}'")
            check_results = []
            if file_type == FileType.JSON:
                if pre_check.type in [PreCheckType.REGEX_CHECK, PreCheckType.REPLACE_CHARS_CHECK, PreCheckType.REPLACE_REGEX_CHECK, PreCheckType.IRI_VALIDATION_CHECK]:
                    check_results.append(pre_check.execute(patient_data=patient_data, keys=keys))
                elif pre_check.type == PreCheckType.JSON_PRE_PROCESSING and ingestion_type == IngestionType.JSON:
                    check_results.append(pre_check.execute(patient_data=patient_data, keys=keys, code_term_paths=code_term_paths))
                elif pre_check.type == PreCheckType.JSON_PRE_PROCESSING and ingestion_type == IngestionType.DATABASE and replace_id_checks:
                    for replace_check in replace_id_checks:
                        check_results.append(replace_check.execute(patient_data=patient_data, keys=keys))
                elif pre_check.type == PreCheckType.VALIDATION_CHECK and ingestion_type == IngestionType.JSON:
                    check_results.append(pre_check.execute(patient_data=patient_data))
                elif pre_check.type == PreCheckType.REPLACE_UNVERSIONED_CODES:
                    check_results.append(pre_check.execute(patient_data=patient_data, keys=keys, code_term_paths=code_term_paths))
                else:
                    continue
            else:
                if pre_check.type in [PreCheckType.DATA_TYPE_CHECK, PreCheckType.RDF_VALIDATION_CHECK]:
                    check_results.append(pre_check.execute(patient_data=patient_data))
                else:
                    continue
            for check_result in check_results:
                check_messages.append(check_result.message)
                if check_result.status == PreCheckStatus.FAILED:
                    checks_failed = True
                    if check_result.level == PreCheckLevel.FAIL:
                        stop_file = True

        if check_messages:
            message = '\n\t' + '\n\t'.join(check_messages)
        else:
            message = "No Pre-Checks defined"
        return checks_failed, stop_file, message

    def execute_de_identification_rules(self, 
                                        patient_data: dict, 
                                        de_id_rules: list, 
                                        patient_id: str, 
                                        file_type: FileType, 
                                        keys: list,
                                        enumerated_keys: set) -> Union[bool, str]:
        """Execute De-Identification rules for a patient

        Args:
            patient_data (dict): patient data
            de_id_rules (list): list of De-Identification rules
            patient_id (str): patient id
            file_type (FileType): patient file type
            keys (list): list of patient data paths
            enumerated_keys (list): list of enumerated patient data paths

        Returns:
            Union[bool, str]: De-Identification failed and messages
        """
        de_identification_failed = False
        de_identification_messages = []
        if de_id_rules and file_type == FileType.JSON:
            for de_id_rule in de_id_rules:
                de_identification_result = de_id_rule.execute(patient_data=patient_data, patient_id=patient_id, keys=keys, enumerated_keys=enumerated_keys)
                de_identification_messages.append(de_identification_result.message)
                if de_identification_result.status == DeIdentificationStatus.FAILED:
                    de_identification_failed = True
            message = '\n\t' + '\n\t'.join(de_identification_messages)
        else:
            message = "No De-Identification rules defined"
        return de_identification_failed, message

    @staticmethod
    def _run_pre_check_and_de_identification(project_name: str, run_uuid: str, dag_run_config:dict):
        """
        Run Pre-check and De-Identification on landing files

        Args:
            project_name (str): name of the project
            run_uuid (str): UUID of the run
            dag_run_config (dict): configuration for the running Airflow dag
        """
        print(f"Running pre-checks/de-identification for project '{project_name}'")
        connector = Connector(project=project_name)
        external_schemas = connector.database.list_objects_keys(bucket=connector.config.s3_connector_bucket, prefix=f"{project_name}/QAF/Schemas/")
        if external_schemas:
            sphn_version_iri = connector.database.get_schema_version_iri(object_name=external_schemas[0])
        else:
            sphn_version_iri = connector.database.get_schema_version_iri(object_name=f"{project_name}/Schema/" + project_name + "_schema.ttl")
        patients_count = {"processed_patients": 0, "failed_patients": 0, "patients_with_warnings": 0}
        step = Steps.PRE_CHECK_DE_ID
        start_timestamp = datetime.now()
        connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.RUNNING, patients_processed=None, patients_with_warnings=None, patients_with_errors=None, 
                                       execution_time=start_timestamp, elapsed_time=None, run_uuid=run_uuid)
        try:
            patient_id = dag_run_config.get('patient_id')
            if patient_id is not None:
                patient_id = str(patient_id)
            landing_data = connector.get_patient_data(project_name=project_name, patient_id=patient_id, layer=DataPersistenceLayer.LANDING_ZONE)
            if not landing_data:
                print(f"No landing files found to check for project '{project_name}'")
                connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.SKIPPED, patients_processed=patients_count["processed_patients"], patients_with_warnings=patients_count["patients_with_warnings"], patients_with_errors=patients_count["failed_patients"],
                                            execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)
            else:
                parallelization_message = f"Step processing {connector.config.machine_cpus} patients in parallel"
                patients_data = {key: {'object_name': value['object_name'], 'ingestion_type': value['ingestion_type']} for key, value in landing_data.items()}
                chunks = Connector._chunks(data=patients_data, parallelization=connector.config.machine_cpus)
                connector.database.create_connection()
                connector.cleanup_logging(project=project_name, patient_ids=patients_data.keys(), step=Steps.PRE_CHECK_DE_ID)
                for patient_id in patients_data.keys():
                    connector.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.PARALLELIZATION, parallel=True, message=parallelization_message)
                connector.database.close_connection()
                code_term_paths = set()
                json_schema = connector.config.s3_client.get_object(Bucket = connector.config.s3_connector_bucket, Key = f'{project_name}/RML/{project_name}_json_schema.json')['Body'].read().decode('utf8')
                Connector._extract_code_term_paths(property_map=json.loads(json_schema), code_keys=code_term_paths, key_path='')
                with ProcessPoolExecutor(max_workers=connector.config.machine_cpus) as executor:
                    futures = []
                    for i_chunk, chunk in enumerate(chunks):
                        futures.append(executor.submit(execute_pre_check_and_de_identification, i_chunk, chunk, project_name, run_uuid, code_term_paths, sphn_version_iri))
                    for future in as_completed(futures):
                        result = future.result()
                        for key in patients_count.keys():
                            patients_count[key] += result[key]
                        connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.RUNNING, patients_processed=patients_count["processed_patients"], patients_with_warnings=patients_count["patients_with_warnings"], patients_with_errors=patients_count["failed_patients"],
                                                       execution_time=start_timestamp, elapsed_time=None, run_uuid=run_uuid)
                connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.SUCCESS, patients_processed=patients_count["processed_patients"], patients_with_warnings=patients_count["patients_with_warnings"], patients_with_errors=patients_count["failed_patients"], 
                                               execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)
                del chunks
                del patients_data
            del landing_data
        except Exception:
            connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.FAILED, patients_processed=patients_count["processed_patients"], patients_with_warnings=patients_count["patients_with_warnings"], patients_with_errors=patients_count["failed_patients"],
                                    execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)
            raise

    def setup_conversion_chunk_environment(self, project_name: str, i_chunk: int) -> Union[str, str, str, str]:
        """Setup chunk environment for RML conversion

        Args:
            project_name (str): name of the project
            i_chunk (int): chunk number

        Returns:
            Union[str, str, str, str]: RDF input data folder, JSON input data folder, output RDF data folder, config file path
        """
        project_folder = os.path.join('/home/sphn/conversion/', project_name)
        chunk_folder = os.path.join(project_folder, str(i_chunk))
        config_file = os.path.join(chunk_folder, f'{project_name}.properties')
        rdf_folder = os.path.join(chunk_folder, 'ttl')
        json_folder = os.path.join(chunk_folder, 'json')
        input_folder = os.path.join(project_folder, 'json')
        output_folder = os.path.join(project_folder, 'converted')
        chunk_input_folder = os.path.join(chunk_folder, 'json')
        chunk_output_folder = os.path.join(chunk_folder, 'converted')
        os.makedirs(chunk_folder, exist_ok=True)
        os.makedirs(rdf_folder, exist_ok=True)
        os.makedirs(json_folder, exist_ok=True)
        os.makedirs(chunk_input_folder, exist_ok=True)
        os.makedirs(chunk_output_folder, exist_ok=True)
        properties_file = os.path.join(project_folder, f"{project_name}.properties")
        chunk_properties_file = os.path.join(chunk_folder, f"{project_name}.properties")
        with open(properties_file, 'r', encoding='utf-8') as input:
            with open(chunk_properties_file, 'w', encoding='utf-8') as output:
                data = input.read()
                data = data.replace(f'inputfolder={input_folder}', f'inputfolder={chunk_input_folder}')
                data = data.replace(f'outputfolder={output_folder}', f'outputfolder={chunk_output_folder}')
                output.write(data)
        return rdf_folder, json_folder, chunk_output_folder, config_file

    def download_refined_data(self, project_name: str, chunk: dict, json_folder: str, rdf_folder: str):
        """Download data from refined zone

        Args:
            project_name (str): name of the project
            chunk (dict): chunk data
            json_folder (str): folder path for JSON data
            rdf_folder (str): folder path for RDF data
        """
        bucket = self.config.s3_connector_bucket
        for patient_id, patient_file in chunk.items():
            patient_filename = os.path.basename(patient_file)
            if patient_filename.endswith('.json'):
                data_folder = json_folder
                extension = ".json"
            else:
                data_folder = rdf_folder
                extension = ".ttl"
            filepath = os.path.join(data_folder, f"{patient_id}{extension}")
            self.config.s3_client.download_file(Bucket=bucket, Key=patient_file, Filename=filepath)
            self.update_logging(project_name=project_name, patient_id=patient_id, step=Steps.INTEGRATION, dag_run_status=DagRun.LOADING, parallel=True)

    def execute_rdf_integration(self, project_name: str, rdf_folder: str, patients_count: dict):
        """Execute Integration step for RDF refined data

        Args:
            project_name (str): name of the project
            rdf_folder (str): folder path of RDF data
            patients_count (dict): patient counts tracker
        """
        for patient_file in glob.glob(os.path.join(rdf_folder, '*')):
            patient_id = os.path.basename(patient_file).split('.')[0]
            self.database.move_to_next_layer(project=project_name, patient_id=patient_id, object_name=patient_file, source_zone=DataPersistenceLayer.REFINED_ZONE, file_type=FileType.RDF, parallel=True)
            message = f"Refined RDF file '{project_name}_{patient_id}_{self.config.data_provider_id}.ttl' copied to graph zone"
            self.update_logging(project_name=project_name, patient_id=patient_id, step=Steps.INTEGRATION, dag_run_status=DagRun.RDF_INTEGRATION, message=message, parallel=True)
            patients_count['processed_patients'] += 1
        shutil.rmtree(rdf_folder, ignore_errors=True)

    def execute_json_integration(self, 
                                 project_name: str, 
                                 json_folder: str, 
                                 output_folder: str, 
                                 config_file: str, 
                                 patients_count: dict, 
                                 run_uuid: str, 
                                 max_java_heap_space: int):
        """Execute Integration step for JSON refined data

        Args:
            project_name (str): name of the project
            json_folder (str): folder path of JSON data
            output_folder (str): path to output folder
            config_file (str): path of configuration file
            patients_count (dict): patient counts tracker
            run_uuid (str): UUID of the run
            max_java_heap_space (int): maximum Java heap space
        """
        step = Steps.INTEGRATION
        if len(os.listdir(json_folder)) != 0:
            for patient_file in glob.glob(os.path.join(json_folder, '*')):
                patient_id = os.path.basename(patient_file).split('.')[0]
                message = f"Found JSON file to convert for patient id '{patient_id}' and project '{project_name}'"
                self.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.JSON_FILES_LOOKUP, message=message, parallel=True)
            p = subprocess.Popen(['/home/sphn/conversion/execute_rml_mapper.sh', f'-Xmx{max_java_heap_space}g', '/home/sphn/conversion/rmlmapper-6.2.1-r0-all.jar', config_file], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            _, error = p.communicate()
            error_msg = error.decode()
            if p.returncode != 0:
                message = f"Execution of rml-mapper failed:\n{error_msg}"
                converted_ids = []
                for patient_file in glob.glob(os.path.join(output_folder, '*')):
                    patient_id = os.path.basename(patient_file).split('.')[0]
                    object_name = f"{project_name}_{patient_id}_{self.config.data_provider_id}.ttl"
                    self.database.move_to_next_layer(project=project_name, patient_id=patient_id, object_name=patient_file, source_zone=DataPersistenceLayer.REFINED_ZONE, file_type=FileType.JSON, parallel=True)
                    message = f"SUCCESSFUL conversion. JSON file '{object_name.replace('.ttl', '.json')}' converted to RDF file '{object_name}'"
                    self.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.JSON_INTEGRATION, message=message, parallel=True)
                    patients_count['processed_patients'] += 1
                    converted_ids.append(patient_id)
                for patient_file in glob.glob(os.path.join(json_folder, '*')):
                    patient_id = os.path.basename(patient_file).split('.')[0]
                    if patient_id not in converted_ids:
                        self.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.JSON_INTEGRATION, message=message, parallel=True)
                        patients_count['failed_patients'] += 1
                        patients_count['processed_patients'] += 1
                        self.update_execution_errors_table(project_name=project_name, patient_id=patient_id, run_uuid=run_uuid, step=Steps.INTEGRATION, error_level=ErrorLevel.ERROR, parallel=True)
            elif len(os.listdir(output_folder)) == 0:
                if error_msg:
                    message = f"FAILED conversion. JSON files not successfully converted. Probably caused by:\n{error_msg}"
                else:
                    message = "FAILED conversion. JSON files not successfully converted."
                for patient_file in glob.glob(os.path.join(json_folder, '*')):
                    patient_id = os.path.basename(patient_file).split('.')[0]
                    self.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.JSON_INTEGRATION, message=message, parallel=True)
                    patients_count['failed_patients'] += 1
                    patients_count['processed_patients'] += 1
                    self.update_execution_errors_table(project_name=project_name, patient_id=patient_id, run_uuid=run_uuid, step=Steps.INTEGRATION, error_level=ErrorLevel.ERROR, parallel=True)
            else:
                for patient_file in glob.glob(os.path.join(output_folder, '*')):
                    patient_id = os.path.basename(patient_file).split('.')[0]
                    object_name = f"{project_name}_{patient_id}_{self.config.data_provider_id}.ttl"
                    self.database.move_to_next_layer(project=project_name, patient_id=patient_id, object_name=patient_file, source_zone=DataPersistenceLayer.REFINED_ZONE, file_type=FileType.JSON, parallel=True)
                    message = f"SUCCESSFUL conversion. JSON file '{object_name.replace('.ttl', '.json')}' converted to RDF file '{object_name}'"
                    self.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.JSON_INTEGRATION, message=message, parallel=True)
                    patients_count['processed_patients'] += 1
        shutil.rmtree(json_folder, ignore_errors=True)

    @staticmethod
    def _run_integration(project_name: str, run_uuid: str, dag_run_config: dict):
        """
        Run JSON to RDF conversion

        Args:
            project_name (str): name of the project
            run_uuid (str): UUID of the run
            dag_run_config (dict): configuration for the running Airflow dag
        """
        print(f"Running integration for project '{project_name}'")
        connector = Connector(project=project_name)
        patients_count = {"processed_patients": 0, "failed_patients": 0}
        patients_with_warnings = 0
        step = Steps.INTEGRATION
        start_timestamp = datetime.now()
        connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.RUNNING, patients_processed=None, patients_with_warnings=None, patients_with_errors=None,
                                  execution_time=start_timestamp, elapsed_time=None, run_uuid=run_uuid)
        try:
            patient_id = dag_run_config.get('patient_id')
            if patient_id is not None:
                patient_id = str(patient_id)
            refined_data = connector.get_patient_data(project_name=project_name, patient_id=patient_id, layer=DataPersistenceLayer.REFINED_ZONE)
            if not refined_data:
                print(f"No files found to integrate for project '{project_name}'")
                connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.SKIPPED, patients_processed=patients_count['processed_patients'], patients_with_warnings=patients_with_warnings, patients_with_errors=patients_count['failed_patients'],
                                          execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)
            else:
                airflow = Airflow()
                concurrent_projects_msg = airflow.get_concurrent_projects_msg(project=project_name)
                patient_groups = connector.get_patient_groups(data=refined_data, step=step)
                for i_group, patient_group in patient_groups.items():
                    parallelization_message = f"Step processing {str(patient_group['parallelization'])} patients in parallel for patient group '{str(i_group)}'"
                    if 'warning' in patient_group:
                        parallelization_message += f". WARNING -- Max. Java heap space memory required ({patient_group['max_java_heap_space']}GB) is more than the memory available to the Connector ({connector.config.available_memory}GB)"
                    patients_data = patient_group['patients_data']
                    if patients_data:
                        chunks = Connector._chunks(data=patients_data, parallelization=patient_group['parallelization'])
                        connector.database.create_connection()
                        connector.cleanup_logging(project=project_name, patient_ids=patients_data.keys(), step=Steps.INTEGRATION)
                        for patient_id in patients_data.keys():
                            connector.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.PARALLELIZATION, parallel=True, message=parallelization_message)
                            if concurrent_projects_msg is not None:
                                connector.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.CONCURRENCY, parallel=True, message=concurrent_projects_msg)
                        connector.database.close_connection()
                        with ThreadPoolExecutor(max_workers=patient_group['parallelization']) as executor:
                            futures = []
                            for i_chunk, chunk in enumerate(chunks):
                                futures.append(executor.submit(execute_chunk_integration, i_chunk, chunk, project_name, run_uuid, patient_group['max_java_heap_space']))
                            for future in as_completed(futures):
                                patients_count_result, chunk_folder = future.result()
                                for key in patients_count.keys():
                                    patients_count[key] += patients_count_result[key]
                                shutil.rmtree(chunk_folder, ignore_errors=True)
                                connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.RUNNING, patients_processed=patients_count['processed_patients'], patients_with_warnings=patients_with_warnings, patients_with_errors=patients_count['failed_patients'], 
                                                               execution_time=start_timestamp, elapsed_time=None, run_uuid=run_uuid)
                        del chunks
                    del patients_data
                connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.SUCCESS, patients_processed=patients_count['processed_patients'], patients_with_warnings=patients_with_warnings, patients_with_errors=patients_count['failed_patients'], 
                                        execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)
                del patient_groups
            del refined_data
        except Exception:
            connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.FAILED, patients_processed=patients_count['processed_patients'], patients_with_warnings=patients_with_warnings, patients_with_errors=patients_count['failed_patients'],
                                      execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)
            raise

    @staticmethod
    def _chunks(data: dict, parallelization: int):
        """Split patient data in chunks

        Args:
            data (dict): patient data

        Yields:
            dict: chunk data
        """
        if len(data.keys()) % parallelization == 0:
            size = int(len(data.keys()) / parallelization)
        else:
            size = int(len(data.keys()) / parallelization) + 1
        it = iter(data)
        for i in range(0, len(data), size):
            yield {k:data[k] for k in islice(it, size)}

    def setup_chunk_environment(self, 
                                project_name: str, 
                                i_chunk: int, 
                                validation_log_level: LogLevel, 
                                step: Steps, 
                                severity_level: QCSeverityLevel = QCSeverityLevel.INFO) -> Union[str, str, str, str]:
        """Setup environment for chunk

        Args:
            project_name (str): name of the project
            i_chunk (int): chunk identifier
            validation_log_level (LogLevel): validation logging level
            step (Steps): pipeline step
            severity_level (QCSeverityLevel): QC severity level

        Returns:
            Union[str, str, str]: paths of chunk test folder, properties file, logs folder, TDB2 folder
        """
        if step == Steps.VALIDATION:
            project_folder = os.path.join('/home/sphn/quality/', project_name)
        else:
            project_folder = os.path.join('/home/sphn/statistics/', project_name)
        properties_file = os.path.join(project_folder, "{}-QC_{}.properties".format(project_name, validation_log_level.value.upper()))
        test_data_folder = os.path.join(project_folder, "data/test_data")
        chunk_test_folder = os.path.join(test_data_folder, str(i_chunk))
        chunk_properties_file = os.path.join(project_folder, f"{str(i_chunk)}_{os.path.basename(properties_file)}")
        logs_folder = os.path.join(project_folder, "log")
        chunk_logs_folder = os.path.join(logs_folder, str(i_chunk))
        sparqler_folder = os.path.join(project_folder, "sparqler-output")
        chunk_sparqler_folder = os.path.join(sparqler_folder, str(i_chunk))
        qc_queries_out_folder = os.path.join(project_folder, "qc-queries-output")
        chunk_qc_queries_out_folder = os.path.join(qc_queries_out_folder, str(i_chunk))
        tdb2_folder_0 = os.path.join("/home/sphn/quality/tdb2", project_name, '0')
        chunk_tdb2_folder = os.path.join("/home/sphn/quality/tdb2", project_name, str(i_chunk))
        os.makedirs(chunk_test_folder, exist_ok=True)
        os.makedirs(chunk_logs_folder, exist_ok=True)
        os.makedirs(chunk_qc_queries_out_folder, exist_ok=True)
        with open(properties_file, 'r', encoding='utf-8') as input:
            with open(chunk_properties_file, 'w', encoding='utf-8') as output:
                data = input.read()
                data = data.replace(f'resources.dir={test_data_folder}', f'resources.dir={chunk_test_folder}')
                data = data.replace(f'logs.dir={logs_folder}', f'logs.dir={chunk_logs_folder}')
                data = data.replace(f'sparqler-output.dir={sparqler_folder}', f'sparqler-output.dir={chunk_sparqler_folder}')
                data = data.replace(f'qc-queries-output.dir={qc_queries_out_folder}', f'qc-queries-output.dir={chunk_qc_queries_out_folder}')
                data = data.replace(f'tdb2prefix={tdb2_folder_0}', f'tdb2prefix={chunk_tdb2_folder}')
                data = data.replace('severity=INFO', f'severity={severity_level.value}')
                output.write(data)
        return chunk_test_folder, chunk_properties_file, chunk_logs_folder, chunk_sparqler_folder, chunk_qc_queries_out_folder

    def cleanup_chunk_environment(self, 
                                  test_folder: str, 
                                  logs_folder: str, 
                                  properties_file: str, 
                                  step = Steps, 
                                  sparqler_folder: str = None, 
                                  qc_queries_out_folder: str = None):
        """Cleanup chunk environment

        Args:
            test_folder (str): path to chunk test folder
            logs_folder (str): path to chunk test folder
            properties_file (str): path to chunk properties file
            step (Steps): pipeline step
            sparqler_folder (str): path to chunk sparqler folder
            qc_queries_out_folder (str): path to chunk qc queries folder
        """
        shutil.rmtree(test_folder, ignore_errors=True)
        if step != Steps.VALIDATION:
            shutil.rmtree(logs_folder, ignore_errors=True)
        shutil.rmtree(properties_file, ignore_errors=True)
        if sparqler_folder is not None:
            if step != Steps.STATISTICS:
                shutil.rmtree(sparqler_folder, ignore_errors=True)
        if qc_queries_out_folder is not None:
            shutil.rmtree(qc_queries_out_folder, ignore_errors=True)

    def update_patient_stats(self, project_name: str, patient_id: str, sparqler_folder: str, qc_queries_out_folder: str, profile: StatisticsProfile):
        """Update patient's statistics data

        Args:
            project_name (str): name of the project
            patient_id (str): ID of the patient
            sparqler_folder (str): path to chunk sparqler folder
            qc_queries_out_folder (str): path to chunk qc queries folder
            profile (StatisticsProfile): statistics profile
        """
        self.update_standard_report_data(project_name=project_name, patient_id=patient_id, sparqler_folder=sparqler_folder, profile=profile)
        self.update_individual_report_data(project_name=project_name, patient_id=patient_id, qc_queries_out_folder=qc_queries_out_folder)

    def update_standard_report_data(self, project_name: str, patient_id: str, sparqler_folder: str, profile: StatisticsProfile):
        """Update data in DB for standardized reports (from SPARQLer queries)

        Args:
            project_name (str): name of the project
            patient_id (str): ID of the patient
            sparqler_folder (str): path to chunk sparqler folder
            profile (StatisticsProfile): statistics profile
        """
        patient_folder = os.path.join(sparqler_folder, patient_id)
        if os.path.exists(patient_folder):
            self.update_logging(project_name=project_name, patient_id=patient_id, step=Steps.STATISTICS, dag_run_status=DagRun.STANDARD_PATIENT_STATS, message="Upading statistics tables..." , parallel=True)
            today = datetime.now()
            self.update_count_instances_table(patient_folder=patient_folder, project_name=project_name, patient_id=patient_id, today=today)
            self.update_has_code_table(patient_folder=patient_folder, project_name=project_name, patient_id=patient_id, today=today)
            self.update_encounters(patient_folder=patient_folder, project_name=project_name, patient_id=patient_id, today=today)
            self.update_min_max_predicates(patient_folder=patient_folder, project_name=project_name, patient_id=patient_id, today=today, profile=profile)
        else:
            self.update_logging(project_name=project_name, patient_id=patient_id, step=Steps.STATISTICS, dag_run_status=DagRun.STANDARD_PATIENT_STATS, message="No patient's statistics found" , parallel=True)

    def update_individual_report_data(self, project_name: str, patient_id: str, qc_queries_out_folder: str):
        """Update data in DB for individual reports (from uploaded QC queries)

        Args:
            project_name (str): name of the project
            patient_id (str): ID of the patient
            qc_queries_out_folder (str): path to chunk qc queries folder
        """
        patient_folder = os.path.join(qc_queries_out_folder, patient_id)
        if os.path.exists(patient_folder):
            self.update_logging(project_name=project_name, patient_id=patient_id, step=Steps.STATISTICS, dag_run_status=DagRun.INDIVIDUAL_PATIENT_STATS, message="Upading statistics tables..." , parallel=True)
            today = datetime.now()
            self.upload_qc_queries_results(patient_folder=patient_folder, project_name=project_name, patient_id=patient_id, today=today)
        else:
            self.update_logging(project_name=project_name, patient_id=patient_id, step=Steps.STATISTICS, dag_run_status=DagRun.INDIVIDUAL_PATIENT_STATS, message="No patient's statistics found" , parallel=True)

    def upload_qc_queries_results(self, patient_folder: str, project_name: str, patient_id: str, today: datetime):
        """Update QC queries results into DB

        Args:
            patient_folder (str): patient folder with query results
            project_name (str): name of the project
            patient_id (str): ID of the patient
            today (datetime): current datetime
        """
        qc_output_files = os.listdir(patient_folder)
        for output_file in qc_output_files:
            filename = os.path.join(patient_folder, output_file)
            query_name = output_file.replace('.csv', '')
            with open(filename, 'r', encoding='utf-8') as file:
                lines = file.readlines()
                header = lines[0]
                csv_data = "".join(lines[1:])
                if csv_data:
                    sql = "INSERT INTO stats_individual_reports (project_name, data_provider_id, patient_id, query_name, timestmp, header, query_output) VALUES (%s, %s, %s, %s, %s, %s, %s) \
                           ON CONFLICT (project_name, data_provider_id, patient_id, query_name) DO UPDATE SET timestmp=EXCLUDED.timestmp, header=EXCLUDED.header, query_output=EXCLUDED.query_output"
                    self.database.cursor.execute(sql, (project_name, self.config.data_provider_id, patient_id, query_name, today, header, csv_data))

    def update_count_instances_table(self, patient_folder: str, project_name: str, patient_id: str, today: datetime):
        """Update table 'sparqler_count_instances'

        Args:
            patient_folder (str): patient folder with query results
            project_name (str): name of the project
            patient_id (str): ID of the patient
            today (datetime): current datetime
        """
        count_instances_results = os.path.join(patient_folder, 'count-instances.csv')
        if os.path.exists(count_instances_results):
            with open(count_instances_results, 'r', encoding='utf-8') as file:
                reader = csv.reader(file)
                records = []
                try:
                    next(reader)
                    records = list(set([(project_name, self.config.data_provider_id, patient_id, today, row[0], row[1]) for row in reader if row]))
                except StopIteration:
                    pass

            if records:
                output_csv = io.StringIO()
                csv_writer = csv.writer(output_csv)
                csv_writer.writerow(['project_name', 'data_provider_id', 'patient_id', 'timestmp', 'concept', 'count'])
                csv_writer.writerows(records)

                copy_sql = """
                COPY sparqler_count_instances (
                    project_name, data_provider_id, patient_id, timestmp, concept, count
                ) FROM stdin WITH CSV HEADER
                """
                output_csv.seek(0)
                self.database.cursor.copy_expert(sql=copy_sql, file=output_csv)


    def update_has_code_table(self, patient_folder: str, project_name: str, patient_id: str, today: datetime):
        """Update table 'sparqler_has_code'

        Args:
            patient_folder (str): patient folder with query results
            project_name (str): name of the project
            patient_id (str): ID of the patient
            today (datetime): current datetime
        """
        has_code_results = os.path.join(patient_folder, 'has-code.csv')
        if os.path.exists(has_code_results):
            with open(has_code_results, 'r', encoding='utf-8') as file:
                reader = csv.reader(file)
                records = []
                try:
                    next(reader)
                    records = list(set([(project_name, self.config.data_provider_id, patient_id, today, row[0], row[1], row[2], row[3]) for row in reader if row]))
                except StopIteration:
                    pass

            if records:
                output_csv = io.StringIO()
                csv_writer = csv.writer(output_csv)
                csv_writer.writerow(['project_name', 'data_provider_id', 'patient_id', 'timestmp', 'concept', 'origin', 'code', 'count_instances'])
                csv_writer.writerows(records)

                copy_sql = """
                COPY sparqler_has_code (
                    project_name, data_provider_id, patient_id, timestmp, concept, attribute, code, count
                ) FROM stdin WITH CSV HEADER
                """
                output_csv.seek(0)
                self.database.cursor.copy_expert(sql=copy_sql, file=output_csv)

    def update_encounters(self, patient_folder: str, project_name: str, patient_id: str, today: datetime):
        """Update table 'sparqler_administrative_case_encounters'

        Args:
            patient_folder (str): patient folder with query results
            project_name (str): name of the project
            patient_id (str): ID of the patient
            today (datetime): current datetime
        """
        encounters_results = os.path.join(patient_folder, 'administrative-case-encounters.csv')
        if os.path.exists(encounters_results):
            with open(encounters_results, 'r', encoding='utf-8') as file:
                reader = csv.DictReader(file)
                records = set()
                try:
                    next(reader)
                    for row in reader:
                        if row:
                            prefix, name = niceify_prefix(prefixable_iri=row['concept'])
                            records.add((project_name, self.config.data_provider_id, patient_id, today, f"{prefix}:{name}", row['administrativeCase']))
                except StopIteration:
                    pass
            records = list(records)

            if records:
                output_csv = io.StringIO()
                csv_writer = csv.writer(output_csv)
                csv_writer.writerow(['project_name', 'data_provider_id', 'patient_id', 'timestmp', 'concept', 'origin', 'code', 'count_instances'])
                csv_writer.writerows(records)

                copy_sql = """
                COPY sparqler_administrative_case_encounters (
                    project_name, data_provider_id, patient_id, timestmp, concept, administrative_case
                ) FROM stdin WITH CSV HEADER
                """
                output_csv.seek(0)
                self.database.cursor.copy_expert(sql=copy_sql, file=output_csv)

    def fill_records(self, reader: csv.reader, project_name: str, patient_id: str, today: datetime, profile: StatisticsProfile) -> Union[list, list]:
        """Extract data from CSV query results

        Args:
            reader (csv.reader): CSV reader
            project_name (str): project name
            patient_id (str): patient ID
            today (datetime): today's time
            profile (StatisticsProfile): statistics profile

        Returns:
            Union[list, list]: numeric and non-numeric records
        """
        pattern = r'^(-)?\d+(\.\d+)?E(-)?\d+$'
        records = []
        numeric_records = []
        errors = []
        for row in reader:
            if row:
                if profile == StatisticsProfile.EXTENSIVE:
                    if re.match(pattern, row[2]) is not None or re.match(pattern, row[3]) is not None or re.match(pattern, row[4]) is not None:
                        if re.match(pattern, row[2]) is not None and re.match(pattern, row[3]) is not None and re.match(pattern, row[4]) is not None:
                            numeric_records.append((project_name, self.config.data_provider_id, patient_id, today, row[0], row[1], row[2], row[3], row[4]))
                        else:
                            errors.append(f"Concept '{row[0]}', attribute '{row[1]}', min '{row[2]}', max '{row[3]}', and value '{row[4]}'")
                    else:
                        records.append((project_name, self.config.data_provider_id, patient_id, today, row[0], row[1], row[2], row[3], row[4]))
                else:
                    if re.match(pattern, row[2]) is not None or re.match(pattern, row[3]) is not None:
                        if re.match(pattern, row[2]) is not None and re.match(pattern, row[3]):
                            numeric_records.append((project_name, self.config.data_provider_id, patient_id, today, row[0], row[1], row[2], row[3], row[4]))
                        else:
                            errors.append(f"Concept '{row[0]}', attribute '{row[1]}', min '{row[2]}', max '{row[3]}', and count '{row[4]}'")
                    else:
                        records.append((project_name, self.config.data_provider_id, patient_id, today, row[0], row[1], row[2], row[3], row[4]))

        if errors:
            self.update_logging(project_name=project_name, patient_id=patient_id, step=Steps.STATISTICS, dag_run_status=DagRun.STATS_UPDATE_FAILURE, message=f"Failed to update patient '{patient_id}' min/max statistics table:\n\t" + '\n\t'.join(errors) , parallel=True)
            raise Exception("Failed update of min/max table")
        return list(set(records)), list(set(numeric_records))

    def update_min_max_predicates(self, patient_folder: str, project_name: str, patient_id: str, today: datetime, profile: StatisticsProfile):
        """Update table 'sparqler_min_max_predicates'

        Args:
            patient_folder (str): patient folder with query results
            project_name (str): name of the project
            patient_id (str): ID of the patient
            today (datetime): current datetime
            profile (StatisticsProfile): statistics profile
        """
        if profile == StatisticsProfile.EXTENSIVE:
            min_max_predicates_results = os.path.join(patient_folder, 'min-max-predicates-extensive.csv')
        else:
            min_max_predicates_results = os.path.join(patient_folder, 'min-max-predicates-fast.csv')
        if os.path.exists(min_max_predicates_results):
            with open(min_max_predicates_results, 'r', encoding='utf-8') as file:
                reader = csv.reader(file)
                records = []
                numeric_records = []
                try:
                    next(reader)
                    records, numeric_records = self.fill_records(reader, project_name, patient_id, today, profile)
                except StopIteration:
                    pass
            if profile == StatisticsProfile.EXTENSIVE:
                columns = ['project_name', 'data_provider_id', 'patient_id', 'timestmp', 'concept', 'attribute', 'min', 'max', 'value']
                if records:
                    output_csv = io.StringIO()
                    csv_writer = csv.writer(output_csv)
                    csv_writer.writerow(columns)
                    csv_writer.writerows(records)

                    copy_sql = """
                    COPY sparqler_min_max_predicates_extensive (
                        project_name, data_provider_id, patient_id, timestmp, concept, attribute, min, max, value
                    ) FROM stdin WITH CSV HEADER
                    """
                    output_csv.seek(0)
                    self.database.cursor.copy_expert(sql=copy_sql, file=output_csv)

                if numeric_records:
                    output_csv = io.StringIO()
                    csv_writer = csv.writer(output_csv)
                    csv_writer.writerow(columns)
                    csv_writer.writerows(numeric_records)

                    copy_sql = """
                    COPY sparqler_min_max_predicates_numeric_extensive (
                        project_name, data_provider_id, patient_id, timestmp, concept, attribute, min, max, value
                    ) FROM stdin WITH CSV HEADER
                    """
                    output_csv.seek(0)
                    self.database.cursor.copy_expert(sql=copy_sql, file=output_csv)
            else:
                columns = ['project_name', 'data_provider_id', 'patient_id', 'timestmp', 'concept', 'attribute', 'min', 'max', 'count']
                if records:
                    output_csv = io.StringIO()
                    csv_writer = csv.writer(output_csv)
                    csv_writer.writerow(columns)
                    csv_writer.writerows(records)

                    copy_sql = """
                    COPY sparqler_min_max_predicates_fast (
                        project_name, data_provider_id, patient_id, timestmp, concept, attribute, min, max, count
                    ) FROM stdin WITH CSV HEADER
                    """
                    output_csv.seek(0)
                    self.database.cursor.copy_expert(sql=copy_sql, file=output_csv)

                if numeric_records:
                    output_csv = io.StringIO()
                    csv_writer = csv.writer(output_csv)
                    csv_writer.writerow(columns)
                    csv_writer.writerows(numeric_records)

                    copy_sql = """
                    COPY sparqler_min_max_predicates_numeric_fast (
                        project_name, data_provider_id, patient_id, timestmp, concept, attribute, min, max, count
                    ) FROM stdin WITH CSV HEADER
                    """
                    output_csv.seek(0)
                    self.database.cursor.copy_expert(sql=copy_sql, file=output_csv)

    def send_tdb2_logs(self, project_name: str, patient_data: dict, step: Steps, dag_run_status: DagRun, message: str):
        """Send logs for TDB2 build

        Args:
            project_name (str): name of the project
            patient_data (dict): patient data map
            step (Steps): pipeline step
            dag_run_status (DagRun): dag run status
            message (str): logging message
        """
        self.database.create_connection()
        for patient_id in patient_data.keys():
            self.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=dag_run_status, parallel=True, message=message)
        self.database.close_connection()

        

    @staticmethod
    def _build_tdb2_model(project_name: str, validation_log_level: LogLevel, step: Steps, run_uuid: str, dag_run_config:dict):
        """
        Build TDB2 model if not already instantiated

        Args:
            project_name (str): name of the project
            validation_log_level (LogLevel): validation log level
            step (Steps): pipeline step
            run_uuid (str): UUID of the run
            dag_run_config (dict): configuration for the running Airflow dag

        Raises:
            Exception: execution failure
        """
        connector = Connector(project=project_name)
        patient_id = dag_run_config.get('patient_id')
        if patient_id is not None:
            patient_id = str(patient_id)
        start_timestamp = datetime.now()
        validation_log_level = LogLevel(validation_log_level)
        if step == Steps.VALIDATION:
            project_folder = os.path.join('/home/sphn/quality/', project_name)
        else:
            project_folder = os.path.join('/home/sphn/statistics/', project_name)
        properties_file = os.path.join(project_folder, "{}-QC_{}.properties".format(project_name, validation_log_level.value.upper()))
        project_tdb2_folder = os.path.join("/home/sphn/quality/tdb2/", project_name)
        graph_data = connector.get_patient_data(project_name=project_name, patient_id=patient_id, layer=DataPersistenceLayer.GRAPH_ZONE)
        patient_data = {key: value['object_name'] for key, value in graph_data.items()}
        connector.database.create_connection()
        connector.cleanup_logging(project=project_name, patient_ids=patient_data.keys(), step=step)
        connector.database.close_connection()
        if not connector.database.is_tdb2_up_to_date(project=project_name) or not os.path.exists(project_tdb2_folder):
            shutil.rmtree(project_tdb2_folder, ignore_errors=True)
            connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.RUNNING, patients_processed=None, patients_with_warnings=None, patients_with_errors=None,
                                  execution_time=start_timestamp, elapsed_time=None, run_uuid=run_uuid)
            if step == Steps.VALIDATION:
                tdb2_message = "Start building TDB2 model for QAF validation"
            else:
                tdb2_message = "Start building TDB2 model for patients statistics"
            connector.send_tdb2_logs(project_name=project_name, patient_data=patient_data, step=step, dag_run_status=DagRun.TDB2_BUILD_START, message=tdb2_message)
            cmd = ["java", "-jar", "/home/sphn/quality/target/sphn-quality-check-tool-v2.3-full.jar", "-properties", properties_file, "-build"]
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            output, error = p.communicate()
            if p.returncode == 0:
                connector.database.update_tdb2_status_table(project=project_name, status=True)
                connector.send_tdb2_logs(project_name=project_name, patient_data=patient_data, step=step, dag_run_status=DagRun.TDB2_BUILD_END, message="TDB2 model built successfully")
                tdb2_folder_0 = os.path.join(project_tdb2_folder, '0')
                for i_chunk in range(1, connector.config.machine_cpus):
                    chunk_tdb2_folder = os.path.join(project_tdb2_folder, str(i_chunk))
                    try:
                        shutil.copytree(tdb2_folder_0, chunk_tdb2_folder)
                    except:
                        print(f"Failed to copy {tdb2_folder_0} to {chunk_tdb2_folder}")
            else:
                print("Exception raised when creating TDB2 model")
                connector.send_tdb2_logs(project_name=project_name, patient_data=patient_data, step=step, dag_run_status=DagRun.TDB2_BUILD_END, message=f"TDB2 model built failed. Exit code: {p.returncode}")
                connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.FAILED, patients_processed=None, patients_with_warnings=None, patients_with_errors=None,
                                          execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)
                if output:
                    print(output.decode())
                if error:
                    print(error.decode())
                raise Exception()

    def check_terminology_imports(self, project_name: str, patient_ids: list) -> int:
        """If TDB2 is not up to date, check that the schema imports are matched

        Args:
            project_name (str): name of the project
            patient_ids (list): patient identifier

        Returns:
            int: number of patients with warnings
        """
        warnings = 0
        schema_imports = self.database.get_schema_imports(project_name=project_name)
        terminologies_iris = self.database.get_project_terminologies_iri(project=project_name, parallel=True)
        missing_schemas = []
        for schema_import in schema_imports:
            if schema_import not in terminologies_iris:
                missing_schemas.append(schema_import)
        if missing_schemas:
            alignment = ' '*36
            missing_terminologies = f"\n{alignment}".join([f"'{ont}'" for ont in missing_schemas])
            schema_imports_list = ", ".join([f"'{ont}'" for ont in schema_imports])
            msg = f"Missing terminologies:\n{alignment}{missing_terminologies}\n{alignment}Please upload external terminologies to match the needed schema imports: {schema_imports_list}"
            for patient_id in patient_ids:
                self.update_logging(project_name=project_name, patient_id=patient_id, step=Steps.VALIDATION, dag_run_status=DagRun.WARNING_IMPORTS, message=msg, parallel=True)
                warnings += 1
        else:
            for patient_id in patient_ids:
                self.delete_missing_schema_warning(project_name=project_name, patient_id=patient_id, step=Steps.VALIDATION, status=DagRun.WARNING_IMPORTS, parallel=True)
        return warnings

    @staticmethod
    def _run_validation(project_name: str, 
                        validation_log_level: LogLevel, 
                        run_uuid: str, 
                        dag_run_config: dict, 
                        severity_level: QCSeverityLevel, 
                        revalidate: bool):
        """
        Run QAF validation
        Args:
            project_name (str): name of the project
            validation_log_level (str): logging level for QAF validation
            run_uuid (str):  UUID of the run
            dag_run_config (dict): configuration for the running Airflow dag
            severity_level (QCSeverityLevel): QC severity level
            revalidate (bool): revalidate all the data
        """
        print(f"Running validation for project '{project_name}'")
        connector = Connector(project=project_name)
        einstein = connector.database.is_einstein_activated(project=project_name) and connector.config.einstein
        patients_count = {'processed_patients': 0, 'failed_patients': 0, 'patients_with_warnings': 0}
        step = Steps.VALIDATION
        start_timestamp = datetime.now()
        sphn_base_iri = connector.database.get_sphn_base_iri(project=project_name)
        project_output_format, compression = connector.database.get_project_output_format(project=project_name)
        connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.RUNNING, patients_processed=None, patients_with_warnings=None, patients_with_errors=None,
                                  execution_time=start_timestamp, elapsed_time=None, run_uuid=run_uuid)
        try:
            patient_ids = set()
            patient_id = dag_run_config.get('patient_id')
            if patient_id is not None:
                patient_id = str(patient_id)
            validation_log_level = LogLevel(validation_log_level)
            graph_data = connector.get_patient_data(project_name=project_name, patient_id=patient_id, layer=DataPersistenceLayer.GRAPH_ZONE, revalidate=revalidate)
            if not graph_data:
                print(f"No RDF files found for validation for project '{project_name}'")
                connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.SKIPPED, patients_processed=patients_count['processed_patients'],
                                               patients_with_warnings=patients_count['patients_with_warnings'], patients_with_errors=patients_count['failed_patients'],
                                               execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)
                return
            airflow = Airflow()
            concurrent_projects_msg = airflow.get_concurrent_projects_msg(project=project_name)
            patient_groups = connector.get_patient_groups(data=graph_data, step=step)
            for i_group, patient_group in patient_groups.items():
                parallelization_message = f"Step processing {str(patient_group['parallelization'])} patients in parallel for patient group '{str(i_group)}'"
                if 'warning' in patient_group:
                    parallelization_message += f". WARNING -- Max. Java heap space memory required ({patient_group['max_java_heap_space']}GB) is more than the memory available to the Connector ({connector.config.available_memory}GB)"
                patient_data = patient_group['patients_data']
                if patient_data:
                    connector.database.create_connection()
                    patients_with_warnings = connector.check_terminology_imports(project_name=project_name, patient_ids=patient_data.keys())
                    patients_count['patients_with_warnings'] = patients_with_warnings
                    for patient_id in patient_data.keys():
                        patient_ids.add(patient_id)
                        connector.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.PARALLELIZATION, parallel=True, message=parallelization_message)
                        if concurrent_projects_msg is not None:
                            connector.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.CONCURRENCY, parallel=True, message=concurrent_projects_msg)
                        connector.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.BACKLOG, parallel=True)
                    connector.database.close_connection()
                    chunks = Connector._chunks(data=patient_data, parallelization=patient_group['parallelization'])
                    with ThreadPoolExecutor(max_workers=patient_group['parallelization']) as executor:
                        futures = []
                        for i_chunk, chunk in enumerate(chunks):
                            futures.append(executor.submit(execute_validation, i_chunk, chunk, project_name, validation_log_level, run_uuid, patient_group['max_java_heap_space'], sphn_base_iri, project_output_format, compression, einstein,
                                                            severity_level))
                        for future in as_completed(futures):
                            patients_count_result, logs_folder = future.result()
                            for key in patients_count.keys():
                                if key != 'patients_with_warnings' or patients_with_warnings == 0:
                                    patients_count[key] += patients_count_result[key]
                            shutil.rmtree(logs_folder, ignore_errors=True)
                            connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.RUNNING, patients_processed=patients_count['processed_patients'], 
                                                            patients_with_warnings=patients_count['patients_with_warnings'], patients_with_errors=patients_count['failed_patients'],
                                                            execution_time=start_timestamp, elapsed_time=None, run_uuid=run_uuid)
            del patient_groups
            del graph_data

            connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.SUCCESS, patients_processed=patients_count['processed_patients'], 
                                           patients_with_warnings=patients_count['patients_with_warnings'], patients_with_errors=patients_count['failed_patients'], 
                                           execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)
        except Exception:
            connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.FAILED, patients_processed=patients_count['processed_patients'], 
                                           patients_with_warnings=patients_count['patients_with_warnings'], patients_with_errors=patients_count['failed_patients'],
                                             execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)
            raise
    
    @staticmethod
    def _run_notify_einstein(project_name: str, run_uuid: str, dag_run_config: dict):
        """
        Run Einstein notification
        Args:
            project_name (str): name of the project
            run_uuid (str):  UUID of the run
            dag_run_config (dict): configuration for the running Airflow dag
        """
        step = Steps.NOTIFY_EINSTEIN
        start_timestamp = datetime.now()
        connector = Connector(project=project_name)
        patients_count = {'processed_patients': 0, 'failed_patients': 0, 'patients_with_warnings': 0}
        einstein = connector.database.is_einstein_activated(project=project_name) and connector.config.einstein
        if einstein:
            connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.RUNNING, patients_processed=None, patients_with_warnings=None, patients_with_errors=None,
                                           execution_time=start_timestamp, elapsed_time=None, run_uuid=run_uuid)
            try:
                patient_id = dag_run_config.get('patient_id')
                if patient_id is not None:
                    patient_id = str(patient_id)
                einstein_data = connector.database.get_patients_data_to_share(project_name=project_name, patient_id=patient_id)
                if not einstein_data['patients']:
                    connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.SKIPPED, patients_processed=patients_count['processed_patients'], 
                                                   patients_with_warnings=patients_count['patients_with_warnings'], patients_with_errors=patients_count['failed_patients'],
                                                   execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)
                else:
                    connector.send_notification(project_name=project_name, patients_count=patients_count, einstein_data=einstein_data, run_uuid=run_uuid)
                    connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.SUCCESS, patients_processed=patients_count['processed_patients'], 
                                                   patients_with_warnings=patients_count['patients_with_warnings'], patients_with_errors=patients_count['failed_patients'], 
                                                   execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)
            except Exception:
                connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.FAILED, patients_processed=patients_count['processed_patients'], 
                                               patients_with_warnings=patients_count['patients_with_warnings'], patients_with_errors=patients_count['failed_patients'],
                                               execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)
                raise
        else:
            print(f"Data sharing with Einstein is not active for project '{project_name}'. To share data with Einstein you must configure EINSTEIN='True' in the .env file, and configure\
                  share_with_einstein=True and output_format must be set to either 'TRIG' or 'NQuads' when creating a project")
            connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.SKIPPED, patients_processed=patients_count['processed_patients'], 
                                           patients_with_warnings=patients_count['patients_with_warnings'], patients_with_errors=patients_count['failed_patients'],
                                           execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)

    def send_notification(self, project_name: str, patients_count: dict, einstein_data: dict, run_uuid: str):
        """Send notification with patients data to Einstein

        Args:
            project_name (str): name of the project
            patients_count (dict): patients count map
            einstein_data (dict): data to share
            run_uuid (str):  UUID of the run
        """
        pushed = False
        retries = 0
        while not pushed and retries < 15:
            try:
                if os.path.exists('/etc/ssl/certs/root-certificate.crt'):
                    response = requests.post(f"{self.database.config.einstein_endpoint}/token", data={"username": self.database.config.einstein_api_user, "password": self.database.config.einstein_api_password}, verify='/etc/ssl/certs/root-certificate.crt')
                else:
                    response = requests.post(f"{self.database.config.einstein_endpoint}/token", data={"username": self.database.config.einstein_api_user, "password": self.database.config.einstein_api_password})
                headers = {'Authorization': 'Bearer ' + response.json()['access_token']}
                bytes_data = json.dumps(einstein_data).encode('utf-8')
                files = {'patient_data': io.BytesIO(bytes_data)}
                if os.path.exists('/etc/ssl/certs/root-certificate.crt'):
                    response = requests.post(f'{self.database.config.einstein_endpoint}/Notify_Einstein', files=files, verify='/etc/ssl/certs/root-certificate.crt', headers=headers)
                else:
                    response = requests.post(f'{self.database.config.einstein_endpoint}/Notify_Einstein', files=files, headers=headers)
                if response.status_code != 200:
                    return_message = f"Got response code '{response.status_code}' with message '{str(response.text)}'."
                    retry_message = f"Wait 60 seconds and trigger re-try {retries + 1}"
                    print(return_message)
                    print(retry_message)
                    self.database.create_connection()
                    for patient_id in einstein_data['patients']:
                        self.update_logging(project_name=project_name, patient_id=patient_id, step=Steps.NOTIFY_EINSTEIN, dag_run_status=DagRun.DATA_SHARING_RETRY, 
                                            message=f"{return_message} {retry_message}", parallel=True)
                        self.update_execution_errors_table(project_name=project_name, patient_id=patient_id, run_uuid=run_uuid, step=Steps.NOTIFY_EINSTEIN, error_level=ErrorLevel.ERROR, parallel=True)
                    self.database.close_connection()
                    time.sleep(60) 
                    retries += 1                   
                else:
                    pushed = True
                    self.database.create_connection()
                    query = "UPDATE einstein_data_sharing SET status='shared' WHERE project_name=%s"
                    self.database.cursor.execute(query, (project_name, ))
                    patients_count['processed_patients'] = len(einstein_data['patients'])
                    for patient_id in einstein_data['patients']:
                        self.update_logging(project_name=project_name, patient_id=patient_id, step=Steps.NOTIFY_EINSTEIN, dag_run_status=DagRun.DATA_SHARING, 
                                            message=f"Details of patient '{patient_id}' shared with Einstein", parallel=True)
                        self.update_execution_errors_table(project_name=project_name, patient_id=patient_id, run_uuid=run_uuid, step=Steps.NOTIFY_EINSTEIN, error_level=ErrorLevel.ERROR, parallel=True)
                    self.database.close_connection()
                    print(f"Connector notified {len(einstein_data['patients'])} patients successfully to Einstein. Response returned from Einstein endpoint: {response.text}")
                    return
            except Exception as exc:
                return_message = f"Exception raised when notifying data: {exc}."
                retry_message = f"Wait 60 seconds and trigger re-try {retries + 1}"
                print(return_message)
                print(retry_message)
                self.database.create_connection()
                for patient_id in einstein_data['patients']:
                    self.update_logging(project_name=project_name, patient_id=patient_id, step=Steps.NOTIFY_EINSTEIN, dag_run_status=DagRun.DATA_SHARING_RETRY, 
                                        message=f"{return_message} {retry_message}", parallel=True)
                self.database.close_connection()
                time.sleep(60)
                retries += 1
                
        patients_count['failed_patients'] = len(einstein_data['patients'])

    def set_parallelization(self, patient_groups: dict, step: Steps) -> int:
        """Sets parallelization level based on Java max. heap space and step

        Args:
            patient_groups (dict): patient groups map
            step (Steps): pipeline step

        Returns:
            int: parallelization level
        """
        if step in [Steps.VALIDATION, Steps.STATISTICS]:
            max_cpus = int(self.config.machine_cpus / 1.5) # We assume that for these steps the process can use up to 1.5 cores to execute
        else:
            max_cpus = self.config.machine_cpus
        max_cpus = 1 if max_cpus == 0 else max_cpus
        for patient_group in patient_groups.values():
            parallelization = int(self.config.available_memory / patient_group['max_java_heap_space'])
            if parallelization == 0:
                patient_group['parallelization'] = 1
                patient_group['warning'] = True
            else:
                if parallelization > max_cpus:
                    patient_group['parallelization'] = max_cpus
                else:
                    patient_group['parallelization'] = parallelization
        
    def get_patient_groups(self, data: dict, step: Steps) -> dict:
        """Separate the patients into groups based on step and parallelization level

        Args:
            data (dict): patients data
            step (Steps): pipeline step

        Returns:
            dict: map with patient groups
        """
        if step in [Steps.VALIDATION, Steps.STATISTICS]:
            patient_groups = {
                '1': {'patients_data': {}, 'max_java_heap_space': 4}, 
                '2': {'patients_data': {}, 'max_java_heap_space': 8}, 
                '3': {'patients_data': {}, 'max_java_heap_space': 12}, 
                }
            self.set_parallelization(patient_groups=patient_groups, step=step)
            for patient_id, patient_data in data.items():
                if patient_data['size'] < 512:
                    patient_groups['1']['patients_data'][patient_id] = patient_data['object_name']
                elif 512 <= patient_data['size'] < 1024:
                    patient_groups['2']['patients_data'][patient_id] = patient_data['object_name']
                else:
                    patient_groups['3']['patients_data'][patient_id] = patient_data['object_name']
        else:
            patient_groups = {
                '1': {'patients_data': {}, 'max_java_heap_space': 5}, 
                '2': {'patients_data': {}, 'max_java_heap_space': 8}, 
                '3': {'patients_data': {}, 'max_java_heap_space': 10}, 
                '4': {'patients_data': {}, 'max_java_heap_space': 20}, 
                '5': {'patients_data': {}, 'max_java_heap_space': 25}
                }
            self.set_parallelization(patient_groups=patient_groups, step=step)
            for patient_id, patient_data in data.items():
                if patient_data['size'] < 10:
                    patient_groups['1']['patients_data'][patient_id] = patient_data['object_name']
                elif 10 <= patient_data['size'] < 100:
                    patient_groups['2']['patients_data'][patient_id] = patient_data['object_name']
                elif 100 <= patient_data['size'] < 1024:
                    patient_groups['3']['patients_data'][patient_id] = patient_data['object_name']
                elif 1024 <= patient_data['size'] < 2048:
                    patient_groups['4']['patients_data'][patient_id] = patient_data['object_name']
                else:
                    patient_groups['5']['patients_data'][patient_id] = patient_data['object_name']
        return patient_groups

    @staticmethod
    def _run_statistics(project_name: str, run_uuid: str, profile: str):
        """Run patients statistics

        Args:
            project_name (str): name of the project
            run_uuid (str): UUID of the run
            profile (str): statistics profile
        """
        print(f"Running statistics for project '{project_name}'")
        connector = Connector(project=project_name)
        patients_count = {'processed_patients': 0, 'failed_patients': 0}
        patients_with_warnings = 0
        step = Steps.STATISTICS
        profile = StatisticsProfile(profile)
        connector.database.truncate_statistics(project=project_name)
        start_timestamp = datetime.now()
        connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.RUNNING, patients_processed=None, patients_with_warnings=None, patients_with_errors=None,
                                  execution_time=start_timestamp, elapsed_time=None, run_uuid=run_uuid)
        try:
            graph_data = connector.get_patient_data(project_name=project_name, patient_id=None, layer=DataPersistenceLayer.GRAPH_ZONE, step=Steps.STATISTICS)
            if not graph_data:
                print(f"No RDF files found for statistics on project '{project_name}'")
                connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.SKIPPED, patients_processed=patients_count['processed_patients'], patients_with_warnings=patients_with_warnings, patients_with_errors=patients_count['failed_patients'],
                                          execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)
                return
            airflow = Airflow()
            concurrent_projects_msg = airflow.get_concurrent_projects_msg(project=project_name)
            patient_groups = connector.get_patient_groups(data=graph_data, step=step)
            for i_group, patient_group in patient_groups.items():
                parallelization_message = f"Step processing {str(patient_group['parallelization'])} patients in parallel for patient group '{str(i_group)}'"
                if 'warning' in patient_group:
                    parallelization_message += f". WARNING -- Max. Java heap space memory required ({patient_group['max_java_heap_space']}GB) is more than the memory available to the Connector ({connector.config.available_memory}GB)"
                patient_data = patient_group['patients_data']
                if patient_data:
                    connector.database.create_connection()
                    for patient_id in patient_data.keys():
                        connector.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.PARALLELIZATION, parallel=True, message=parallelization_message)
                        if concurrent_projects_msg is not None:
                            connector.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.CONCURRENCY, parallel=True, message=concurrent_projects_msg)
                        connector.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.BACKLOG, parallel=True)
                    connector.database.close_connection()
                    
                    chunks = Connector._chunks(data=patient_data, parallelization=patient_group['parallelization'])
                    with ThreadPoolExecutor(max_workers=patient_group['parallelization']) as executor:
                            futures = []
                            for i_chunk, chunk in enumerate(chunks):
                                futures.append(executor.submit(execute_statistics, i_chunk, chunk, project_name, run_uuid, profile, patient_group['max_java_heap_space']))
                            for future in as_completed(futures):
                                patients_count_result, sparqler_output_folder = future.result()
                                for key in patients_count.keys():
                                    patients_count[key] += patients_count_result[key]
                                shutil.rmtree(sparqler_output_folder, ignore_errors=True)
                                connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.RUNNING, patients_processed=patients_count['processed_patients'], patients_with_warnings=patients_with_warnings, patients_with_errors=patients_count['failed_patients'], 
                                                               execution_time=start_timestamp, elapsed_time=None, run_uuid=run_uuid)
            connector.database.store_statistics(project=project_name, profile=profile)
            connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.SUCCESS, patients_processed=patients_count['processed_patients'], patients_with_warnings=patients_with_warnings, patients_with_errors=patients_count['failed_patients'],
                                      execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)
        except Exception:
            connector.database.update_history_table(project_name=project_name, pipeline_step=step, pipeline_status=PipelineStep.FAILED, patients_processed=patients_count['processed_patients'], patients_with_warnings=patients_with_warnings, patients_with_errors=patients_count['failed_patients'],
                                      execution_time=start_timestamp, elapsed_time=(datetime.now() - start_timestamp).seconds, run_uuid=run_uuid)
            raise

    @staticmethod
    def _extract_code_term_paths(property_map: dict, code_keys: set, key_path: str, in_array_field: bool = False, in_one_of: bool = False):
        """Extract Code/Terminology key paths

        Args:
            property_map (dict): map of the property
            code_keys (set): collection of key paths
            in_array_field (bool, optional): field is boolean. Defaults to False.
            in_one_of (bool, optional): field is in oneOf. Defaults to False.
        """
        if property_map['type'] == 'object':
            if 'oneOf' in property_map:
                for element in property_map['oneOf']:
                    Connector._extract_code_term_paths(property_map=element, code_keys=code_keys, key_path=key_path)
            else:
                one_of_in_array = in_array_field and in_one_of
                for key, value in property_map['properties'].items():
                    Connector._extract_code_term_paths(property_map=value, code_keys=code_keys, key_path=key_path + '/'+ key, in_array_field=one_of_in_array)
        elif property_map['type'] == 'array':
            if 'oneOf' in property_map['items']:
                for element in property_map['items']['oneOf']:
                    Connector._extract_code_term_paths(property_map=element, code_keys=code_keys, key_path=key_path, in_array_field=True, in_one_of=True)
            else:
                if property_map['items']['type'] == 'object':
                    for key, value in property_map['items']['properties'].items():
                        Connector._extract_code_term_paths(property_map=value, key_path=key_path  + '/' +  key, code_keys=code_keys, in_array_field=True)
                else:
                    Connector._extract_code_term_paths(property_map=property_map['items'], key_path=key_path, code_keys=code_keys, in_array_field=True)

        if 'description' in property_map and property_map['description'] in ["Unique ID for the given IRI. String format follows convention: <coding_system>-<identifier>", "ID of SPHN Concept 'Code'"]:
            # Supporting concepts define sourceConceptType and sourceConceptID by default therefore we expect those values to be there
            if not key_path.startswith('/supporting_concepts/'):
                code_keys.add(key_path.lstrip('/'))

class ValidationResult(object):

    def __init__(self, patient_id: str, status: bool, report: str, log_filename: str):
        """Class constructor

        Args:
            patient_id (str): unique ID of the patient
            status (bool): validation status
            report (str): validation report
            log_filename (str): name of the log file
        """
        self.patient_id = patient_id
        self.status = status
        self.report = report
        self.log_filename = log_filename

    def _get_validation_results(logs_folder: str) -> list:
        validation_results = []
        files = glob.glob(os.path.join(logs_folder, '*', '*'))
        for file in files:
            log_filename = os.path.basename(file)
            validation_status = True if os.path.basename(os.path.dirname(file)) == 'success' else False
            patient_id = log_filename.replace('.log', '')
            report_data = "No report generated"
            with open(file, 'r', encoding='utf-8') as report_file:
                report_data = report_file.read()
            validation_results.append(ValidationResult(patient_id=patient_id, status=validation_status, report=report_data, log_filename=log_filename))
        return validation_results


def execute_pre_check_and_de_identification(i_chunk: int, chunk: dict, project_name: str, run_uuid: str, code_term_paths: set, sphn_version_iri: str) -> dict:
    """Execute pre-checks and de-identification over patient chunks

    Args:
        i_chunk (int): chunk number
        chunk (dict): chunk data
        project_name (str): name of the project
        step (Steps): name of the step
        run_uuid (str): UUID of the run
        code_term_paths (set): collection to paths of codes
        sphn_version_iri (str): SPHN schema version IRI

    Returns:
        dict: patients count
    """
    print(f"Start processing chunk {i_chunk}")
    patients_count = {"processed_patients": 0, "failed_patients": 0, "patients_with_warnings": 0}
    connector = Connector(project=project_name)
    sphn_base_iri = connector.database.get_sphn_base_iri(project=project_name)
    is_legacy = Database._is_legacy_schema(sphn_base_iri=sphn_base_iri)
    connector.database.create_connection()
    pre_checks = create_pre_checks(project=project_name, config=connector.config, is_legacy=is_legacy, sphn_version_iri=sphn_version_iri)
    replace_id_checks = get_replace_id_checks(project=project_name, config=connector.config, pre_checks=pre_checks)
    de_id_rules, anonymized = create_de_identification_rules(project=project_name, config=connector.config, database=connector.database, i_chunk=i_chunk)
    step = Steps.PRE_CHECK_DE_ID
    for patient_id, patient_file in chunk.items():
        if pre_checks or de_id_rules:
            patient_data_type = FileType.JSON if patient_file['object_name'].endswith('.json') else FileType.RDF
            if not pre_checks and patient_data_type == FileType.RDF:
                connector.database.move_to_next_layer(project=project_name, patient_id=patient_id, object_name=patient_file['object_name'], source_zone=DataPersistenceLayer.LANDING_ZONE, copy_only=True, parallel=True)
            else:
                if patient_data_type == FileType.JSON:
                    patient_data = json.loads(connector.config.s3_client.get_object(Bucket=connector.config.s3_connector_bucket, Key=patient_file['object_name'])['Body'].read().decode('utf8'))
                else:
                    patient_data = connector.config.s3_client.get_object(Bucket=connector.config.s3_connector_bucket, Key=patient_file['object_name'])["Body"].read()
                keys = Connector._get_keys(data=patient_data, enumerated=False) if patient_data_type == FileType.JSON else None
                enumerated_keys = Connector._get_keys(data=patient_data, enumerated=True) if patient_data_type == FileType.JSON else None
                if pre_checks:
                    print(f"{{'chunk': '{i_chunk}', 'patient': '{patient_id}'}}: Start pre-checks execution")
                    checks_failed, stop_file, message = connector.execute_pre_checks(patient_data=patient_data, pre_checks=pre_checks, file_type=patient_data_type, keys=keys,
                                                                                     code_term_paths=code_term_paths, ingestion_type=patient_file['ingestion_type'], replace_id_checks=replace_id_checks,
                                                                                     i_chunk=i_chunk, patient_id=patient_id)
                    connector.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.PRE_CHECK, message=message, parallel=True)
                    if checks_failed:
                        if stop_file:
                            patients_count["failed_patients"] += 1
                            patients_count["processed_patients"] += 1
                            connector.update_execution_errors_table(project_name=project_name, patient_id=patient_id, run_uuid=run_uuid, step=Steps.PRE_CHECK_DE_ID, error_level=ErrorLevel.ERROR, parallel=True)
                            continue
                        else:
                            patients_count["patients_with_warnings"] += 1
                            connector.update_execution_errors_table(project_name=project_name, patient_id=patient_id, run_uuid=run_uuid, step=Steps.PRE_CHECK_DE_ID, error_level=ErrorLevel.WARNING, parallel=True)
                if de_id_rules:
                    print(f"{{'chunk': '{i_chunk}', 'patient': '{patient_id}'}}: Start de-identification rules execution")
                    de_identification_failed, de_identification_message = connector.execute_de_identification_rules(patient_data=patient_data, de_id_rules=de_id_rules, patient_id=patient_id, file_type=patient_data_type, keys=keys,
                                                                                                                    enumerated_keys=enumerated_keys)
                    connector.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.DE_IDENTIFICATION, message=de_identification_message, parallel=True)
                    if de_identification_failed:
                        patients_count["failed_patients"] += 1
                        patients_count["processed_patients"] += 1
                        connector.update_execution_errors_table(project_name=project_name, patient_id=patient_id, run_uuid=run_uuid, step=Steps.PRE_CHECK_DE_ID, error_level=ErrorLevel.ERROR, parallel=True)
                        continue
                if patient_data_type == FileType.JSON:
                    patient_data = json.dumps(patient_data, indent=4).encode('utf-8')
                connector.database.move_to_next_layer(project=project_name, patient_id=patient_id, object_name=patient_file['object_name'], source_zone=DataPersistenceLayer.LANDING_ZONE, patient_data=patient_data, parallel=True,
                                                      anonymized=anonymized)
                print(f"{{'chunk': '{i_chunk}', 'patient': '{patient_id}'}}: End of processing, data moved to refined zone")
                del patient_data, keys, enumerated_keys
        else:
            connector.database.move_to_next_layer(project=project_name, patient_id=patient_id, object_name=patient_file['object_name'], source_zone=DataPersistenceLayer.LANDING_ZONE, copy_only=True, parallel=True)
        message = f"Landing file '{os.path.basename(patient_file['object_name'])}' copied to refined zone"
        connector.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.COPY, message=message, parallel=True)
        patients_count["processed_patients"] += 1
    if anonymized:
        connector.database.cleanup_anonymized_patient_records(project=project_name)
    connector.database.close_connection()
    del connector, pre_checks, de_id_rules
    print(f"End of chunk {i_chunk} processing")
    return patients_count


def execute_chunk_integration(i_chunk: int, chunk: dict, project_name: str, run_uuid: str, max_java_heap_space: int) -> Union[dict, str]:
    """Execute integration over patient chunks

    Args:
        i_chunk (int): chunk number
        chunk (dict): chunk data
        project_name (str): name of the project
        step (Steps): name of the step
        run_uuid (str): UUID of the run
        max_java_heap_space (int): maximum Java heap space
    
    Returns:
        dict, str: patients count, chunk folder
    """
    chunk_connector = Connector(project=project_name)
    chunk_connector.database.create_connection(auto_commit=False)
    patients_count = {"processed_patients": 0, "failed_patients": 0}
    rdf_folder, json_folder, output_folder, config_file = chunk_connector.setup_conversion_chunk_environment(project_name=project_name, i_chunk=i_chunk)
    chunk_connector.download_refined_data(project_name=project_name, chunk=chunk, json_folder=json_folder, rdf_folder=rdf_folder)
    chunk_connector.execute_rdf_integration(project_name=project_name, rdf_folder=rdf_folder, patients_count=patients_count)
    chunk_connector.execute_json_integration(project_name=project_name, json_folder=json_folder, output_folder=output_folder, config_file=config_file, patients_count=patients_count, run_uuid=run_uuid, max_java_heap_space=max_java_heap_space)
    chunk_connector.database.conn.commit()
    chunk_connector.database.close_connection()
    return patients_count, os.path.dirname(json_folder)


def execute_validation(i_chunk: int, 
                       chunk: dict, 
                       project_name: str, 
                       validation_log_level: LogLevel, 
                       run_uuid: str, 
                       max_java_heap_space: int, 
                       sphn_base_iri: str, 
                       project_output_format: OutputFormat,
                       compression: bool, 
                       einstein: bool, 
                       severity_level: QCSeverityLevel) -> dict:
    """Execute validation in parallel over patient chunks

    Args:
        i_chunk (int): chunk number
        chunk (list): chunk of patient data
        project_name (str): name of the project
        validation_log_level (LogLevel): validation logging level
        run_uuid (str): UUID of the run
        max_java_heap_space (int): maximum Java heap space
        sphn_base_iri (str): SPHN schema base IRI
        project_output_format (OutputFormat): project output format
        compression (str): project files should be compressed
        einstein (bool): data sharing with Einstein activated
        severity_level (QCSeverityLevel): QC severity level
    
    Returns:
        dict: patients count
    """
    chunk_connector = Connector(project=project_name)
    chunk_connector.database.create_connection(auto_commit=False)
    patients_count = {'processed_patients': 0, 'failed_patients': 0, 'patients_with_warnings': 0}
    bucket = chunk_connector.config.s3_connector_bucket
    step = Steps.VALIDATION
    test_folder, properties_file, logs_folder, _, _ = chunk_connector.setup_chunk_environment(project_name=project_name, i_chunk=i_chunk, validation_log_level=validation_log_level, 
                                                                                              step=step, severity_level=severity_level)
    is_custom_query = os.path.exists(os.path.join("/home/sphn/quality", project_name, "report-query", "report_sparql_query.rq"))
    for patient_id, patient_file in chunk.items():
        filepath = os.path.join(test_folder, f"{patient_id}.ttl")
        chunk_connector.config.s3_client.download_file(Bucket=bucket, Key=patient_file, Filename=filepath)
        chunk_connector.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.LOADING, parallel=True)

    print(f"Start -- QC tool for chunk {i_chunk} triggered")
    exc_timestamp = datetime.now()
    cmd = ["java", f"-Xmx{max_java_heap_space}g", "-jar", "/home/sphn/quality/target/sphn-quality-check-tool-v2.3-full.jar", "-properties", properties_file]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = p.communicate()
    print(f"End -- QC tool for chunk {i_chunk} took {str(timedelta(seconds=(datetime.now() - exc_timestamp).seconds))}")

    if p.returncode != 0:
        error_message = f'QAF execution for project "{project_name}" failed. Potential issues when loading project files.\n'
        if output:
            error_message += "Output:\n" + output.decode()
        if error:
            error_message += "\nError\n" + error.decode()
        for patient_id in chunk.keys():
            chunk_connector.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.VALIDATION_RESULT, message=error_message, validation_ok=False, parallel=True)
            patients_count['processed_patients'] += 1
            patients_count['failed_patients'] += 1
    else:
        einstein_data = {'data_provider_id': chunk_connector.config.data_provider_id, 'patients': {}}
        print(f"Start -- Upload data for chunk {i_chunk} triggered")
        exc_timestamp = datetime.now()
        for validation_result in ValidationResult._get_validation_results(logs_folder=logs_folder):
            chunk_connector.update_logging(project_name=project_name, patient_id=validation_result.patient_id, step=step, dag_run_status=DagRun.VALIDATION_RESULT, report=validation_result.report, validation_ok=validation_result.status, parallel=True)
            if not is_custom_query:
                chunk_connector.update_quality_checks(project_name=project_name, patient_id=validation_result.patient_id, report=validation_result.report, validation_ok=validation_result.status)
            chunk_connector.database.move_to_next_layer(project=project_name, patient_id=validation_result.patient_id, object_name=chunk[validation_result.patient_id], source_zone=DataPersistenceLayer.GRAPH_ZONE, validated_ok=validation_result.status, 
                                                        parallel=True, sphn_base_iri=sphn_base_iri, project_output_format=project_output_format, compression=compression, einstein_data=einstein_data, einstein=einstein)
            patients_count['processed_patients'] += 1
            if not validation_result.status:
                patients_count['failed_patients'] += 1
                chunk_connector.update_execution_errors_table(project_name=project_name, patient_id=validation_result.patient_id, run_uuid=run_uuid, step=step, error_level=ErrorLevel.ERROR, parallel=True)
        if einstein and einstein_data['patients']:
            chunk_connector.database.store_einstein_data(patient_data=einstein_data, project_name=project_name)
        print(f"End -- Upload data for chunk {i_chunk} took {str(timedelta(seconds=(datetime.now() - exc_timestamp).seconds))}")
    chunk_connector.cleanup_chunk_environment(test_folder=test_folder, logs_folder=logs_folder, properties_file=properties_file, step=step)
    chunk_connector.database.conn.commit()
    chunk_connector.database.close_connection()
    return patients_count, logs_folder


def execute_statistics(i_chunk: int, 
                       chunk: dict, 
                       project_name: str, 
                       run_uuid: str, 
                       profile: StatisticsProfile, 
                       max_java_heap_space: int) -> Union[dict, str]:
    """Execute statistics in parallel over patient chunks

    Args:
        i_chunk (int): chunk number
        chunk (list): chunk of patient data
        project_name (str): name of the project
        run_uuid (str): UUID of the run
        profile (StatisticsProfile): statistics profile
        max_java_heap_space (int): maximum Java heap space
    
    Returns:
        dict, str: patients count, SPARQLer output folder
    """
    chunk_connector = Connector(project=project_name)
    chunk_connector.database.create_connection(auto_commit=False)
    patients_count = {'processed_patients': 0, 'failed_patients': 0}
    bucket = chunk_connector.config.s3_connector_bucket
    step = Steps.STATISTICS
    test_folder, properties_file, logs_folder, sparqler_folder, qc_queries_out_folder = chunk_connector.setup_chunk_environment(project_name=project_name, i_chunk=i_chunk, validation_log_level=LogLevel.COMPACT, step=step)

    for patient_id, patient_file in chunk.items():
        filepath = os.path.join(test_folder, f"{patient_id}.ttl")
        chunk_connector.config.s3_client.download_file(Bucket=bucket, Key=patient_file, Filename=filepath)
        chunk_connector.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.LOADING, parallel=True)

    cmd = ["java", f"-Xmx{max_java_heap_space}g", "-jar", "/home/sphn/quality/target/sphn-quality-check-tool-v2.3-full.jar", "-properties", properties_file, "-statistics"]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = p.communicate()
    if p.returncode != 0:
        error_message = f'Statistics execution for project "{project_name}" failed. Potential issues when loading project files.\n'
        if output:
            error_message += "Output:\n" + output.decode()
        if error:
            error_message += "\nError\n" + error.decode()
        for patient_id in chunk.keys():
            chunk_connector.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.STATS_FAILED, message=error_message, parallel=True)
            patients_count['processed_patients'] += 1
            patients_count['failed_patients'] += 1
            chunk_connector.update_execution_errors_table(project_name=project_name, patient_id=patient_id, run_uuid=run_uuid, step=Steps.STATISTICS, error_level=ErrorLevel.ERROR, parallel=True)
    else:
        for patient_id, _ in chunk.items():
            try:
                chunk_connector.update_patient_stats(project_name=project_name, patient_id=patient_id, sparqler_folder=sparqler_folder, qc_queries_out_folder=qc_queries_out_folder, profile=profile)
                chunk_connector.update_logging(project_name=project_name, patient_id=patient_id, step=step, dag_run_status=DagRun.STATS_SUCCESSFUL, parallel=True)
                patients_count['processed_patients'] += 1
            except Exception as exc:
                import traceback
                traceback.print_exc()
                chunk_connector.update_logging(project_name=project_name, patient_id=patient_id, step=step, message=str(exc), dag_run_status=DagRun.STATS_FAILED, parallel=True)
                patients_count['processed_patients'] += 1
                patients_count['failed_patients'] += 1
                chunk_connector.update_execution_errors_table(project_name=project_name, patient_id=patient_id, run_uuid=run_uuid, step=Steps.STATISTICS, error_level=ErrorLevel.ERROR, parallel=True)
    chunk_connector.cleanup_chunk_environment(test_folder=test_folder, logs_folder=logs_folder, properties_file=properties_file, step=step, sparqler_folder=sparqler_folder, qc_queries_out_folder=qc_queries_out_folder)
    chunk_connector.database.conn.commit()
    chunk_connector.database.close_connection()

    return patients_count, sparqler_folder