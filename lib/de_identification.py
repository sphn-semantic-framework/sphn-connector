#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'de_identification.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

import json
import re
import random
import traceback
from datetime import timedelta, datetime
from typing import Union
from dateutil import parser
import sys
import os
import uuid

sys.path.append(os.path.abspath(os.path.dirname(__file__)))
from enums import DateShiftFormat, DeIdentificationRuleType, DeIdentificationStatus
from config import Config


class DeIdentificationRule(object):
    """
    Super class for De-Identification rules
    """
    def __init__(self, project: str, config: Config, rule_name: str, database: object):
        """Class constructor

        Args:
            project (str): name of the project
            config (Config): config object
            rule_name (str): name of the rule
            database (Database): database object
        """
        self.project = project
        self.config = config
        self.rule_name = rule_name
        self.database = database
    
    def get_format_mapping(self) -> dict:
        """Gets key paths of columns with specific format

        Returns:
            dict: formats map
        """
        response = self.config.s3_client.get_object(Bucket=self.config.s3_connector_bucket, Key=f'{self.project}/De-Identification/{self.project}_formats_mapping.json')
        formats_mapping = json.loads(response["Body"].read().decode('utf8'))
        return formats_mapping

    def update_de_identification_table(self, 
                                       patient_id: str, 
                                       input_field: str, 
                                       old_value: str, 
                                       new_value: str, 
                                       date_shift: int = None, 
                                       substituted: bool = None):
        """Update table de_identification in database

        Args:
            patient_id (str): patient ID
            input_field (str): field to which the rule is applied
            old_value (str): old value of the field
            new_value (str): new value of the field
            date_shift (int): number of days shifted
            substituted (bool): match found in substitution list or with regex expression
        """
        sql = f"INSERT INTO de_identification (project_name, data_provider_id, patient_id, input_field, rule, rule_name, date_shift, substituted, old_value, new_value, timestmp) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) \
                ON CONFLICT (project_name, data_provider_id, patient_id, input_field, rule, rule_name, timestmp) DO UPDATE SET date_shift=EXCLUDED.date_shift, old_value=EXCLUDED.old_value, substituted=EXCLUDED.substituted, new_value=EXCLUDED.new_value"
        self.database.cursor.execute(sql, (self.project, self.config.data_provider_id, patient_id, input_field, self.type.value, self.rule_name, date_shift, substituted, old_value, new_value, datetime.now()))


    def _extract_paths_with_value(value: str, data: dict, current_path: str = '', result: list = None) -> list:
        """Extract paths with given value

        Args:
            value (str): defined value
            data (dict): data map
            current_path (str, optional): currently processed path. Defaults to ''.
            result (list, optional): list of results. Defaults to None.

        Returns:
            list: list of results
        """
        if result is None:
            result = []

        if isinstance(data, dict):
            for key, item in data.items():
                next_path = current_path + '/' + key if current_path else key
                if item == value:
                    result.append(next_path)
                if isinstance(item, (dict, list)):
                    DeIdentificationRule._extract_paths_with_value(value, item, next_path, result)
        elif isinstance(data, list):
            for index, item in enumerate(data):
                next_path = current_path + '/' + str(index) if current_path else str(index)
                if item == value:
                    result.append(next_path)
                if isinstance(item, (dict, list)):
                    DeIdentificationRule._extract_paths_with_value(value, item, next_path, result)
        
        return result

    @staticmethod
    def _transverse_data(key_path: str, data: dict) -> Union[dict, str]:
        """Transverse data object

        Args:
            key_path (str): key path
            data (dict): data object

        Returns:
            Union[dict, str]: map of the last key, last key name
        """
        current = data
        extracted_keys = key_path.split('/')
        for key in extracted_keys[:-1]:
            if isinstance(current, list):
                key = int(key)
            current = current[key]
        if isinstance(current, list):
            last_key = int(extracted_keys[-1])
        else:
            last_key = extracted_keys[-1]
        return current, last_key

class DeIdentificationResult(object):
    """
    Class defining Pre-Check Result object
    """
    def __init__(self, status: DeIdentificationStatus, rule: DeIdentificationRule, message: str = None):
        """Class constructor

        Args:
            status (DeIdentificationStatus): status of the exectution
            rule (DeIdentificationRule): de-identification rule
            message (str, optional): exit message. Defaults to None.
        """
        self.status = status
        if self.status == DeIdentificationStatus.SUCCESS:
            self.message = f"De-Identification rule '{rule.rule_name}' of type '{rule.type.value}' EXECUTED"
        else:
            self.message = f"De-Identification rule '{rule.rule_name}' of type '{rule.type.value}' FAILED': {message}"


class ScrambleField(DeIdentificationRule):
    """
    Class defining de-identification rule of type 'scrambleField'

    """
    def __init__(self, applies_to_fields: list, anonymize: bool, i_chunk: int, **kwargs):
        """Class constructor

        Args:
            applies_to_fields (list): list of fields to which the de-identification is applied
            anonymize (bool): anonymize data (do not keep track of processed patients)
            i_chunk (int): chunk number
        """
        super().__init__(**kwargs)
        self.type = DeIdentificationRuleType.SCRAMBLE_FIELD
        self.applies_to_fields = applies_to_fields
        self.anonymize = anonymize
        self.scrambling_map = None
        self.rnd = random.Random()
        self.rnd.seed(str(uuid.uuid4()) + str(i_chunk))
        
    def execute(self, patient_data: dict, patient_id: str, keys: list, enumerated_keys: list) -> DeIdentificationResult:
        """Execute de-identification

        Args:
            patient_data (dict): patient data
            patient_id (str): patient ID
            keys (list): list of data keys
            enumerated_keys (list): list of enumerated data keys

        Returns:
            DeIdentificationResult: De-Identification execution result
        """
        try:
            self.set_scrambling_map(patient_id=patient_id)
            filtered_keys = []
            for field in self.applies_to_fields:
                filtered_keys += [key.rstrip('/') for key in enumerated_keys if re.sub(r"(\/[0-9]*\/)", "/", key).rstrip('/').endswith(field)]
            self.apply_scrambling(data=patient_data, key_paths=filtered_keys, patient_id=patient_id)
            return DeIdentificationResult(status=DeIdentificationStatus.SUCCESS, rule=self)
        except Exception:
            return DeIdentificationResult(status=DeIdentificationStatus.FAILED, rule=self, message=traceback.format_exc())

    def set_scrambling_map(self, patient_id: str) -> dict:
        """Extract existing scrambled values for all patients

        Args:
            patient_id (str): patient ID

        Returns:
            dict: map of scrambled values for all patients
        """
        scrambling_map = {}
        if not self.anonymize:
            sql = "SELECT key_path, new_value FROM de_identification_scrambling WHERE project_name=%s AND data_provider_id=%s AND patient_id=%s"
            self.database.cursor.execute(sql, (self.project, self.config.data_provider_id, patient_id))
            records = self.database.cursor.fetchall()
            for record in records:
                scrambling_map[record[0]]= record[1]
        self.scrambling_map = scrambling_map

    def update_scrambling_table(self, patient_id: str, key_path: str, new_value: str):
        """Update table 'de_identification_scrambling'

        Args:
            patient_id (str): patient ID
            key_path (str): key path to update
            new_value (str): new generated value
        """
        sql = "INSERT INTO de_identification_scrambling (project_name, data_provider_id, patient_id, key_path, new_value, anonymized) VALUES (%s, %s, %s, %s, %s, %s) \
               ON CONFLICT (project_name, data_provider_id, patient_id, key_path) DO UPDATE SET new_value=EXCLUDED.new_value, anonymized=EXCLUDED.anonymized"
        self.database.cursor.execute(sql, (self.project, self.config.data_provider_id, patient_id, key_path, new_value, self.anonymize))

    def apply_scrambling(self, data: dict, key_paths: list, patient_id: str):
        """Loop through the data and scramble it

        Args:
            data (dict): patient data
            key_paths (list): list of key paths to update
            patient_id (str): patient id
        """
        scrambled_paths = set()
        for key_path in key_paths:
            if key_path not in scrambled_paths:
                current, last_key = DeIdentificationRule._transverse_data(key_path=key_path, data=data)
                current_value = current[last_key]
                if key_path in self.scrambling_map:
                    new_value = self.scrambling_map[key_path]
                else:
                    new_value = str(uuid.UUID(int=self.rnd.getrandbits(128), version=4))
                    self.scrambling_map[key_path] = new_value
                    self.update_scrambling_table(patient_id=patient_id, key_path=key_path, new_value=new_value)
                if not self.anonymize:
                    self.update_de_identification_table(patient_id=patient_id, input_field=key_path, old_value=current_value, new_value=new_value)
                current[last_key] = new_value
                scrambled_paths.add(key_path)
                
                json_paths_for_value = DeIdentificationRule._extract_paths_with_value(value=current_value, data=data)
                for json_key in json_paths_for_value:
                    if json_key not in scrambled_paths:
                        json_current, json_last_key = DeIdentificationRule._transverse_data(key_path=json_key, data=data)
                        if last_key == json_last_key or (last_key == 'id' and json_last_key == 'sourceConceptID') or (isinstance(json_last_key, int) and key_path.split('/')[-2] == json_key.split('/')[-2]):
                            if json_key in self.scrambling_map:
                                json_new_value = self.scrambling_map[json_key]
                            else:
                                json_new_value = new_value
                                self.scrambling_map[json_key] = json_new_value
                                self.update_scrambling_table(patient_id=patient_id, key_path=json_key, new_value=json_new_value)
                            if not self.anonymize:
                                self.update_de_identification_table(patient_id=patient_id, input_field=json_key, old_value=current_value, new_value=json_new_value)
                            json_current[json_last_key] = json_new_value
                            scrambled_paths.add(json_key)
       
class DateShift(DeIdentificationRule):
    """
    Class defining de-identification rule of type 'dateShift'

    """
    def __init__(self, low_range: int, high_range: int, **kwargs) -> None:
        """Class constructor

        Args:
            low_range (int): low date shift range
            high_range (int): high date shift range
        """
        super().__init__(**kwargs)
        self.type = DeIdentificationRuleType.DATE_SHIFT
        self.low_range = low_range
        self.high_range = high_range

    def execute(self, patient_data: dict, patient_id: str, keys: list, enumerated_keys: list) -> DeIdentificationResult:
        """Execute de-identification

        Args:
            patient_data (dict): patient data
            patient_id (str): patient ID
            keys (list): list of data keys
            enumerated_keys (list): list of enumerated data keys

        Returns:
            DeIdentificationResult: De-Identification execution result
        """
        try:
            days_shift = self.get_day_shift(patient_id=patient_id)
            formats_map = self.get_format_mapping()

            formats_list = formats_map['date-time']
            filtered_keys = [key for key in keys if re.sub(r"(\/[0-9]*\/)", "/", key) in formats_list]
            self.apply_dates_shift(data=patient_data, key_paths=filtered_keys, patient_id=patient_id, date_type=DateShiftFormat.DATE_TIME, days_shift=days_shift)

            formats_list = formats_map['date']
            filtered_keys = [key for key in keys if re.sub(r"(\/[0-9]*\/)", "/", key) in formats_list]
            self.apply_dates_shift(data=patient_data, key_paths=filtered_keys, patient_id=patient_id, date_type=DateShiftFormat.DATE, days_shift=days_shift)

            years = [key for key in keys if re.sub(r"(\/[0-9]*\/)", "/", key) in formats_map['gYear']]
            months = [key for key in keys if re.sub(r"(\/[0-9]*\/)", "/", key) in formats_map['gMonth']]
            days = [key for key in keys if re.sub(r"(\/[0-9]*\/)", "/", key) in formats_map['gDay']]
            self.apply_g_types_shift(data=patient_data, patient_id=patient_id, years=years, months=months, days=days, days_shift=days_shift)
            return DeIdentificationResult(status=DeIdentificationStatus.SUCCESS, rule=self)
        except Exception:
            return DeIdentificationResult(status=DeIdentificationStatus.FAILED, rule=self, message=traceback.format_exc())

    def apply_dates_shift(self, data: dict, key_paths: list, patient_id: str, date_type: DateShiftFormat, days_shift: int):
        """Loop through the data and scramble it

        Args:
            data (dict): patient data
            key_paths (list): list of key paths to update
            patient_id (str): patient id
            date_type (DateShiftFormat): type of date, either date-time or date
            days_shift (int): shift in days
        """
        for key_path in key_paths:
            current, last_key = DeIdentificationRule._transverse_data(key_path=key_path, data=data)
            if isinstance(current[last_key], list):
                # Datatype property with maxCardinality != 1 
                for i_key, _ in enumerate(current[last_key]):
                    new_value = self.shift_date(old_value=current[last_key][i_key], date_type=date_type, days_shift=days_shift)
                    self.update_de_identification_table(patient_id=patient_id, input_field=key_path, old_value=current[last_key][i_key], new_value=new_value, date_shift=days_shift)
                    current[last_key][i_key] = new_value
            else:
                new_value = self.shift_date(old_value=current[last_key], date_type=date_type, days_shift=days_shift)
                self.update_de_identification_table(patient_id=patient_id, input_field=key_path, old_value=current[last_key], new_value=new_value, date_shift=days_shift)
                current[last_key] = new_value

    def get_day_shift(self, patient_id: str) -> int:
        """Get day shift depending on previous executions

        Args:
            patient_id (str): patient ID

        Returns:
            int: shift in days
        """
        sql = "SELECT shift FROM de_identification_shifts WHERE project_name=%s AND patient_id=%s AND data_provider_id=%s"
        self.database.cursor.execute(sql, (self.project, patient_id, self.config.data_provider_id))
        shift = self.database.cursor.fetchone()
        if shift is None:
            if self.low_range == self.high_range:
                day_shift = self.low_range
            else:
                day_shift = random.choice([i for i in range(self.low_range, self.high_range) if i != 0])
            sql = "INSERT INTO de_identification_shifts (project_name, data_provider_id, patient_id, shift) VALUES (%s, %s, %s, %s)"
            self.database.cursor.execute(sql, (self.project, self.config.data_provider_id, patient_id, day_shift))
        else:
            day_shift = shift[0]
        return day_shift

    def shift_date(self, old_value: str, date_type: DateShiftFormat, days_shift: Union[int, float]):
        """Shift date value

        Args:
            old_value (str): old value
            date_type (DateShiftFormat): type of date, either date-time or date
            days_shift (Union[int, float]): shift in days
        """
        datetime_value = parser.parse(old_value)
        shift = timedelta(days=days_shift)
        new_value = datetime_value + shift
        if date_type == DateShiftFormat.DATE:
            new_value = new_value.strftime(format="%Y-%m-%d")
        else:
            new_value = new_value.strftime(format="%Y-%m-%dT%H:%M:%S.%f%z")
            try:
                datetime.strptime(new_value, "%Y-%m-%dT%H:%M:%S.%f%z")
                new_value = f"{new_value[:-2]}:{new_value[-2:]}"
            except ValueError:
                pass
        return new_value
    
    def apply_g_types_shift(self, 
                            data: dict, 
                            patient_id: str, 
                            years: list, 
                            months: list, 
                            days: list, 
                            days_shift: int):
        """Apply shift to gTypes

        Args:
            data (dict): patient data
            patient_id (str): patient id
            years (list): gYear keys
            months (list): gMonth keys
            days (list): gDay keys
            days_shift (int): shift in days

        """
        grouped_paths = DateShift._group_by_prefix(years=years, months=months, days=days)
        for key_path, keys in grouped_paths.items():
            current, last_key = DeIdentificationRule._transverse_data(key_path=key_path, data=data)
            if isinstance(current, list):
                last_key = int(last_key)
            current = current[last_key]
            self.update_g_type(current=current, key_path=key_path, year_key=keys['year_key'], month_key=keys['month_key'], day_key=keys['day_key'], patient_id=patient_id, days_shift=days_shift)
    
    def update_g_type(self, 
                      current: dict, 
                      key_path: str, 
                      year_key: str, 
                      month_key: str, 
                      day_key: str, 
                      patient_id: str, 
                      days_shift: int):
        """Update gType values

        Args:
            current (dict): current object
            key_path (str): path to the object
            year_key (str): gYear key
            month_key (str): gMonth key
            day_key (str): gDay key
            patient_id (str): patient id
            days_shift (int): shift in days
        """
        if year_key is not None and month_key is not None and day_key is not None:
            if isinstance(current[year_key], list) or isinstance(current[month_key], list) or isinstance(current[day_key], list):
                print(f"WARNING -- Shifting of array properties '{year_key}', '{month_key}', '{day_key}' on '{key_path}' is not supported")
                return
            search = re.match(r"^[-]?(\d\d\d\d).*$", current[year_key])
            year = search.group(1)
            search = re.match(r"^--(\d\d*).*$", current[month_key])
            month = search.group(1)
            search = re.match(r"^---(\d\d*).*$", current[day_key])
            day = search.group(1)
            try:
                new_value = datetime(int(year), int(month), int(day)) + timedelta(days=days_shift)
            except ValueError as exc:
                msg = f"Exception message: {exc}. Current values on path '{key_path}': {{'{year_key}': {year}, '{month_key}': {month}, '{day_key}': {day}}}"
                raise Exception(msg)
            if new_value.year != int(year):
                new_year = current[year_key].replace(year, str(new_value.year))
                self.update_de_identification_table(patient_id=patient_id, input_field=f"{key_path}/{year_key}", old_value=current[year_key], new_value=new_year, date_shift=days_shift)
                current[year_key] = new_year
            if new_value.month != int(month):
                new_month = current[month_key].replace(f"--{month}", f"--{str(new_value.month).zfill(2)}")
                self.update_de_identification_table(patient_id=patient_id, input_field=f"{key_path}/{month_key}", old_value=current[month_key], new_value=new_month, date_shift=days_shift)
                current[month_key] = new_month
            if new_value.day != int(day):
                new_day = current[day_key].replace(f"---{day}", f"---{str(new_value.day).zfill(2)}")
                self.update_de_identification_table(patient_id=patient_id, input_field=f"{key_path}/{day_key}", old_value=current[day_key], new_value=new_day, date_shift=days_shift)
                current[day_key] = new_day

    @staticmethod
    def _group_by_prefix(years: list, months: list, days: list) -> dict:
        """Group gTypes by prefix

        Args:
            years (list): gYear keys
            months (list): gMonth keys
            days (list): gDay keys

        Returns:
            dict: map of prefixes and gTypes keys
        """
        grouped_paths = {}
        for year in years:
            year_prefix, year_key = year.rsplit("/", 1)
            g_date = {'year_key': year_key, 'month_key': None, 'day_key': None, 'time_key': None}
            for month in months:
                month_prefix, month_key = month.rsplit("/", 1)
                if month_prefix == year_prefix:
                    g_date['month_key'] = month_key
                    break
            for day in days:
                day_prefix, day_key = day.rsplit("/", 1)
                if day_prefix == year_prefix:
                    g_date['day_key'] = day_key
                    break
            grouped_paths[year_prefix] = g_date
        return grouped_paths

class SubstituteFieldList(DeIdentificationRule):
    """
    Class defining de-identification rule of type 'substituteFieldList'
    """
    def __init__(self, applies_to_field: str, substitution_list: list, replacement: str, **kwargs):
        """Class constructor

        Args:
            applies_to_field (str): list of field suffixed
            substitution_list (list): list of strings to replace
            replacement (str): replacement string
        """
        super().__init__(**kwargs)
        self.type = DeIdentificationRuleType.SUBSTITUTE_FIELD_LIST
        self.applies_to_field = applies_to_field
        self.substitution_list = substitution_list
        self.replacement = replacement

    def execute(self, patient_data: dict, patient_id: str, keys: list, enumerated_keys: list) -> DeIdentificationResult:
        """Execute de-identification

        Args:
            patient_data (dict): patient data
            patient_id (str): patient ID
            keys (list): list of data keys
            enumerated_keys (list): list of enumerated data keys

        Returns:
            DeIdentificationResult: De-Identification execution result
        """
        try:
            filtered_keys = [key.rstrip('/') for key in enumerated_keys if re.sub(r"(\/[0-9]*\/)", "/", key).rstrip('/').endswith(self.applies_to_field)]
            self.substitute_strings(data=patient_data, key_paths=filtered_keys, patient_id=patient_id)
            return DeIdentificationResult(status=DeIdentificationStatus.SUCCESS, rule=self)
        except Exception:
            return DeIdentificationResult(status=DeIdentificationStatus.FAILED, rule=self, message=traceback.format_exc())

    def substitute_strings(self, data: dict, key_paths: str, patient_id: str):
        """Substitute strings in substitution list

        Args:
            data (dict): patient data
            key_paths (str): list of key paths to check
            patient_id (str): patient id
        """
        for key_path in key_paths:
            current, last_key = DeIdentificationRule._transverse_data(key_path=key_path, data=data)
            if current[last_key] in self.substitution_list:
                self.update_de_identification_table(patient_id=patient_id, input_field=key_path, old_value=current[last_key], new_value=self.replacement, substituted=True)
                current[last_key] = self.replacement
            else:
                self.update_de_identification_table(patient_id=patient_id, input_field=key_path, old_value=current[last_key], new_value=current[last_key], substituted=False)

class SubstituteFieldRegex(DeIdentificationRule):
    """
    Class defining de-identification rule of type 'substituteFieldRegex'
    """
    def __init__(self, applies_to_field: str, regex: str, replacement: str, **kwargs):
        """Class constructor

        Args:
            applies_to_field (str): list of field suffixed
            regex (str): regex expression
            replacement (str): replacement string
        """
        super().__init__(**kwargs)
        self.type = DeIdentificationRuleType.SUBSTITUTE_FIELD_LIST
        self.applies_to_field = applies_to_field
        self.regex = regex
        self.replacement = replacement

    def execute(self, patient_data: dict, patient_id: str, keys: list, enumerated_keys: list) -> DeIdentificationResult:
        """Execute de-identification

        Args:
            patient_data (dict): patient data
            patient_id (str): patient ID
            keys (list): list of data keys
            enumerated_keys (list): list of enumerated data keys

        Returns:
            DeIdentificationResult: De-Identification execution result
        """
        try:
            filtered_keys = [key.rstrip('/') for key in enumerated_keys if re.sub(r"(\/[0-9]*\/)", "/", key).rstrip('/').endswith(self.applies_to_field)]
            self.substitute_regex(data=patient_data, key_paths=filtered_keys, patient_id=patient_id)
            return DeIdentificationResult(status=DeIdentificationStatus.SUCCESS, rule=self)
        except Exception:
            return DeIdentificationResult(status=DeIdentificationStatus.FAILED, rule=self, message=traceback.format_exc())

    def substitute_regex(self, data: dict, key_paths: str, patient_id: str):
        """Substitute strings matching regex expression

        Args:
            data (dict): patient data
            key_paths (str): list of key paths to check
            patient_id (str): patient id
        """
        for key_path in key_paths:
            current, last_key = DeIdentificationRule._transverse_data(key_path=key_path, data=data)
            if bool(re.match(rf"^{self.regex}$", current[last_key])):
                self.update_de_identification_table(patient_id=patient_id, input_field=key_path, old_value=current[last_key], new_value=self.replacement, substituted=True)
                current[last_key] = self.replacement
            else:
                self.update_de_identification_table(patient_id=patient_id, input_field=key_path, old_value=current[last_key], new_value=current[last_key], substituted=False)

def get_applies_to_field(field: str) -> str:
    """Returns applied_to_field with slash added if needed

    Args:
        field (str): applies_to_field defined in configuration file

    Returns:
        str: updated field
    """
    if field is None:
        return field
    elif field.startswith('/') or field.startswith('content/') or field.startswith('supporting_concepts/') or field.startswith('sphn:DataRelease/') or field.startswith('sphn:DataProviderInstitute/') or field.startswith('sphn:DataProvider/') or field.startswith('sphn:SubjectPseudoIdentifier/'):
        return field
    elif field.startswith('/content/') or field.startswith('/supporting_concepts/') or field.startswith('/sphn:DataRelease/') or field.startswith('/sphn:DataProviderInstitute/') or field.startswith('/sphn:DataProvider/') or field.startswith('/sphn:SubjectPseudoIdentifier/'):
        return field[1:]
    else:
        return "/" + field

def create_de_identification_rules(project: str, config: Config, database: object, i_chunk: int) -> Union[list, bool]:
    """Reads the configuration file for De-Identification and creates the DeIdentificationRule objects accordingly

    Args:
        project (str): name of the project
        config (Config): configuration object
        database (Database): database object
        i_chunk (int): chunk number

    Returns:
            Union[list, bool]: list of DeIdentificationRule objects, anonymized scrambling rule
    """
    anonymized = False
    try:
        de_identification_config = config.s3_client.get_object(Bucket=database.config.s3_connector_bucket, Key=f'{project}/De-Identification/{project}_de_identification_config.json')
        de_identification_json = json.loads(de_identification_config["Body"].read())
    except Exception:
        print("No De-Identification configuration file uploaded")
        return [], anonymized
    de_id_rules = []
    for rule_type, rules in de_identification_json.items():
        rule_type = DeIdentificationRuleType(rule_type)
        for rule_name, rule in rules.items():
            if rule_type == DeIdentificationRuleType.SCRAMBLE_FIELD:
                de_id_rules.append(ScrambleField(project=project, 
                                                 config=config,
                                                 rule_name=rule_name, 
                                                 database=database,
                                                 applies_to_fields=[get_applies_to_field(field=field) for field in rule.get('applies_to_fields')],
                                                 anonymize=rule.get('anonymize', False),
                                                 i_chunk=i_chunk)
                                                 )
                anonymized = rule.get('anonymize', False)
            elif rule_type == DeIdentificationRuleType.DATE_SHIFT:
                de_id_rules.append(DateShift(project=project, 
                                             config=config, 
                                             rule_name=rule_name, 
                                             database=database,
                                             low_range=rule.get('low_range', -30),
                                             high_range=rule.get('high_range', 30))
                                             )
            elif rule_type == DeIdentificationRuleType.SUBSTITUTE_FIELD_LIST:
                de_id_rules.append(SubstituteFieldList(project=project, 
                                                       config=config, 
                                                       rule_name=rule_name, 
                                                       database=database, 
                                                       applies_to_field=get_applies_to_field(field=rule.get('applies_to_field')),
                                                       substitution_list=rule.get('substitution_list'),
                                                       replacement=rule.get('replacement'))
                                                       )
            elif rule_type == DeIdentificationRuleType.SUBSTITUTE_FIELD_REGEX:
                de_id_rules.append(SubstituteFieldRegex(project=project, 
                                                        config=config, 
                                                        rule_name=rule_name, 
                                                        database=database, 
                                                        applies_to_field=get_applies_to_field(field=rule.get('applies_to_field')),
                                                        regex=rule.get('regex'),
                                                        replacement=rule.get('replacement'))
                                                        )
    return de_id_rules, anonymized


def validate_configuration_file(content: bytes) -> str:
    """Validates De-Identification configuration file

    Args:
        content (bytes): de-identification configuration file content

    Returns:
        str: error messages
    """
    de_id_json = json.loads(content)
    pre_conditions_list = []
    for rule_type, rules in de_id_json.items():
        if rule_type not in [type.value for type in DeIdentificationRuleType]:
            pre_conditions_list.append(f"Defined type '{rule_type}' is not a valid De-Identification rule type. Allowed values: [{', '.join([type.value for type in DeIdentificationRuleType])}]")
            continue
        rule_type = DeIdentificationRuleType(rule_type)
        if rule_type == DeIdentificationRuleType.DATE_SHIFT and len(rules) > 1:
            pre_conditions_list.append("Only one rule can be defined for De-identification rule of type 'dateShift'")
            continue
        for rule_name, rule in rules.items():
            if rule_type == DeIdentificationRuleType.SCRAMBLE_FIELD:
                applies_to_fields = rule.get('applies_to_fields')
                anonymize = rule.get('anonymize', False)
                if applies_to_fields is None:
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: 'applies_to_fields' field is mandatory")
                elif not isinstance(applies_to_fields, list):
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: '{str(applies_to_fields)}' is not a valid entry for 'applies_to_fields' field. Entry must be of type list")
                elif not all(isinstance(item, str) for item in applies_to_fields):
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: '{str(applies_to_fields)}' is not a valid entry for 'applies_to_fields' field. Entry must be a list of strings")
                if not isinstance(anonymize, bool):
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: '{str(anonymize)}' is not a valid entry for 'anonymize' field. Entry must be of type boolean")
            elif rule_type == DeIdentificationRuleType.DATE_SHIFT:
                low_range = rule.get('low_range', -30)
                high_range = rule.get('high_range', 30)
                if not isinstance(low_range, int):
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: '{str(low_range)}' is not a valid entry for 'low_range' field. Entry must be of type integer")
                if not isinstance(high_range, int):
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: '{str(high_range)}' is not a valid entry for 'high_range' field. Entry must be of type integer")
                if isinstance(low_range, int) and isinstance(high_range, int) and low_range > high_range:
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: value of 'low_range' field must be smaller than value of 'high_range' field")
            elif rule_type == DeIdentificationRuleType.SUBSTITUTE_FIELD_LIST:
                applies_to_field = rule.get('applies_to_field')
                substitution_list = rule.get('substitution_list')
                replacement = rule.get('replacement')
                if applies_to_field is None:
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: 'applies_to_field' field is mandatory")
                elif not isinstance(applies_to_field, str):
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: '{str(applies_to_field)}' is not a valid entry for 'applies_to_field' field. Entry must be of type string")
                if substitution_list is None:
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: 'substitution_list' field is mandatory")
                elif not isinstance(substitution_list, list):
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: '{str(substitution_list)}' is not a valid entry for 'substitution_list' field. Entry must be of type list")
                elif not all(isinstance(item, str) for item in substitution_list):
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: '{str(substitution_list)}' is not a valid entry for 'substitution_list' field. Entry must be a list of strings")
                if replacement is None:
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: 'replacement' field is mandatory")
                elif not isinstance(replacement, str):
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: '{str(replacement)}' is not a valid entry for 'replacement' field. Entry must be of type string")
            elif rule_type == DeIdentificationRuleType.SUBSTITUTE_FIELD_REGEX:
                applies_to_field = rule.get('applies_to_field')
                regex = rule.get('regex')
                replacement = rule.get('replacement')
                if applies_to_field is None:
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: 'applies_to_field' field is mandatory")
                elif not isinstance(applies_to_field, str):
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: '{str(applies_to_field)}' is not a valid entry for 'applies_to_field' field. Entry must be of type string")
                if regex is None:
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: 'regex' field is mandatory")
                elif not isinstance(regex, str):
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: '{str(regex)}' is not a valid entry for 'regex' field. Entry must be of type string")
                if replacement is None:
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: 'replacement' field is mandatory")
                elif not isinstance(replacement, str):
                    pre_conditions_list.append(f"Rule '{rule_name}' is misconfigured: '{str(replacement)}' is not a valid entry for 'replacement' field. Entry must be of type string")
    if pre_conditions_list:
        return ', '.join(pre_conditions_list)