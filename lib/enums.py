#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'enums.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from enum import Enum
class FileClass(Enum):
    """
    Enum class defining the class of the file (i.e. what the file represents)
    """
    PATIENT_DATA = "Patient Data"
    SCHEMA = "Schema"
    EXT_SCHEMA = "External Schema"
    EXT_TERMINOLOGY = "External Terminology"
    EXT_SHACL = "External SHACL"
    EXT_QUERIES = "External Queries"
    EXCEPTIONS = "Exception File"
    RML = "RML"
    PRE_CHECK_CONFIG = "Pre-Check Config"
    DE_IDENTIFICATION_CONFIG = "De-Identification Config"
    REPORT_SPARQL_QUERY = "Report SPARQL Query"


class FileType(Enum):
    """
    Enum class defining the file type
    """
    RDF = "Rdf"
    JSON = "Json"
    ZIP = "Zip"
    CSV = "csv"


class PreCondition(Enum):
    """
    Enum class defining on what the pre-condition is applied
    """
    PROJECT_CREATION = "Project Creation"
    INGESTION = "Ingestion"
    EXT_SCHEMA_UPLOAD = "External Schema Upload"
    OTHER = "Other"


class Steps(Enum):
    """
    Enum class defining SPHN Connector pipeline steps
    """
    ALL = "All"
    PRE_CHECK_DE_ID = "Pre-check/De-ID"
    INTEGRATION = "Integration"
    VALIDATION = "Validation"
    STATISTICS = "Statistics"
    NOTIFY_EINSTEIN = "Notify-Einstein"
    INGEST_FROM_DATABASE = "Ingest from Database"

class DagRun(Enum):
    """
    Enum class defining SPHN Connector patient id file status
    """
    BACKLOG = "Backlog"
    LOADING =  "Loading"
    VALIDATION_RESULT = "Validation Result"
    JSON_FILES_LOOKUP = "JSON Files Lookup"
    JSON_INTEGRATION = "JSON Integration"
    RDF_INTEGRATION = "RDF Integration"
    COPY = "Copy"
    PRE_CHECK = "Pre-Check"
    DE_IDENTIFICATION = "De-Identification"
    WARNING_IMPORTS = "WARNING Imports"
    TDB2_BUILD_START = "Start TDB2 Build"
    TDB2_BUILD_END = "End TDB2 Build"
    STANDARD_PATIENT_STATS = "Standardized Patient Statistics"
    INDIVIDUAL_PATIENT_STATS = "Individual Patient Statistics"
    STATS_FAILED = "Statistics Failed"
    STATS_SUCCESSFUL = "Statistics Successful"
    STATS_UPDATE_FAILURE = "Statistics Update Failure"
    PARALLELIZATION = "Parallelization"
    CONCURRENCY = "Concurrency"
    DATA_SHARING = "Data Sharing"
    DATA_SHARING_RETRY = "Data Sharing Re-Try"

class Shacler(Enum):
    """
    Enum class defining SHACL status
    """
    EXT_SHACL_FILE = "External SHACL provided"
    NO_EXT = "No external Schema"
    STARTED = "Shacler started"
    FAILED = "Shacler failed"
    DONE = "Shacler finished"
    DONE_ERROR = "Shacler finished with errors"
    NO_SHACL = "Shacler will start"
    NO_EXCEPT = "No Exception File"
    SCHEMA_FILE = "Schema File"
    EXT_SCHEMA_FILE = "External Schema File"
    SHACL = "Shacl file"
    EXCEPTION = "Exception File"
    REINIT = "Created SHACL File detected"
    SHAPES = "Node shapes generated"

class PipelineAction(Enum):
    """
    Enum class defining pipeline actions
    """
    TRIGGERED = "triggered"
    STOPPED = "stopped"
    

class LogLevel(Enum):
    """
    Enum class defining validation logging level
    """
    COMPACT = "Compact"
    VERBOSE = "Verbose"


class PipelineStep(Enum):
    """
    Enum class defining pipeline step status
    """
    RUNNING = "RUNNING"
    SKIPPED = "SKIPPED"
    SUCCESS = "SUCCESS"
    FAILED = "FAILED"


class CompressionType(Enum):
    """
    Enum class defining compression type
    """
    ZIP = ".zip"
    TAR = ".tar.gz"

class StatisticsOutputType(Enum):
    """
    Enum class defining statistics report output file type
    """
    ZIP = ".zip"
    TAR = ".tar.gz"
    EXCEL = ".xlsx"

class RML_JSON_LOGS(Enum):
    """
    Enum class for the RML JSON logs
    """
    START = "Execution started"
    FAILED = "Execution failed"
    ENDED = "Execution finished"
    SCHEMA_FILE = "Loading Schema"
    GRAPH_SIMPLE = "Simplification"
    EXPORT_SIMPLE = "Export simplified Schema"
    EXPORT_GRAPH = "Export RML"
    LOAD_SIMPLE = "Loading simplified Schema"
    RMLGenerator = "RML Generation"
    RMLGENERATOR_EXPORT = "Export RML"
    JSON_SCHEMA_GENERATOR =  "JSON Generation"
    JSON_SCHEMA_GENERATOR_Export = "Export JSON"
    JSON_DATATYPE_WARNING = "JSON Datatype Warning"
    RML_DATATYPE_WARNING = "RML Datatype Warning"


class DataPersistenceLayer(Enum):
    """
    Enum class for Data Persistence Layer
    """
    LANDING_ZONE = "landing_zone"
    REFINED_ZONE = "refined_zone"
    GRAPH_ZONE = "graph_zone"
    RELEASE_ZONE = "release_zone"


class UserType(Enum):
    """
    Enum class defining different types of users
    """
    ADMIN = "Admin"
    STANDARD = "Standard"
    INGESTION = "Ingestion"


class IngestionType(Enum):
    """
    Enum class defining ingestion types
    """
    RDF = "RDF"
    JSON = "JSON"
    DATABASE = "DATABASE"

class PreCheckLevel(Enum):
    """
    Enum class defining check level
    """
    FAIL = "FAIL"
    WARN = "WARN"

class PreCheckType(Enum):
    """
    Enum class defining pre-checks types
    """
    REGEX_CHECK = "regexCheck"
    VALIDATION_CHECK = "validationCheck"
    REPLACE_CHARS_CHECK = "replaceCharsCheck"
    REPLACE_REGEX_CHECK = "replaceRegexCheck"
    DATA_TYPE_CHECK = "dataTypeCheck"
    IRI_VALIDATION_CHECK = "IRIValidationCheck"
    RDF_VALIDATION_CHECK = "rdfValidationCheck"
    JSON_PRE_PROCESSING = "jsonPreProcessing"
    REPLACE_UNVERSIONED_CODES = "replaceUnversionedCodes"


class PreCheckStatus(Enum):
    """
    Enum class defining pre-checks status
    """
    SUCCESS = "Success"
    FAILED = "Failed"

class DeIdentificationRuleType(Enum):
    """
    Enum class defining de-identification rule types
    """
    SCRAMBLE_FIELD = 'scrambleField'
    DATE_SHIFT = 'dateShift'
    SUBSTITUTE_FIELD_LIST = 'substituteFieldList'
    SUBSTITUTE_FIELD_REGEX = 'substituteFieldRegex'

class DateShiftFormat(Enum):
    """
    Enum class defining date shift formats
    """
    DATE_TIME = "date-time"
    DATE = "date"

class DeIdentificationStatus(Enum):
    """
    Enum class defining de-identification status
    """
    SUCCESS = "Success"
    FAILED = "Failed"

class ConfigFileAction(Enum):
    """
    Enum class defining config file action
    """
    UPLOADED = "uploaded"
    DELETED = "deleted"


class Endpoint(Enum):
    """
    Enum class defining API endpoints
    """
    CREATE_PROJECT = "/Create_project"
    UPLOAD_EXTERNAL_FILES = "/Upload_external_files"
    GET_PROJECTS = "/Get_projects"
    GET_JSON_SCHEMA = "/Get_json_schema"
    GET_DDL_STATEMENTS = "/Get_DDL_statements"
    GET_CSV_TEMPLATES = "/Get_csv_templates"
    GET_EXCEL_TEMPLATE = "/Get_excel_template"
    GET_SPARQL_QUERIES = "/Get_sparql_queries"
    RESET_PROJECT = "/Reset_project"
    DELETE_PROJECT = "/Delete_project"
    INGEST_RDF = "/Ingest_rdf"
    INGEST_JSON = "/Ingest_json"
    INGEST_FROM_DATABASE = "/Ingest_from_database"
    INGEST_CSV = "/Ingest_csv"
    INGEST_EXCEL = "/Ingest_excel"
    TRIGGER_BATCH_INGESTION = "/Trigger_batch_ingestion"
    START_PIPELINE = "/Start_pipeline"
    STOP_PIPELINE = "/Stop_pipeline"
    START_STATISTICS = "/Start_statistics"
    GET_STATUS = "/Get_status"
    GET_PATIENTS_WITH_ERRORS = "/Get_patients_with_errors"
    GET_LOGS = "/Get_logs"
    GET_GLOBAL_LOGS = "/Get_global_logs"
    GET_QC_REPORT = "/Get_qc_report"
    GET_INIT_LOGS = "/Get_init_logs"
    GET_AIRFLOW_LOGS = "/Get_Airflow_logs"
    GET_PATIENT_FILE = "/Get_patient_file"
    GET_PATIENTS_REPORT = "/Get_patients_report"
    GET_PROCESSED_PATIENTS = "/Get_processed_patients"
    GET_DOCKER_LOGS = "/Get_docker_logs"
    DOWNLOAD_DATA = "/Download_data"
    DOWNLOAD_IN_S3 = "/Download_in_S3"
    RESET_DOWNLOAD_STATUS = "/Reset_download_status"
    GET_DE_IDENTIFICATION_REPORT = "/Get_de_identification_report"
    GET_STATISTICS_REPORTS = "/Get_statistics_reports"
    CHANGE_PASSWORD = "/Change_password"
    GET_USERS_LIST = "/Get_users_list"
    CREATE_NEW_USER = "/Create_new_user"
    DELETE_USER = "/Delete_user"
    RESET_PASSWORD = "/Reset_password"
    EXPORT_PROJECT = "/Export_project"
    IMPORT_PROJECT = "/Import_project"
    BACKUP_DATABASE = "/Backup_database"
    RESTORE_DATABASE = "/Restore_database"
    RESTORE_DE_IDENTIFICATION = "/Restore_de_identification"

class ErrorLevel(Enum):
    """
    Enum class defining pipeline error level
    """
    ERROR = "ERROR"
    WARNING = "WARNING"

class AirflowDag(Enum):
    """
    Enum class defining Airflow Dags
    """
    ALL = "all"
    RUN_BATCH_INGESTION = "run_batch_ingestion"
    RUN_DATABASE_BACKUP = "run_database_backup"
    RUN_DATABASE_EXTRACTION = "run_database_extraction"
    RUN_GRAFANA_COUNT = "run_grafana_count"
    RUN_INTEGRATION = "run_integration"
    RUN_S3_DOWNLOAD = "run_s3_download"
    RUN_NOTIFY_EINSTEIN = "run_notify_einstein"
    RUN_PRE_CHECK_AND_DE_IDENTIFICATION = "run_pre_check_and_de_identification"
    RUN_REAL_TIME_COUNT = "run_real_time_count"
    RUN_SHACLER = "run_shacler"
    RUN_SPARQLER = "run_sparqler"
    RUN_STATISTICS = "run_statistics"
    RUN_VALIDATION = "run_validation"
    RUN_WHOLE_PIPELINE = "run_whole_pipeline"

class TabularDataType(Enum):
    """
    Enum class defining type of tabular data
    """
    CSV = "CSV"
    EXCEL = "Excel"

class ActionType(Enum):
    """
    Enum class defining action types on /Ingest_from_database
    """
    START = "Start"
    CHECK_STATUS = "Check-status"
    STOP = "Stop"
    DOWNLOAD = "Download"

class StatisticsProfile(Enum):
    """
    Enum class defining statistics profile
    """
    FAST = "Fast"
    EXTENSIVE = "Extensive"

class DeIdReportType(Enum):
    """
    Enum class defining De-Id report type
    """
    CSV = ".csv"
    EXCEL = ".xlsx"

class IngestCSVDataType(Enum):
    """
    Enum class defining file type of CSV ingestion
    """
    CSV = ".csv"
    ZIP = ".zip"

class ConceptType(Enum):
    """
    Enum class with concepts type
    """
    CORE = "content"
    SUPPORTING = "supporting_concepts"

class OutputFormat(Enum):
    """
    Enum class with output formats
    """
    TURTLE = "Turtle"
    NQUADS = "NQuads"
    TRIG = "Trig"

class QCSeverityLevel(Enum):
    """
    Enum class with QC severity level
    """
    INFO = "INFO"
    WARNING = "WARNING"
    ERROR = "ERROR"

class DockerService(Enum):
    """Enum class defining Docker services
    """
    ALL = "All"
    API = "api"
    DATA_HANDLER = "data_handler"
    GRAFANA = "grafana"
    MINIO = "minio"
    PGADMIN = "pgadmin"
    POSTGRES = "postgres"
    REVERSE_PROXY = "reverse-proxy"
    CONNECTOR = "connector"