#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'database.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

import gzip
import io
import os
from datetime import datetime, date, time
import decimal
import traceback
import psycopg2
from io import BytesIO
import zipfile
import json
import tarfile
from enums import RML_JSON_LOGS, CompressionType, ConfigFileAction, DataPersistenceLayer, DeIdReportType, ErrorLevel, FileClass, FileType, IngestCSVDataType, OutputFormat, PipelineStep, PreCondition, StatisticsProfile, Steps, TabularDataType, UserType, StatisticsOutputType
from rdflib import OWL
from typing import Union, Dict
from fastapi import Response, HTTPException, status
from fastapi.responses import StreamingResponse
import re
from passlib.context import CryptContext
from botocore.exceptions import ClientError
import sys
from openpyxl import Workbook
from openpyxl.worksheet.worksheet import Worksheet
from openpyxl.worksheet.table import Table, TableStyleInfo
from openpyxl.utils.cell import get_column_letter
from openpyxl import load_workbook
from openpyxl.styles import Alignment
from openpyxl.styles import Font
import csv
import lightrdf
from psycopg2 import sql
import glob

sys.path.append(os.path.abspath(os.path.dirname(__file__)))
from minio_policy import MinioPolicy
from pre_checks import validate_configuration_file as json_validation
from de_identification import validate_configuration_file as de_identification_validation
from rml_generator_lib import niceify_prefix
from config import Config
from rdf_parser import convert_to_quads, validate_external_terminology


class Database(object):
    """
    Class containing information and methods about PostgreSQL Database. It contains the logic to connect and query data on the database and it is invoked from multiple containers
    """
    def __init__(self, project: str = None, data_provider_id: str = None):
        """Class constructor

        Args:
            project (str, optional): name of the project. Defaults to None.
            data_provider_id (str, optional): ID of the data provider. Defaults to None.
        """
        self.config = Config()
        self.conn = None
        self.cursor = None
        self.pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
        if data_provider_id is None:
            data_provider_id = self.get_data_provider_id(project=project)
        self.config.data_provider_id = data_provider_id

    def create_connection(self, database_name: str = None, auto_commit: bool = True):
        """Create database connection to Postgres

        Args:
            database_name (str, optional): Name of the database. Defaults to None.
            auto_commit (bool, optional): connection auto commit value. Defaults to True.
        """
        database = database_name if database_name is not None else self.config.postgres_user
        self.conn = psycopg2.connect(host="postgres", database=database, user=self.config.postgres_user, password=self.config.postgres_password, port='5432')
        self.conn.autocommit = auto_commit
        self.cursor = self.conn.cursor()

    def close_connection(self):
        """
        Closes database connection
        """
        self.cursor.close()
        self.conn.close()
        self.conn = None
        self.cursor = None

    def create_project(self, 
                       project_name: str, 
                       current_user: str, 
                       sphn_base_uri: str, 
                       output_format: OutputFormat, 
                       compression: bool, 
                       share_with_einstein: bool,
                       project_extended_tables_prefix: str = None):
        """
        Creates SPHN Connector project

        Args:
            project_name (str): name of the project
            current_user (str): current user triggering the project creation
            sphn_base_iri (str): base IRI of SPHN schema
            output_format (OutputFormat): output format of generated RDF data
            compression (bool): patient file must be compressed
            share_with_einstein (bool): share data with Einstein tool
            project_extended_tables_prefix (str, optional): prefix of project extended tables. Defaults to None.
        """
        credentials = self.get_credentials(user_type=UserType.INGESTION)
        current_password = credentials[current_user]
        project_extended_tables_prefix = project_extended_tables_prefix if project_extended_tables_prefix is not None else 'P_EXT__'
        self.create_connection()
        sql = """INSERT INTO configuration (project_name, data_provider_id, api_user, api_password, s3_access_key, s3_secret_key, postgres_user, postgres_password, postgres_ui_email, postgres_ui_password, airflow_user, airflow_password, airflow_firstname, airflow_lastname, airflow_email, initialized, sphn_base_iri, output_format, compression, share_with_einstein, project_extended_tables_prefix) 
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) 
        ON CONFLICT (project_name, data_provider_id) DO UPDATE SET api_user=EXCLUDED.api_user, api_password=EXCLUDED.api_password, s3_access_key=EXCLUDED.s3_access_key, s3_secret_key=EXCLUDED.s3_secret_key, postgres_user=EXCLUDED.postgres_user, postgres_password=EXCLUDED.postgres_password, \
            postgres_ui_email=EXCLUDED.postgres_ui_email, postgres_ui_password=EXCLUDED.postgres_ui_password, airflow_user=EXCLUDED.airflow_user, airflow_password=EXCLUDED.airflow_password, airflow_firstname=EXCLUDED.airflow_firstname, airflow_lastname=EXCLUDED.airflow_lastname, airflow_email=EXCLUDED.airflow_email, \
                initialized=EXCLUDED.initialized, sphn_base_iri=EXCLUDED.sphn_base_iri, output_format=EXCLUDED.output_format, compression=EXCLUDED.compression, share_with_einstein=EXCLUDED.share_with_einstein, project_extended_tables_prefix=EXCLUDED.project_extended_tables_prefix
        """

        self.cursor.execute(sql, (project_name, self.config.data_provider_id, current_user, current_password, self.config.s3_access_key, self.pwd_context.hash(self.config.s3_secret_key), 
                                self.config.postgres_user, self.pwd_context.hash(self.config.postgres_password), self.config.postgres_ui_email, self.pwd_context.hash(self.config.postgres_ui_password), self.config.airflow_user, 
                                self.pwd_context.hash(self.config.airflow_password), self.config.airflow_firstname, self.config.airflow_lastname, self.config.airflow_email, False, sphn_base_uri, output_format.value, compression,
                                share_with_einstein, project_extended_tables_prefix))
        self.close_connection()

    def delete_project(self, project_name: str, minio_policy: MinioPolicy, delete_policies: bool):
        """Deletes a project

        Args:
            project_name (str): name of the project
            minio_policy (MinioPolicy): Minio policy object
            delete_policies (bool): delete Minio policies
        """
        if delete_policies:
            minio_policy.delete(project=project_name)
        bucket = self.config.s3_connector_bucket
        if bucket_exists(bucket = bucket, config=self.config):
            page_objects_to_delete = self.list_objects_keys(bucket=bucket, prefix=f"{project_name}/")
            if page_objects_to_delete:
                for chunk in Database._split_list_into_chunks(object_names=page_objects_to_delete):
                    delete_keys = {'Objects': [{'Key': k} for k in chunk]}
                    if delete_keys['Objects']:
                        self.config.s3_client.delete_objects(Bucket=self.config.s3_connector_bucket, Delete=delete_keys)

        self.create_connection()
        cleanup_sqls = ["DELETE FROM landing_zone WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM release_zone WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM graph_zone WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM refined_zone WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM logging WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM configuration WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM shacler_logging WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM ddl_generation WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM pipeline_history WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM rml_json_logs WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM de_identification WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM de_identification_shifts WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM de_identification_scrambling WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM names_tracker WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM config_files_history WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM execution_errors WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM tdb2_status WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM external_terminologies WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM sparqler_count_instances WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM sparqler_min_max_predicates_extensive WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM sparqler_min_max_predicates_numeric_extensive WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM sparqler_min_max_predicates_fast WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM sparqler_min_max_predicates_numeric_fast WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM sparqler_has_code WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM sparqler_administrative_case_encounters WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM stats_individual_reports WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM quality_checks WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM real_time_processing WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM grafana_real_time_report WHERE project_name=%s AND data_provider_id=%s;"]
        self.cursor.execute("\n".join(cleanup_sqls), (project_name, self.config.data_provider_id) * len(cleanup_sqls))
        self.close_connection()
        if self.schema_exists(schema_name=project_name):
            self.create_connection(database_name='sphn_tables')
            self.cursor.execute(f'DROP SCHEMA "{project_name}" CASCADE')
            self.close_connection()

    def truncate_tables(self, schema_name: str):
        """Truncates all tables on the schema

        Args:
            schema_name (str): name of the schema
        """
        self.create_connection(database_name='sphn_tables')
        query = "SELECT DISTINCT table_name FROM information_schema.tables WHERE table_schema = %s"
        self.cursor.execute(query, (schema_name, ))
        tables = self.cursor.fetchall()
        if tables:
            table_names = ', '.join([f'"{schema_name}"."{table[0]}"' for table in tables])
            self.cursor.execute(f"TRUNCATE {table_names}")
        self.close_connection()

    def reset_project(self, project_name: str):
        """Resets project data

        Args:
            project_name (str): name of the project
        """
        bucket = self.config.s3_connector_bucket
        if bucket_exists(bucket = bucket, config=self.config):
            input_objects = self.list_objects_keys(bucket=bucket, prefix=f"{project_name}/Input/")
            output_objects = self.list_objects_keys(bucket=bucket, prefix=f"{project_name}/Output/")
            objects_to_remove = input_objects + output_objects
            if objects_to_remove:
                for chunk in Database._split_list_into_chunks(object_names=objects_to_remove):
                    objects_to_delete = {"Objects":[]}
                    objects_to_delete["Objects"] = [{"Key": k} for k in chunk]
                    if objects_to_delete["Objects"]:
                        self.config.s3_client.delete_objects(Bucket=bucket, Delete=objects_to_delete)

        self.create_connection()
        cleanup_queries = ["DELETE FROM landing_zone WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM release_zone WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM refined_zone WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM graph_zone WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM logging WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM pipeline_history WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM execution_errors WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM sparqler_count_instances WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM sparqler_min_max_predicates_extensive WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM sparqler_min_max_predicates_numeric_extensive WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM sparqler_min_max_predicates_fast WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM sparqler_min_max_predicates_numeric_fast WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM sparqler_has_code WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM sparqler_administrative_case_encounters WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM stats_individual_reports WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM quality_checks WHERE project_name=%s AND data_provider_id=%s;",
                        "DELETE FROM real_time_processing WHERE project_name=%s AND data_provider_id=%s;",
                        "UPDATE grafana_real_time_report SET patients_processed=0, project_size = 0 WHERE project_name=%s AND data_provider_id=%s AND step in ('Pre-check/De-ID', 'Integration', 'Validation', 'Statistics');"]
        self.cursor.execute("\n".join(cleanup_queries), (project_name, self.config.data_provider_id) * len(cleanup_queries))
        self.close_connection()

        if self.schema_exists(schema_name=project_name):
            self.truncate_tables(schema_name=project_name)

    def get_projects_configuration(self) -> list:
        """Returns configuration of initialized projects

        Returns:
            list: list of projects configuration
        """
        self.create_connection()
        query = "SELECT * FROM configuration"
        self.cursor.execute(query)
        records = self.cursor.fetchall()
        self.close_connection()
        return records

    def update_names_tracker(self, project_name: str, file_class: FileClass, object_name: str, original_name: str):
        """Updates table 'names_tracker'

        Args:
            project_name (str): name of the project
            file_class (FileClass): file class type
            object_name (str): internal name
            original_name (str): uploaded file name
        """
        self.create_connection()
        query = "INSERT INTO names_tracker (project_name, data_provider_id, file_class, object_name, original_name) VALUES (%s, %s, %s, %s, %s) \
               ON CONFLICT (project_name, data_provider_id, file_class, object_name) DO UPDATE SET original_name=EXCLUDED.original_name"
        self.cursor.execute(query, (project_name, self.config.data_provider_id, file_class.value, object_name, original_name))
        self.close_connection()

    def get_original_name(self, project_name: str, file_class: FileClass, object_name: str) -> str:
        """Gets original name of uploaded files which have been renamed by SPHN Connector logic

        Args:
            project_name (str): name of the project
            file_class (FileClass): file class type
            object_name (str): SPHN Connector internal object name

        Returns:
            str: name of uploaded file
        """
        self.create_connection()
        sql = "SELECT original_name from names_tracker WHERE project_name=%s AND data_provider_id=%s AND file_class=%s AND object_name=%s"
        self.cursor.execute(sql, (project_name, self.config.data_provider_id, file_class.value, object_name))
        original_name = self.cursor.fetchone()
        self.close_connection()
        if original_name is not None:
            return original_name[0]
    
    def get_schema_version_iri(self, object_name: str) -> str:
        """Return versionIRI value for turtle file

        Args:
            object_name (str): name of the object on S3

        Returns:
            str: version IRI
        """
        bucket = self.config.s3_connector_bucket
        object_file = self.config.s3_client.get_object(Bucket=bucket, Key=object_name)
        version_iri = validate_external_terminology(content=object_file['Body'].read(), filename=object_name)
        if version_iri is not None:
            return version_iri.lstrip('<').rstrip('>')
        else:
            return version_iri

    @staticmethod
    def _get_human_readable_size(file_size: int) -> str:
        """Converts bytes size into human readable file size

        Args:
            file_size (int): file size in bytes

        Returns:
            str: human readable file size
        """
        if file_size > 1024**3:
            return f"{round(file_size/1024**3, 2)}GB"
        elif file_size > 1024**2:
            return f"{round(file_size/1024**2, 2)}MB"
        elif file_size > 1024:
            return f"{round(file_size/1024, 2)}KB"
        else:
            return f"{round(file_size, 2)}B"

    def get_created_projects(self) -> dict:
        """Returns map with details of created projects

        Returns:
            dict: map of created projects details
        """
        self.create_connection()
        sql = "SELECT project_name, data_provider_id, api_user, initialized, output_format, share_with_einstein FROM configuration"
        self.cursor.execute(sql)
        records = self.cursor.fetchall()
        self.close_connection()
        projects = {"projects": []}
        for project in records:
            project_name = project[0]
            bucket = self.config.s3_connector_bucket
            project_dict = {"project-name": project_name, "schema-name": project_name, "data-provider-id": project[1], "created-by": project[2], "is_initialized": project[3], "output_format": project[4], 
                            "share_with_einstein": project[5], "uploaded-files": {}}
            schemas = self.list_objects(bucket=bucket, prefix=f"{project_name}/Schema/")              
            if schemas:
                schema = schemas[0]
                version_iri = self.get_schema_version_iri(object_name=schema["Key"])
                schema_name = schema["Key"].replace(f"{project_name}/Schema/", "")
                original_name = self.get_original_name(project_name=project_name, file_class=FileClass.SCHEMA, object_name=schema_name)
                schema_size = Database._get_human_readable_size(file_size=schema["Size"])
                if original_name is not None:
                    schema_entry = {'filename': schema_name, "original_name": original_name, "file_size": schema_size}
                else:
                    schema_entry = {'filename': schema_name, "file_size": schema_size}
                if version_iri is not None:
                    schema_entry['versionIRI'] = version_iri
                schema_entry['owl:imports'] = sorted(self.get_schema_imports(project_name=project_name))
                project_dict["uploaded-files"]["project_specific_schema"] = [schema_entry]

            ext_schemas = self.list_objects_keys(bucket=bucket, prefix=f"{project_name}/QAF/Schemas/")
            if ext_schemas:
                # There is only one project specific schema allowed per project
                ext_schema_dict = self.extract_content_information(file_name=ext_schemas[0], prefix=f"{project_name}/QAF/Schemas/", extract_iri=True)
                if ext_schema_dict["version_iri"] is not None:
                    ext_schema_entry = {"filename": ext_schema_dict["name"], "file_size": ext_schema_dict["file_size"], "versionIRI": ext_schema_dict["version_iri"]}
                else:
                    ext_schema_entry = {"filename": ext_schema_dict["name"], "file_size": ext_schema_dict["file_size"]}
                project_dict["uploaded-files"]["ext_schemas"] = [ext_schema_entry]

            ext_shacls = self.list_objects_keys(bucket=bucket, prefix=f"{project_name}/QAF/SHACL/")
            if ext_shacls:
                project_dict["uploaded-files"]["ext_shacls"] = []
                for ext_shacl in ext_shacls:
                    ext_shacl_dict = self.extract_content_information(file_name=ext_shacl, prefix=f"{project_name}/QAF/SHACL/")
                    project_dict["uploaded-files"]["ext_shacls"].append({"filename": ext_shacl_dict["name"], "file_size": ext_shacl_dict["file_size"]})

            ext_terminologies = self.list_objects_keys(bucket=bucket, prefix=f"{project_name}/QAF/Terminologies/")
            if ext_terminologies:
                project_dict["uploaded-files"]["ext_terminologies"] = []
                for ext_terminology in ext_terminologies:                    
                    ext_terminology_dict = self.extract_content_information(file_name=ext_terminology, prefix=f"{project_name}/QAF/Terminologies/", extract_iri=True)
                    project_dict["uploaded-files"]["ext_terminologies"].append({"filename": ext_terminology_dict["name"], "file_size": ext_terminology_dict["file_size"], "versionIRI": ext_terminology_dict["version_iri"]})

            ext_queries= self.list_objects_keys(bucket=bucket, prefix=f"{project_name}/QAF/Queries/")
            if ext_queries:
                project_dict["uploaded-files"]["ext_queries"] = []
                for ext_query in ext_queries:
                    ext_query_dict = self.extract_content_information(file_name=ext_query, prefix=f"{project_name}/QAF/Queries/")
                    project_dict["uploaded-files"]["ext_queries"].append({"filename": ext_query_dict["name"], "file_size": ext_query_dict["file_size"]})

            exceptions= self.list_objects_keys(bucket=bucket, prefix=f"{project_name}/QAF/Exceptions/")
            if exceptions:
                project_dict["uploaded-files"]["exceptions"] = []
                for exception in exceptions:
                    exceptions_dict =  self.extract_content_information(file_name=exception, prefix=f"{project_name}/QAF/Exceptions/")
                    project_dict["uploaded-files"]["exceptions"].append({"filename": exceptions_dict["name"], "file_size": exceptions_dict["file_size"]})

            checks_configs = self.list_objects_keys(bucket=bucket, prefix=f"{project_name}/Pre-Check/")
            if checks_configs:
                checks_config_dict = self.extract_content_information(file_name=checks_configs[0], prefix=f"{project_name}/Pre-Check/")
                original_name = self.get_original_name(project_name=project_name, file_class=FileClass.PRE_CHECK_CONFIG, object_name=checks_config_dict["name"])
                if original_name is not None:
                    checks_config_entry = {"filename": ext_query_dict["name"], "original_name": original_name, "file_size": checks_config_dict["file_size"]}
                else:
                    checks_config_entry = {"filename": ext_query_dict["name"], "file_size": checks_config_dict["file_size"]}
                project_dict["uploaded-files"]["pre_checks_config"] = [checks_config_entry]

            de_id_configs= self.list_objects_keys(bucket=bucket, prefix=f"{project_name}/De-Identification/{project_name}_de_identification_config.json", file_path=True)
            if de_id_configs:
                de_id_config_dict =  self.extract_content_information(file_name=de_id_configs[0], prefix=f"{project_name}/De-Identification/")
                original_name = self.get_original_name(project_name=project_name, file_class=FileClass.DE_IDENTIFICATION_CONFIG, object_name=de_id_config_dict["name"])
                if original_name is not None:
                    de_id_config_entry = {'filename': de_id_config_dict["name"], "original_name": original_name, "file_size": de_id_config_dict["file_size"]}
                else:
                    de_id_config_entry = {'filename': de_id_config_dict["name"], "file_size": de_id_config_dict["file_size"]}
                project_dict["uploaded-files"]["de_identification_config"] = [de_id_config_entry]

            projects["projects"].append(project_dict)
        return projects

    def zip_download_files(self, file_list: list, project: str, compression_type: CompressionType, zip_filename: str = None) -> StreamingResponse:
        """Zip output files to download

        Args:
            file_list (list): list of files prefixes
            project (str): name of the project
            compression_type (CompressionType): compression type
            zip_filename (str, optional): name of the ZIP file. Defaults to None.

        Returns:
            StreamingResponse: compressed data
        """
        bucket = self.config.s3_connector_bucket
        if zip_filename is None:
            zip_filename = f"{project}{compression_type.value}"
        bytes_buffer = BytesIO()
        if compression_type == CompressionType.ZIP:
            with zipfile.ZipFile(bytes_buffer, mode='w', compression=zipfile.ZIP_DEFLATED) as zip:
                for fpath in file_list:
                    try:
                        response = self.config.s3_client.get_object(Bucket=bucket, Key=fpath)
                    except Exception:
                        print("ERROR -- File '{}' for project '{}' not found in object storage even if it's supposed to be there".format(fpath, project))
                        continue
                    zip.writestr(fpath.replace(f"{project}/Statistics/Queries/", ""), response["Body"].read())
                zip.close()
        elif compression_type == CompressionType.TAR:
            tar_file_obj = tarfile.open(mode = "w:gz", fileobj=bytes_buffer) 
            for fpath in file_list:
                response = self.config.s3_client.get_object(Bucket=bucket, Key=fpath)
                _file_name = fpath.split("/")[-1]
                info = tarfile.TarInfo(_file_name)
                fpath_obj = io.BytesIO(response["Body"].read())
                info.size = fpath_obj.getbuffer().nbytes
                tar_file_obj.addfile(info, fpath_obj)
            tar_file_obj.close()

        return StreamingResponse(
            iter([bytes_buffer.getvalue()]),
            media_type="application/x-zip-compressed",
            headers = { "Content-Disposition": "attachment; filename={}".format(zip_filename)}
        )
    
    def zip_log_files(self, project: str, patient_ids: list, step: Steps, compression_type: CompressionType) -> StreamingResponse:
        """Zip log files for a project

        Args:
            project (str): name of the project
            patient_ids (list): list of patient IDs
            step (Steps): pipeline step
            compression_type (CompressionType): compression type

        Returns:
            StreamingResponse: compressed log files
        """
        zip_filename = f"{project}_logs{compression_type.value}"
        bytes_buffer = BytesIO()
        if compression_type == CompressionType.ZIP:
            with zipfile.ZipFile(bytes_buffer, mode='w', compression=zipfile.ZIP_DEFLATED) as zip:
                for patient_id in patient_ids:
                    output_filename = self.get_log_filename(project=project, patient_id=patient_id, step=step)
                    logging_messages = self.get_logging_messages(project=project, patient_id=patient_id, step=step)
                    zip.writestr(output_filename, logging_messages.encode())
                zip.close()
        elif compression_type == CompressionType.TAR:
            tar_file_obj = tarfile.open(mode = "w:gz", fileobj=bytes_buffer)
            for patient_id in patient_ids:
                output_filename = self.get_log_filename(project=project, patient_id=patient_id, step=step)
                logging_messages = self.get_logging_messages(project=project, patient_id=patient_id, step=step)
                info = tarfile.TarInfo(output_filename)
                log_bytes = io.BytesIO(logging_messages.encode())
                info.size = log_bytes.getbuffer().nbytes
                tar_file_obj.addfile(info, log_bytes)
            tar_file_obj.close()
        return StreamingResponse(
            iter([bytes_buffer.getvalue()]),
            media_type="application/x-zip-compressed",
            headers = { "Content-Disposition": "attachment; filename={}".format(zip_filename)}
        )

    def record_patients_download(self, project_name: str, patient_ids: list):
        """Mark patient as downloaded in release_zone table

        Args:
            project_name (str): name of the project
            patient_ids (list): list of downloaded patient IDs
        """
        self.create_connection()
        for patient_id in patient_ids:
            sql = "UPDATE release_zone SET downloaded=True WHERE project_name=%s AND data_provider_id=%s and patient_id=%s"
            self.cursor.execute(sql, (project_name, self.config.data_provider_id, patient_id))
        self.close_connection()

    def get_released_patients(self, 
                              project: str, 
                              patient_id: str, 
                              validation_status: str, 
                              skip_downloaded: bool, 
                              batch_size: int, 
                              patients_only: bool) -> Union[dict, dict]:
        """Get released patients

        Args:
            project (str): name of the project
            patient_id (str): patient ID
            validation_status (str): validation status
            skip_downloaded (bool): only download files that haven't been downloaded
            batch_size (int): batch size of downloaded data
            patients_only (bool): download patient data only

        Returns:
            Union[dict, dict]: patients map, map with QC logs
        """
        if batch_size is not None:
            skip_downloaded = True
            limit = f" LIMIT {str(abs(int(batch_size)))}"
        else:
            limit = ""
        self.create_connection()
        if patient_id is not None:
            if validation_status is not None:
                if skip_downloaded:
                    sql = "SELECT object_name, patient_id FROM release_zone WHERE project_name = %s AND patient_id = %s AND data_provider_id = %s AND validated_ok = %s AND downloaded=False" + limit
                else:
                    sql = "SELECT object_name, patient_id FROM release_zone WHERE project_name = %s AND patient_id = %s AND data_provider_id = %s AND validated_ok = %s"
                self.cursor.execute(sql, (project, patient_id, self.config.data_provider_id, validation_status == 'OK'))
            else:
                if skip_downloaded:
                    sql = "SELECT object_name, patient_id FROM release_zone WHERE project_name = %s AND patient_id = %s AND data_provider_id = %s AND downloaded=False" + limit
                else:
                    sql = "SELECT object_name, patient_id FROM release_zone WHERE project_name = %s AND patient_id = %s AND data_provider_id = %s"
                self.cursor.execute(sql, (project, patient_id, self.config.data_provider_id))
        else:
            if validation_status is not None:
                if skip_downloaded:
                    sql = "SELECT object_name, patient_id FROM release_zone WHERE project_name = %s AND data_provider_id = %s AND validated_ok = %s AND downloaded=False ORDER BY patient_id" + limit
                else:
                    sql = "SELECT object_name, patient_id FROM release_zone WHERE project_name = %s AND data_provider_id = %s AND validated_ok = %s ORDER BY patient_id"
                self.cursor.execute(sql, (project, self.config.data_provider_id, validation_status == 'OK'))
            else:
                if skip_downloaded:
                    sql = "SELECT object_name, patient_id FROM release_zone WHERE project_name = %s AND data_provider_id = %s AND downloaded=False ORDER BY patient_id" + limit
                else:
                    sql = "SELECT object_name, patient_id FROM release_zone WHERE project_name = %s AND data_provider_id = %s ORDER BY patient_id"
                self.cursor.execute(sql, (project, self.config.data_provider_id))

        records = self.cursor.fetchall()
        patients_map = {}
        if records:
            for record in records:
                patients_map[record[1].strip()] = record[0].strip()
        qc_logs = {}
        if not patients_only:
            for patient_id in patients_map.keys():
                sql = "SELECT message FROM logging WHERE project_name=%s AND data_provider_id=%s AND patient_id=%s AND step='Validation' AND status='Validation Result'"
                self.cursor.execute(sql, (project, self.config.data_provider_id, patient_id))
                record = self.cursor.fetchone()
                if record:
                    pattern = r'\| (.*?) \| (.*?) \| (.*?) \| (.*?) \| (.*?) \| (.*?) \| (.*?) \| (.*?) \|'
                    matches = re.findall(pattern, record[0])
                    if len(matches) >1:
                        header_columns = f"| {' | '.join(matches[0])} |"
                        header = f"{'-'*len(header_columns)}\n{header_columns}\n{'='*len(header_columns)}"
                        results = '\n'.join([f"| {' | '.join(match)} | " for match in matches[1:]])
                        report = f"{header}\n{results}\n{'-'*len(header_columns)}"
                        qc_logs[patient_id] = report
        self.close_connection()
        return patients_map, qc_logs

    def get_processed_files(self, 
                            project_name: str, 
                            patient_id: str, 
                            validation_status: str, 
                            compression_type: CompressionType, 
                            skip_downloaded: bool, 
                            batch_size: int,
                            patients_only: bool) -> list:
        """Returns the S3 prefix of the processed files

        Args:
            project_name (str): name of the project
            patient_id (str): unique ID of the patient
            validation_status (str): validation status of the output file
            compression_type (CompressionType): compression type if multiple files found
            skip_downloaded (bool): only download files that haven't already been downloaded
            batch_size (int): batch size of download data
            patients_only (bool): download only patient data

        Raises:
            HTTPException: no files found

        Returns:
            list: list of processed files
        """
        patients_map, qc_logs = self.get_released_patients(project=project_name, patient_id=patient_id, validation_status=validation_status, skip_downloaded=skip_downloaded, batch_size=batch_size,
                                                           patients_only=patients_only)
        if not patients_map:
            if patient_id:
                if validation_status is not None:
                    error_message = "No files to download for patient id '{}' and project '{}' and validation status '{}'".format(patient_id, project_name, validation_status)
                else:
                    error_message = "No files to download for patient id '{}' and project '{}'".format(patient_id, project_name)
            else:
                if validation_status is not None:
                    error_message = "No files to download for project '{}' and validation status '{}'".format(project_name, validation_status)
                else:
                    error_message = "No files to download for project '{}'".format(project_name)
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=error_message)
        else:
            download_bundle = self.generate_download_bundle(patients_map=patients_map, project=project_name, compression_type=compression_type, qc_logs=qc_logs, batch_size=batch_size, patients_only=patients_only)
            self.record_patients_download(project_name=project_name, patient_ids=patients_map.keys())
            return download_bundle

    def check_data_to_download(self, project_name: str, patient_id: str, validation_status: str):
        """Check that data is available for download

        Args:
            project_name (str): name of the project
            patient_id (str): ID of the patient
            validation_status (str): validation status

        Raises:
            HTTPException: exception if no data is available
        """
        patients_map, _ = self.get_released_patients(project=project_name, patient_id=patient_id, validation_status=validation_status, skip_downloaded=False, batch_size=None,
                                                     patients_only=True)
        if not patients_map:
            if patient_id:
                if validation_status is not None:
                    error_message = "No files to download for patient id '{}' and project '{}' and validation status '{}'".format(patient_id, project_name, validation_status)
                else:
                    error_message = "No files to download for patient id '{}' and project '{}'".format(patient_id, project_name)
            else:
                if validation_status is not None:
                    error_message = "No files to download for project '{}' and validation status '{}'".format(project_name, validation_status)
                else:
                    error_message = "No files to download for project '{}'".format(project_name)
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=error_message)
        
    def get_anonymized_patient_id(self, project: str, patient_id: str) -> str:
        """Return anonymized patient ID

        Args:
            project (str): name of the project
            patient_id (str): patient ID

        Returns:
            str: anonymized patient ID
        """
        query = "SELECT new_value FROM de_identification_scrambling WHERE data_provider_id=%s AND project_name=%s AND patient_id=%s AND key_path=%s AND anonymized=True"
        self.cursor.execute(query, (self.config.data_provider_id, project, patient_id, 'sphn:SubjectPseudoIdentifier/id'))
        record = self.cursor.fetchone()
        if record is not None:
            return record[0]
        else:
            return patient_id
        
    def cleanup_anonymized_patient_records(self, project: str):
        """Cleanup anonymized patient records

        Args:
            project (str): name of the project
        """
        query = "DELETE FROM de_identification_scrambling WHERE data_provider_id=%s AND project_name=%s AND anonymized=True"
        self.cursor.execute(query, (self.config.data_provider_id, project))

    def get_de_identified_filename(self, project: str, patient_id: str, extension: str) -> str:
        """Get de-identified filename

        Args:
            project (str): name of the project
            patient_id (str): ID of the patient
            extension (str): file extension

        Returns:
            str: de-identified filename
        """
        query = "SELECT new_value FROM de_identification_scrambling WHERE data_provider_id=%s AND project_name=%s AND patient_id=%s AND key_path=%s"
        self.cursor.execute(query, (self.config.data_provider_id, project, patient_id, 'sphn:SubjectPseudoIdentifier/id'))
        record = self.cursor.fetchone()
        if record is not None:
            de_id_patient_id = record[0]
        else:
            de_id_patient_id = patient_id
        return f"{project}_{de_id_patient_id}_{self.config.data_provider_id}{extension}"

    def generate_download_bundle(self, 
                                 patients_map: dict, 
                                 project: str, 
                                 compression_type: CompressionType, 
                                 qc_logs: dict, batch_size: int, 
                                 patients_only: bool, 
                                 s3_download: bool = False) -> StreamingResponse:
        """Generate download bundle

        Args:
            patients_map (dict): patients map
            project (str): name of the project
            compression_type (CompressionType): compression type
            batch_size (int): batch size of downloaded data
            patients_only (bool): download patients data only
            s3_download (bool): download into S3 storage

        Returns:
            StreamingResponse: download bundle archive
        """
        bucket = self.config.s3_connector_bucket
        excel_stats_report = None
        if batch_size is None and not patients_only:
            report_name = f"{project}/Statistics/Reports/{project}_statistics_report{StatisticsOutputType.EXCEL.value}"
            reports = self.list_objects_keys(bucket=bucket, prefix=report_name, file_path=True)
            if reports:
                excel_stats_report = self.config.s3_client.get_object(Bucket=bucket, Key=report_name)["Body"].read()
        zip_filename = f"{project}{compression_type.value}"
        bytes_buffer = BytesIO()
        self.create_connection()
        if compression_type == CompressionType.ZIP:
            with zipfile.ZipFile(bytes_buffer, mode='w', compression=zipfile.ZIP_DEFLATED) as zip:
                for patient_id, fpath in patients_map.items():
                    try:
                        response = self.config.s3_client.get_object(Bucket=bucket, Key=fpath)
                    except Exception:
                        print(f"ERROR -- File '{fpath}' for project '{project}' not found in object storage even if it's supposed to be there")
                        continue
                    extension = f".{fpath.split('.')[-1]}"
                    de_id_filename = self.get_de_identified_filename(project=project, patient_id=patient_id, extension=extension)
                    zip.writestr(de_id_filename, response["Body"].read())
                if excel_stats_report is not None:
                    zip.writestr("Statistics/statistics_report.xlsx", excel_stats_report)
                for patient_id, qc_log in qc_logs.items():
                    de_id_filename = self.get_de_identified_filename(project=project, patient_id=patient_id, extension='.log')
                    zip.writestr(de_id_filename, qc_log.encode())
                zip.close()
        elif compression_type == CompressionType.TAR:
            tar_file_obj = tarfile.open(mode = "w:gz", fileobj=bytes_buffer) 
            for patient_id, fpath in patients_map.items():
                response = self.config.s3_client.get_object(Bucket=bucket, Key=fpath)
                extension = f".{fpath.split('.')[-1]}"
                _file_name = self.get_de_identified_filename(project=project, patient_id=patient_id, extension=extension)
                info = tarfile.TarInfo(_file_name)
                fpath_obj = io.BytesIO(response["Body"].read())
                info.size = fpath_obj.getbuffer().nbytes
                tar_file_obj.addfile(info, fpath_obj)
            if excel_stats_report is not None:
                info = tarfile.TarInfo('statistics_report.xlsx')
                log_bytes = io.BytesIO(excel_stats_report)
                info.size = log_bytes.getbuffer().nbytes
                tar_file_obj.addfile(info, log_bytes)
            for patient_id, qc_log in qc_logs.items():
                de_id_filename = self.get_de_identified_filename(project=project, patient_id=patient_id, extension='.log')
                info = tarfile.TarInfo(de_id_filename)
                fpath_obj = io.BytesIO(qc_log.encode())
                info.size = fpath_obj.getbuffer().nbytes
                tar_file_obj.addfile(info, fpath_obj)
            tar_file_obj.close()
        self.close_connection()
        if s3_download:
            return bytes_buffer.getvalue()
        else:
            return StreamingResponse(
                iter([bytes_buffer.getvalue()]),
                media_type="application/x-zip-compressed",
                headers = { "Content-Disposition": "attachment; filename={}".format(zip_filename)}
            )

    def get_patients_list(self, project_name: str) -> dict:
        """Returns list of patients which have been processed

        Args:
            project_name (str): name of the project

        Raises:
            HTTPException: no patients found

        Returns:
            dict: patient data
        """
        self.create_connection()
        data_provider_id = self.config.data_provider_id
        sql = "SELECT patient_id, validated_ok, timestmp FROM release_zone WHERE project_name = %s AND data_provider_id = %s"
        self.cursor.execute(sql, (project_name, data_provider_id))
        results = self.cursor.fetchall()
        patients = {"patients": []}
        for patient in results:
            patients["patients"].append({"id": patient[0], "validated_ok": patient[1], "timestamp": patient[2].strftime("%d-%m-%Y %H:%M:%S")})
        self.close_connection()
        if patients['patients']:
            return patients
        else:
            message = f"No processed patients found for project '{project_name}'"
            raise  HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=message)

    def get_credentials(self, user_type: UserType) -> dict:
        """Returns map of API users and hashed passwords

        Args:
            user_type (UserType): type of user

        Returns:
            dict: credentials map
        """
        users = [self.config.api_user]
        passwords = [self.pwd_context.hash(self.config.api_password)]
        self.create_connection()
        sql = "SELECT api_user, api_password FROM api_credentials"
        if user_type == UserType.ADMIN:
            sql += ' WHERE user_type<>%s and user_type<>%s'
            self.cursor.execute(sql, (UserType.INGESTION.value, UserType.STANDARD.value))
        elif user_type == UserType.STANDARD:
            sql += ' WHERE user_type<>%s'
            self.cursor.execute(sql, (UserType.INGESTION.value, ))
        else:
            self.cursor.execute(sql)
        results = self.cursor.fetchall()
        for item in results:
            users.append(item[0])
            passwords.append(item[1])
        self.close_connection()
        credentials = dict(zip(users, passwords))
        return credentials

    def setup_api_user(self, user: str, password: str, user_type: UserType, current_user: str, minio_policy: MinioPolicy) -> str:
        """Setup a new API user

        Args:
            user (str): new username
            password (str): new password
            user_type (UserType): type of user
            current_user (str): current user
            minio_policy (MinioPolicy): Minio policy object

        Raises:
            HTTPException: password length issue
            HTTPException: user already existing
            HTTPException: no permissions to create user
            HTTPException: not allowed username
            HTTPException: unexpected exception

        Returns:
            str: return message
        """
        if len(password) < 12:
            raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail="Password must be at least 12 characters long")
        api_credentials = self.get_credentials(user_type=UserType.INGESTION)
        if user in api_credentials:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f"API user '{user}' already exists. To reset the password for that user, please use the ad hoc method")
        else:
            if current_user != self.config.api_user and user_type == UserType.ADMIN:
                raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Current user does not have permissions to create ADMIN type users")
            if not bool(re.match(r'^[a-zA-Z0-9\._-]*$', user)):
                raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=rf"Creation of API user '{user}' failed. User name must match regex expression '^[a-zA-Z0-9\._-]*$'")
            minio_policy.create(user=user, password=password, user_type=user_type)
            self.create_connection()
            sql = "INSERT INTO api_credentials (api_user, api_password, user_type) VALUES (%s, %s, %s)"
            self.cursor.execute(sql, (user, self.pwd_context.hash(password), user_type.value))
            self.close_connection()
            try:
                self.grant_permissions(user=user, password=password, user_type=user_type)
            except Exception as exc:
                try:
                    self.create_connection()
                    sql = 'DELETE FROM api_credentials WHERE api_user=%s'
                    self.cursor.execute(sql, (user, ))
                    self.close_connection()
                    self.revoke_permissions(user=user, user_type=user_type)
                except:
                    pass
                raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=f"Creation of API user '{user}' failed: {exc}")
            return "API user '{}' successfully configured".format(user)

    def delete_api_user(self, user: str, current_user: str, minio_policy: MinioPolicy) -> str:
        """Deletes API user

        Args:
            user (str): user to delete
            current_user (str): current user
            minio_policy (MinioPolicy): Minio policy object

        Raises:
            HTTPException: no permissions to delete user
            HTTPException: user not found

        Returns:
            str: return message
        """
        minio_policy.delete_user(user=user)
        api_credentials = self.get_credentials(user_type=UserType.INGESTION)
        if user == self.config.api_user:
            error_message = f"Admin user '{user}' cannot be deleted. To change it, update configuration file, and restart the connector"
            raise HTTPException(status_code=status.HTTP_405_METHOD_NOT_ALLOWED, detail=error_message)
        elif user not in api_credentials:
            error_message = "API user '{}' not found".format(user)
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=error_message)
        else:
            user_type = self.get_user_type(user=user)
            if current_user != self.config.api_user and user_type == UserType.ADMIN:
                raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Current user does not have permissions to delete ADMIN type users")
            self.create_connection()
            sql = "DELETE FROM api_credentials WHERE api_user = %s"
            self.cursor.execute(sql, (user, ))
            self.close_connection()
            self.revoke_permissions(user=user, user_type=user_type)
            return "API user '{}' successfully removed".format(user)

    def change_api_user_pwd(self, 
                            user: str, 
                            old_password: str, 
                            new_password: str, 
                            current_user: str, 
                            minio_policy: MinioPolicy) -> str:
        """Changes API user password

        Args:
            user (str): name of the user
            old_password (str): old password
            new_password (str): new password
            current_user (str): current user
            minio_policy (MinioPolicy): Minio policy object

        Raises:
            HTTPException: no permissions to modify user
            HTTPException: password too short
            HTTPException: wrong old password
            HTTPException: new password must differ from old password
            HTTPException: user not found

        Returns:
            str: return message
        """
        if user == self.config.api_user:
            error_message = f"Admin user '{user}' cannot be modified. To change it, update configuration file, and restart the connector"
            raise HTTPException(status_code=status.HTTP_405_METHOD_NOT_ALLOWED, detail=error_message)
        if len(new_password) < 12:
            raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail="Password must be at least 12 characters long")
        self.create_connection()
        sql = "SELECT api_password from api_credentials WHERE api_user = %s"
        self.cursor.execute(sql, (user, ))
        result = self.cursor.fetchone()
        self.close_connection()
        if result is None:
            return_message = "API user '{}' not found".format(user)
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=return_message)
        user_type = self.get_user_type(user=user)
        if current_user != self.config.api_user and current_user != user:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Current user does not have permissions to modify other users")
        if not self.pwd_context.verify(old_password, result[0]):
            return_message = "Provided old password for user '{}' differs from the existing one. No changes applied".format(user)
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=return_message)
        elif self.pwd_context.verify(new_password, result[0]):
            return_message = "New password for user '{}' must differ from the old one".format(user)
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=return_message)
        else:
            minio_policy.set_user_password(user=user, password=new_password)
            self.revoke_permissions(user=user, user_type=user_type)
            self.grant_permissions(user=user, password=new_password, user_type=user_type)
            self.create_connection()
            sql = "UPDATE api_credentials SET api_password = %s WHERE api_user = %s"
            self.cursor.execute(sql, (self.pwd_context.hash(new_password), user))
            self.close_connection()
            return "Password for API user '{}' successfully updated".format(user)
        
    def reset_api_user_pwd(self, user: str, new_password: str, current_user: str, minio_policy: MinioPolicy) -> str:
        """Changes API user password

        Args:
            user (str): name of the user
            new_password (str): new password
            current_user (str): current user
            minio_policy (MinioPolicy): Minio policy object

        Raises:
            HTTPException: no permissions to reset password
            HTTPException: password too short
            HTTPException: user not found

        Returns:
            str: return message
        """
        if user == self.config.api_user:
            error_message = f"Admin user '{user}' cannot be modified. To change it, update configuration file, and restart the connector"
            raise HTTPException(status_code=status.HTTP_405_METHOD_NOT_ALLOWED, detail=error_message)
        if len(new_password) < 12:
            raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail="Password must be at least 12 characters long")
        self.create_connection()
        sql = "SELECT api_password from api_credentials WHERE api_user = %s"
        self.cursor.execute(sql, (user, ))
        result = self.cursor.fetchone()
        self.close_connection()
        if result is None:
            return_message = "API user '{}' not found".format(user)
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=return_message)
        user_type = self.get_user_type(user=user)
        current_user_type = self.get_user_type(user=current_user)
        if current_user != self.config.api_user:
            if current_user_type != UserType.ADMIN:
                raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Only admin users can reset passwords")
            elif user_type == UserType.ADMIN and current_user != user:
                raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Current user does not have permissions to reset other ADMIN type users")
        minio_policy.set_user_password(user=user, password=new_password)
        self.revoke_permissions(user=user, user_type=user_type)
        self.grant_permissions(user=user, password=new_password, user_type=user_type)
        self.create_connection()
        sql = "UPDATE api_credentials SET api_password = %s WHERE api_user = %s"
        self.cursor.execute(sql, (self.pwd_context.hash(new_password), user))
        self.close_connection()
        return "Password for API user '{}' successfully updated".format(user)

    
    def get_log_filename(self, project: str, patient_id: str, step: Steps) -> str:
        """Gets name of log file

        Args:
            project (str): name of the project
            patient_id (str): unique ID of the patients
            step (Steps): pipeline step

        Returns:
            str: log filename
        """
        if step == Steps.ALL:
            return f"{project}_{patient_id}_{self.config.data_provider_id}.log"
        else:
            return f"{project}_{patient_id}_{self.config.data_provider_id}__{step.value.replace('/', '-').upper()}.log"
    
    def get_patient_log_response(self, project: str, patient_id: str, step: Steps) -> Response:
        """Returns Response of logs for specific patient

        Args:
            project (str): project name
            patient_id (str): unique patient ID
            step (Steps): step

        Returns:
            Response: logs response
        """
        output_filename = self.get_log_filename(project=project, patient_id=patient_id, step=step)
        logging_messages = self.get_logging_messages(project=project, patient_id=patient_id, step=step)
        headers = {'Content-Disposition': 'attachment; filename="{}"'.format(output_filename)}
        return Response(logging_messages, headers=headers)

    def get_logs(self, project: str, patient_id: str, step: str, compression_type: CompressionType) -> Union[Response, StreamingResponse]:
        """Gets logs data to be downloaded

        Args:
            project (str): name of the project
            patient_id (str): unique ID of the patient
            step (str): pipeline step
            compression_type (CompressionType): compression type

        Raises:
            HTTPException: no logs found

        Returns:
            Union[Response, StreamingResponse]: logs data
        """
        step = Steps(step)  
        if patient_id is not None:
            return self.get_patient_log_response(project=project, patient_id=patient_id, step=step)
        else:
            self.create_connection()
            if step == Steps.ALL:
                sql = "SELECT DISTINCT patient_id from logging WHERE project_name = %s"
                self.cursor.execute(sql, (project, ))
            else:
                sql = "SELECT DISTINCT patient_id from logging WHERE project_name = %s and step = %s"
                self.cursor.execute(sql, (project, step.value))
            patient_ids = [id[0] for id in self.cursor.fetchall()]
            self.close_connection()
            if patient_ids:
                if len(patient_ids) > 1:
                    return self.zip_log_files(project=project, patient_ids=patient_ids, step=step, compression_type=compression_type)
                else:
                    return self.get_patient_log_response(project=project, patient_id=patient_ids[0], step=step)
            else:
                if step == Steps.ALL:
                    error_message = f"No logs found for project '{project}'"
                else:
                    error_message = f"No logs found for project '{project}' and step '{step.value}'"
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=error_message)

    def get_landing_zone_report(self, project: str, patient_id: str) -> str:
        """Get landing zone report

        Args:
            project (str): name of the project
            patient_id (str): ID of the patient

        Returns:
            str: landing zone report
        """
        self.create_connection()
        landing_zone_report = ""
        if patient_id is None:
            sql = "SELECT patient_id, timestmp, ingestion_type FROM landing_zone WHERE project_name=%s AND data_provider_id=%s ORDER BY timestmp DESC"
            self.cursor.execute(sql, (project, self.config.data_provider_id))
        else:
            sql = "SELECT patient_id, timestmp, ingestion_type FROM landing_zone WHERE project_name=%s AND data_provider_id=%s AND patient_id=%s ORDER BY timestmp DESC"
            self.cursor.execute(sql, (project, self.config.data_provider_id, patient_id))
        records = self.cursor.fetchall()
        for record in records:
            landing_zone_report += f"\nPatient '{record[0]}' ingested at '{record[1]}' with {record[2]} ingestion type"
        self.close_connection()
        if landing_zone_report:
            landing_zone_report = f"{'-'*100}\nData ingestion report for project '{project}'\n{'-'*100}" + landing_zone_report + "\n\n"
        return landing_zone_report

    def get_configuration_history_report(self, project: str) -> str:
        """Gets configuration files history report

        Args:
            project (str): name of the project

        Returns:
            str: report
        """
        self.create_connection()
        config_files_history_report = ""
        sql = "SELECT filename, file_class, file_action, timestmp FROM config_files_history WHERE project_name=%s ORDER BY timestmp DESC"
        self.cursor.execute(sql, (project, ))
        records = self.cursor.fetchall()
        for record in records:
            config_files_history_report += f"\nFile '{record[0]}' of type '{record[1]}' {record[2]} at '{record[3]}'"
        self.close_connection()
        if config_files_history_report:
            config_files_history_report = f"{'-'*100}\nConfiguration files history report for project '{project}'\n{'-'*100}" + config_files_history_report + "\n\n"
        return config_files_history_report

    def get_patient_ids(self, project: str, patient_id: str, validation_status: str) -> list:
        """Get list of patient ids for global logs

        Args:
            project (str): project name
            patient_id (str): patient ID
            validation_status (str): validation status

        Returns:
            list: list of patient ids
        """
        self.create_connection()
        patient_ids = []
        if validation_status:
            condition = f"AND validated_ok = {validation_status == 'OK'}" if validation_status is not None else ''
            if patient_id is None:
                sql = f"SELECT patient_id FROM {DataPersistenceLayer.RELEASE_ZONE.value} WHERE project_name = %s AND data_provider_id = %s {condition} ORDER BY timestmp DESC"
                self.cursor.execute(sql, (project, self.config.data_provider_id))
                records = self.cursor.fetchall()
                for record in records:
                    patient_ids.append(record[0])
            else:
                sql = f"SELECT patient_id FROM {DataPersistenceLayer.RELEASE_ZONE.value} WHERE project_name = %s AND patient_id = %s AND data_provider_id = %s {condition} ORDER BY timestmp DESC"
                self.cursor.execute(sql, (project, patient_id, self.config.data_provider_id))
                records = self.cursor.fetchone()
                if records is not None:
                    patient_ids = [patient_id]
        else:
            if patient_id is None:
                sql = "SELECT DISTINCT patient_id from logging WHERE project_name = %s AND data_provider_id = %s"
                self.cursor.execute(sql, (project, self.config.data_provider_id))
                records = self.cursor.fetchall()
                for record in records:
                    patient_ids.append(record[0])
            else:
                sql = "SELECT DISTINCT patient_id from logging WHERE project_name = %s AND patient_id = %s AND data_provider_id = %s"
                self.cursor.execute(sql, (project, patient_id, self.config.data_provider_id))
                records = self.cursor.fetchone()
                if records is not None:
                    patient_ids = [patient_id]
        self.close_connection()
        return patient_ids

    def get_global_logs(self, project: str, patient_id: str, validation_status: str):
        """Gets concatenated global logs for given validation status

        Args:
            project (str): project name
            patient_id (str): ID of the patient
            validation_status (str): status of the validation
        """
        headers = {'Content-Disposition': 'attachment; filename="{}_global_logs.log"'.format(project)}
        patient_ids = self.get_patient_ids(project=project, patient_id=patient_id, validation_status=validation_status)
        global_logs = f"{'='*100}\nGlobal logs for project '{project}'\n{'='*100}\n\n"
        global_logs += self.get_configuration_history_report(project=project)
        global_logs += self.get_landing_zone_report(project=project, patient_id=patient_id)
        if not patient_ids:
            if global_logs == f"{'='*100}\nGlobal logs for project '{project}'\n{'='*100}\n\n":
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"No global logs found for project '{project}'")
            return Response(global_logs, headers=headers)
        for patient_id in patient_ids:
            global_logs += f"{'-'*100}\nLogs for patient '{patient_id}'\n{'-'*100}\n"
            try:
                logging_messages = self.get_logging_messages(project=project, patient_id=patient_id, step="All")
            except HTTPException:
                logging_messages = f"No logs found for patient '{patient_id}'"
            global_logs += f"{logging_messages}\n\n"
        return Response(global_logs, headers=headers)

    def get_logging_messages(self, project: str, patient_id: str, step: str) -> Response:
        """Returns the logging messages for a specific project, patient and eventually specific step

        Args:
            project (str): name of the project
            patient_id (str): unique ID of the patient
            step (str): pipeline step

        Raises:
            HTTPException: no logs found

        Returns:
            Response: logging messages data
        """
        step = Steps(step)
        self.create_connection()
        logging_messages = ""
        if step == Steps.ALL:
            sql = "SELECT step, message, timestmp, status from logging WHERE project_name = %s AND patient_id = %s order by timestmp ASC"
            self.cursor.execute(sql, (project, patient_id))
            messages = self.cursor.fetchall()
            for message in messages:
                timestmp = message[2].strftime("%d-%m-%Y %H:%M:%S")
                logging_messages += "{} [{}] -- [{}] {}\n".format(message[0].upper(), timestmp, message[3].upper(), message[1])
        else:
            sql = "SELECT step, message, timestmp, status from logging WHERE project_name = %s AND patient_id = %s AND step = %s order by timestmp ASC"
            self.cursor.execute(sql, (project, patient_id, step.value))
            messages = self.cursor.fetchall()
            for message in messages:
                timestmp = message[2].strftime("%d-%m-%Y %H:%M:%S")
                logging_messages += "{} [{}] -- [{}] {}\n".format(message[0].upper(), timestmp, message[3].upper() ,message[1])
        self.close_connection()
        if not logging_messages:
            if step == Steps.ALL:
                error_message = "No logs found for patient '{}' and project '{}'".format(patient_id, project)
            else:
               error_message = "No logs found for patient '{}', project '{}', and step '{}'".format(patient_id, project, step.value)
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=error_message)
        else:
            return logging_messages

    def get_shacler_messages(self, project: str) -> Response:
        """Returns SHACLer execution messages for a specific project

        Args:
            project (str): name of the project

        Raises:
            HTTPException: no logs found

        Returns:
            Response: SHACLer messages data
        """
        self.create_connection()
        logging_messages = ""
        sql = "SELECT status, timestmp, message from shacler_logging WHERE project_name = %s order by timestmp ASC"
        self.cursor.execute(sql, (project, ))
        messages = self.cursor.fetchall()
        for message in messages:
            timestmp = message[1].strftime("%d-%m-%Y %H:%M:%S")
            logging_messages += "{} [{}]: -- {}\n".format(message[0].upper(), timestmp, message[2])
        self.close_connection()
        if not logging_messages:
            error_message = "No SHACLer logs found for project '{}'".format(project)
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=error_message)
        else:
            return logging_messages

    def is_project_created(self, project: str) -> bool:
        """Checks that project has been correctly created

        Args:
            project (str): name of the project

        Returns:
            bool: is project created
        """
        self.create_connection()
        sql = "SELECT * from configuration WHERE project_name = %s"
        self.cursor.execute(sql, (project, ))
        record = self.cursor.fetchone()
        self.close_connection()
        return record is not None
    
    def is_project_initialized(self, project: str) -> bool:
        """Checks that project has been initialized

        Args:
            project (str): name of the project

        Raises:
            HTTPException: failed initialization

        Returns:
            bool: is project initialized
        """
        self.create_connection()
        sql = "SELECT initialized from configuration WHERE project_name = %s"
        self.cursor.execute(sql, (project, ))
        record = self.cursor.fetchone()
        self.close_connection()
        is_initialized = False if record is None else record[0]
        if not is_initialized:
            return_message = f"Cannot trigger action because project '{project}' has not been successfully initialized. Check init logs and recreate the project"
            raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=return_message)

    def mark_project_as_initialized_in_db(self, project: str):
        """Update 'initialized' column in 'configuration table

        Args:
            project (str): name of the project
        """
        self.create_connection()
        sql = "UPDATE configuration SET initialized = %s WHERE project_name = %s"
        self.cursor.execute(sql, (True, project, ))
        self.close_connection()

    def deinitialize_project_in_db(self, project: str):
        """Deinitialize a project in the configuration table in the DB

        Args:
            project (str): name of the project
        """
        self.create_connection()
        sql = "UPDATE configuration SET initialized = %s WHERE project_name = %s"
        self.cursor.execute(sql, (False, project, ))
        self.close_connection()

    def get_existing_case_insensitive_project(self, project: str) -> str:
        """Returns existing project name when case insensitive equality is found between project names but case 
           insensitive inequiality is fulfilled

        Args:
            project (str): name of the project

        Returns:
            str: project name
        """
        self.create_connection()
        sql = "SELECT project_name from configuration WHERE lower(project_name)=lower(%s) AND project_name<>%s"
        self.cursor.execute(sql, (project, project))
        record = self.cursor.fetchone()
        self.close_connection()
        if record is None:
            return None
        else:
            return record[0]

    def project_exists(self, project: str):
        """Raise exception if a project already exists during project creation

        Args:
            project (str): name of the project

        Raises:
            HTTPException: project exists and cannot be created
        """
        existing_project = self.get_existing_case_insensitive_project(project=project)
        exists = False
        if existing_project is not None:
            pre_conditions = "Invalid project name '{}'. Project '{}' already exists. Case insensitive equality between project name is not allowed".format(project, existing_project)
            exception_type = status.HTTP_400_BAD_REQUEST
            exists = True
        elif self.is_project_created(project=project):
            pre_conditions = f"Project '{project}' already exists. To update schema file, please delete and recreate the project"
            exception_type = status.HTTP_409_CONFLICT
            exists = True
        if exists:
            raise HTTPException(status_code=exception_type, detail=pre_conditions)

    def check_pre_conditions(self, 
                             project: str, 
                             type: PreCondition, 
                             content: bytes = None, 
                             filename: str = None, 
                             file_type: FileType = None, 
                             file_class: FileClass = None, 
                             tabular_type: TabularDataType = None, 
                             csv_file_type: IngestCSVDataType = None, 
                             einstein: bool = False, 
                             output_format: OutputFormat = None,
                             project_specific: bool = False):
        """Checks that pre-conditions are met for ingesting data

        Args:
            project (str): name of the project
            type (PreCondition): type of pre-condition to check
            content (bytes, optional): content of file to ingest. Defaults to None.
            filename (str, optional): name of the file to ingest. Defaults to None.
            file_type (FileType, optional): type of the file. Defaults to None.
            file_class (FileClass, optional): class of the file. Defaults to None.
            tabular_type (TabularDataType, optional): data type for tabular ingestion. Defaults to None.
            csv_file_type (IngestCSVDataType, optional): data type of file for CSV ingestion. Defaults to None.
            einstein (bool, optional): share data with Einstein tool. Defaults to False.
            output_format (OutputFormat, optional): data output format. Defaults to None.
            project_specific (bool, optional): project specific schema provided. Defaults to False.

        Raises:
            HTTPException: pre-condition failed
        """
        pre_conditions = None
        exception_type = None
                    
        if type == PreCondition.PROJECT_CREATION:
            if len(project) < 3 or len(project) > 63:
                pre_conditions = "Invalid project name '{}'. Allowed number of characters: [3, 63]".format(project)
            elif not bool(re.match(r"^[a-zA-Z0-9-\\.]*$", project)):
                pre_conditions = "Invalid project name '{}'. Allowed characters: letters, numbers, dots (.), and hyphens (-)".format(project)
            elif not bool(re.match(r"^[a-zA-Z0-9]*$", project[0])) or not bool(re.match(r"^[a-zA-Z0-9]*$", project[-1])):
                pre_conditions = "Invalid project name '{}'. Project name must begin and end with a letter or number".format(project)
            elif project.lower() in ['einstein', 'data', 'config', 'sphn-connector-backup']:
                pre_conditions = f"'{project}' is a reserved word and cannot be used as project name"
            elif einstein and not self.config.einstein:
                pre_conditions = f"To activate data sharing with Einstein for project '{project}', the EINSTEIN parameter must be set to 'True' in the .env file"
            elif einstein and output_format not in [OutputFormat.TRIG, OutputFormat.NQUADS]:
                pre_conditions = f"To activate data sharing with Einstein for project '{project}', the parameter 'output_format' must be set to either '{OutputFormat.TRIG.value}' or '{OutputFormat.NQUADS.value}'"
            elif einstein and  project_specific:
                pre_conditions  = f"Project specific data cannot be shared with Einstein. Either set share_with_einstein=False or remove the provided project specific schema"
            if pre_conditions is not None:  
                exception_type = status.HTTP_400_BAD_REQUEST
        elif type != PreCondition.EXT_SCHEMA_UPLOAD:
            if not self.is_project_created(project=project):
                pre_conditions = "Project '{}' has not been created yet. Please configure it at endpoint /Create_project".format(project)
                exception_type = status.HTTP_404_NOT_FOUND

        if type != PreCondition.OTHER and pre_conditions is None:
            if io.BytesIO(content).getbuffer().nbytes == 0:
                pre_conditions = "File '{}' is empty and cannot be ingested. Please upload a file with data".format(filename)
            elif file_type == FileType.RDF:
                if file_class in [FileClass.EXT_QUERIES, FileClass.REPORT_SPARQL_QUERY]:
                    if filename.split('.')[-1] != "rq":
                        pre_conditions = "File '{}' is not a SPARQL file. Expected extension: '.rq'".format(filename)
                elif file_class == FileClass.EXT_TERMINOLOGY:
                    if filename.split('.')[-1] not in ["ttl", "zip"]:
                        pre_conditions = "File '{}' is not of type RDF/ZIP. Expected extensions: '.ttl', '.zip'".format(filename)
                else:
                    if filename.split('.')[-1] != "ttl":
                        pre_conditions = "File '{}' is not of type RDF. Expected extension: '.ttl'".format(filename)
            elif file_type == FileType.JSON:
                if filename.split('.')[-1] != "json":
                    pre_conditions = "File '{}' is not of type JSON".format(filename)
                else:
                    try:
                        json.loads(content)
                    except Exception:
                        pre_conditions = f"Invalid JSON file provided '{filename}'"
            elif file_type == FileType.ZIP and filename.split('.')[-1] != "zip":
                pre_conditions = f"File '{filename}' is not in compressed ZIP format"

            if pre_conditions is not None:
                exception_type = status.HTTP_422_UNPROCESSABLE_ENTITY

        if file_class == FileClass.PRE_CHECK_CONFIG and pre_conditions is None:
            pre_conditions = json_validation(content=content) 
            if pre_conditions is not None:
                exception_type = status.HTTP_422_UNPROCESSABLE_ENTITY
        
        if file_class == FileClass.DE_IDENTIFICATION_CONFIG and pre_conditions is None:
            pre_conditions = de_identification_validation(content=content) 
            if pre_conditions is not None:
                exception_type = status.HTTP_422_UNPROCESSABLE_ENTITY
        
        if type == PreCondition.OTHER and tabular_type is not None and pre_conditions is None:
            if tabular_type == TabularDataType.CSV:
                if csv_file_type == IngestCSVDataType.CSV and filename.split('.')[-1] != "csv":
                    pre_conditions = f"File '{filename}' is not a CSV file"
                    exception_type = status.HTTP_422_UNPROCESSABLE_ENTITY
                elif csv_file_type == IngestCSVDataType.ZIP:
                    if filename.split('.')[-1] != "zip":
                        pre_conditions = f"File '{filename}' is not a ZIP file"
                        exception_type = status.HTTP_422_UNPROCESSABLE_ENTITY
                    else:
                        bytes_buffer = io.BytesIO(content)
                        with zipfile.ZipFile(bytes_buffer) as zip_ref:
                            for zipinfo in zip_ref.infolist():
                                if zipinfo.filename.split('.')[-1] != "csv":
                                    pre_conditions = f"File '{os.path.basename(zipinfo.filename)}' in zipped archive is not a CSV file"
                                    exception_type = status.HTTP_422_UNPROCESSABLE_ENTITY
            elif tabular_type == TabularDataType.EXCEL and filename.split('.')[-1] != "xlsx":
                pre_conditions = f"File '{filename}' is not an Excel file"
                exception_type = status.HTTP_422_UNPROCESSABLE_ENTITY

        if pre_conditions is not None:
            raise HTTPException(status_code=exception_type, detail=pre_conditions)

    @staticmethod
    def _get_db_schema_prop_type(type: str, format: str, pattern: str) -> str:
        """Converts JSON type to Postgres type

        Args:
            type (str): JSON type
            format (str): format of string type if existing
            pattern (str): pattern of the string

        Returns:
            str: Postgres type
        """
        if type == "string":
            if format == 'date-time':
                return "TIMESTAMPTZ"
            elif format == 'time':
                return "TIMETZ"
            elif format == 'date':
                return 'DATE'
            elif pattern:
                return "integer"
            else:
                return "varchar(3000)"
        elif type == "number":
            return "numeric"
        else:
            return type

    @staticmethod
    def _extract_columns(col_names: list, 
                         property_map: dict, 
                         ddl_statements: list, 
                         table_name: str, 
                         schema_name: str, 
                         columns_set: set, 
                         required: bool = True):
        """Extract columns and enums creation statements

        Args:
            col_names (list): column names
            property_map (dict): map with property details
            ddl_statements (list): end result with columns and enums information
            table_name (str): name of the table
            schema_name (str): name of the schema
            required (str): defines if the column is required and needs to be marked as NOT NULL
        """
        if property_map['type'] == 'object':
            if 'oneOf' in property_map:
                for element in property_map['oneOf']:
                    Database._extract_columns(col_names=col_names, property_map=element, ddl_statements=ddl_statements, table_name=table_name, schema_name=schema_name, columns_set=columns_set, required=False)
            else:
                target_concept_defined = "target_concept" in property_map["properties"]
                if target_concept_defined:
                    enums = property_map["properties"]["target_concept"]["enum"]
                    pseudoprefix, classref = niceify_prefix(enums[0])
                    target_reference = classref if pseudoprefix == 'sphn' else pseudoprefix + classref
                for key, value in property_map['properties'].items():
                    key_is_required = key in property_map['required'] and required
                    if target_concept_defined and len(enums) == 1:
                        if key != "target_concept":
                            Database._extract_columns(col_names=col_names + [target_reference] + [key], property_map=value, ddl_statements=ddl_statements, table_name=table_name, schema_name=schema_name, columns_set=columns_set, required=key_is_required)
                    else:
                        Database._extract_columns(col_names=col_names + [key], property_map=value, ddl_statements=ddl_statements, table_name=table_name, schema_name=schema_name, columns_set=columns_set, required=key_is_required)
        elif property_map['type'] == 'array':
            if 'oneOf' in property_map['items']:
                for element in property_map['items']['oneOf']:
                    Database._extract_columns(col_names=col_names, property_map=element, ddl_statements=ddl_statements, table_name=table_name, schema_name=schema_name, columns_set=columns_set, required=False)
            else:
                if property_map['items']['type'] == 'object':
                    for key, value in property_map['items']['properties'].items():
                        key_is_required = key in property_map['items']['required'] and required
                        Database._extract_columns(col_names=col_names + [key], property_map=value, ddl_statements=ddl_statements, table_name=table_name, schema_name=schema_name, columns_set=columns_set, required=key_is_required)
                else:
                    Database._extract_columns(col_names=col_names, property_map=property_map['items'], ddl_statements=ddl_statements, table_name=table_name, schema_name=schema_name, columns_set=columns_set, required=required)
        else:
            name_of_column = '__'.join([col.replace(':', '_') for col in col_names])
            if not table_name.startswith('supporting__') and name_of_column.endswith('__sourceConceptID'):
                return
            if 'enum' in property_map:
                values = ', '.join([f"'{v}'" for v in property_map['enum']])
                type_statement = f'CREATE TYPE "{schema_name}"."{table_name}__{name_of_column}_type" AS ENUM ({values});'
                col_type = f'"{schema_name}"."{table_name}__{name_of_column}_type"'
            else:
                type_statement = None
                col_type =  Database._get_db_schema_prop_type(type=property_map['type'], format=property_map.get('format'), pattern=property_map.get('pattern'))
            not_null_constraint = " NOT NULL" if required else ""
            if name_of_column in columns_set:
                # Column is already defined (special condition when multiple target concepts grouped in different objects)
                # If no types are defined we skip the column instantiation
                if type_statement is not None:
                    # If types are defined, we add the values to the existing type
                    for element in ddl_statements:
                        if element['column_ddl'].startswith(f'"{name_of_column}"'):
                            if element['type_ddl'] is not None:
                                # Make sure new enum values are not already defined in existing type and add them
                                values_to_add = [value for value in property_map['enum'] if f"'{value}'" not in element['type_ddl']]
                                if values_to_add:
                                    values_to_add = ', '.join([f"'{v}'" for v in values_to_add])
                                    element['type_ddl'] = element['type_ddl'].replace(');', f", {values_to_add});")
                                    break
                            else:
                                # It the column exists already but does not have a type defined, then we do not want to restrict it to the collected enum values
                                break
                else:
                    # If no type is defined in this case, we check if there already exist a column where a type is defined. In that case we removed it and define the column without a type
                    for element in ddl_statements:
                        if element['column_ddl'].startswith(f'"{name_of_column}"') and element['type_ddl'] is not None:
                            ddl_statements.pop(element)
                            ddl_statements.append({'column_ddl': f'"{name_of_column}" {col_type}{not_null_constraint}', 'type_ddl': type_statement})
                            break
            else:
                columns_set.add(name_of_column)
                ddl_statements.append({'column_ddl': f'"{name_of_column}" {col_type}{not_null_constraint}', 'type_ddl': type_statement})

    def save_ddl_statements(self, project_name: str, columns: list, types: list):
        """Push creation statements of tables and enum types to database table ddl_generation

        Args:
            project_name (str): name of the project
            columns (list): list of columns creation statements
            types (list): list of Enum types creation statements
        """
        self.create_connection()
        timestmp = datetime.now()
        db_sql = "INSERT INTO ddl_generation (project_name, data_provider_id, ddl_statements, object_type, timestmp) VALUES (%s, %s, %s, %s, %s)"
        if columns:
            self.cursor.execute(db_sql, (project_name, self.config.data_provider_id, '\n'.join(columns), 'TABLE', timestmp))
        if types:
            self.cursor.execute(db_sql, (project_name, self.config.data_provider_id, '\n'.join(types), 'TYPE', timestmp))
        self.close_connection()

    def schema_exists(self, schema_name: str) -> bool:
        """Checks if the schema exists on database

        Args:
            schema_name (str): name of the schema

        Returns:
            bool: schema exists or not
        """
        self.create_connection(database_name='sphn_tables')
        sql = "SELECT EXISTS(SELECT 1 FROM pg_namespace WHERE nspname = %s);"
        self.cursor.execute(sql,(schema_name,))
        response = self.cursor.fetchone()
        self.close_connection()
        return response[0]

    def create_schema(self, schema_name: str):
        """Generates project specific schema

        Args:
            schema_name (str): name of the schema
        """
        schema_exists = self.schema_exists(schema_name=schema_name)
        self.create_connection(database_name='sphn_tables')
        if schema_exists:
            self.cursor.execute(f'DROP SCHEMA "{schema_name}" CASCADE')
            self.cursor.execute(f'CREATE SCHEMA "{schema_name}"')
        else:
            self.cursor.execute(f'CREATE SCHEMA "{schema_name}"')
        self.close_connection()

    @staticmethod
    def _generate_ddl_creation_statements(ddl_statements: dict, 
                                          json_content: list, 
                                          schema_name: str, 
                                          supporting_concepts: list,
                                          SPHN_ONLY_sphn_concepts: list, 
                                          data_provider_class: str, 
                                          project_extended_prefix: str):
        """Generate DDL creation statements

        Args:
            ddl_statements (dict): DDL statements
            json_content (list): JSON schema content
            schema_name (str): name of the schema
            supporting_concepts (list): supporting concepts
            data_provider_class (str): data provider class name
            project_extended_prefix (str): prefix of project extended tables
        """
        if SPHN_ONLY_sphn_concepts:
            standard_concepts = SPHN_ONLY_sphn_concepts
            project_extended_concepts = json_content
        else:
            standard_concepts = json_content
            project_extended_concepts = []

        for core_content in standard_concepts:
            core_ddl_statements = []
            for core_key, core_value in core_content.items():
                table_name = core_key.replace(':', '_')
                Database._extract_columns(col_names=[], property_map=core_value, ddl_statements=core_ddl_statements, table_name=table_name, schema_name=schema_name, columns_set=set())
                ddl_statements[table_name] = core_ddl_statements

        for core_content in project_extended_concepts:
            core_ddl_statements = []
            if core_content not in standard_concepts:
                for core_key, core_value in core_content.items():
                    table_name = core_key.replace(':', '_')
                    if core_key.split(':')[-1] not in [data_provider_class, 'SubjectPseudoIdentifier', 'DataRelease']:
                        table_name = project_extended_prefix + table_name
                    Database._extract_columns(col_names=[], property_map=core_value, ddl_statements=core_ddl_statements, table_name=table_name, schema_name=schema_name, columns_set=set())
                    ddl_statements[table_name] = core_ddl_statements
        
        # Problem here is that we for sure don't have the sphn:hasSubjectPesudoIdentifier. The sphn:hasDataProvider could be present e.g. sphn:SemanticMapping 
        # I assume we cannot distinguish the patients therefore I think all the supporting concepts should be displayed for every patient (special logic in database_extractor.py)
        for supporting_concept in supporting_concepts:
            supporting_ddl_statements = []
            for concept_key, concept_value in supporting_concept.items():
                table_name = "supporting__" + concept_key.replace(':', '_')
                column_set = set()
                Database._extract_columns(col_names=[], property_map=concept_value, ddl_statements=supporting_ddl_statements, table_name=table_name, schema_name=schema_name, columns_set=column_set)
                ddl_statements[table_name] = supporting_ddl_statements

    def execute_ddl_creation_statements(self, 
                                        ddl_statements: dict, 
                                        schema_name: str, 
                                        supporting_tables_with_dpi: list, 
                                        data_provider_class: str, 
                                        is_legacy: bool,
                                        project_extended_prefix: str) -> Union[list, list]:        
        """Execute DDL creation statements and return list of statements

        Args:
            ddl_statements (dict): DDL statements
            schema_name (str): name of the schema
            supporting_tables_with_dpi (list): supporting concepts with link to DataProvider
            data_provider_class (str): data provider class
            is_legacy (bool): schema is older than 2024
            project_extended_prefix (str): prefix of project extended tables

        Returns:
            [list, list]: list of table creation statements and type creation statements
        """
        self.create_connection(database_name='sphn_tables')
        default_columns = ['"sphn_hasSubjectPseudoIdentifier__id" varchar(3000) NOT NULL', f'"sphn_has{data_provider_class}__id" varchar(3000) NOT NULL']
        supporting_concept_default = ['"patient_id" varchar(3000)', f'"sphn_has{data_provider_class}__id" varchar(3000) NOT NULL']
        column_statements = []
        type_statements = []
        for table_name, values in ddl_statements.items():
            if table_name.split('_')[-1] not in [data_provider_class, 'SubjectPseudoIdentifier']:
                if not is_legacy and table_name.replace(project_extended_prefix, '') in supporting_tables_with_dpi + ['sphn_SourceSystem']:
                    column_definitions = ', '.join(sorted(supporting_concept_default + list(set([element['column_ddl'] for element in values]))))
                elif not is_legacy and table_name.replace(project_extended_prefix, '').startswith('supporting__'):
                    column_definitions = ', '.join(sorted(supporting_concept_default[:1] + list(set([element['column_ddl'] for element in values]))))
                else:
                    column_definitions = ', '.join(sorted(default_columns + list(set([element['column_ddl'] for element in values]))))
            elif table_name.split('_')[-1] == 'SubjectPseudoIdentifier':
                column_definitions = ', '.join(sorted([f'"sphn_has{data_provider_class}__id" varchar(3000) NOT NULL'] + list(set([element['column_ddl'] for element in values]))))
            else:
                column_definitions = ', '.join(sorted(list(set([element['column_ddl'] for element in values]))))
            type_definitions = list(set([element['type_ddl'] for element in values if element['type_ddl'] is not None]))
            if type_definitions:
                type_statements += type_definitions
                for type_statement in type_definitions:
                    self.cursor.execute(type_statement)
            if table_name.replace(project_extended_prefix, '') == 'sphn_DataRelease':
                primary_key_constraint = f', PRIMARY KEY ("sphn_hasSubjectPseudoIdentifier__id", "sphn_has{data_provider_class}__id")'
            elif table_name.replace(project_extended_prefix, '') in [f'sphn_{data_provider_class}']:
                primary_key_constraint = ", PRIMARY KEY (id)"
            else:
                primary_key_constraint = ''
            sql = f'CREATE TABLE "{schema_name}"."{table_name}" ({column_definitions}{primary_key_constraint});'
            column_statements.append(sql)
            self.cursor.execute(sql)
        self.close_connection()
        return sorted(column_statements), sorted(type_statements)

    def ddl_generation(self, project_name: str):
        """Generates DDL for project

        Args:
            project_name (str): name of the project
        """
        sphn_base_iri = self.get_sphn_base_iri(project=project_name)
        data_provider_class = Database._get_data_provider_class(sphn_base_iri=sphn_base_iri)
        is_legacy = Database._is_legacy_schema(sphn_base_iri=sphn_base_iri)
        ddl_statements = {}
        supporting_tables_with_dpi = Database._get_tables_with_dpi(project_name=project_name)
        project_extended_prefix = self.get_project_extended_prefix(project=project_name)
        # Get JSON schema
        bucket = self.config.s3_connector_bucket
        json_schema_name = f'{project_name}/RML/{project_name}_json_schema.json'
        response = self.config.s3_client.get_object(Bucket=bucket, Key=json_schema_name)['Body'].read().decode('utf8')
        json_schema = json.loads(response)

        try:
            SPHN_ONLY_json_schema_name = f'{project_name}/RML/SPHN_ONLY_{project_name}_json_schema.json'
            SPHN_ONLY_response = self.config.s3_client.get_object(Bucket=bucket, Key=SPHN_ONLY_json_schema_name)['Body'].read().decode('utf8')
            SPHN_ONLY_json_schema = json.loads(SPHN_ONLY_response)
            sphn_only = True
        except Exception:
            sphn_only = False
        
        # Create/Drop database schema
        self.create_schema(schema_name=project_name)

        # Extract creation statements
        sphn_concepts = []
        supporting_concepts = []
        SPHN_ONLY_sphn_concepts = []
        for property in json_schema['properties']:
            if property.split(':')[-1] in [data_provider_class, 'SubjectPseudoIdentifier', 'DataRelease']:
                sphn_concepts += [{property: json_schema['properties'][property]}]
        for key, value in json_schema['properties']['content']['properties'].items():
            sphn_concepts += [{key: value['items']}]
        if 'supporting_concepts' in json_schema['properties']:
            for key, value in json_schema['properties']['supporting_concepts']['properties'].items():
                supporting_concepts += [{key: value['items']}]

        if sphn_only:
            for key, value in SPHN_ONLY_json_schema['properties']['content']['properties'].items():
                SPHN_ONLY_sphn_concepts += [{key: value['items']}]
        Database._generate_ddl_creation_statements(ddl_statements=ddl_statements, json_content=sphn_concepts, schema_name=project_name, supporting_concepts=supporting_concepts,
                                                   SPHN_ONLY_sphn_concepts=SPHN_ONLY_sphn_concepts, data_provider_class=data_provider_class, project_extended_prefix=project_extended_prefix)

        # Create tables
        column_statements, type_statements = self.execute_ddl_creation_statements(ddl_statements=ddl_statements, schema_name=project_name, supporting_tables_with_dpi=supporting_tables_with_dpi,
                                                                                  data_provider_class=data_provider_class, is_legacy=is_legacy, project_extended_prefix=project_extended_prefix)

        self.save_ddl_statements(project_name=project_name, columns=column_statements, types=type_statements)

    def download_ddl_statements(self, project: str, compression_type: CompressionType) -> StreamingResponse:
        """Downloads DDL statements in a ZIP file

        Args:
            project (str): name of the project
            compression_type (Enum): compression type

        Raises:
            HTTPException: exception if no DDL are found

        Returns:
            StreamingResponse: ZIP file with DDL statements
        """
        self.create_connection()
        table_sql = "SELECT ddl_statements, timestmp FROM ddl_generation WHERE project_name=%s AND data_provider_id=%s AND object_type='TABLE' AND timestmp IN (SELECT MAX(timestmp) FROM ddl_generation WHERE project_name=%s AND data_provider_id=%s AND object_type='TABLE')"
        enum_sql = "SELECT ddl_statements, timestmp FROM ddl_generation WHERE project_name=%s AND data_provider_id=%s AND object_type='TYPE' AND timestmp IN (SELECT MAX(timestmp) FROM ddl_generation WHERE project_name=%s AND data_provider_id=%s AND object_type='TYPE')"
        self.cursor.execute(table_sql, (project, self.config.data_provider_id, project, self.config.data_provider_id))
        table_result = self.cursor.fetchone()
        table_statements = table_result[0] if table_result else None
        table_timestamp = table_result[1].strftime("%Y-%m-%dT%H-%M-%S") if table_result else None
        self.cursor.execute(enum_sql, (project, self.config.data_provider_id, project, self.config.data_provider_id))
        enum_result = self.cursor.fetchone()
        enum_statements = enum_result[0] if enum_result else None
        enum_timestamp = table_result[1].strftime("%Y-%m-%dT%H-%M-%S") if enum_result else None
        self.close_connection()

        if table_statements is not None or enum_statements is not None:
            bytes_buffer = BytesIO()
            if table_timestamp is not None:
                zip_filename = f"{project}_DDL_statements_{table_timestamp}{compression_type.value}"
            else:
                zip_filename = f"{project}_DDL_statements_{enum_timestamp}{compression_type.value}"
            if compression_type == CompressionType.ZIP:
                with zipfile.ZipFile(bytes_buffer, mode='w', compression=zipfile.ZIP_DEFLATED) as zip:
                    if table_statements is not None:
                        zip.writestr(f'{project}_tables_DDL.sql', table_statements.encode('utf_8'))
                    if enum_statements is not None:        
                        zip.writestr(f'{project}_types_DDL.sql', enum_statements.encode('utf_8'))
                    zip.close()
            elif compression_type == CompressionType.TAR:
                tar_file_obj = tarfile.open(mode = "w:gz", fileobj=bytes_buffer) 
                info = tarfile.TarInfo(f'{project}_tables_DDL.sql')
                fpath_obj = io.BytesIO(table_statements.encode('utf_8'))
                info.size = fpath_obj.getbuffer().nbytes
                tar_file_obj.addfile(info, fpath_obj)
                info = tarfile.TarInfo(f'{project}_types_DDL.sql')
                fpath_obj = io.BytesIO(enum_statements.encode('utf_8'))
                info.size = fpath_obj.getbuffer().nbytes
                tar_file_obj.addfile(info, fpath_obj)
                tar_file_obj.close()
            return StreamingResponse(
                iter([bytes_buffer.getvalue()]),
                media_type="application/x-zip-compressed",
                headers = { "Content-Disposition": f"attachment; filename={zip_filename}"}
            )
        else:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"No DDL creation statements found for project '{project}'. Make sure it has been successfully initialized")

    def update_rml_json_logging(self, project_name: str, message: str, status: RML_JSON_LOGS):
        """Updates / Creates a status for a specific rml operation for a specific project

        Args:
            project_name (str): name of the project
            message (str): message to be incerted into the pipeline
            status (RML_JSON_LOGS): status
        """
        self.create_connection()
        sql = "INSERT INTO rml_json_logs (project_name, data_provider_id, status, message, timestmp) VALUES \
                (%s, %s, %s, %s, %s) \
                ON CONFLICT (project_name, data_provider_id, status) DO UPDATE SET message=EXCLUDED.message, timestmp=EXCLUDED.timestmp"
        self.cursor.execute(sql, (project_name, self.config.data_provider_id, status.value, message, datetime.now()))
        self.close_connection()

    def download_init_files(self, project: str, compression_type: CompressionType) -> StreamingResponse:
        """Downloads Log files for Shacler and the RML & JSON step in a ZIP file

        Args:
            project (str): name of the project
            compression_type (CompressionType): compression type

        Raises:
            HTTPException: exception if no Shacler, json and rml logs  are found

        Returns:
            StreamingResponse: ZIP file with Shacler, json and rml logs statements
        """
        
        logging_messages_shacler = ""
        logging_messages_json_rml = ""
        self.create_connection()
        shacler = "SELECT status, timestmp, message from shacler_logging WHERE project_name = %s order by timestmp ASC"
        self.cursor.execute(shacler, (project,))
        messages_shacler = self.cursor.fetchall()
        for message in messages_shacler:
            timestmp = message[1].strftime("%d-%m-%Y %H:%M:%S")
            logging_messages_shacler += "{} [{}]: -- {}\n".format(message[0].upper().ljust(30), timestmp, message[2])
        self.close_connection()

        self.create_connection()
        json_rml ="SELECT status, timestmp, message from rml_json_logs WHERE project_name = %s order by timestmp ASC"
        self.cursor.execute(json_rml, (project,))
        messages_json_rml = self.cursor.fetchall()
        for message in messages_json_rml:
            timestmp = message[1].strftime("%d-%m-%Y %H:%M:%S")
            logging_messages_json_rml += "{} [{}]: {}\n".format(message[0].upper().ljust(30), timestmp, message[2])
        self.close_connection()


        if logging_messages_shacler is not None or logging_messages_json_rml is not None:        
            bytes_buffer = BytesIO()
            if compression_type == CompressionType.ZIP:
                with zipfile.ZipFile(bytes_buffer, mode='w', compression=zipfile.ZIP_DEFLATED) as zip:
                    if logging_messages_shacler is not None:
                        zip.writestr(f"{project}_shacler_output.txt", logging_messages_shacler.encode("utf_8"))
                    if logging_messages_json_rml is not None:
                        zip.writestr(f"{project}_json_rml.txt", logging_messages_json_rml.encode("utf_8"))
                    zip.close()
            elif compression_type == CompressionType.TAR:
                tar_file_obj = tarfile.open(mode = "w:gz", fileobj=bytes_buffer)
                if logging_messages_shacler is not None:
                    info = tarfile.TarInfo(f"{project}_shacler_output.txt")
                    fpath_obj = io.BytesIO(logging_messages_shacler.encode("utf_8"))
                    info.size = fpath_obj.getbuffer().nbytes
                    tar_file_obj.addfile(info, fpath_obj)
                if logging_messages_json_rml is not None:  
                    info = tarfile.TarInfo(f"{project}_json_rml.txt")
                    fpath_obj = io.BytesIO(logging_messages_json_rml.encode("utf_8"))
                    info.size = fpath_obj.getbuffer().nbytes
                    tar_file_obj.addfile(info, fpath_obj)
                tar_file_obj.close()

            return StreamingResponse(
                    iter([bytes_buffer.getvalue()]),
                    media_type="application/x-zip-compressed",
                    headers = { "Content-Disposition": f"attachment; filename={project}_Shacler_JSON_RML_logs{compression_type.value}"}
                )
        else:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"No init logs found for project '{project}'")

    @staticmethod
    def _split_list_into_chunks(object_names: list, chunk_size: int =1000) -> list:
        """Split list in chunks

        Args:
            object_names (list): list of objects
            chunk_size (int, optional): chunk size. Defaults to 1000.

        Returns:
            list: list of chunks
        """
        return [object_names[i:i + chunk_size] for i in range(0, len(object_names), chunk_size)]

    def cleanup_objects(self, project_name: str, patient_id: str, parallel: bool = False):
        """Removes all objects for a patient in all data persistence zones

        Args:
            project_name (str): name of the project
            patient_id (str): ID of the patient
            parallel (bool): in parallel execution
        """
        object_names = []
        bucket = self.config.s3_connector_bucket
        if not parallel:
            self.create_connection()
        for persistence_layer in DataPersistenceLayer:
            sql = f"SELECT object_name FROM {persistence_layer.value} WHERE project_name=%s AND data_provider_id=%s AND patient_id=%s"
            self.cursor.execute(sql, (project_name, self.config.data_provider_id, patient_id))
            record = self.cursor.fetchone()
            if record is not None:
                object_names.append(record[0].strip())
        if not parallel:
            self.close_connection()

        if object_names:
            for chunk in Database._split_list_into_chunks(object_names=object_names):
                objects_to_delete = {"Objects":[]}
                objects_to_delete["Objects"] = [{"Key" : k} for k in chunk]
                if objects_to_delete['Objects']:
                    try:
                        self.config.s3_client.delete_objects(Bucket=bucket, Delete=objects_to_delete)
                    except Exception:
                        print(traceback.print_exc())
                        pass

    def cleanup_database_data(self, project_name: str, patient_id: str, parallel: bool = False):
        """Cleanup patient data in database tables

        Args:
            project_name (str): name of the project
            patient_id (str): ID of the patient
            parallel (bool): in parallel execution
        """
        if not parallel:
            self.create_connection()
        # If data has been reingested into the Connector we need to adjust the counts for the Grafana dashboard:
        found_in_layer = self.check_if_reingested(project=project_name,patient_id=patient_id)
        if found_in_layer:
            decrease_final_count = self.return_update_query(result_set=found_in_layer)
            self.cursor.execute(decrease_final_count, (project_name, self.config.data_provider_id))
        for persistence_layer in DataPersistenceLayer:
            sql = f"DELETE FROM {persistence_layer.value} WHERE project_name = %s AND patient_id = %s AND data_provider_id=%s"
            self.cursor.execute(sql, (project_name, patient_id, self.config.data_provider_id))
        logging_sql = "DELETE FROM logging WHERE project_name = %s AND patient_id = %s AND data_provider_id=%s"
        self.cursor.execute(logging_sql, (project_name, patient_id, self.config.data_provider_id))
        qc_sql = "DELETE FROM quality_checks WHERE project_name = %s AND patient_id = %s AND data_provider_id=%s"
        self.cursor.execute(qc_sql, (project_name, patient_id, self.config.data_provider_id))
        statistics_sqls = ["DELETE FROM sparqler_count_instances WHERE project_name = %s AND patient_id = %s AND data_provider_id=%s",
                           "DELETE FROM sparqler_has_code WHERE project_name = %s AND patient_id = %s AND data_provider_id=%s",
                           "DELETE FROM sparqler_min_max_predicates_extensive WHERE project_name = %s AND patient_id = %s AND data_provider_id=%s",
                           "DELETE FROM sparqler_min_max_predicates_numeric_extensive WHERE project_name = %s AND patient_id = %s AND data_provider_id=%s",
                           "DELETE FROM sparqler_min_max_predicates_fast WHERE project_name = %s AND patient_id = %s AND data_provider_id=%s",
                           "DELETE FROM sparqler_min_max_predicates_numeric_fast WHERE project_name = %s AND patient_id = %s AND data_provider_id=%s",
                           "DELETE FROM sparqler_administrative_case_encounters WHERE project_name = %s AND patient_id = %s AND data_provider_id=%s",
                           "DELETE FROM stats_individual_reports WHERE project_name = %s AND patient_id = %s AND data_provider_id=%s"]
        for statistics_sql in statistics_sqls:
            self.cursor.execute(statistics_sql, (project_name, patient_id, self.config.data_provider_id))
        if not parallel:
            self.close_connection()

    def cleanup_before_ingestion(self, project_name: str, patient_id: str, parallel: bool = False):
        """Cleanup all patient existing DB data and S3 data before ingesting a new patient

        Args:
            project_name (str): name of the project
            patient_id (str): ID of the patient
            parallel (bool): in parallel execution
        """
        self.cleanup_objects(project_name=project_name, patient_id=patient_id, parallel=parallel)
        self.cleanup_database_data(project_name=project_name, patient_id=patient_id, parallel=parallel)

    @staticmethod
    def _get_released_object_name(project: str, input_object: str, output_format: OutputFormat, compression: bool) -> str:
        """Return released object name

        Args:
            project (str): name of the project
            input_object (str): name of graph zone input object
            output_format (OutputFormat): output format
            compression (bool): output files should be compressed

        Returns:
            str: released object name
        """
        target_object_name = f"{project}/Output/{os.path.basename(input_object)}"
        if output_format == OutputFormat.NQUADS:
            target_object_name = target_object_name.replace('.ttl', '.nq')
        elif output_format == OutputFormat.TRIG:
            target_object_name = target_object_name.replace('.ttl', '.trig')
        else:
            target_object_name = target_object_name
        
        if not compression:
            return target_object_name
        else:
            return target_object_name + '.gz'

    def move_to_next_layer(self, 
                           project: str, 
                           patient_id: str, 
                           object_name: str, 
                           source_zone: DataPersistenceLayer, 
                           validated_ok: bool = False, 
                           file_type: FileType = None, 
                           patient_data: bytes = None, 
                           parallel: bool = False, 
                           copy_only: bool = False, 
                           sphn_base_iri: str = None,
                           project_output_format: OutputFormat = None, 
                           compression: bool = False, 
                           einstein_data: dict = None, 
                           anonymized: bool = False, 
                           einstein: bool = False):
        """Moves patient data to next data persistence layer and updates DB data persistence tables accordingly

        Args:
            project (str): name of the project
            patient_id (str): ID of the patient
            object_name (str): name of the object [S3 prefix, local path]
            source_zone (DataPersistenceLayer): source data persistence layer
            validated_ok (bool, optional): result of QAF validation. Defaults to False.
            file_type (FileType, optional): type of the file. Defaults to None.
            patient_data (bytes, optional): patient data
            parallel (bool): in parallel execution
            copy_only (bool): copy file from landing zone to refined zone
            sphn_base_iri (str): SPHN schema base IRI
            project_output_format (OutputFormat): project output format
            compression (bool): project files should be compressed
            einstein_data (dict): map with patients info to send to Einstein
            anonymized (bool): anonymization activated on scrambling de-id rule
            einstein (bool, optional): data sharing with Einstein activated
        """
        bucket = self.config.s3_connector_bucket
        timestmp = datetime.now()
        if not parallel:
            self.create_connection()
        if source_zone == DataPersistenceLayer.LANDING_ZONE:
            target_zone = DataPersistenceLayer.REFINED_ZONE
            if anonymized:
                target_patient_id = self.get_anonymized_patient_id(project=project, patient_id=patient_id)
                extension = f".{os.path.basename(object_name).split('.')[-1]}"
                target_basename = f"{project}_{target_patient_id}_{self.config.data_provider_id}{extension}"
            else:
                target_patient_id = patient_id
                target_basename = os.path.basename(object_name)
            target_object_name = f"{project}/Input/{target_zone.value}/{target_basename}"
            if copy_only:
                copy_source = {"Bucket": bucket, "Key": object_name}
                self.config.s3_client.copy(copy_source, bucket, target_object_name)
            else:
                file_size = io.BytesIO(patient_data).getbuffer().nbytes
                self.config.s3_client.put_object(Bucket=bucket, Key=target_object_name, Body=io.BytesIO(patient_data), ContentLength=file_size)
            s3_file_size = self.config.s3_client.head_object(Bucket=bucket, Key=target_object_name)['ContentLength'] / (1024 * 1024)
            sql = f"INSERT INTO {target_zone.value} (project_name, patient_id, data_provider_id, object_name, timestmp, processed, size) VALUES (%s, %s, %s, %s, %s, False, %s) \
                    ON CONFLICT (project_name, patient_id, data_provider_id) DO UPDATE SET object_name=EXCLUDED.object_name, timestmp=EXCLUDED.timestmp, processed=False, size=EXCLUDED.size"
            self.cursor.execute(sql, (project, target_patient_id, self.config.data_provider_id, target_object_name, timestmp, s3_file_size))
            sql = f"UPDATE {source_zone.value} SET processed=True WHERE project_name=%s AND data_provider_id=%s AND patient_id=%s"
            self.cursor.execute(sql, (project, self.config.data_provider_id, patient_id))
        elif source_zone == DataPersistenceLayer.REFINED_ZONE:
            target_zone = DataPersistenceLayer.GRAPH_ZONE
            target_object_name = f"{project}/Input/{DataPersistenceLayer.GRAPH_ZONE.value}/{project}_{patient_id}_{self.config.data_provider_id}.ttl"
            if file_type == FileType.JSON:
                self.config.s3_client.upload_file(Bucket=bucket, Key=target_object_name, Filename=object_name)
                try:
                    os.remove(object_name)
                except Exception:
                    pass
            else:
                object_name = f"{project}/Input/{DataPersistenceLayer.REFINED_ZONE.value}/{project}_{patient_id}_{self.config.data_provider_id}.ttl"
                copy_source = {"Bucket": bucket, "Key": object_name}
                self.config.s3_client.copy(copy_source, bucket, target_object_name)
            s3_file_size = self.config.s3_client.head_object(Bucket=bucket, Key=target_object_name)['ContentLength'] / (1024 * 1024)
            sql = f"INSERT INTO {target_zone.value} (project_name, patient_id, data_provider_id, object_name, timestmp, processed, size) VALUES (%s, %s, %s, %s, %s, False, %s) \
                    ON CONFLICT (project_name, patient_id, data_provider_id) DO UPDATE SET object_name=EXCLUDED.object_name, timestmp=EXCLUDED.timestmp, processed=False, size=EXCLUDED.size"
            self.cursor.execute(sql, (project, patient_id, self.config.data_provider_id, target_object_name, timestmp, s3_file_size))
            sql = f"UPDATE {source_zone.value} SET processed=True WHERE project_name=%s AND data_provider_id=%s AND patient_id=%s"
            self.cursor.execute(sql, (project, self.config.data_provider_id, patient_id))
        elif source_zone == DataPersistenceLayer.GRAPH_ZONE:
            target_zone = DataPersistenceLayer.RELEASE_ZONE
            target_object_name = Database._get_released_object_name(project=project, input_object=object_name, output_format=project_output_format, compression=compression)
            # Store data in release zone according to the specified format and compression type
            if project_output_format == OutputFormat.TURTLE:
                if not compression:
                    copy_source = {"Bucket": bucket, "Key": object_name}
                    self.config.s3_client.copy(copy_source, bucket, target_object_name)
                else:
                    graph_object = self.config.s3_client.get_object(Bucket=bucket, Key=object_name)["Body"].read()
                    compressed_content = io.BytesIO()
                    with gzip.open(compressed_content, 'wb') as f_out:
                        f_out.write(graph_object)
                    compressed_file_size = compressed_content.getbuffer().nbytes
                    compressed_content.seek(0)
                    self.config.s3_client.put_object(Bucket=bucket, Key=target_object_name, Body=compressed_content, ContentLength=compressed_file_size, ContentEncoding='gzip')
            else:
                graph_object = self.config.s3_client.get_object(Bucket=bucket, Key=object_name)["Body"].read()
                converted_object, graph_named_iri = convert_to_quads(content=graph_object, sphn_base_iri=sphn_base_iri, output_format=project_output_format, 
                                                                     data_provider_id=self.config.data_provider_id, patient_id=patient_id)
                if not compression:
                    file_size = converted_object.getbuffer().nbytes
                    converted_object.seek(0)
                    self.config.s3_client.put_object(Bucket=bucket, Key=target_object_name, Body=converted_object, ContentLength=file_size)
                else:
                    compressed_content = io.BytesIO()
                    with gzip.open(compressed_content, 'wb') as f_out:
                        f_out.write(converted_object.getvalue())
                        
                    compressed_file_size = compressed_content.getbuffer().nbytes
                    compressed_content.seek(0)
                    self.config.s3_client.put_object(Bucket=bucket, Key=target_object_name, Body=compressed_content, ContentLength=compressed_file_size, ContentEncoding='gzip')
                
                if einstein:
                    object_key = f'einstein/data/graph_zone/{os.path.basename(target_object_name)}'
                    if not compression:
                        converted_object.seek(0)
                        object_data = converted_object
                        file_size = file_size
                        self.config.s3_client.put_object(Bucket=self.config.s3_einstein_bucket, Key=object_key, Body=object_data, ContentLength=file_size)
                    else:
                        compressed_content.seek(0)
                        object_data = compressed_content
                        file_size = compressed_file_size
                        self.config.s3_client.put_object(Bucket=self.config.s3_einstein_bucket, Key=object_key, Body=object_data, ContentLength=file_size, ContentEncoding='gzip')
                    response = self.config.s3_client.get_object(Bucket=self.config.s3_einstein_bucket, Key=object_key)
                    einstein_data['patients'][patient_id] = {'object_name': object_key, 'patient_graph': graph_named_iri, 'hash': response['ETag'].strip('"')}

            sql = f"INSERT INTO {target_zone.value} (project_name, patient_id, data_provider_id, object_name, validated_ok, timestmp, downloaded) VALUES (%s, %s, %s, %s, %s, %s, False) \
                    ON CONFLICT (project_name, patient_id, data_provider_id) DO UPDATE SET object_name=EXCLUDED.object_name, validated_ok=EXCLUDED.validated_ok, timestmp=EXCLUDED.timestmp, downloaded=EXCLUDED.downloaded"
            self.cursor.execute(sql, (project, patient_id, self.config.data_provider_id, target_object_name, validated_ok, timestmp))
            if validated_ok:
                sql = f"UPDATE {source_zone.value} SET object_name = %s, timestmp = %s, processed = True WHERE project_name = %s AND patient_id =%s AND data_provider_id = %s"
                self.cursor.execute(sql, (object_name, timestmp, project, patient_id, self.config.data_provider_id))
        if not parallel:
            self.close_connection()

    def get_initialized_projects(self) -> list:
        """Returns list of initialized projects

        Returns:
            list: initialized project names
        """
        self.create_connection()
        sql = "SELECT project_name FROM configuration WHERE initialized = True"
        self.cursor.execute(sql)
        records = self.cursor.fetchall()
        self.close_connection()
        project_names = [record[0] for record in records]
        return project_names

    def grant_permissions(self, project: str = None, user: str = None, password: str = None, user_type: UserType = None):
        """Grant all privileges to the database schema to all API users

        Args:
            project (str, optional): name of the project. Defaults to None.
            user (str, optional): name of the user. Defaults to None.
            password (str, optional): user password. Defaults to None.
            user_type (UserType, optional): user type. Defaults to None.
        """
        if project is not None:
            api_users = list(self.get_credentials(user_type=UserType.INGESTION).keys())
            self.create_connection(database_name='sphn_tables')
            for user in api_users:
                if user == self.config.api_user:
                    sql = "SELECT EXISTS(SELECT 1 FROM pg_catalog.pg_roles WHERE rolname = %s);"
                    self.cursor.execute(sql, (user, ))
                    db_user_exist = self.cursor.fetchone()[0]
                    if not db_user_exist:
                        sql = f'CREATE USER "{user}" WITH PASSWORD %s;'
                        self.cursor.execute(sql, (self.config.api_password, ))
                sql = f'GRANT ALL ON SCHEMA "{project}" TO "{user}"; GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA "{project}" TO "{user}";'
                self.cursor.execute(sql)
            self.close_connection()
        elif user is not None and password is not None:
            project_names = self.get_initialized_projects()
            self.create_connection(database_name='sphn_tables')
            sql = "SELECT EXISTS(SELECT 1 FROM pg_catalog.pg_roles WHERE rolname = %s);"
            self.cursor.execute(sql, (user, ))
            db_user_exist = self.cursor.fetchone()[0]
            if not db_user_exist:
                sql = f'CREATE USER "{user}" WITH PASSWORD %s; GRANT ALL PRIVILEGES ON DATABASE sphn_tables TO "{user}";'
                self.cursor.execute(sql, (password, ))
            for project in project_names:
                sql = f'GRANT ALL ON SCHEMA "{project}" TO "{user}"; GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA "{project}" TO "{user}";'
                self.cursor.execute(sql)
            self.close_connection()
            if not db_user_exist and user_type == UserType.ADMIN:
                self.create_connection()
                sql = f'GRANT ALL PRIVILEGES ON DATABASE "{self.config.postgres_user}" TO "{user}";\
                        GRANT ALL ON SCHEMA "public" TO "{user}"; GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA "public" TO "{user}";'
                self.cursor.execute(sql)
                self.close_connection()

    def revoke_permissions(self, user: str, user_type: UserType):
        """Drops a DB user and its permissions

        Args:
            user (str): user name
            user_type (UserType): user type
        """
        self.create_connection(database_name='sphn_tables')
        sql = "SELECT EXISTS(SELECT 1 FROM pg_catalog.pg_roles WHERE rolname = %s);"
        self.cursor.execute(sql, (user, ))
        db_user_exist = self.cursor.fetchone()[0]
        self.close_connection()
        if db_user_exist:
            self.create_connection(database_name='sphn_tables')
            self.cursor.execute(f'REASSIGN OWNED BY "{user}" to "{self.config.postgres_user}"; DROP OWNED BY "{user}";')
            if user_type != UserType.ADMIN:
                self.cursor.execute(f'DROP USER "{user}";')
            self.close_connection()
            if user_type == UserType.ADMIN:
                self.create_connection()
                self.cursor.execute(f'REASSIGN OWNED BY "{user}" to "{self.config.postgres_user}"; DROP OWNED BY "{user}"; DROP USER "{user}"')
                self.close_connection()

    def is_stats_data_available(self, project: str):
        """Check if there is data in graph_zone for statistics

        Args:
            project (str): name of the project
        """
        self.create_connection()
        sql = "SELECT COUNT(*) FROM graph_zone WHERE project_name=%s AND data_provider_id=%s"
        self.cursor.execute(sql, (project, self.config.data_provider_id))
        count = self.cursor.fetchone()[0]
        if count == 0:
            raise HTTPException(status_code=status.HTTP_412_PRECONDITION_FAILED, detail=f"No data to process found for '{project}'. Make sure necessary steps are executed")
        self.close_connection()

    def is_patient_data_available(self, project: str, patient_id: str, step: Steps, revalidate: bool):
        """Check that non-processed data is available in order to trigger the pipeline

        Args:
            project (str): name of the project
            patient_id (str): ID of the patient
            step (Steps): pipeline step
            revalidate (bool): revalidate all the data

        Raises:
            HTTPException: not-found exception if not data is retrieved
        """
        if revalidate:
            where_condition = ""
        else:
            where_condition = "processed=False AND"
        if step == Steps.PRE_CHECK_DE_ID:
            if patient_id is None:
                sql = "SELECT COUNT(*) FROM landing_zone WHERE processed=False AND project_name=%s AND data_provider_id=%s"
            else:
                sql = "SELECT COUNT(*) FROM landing_zone WHERE processed=False AND patient_id=%s AND project_name=%s AND data_provider_id=%s"
        elif step == Steps.INTEGRATION:
            if patient_id is None:
                sql = "SELECT COUNT(*) FROM refined_zone WHERE processed=False AND project_name=%s AND data_provider_id=%s"
            else:
                sql = "SELECT COUNT(*) FROM refined_zone WHERE processed=False AND patient_id=%s AND project_name=%s AND data_provider_id=%s"
        elif step == Steps.VALIDATION:
            if patient_id is None:
                sql = f"SELECT COUNT(*) FROM graph_zone WHERE {where_condition} project_name=%s AND data_provider_id=%s"
            else:
                sql = f"SELECT COUNT(*) FROM graph_zone WHERE {where_condition} patient_id=%s AND project_name=%s AND data_provider_id=%s"
        elif step == Steps.NOTIFY_EINSTEIN:
            if patient_id is None:
                sql = "SELECT COUNT(*) FROM einstein_data_sharing WHERE project_name=%s AND data_provider_id=%s AND status='ready'"
            else:
                sql = "SELECT COUNT(*) FROM einstein_data_sharing WHERE file_identifier=%s AND project_name=%s AND data_provider_id=%s AND status='ready'"
        elif step == Steps.ALL:
            if patient_id is None:
                sql = f"""SELECT SUM(c) FROM (
                            SELECT COUNT(*) as c FROM landing_zone WHERE processed=False AND project_name=%s AND data_provider_id=%s 
                            UNION ALL SELECT COUNT(*) as c FROM refined_zone WHERE processed=False AND project_name=%s AND data_provider_id=%s
                            UNION ALL SELECT COUNT(*) as c FROM graph_zone WHERE {where_condition} project_name=%s AND data_provider_id=%s
                            UNION ALL SELECT COUNT(*) as c FROM einstein_data_sharing WHERE project_name=%s AND data_provider_id=%s AND status='ready'
                          ) as counts"""
            else:
                sql = f"""SELECT SUM(c) FROM (
                            SELECT COUNT(*) as c FROM landing_zone WHERE processed=False AND patient_id=%s AND project_name=%s AND data_provider_id=%s 
                            UNION ALL SELECT COUNT(*) as c FROM refined_zone WHERE processed=False AND patient_id=%s AND project_name=%s AND data_provider_id=%s 
                            UNION ALL SELECT COUNT(*) as c FROM graph_zone WHERE {where_condition} patient_id=%s AND project_name=%s AND data_provider_id=%s
                            UNION ALL SELECT COUNT(*) as c FROM einstein_data_sharing WHERE file_identifier=%s AND project_name=%s AND data_provider_id=%s AND status='ready'
                          ) as counts"""
        else:
            return
        self.create_connection()
        if step == Steps.ALL:
            if patient_id is None:
                self.cursor.execute(sql, (project, self.config.data_provider_id, project, self.config.data_provider_id, project, self.config.data_provider_id, project, self.config.data_provider_id))
            else:
                self.cursor.execute(sql, (patient_id, project, self.config.data_provider_id, patient_id, project, self.config.data_provider_id, patient_id, project, self.config.data_provider_id, patient_id, project, self.config.data_provider_id))
        else:
            if patient_id is None:
                self.cursor.execute(sql, (project, self.config.data_provider_id))
            else:
                self.cursor.execute(sql, (patient_id, project, self.config.data_provider_id))
        count = self.cursor.fetchone()[0]
        if count == 0:
            raise HTTPException(status_code=status.HTTP_412_PRECONDITION_FAILED, detail=f"No data to process found for '{project}'. Make sure necessary steps are executed")
        self.close_connection()

    def get_user_type(self, user: str) -> UserType:
        """Gets user type

        Args:
            user (str): user name

        Returns:
            UserType: type of the useer
        """
        self.create_connection()
        if user == self.config.api_user:
            user_type = UserType.ADMIN
        else:
            sql = "SELECT user_type FROM api_credentials WHERE api_user=%s"
            self.cursor.execute(sql, (user, ))
            user_type = UserType(self.cursor.fetchone()[0])
        self.close_connection()
        return user_type

    def get_users_map(self, current_user: str) -> dict:
        """Gets map of existing users

        Args:
            current_user (UserInDB): current user

        Returns:
            dict: users map
        """
        admin_users = []
        standard_users = []
        ingestion_users = []
        current_type = self.get_user_type(user=current_user)
        self.create_connection()
        sql = "SELECT api_user FROM api_credentials WHERE user_type=%s"
        
        self.cursor.execute(sql, (UserType.ADMIN.value, ))
        users = self.cursor.fetchall()
        for user in users:
            admin_users.append(user[0])

        self.cursor.execute(sql, (UserType.STANDARD.value, ))
        users = self.cursor.fetchall()
        for user in users:
            standard_users.append(user[0])

        self.cursor.execute(sql, (UserType.INGESTION.value, ))
        users = self.cursor.fetchall()
        for user in users:
            ingestion_users.append(user[0])
        
        self.close_connection()
        if current_type == UserType.STANDARD:
            return {"standard_users": standard_users, "ingestion_users": ingestion_users}
        elif current_type == UserType.ADMIN:
            return {"admin_users": admin_users, "standard_users": standard_users, "ingestion_users": ingestion_users}

    @staticmethod
    def _extract_formats(col_names: list, property_map: dict, formats: dict):
        """Loops through JSON schema and extracts fields with formats

        Args:
            col_names (list): name of columns
            property_map (dict): property map
            formats (dict): formats map
        """
        if property_map['type'] == 'object':
            if 'oneOf' in property_map:
                for element in property_map['oneOf']:
                    Database._extract_formats(col_names=col_names, property_map=element, formats=formats)
            else:
                for key, value in property_map['properties'].items():
                    Database._extract_formats(col_names=col_names + [key], property_map=value, formats=formats)
        elif property_map['type'] == 'array':
            if 'oneOf' in property_map['items']:
                for element in property_map['items']['oneOf']:
                    Database._extract_formats(col_names=col_names, property_map=element, formats=formats)
            else:
                if property_map['items']['type'] == 'object':
                    for key, value in property_map['items']['properties'].items():
                        Database._extract_formats(col_names=col_names + [key], property_map=value, formats=formats)
                else:
                    Database._extract_formats(col_names=col_names, property_map=property_map['items'], formats=formats)
        else:
            name_of_column = '/'.join([col for col in col_names])
            if 'format' in property_map:
                column_type = property_map['format']
                if name_of_column not in formats[column_type]:
                    formats[column_type].append(name_of_column)
            if 'pattern' in property_map:
                pattern = property_map['pattern']
                if pattern == "^(-)?[0-9][0-9][0-9][0-9](Z|[+-]14:00|[+-](?:[0][0-9]|[1][0-3]):[0-5][0-9])?$" and name_of_column not in formats['gYear']:
                    formats['gYear'].append(name_of_column)
                elif pattern == "^(--[0][1-9]|--[1][0-2])(Z|[+-]14:00|[+-](?:[0][0-9]|[1][0-3]):[0-5][0-9])?$" and name_of_column not in formats['gMonth']:
                    formats['gMonth'].append(name_of_column)
                elif pattern == "^(---[0][1-9]|---[1][0-9]|---[2][0-9]|---[3][0-1])(Z|[+-]14:00|[+-](?:[0][0-9]|[1][0-3]):[0-5][0-9])?$" and name_of_column not in formats['gDay']:
                    formats['gDay'].append(name_of_column)

    def extract_formats_mapping(self, project: str):
        """Extracts string with format from JSON schema and pushes mapping to object storage

        Args:
            project (str): name of the project
        """
        bucket = self.config.s3_connector_bucket
        json_schema_name = f'{project}/RML/{project}_json_schema.json'
        response = self.config.s3_client.get_object(Bucket=bucket, Key=json_schema_name)['Body'].read().decode('utf8')
        json_schema = json.loads(response)
        formats = {'date-time': [], 'date': [], 'time': [], 'gYear': [], 'gMonth': [], 'gDay': []}
        Database._extract_formats(col_names=[], property_map=json_schema, formats=formats)
        formats = json.dumps(formats, indent=4).encode('utf-8')
        file_size = io.BytesIO(formats).getbuffer().nbytes
        object_name = f'{project}/De-Identification/{project}_formats_mapping.json'
        self.config.s3_client.put_object(Bucket=bucket, Key=object_name, Body=io.BytesIO(formats), ContentLength=file_size)
    
    @staticmethod
    def _get_excel_de_id_report(file: io.StringIO) -> bytes:
        """Get Excel report for De-Identification

        Args:
            file (io.StringIO): De-Id data

        Returns:
            bytes: Excel report
        """
        # Create an Excel workbook
        workbook = Workbook()

        # Loop through the data dictionary and write each CSV data to a separate sheet
        sheet_name = 'De-Identification Report'
        
        # Read the CSV data into a list of lists
        data_list = []
        for line in file:
            data_list.append(line.strip().split(','))

        # Create a new sheet in the workbook
        sheet = workbook.create_sheet(title=sheet_name)

        # Write the CSV data to the sheet
        for row in data_list:
            sheet.append(row)

        # Calculate the number of columns and rows in the data
        num_columns = len(data_list[0])
        num_rows = len(data_list)
        table_range = f"A1:{get_column_letter(num_columns)}{num_rows}"
        tab = Table(displayName="de_id_table", ref=table_range)

        # Add a default style with striped rows and banded columns
        style = TableStyleInfo(name="TableStyleMedium9", showFirstColumn=False, showLastColumn=False, showRowStripes=True, showColumnStripes=True)
        tab.tableStyleInfo = style
        sheet.add_table(tab)

        # Autoset the column widths
        for col_cells in sheet.columns:
            max_length = 0
            for cell in col_cells:
                try:
                    if len(str(cell.value)) > max_length:
                        max_length = len(cell.value)
                except Exception:
                    pass
            adjusted_width = (max_length + 2)
            sheet.column_dimensions[col_cells[0].column_letter].width = adjusted_width

        # Remove the default sheet that is created automatically
        workbook.remove(workbook["Sheet"])
            
        buffer = io.BytesIO()
        workbook.save(buffer)
        buffer.seek(0)

        return buffer.getvalue()

    def get_de_identification_report(self, project: str, output_file_type: DeIdReportType) -> StreamingResponse:
        """Gets De-Identification report

        Args:
            project (str): name of the project
            output_file_type (DeIdReportType): output file type

        Raises:
            HTTPException: no report found

        Returns:
            StreamingResponse: De-Identification zip file
        """
        self.create_connection()
        sql = f"SELECT COUNT(*) FROM de_identification WHERE project_name='{project}'"
        self.cursor.execute(sql)
        count = self.cursor.fetchone()[0]
        self.close_connection()
        bytes_buffer = BytesIO()
        zip_filename = f"{project}_de_identification.zip"
        if count == 0:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"No De-Identification report found for project '{project}'")
        else:
            with io.StringIO() as file:
                sql = f"COPY (SELECT * FROM de_identification WHERE project_name='{project}' AND data_provider_id='{self.config.data_provider_id}' ORDER BY timestmp ASC) TO STDOUT WITH CSV HEADER"
                self.create_connection()
                self.cursor.copy_expert(sql, file)
                self.close_connection()
                file.seek(0)
                with zipfile.ZipFile(bytes_buffer, mode='w', compression=zipfile.ZIP_DEFLATED) as zip_file:
                    # Save De-Identification report in specified format
                    if output_file_type == DeIdReportType.CSV:
                        with zip_file.open(f'{project}_de_identification_report.csv', 'w') as csv_in_zip:
                            for line in file:
                                csv_in_zip.write(line.encode('utf-8'))
                    else:
                        report = Database._get_excel_de_id_report(file=file)
                        zip_file.writestr(f'{project}_de_identification_report.xlsx', report)
                    
                    # Save SQL insert statements
                    self.write_de_id_insert_statements(project=project, zip_file=zip_file)

                    # Save De-Identification configuration file
                    de_id_config = f"{project}/De-Identification/{project}_de_identification_config.json"
                    response = self.config.s3_client.get_object(Bucket=self.config.s3_connector_bucket, Key=de_id_config)
                    zip_file.writestr('Restore/de_id_config.json', response["Body"].read())
    
        return StreamingResponse(
            iter([bytes_buffer.getvalue()]),
            media_type="application/x-zip-compressed",
            headers = { "Content-Disposition": "attachment; filename={}".format(zip_filename)}
        )  
    
    def purge_tables(self, project: str, file_class: FileClass):
        """Cleans up patient data and monitoring data related to all zones except the landing zone

        Args:
            project (str): name of the project
            file_class (FileClass): class of the file
        """
        self.create_connection()
        cleanup_sqls = ["DELETE FROM release_zone WHERE project_name=%s AND data_provider_id=%s",
                        "DELETE FROM refined_zone WHERE project_name=%s AND data_provider_id=%s",
                        "DELETE FROM graph_zone WHERE project_name=%s AND data_provider_id=%s",
                        "DELETE FROM logging WHERE project_name=%s AND data_provider_id=%s",
                        "DELETE FROM pipeline_history WHERE project_name=%s AND data_provider_id=%s",
                        "DELETE FROM execution_errors WHERE project_name=%s AND data_provider_id=%s",
                        "DELETE FROM sparqler_count_instances WHERE project_name=%s AND data_provider_id=%s",
                        "DELETE FROM sparqler_has_code WHERE project_name=%s AND data_provider_id=%s",
                        "DELETE FROM sparqler_min_max_predicates_extensive WHERE project_name=%s AND data_provider_id=%s",
                        "DELETE FROM sparqler_min_max_predicates_numeric_extensive WHERE project_name=%s AND data_provider_id=%s",
                        "DELETE FROM sparqler_min_max_predicates_fast WHERE project_name=%s AND data_provider_id=%s",
                        "DELETE FROM sparqler_min_max_predicates_numeric_fast WHERE project_name=%s AND data_provider_id=%s",
                        "DELETE FROM sparqler_administrative_case_encounters WHERE project_name=%s AND data_provider_id=%s",
                        "DELETE FROM stats_individual_reports WHERE project_name=%s AND data_provider_id=%s",
                        "DELETE FROM quality_checks WHERE project_name=%s AND data_provider_id=%s",
                        "DELETE FROM real_time_processing WHERE project_name=%s AND data_provider_id=%s",
                        "UPDATE grafana_real_time_report SET patients_processed=0, project_size = 0 WHERE project_name=%s AND data_provider_id=%s AND step in ('Pre-check/De-ID', 'Integration', 'Validation', 'Statistics')"]
        for sql_query in cleanup_sqls:
            self.cursor.execute(sql_query, (project, self.config.data_provider_id))
        
        sql = "UPDATE landing_zone SET processed=False WHERE project_name=%s AND data_provider_id=%s"
        self.cursor.execute(sql, (project, self.config.data_provider_id))
        if file_class == FileClass.EXT_TERMINOLOGY:
            sql = "DELETE FROM external_terminologies WHERE project_name=%s AND data_provider_id=%s"
            self.cursor.execute(sql, (project, self.config.data_provider_id))
        self.close_connection()

    def download_graph_zone_file(self, project: str, patient_id: str) -> Response:
        """Downloads patient file from Graph Zone

        Args:
            project (str): name of the project
            patient_id (str): patient ID

        Returns:
            Response: RDF file
        """
        self.create_connection()
        sql = "SELECT object_name FROM graph_zone WHERE project_name=%s AND patient_id=%s AND data_provider_id=%s"
        self.cursor.execute(sql, (project, patient_id, self.config.data_provider_id))
        object_name = self.cursor.fetchone()
        self.close_connection()
        if object_name is None:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"No file found for project '{project}' and patient '{patient_id}'")
        else:
            bucket = self.config.s3_connector_bucket
            try:
                response = self.config.s3_client.get_object(Bucket=bucket, Key=object_name[0])
            except Exception:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"File '{object_name[0]}' for project '{project}' and patient '{patient_id}' not found in object storage")
            headers = {'Content-Disposition': f'attachment; filename="{object_name[0].split("/")[-1]}"'}
            return Response(response["Body"].read(), headers=headers)

    def update_config_files_history(self, project: str, filename: str, action: ConfigFileAction, file_class: FileClass):
        """Updates table 'config_files_history'

        Args:
            project (str): name of the project
            filename (str): name of the file
            action (ConfigFileAction): config file action
            file_class (FileClass): type of file
        """
        self.create_connection()
        sql = "INSERT INTO config_files_history (project_name, data_provider_id, filename, file_action, file_class, timestmp) VALUES (%s, %s, %s, %s, %s, %s)"
        self.cursor.execute(sql, (project, self.config.data_provider_id, filename, action.value, file_class.value, datetime.now()))
        self.close_connection()

    def get_sphn_table_names(self, project: str) -> list:
        """Extract table names from SPHN_TABLES database for a given project

        Args:
            project (str): name of the project

        Returns:
            list: list of table names
        """
        self.create_connection(database_name='sphn_tables')
        sql = f"SELECT table_name FROM information_schema.tables WHERE table_schema = '{project}'"
        self.cursor.execute(sql)
        table_names = [table_name[0] for table_name in self.cursor.fetchall()]
        self.close_connection()
        return table_names

    def get_csv_templates(self, project: str) -> StreamingResponse:
        """Extract CSV files with the structure of the tables

        Args:
            project (str): name of the project

        Returns:
            StreamingResponse: zip file with CSV files
        """
        table_names = sorted(self.get_sphn_table_names(project=project))
        zip_filename = f"{project}_csv_structure_files.zip"
        bytes_buffer = BytesIO()
        workbook = Workbook()
        Database._create_info_sheet(workbook=workbook, project=project, table_names=table_names, tabular_type=TabularDataType.CSV)
        columns_sheet = workbook.create_sheet(title='Columns')
        self.create_connection(database_name='sphn_tables')
        row_number = 1
        with zipfile.ZipFile(bytes_buffer, mode='w', compression=zipfile.ZIP_DEFLATED) as zip:
            # Generate CSV templates
            for i_table, table_name in enumerate(table_names):
                sql = f"SELECT column_name, data_type FROM information_schema.columns WHERE table_name='{table_name}' AND table_schema='{project}' ORDER BY column_name"
                self.cursor.execute(sql)
                columns_record = self.cursor.fetchall()
                columns = {}
                for col in columns_record:
                    columns[col[0]] = col[1]
                zip.writestr(f"{table_name}.csv", ';'.join([col for col in list(columns.keys())]))
                row_number = self.create_columns_sheet(worksheet=columns_sheet, project=project, columns=columns, table_name=table_name, row_number=row_number, i_table=i_table, tabular_type=TabularDataType.CSV)
            
            # Generate Excel README.xlsx file
            workbook.remove(workbook["Sheet"])
            buffer = io.BytesIO()
            workbook.save(buffer)
            buffer.seek(0)
            zip.writestr("README.xlsx", buffer.getvalue())

            # Generate TXT README.txt file
            buffer = io.StringIO()
            with buffer as txt_file:
                info_sheet = workbook['Info']
                for row in info_sheet.iter_rows(min_row=1, max_row=info_sheet.max_row, min_col=1, max_col=info_sheet.max_column):
                    row_values = [cell.value for cell in row]
                    row_text = '\t'.join(str(value) for value in row_values if value is not None)
                    txt_file.write(row_text + '\n')
                txt_file.write('\n\n')
                for row in columns_sheet.iter_rows(min_row=1, max_row=columns_sheet.max_row, min_col=1, max_col=columns_sheet.max_column):
                    row_values = [cell.value for cell in row]
                    row_text = '\t'.join(str(value).replace('\n', ' ') for value in row_values if value is not None)
                    txt_file.write(row_text + '\n')
                buffer.seek(0)
                zip.writestr("README.txt", buffer.getvalue())

            zip.close()
        self.close_connection()
        return StreamingResponse(
            iter([bytes_buffer.getvalue()]),
            media_type="application/x-zip-compressed",
            headers = { "Content-Disposition": "attachment; filename={}".format(zip_filename)}
        )

    def get_enum_type_values(self, project: str, table_name: str, column: str) -> str:
        """Returns list of enum values for a specific user-defined type

        Args:
            project (str): name of the project
            table_name (str): name of the table
            column (str): column name

        Returns:
            str: concetenated string of enum values
        """
        enum_query = f"SELECT unnest(enum_range(NULL::\"{project}\".\"{table_name}__{column}_type\")) AS enum_value"
        self.cursor.execute(enum_query)
        enum_values_records = self.cursor.fetchall()
        enum_values = [enum_value[0] for enum_value in enum_values_records]
        return ",\n".join(enum_values)

    @staticmethod
    def _create_info_sheet(workbook: Workbook, 
                           project: str, 
                           table_names: list, 
                           tabular_type: TabularDataType, 
                           sphn_base_iri: str = None):
        """Creates Info sheet in Excel template

        Args:
            workbook (Workbook): Excel workbook
            project (str): name of the project
            table_names (list): list of tables
            tabular_type (TabularDataType): tabular type
            sphn_base_iri (str, Optional): SPHN base IRI. Defaults to None.
        """
        info_sheet = workbook.create_sheet(title='Info')
        if tabular_type == TabularDataType.EXCEL:
            info_cell = info_sheet.cell(row=1, column=1, value="Excel template for SPHN Connector ingestion")
        else:
            info_cell = info_sheet.cell(row=1, column=1, value="CSV templates for SPHN Connector ingestion")
        info_sheet.append([])
        font = Font(size=14, bold=True, color='00003366')
        info_cell.font = font
        info_sheet.cell(row=3, column=1, value=f"Project: {project}")
        info_sheet.cell(row=4, column=1, value=f"Number of tables: {len(table_names)}")
        info_sheet.append([])
        if tabular_type == TabularDataType.EXCEL:
            details = """This Excel template can be used to ingest data into the SPHN Connector in an user friendly manner.
The worksheet 'Columns' gives an overview over all the available tables (representing SPHN/project specific concepts), and the corresponding columns and types.
Some columns are associated with a list of allowed values. Only these values must be used to fill the columns in the concept worksheet.
The 'Columns' worksheet also defined the mapping between concept name and the sheet name defined in the template.
All other sheets represents the concepts that needs to be filled with patient data. Make sure that all the data has been ingested before passing the Excel file to the SPHN Connector.

Guidelines to follow:
    - Download template via /Get_excel_template
    - Fill the template manually with the patient data
    - Ingest the filled templates to the SPHN Connector via endpoint /Ingest_excel
    - Trigger /Ingest_from_database endpoint
    - Patient data is not in the landing zone, trigger pipeline via /Start_pipeline

For all the column names representing an IRI value (i.e. column names ending with __iri), the SPHN Connector implicitly replaces a list of prefixes with the corresponding full IRI. 
This allows the user to fill in the data faster and in a compacted way. Prefixes automatically replaced:"""
            prefixes = ["snomed:", "chop:", "geno:", "icd-10-gm:", "loinc:", "so:", "atc:", "hgnc:", "ucum:", "sphn:"]
            replacements = ["http://snomed.info/id/", "https://biomedit.ch/rdf/sphn-resource/chop/", "http://purl.obolibrary.org/obo/GENO_", 
                            "https://biomedit.ch/rdf/sphn-resource/icd-10-gm/", "https://loinc.org/rdf/", "http://purl.obolibrary.org/obo/SO_", 
                            "https://www.whocc.no/atc_ddd_index/?code=", "https://biomedit.ch/rdf/sphn-resource/hgnc/", "https://biomedit.ch/rdf/sphn-resource/ucum/", 
                            f"{sphn_base_iri}#"]
            if sphn_base_iri == 'https://biomedit.ch/rdf/sphn-schema/sphn':
                prefixes += ['eco:', 'emdn:', 'genepio:', 'obi:', 'ordo:', 'efo:', 'edam:']
                replacements += ['http://purl.obolibrary.org/obo/ECO_', 'https://biomedit.ch/rdf/sphn-resource/emdn/', 'http://purl.obolibrary.org/obo/GENEPIO_', 
                                 'http://purl.obolibrary.org/obo/OBI_', 'http://www.orpha.net/ORDO/', 'http://www.ebi.ac.uk/efo/EFO_', 'http://edamontology.org/']
            info_sheet.column_dimensions['A'].width = 190
            for col_num, col_name in enumerate(["Prefix", "Replacement"], 1):
                info_sheet.cell(row=7, column=col_num, value=col_name)
            for row_num, (value1, value2) in enumerate(zip(prefixes, replacements), 8):
                info_sheet.cell(row=row_num, column=1, value=value1)
                info_sheet.cell(row=row_num, column=2, value=value2)
            table = Table(displayName="MyTable", ref=f"A7:B{len(prefixes) + 7}")
            style = TableStyleInfo(name="TableStyleMedium9", showFirstColumn=False, showLastColumn=False, showRowStripes=True, showColumnStripes=True)
            table.tableStyleInfo = style
            info_sheet.add_table(table)
            info_sheet.column_dimensions['B'].width = 50
        else:
            details = f"""This CSV templates can be used to ingest data into the SPHN Connector especially when the load of the data is consistent.
The worksheet 'Columns' gives an overview over all the available tables (representing SPHN/project specific concepts), and the corresponding columns and types.
Some columns are associated with a list of allowed values. Only these values must be used to fill the columns in the concept worksheet.
All other sheets represents the concepts that needs to be filled with patient data. Make sure that all the data has been ingested before passing the Excel file to the SPHN Connector.

Guidelines to follow:
    - Download templates via /Get_csv_templates
    - Fill the template manually with the patient data
    - Ingest the filled templates one by one to the SPHN Connector via endpoint /Ingest_csv
    - Execute the endpoint /Trigger_batch_ingestion
    - Trigger /Ingest_from_database endpoint
    - Patient data is not in the landing zone, trigger pipeline via /Start_pipeline
"""
        info_cell = info_sheet.cell(row=6, column=1, value=details)
        info_cell.alignment = Alignment(wrapText=True)
        info_sheet.column_dimensions['A'].width = 190

    def create_columns_sheet(self, 
                             worksheet: Worksheet , 
                             project: str, 
                             columns: dict, 
                             table_name: str, 
                             row_number: int, 
                             i_table: int, 
                             tabular_type: TabularDataType) -> int:
        """Create columns details Excel worksheet

        Args:
            worksheet (Worksheet): Excel worksheet
            project (str): name of the project
            columns (dict): list of columns
            table_name (str): name of the table
            row_number (int): current row number in Excel sheet
            i_table (int): table number
            tabular_type (TabularDataType): tabular type

        Returns:
            int: current row number
        """
        if tabular_type == TabularDataType.EXCEL:
            worksheet.append(["Table", "Sheet name"])
            worksheet.append([table_name, table_name[:31]])
            table_range = f"A{row_number}:B{row_number + 1}"
        else:
            worksheet.append(["CSV template"])
            worksheet.append([f"{table_name}.csv"])
            table_range = f"A{row_number}:A{row_number + 1}"
        tab = Table(displayName=f"Header{i_table}", ref=table_range)
        style = TableStyleInfo(name="TableStyleMedium9", showFirstColumn=False, showLastColumn=False, showRowStripes=True, showColumnStripes=True)
        tab.tableStyleInfo = style
        worksheet.add_table(tab)
        row_number += 2
        start_row = row_number
        worksheet.append(["Column", "Values/Type"])
        row_number += 1
        for col, col_type in columns.items():
            if col_type != 'USER-DEFINED':
                worksheet.append([col, col_type])
            else:
                cell_1 = worksheet.cell(row=row_number, column=1, value=col)
                cell_1.alignment = Alignment(wrapText=True, vertical="top")
                cell_2 = worksheet.cell(row=row_number, column=2, value=self.get_enum_type_values(project=project, table_name=table_name, column=col))
                cell_2.alignment = Alignment(wrapText=True)
            row_number += 1
        end_row = row_number - 1
        table_range = f"A{start_row}:B{end_row}"
        tab = Table(displayName=f"Columns{i_table}", ref=table_range)
        style = TableStyleInfo(name="TableStyleMedium9", showFirstColumn=False, showLastColumn=False, showRowStripes=True, showColumnStripes=True)
        tab.tableStyleInfo = style
        worksheet.add_table(tab)
        for col_cells in worksheet.columns:
            max_length = 0
            for cell in col_cells:
                try:
                    if len(str(cell.value)) > max_length:
                        max_length = len(cell.value)
                except Exception:
                    pass
            adjusted_width = (max_length + 2)
            worksheet.column_dimensions[col_cells[0].column_letter].width = adjusted_width
        worksheet.append([])
        worksheet.append([])
        row_number += 2
        return row_number

    def create_concept_sheet(self, workbook: Workbook, project: str, columns: list, table_name: str, i_table: int):
        """Creates Excel worksheet per concept

        Args:
            workbook (Workbook): Excel workbook
            project (str): name of the project
            columns (dict): list of columns
            table_name (str): name of the table
            i_table (int): table number
        """
        sheet = workbook.create_sheet(title=table_name[:31])
        
        sheet.append(list(columns.keys()))
        table_range = f"A1:{get_column_letter(len(columns.keys()))}{2}"
        tab = Table(displayName=f"Table{i_table}", ref=table_range)
        style = TableStyleInfo(name="TableStyleMedium9", showFirstColumn=False, showLastColumn=False, showRowStripes=True, showColumnStripes=True)
        tab.tableStyleInfo = style
        sheet.add_table(tab)
        for col_cells in sheet.columns:
            max_length = 0
            for cell in col_cells:
                try:
                    if len(str(cell.value)) > max_length:
                        max_length = len(cell.value)
                except Exception:
                    pass
            adjusted_width = (max_length + 2)
            sheet.column_dimensions[col_cells[0].column_letter].width = adjusted_width

    def get_excel_template(self, project: str) -> Response:
        """Extract Excel template for ingestion

        Args:
            project (str): name of the project

        Returns:
            Response: Excel template
        """
        table_names = sorted(self.get_sphn_table_names(project=project))
        workbook = Workbook()
        sphn_base_iri = self.get_sphn_base_iri(project=project)
        Database._create_info_sheet(workbook=workbook, project=project, table_names=table_names, tabular_type=TabularDataType.EXCEL, sphn_base_iri=sphn_base_iri)
        columns_sheet = workbook.create_sheet(title='Columns')
        row_number = 1
        self.create_connection(database_name='sphn_tables')
        for i_table, table_name in enumerate(table_names):
            sql = f"SELECT column_name, data_type FROM information_schema.columns WHERE table_name='{table_name}' AND table_schema='{project}' ORDER BY column_name"
            self.cursor.execute(sql)
            columns_record = self.cursor.fetchall()
            columns = {}
            for col in columns_record:
                columns[col[0]] = col[1]
            self.create_concept_sheet(workbook=workbook, project=project, columns=columns, table_name=table_name, i_table=i_table)
            row_number = self.create_columns_sheet(worksheet=columns_sheet, project=project, columns=columns, table_name=table_name, row_number=row_number, i_table=i_table, tabular_type=TabularDataType.EXCEL)
        workbook.remove(workbook["Sheet"])
        self.close_connection()
        buffer = io.BytesIO()
        workbook.save(buffer)
        buffer.seek(0)
        response = Response(content=buffer.getvalue(), media_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
        response.headers["Content-Disposition"] = f"attachment; filename={project}_ingestion_template.xlsx"
        return response

    @staticmethod
    def _extract_sheet_concept_mapping(workbook: Workbook) -> dict:
        """Get map between sheet names and table names

        Args:
            workbook (Workbook): Excel workbook

        Returns:
            dict: sheets-concepts map
        """
        worksheet = workbook['Columns']
        concepts_sheets_map = {}
        for row in worksheet.iter_rows(min_row=1, max_row=worksheet.max_row, min_col=1, max_col=worksheet.max_column):
            for cell in row:
                if cell.value == "Sheet name":
                    sheet_name = worksheet.cell(row=cell.row + 1, column=cell.column).value
                    table_name = worksheet.cell(row=cell.row + 1, column=cell.column - 1).value
                    concepts_sheets_map[sheet_name] = table_name
        return concepts_sheets_map
    
    @staticmethod
    def _replace_IRI_prefixes(row: list, column_names: str, sphn_base_iri: str) -> list:
        """Replaces terminologies prefixes with IRI

        Args:
            row (list): row of data
            column_names (str): list of column names
            sphn_base_iri (str): SPHN base IRI

        Returns:
            list: replaced row data
        """
        prefixes_map = {'snomed:': 'http://snomed.info/id/', 'chop:': 'https://biomedit.ch/rdf/sphn-resource/chop/', 
                        'geno:': 'http://purl.obolibrary.org/obo/GENO_', 'icd-10-gm:': 'https://biomedit.ch/rdf/sphn-resource/icd-10-gm/',
                        'loinc:': 'https://loinc.org/rdf/', 'so:': 'http://purl.obolibrary.org/obo/SO_', 'atc:': 'https://www.whocc.no/atc_ddd_index/?code=', 
                        'hgnc:': 'https://biomedit.ch/rdf/sphn-resource/hgnc/', 'ucum:': 'https://biomedit.ch/rdf/sphn-resource/ucum/',
                        'sphn:': f'{sphn_base_iri}#'}
        
        if sphn_base_iri == 'https://biomedit.ch/rdf/sphn-schema/sphn':
            prefixes_map['eco:'] = 'http://purl.obolibrary.org/obo/ECO_'
            prefixes_map['emdn:'] = 'https://biomedit.ch/rdf/sphn-resource/emdn/'
            prefixes_map['genepio:'] = 'http://purl.obolibrary.org/obo/GENEPIO_'
            prefixes_map['obi:'] = 'http://purl.obolibrary.org/obo/OBI_'
            prefixes_map['ordo:'] = 'http://www.orpha.net/ORDO/'
            prefixes_map['efo:'] = 'http://www.ebi.ac.uk/efo/EFO_'
            prefixes_map['edam:'] = 'http://edamontology.org/'

        replaced_row = []
        for col_name, cell in zip(column_names, row):
            if col_name.endswith('__iri') and cell is not None:
                for prefix, iri in prefixes_map.items():
                    if cell.startswith(prefix):
                        cell = cell.replace(prefix, iri)
            replaced_row.append(cell)
        return replaced_row

    def ingest_excel_data(self, project: str, content: bytes):
        """Ingest data from Excel template into database

        Args:
            project (str): name of the project
            content (bytes): data from Excel template

        Raises:
            HTTPException: unprocessable data
        """
        sheet_name = None
        sphn_base_iri = self.get_sphn_base_iri(project=project)
        try:
            workbook = load_workbook(io.BytesIO(content), read_only=True)
            sheet_concept_map = Database._extract_sheet_concept_mapping(workbook=workbook)
            self.create_connection(database_name='sphn_tables')
            for sheet_name in workbook.sheetnames:
                if sheet_name not in ['Info', 'Columns']:
                    sheet = workbook[sheet_name]
                    column_names = [cell.value for cell in sheet[1]]
                    placeholders = ', '.join(['%s'] * len(column_names))
                    columns_list = ",".join([f'"{col}"' for col in column_names])
                    insert_query = f"""INSERT INTO "{project}"."{sheet_concept_map[sheet_name]}" ({columns_list}) VALUES ({placeholders})"""
                    for row in sheet.iter_rows(min_row=2, values_only=True):
                        row = [None if item == "" else item for item in row]
                        if any(value is not None for value in row):
                            row = Database._replace_IRI_prefixes(row=row, column_names=column_names, sphn_base_iri=sphn_base_iri)
                            self.cursor.execute(insert_query, row)
            self.close_connection()
            return f"Excel data ingested into SPHN Connector for project '{project}'"
        except Exception:
            if sheet_name is not None:
                error_message = f"Update of table '{sheet_name}' failed for project '{project}'. Potential wrong data provided (invalid types, not-null constraint violations, ...). Fix the data, reset the project, and reingest the data. Exception: {traceback.format_exc()}"
            else:
                error_message = f"Ingestion of Excel data for project '{project}' failed. Potential wrong data provided (invalid types, not-null constraint violations, ...). Fix the data, reset the project, and reingest the data. Exception: {traceback.format_exc()}"
            raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=error_message)
    
    def ingest_csv_file(self, project: str, object_name: str):
        """Ingest data from CSV file into database

        Args:
            project (str): name of the project
            object_name (str): object name in S3

        Raises:
            HTTPException: unprocessable data
        """
        csv_file = self.config.s3_client.get_object(Bucket=self.config.s3_connector_bucket, Key=object_name)
        bytes_buffer = io.BytesIO(csv_file["Body"].read())
        self.create_connection(database_name='sphn_tables')
        try:
            with bytes_buffer as file:
                table_name = os.path.basename(object_name.rsplit('.', 1)[0])
                columns = ','.join(f'\"{col}\"' for col in file.readline().decode("utf-8-sig").strip().split(';'))
                self.cursor.copy_expert(f"COPY \"{project}\".\"{table_name}\" ({columns}) FROM STDIN WITH DELIMITER ';' CSV", file=file)
        except Exception:
            error_message = f"Update of table '{table_name}' failed for project '{project}'. Potential wrong data provided (invalid types, not-null constraint violations, ...). Fix the data, reset the project, and reingest the data. Exception: {traceback.format_exc()}"
            raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=error_message)
        self.close_connection()
        return f"CSV data ingested into SPHN Connector for project '{project}'"


    def get_patient_list_with_errors(self, project: str, step: Steps, error_level: ErrorLevel) -> dict:
        """Get patient IDs with specified error type

        Args:
            project (str): name of the project
            step (Steps): pipeline step
            error_level (ErrorLevel): error level

        Raises:
            HTTPException: no errors found

        Returns:
            dict: information about patient errors
        """
        self.create_connection()
        sql = f"SELECT run_uuid, patient_id FROM execution_errors WHERE project_name=%s AND data_provider_id=%s AND step=%s AND error_level=%s ORDER BY timestmp DESC"
        self.cursor.execute(sql, (project, self.config.data_provider_id, step.value, error_level.value))
        records = self.cursor.fetchall()
        patient_errors = {"step": step.value, "error_level": error_level.value, "run_uuids": {}}
        for record in records:
            if record[0] not in patient_errors['run_uuids']:
                patient_errors['run_uuids'][record[0]] = {"patient_ids": []}
            patient_errors['run_uuids'][record[0]]['patient_ids'].append(record[1])
        self.close_connection()
        if not patient_errors["run_uuids"]:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"No errors found for project '{project}', step '{step.value}', and error level '{error_level.value}'")
        else:
            return patient_errors
        
    def check_delete_project_permissions(self, project: str, current_user: str):
        """Check that the current user has permissions to delete the project

        Args:
            project (str): name of the project
            current_user (str): current user

        Raises:
            HTTPException: Unauthorized exception
        """
        current_user_type = self.get_user_type(user=current_user)
        if current_user_type != UserType.ADMIN:
            self.create_connection()
            sql = "SELECT api_user from configuration WHERE data_provider_id=%s AND project_name=%s"
            self.cursor.execute(sql, (self.config.data_provider_id, project))
            creation_user = self.cursor.fetchone()[0]
            self.close_connection()
            if current_user != creation_user:
                raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail=f"User '{current_user}' is not authorized to delete project '{project}'. Only the user who created the project and admin users can delete it")
        
    def is_tdb2_up_to_date(self, project: str, parallel: bool = False) -> bool:
        """Check if the TDB2 model is updated with the latest files

        Args:
            project (str): name of the project
            parallel (bool, optional): Called during parallel execution. Defaults to False.

        Returns:
            bool: TDB2 up to date
        """
        if not parallel:
            self.create_connection()
        sql = "SELECT up_to_date FROM tdb2_status WHERE project_name=%s and data_provider_id=%s"
        self.cursor.execute(sql, (project, self.config.data_provider_id))
        up_to_date = self.cursor.fetchone()[0]
        if not parallel:
            self.close_connection()
        return up_to_date
    
    def update_tdb2_status_table(self, project: str, status: bool, parallel: bool = False):
        """_summary_

        Args:
            project (str): name of the project
            status (bool): status of the build
            parallel (bool, optional): run in parallel. Defaults to False.
        """
        if not parallel:
            self.create_connection()
        sql = "INSERT INTO tdb2_status (project_name, data_provider_id, up_to_date) VALUES (%s, %s, %s) ON CONFLICT (project_name, data_provider_id) DO UPDATE SET up_to_date=EXCLUDED.up_to_date"
        self.cursor.execute(sql, (project, self.config.data_provider_id, status))
        if not parallel:
            self.close_connection()

    def update_external_terminologies_table(self, project: str, filename: str, version_iri: str):
        """Update external_terminologies table in the database

        Args:
            project (str): name of the project
            filename (str): name of tge file
            version_iri (str): version IRI of the terminology
        """
        if version_iri is not None:
            sql = "INSERT INTO external_terminologies (project_name, data_provider_id, terminology_name, version_iri) VALUES (%s, %s, %s, %s) ON CONFLICT (project_name, data_provider_id, terminology_name) DO UPDATE SET version_iri=%s"
            self.create_connection()
            self.cursor.execute(sql, (project, self.config.data_provider_id, filename, version_iri, version_iri))
            self.close_connection()
        else:
            print(f"owl:versionIRI not found for file '{filename}'")

    def get_project_terminologies_iri(self, project: str, parallel: bool = False) -> list:
        """Get list of terminologies version IRIs

        Args:
            project (str): name of the project

        Returns:
            list: list of version IRIs
        """
        if not parallel:
            self.create_connection()
        sql = "SELECT version_iri FROM external_terminologies WHERE project_name=%s AND data_provider_id=%s"
        self.cursor.execute(sql, (project, self.config.data_provider_id))
        iris = self.cursor.fetchall()
        if not parallel:
            self.close_connection()
        return list(set([iri[0] for iri in iris]))
    
    def get_terminology_iri(self, project: str, terminology_name: str) -> str:
        """Extract terminology IRI from database

        Args:
            project (str): name of the project
            terminology_name (str): terminology filename

        Returns:
            str: version IRI
        """
        self.create_connection()
        sql = "SELECT version_iri FROM external_terminologies WHERE project_name=%s AND data_provider_id=%s AND terminology_name=%s"
        self.cursor.execute(sql, (project, self.config.data_provider_id, terminology_name))
        record = self.cursor.fetchone()
        self.close_connection()
        if record is not None:
            return record[0]

    @staticmethod
    def _get_report_key_name(zone: DataPersistenceLayer) -> str:
        """Return key for patients reports given the zone

        Args:
            zone (DataPersistenceLayer): data persistence layer

        Returns:
            str: report key name
        """
        if zone == DataPersistenceLayer.LANDING_ZONE:
            return "Pre-check & De-Identification (Pre-check and De-Identification not run or failed)"
        elif zone == DataPersistenceLayer.REFINED_ZONE:
            return "Transformation (not transformed to RDF or failed)"
        elif zone == DataPersistenceLayer.GRAPH_ZONE:
            return "Validation (Validation failed or not run)"
        elif zone == DataPersistenceLayer.RELEASE_ZONE:
            return "Downloaded (not yet downloaded)"

    def set_database_layer_stats(self, project: str, report: dict, extract_ids: bool):
        """Generates report for database stage

        Args:
            project (str): name of the project
            report (dict): patients report
            extract_ids (bool): extract IDs of patients
        """
        sphn_base_iri = self.get_sphn_base_iri(project=project)
        data_provider_class = Database._get_data_provider_class(sphn_base_iri=sphn_base_iri)
        report_key = "Ingested into DB (Patient is ingested into DB but /Ingest_from_database not run)"
        self.create_connection(database_name='sphn_tables')
        sql = f'SELECT count(*) FROM "{project}"."sphn_DataRelease" WHERE "sphn_has{data_provider_class}__id"=\'{self.config.data_provider_id}\''
        self.cursor.execute(sql)
        total = self.cursor.fetchone()[0]
        self.close_connection()
        if total == 0:
            report[report_key] = {
                "total": 0,
                "processed": 0,
                "not-processed": 0
            }
        if extract_ids:
            report[report_key]["patients-not-processed"] = []
        else:
            self.create_connection(database_name='sphn_tables')
            sql = f'SELECT "sphn_hasSubjectPseudoIdentifier__id", "sphn_hasExtractionDateTime" from "{project}"."sphn_DataRelease" where "sphn_has{data_provider_class}__id"=\'{self.config.data_provider_id}\''
            self.cursor.execute(sql)
            records = self.cursor.fetchall()
            released_patients = []
            for record in records:
                released_patients.append((record[0], record[1]))
            self.close_connection()
        
            self.create_connection()
            sql = f"SELECT patient_id, creation_time FROM {DataPersistenceLayer.LANDING_ZONE.value} WHERE project_name='{project}' and creation_time IS NOT NULL"
            self.cursor.execute(sql)
            records = self.cursor.fetchall()
            ingested_patients = []
            for record in records:
                ingested_patients.append((record[0], record[1]))
            self.close_connection()

            not_extracted_patients = []
            for patient in released_patients:
                if patient not in ingested_patients:
                    not_extracted_patients.append(patient)
            if not not_extracted_patients:
                report[report_key]= {
                    "total": total,
                    "processed": len(ingested_patients),
                    "not-processed": 0
                }
                if extract_ids:
                    report[report_key]["patients-not-processed"] = []
            else:
                report[report_key]= {
                    "total": total,
                    "processed": len(ingested_patients),
                    "not-processed": len(not_extracted_patients)
                }
                if extract_ids:
                    report[report_key]["patients-not-processed"] = sorted([record[0] for record in not_extracted_patients])
    
    def set_downloaded_status(self, project: str, report: dict, extract_ids: bool):
        """Generates report for downloaded stage

        Args:
            project (str): name of the project
            report (dict): patients report
            extract_ids (bool): extract IDs of patients
        """
        zone = DataPersistenceLayer.RELEASE_ZONE
        self.create_connection()
        sql = f"SELECT count(*) FROM {zone.value} WHERE downloaded=False AND project_name=%s AND data_provider_id=%s"
        self.cursor.execute(sql, (project, self.config.data_provider_id))
        not_downloaded = self.cursor.fetchone()[0]
        sql = f"SELECT count(*) FROM {zone.value} WHERE downloaded=True AND project_name=%s AND data_provider_id=%s"
        self.cursor.execute(sql, (project, self.config.data_provider_id))
        downloaded = self.cursor.fetchone()[0]
        sql = f"SELECT count(*) FROM {zone.value} WHERE project_name=%s AND data_provider_id=%s"
        self.cursor.execute(sql, (project, self.config.data_provider_id))
        total = self.cursor.fetchone()[0]
        if not_downloaded != 0:
            sql = f"SELECT patient_id FROM {zone.value} WHERE downloaded=False AND project_name=%s AND data_provider_id=%s"
            self.cursor.execute(sql, (project, self.config.data_provider_id))
            not_downloaded_list = [patient[0] for patient in self.cursor.fetchall()]
        else:
            not_downloaded_list = []
        report[Database._get_report_key_name(zone=zone)] = {
                "total": total,
                "downloaded": downloaded,
                "not-downloaded": not_downloaded
            }
        if extract_ids:
            report[Database._get_report_key_name(zone=zone)]["not-downloaded-patients"] = sorted(not_downloaded_list)
        self.close_connection()

    def set_processing_stages(self, project: str, report: dict, extract_ids: bool):
        """Generates report for landing, refined, graph zones

        Args:
            project (str): name of the project
            report (dict): patients report
            extract_ids (bool): extract IDs of patients
        """
        self.create_connection()
        zones = [DataPersistenceLayer.LANDING_ZONE, DataPersistenceLayer.REFINED_ZONE, DataPersistenceLayer.GRAPH_ZONE]
        for zone in zones:
            sql = f"SELECT count(*) FROM {zone.value} WHERE processed=False AND project_name=%s AND data_provider_id=%s"
            self.cursor.execute(sql, (project, self.config.data_provider_id))
            not_processed = self.cursor.fetchone()[0]
            sql = f"SELECT count(*) FROM {zone.value} WHERE processed=True AND project_name=%s AND data_provider_id=%s"
            self.cursor.execute(sql, (project, self.config.data_provider_id))
            processed = self.cursor.fetchone()[0]
            sql = f"SELECT count(*) FROM {zone.value} WHERE project_name=%s AND data_provider_id=%s"
            self.cursor.execute(sql, (project, self.config.data_provider_id))
            total = self.cursor.fetchone()[0]
            if not_processed != 0:
                sql = f"SELECT patient_id FROM {zone.value} WHERE processed=False AND project_name=%s AND data_provider_id=%s"
                self.cursor.execute(sql, (project, self.config.data_provider_id))
                not_processed_list = [patient[0] for patient in self.cursor.fetchall()]
            else:
                not_processed_list = []
            report[Database._get_report_key_name(zone=zone)] = {
                "total": total,
                "processed": processed,
                "not-processed": not_processed
            }
            if extract_ids:
                report[Database._get_report_key_name(zone=zone)]["not-processed-patients"] = sorted(not_processed_list)
        self.close_connection()

    def set_notify_einstein_stage(self, project: str, report: dict, extract_ids: bool):
        """Generates report for Einstein data sharing
        Args:
            project (str): name of the project
            report (dict): patients report
            extract_ids (bool): extract IDs of patients
        """
        if self.is_einstein_activated(project=project) and self.config.einstein:
            self.create_connection()
            sql_total = "SELECT COUNT(*) FROM einstein_data_sharing WHERE project_name=%s AND data_provider_id=%s"
            self.cursor.execute(sql_total, (project, self.config.data_provider_id))
            total = self.cursor.fetchone()[0]
            sql_ready = "SELECT COUNT(*) FROM einstein_data_sharing WHERE project_name=%s AND data_provider_id=%s AND status='ready'"
            self.cursor.execute(sql_ready, (project, self.config.data_provider_id))
            ready = self.cursor.fetchone()[0]
            sql_shared = "SELECT COUNT(*) FROM einstein_data_sharing WHERE project_name=%s AND data_provider_id=%s AND status='shared'"
            self.cursor.execute(sql_shared, (project, self.config.data_provider_id))
            shared = self.cursor.fetchone()[0]
            if ready != 0:
                sql = "SELECT file_identifier FROM einstein_data_sharing WHERE status='ready' AND project_name=%s AND data_provider_id=%s"
                self.cursor.execute(sql, (project, self.config.data_provider_id))
                not_shared = [patient[0] for patient in self.cursor.fetchall()]
            else:
                not_shared = []
            
            report["Notify-Einstein (Notify-Einstein failed or not run)"] = {
                    "total": total,
                    "shared": shared,
                    "not-shared": ready
                }
            if extract_ids:
                report["Notify-Einstein (Notify-Einstein failed or not run)"]["not-shared-patients"] = sorted(not_shared)
            self.close_connection()

    def set_pre_release_stage(self, project: str, report: dict, extract_ids: bool):
        """Generates report for pre-release zone

        Args:
            project (str): name of the project
            report (dict): patients report
            extract_ids (bool): extract IDs of patients
        """
        sphn_base_iri = self.get_sphn_base_iri(project=project)
        data_provider_class = Database._get_data_provider_class(sphn_base_iri=sphn_base_iri)
        report_key = "Pre-Release DB ingestion (Patient is ingested into DB but not released to sphn_DataRelease)"
        project_extended_prefix = self.get_project_extended_prefix(project=project)
        self.create_connection(database_name='sphn_tables')
        self.cursor.execute("SELECT table_name FROM information_schema.tables WHERE table_schema = %s", (project,))
        table_names = [row[0] for row in self.cursor.fetchall() if row[0] not in ['sphn_DataRelease', f'sphn_{data_provider_class}']]
        patient_ids = set()
        for table_name in table_names:
            try:
                if table_name != 'sphn_SubjectPseudoIdentifier':
                    if table_name.startswith('supporting__') or table_name.replace(project_extended_prefix, '') == 'sphn_SourceSystem':
                        sql = f'SELECT DISTINCT "patient_id" FROM "{project}"."{table_name}" WHERE "patient_id" IS NOT NULL'
                    else:
                        sql = f'SELECT DISTINCT "sphn_hasSubjectPseudoIdentifier__id" FROM "{project}"."{table_name}"'
                    self.cursor.execute(sql)
                    patients = self.cursor.fetchall()
                else:
                    sql = f'SELECT DISTINCT "id" FROM "{project}"."{table_name}"'
                    self.cursor.execute(sql)
                    patients = self.cursor.fetchall()
                for patient_id in patients:
                    patient_ids.add(patient_id[0].strip())
            except:
                print(f"Patient IDs extraction failed for table '{table_name}'. Exception: {traceback.print_exc()}")
        patient_ids = list(patient_ids)
        sql = f'SELECT "sphn_hasSubjectPseudoIdentifier__id" from "{project}"."sphn_DataRelease" where "sphn_has{data_provider_class}__id"=\'{self.config.data_provider_id}\''
        self.cursor.execute(sql)
        records = self.cursor.fetchall()
        released_patients = []
        for record in records:
            released_patients.append(record[0])
        self.close_connection()
        not_released_patients = []
        for patient_id in patient_ids:
            if patient_id not in released_patients:
                not_released_patients.append(patient_id)
        report[report_key] = {
                "total": len(patient_ids),
                "released": len(released_patients),
                "not-released": len(not_released_patients)
            }
        if extract_ids:
            report[report_key]["not-released-patients"] = sorted(not_released_patients)

    def get_patients_report(self, project: str, extract_ids: bool) -> Union[dict, Response]:
        """Generates patients report

        Args:
            project (str): name of the project
            extract_ids (bool): extract IDs of patients

        Returns:
            Union[dict, Response]: patients status report
        """
        report = {}
        self.set_pre_release_stage(project=project, report=report, extract_ids=extract_ids)
        self.set_database_layer_stats(project=project, report=report, extract_ids=extract_ids)
        self.set_processing_stages(project=project, report=report, extract_ids=extract_ids)
        self.set_notify_einstein_stage(project=project, report=report, extract_ids=extract_ids)
        self.set_downloaded_status(project=project, report=report, extract_ids=extract_ids)
        if extract_ids:
            headers = {'Content-Disposition': f'attachment; filename="{project}_patients_report.json"'}
            return Response(json.dumps(report, indent=4), headers=headers)
        else:
            return report
    
    def get_schema_imports(self, project_name: str) -> list:
        """Get list of terminology imports in schema

        Args:
            project_name (str): name of the project

        Returns:
            list: list of imports
        """
        bucket = self.config.s3_connector_bucket
        imports = []
        sphn_version_iri = self.get_external_schema_imports(project_name=project_name, imports=imports)
        schema = self.config.s3_client.get_object(Bucket=bucket, Key=f"{project_name}/Schema/{project_name}_schema.ttl")
        doc = lightrdf.RDFDocument(io.BytesIO(schema["Body"].read()), parser=lightrdf.turtle.PatternParser)
        for _, _, o in doc.search_triples(None, OWL.imports, None):
            if str(o) != sphn_version_iri:
                imports.append(str(o))
        return list(set(imports))

    def get_external_schema_imports(self, project_name: str, imports: list) -> str:
        """Get imported schemas from external schema if it exists (SPHN schema)

        Args:
            project_name (str): name of the project
            imports (list): list of schema imports

        Returns:
            str: SPHN version IRI
        """
        sphn_version = None
        bucket = self.config.s3_connector_bucket
        
        external_schemas = self.list_objects_keys(bucket=bucket, prefix=f"{project_name}/QAF/Schemas/")
        for external_schema in external_schemas:
            sphn_schema = self.config.s3_client.get_object(Bucket=bucket, Key=external_schema)
            doc = lightrdf.RDFDocument(io.BytesIO(sphn_schema["Body"].read()), parser=lightrdf.turtle.PatternParser)
            for _, _, o in doc.search_triples(None, OWL.imports, None):
                imports.append(str(o))
            for _, _, o in doc.search_triples(None, OWL.versionIRI, None):
                sphn_version = str(o)
            break
        return sphn_version
    
    def download_sparql_queries(self, project: str, compression_type: CompressionType) -> StreamingResponse:
        """Download auto-generated SPARQL queries

        Args:
            project (str): name of the project
            compression_type (CompressionType): compression type

        Raises:
            HTTPException: exception if no SPARQL queries are found

        Returns:
            StreamingResponse: ZIP archive with SPARQL queries
        """
        bucket = self.config.s3_connector_bucket
        sparql_files = self.list_objects_keys(bucket=bucket, prefix=f"{project}/Statistics/Queries/")
        if sparql_files:
            return self.zip_download_files(file_list=sparql_files, project=project, compression_type=compression_type, zip_filename=f"{project}_SPARQL_queries{compression_type.value}")  
        else:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"No SPAQRL queries found for project '{project}'. Make sure it has been successfully initialized")

    def store_excel_statistics(self, project: str, stats_reports: dict):
        """Extract Excel file with statistics tables

        Args:
            project (str): name of the project
            stats_reports (dict): statistics data
        """
        # Create an Excel workbook
        workbook = Workbook()

        # Loop through the data dictionary and write each CSV data to a separate sheet
        for sheet_name, data in stats_reports.items():
            sheet_name = sheet_name.split('/')[-1].replace('.csv', '').replace('-', '_')
            # Read the CSV data into a list of lists
            data_list = list(csv.reader(io.StringIO(data)))

            # Create a new sheet in the workbook
            sheet = workbook.create_sheet(title=sheet_name)

            # Write the CSV data to the sheet
            for row in data_list:
                sheet.append(row)

            # Calculate the number of columns and rows in the data
            num_columns = len(data_list[0])
            num_rows = len(data_list)
            table_range = f"A1:{get_column_letter(num_columns)}{num_rows}"
            tab = Table(displayName=sheet_name, ref=table_range)

            # Add a default style with striped rows and banded columns
            style = TableStyleInfo(name="TableStyleMedium9", showFirstColumn=False, showLastColumn=False, showRowStripes=True, showColumnStripes=True)
            tab.tableStyleInfo = style
            sheet.add_table(tab)

            # Autoset the column widths
            for col_cells in sheet.columns:
                max_length = 0
                for cell in col_cells:
                    try:
                        if len(str(cell.value)) > max_length:
                            max_length = len(cell.value)
                    except Exception:
                        pass
                adjusted_width = (max_length + 2)
                sheet.column_dimensions[col_cells[0].column_letter].width = adjusted_width

        # Remove the default sheet that is created automatically
        workbook.remove(workbook["Sheet"])
            
        buffer = io.BytesIO()
        workbook.save(buffer)
        buffer.seek(0)
        content = buffer.getvalue()
        file_size = io.BytesIO(content).getbuffer().nbytes
        self.config.s3_client.put_object(Bucket=self.config.s3_connector_bucket, Key=f"{project}/Statistics/Reports/{project}_statistics_report.xlsx", Body=io.BytesIO(content), ContentLength=file_size)

    def store_zipped_statistics(self, project: str, stats_reports: dict):
        """Extract ZIP archive with statistics report

        Args:
            project (str): name of the project
            stats_reports (dict): statistics data
        """
        # Store ZIP report
        bytes_buffer = BytesIO()
        with zipfile.ZipFile(bytes_buffer, mode='w', compression=zipfile.ZIP_DEFLATED) as zip:
            for filename, data in stats_reports.items():
                zip.writestr(filename, data.encode())
            zip.close()
        content = bytes_buffer.getvalue()
        file_size = io.BytesIO(content).getbuffer().nbytes
        self.config.s3_client.put_object(Bucket=self.config.s3_connector_bucket, Key=f"{project}/Statistics/Reports/{project}_statistics_report{CompressionType.ZIP.value}", Body=io.BytesIO(content), ContentLength=file_size)

        # Store TAR report
        bytes_buffer = BytesIO()
        tar_file_obj = tarfile.open(mode = "w:gz", fileobj=bytes_buffer)
        for filename, data in stats_reports.items():
            info = tarfile.TarInfo(filename)
            log_bytes = io.BytesIO(data.encode())
            info.size = log_bytes.getbuffer().nbytes
            tar_file_obj.addfile(info, log_bytes)
        tar_file_obj.close()
        content = bytes_buffer.getvalue()
        file_size = io.BytesIO(content).getbuffer().nbytes
        self.config.s3_client.put_object(Bucket=self.config.s3_connector_bucket, Key=f"{project}/Statistics/Reports/{project}_statistics_report{CompressionType.TAR.value}", Body=io.BytesIO(content), ContentLength=file_size)
    
    def extract_statistics(self, project: str, output_file_type: StatisticsOutputType) -> StreamingResponse:
        """Extract project statistics

        Args:
            project (str): name of the project
            output_file_type (StatisticsOutputType): output file type of statistics reports

        Raises:
            HTTPException: no statistics found

        Returns:
            StreamingResponse: statistics reports
        """
        bucket = self.config.s3_connector_bucket
        report_name = f"{project}/Statistics/Reports/{project}_statistics_report{output_file_type.value}"
        reports = self.list_objects_keys(bucket=bucket, prefix=report_name, file_path=True)
        if reports:
            return StreamingResponse(
                    iter([self.config.s3_client.get_object(Bucket=bucket, Key=report_name)["Body"].read()]),
                    media_type="application/x-zip-compressed",
                    headers = { "Content-Disposition": "attachment; filename={}".format(os.path.basename(report_name))}
                )
        else:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"No statistics found for project '{project}'")
        
    def store_statistics(self, project: str, profile: StatisticsProfile):
        """Store project statistics in S3

        Args:
            project (str): name of the project
            profile (StatisticsProfile): statistics profile

        Raises:
            HTTPException: no statistics found
        """
        stats_reports = {}
        self.extract_standardized_reports(project=project, stats_reports=stats_reports, profile=profile)
        self.extract_individual_reports(project=project, stats_reports=stats_reports)
        if stats_reports:
            self.store_excel_statistics(project=project, stats_reports=stats_reports)
            self.store_zipped_statistics(project=project, stats_reports=stats_reports)

    def extract_standardized_reports(self, project: str, stats_reports: dict, profile: StatisticsProfile):
        """Extract standardized reports

        Args:
            project (str): name of the project
            stats_reports (dict): map of reports
            profile (StatisticsProfile): statistics profile
        """
        self.create_connection()
        concepts_count_report = self.extract_concepts_count(project=project)
        concepts_min_max_report = self.extract_concepts_min_max(project=project, profile=profile)
        concepts_codes_report = self.extract_concepts_codes(project=project, profile=profile)
        
        self.close_connection()
        if concepts_count_report:
            stats_reports['standardized_reports/concepts_count.csv'] = concepts_count_report
        if concepts_min_max_report:
            stats_reports['standardized_reports/concepts_min_max.csv'] = concepts_min_max_report
        if concepts_codes_report:
            stats_reports['standardized_reports/concepts_codes.csv'] = concepts_codes_report

    def extract_individual_reports(self, project: str, stats_reports: dict):
        """Extract individual reports

        Args:
            project (str): name of the project
            stats_reports (dict): map of reports
        """
        self.create_connection()
        query = "SELECT patient_id, query_name, header, query_output FROM stats_individual_reports WHERE project_name = %s AND data_provider_id = %s"
        self.cursor.execute(query, (project, self.config.data_provider_id))
        records = self.cursor.fetchall()
        self.close_connection()
        for record in records:
            patient_id = record[0]
            report_name = f"individual_reports/{record[1]}.csv"
            header = record[2]
            query_output = record[3]
            if report_name not in stats_reports:
                stats_reports[report_name] = "data_provider_id, project_name, patient_id," + header
            query_records = query_output.split('\n')
            for query_record in query_records:
                if query_record:
                    stats_reports[report_name] += f"{self.config.data_provider_id},{project},{patient_id},{query_record}\n"

    def extract_concepts_count(self, project: str) -> str:
        """Extract concepts count

        Args:
            project (str): name of the project

        Returns:
            str: concepts count report
        """
        encounters_query = """SELECT concept, COUNT(DISTINCT administrative_case) as encounters
        FROM sparqler_administrative_case_encounters
        WHERE project_name = %s AND data_provider_id = %s
        GROUP BY concept
        """
        self.cursor.execute(encounters_query, (project, self.config.data_provider_id))
        encounters_records = self.cursor.fetchall()
        encounters = {}
        for encounter in encounters_records:
            encounters[encounter[0]] = encounter[1]

        concept_query = """SELECT concept AS concept, SUM(count) AS instances, COUNT(DISTINCT patient_id) AS patients
        FROM sparqler_count_instances
        WHERE project_name = %s AND data_provider_id = %s
        GROUP BY concept
        """
        self.cursor.execute(concept_query, (project, self.config.data_provider_id))
        concept_records = self.cursor.fetchall()
        columns = [desc[0] for desc in self.cursor.description]

        stats_extract = ""
        for record in concept_records:
            if record[0] in encounters:
                stats_records = [str(col) for col in record] + [str(encounters[record[0]])]
            else:
                stats_records = [str(col) for col in record] + ['0']

            stats_extract += ",".join(stats_records) + "\n"
        if stats_extract:
            stats_extract = ",".join(columns + ['encounters']) + "\n" + stats_extract
        return stats_extract

    def extract_concepts_min_max(self, project: str, profile: StatisticsProfile) -> str:
        """Extract concepts min/max report

        Args:
            project (str): name of the project
            profile (StatisticsProfile): statistics profile

        Returns:
            str: concepts min/max report
        """
        if profile == StatisticsProfile.EXTENSIVE:
            query = """SELECT
            concept, attribute, min(min) as min, max(max) as max, count(distinct value) as count_distinct, substring(string_agg(distinct value, '; '), '([^;]+(;[^;]+){{0,4}})') as sample_values
            FROM
            sparqler_min_max_predicates_extensive 
            WHERE
            project_name = %s AND data_provider_id = %s
            GROUP BY concept, attribute
            UNION ALL
                SELECT
                concept, attribute, cast(min(min) as VARCHAR) as min, cast(max(max) as VARCHAR) as max, count(distinct value) as count_distinct, substring(string_agg(distinct cast(value AS VARCHAR), '; '), '([^;]+(;[^;]+){{0,4}})') as sample_values
                FROM
                sparqler_min_max_predicates_numeric_extensive 
                WHERE
                project_name = %s AND data_provider_id = %s
                GROUP BY concept, attribute
            ORDER BY concept, attribute, min, max
            """
        else:
            query = """SELECT
                concept, attribute, min(min) as min, max(max) as max, min(count) as min_count, max(count) as max_count  
                FROM
                sparqler_min_max_predicates_fast 
                WHERE
                project_name = %s AND data_provider_id = %s
                GROUP BY concept, attribute
            UNION ALL
                SELECT
                concept, attribute, cast(min(min) as VARCHAR) as min, cast(max(max) as VARCHAR) as max, min(count) as min_count, max(count) as max_count 
                FROM
                sparqler_min_max_predicates_numeric_fast 
                WHERE
                project_name = %s AND data_provider_id = %s
                GROUP BY concept, attribute
            ORDER BY concept, attribute, min, max
            """
        self.cursor.execute(query, (project, self.config.data_provider_id, project, self.config.data_provider_id))
        records = self.cursor.fetchall()
        stats_extract = ""
        for record in records:
            stats_extract += ",".join([str(col) for col in record]) + "\n"
        if stats_extract:
            stats_extract = ",".join([desc[0] for desc in self.cursor.description]) + "\n" + stats_extract
        return stats_extract

    def extract_concepts_codes(self, project: str, profile: StatisticsProfile) -> str:
        """Extract concepts codes report

        Args:
            project (str): name of the project
            profile (StatisticsProfile): statistics profile

        Returns:
            str: concepts codes report
        """
        if profile == StatisticsProfile.EXTENSIVE:
            query = f"""SELECT concept, attribute, SUM(count) as instances, COUNT(distinct code) AS count_distinct_values, substring(string_agg(distinct code, '; '), '([^;]+(;[^;]+){{0,4}})') as sample_values
FROM sparqler_has_code
WHERE project_name = '{project}' AND data_provider_id = '{self.config.data_provider_id}'
GROUP BY concept, attribute
ORDER BY concept, attribute, count_distinct_values"""
        else:
            query = f"""SELECT concept, attribute, SUM(count) as instances, COUNT(distinct code) AS count_distinct_values
FROM sparqler_has_code
WHERE project_name = '{project}' AND data_provider_id = '{self.config.data_provider_id}'
GROUP BY concept, attribute
ORDER BY concept, attribute, count_distinct_values
"""
        self.cursor.execute(query)
        records = self.cursor.fetchall()
        stats_extract = ""
        for record in records:
            stats_extract += ",".join([str(col) for col in record]) + "\n"
        if stats_extract:
            stats_extract = ",".join([desc[0] for desc in self.cursor.description]) + "\n" + stats_extract
        return stats_extract
    
    def cleanup_qc_statistics(self, project: str, filename: str):
        """When a query file is re-ingested the associated data is cleaned up from the DB table

        Args:
            project (str): name of the project
            filename (str): query filename
        """
        query_name = filename.split('.')[0]
        self.create_connection()
        sql = "DELETE FROM stats_individual_reports WHERE project_name=%s AND data_provider_id=%s AND query_name=%s"
        self.cursor.execute(sql, (project, self.config.data_provider_id, query_name))
        self.close_connection()

    def truncate_statistics(self, project: str):
        """Truncate statistics tables

        Args:
            project (str): name of the project
        """
        self.create_connection()
        sqls = ["DELETE FROM sparqler_count_instances WHERE project_name=%s AND data_provider_id=%s",
                "DELETE FROM sparqler_min_max_predicates_extensive WHERE project_name=%s AND data_provider_id=%s",
                "DELETE FROM sparqler_min_max_predicates_numeric_extensive WHERE project_name=%s AND data_provider_id=%s",
                "DELETE FROM sparqler_min_max_predicates_fast WHERE project_name=%s AND data_provider_id=%s",
                "DELETE FROM sparqler_min_max_predicates_numeric_fast WHERE project_name=%s AND data_provider_id=%s",
                "DELETE FROM sparqler_has_code WHERE project_name=%s AND data_provider_id=%s",
                "DELETE FROM sparqler_administrative_case_encounters WHERE project_name=%s AND data_provider_id=%s",
                "DELETE FROM stats_individual_reports WHERE project_name=%s AND data_provider_id=%s"]
        for sql_query in sqls:
            self.cursor.execute(sql_query, (project, self.config.data_provider_id))
        self.close_connection()

    def reset_download_status(self, project: str) -> str:
        """Reset the status of downloaded data

        Args:
            project (str): name of the project

        Returns:
            str: return message
        """
        self.create_connection()
        query = "UPDATE release_zone SET downloaded=False WHERE data_provider_id=%s AND project_name=%s"
        self.cursor.execute(query, (self.config.data_provider_id, project))
        self.close_connection()
        return f"Download status of project '{project}' reset"
    
    def reset_notification_status(self, project: str):
        """Reset the status of data notified to Einstein

        Args:
            project (str): name of the project
        """
        self.create_connection()
        query = "UPDATE einstein_data_sharing SET status='ready' WHERE data_provider_id=%s AND project_name=%s"
        self.cursor.execute(query, (self.config.data_provider_id, project))
        self.close_connection()
    
    @staticmethod
    def _convert_type(value: object) -> object:
        """Convert value to correct type

        Args:
            value (object): value of column

        Returns:
            object: converted value
        """
        if isinstance(value, datetime) or isinstance(value, date) or isinstance(value, time):
            return str(value)
        elif isinstance(value, decimal.Decimal):
            return float(value)
        else:
            return value

    def write_de_id_insert_statements(self, project: str, zip_file: zipfile.ZipFile):
        """Extract insert statement into de-identification tables

        Args:
            project (str): name of the project
            zip_file (zipfile.ZipFile): zip file
        """
        table_names = ['de_identification', 'de_identification_shifts', 'de_identification_scrambling']
        self.create_connection()
        for table_name in table_names:
            insert_statements = []
            query = sql.SQL(
                'SELECT * FROM "public".{table_name} WHERE project_name=%s'
                ).format(table_name=sql.Identifier(table_name))
            self.cursor.execute(query, (project, ))
            records = self.cursor.fetchall()
            if records:
                column_names = [desc[0] for desc in self.cursor.description]
                for record in records:
                    record = [Database._convert_type(rec) for rec in record]
                    filtered_record = '$PROJECT_NAME$,$DATA_PROVIDER_ID$,'
                    filtered_columns = "project_name,data_provider_id,"
                    for rec, col in zip(record, column_names):
                        if rec is not None and col not in ['project_name', 'data_provider_id']:
                            if isinstance(rec, str):
                                rec = rec.replace("'", "''")
                                rec = f"'{rec}'"
                            filtered_record += f"{rec},"
                            filtered_columns += f"{col},"
                    insert_statements.append(f'INSERT INTO "public".{table_name} ({filtered_columns.rstrip(",")}) VALUES ({filtered_record.rstrip(",")});')
            table_insert_statement = '\n'.join(insert_statements)
            if table_insert_statement:
                zip_file.writestr(f'Restore/{table_name}_table_insert.sql', table_insert_statement.encode())
        self.close_connection()
    
    def restore_de_identification(self, project: str, de_id_data: bytes) -> str:
        """Restore de-identification data

        Args:
            project (str): name of the project
            de_id_data (bytes): De-Id data extracted from /Get_de_identification_report

        Returns:
            str: successful message
        """
        bytes_buffer = io.BytesIO(de_id_data)
        bucket = self.config.s3_connector_bucket
        insert_files = ['Restore/de_identification_scrambling_table_insert.sql', 'Restore/de_identification_shifts_table_insert.sql', 'Restore/de_identification_table_insert.sql']
        with zipfile.ZipFile(bytes_buffer) as zip_ref:
            for zipinfo in zip_ref.infolist():
                if zipinfo.filename in insert_files:
                    truncation_statements = ["DELETE FROM de_identification WHERE project_name=%s AND data_provider_id=%s",
                                             "DELETE FROM de_identification_shifts WHERE project_name=%s AND data_provider_id=%s",
                                             "DELETE FROM de_identification_scrambling WHERE project_name=%s AND data_provider_id=%s"]
                    self.create_connection()
                    for truncation_stm in truncation_statements:
                        self.cursor.execute(truncation_stm, (project, self.config.data_provider_id))
                    self.close_connection()
                    break
            for zipinfo in zip_ref.infolist():
                if 'Restore/de_id_config.json' in zipinfo.filename:
                    with zip_ref.open(zipinfo) as extracted_file:
                        file_content = extracted_file.read()
                        de_id_config = f"{project}/De-Identification/{project}_de_identification_config.json"
                        bytes_content= io.BytesIO(file_content)
                        bytes_content.seek(0)
                        file_size = bytes_content.getbuffer().nbytes
                        self.config.s3_client.put_object(Bucket=bucket, Key=de_id_config, Body=bytes_content, ContentLength=file_size)
                elif zipinfo.filename in insert_files:
                    with zip_ref.open(zipinfo) as extracted_file:
                        file_content = extracted_file.read()
                        file_content = file_content.decode().replace("$PROJECT_NAME$", f"'{project}'").replace("$DATA_PROVIDER_ID$", f"'{self.config.data_provider_id}'")
                        self.create_connection(auto_commit=False)
                        query_no = 1
                        for query in file_content.split('\n'):
                            self.cursor.execute(query)
                            query_no += 1
                            if query_no % 100000 == 0:
                                self.conn.commit()
                        self.conn.commit()
                        self.close_connection()
        return f"De-Identification for project '{project}' restored"

    @staticmethod
    def _trigger_s3_download(project_name: str, 
                             compression_type: str, 
                             skip_downloaded: str, 
                             dag_run_config: dict, 
                             patients_only: bool):
        """Trigger download of data into S3 storage

        Args:
            project_name (str): name of the project
            compression_type (str): compression type
            skip_downloaded (str): skip already downloaded files
            dag_run_config (dict): configuration for the running Airflow dag
            patients_only (bool): download patients data only
        """
        database = Database(project=project_name)
        compression_type = CompressionType(compression_type)
        skip_downloaded = skip_downloaded == 'True'
        validation_status = dag_run_config.get('validation_status')
        if validation_status is None:
            for validation_status in ['OK', 'NOT-OK']:
                patients_map, qc_logs = database.get_released_patients(project=project_name, patient_id=None, validation_status=validation_status, skip_downloaded=skip_downloaded, batch_size=None, patients_only=patients_only)
                download_bundle = database.generate_download_bundle(patients_map=patients_map, project=project_name, compression_type=compression_type, qc_logs=qc_logs, batch_size=None, patients_only=patients_only, s3_download=True)
                file_size = io.BytesIO(download_bundle).getbuffer().nbytes
                object_name = f"{project_name}/Download/{project_name}_{validation_status}{compression_type.value}"
                database.config.s3_client.put_object(Bucket=database.config.s3_connector_bucket, Key=object_name, Body=io.BytesIO(download_bundle), ContentLength=file_size)
                database.record_patients_download(project_name=project_name, patient_ids=patients_map.keys())
        else:
            patients_map, qc_logs = database.get_released_patients(project=project_name, patient_id=None, validation_status=validation_status, skip_downloaded=skip_downloaded, batch_size=None, patients_only=patients_only)
            download_bundle = database.generate_download_bundle(patients_map=patients_map, project=project_name, compression_type=compression_type, qc_logs=qc_logs, batch_size=None, patients_only=patients_only, s3_download=True)
            file_size = io.BytesIO(download_bundle).getbuffer().nbytes
            object_name = f"{project_name}/Download/{project_name}_{validation_status}{compression_type.value}"
            database.config.s3_client.put_object(Bucket=database.config.s3_connector_bucket, Key=object_name, Body=io.BytesIO(download_bundle), ContentLength=file_size)
            database.record_patients_download(project_name=project_name, patient_ids=patients_map.keys())

    def download_s3_file(self, 
                         dag_id: str, 
                         project: str = None, 
                         validation_status: str = None, 
                         compression_type: CompressionType = None) -> StreamingResponse:
        """Download file previously stored in S3

        Args:
            dag_id (str): DAG ID
            project (str, optional): Name of the project. Defaults to None.
            validation_status (str, optional): Validation status. Defaults to None.
            compression_type (CompressionType, optional): Compression type. Defaults to None.

        Raises:
            HTTPException: no files found

        Returns:
            StreamingResponse: file to download
        """
        if dag_id == 'run_database_backup':
            bucket = self.config.s3_connector_bucket
            objects = self.list_objects(bucket=bucket, prefix='sphn-connector-backup/')
            if objects:
                sorted_objects = sorted(objects, key=lambda obj: obj['LastModified'], reverse=True)
                if sorted_objects:
                    object_name = sorted_objects[0]['Key']
                    return StreamingResponse(
                        iter([self.config.s3_client.get_object(Bucket=bucket, Key=object_name)["Body"].read()]),
                        media_type="application/x-zip-compressed",
                        headers = { "Content-Disposition": "attachment; filename={}".format(object_name)}
                    )
                else:
                   raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No backup of SPHN Connector database found") 
            else:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No backup of SPHN Connector database found")
        elif dag_id == 'run_s3_download':
            bucket = self.config.s3_connector_bucket
            if validation_status is not None:
                objects_list = self.list_objects_keys(bucket=bucket, prefix=f"{project}/Download/{project}_{validation_status}{compression_type.value}", file_path=True)
            else:
                objects_list = []
                for validation_status in ['OK', 'NOT-OK']:
                    objects_list += self.list_objects_keys(bucket=bucket, prefix=f"{project}/Download/{project}_{validation_status}{compression_type.value}", file_path=True)
            if objects_list:
                if len(objects_list) == 1:
                    object_name = objects_list[0]
                    return StreamingResponse(
                            iter([self.config.s3_client.get_object(Bucket=bucket, Key=object_name)["Body"].read()]),
                            media_type="application/x-zip-compressed",
                            headers = { "Content-Disposition": "attachment; filename={}".format(os.path.basename(object_name))}
                        )
                else:
                    merged_zip_data = io.BytesIO()
                    with zipfile.ZipFile(merged_zip_data, 'w') as merged_zip_file:
                        for object_name in objects_list:
                            zip_data = self.config.s3_client.get_object(Bucket=bucket, Key=object_name)["Body"].read()
                            merged_zip_file.writestr(os.path.basename(object_name), zip_data)
                    return StreamingResponse(
                        iter([merged_zip_data.getvalue()]),
                        media_type="application/x-zip-compressed",
                        headers = { "Content-Disposition": "attachment; filename={}".format(f'{project}.zip')}
                    )
            else:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"No download bundle found for project '{project}'") 

    @staticmethod
    def _get_tables_with_dpi(project_name: str) -> list:
        """Gets supporting concepts with property sphn:hasDataProvider

        Args:
            project_name (str): name of the project

        Returns:
            list: table names
        """
        table_names = []
        database = Database(project=project_name)
        database.create_connection()
        query = "SELECT DISTINCT table_name FROM supporting_concepts WHERE data_provider_id=%s AND project_name=%s"
        database.cursor.execute(query, (database.config.data_provider_id, project_name))
        records = database.cursor.fetchall()
        for table_name in records:
            table_names.append("supporting__" + table_name[0])
        database.close_connection()
        return table_names
    
    def get_sphn_base_iri(self, project: str) -> str:
        """Get SPHN base IRI for project

        Args:
            project (str): name of the project

        Returns:
            str: SPHN base IRI
        """
        self.create_connection()
        query = "SELECT sphn_base_iri FROM configuration WHERE project_name=%s"
        self.cursor.execute(query, (project, ))
        record = self.cursor.fetchone()
        self.close_connection()
        return record[0]

    def get_quality_checks_report(self, project: str, patient_id: str, validation_status: str) -> Response:
        """Extract quality checks report

        Args:
            project (str): name of the project
            patient_id (str): ID of the patient
            validation_status (str): validation status

        Raises:
            HTTPException: no quality checks data found

        Returns:
            REsponse: quality checks Excel report
        """
        self.create_connection()
        if patient_id is not None:
            if validation_status is not None:
                query = "SELECT patient_id, successful_validation, severity, result_path, source_constraint_component, source_shape, count, focus_node, result_message, value \
                         FROM quality_checks WHERE project_name=%s AND data_provider_id=%s AND patient_id=%s AND successful_validation=%s"
                self.cursor.execute(query, (project, self.config.data_provider_id, patient_id, validation_status == 'OK'))
            else:
                query = "SELECT patient_id, successful_validation, severity, result_path, source_constraint_component, source_shape, count, focus_node, result_message, value \
                         FROM quality_checks WHERE project_name=%s AND data_provider_id=%s AND patient_id=%s"
                self.cursor.execute(query, (project, self.config.data_provider_id, patient_id))
        else:
            if validation_status is not None:
                query = "SELECT patient_id, successful_validation, severity, result_path, source_constraint_component, source_shape, count, focus_node, result_message, value \
                         FROM quality_checks WHERE project_name=%s AND data_provider_id=%s AND successful_validation=%s"
                self.cursor.execute(query, (project, self.config.data_provider_id, validation_status == 'OK'))
            else:
                query = "SELECT patient_id, successful_validation, severity, result_path, source_constraint_component, source_shape, count, focus_node, result_message, value \
                         FROM quality_checks WHERE project_name=%s AND data_provider_id=%s"
                self.cursor.execute(query, (project, self.config.data_provider_id))
        records = self.cursor.fetchall()
        self.close_connection()
        if records:
            workbook = Workbook()
            sheet_name = 'Quality Checks Report'
            sheet = workbook.create_sheet(title=sheet_name)
            sheet.append(["Patient ID", "Successful Validation", "Severity", "Result Path", "Source Constraint Component", "Source Shape", "Count", "Focus Node", "Result Message", "Value"])
            for record in records:
                sheet.append(record)
            
            num_columns = 10
            num_rows = len(records) + 1
            table_range = f"A1:{get_column_letter(num_columns)}{num_rows}"
            tab = Table(displayName="qc_table", ref=table_range)
            style = TableStyleInfo(name="TableStyleMedium9", showFirstColumn=False, showLastColumn=False, showRowStripes=True, showColumnStripes=True)
            tab.tableStyleInfo = style
            sheet.add_table(tab)

            for col_cells in sheet.columns:
                max_length = 0
                for cell in col_cells:
                    try:
                        if len(str(cell.value)) > max_length:
                            max_length = len(cell.value)
                    except Exception:
                        pass
                adjusted_width = (max_length + 2)
                sheet.column_dimensions[col_cells[0].column_letter].width = adjusted_width

            workbook.remove(workbook["Sheet"])
                
            buffer = io.BytesIO()
            workbook.save(buffer)
            buffer.seek(0)
            response = Response(content=buffer.getvalue(), media_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            response.headers["Content-Disposition"] = f"attachment; filename={project}_quality_checks.xlsx"
            return response
        else:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No quality checks found")

    @staticmethod
    def _get_data_provider_class(sphn_base_iri: str) -> str:
        """Return correct data provider class name based on the owl:versionIRI of SPHN schema.
           In 2023.2 it was defined as sphn:DataProviderInstitute, while fro 2024.1 it is defined as sphn:DataProvider 

        Args:
            sphn_base_iri (str): SPHN base IRI

        Returns:
            str: data provider class name
        """
        if sphn_base_iri.startswith('https://biomedit.ch/rdf/sphn-schema/sphn'):
            return 'DataProvider'
        else:
            return 'DataProviderInstitute'
    
    @staticmethod
    def _is_legacy_schema(sphn_base_iri: str) -> bool:
        """Check if SPHN schema is older than 2024.1

        Args:
            sphn_base_iri (str): SPHN base IRI

        Returns:
            bool: is legacy schema
        """
        if sphn_base_iri.startswith('https://biomedit.ch/rdf/sphn-schema/sphn'):
            return False
        else:
            return True

    @staticmethod
    def _is_2024_schema(sphn_schema_iri: str) -> bool:
        """Check if schema is 2024 RDF schema

        Args:
            sphn_schema_iri (str): SPHN schema IRI

        Returns:
            bool: is related to 2024 RDF schema
        """
        if sphn_schema_iri.startswith('https://biomedit.ch/rdf/sphn-schema/sphn/2024/'):
            return True
        else:
            return False

    def get_pre_check_de_id_processed_patients(self, project: str, execution_time: datetime) -> int:
        """Get number of patients processed in Pre-Checks/De-Identification step

        Args:
            project (str): name of the project
            execution_time (datetime): execution time of the step

        Returns:
            int: count of processed patients
        """
        self.create_connection()
        query = "SELECT COUNT(*) FROM refined_zone WHERE project_name=%s AND data_provider_id=%s AND processed=False AND timestmp > %s"
        self.cursor.execute(query, (project, self.config.data_provider_id, execution_time))
        count = self.cursor.fetchone()[0]
        self.close_connection()
        return int(count)

    @staticmethod
    def _count_patients_processed(project_name: str, step: str, grafana: bool = False):
        """Count converted/validated patients on the file system

        Args:
            project_name (str): name of the project
            step (str): pipeline step
        """
        database = Database(project=project_name)
        step = Steps(step)
        if step == Steps.INTEGRATION:
            conversion_folder = f"/home/sphn/conversion/{project_name}/**/converted/*"
            file_count = len(glob.glob(conversion_folder, recursive=True))
        elif step == Steps.VALIDATION:
            log_folder = f"/home/sphn/quality/{project_name}/log"
            success_files = len(glob.glob(os.path.join(log_folder, '**/success/*'), recursive=True))
            failed_files = len(glob.glob(os.path.join(log_folder, '**/failed/*'), recursive=True))
            file_count = success_files + failed_files
        else:
            stats_folder = f"/home/sphn/statistics/{project_name}/sparqler-output/**/*"
            file_count = len(glob.glob(stats_folder))
        database.create_connection()
        if grafana:
            table_name = "grafana_real_time_report"
        else:
            table_name = "real_time_processing"
        query = sql.SQL(
            """INSERT INTO {table_name} (data_provider_id, project_name, step, patients_processed)
                VALUES (%s, %s, %s, %s) ON CONFLICT (data_provider_id, project_name, step) \
                DO UPDATE SET patients_processed=EXCLUDED.patients_processed"""
                ).format(table_name=sql.Identifier(table_name))
        database.cursor.execute(query, (database.config.data_provider_id, project_name, step.value, file_count))
        database.close_connection()

    def get_patients_count(self, project: str, step: Steps) -> dict:
        """Return count of processed patients

        Args:
            project (str): name of the project
            step (Steps): pipeline step

        Raises:
            HTTPException: failed to extract count

        Returns:
            dict: patient count
        """
        self.create_connection()
        query = "SELECT patients_processed FROM real_time_processing WHERE project_name=%s AND data_provider_id=%s AND step=%s"
        self.cursor.execute(query, (project, self.config.data_provider_id, step.value))
        records = self.cursor.fetchone()
        self.close_connection()
        if records:
            return int(records[0])
        else:
            return 0
        
    def check_for_batch_data(self, project: str):
        """Check if batch data is available for the load

        Args:
            project (str): name of the project

        Raises:
            HTTPException: no data found
            HTTPException: bucket not found
        """
        bucket = self.config.s3_connector_bucket
        if bucket_exists(bucket = bucket, config=self.config):
            prefix = f'{project}/Input/batch_ingestion/'
            batch_files = self.list_objects(bucket=bucket, prefix=prefix)
            if not batch_files:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"No batch files found for project '{project}' under prefix '{project}/Input/batch_ingestion/'")
        else:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Bucket '{bucket}' not found")

    def get_data_provider_id(self, project: str) -> str:
        """Get data provider ID defined for the project

        Args:
            project (str): project name

        Returns:
            str: data provider ID
        """
        self.create_connection()
        query = "SELECT data_provider_id FROM configuration WHERE project_name=%s"
        self.cursor.execute(query, (project, ))
        result = self.cursor.fetchone()
        self.close_connection()
        if result is not None:
            data_provider_id = result[0]
        else:
            config_file = "/home/sphn/code/config.json"
            config_data = json.load(open(config_file, encoding="utf-8"))
            data_provider_id = config_data.get("data_provider_id")
        return data_provider_id

    def get_status(self, project:str, step: Steps) -> list:
        """Get status of pipeline from history table

        Args:
            project (str): name of the project
            step (Steps): pipeline step

        Returns:
            list: list of results
        """
        self.create_connection()
        if step != Steps.ALL:
            sql = "SELECT run_uuid, project_name, pipeline_step, pipeline_status, patients_processed, patients_with_warnings, patients_with_errors, execution_time, elapsed_time \
                   FROM pipeline_history WHERE project_name=%s AND data_provider_id=%s AND pipeline_step=%s ORDER BY execution_time DESC"
            self.cursor.execute(sql, (project, self.config.data_provider_id, step.value))
        else:
            sql = "SELECT run_uuid, project_name, pipeline_step, pipeline_status, patients_processed, patients_with_warnings, patients_with_errors, execution_time, elapsed_time \
                   FROM pipeline_history WHERE project_name=%s AND data_provider_id=%s ORDER BY execution_time DESC"
            self.cursor.execute(sql, (project, self.config.data_provider_id))
        records = self.cursor.fetchall()
        self.close_connection()
        return records

    def get_project_output_format(self, project: str) -> Union[OutputFormat, bool]:
        """Get project output format (Turtle, Trig, Nquads) and compression

        Args:
            project (str): name of the project

        Returns:
            OutputFormat, str: output format and compression
        """
        self.create_connection()
        query = "SELECT output_format, compression FROM configuration WHERE project_name=%s AND data_provider_id=%s"
        self.cursor.execute(query, (project, self.config.data_provider_id))
        result = self.cursor.fetchone()
        output_format = result[0]
        compression = result[1]
        self.close_connection()
        return OutputFormat(output_format), compression
    def check_if_reingested(self, project:str,patient_id: str) -> set:
        """
        check in the graph_zone and release_zone if new patient data is uploaded.
        returns a set of zones in which the patient id is found.
        Output is used as input for the 'return_update_query' method.
        Args:
            project (str): name of the project
            patient_id (str): patient ID
        Returns:
            set: zone_name: (DataPersistenceLayer)
        """

        result_list = []
        try:
            # if new data is uploaded for an existing patient we find its id in the graph / release zone.
            for zone in  [DataPersistenceLayer.GRAPH_ZONE, DataPersistenceLayer.RELEASE_ZONE]:
                query = sql.SQL(
                    "SELECT patient_id FROM {zone.value} WHERE project_name=%s AND data_provider_id=%s AND patient_id like %s;"
                    ).format(zone_value = sql.Identifier(str(zone.value)))
                self.cursor.execute(query, (project, self.config.data_provider_id, patient_id))
                result = self.cursor.fetchone()
                if result:
                    result_list.append(zone)
                else:
                    continue
        finally:
            return set(result_list)

    def return_update_query(self,result_set: set) -> str:
        """
        return_update_query takes the output from the 'check_if_reingested' method
        and returns the necessary SQL update as a string.
        Used to correctly display the patient count in case of new patient data for an
        existing patient.

        Args:
            result_set (set): DataPersistenceLayer zones

        Returns:
            str | None: update query string 
        """

        if result_set == set([DataPersistenceLayer.GRAPH_ZONE]):
            decrease_final_count = "UPDATE grafana_real_time_report SET patients_processed = patients_processed - 1 WHERE project_name=%s AND data_provider_id=%s AND step='Integration' AND patients_processed > 0;"
            return decrease_final_count
        # if the patient id is found in the Graph_zone and the release_zone we may only decrease the value of patients processed of validation and statistics step.
        # Otherwise we might reduce the number of the patients that are currently in the Graph_zone but have not yet been validated.
        elif result_set == set([DataPersistenceLayer.RELEASE_ZONE]) or result_set == set([DataPersistenceLayer.GRAPH_ZONE, DataPersistenceLayer.RELEASE_ZONE]):
            decrease_final_count = "UPDATE grafana_real_time_report SET patients_processed = patients_processed - 1 WHERE project_name=%s AND data_provider_id=%s AND step in ('Validation', 'Statistics') AND patients_processed > 0;"
            return decrease_final_count

    def is_einstein_activated(self, project: str) -> bool:
        """Check if Einstein data sharing is activated

        Args:
            project (str): name of the project

        Returns:
            bool: Einstein connection is active
        """
        self.create_connection()
        query = "SELECT share_with_einstein FROM configuration WHERE data_provider_id=%s AND project_name=%s"
        self.cursor.execute(query, (self.config.data_provider_id, project))
        record = self.cursor.fetchone()
        self.close_connection()
        return record[0]

    def store_einstein_data(self, patient_data: dict, project_name: str):
        """Store patient data to share with Einstein in local Postgres database

        Args:
            patient_data (dict): data to share
            project_name (str): name of the project
        """
        for patient_id, patient_info in patient_data['patients'].items():
            query = "INSERT INTO einstein_data_sharing (data_provider_id, project_name, file_identifier, file_graph, object_name, status, hash) VALUES \
                    (%s, %s, %s, %s, %s, %s, %s) ON CONFLICT (data_provider_id, project_name, file_identifier) DO UPDATE SET file_graph=EXCLUDED.file_graph, object_name=EXCLUDED.object_name, \
                    status=EXCLUDED.status, hash=EXCLUDED.hash"
            self.cursor.execute(query, (patient_data['data_provider_id'], project_name, patient_id, patient_info['patient_graph'], patient_info['object_name'], 'ready', patient_info['hash']))

    def get_patients_data_to_share(self, project_name: str, patient_id: str) -> dict:
        """Returns map of patients data ready to be shared with Einstein

        Args:
            project_name (str): name of the project
            patient_id (str): ID of the patients

        Returns:
            dict: map of patients data to share with Einstein
        """
        einstein_data = {'data_provider_id': self.config.data_provider_id, 'patients': {}}
        self.create_connection()
        if patient_id is None:
            query = "SELECT data_provider_id, file_identifier, file_graph, object_name, status, hash FROM einstein_data_sharing WHERE project_name=%s AND status='ready'"
            self.cursor.execute(query, (project_name, ))
        else:
            query = "SELECT data_provider_id, file_identifier, file_graph, object_name, status, hash FROM einstein_data_sharing WHERE project_name=%s AND file_identifier=%s AND status='ready'"
            self.cursor.execute(query, (project_name, patient_id))
        records = self.cursor.fetchall()
        self.close_connection()
        for patient in records:
            einstein_data['patients'][patient[1]] = {'object_name': patient[3], 'patient_graph': patient[2], 'hash': patient[5]}
        return einstein_data

    def extract_content_information(self, file_name: str, prefix:str, extract_iri = False)-> Dict:
        """
        extracts key information for given objects in S3
        
        Args:
            file_name (str): object name in s3 storage
            prefix (str): prefix to object
            extract_iri (bool, optional): _description_. Defaults to False.

        Returns:
            Dict: Dict: consists of name, file_size and version_iri (optional)
        """

        bucket = self.config.s3_connector_bucket
        s3_object = self.config.s3_client.get_object(Bucket=bucket, Key=file_name)
        name_of_file = file_name.replace(prefix, "")
        size_of_file = self._get_human_readable_size(file_size=s3_object["ContentLength"])
        if extract_iri:
            version_iri = self.get_schema_version_iri(object_name=file_name)
            return {"name": name_of_file, "version_iri": version_iri, "file_size": size_of_file}
        else:
            return {"name": name_of_file, "file_size": size_of_file}

    def list_objects_keys(self, bucket: str, prefix: str, file_path: bool = False) -> list:
        """Returns list of object keys for specified prefix

        Args:
            bucket (str): bucket name
            prefix (str): prefix in S3 storage
            file_path (bool): prefix targets a single file

        Returns:
            list: list of objects keys
        """
        paginator = self.config.s3_client.get_paginator('list_objects_v2')
        pages = paginator.paginate(Bucket=bucket, Prefix=prefix)
        objects = []
        for page in pages:
            for obj in page.get('Contents', []):
                if file_path and obj["Key"] == prefix:
                    objects.append(obj["Key"])
                    break
                if not file_path and obj["Key"] != prefix:
                    objects.append(obj["Key"])
        return objects

    def list_objects(self, bucket: str, prefix: str, file_path: bool = False) -> list:
        """Returns list of objects metadata for specified prefix

        Args:
            bucket (str): bucket name
            prefix (str): prefix in S3 storage
            file_path (bool): prefix targets a single file

        Returns:
            list: list of objects metadata
        """
        paginator = self.config.s3_client.get_paginator('list_objects_v2')
        pages = paginator.paginate(Bucket=bucket, Prefix=prefix)
        objects = []
        for page in pages:
            for obj in page.get('Contents', []):
                if file_path and obj["Key"] == prefix:
                    objects.append(obj)
                    break
                if not file_path and obj["Key"] != prefix:
                    objects.append(obj)
        return objects

    def get_project_extended_prefix(self, project: str) -> str:
        """Get project extended tables prefix for project

        Args:
            project (str): name of the project

        Returns:
            str: project extended tables prefix
        """
        self.create_connection()
        query = "SELECT project_extended_tables_prefix FROM configuration WHERE data_provider_id=%s AND project_name=%s"
        self.cursor.execute(query, (self.config.data_provider_id, project))
        result = self.cursor.fetchone()
        self.close_connection()
        if result:
            return result[0]
        else:
            return "P_EXT__"

    def update_history_table(self, 
                             project_name: str, 
                             pipeline_step: Steps, 
                             pipeline_status: PipelineStep, 
                             patients_processed: int, 
                             patients_with_warnings: int, 
                             patients_with_errors: int, 
                             execution_time: datetime, 
                             elapsed_time: int, 
                             run_uuid: str, 
                             parallel: bool = False):
        """Updates table 'pipeline_history' with the results of the processing

        Args:
            project_name (str): name of the project
            pipeline_step (Steps): name of the pipeline step
            pipeline_status (PipelineStep): status of the execution
            patients_processed (int): number of patients processed
            patients_with_warnings (int): number of patients with warnings
            patients_with_errors (int): number of failed patients
            execution_time (datetime): timestamp of pipeline execution
            elapsed_time (int): running time of pipeline in seconds
            run_uuid (str): UUID of the run
            parallel (bool, optional): parallel execution. Defaults to False.
        """
        if not parallel:
            self.create_connection()
        sql = "INSERT INTO pipeline_history (project_name, data_provider_id, pipeline_step, pipeline_status, patients_processed, patients_with_warnings, patients_with_errors, execution_time, elapsed_time, run_uuid) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s) \
               ON CONFLICT (project_name, data_provider_id, pipeline_step, run_uuid) DO UPDATE SET pipeline_status=%s, patients_processed=%s, patients_with_warnings=%s, patients_with_errors=%s, elapsed_time=%s, execution_time=%s"
        self.cursor.execute(sql, (project_name, self.config.data_provider_id, pipeline_step.value, pipeline_status.value, patients_processed, patients_with_warnings, patients_with_errors, execution_time, elapsed_time, run_uuid, pipeline_status.value,
                                           patients_processed, patients_with_warnings, patients_with_errors, elapsed_time, execution_time))
        if not parallel:
            self.close_connection()

def bucket_exists(bucket: str, config: Config) -> bool:
    """Check if bucket exists

    Args:
        bucket (str): bucket name
        config (Config): Config object

    Returns:
        bool: bucket exists
    """
    try:
        config.s3_client.head_bucket(Bucket=bucket)
        return True
    except ClientError as e:
        return not e.response['Error']['Code'] == '404'
