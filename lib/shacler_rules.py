#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# TThis file: 'shacler_rules.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

import io
import os
from datetime import datetime
import subprocess
import sys
from rdflib import Graph
from rdflib.plugins.sparql import prepareQuery

sys.path.append(os.path.abspath(os.path.dirname(__file__)))
from config import Config
from database import Database, bucket_exists
from connector import Connector
from typing import Optional
from api import create_s3_bucket
from enums import Enum, Shacler


class Shacl(Connector):
    """class for interactions with the Shacler to generate shacl rules from a given schema"""

    def __init__(self, project: str):
        """Class constructor

        Args:
            project (str): name of the project
        """
        self.config = Config()
        self.database = Database(project=project)
        self.config.data_provider_id = self.database.config.data_provider_id

    @staticmethod
    def _download_shacler_files(project_name: str):
        """Download locally main schema/SHACL files for a project

        Args:
            project_name (str): name of the project
        """
        shacler = Shacl(project=project_name)
        bucket = shacler.config.s3_connector_bucket
        shacler_folder = os.path.join("/home/sphn/shacler", project_name)
        os.makedirs(shacler_folder, exist_ok=True)
        if bucket_exists(bucket=bucket, config=shacler.config):

            schemas = shacler.database.list_objects_keys(bucket=bucket, prefix=f"{project_name}/Schema/")
            schemas = [schema.replace(f"{project_name}/Schema/", "") for schema in schemas]

            exceptions = shacler.database.list_objects_keys(bucket=bucket, prefix=f"{project_name}/QAF/Exceptions/")
            if exceptions:
                exceptions = [exception.replace(f"{project_name}/QAF/SCHACL/", "") for exception in exceptions]
            else:
                exceptions = None

            shacls = shacler.database.list_objects_keys(bucket=bucket, prefix=f"{project_name}/QAF/SHACL/")
            if shacls:
                shacls = [shacls.replace(f"{project_name}/QAF/SCHACL/", "") for shacls in shacls]
            else:
                shacls = None
            ext_schemas = shacler.database.list_objects_keys(bucket=bucket, prefix=f"{project_name}/QAF/Schemas/")
            if ext_schemas:
                ext_schemas = [ext_schemas.replace(f"{project_name}/QAF/Schemas/", "") for ext_schemas in ext_schemas]
            else:
                ext_schemas = None

            if schemas:
                schema_folder = os.path.join(shacler_folder, "schemas")
                os.makedirs(schema_folder, exist_ok=True)
                for schema in schemas:
                    file_path = os.path.join(schema_folder, schema)
                    object_name = f"{project_name}/Schema/" + str(schema)
                    msg = f"{str(schema)} [Schema] downloaded for Shacler"
                    shacler.update_shacler_logging(project_name=project_name, first_shacler_status=Shacler.SCHEMA_FILE, message=msg)
                    shacler.config.s3_client.download_file(Bucket=bucket, Key=object_name, Filename=file_path)
                    break

            if exceptions:
                exceptions_folder = os.path.join(shacler_folder, "exceptions")
                os.makedirs(exceptions_folder, exist_ok=True)
                for exception in exceptions:
                    file_path = os.path.join(exceptions_folder, exception)
                    object_name = f"{project_name}/QAF/Exceptions/" +str(exception)
                    msg = f"{str(exception)} [Exception] downloaded for Shacler"
                    shacler.update_shacler_logging(project_name=project_name, first_shacler_status=Shacler.EXCEPTION, message=msg)
                    shacler.config.s3_client.download_file(Bucket=bucket, Key=object_name, Filename=file_path)
                    break

            if shacls:
                shacl_folder = os.path.join(shacler_folder, "shapes")
                os.makedirs(shacl_folder, exist_ok=True)
                for shacl in shacls:
                    file_path = os.path.join(shacl_folder, shacl)
                    object_name = str(shacl)
                    #If we encountered an already existing, created Shacl file we need to delete it
                    if object_name == f"{project_name}/QAF/SHACL/Shacler_{project_name}_shacl.ttl":
                        shacler.config.s3_client.delete_objects(Bucket=bucket, Delete={"Objects": [{"Key": object_name}]})
                        msg = f"{object_name}: Created Shacl file found. Due to re-init file was deleted."
                        shacler.update_shacler_logging(project_name=project_name, first_shacler_status=Shacler.REINIT, message=msg)
                    else:
                        msg = f"{str(object_name)} [External Shacl file] downloaded for Shacler"
                        shacler.update_shacler_logging(project_name=project_name, first_shacler_status=Shacler.SHACL, message=msg)
                        shacler.config.s3_client.download_file(Bucket=bucket, Key=object_name, Filename=file_path)
                        break

            if ext_schemas:
                ext_schema_folder = os.path.join(shacler_folder, "ext_schemas")
                os.makedirs(ext_schema_folder, exist_ok=True)
                for ext_ont in ext_schemas:
                    file_path = os.path.join(ext_schema_folder, ext_ont)
                    object_name = f"{project_name}/QAF/Schemas/{str(ext_ont)}"
                    msg = f"{str(ext_ont)} [External schema] downloaded for Shacler"
                    shacler.update_shacler_logging(project_name=project_name, first_shacler_status=Shacler.EXT_SCHEMA_FILE, message=msg)
                    shacler.config.s3_client.download_file(Bucket=bucket, Key=object_name, Filename=file_path)
                    break
        else:
            print(f"No project named {project_name} found")
            shacler.update_shacler_logging(project_name=project_name, first_shacler_status=Shacler.NO_EXT)

    def upload_data(self, filename: str, file_path: str):
        """Uploads Output of the Shacler after creation

        Args:
            filename (str): name of the file
            file_path (str): file location
        """
        bucket_name = self.config.s3_connector_bucket
        create_s3_bucket(bucket=bucket_name, config=self.config)
        with open(file=file_path, mode="rb") as datafile:
            content = datafile.read()
            file_size = io.BytesIO(content).getbuffer().nbytes
            self.config.s3_client.put_object(Bucket=bucket_name, Key=filename, Body=io.BytesIO(content),ContentLength=file_size )

    def get_shacler_validation_message(self, 
                                       project_name: str, 
                                       first_shacler_satus: Shacler, 
                                       second_shacler_status: Optional[Enum] = None, message: Optional[str] = None) -> dict:
        """Generate the validation message for the shacler update

        Args:
            project_name (str): name of the project
            first_shacler_satus (Enum): Enum class for the Shacler status
            second_shacler_status (Enum, optional): Enum class for the Shacler status. Default to None.
            message (str, optional): custom message that will be reported as validation message. Default to None.

        Returns:
            dict: Shacler Status and associated validation message
        """
        if second_shacler_status:
            compare_dict = {first_shacler_satus:first_shacler_satus.value, second_shacler_status:second_shacler_status.value}
        else:
            compare_dict = {first_shacler_satus:first_shacler_satus.value}

        validation_message = {}

        if message is None:
            for key, value in compare_dict.items():
                if key == Shacler.NO_EXT:
                    validation_message[value] = f"No external, project specific schema for project: {project_name.upper()} available"
                elif key == Shacler.EXT_SHACL_FILE:
                    validation_message[value] = f"External Shacl File for project: {project_name.upper()} provided"
                elif key == Shacler.NO_SHACL:
                    validation_message[value] = f"No external Shacl file for project: {project_name.upper()} provided. Shacler will be executed"
                elif key == Shacler.NO_EXCEPT:
                    validation_message[value] = f"No exception file for project: {project_name.upper()} provided. Shacler will be executed"
                elif key in [Shacler.EXT_SCHEMA_FILE, Shacler.SHACL, Shacler.EXCEPTION, Shacler.SCHEMA_FILE]:
                    validation_message[value] = f"File for project: {project_name.upper()} beeing downloaded"
                elif key ==  Shacler.STARTED:
                    validation_message[value] = f"Shacler for project: {project_name.upper()} started"
                elif key ==  Shacler.DONE:
                    validation_message[value] = f"Shacler finished for project: {project_name.upper()}"
                elif key ==  Shacler.FAILED:
                    validation_message[value] = f"Shacler failed for project: {project_name.upper()}"
        else:
            validation_message[first_shacler_satus.value] = message

        return validation_message

    def update_shacler_logging(self, 
                               project_name: str, 
                               first_shacler_status: Shacler, 
                               second_shacler_status: Optional[Enum] = None, 
                               message: Optional[str] = None):
        """Updates shacler logs with results

        Args:
            project_name (str): name of the project
            first_shacler_status (Enum): Enum class for the Shacler status
            second_shacler_status (Enum, optional): Enum class for the second Shacler status used when two different status follow each other closely in time. Defaults to None.
            message (str, optional): update message (if available). Defaults to None.

        """
        validation_message = self.get_shacler_validation_message(project_name=project_name, first_shacler_satus=first_shacler_status, second_shacler_status=second_shacler_status, message=message)

        self.database.create_connection()
        for key, value in validation_message.items():
            timestmp = datetime.now()
            sql = "INSERT INTO shacler_logging (project_name, data_provider_id, status, message, timestmp) VALUES \
                    (%s, %s, %s, %s, %s) \
                    ON CONFLICT (project_name, data_provider_id, status) DO UPDATE SET message=EXCLUDED.message, timestmp=EXCLUDED.timestmp"
            self.database.cursor.execute(sql, (project_name, self.config.data_provider_id, key, value, timestmp))
        self.database.close_connection()

    @staticmethod
    def _get_node_shapes(local_shacl_file: str, project_name: str) -> str:
        """Get generated node shapes constraints

        Args:
            local_shacl_file (str): path to generated Shacl file
            project_name (str): name of the project

        Returns:
            str: list of node shapes
        """
        g = Graph()
        g.parse(local_shacl_file, format="turtle")
        query = prepareQuery("""
            PREFIX sh:<http://www.w3.org/ns/shacl#> select distinct ?shape where { ?shape a sh:NodeShape. }
        """)
        shapes = []
        for row in g.query(query):
            shapes.append(row.shape)
        shapes_str = '\n\t\t' + '\n\t\t'.join(shapes)
        return f"Node shapes created for project '{project_name.upper()}':{shapes_str}"

    def check_shacl_generated(self, error_msg: str, output_file: str, shacl_folder: str, project_name: str):
        """Check if the SHACL file has been successfully generated

        Args:
            report_file_loc (str): file location of report
            report_file (str): filename of the report
            output_file (str): filename of the output file
            shacl_folder (str): name of the shacl folder
            project_name (str): name of the project
        """
        local_shacl_file = f"{shacl_folder}/{output_file}"
        if os.path.exists(local_shacl_file):
            self.upload_data(filename=f"{project_name}/QAF/SHACL/{output_file}", file_path=local_shacl_file)
            self.update_shacler_logging(project_name=project_name, first_shacler_status=Shacler.SHAPES, message=Shacl._get_node_shapes(local_shacl_file=local_shacl_file, project_name=project_name))
            self.update_shacler_logging(project_name=project_name, first_shacler_status=Shacler.DONE)
        else:
            self.update_shacler_logging(project_name=project_name, first_shacler_status=Shacler.DONE_ERROR, message=f"SHACL file not successfully generated:\n{error_msg}")
            raise Exception("SHACL file not generated")

    @staticmethod
    def _generate_shacl(project_name: str):
        """Create Shacler rules automatically from the SPHN schema or in combination with a project specific one

        Args:
            project_name (str): name of the project
        """
        # Path definitions to all files necessary
        # Paths are available if the files exist in S3
        # Shacler will abort if the Shacl folder exist

        shacler = Shacl(project=project_name)
        shacler_folder = os.path.join("/home/sphn/shacler", project_name)
        sphn_schema_folder = os.path.join(shacler_folder, "schemas")
        shacl_folder = os.path.join(shacler_folder, "shapes")
        exceptions_folder = os.path.join(shacler_folder, "exceptions")
        project_schema_folder = os.path.join(shacler_folder, "ext_schemas")
        output_file = f"Shacler_{project_name}_shacl.ttl"

        if os.path.exists(shacl_folder):
            warning_message = f"Shacler ended since external shacl files have been provided for project: {project_name}"
            print("INFO --" + warning_message)
            cmd = Shacler.DONE
        elif os.path.exists(exceptions_folder):
            # an exception file is available
            os.makedirs(shacl_folder, exist_ok=True)
            sphn_schema = os.listdir(sphn_schema_folder)
            exception = os.listdir(exceptions_folder)
            shacler.update_shacler_logging(project_name=project_name, first_shacler_status=Shacler.NO_SHACL)
            if os.path.exists(project_schema_folder):
                shacler.update_shacler_logging(project_name=project_name, first_shacler_status=Shacler.STARTED)
                # there is a project specific schema
                project_schema = os.listdir(project_schema_folder)

                # ** ATTENTION: If a project specific schema is provided it is uploaded under the path of the SPHN-Schema.
                # ** That means that the variable `sphn_schema` is actually the project_schema
                # ** Therefore we need to switch the arguments when we call `produceProjectShacl` in a counterintuitive way
                # ** produceProjectShacl expects: `project_schema` `sphn_schema` as positional arguments
                # ** for more information review: https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-shacl-generator

                cmd = f"python /home/sphn/shacler/shacler.py produceProjectShacl -type turtle {sphn_schema_folder}/{sphn_schema[0]} {project_schema_folder}/{project_schema[0]} {shacl_folder}/{output_file} -e {exceptions_folder}/{exception[0]}"
            else:
                # there is no project specific schema
                shacler.update_shacler_logging(project_name=project_name, first_shacler_status=Shacler.NO_EXT, second_shacler_status=Shacler.STARTED)
                cmd = f"python /home/sphn/shacler/shacler.py produceSphnShacl -type turtle {sphn_schema_folder}/{sphn_schema[0]} {shacl_folder}/{output_file} -e {exceptions_folder}/{exception[0]}"
        else:
            os.makedirs(shacl_folder, exist_ok=True)
            sphn_schema = os.listdir(sphn_schema_folder)
            shacler.update_shacler_logging(project_name=project_name, first_shacler_status=Shacler.NO_SHACL, second_shacler_status=Shacler.NO_EXCEPT )
            if os.path.exists(project_schema_folder):
                shacler.update_shacler_logging(project_name=project_name, first_shacler_status=Shacler.STARTED)
                # there is a project specific schema
                project_schema = os.listdir(project_schema_folder)

                # ** ATTENTION: If a project specific schema is provided it is uploaded under the path of the SPHN-Schema.
                # ** That means that the variable `sphn_schema` is actually the project_schema
                # ** Therefore we need to switch the arguments when we call `produceProjectShacl` in a counterintuitive way
                # ** produceProjectShacl expects: `project_schema` `sphn_schema` as positional arguments
                # ** for more information review: https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-shacl-generator

                cmd = f"python /home/sphn/shacler/shacler.py produceProjectShacl -type turtle {sphn_schema_folder}/{sphn_schema[0]} {project_schema_folder}/{project_schema[0]} {shacl_folder}/{output_file}"
            else:
                # there is no project specific schema
                shacler.update_shacler_logging(project_name=project_name, first_shacler_status=Shacler.NO_EXT, second_shacler_status=Shacler.STARTED)
                cmd = f"python /home/sphn/shacler/shacler.py produceSphnShacl -type turtle {sphn_schema_folder}/{sphn_schema[0]} {shacl_folder}/{output_file}"

        if cmd == Shacler.DONE:
            shacler.update_shacler_logging(project_name=project_name, first_shacler_status=Shacler.EXT_SHACL_FILE, second_shacler_status=Shacler.DONE)
        else:
            p = subprocess.Popen(cmd.split(' '), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            _, error = p.communicate()
            error_msg = error.decode()
            if p.returncode == 0:
                shacler.check_shacl_generated(error_msg=error_msg, output_file=output_file, shacl_folder=shacl_folder, project_name=project_name)
            else:
                # everything that does not result in returncode == 0 is an error
                error_message = f'SHACL generation for project {project_name} failed:\n{error_msg}'
                shacler.update_shacler_logging(project_name=project_name, first_shacler_status=Shacler.FAILED, message=error_message)
                # if the return is not 0 / okay we need to let the execution fail to prevent the initialization of an unfit project
                # report is printed out so Airflow can catch it for its exception message. Avoids duplication in log-file
                raise Exception(f"Failed execution of {cmd}")