#!/bin/bash

#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'execute_rml_mapper.sh' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

echo "Calling the rmlmapper"
env

java "$1" -jar "$2" -c "$3" &
pid=$!

echo "exiting the shell starter..."

wait "$pid"
java_exit_code=$?

if [[ $java_exit_code -ne 0 ]]; then
    exit $java_exit_code
fi