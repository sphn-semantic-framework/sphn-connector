""" functionality for the 'realtime' counts for sphn connector overview dashboard in grafana """

#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# This file: 'grafana_counts.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from itertools import product
from enum import Enum
from database import Database
from enums import Steps, AirflowDag, DataPersistenceLayer
from airflow_lib import Airflow
import os

def get_project_names() -> list:
    """Extracts the names of all configured project inside the SPHN Connector.

    Returns:
        list[str]: list of project names
    """
    database = Database()
    database.create_connection()
    project_name_query = "SELECT project_name from configuration"
    database.cursor.execute(query=project_name_query)
    project_names = [table_name[0] for table_name in database.cursor.fetchall()]
    database.close_connection()
    return project_names


def extract_patient_count_from_zone(project_name: str, zone: Enum):
    """Extracts the number of patients processed from a pipeline run

    Args:
        project_name (str): name of the project
        zone (Enum): Steps class instance
    """
    database = Database(project=project_name)
    database.create_connection()
    zone_count = f"SELECT count(*) FROM {zone.value} WHERE project_name=%s AND data_provider_id=%s"
    database.cursor.execute(
        zone_count, (project_name, database.config.data_provider_id)
    )
    total = database.cursor.fetchone()[0]
    database.close_connection()
    return total


def update_grafana_real_time_report(project_name: str, step: Enum, patients_processed: int):
    """Updates grafana_real_time_report with the final processed patient count

    Args:
        project_name (str): name of the project
        step (Enum): Steps class instance
        patients_processed (int): number of patients processed
    """

    database = Database(project=project_name)
    database.create_connection()
    insert_processed_patients = """
    INSERT INTO grafana_real_time_report (data_provider_id, project_name, step, patients_processed)
    VALUES (%s, %s, %s, %s) ON CONFLICT (data_provider_id, project_name, step)
    DO UPDATE SET patients_processed=EXCLUDED.patients_processed"""
    database.cursor.execute(
        insert_processed_patients,
        (
            database.config.data_provider_id,
            project_name,
            step.value,
            patients_processed,
        ),
    )
    database.close_connection()


def update_processed_patients_with_history(project_name: str, step: Enum):
    """After the successful execution of the "main" task like 'run_QAF_validation'
       or 'run_integration_task' extracts the final count of all processed patient files
       and updates the 'grafana_real_time_report' for the sphn connector overview dashboard

    Args:
        project_name (str): name of the project
        step (Enum): Steps class instance
    """
    patients_in_release_zone = extract_patient_count_from_zone(
            project_name=project_name, zone=DataPersistenceLayer.RELEASE_ZONE
        )
    patients_in_graph_zone = extract_patient_count_from_zone(
            project_name=project_name, zone=DataPersistenceLayer.GRAPH_ZONE
        )
    delta = patients_in_graph_zone - patients_in_release_zone

    if step == Steps.VALIDATION:
        # update the value for the release zone
        update_grafana_real_time_report(
            project_name=project_name, step=step, patients_processed=patients_in_release_zone
        )
        if delta == 0:
            # if delate is 0 all patients have been moved into the release zone
            # Therefore the count of patients 'waiting to be processed' in the Graph zone 
            # Integration step can be set to 0
            update_grafana_real_time_report(
            project_name=project_name, step=Steps.INTEGRATION, patients_processed=0
        )
        else:
        # in case the user only validates some patients we adjust the displayed amount accordingly
            update_grafana_real_time_report(
            project_name=project_name, step=Steps.INTEGRATION, patients_processed=delta
        )
    else:
        # for the integration and statistic step
        if delta == 0:
            update_grafana_real_time_report(
            project_name=project_name, step=step, patients_processed=patients_in_graph_zone
        )
        else:
            # in case the user only validates some patients we adjust the displayed amount accordingly
            update_grafana_real_time_report(
            project_name=project_name, step=step, patients_processed=delta
        )


def add_grafana_initial_count(project_name: str):
    """Add initial values of 0 to the table 'grafana_real_time_report'
       during the execution of the PreCheck/De-Id step.

    Args:
        project_name (str): name of the project
    """

    database = Database(project=project_name)

    database.create_connection()

    setup_statement = """
    INSERT INTO grafana_real_time_report (data_provider_id, project_name, step, patients_processed, project_size)
    VALUES (%s, %s, %s, %s, %s);
    """
    for step in [Steps.INTEGRATION, Steps.VALIDATION, Steps.STATISTICS]:
        database.cursor.execute(
            setup_statement,
            (
                database.config.data_provider_id,
                project_name,
                step.value,
                0,
                0
            ),
        )
    database.close_connection()


def update_grafana_realtime_count():
    """Updates the grafana_real_time_report table to visual the progress in the Connector
       in the Grafana SPHN - Connector Dashboard.
       counts for the step PRE_CHECK_DE_ID step are extracted via the Airflow class method 'get_status'
       for the steps 'Integration' and 'Validation' the method _count_patients_processed from the
       'Database' class is used.
    """

    airflow = Airflow()

    steps_to_dags_dict = {
        Steps.INTEGRATION: AirflowDag.RUN_INTEGRATION,
        Steps.VALIDATION: AirflowDag.RUN_VALIDATION,
        Steps.STATISTICS: AirflowDag.RUN_STATISTICS,
    }

    projects = get_project_names()
    all_combinations = product(projects, steps_to_dags_dict.keys())
    for project_name, step in all_combinations:
        database = Database(project=project_name)
        try:
            dag = airflow.get_latest_dag(
                project=project_name, dag_id=steps_to_dags_dict[step].value
            )
            dag_state = dag.get("state")
            if dag_state == "running":
                database._count_patients_processed(
                    project_name=project_name, step=step, grafana=True
                )
        except KeyError:
            print("Pipeline not yet executed")
            continue


def extract_sizes(project_name: str, zone: DataPersistenceLayer) -> int:
    """For a given project name and zone extracts the file size of all
       present patients file for reporting.

    Args:
        project_name (str): name of the project
        zone (DataPersistenceLayer): zone to check

    Returns:
        int: file size in bits
    """
    database = Database(project=project_name)
    total_size = 0
    if zone == DataPersistenceLayer.RELEASE_ZONE:
        prefix = f"{project_name}/Output"
    else:
        prefix = os.path.join(project_name, "Input", zone.value)
    
    objects = database.list_objects(bucket=database.config.s3_connector_bucket, prefix=prefix)
    for object in objects:
        total_size += object['Size']
    return total_size

def save_file_sizes(project_name: str, zone: DataPersistenceLayer, step: Steps):
    """"Reports the extracts files sizes for a given project and zone in
        the 'grafana_real_time_report' table.
    
    Args:
        project_name (str): name of the project
        zone (DataPersistenceLayer): zone to check
        step (Steps): name of the step
    """
    database = Database(project=project_name)
    database.create_connection()
    size = extract_sizes(project_name=project_name, zone=zone)
    sql = """INSERT INTO grafana_real_time_report (data_provider_id, project_name, step, project_size)
    VALUES (%s, %s, %s, %s) ON CONFLICT (data_provider_id, project_name, step)
    DO UPDATE SET project_size=EXCLUDED.project_size"""
    database.cursor.execute(
        sql,
        (
            database.config.data_provider_id,
            project_name,
            step.value,
            size,
        ),
    )
    database.close_connection()
