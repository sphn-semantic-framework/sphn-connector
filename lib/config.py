#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'config.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#


from datetime import datetime
import os
import json
import psutil
import boto3
from botocore.client import Config as botocoreConfig


class Config(object):
    """
    Class defining SPHN connector configuration. It defines all the configuration details specified in the .env file and then reported 
    during startup to the JSON file config.json
    """
    def __init__(self) -> None:
        """
        Class constructor
        """
        config_file = "/home/sphn/code/config.json"
        config_data = json.load(open(config_file))

        # General configuration details
        self.data_provider_id = config_data.get("data_provider_id")

        # API configuration details
        self.api_secret_key = config_data.get("api", {}).get('secret_key')
        self.api_user = config_data.get("api", {}).get("user")
        self.api_password = config_data.get("api", {}).get("password")

        # S3 configuration details
        self.use_external_s3 = str(config_data.get('external_s3', {}).get('activated', False)) == "True"
        if self.use_external_s3:
            self.s3_access_key = config_data.get("external_s3", {}).get("access_key")
            self.s3_secret_key = config_data.get("external_s3", {}).get("secret_key")
            self.s3_compression = None
            self.s3_endpoint = config_data.get("external_s3", {}).get("URL")
            if not self.s3_endpoint.startswith('https://'):
                self.s3_endpoint = 'https://' + self.s3_endpoint
            self.s3_connector_bucket = config_data.get("external_s3", {}).get('connector_bucket', 'connector')
            self.s3_einstein_bucket = config_data.get("external_s3", {}).get('einstein_bucket', 'einstein')
        else:
            self.s3_access_key = config_data.get("minio", {}).get("access_key")
            self.s3_secret_key = config_data.get("minio", {}).get("secret_key")
            self.s3_compression = config_data.get("minio", {}).get("compression", "OFF")
            self.s3_endpoint = "http://minio:9000"
            self.s3_connector_bucket = 'connector'
            self.s3_einstein_bucket = 'einstein'

                
        if self.use_external_s3 and os.path.exists('/etc/ssl/certs/root-certificate.crt'):
            self.s3_client = boto3.client('s3', endpoint_url=self.s3_endpoint, aws_access_key_id=self.s3_access_key,
                                          aws_secret_access_key=self.s3_secret_key, config=botocoreConfig(signature_version='s3v4'),
                                          region_name='eu-central-1', verify='/etc/ssl/certs/root-certificate.crt')
        else:
            self.s3_client = boto3.client('s3', endpoint_url=self.s3_endpoint, aws_access_key_id=self.s3_access_key,
                                          aws_secret_access_key=self.s3_secret_key, config=botocoreConfig(signature_version='s3v4'),
                                          region_name='eu-central-1')

        #PostgreSQL configuration details
        self.postgres_user = config_data.get("postgres", {}).get("user")
        self.postgres_password = config_data.get("postgres", {}).get("password")
        self.postgres_ui_email = config_data.get("postgres", {}).get("ui_email")
        self.postgres_ui_password = config_data.get("postgres", {}).get("ui_password")

        #Airflow configuration details
        self.airflow_user = config_data.get("airflow", {}).get("user")
        self.airflow_password = config_data.get("airflow", {}).get("password")
        self.airflow_firstname = config_data.get("airflow", {}).get("firstname")
        self.airflow_lastname = config_data.get("airflow", {}).get("lastname")
        self.airflow_email = config_data.get("airflow", {}).get("email")

        # Grafana configuration details
        self.grafana_user = config_data.get("grafana", {}).get("user")
        self.grafana_password = config_data.get("grafana", {}).get("password")
        self.grafana_postgres_user = config_data.get("grafana", {}).get("postgres_user")
        self.grafana_postgres_user_password = config_data.get("grafana", {}).get("postgres_user_password")

        # Machine hardware
        if config_data.get('machine', {}).get('memory') is not None:
            self.available_memory = int(config_data.get('machine', {}).get('memory')) - 6
        else:
            self.available_memory = psutil.virtual_memory().available/1024**3
        if config_data.get('machine', {}).get('cpus') is not None:
            self.machine_cpus = int(config_data.get('machine', {}).get('cpus'))
        else:
            self.machine_cpus = psutil.cpu_count(logical=True)

        # Parallelization option for the Connector
        self.parallel_projects = int(config_data.get('parallelization', {}).get('parallel_projects', 3))

        # Einstein Relativity configuration
        self.einstein = str(config_data.get('einstein', {}).get('activated', False)).upper() == "TRUE"
        self.einstein_endpoint = config_data.get('einstein', {}).get('endpoint')
        if self.einstein_endpoint is not None and not self.einstein_endpoint.startswith('https://'):
            self.einstein_endpoint = f'https://{self.einstein_endpoint}'
        self.einstein_api_user = config_data.get('einstein', {}).get('api_user')
        self.einstein_api_password = config_data.get('einstein', {}).get('api_password')
           
        #Parsing the input for the ACCESS_TOKEN_TIMEOUT setting provided in .env file
        # In case the user provides an invalid value (like negative integers)
        # we take the absolute value of the user input
        # In case we are not able to convert it to a valid integers we default to the standard value of 3.
        try:
            self.access_token_timeout = abs(int(config_data.get('token_timeout', {}).get('access_token_timeout', 3)))
        except ValueError as wrong_value:
            print(f"ERROR: {wrong_value}:\nValue is invalid for ACCESS_TOKEN_TIMEOUT in configuration file 'config.json'\nOnly positive integers are allowed. Defaulting to standard setting: '3'")
            self.access_token_timeout = 3


        self.commit_hash = config_data.get('version', {}).get('commit_hash')
        if config_data.get('version', {}).get('is_tag') == 'true':
            self.is_tag = True
        else:
            self.is_tag = False
        self.version_name = config_data.get('version', {}).get('version_name')
        self.commit_timestamp = config_data.get('version', {}).get('commit_timestamp')
        if self.commit_timestamp:
            self.commit_timestamp = datetime.strptime(self.commit_timestamp, "%Y-%m-%d %H:%M:%S %z")
        else:
            self.commit_timestamp = datetime.now()

        self._perform_validation()

    def _perform_validation(self):
        """
        Checks that config.json configuration file has all mandatory fields defined
        """
        if self.data_provider_id is None:
            print("ERROR - Parameter 'DATA_PROVIDER_ID' is mandatory in '.env' file")
            exit(1)
        if self.api_user is None:
            print("ERROR - Parameter 'API_USER' is mandatory in '.env' file")
            exit(1)
        elif self.api_password is None:
            print("ERROR - Parameter 'API_PASSWORD' is mandatory in '.env' file")
            exit(1)
        elif self.s3_access_key is None:
            if self.use_external_s3:
                print("ERROR - Parameter 'EXTERNAL_S3_ACCESS_KEY' is mandatory in '.env' file")
            else:
                print("ERROR - Parameter 'MINIO_ACCESS_KEY' is mandatory in '.env' file")
            exit(1)
        elif self.s3_secret_key is None:
            if self.use_external_s3:
                print("ERROR - Parameter 'EXTERNAL_S3_SECRET_KEY' is mandatory in '.env' file")
            else:
                print("ERROR - Parameter 'MINIO_SECRET_KEY' is mandatory in '.env' file")
            exit(1)
        elif self.postgres_user is None:
            print("ERROR - Parameter 'POSTGRES_USER' is mandatory in '.env' file")
            exit(1)
        elif self.postgres_password is None:
            print("ERROR - Parameter 'POSTGRES_PASSWORD' is mandatory in '.env' file")
            exit(1)
        elif self.postgres_ui_email is None:
            print("ERROR - Parameter 'POSTGRES_UI_EMAIL' is mandatory in '.env' file")
            exit(1)
        elif self.postgres_ui_password is None:
            print("ERROR - Parameter 'POSTGRES_UI_PASSWORD' is mandatory in '.env' file")
            exit(1)
        elif self.airflow_user is None:
            print("ERROR - Parameter 'AIRFLOW_USER' is mandatory in '.env' file")
            exit(1)
        elif self.airflow_password is None:
            print("ERROR - Parameter 'AIRFLOW_PASSWORD' is mandatory in '.env' file")
            exit(1)
        elif self.airflow_firstname is None:
            print("ERROR - Parameter 'AIRFLOW_FIRSTNAME' is mandatory in '.env' file")
            exit(1)
        elif self.airflow_lastname is None:
            print("ERROR - Parameter 'AIRFLOW_LASTNAME' is mandatory in '.env' file")
            exit(1)
        elif self.airflow_email is None:
            print("ERROR - Parameter 'AIRFLOW_EMAIL' is mandatory in '.env' file")
            exit(1)
        elif self.grafana_user is None:
            print("ERROR - Parameter 'GRAFANA_ADMIN_USER' is mandatory in '.env' file")
            exit(1)
        elif self.grafana_password is None:
            print("ERROR - Parameter 'GRAFANA_ADMIN_PASSWORD' is mandatory in '.env' file")
            exit(1)
        elif self.grafana_postgres_user is None:
            print("ERROR - Parameter 'GRAFANA_POSTGRES_USER' is mandatory in '.env' file")
            exit(1)
        elif self.grafana_postgres_user_password is None:
            print("ERROR - Parameter 'GRAFANA_POSTGRES_USER_PASSWORD' is mandatory in '.env' file")
            exit(1)
        elif self.einstein:
            if self.einstein_endpoint is None:
                print("ERROR - Parameter 'EINSTEIN_ENDPOINT' is mandatory in '.env' file")
                exit(1)
            elif self.einstein_api_user is None:
                print("ERROR - Parameter 'EXTERNAL_S3_ACCESS_KEY' is mandatory in '.env' file")
                exit(1)
            elif self.einstein_api_password is None:
                print("ERROR - Parameter 'EXTERNAL_S3_SECRET_KEY' is mandatory in '.env' file")
                exit(1)
