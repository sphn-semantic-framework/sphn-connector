INSERT INTO "test-project"."sphn_DataRelease" (
    "sphn_hasSubjectPseudoIdentifier__id", 
    "sphn_hasDataProvider__id", 
    "id", 
    "sphn_hasExtractionDateTime"
    ) 
    VALUES (
        'db_ok', 
        'DATA-PROVIDER-ID', 
        '1666216800', 
        '2022-10-20T12:00:00.000'
        );

INSERT INTO "test-project"."sphn_SubjectPseudoIdentifier" (
	"id", 
    "sphn_hasIdentifier",
    "sphn_hasDataProvider__id",
    "sphn_hasSharedIdentifier"
    )
    VALUES (
        'db_ok', 
        'db_ok', 
        'DATA-PROVIDER-ID',
        'shared_1'
        );

INSERT INTO "test-project"."sphn_SubjectPseudoIdentifier" (
	"id", 
    "sphn_hasIdentifier",
    "sphn_hasDataProvider__id",
    "sphn_hasSharedIdentifier"
    )
    VALUES (
        'db_ok', 
        'db_ok', 
        'DATA-PROVIDER-ID',
        'shared_2'
        );

INSERT INTO "test-project"."sphn_DataProvider" (
	"id", 
    "sphn_hasInstitutionCode__id", 
    "sphn_hasInstitutionCode__sphn_hasIdentifier", 
    "sphn_hasInstitutionCode__sphn_hasCodingSystemAndVersion",
    "sphn_hasCategory__iri",
    "sphn_hasDepartment__id",
    "sphn_hasDepartment__sphn_hasName"
    )
    VALUES (
        'DATA-PROVIDER-ID', 
        'test_data_provider', 
        'DGG_346_346_233', 
        'UID',
        'https://biomedit.ch/rdf/sphn-schema/sphn/individual#Hospital',
        'department01',
        'department01Name'
        );

INSERT INTO "test-project"."sphn_Allergy" (
	"sphn_hasSubjectPseudoIdentifier__id", 
    "sphn_hasDataProvider__id", 
    "id", 
    "sphn_hasLastReactionDateTime", 
    "sphn_hasFirstRecordDateTime",
    "sphn_hasAllergen__id",
    "sphn_hasAllergen__sphn_hasCode__id", 
    "sphn_hasAllergen__sphn_hasCode__sphn_hasCodingSystemAndVersion",
    "sphn_hasAllergen__sphn_hasCode__sphn_hasIdentifier",
    "sphn_hasAllergen__sphn_hasCode__sphn_hasName",
    "sphn_hasSourceSystem__id"
    )
	VALUES (
        'db_ok', 
        'DATA-PROVIDER-ID',
        'allergy01',
        '2022-03-31T11:55:43.304Z', 
        '2022-04-24T11:55:43.304Z', 
        'allergen01',
        'TestInternal-test_pf4n3cke3',
        'TEST Internal-TEST', 
        'P4dgneKf',
        'PF4N3Cke3',
        'sourceSystem01'
        );

INSERT INTO "test-project"."sphn_SourceSystem"(
	"id", 
    "patient_id", 
    "sphn_hasDataProvider__id", 
    "sphn_hasName"
    )
	VALUES (
        'sourceSystem01',
        'db_ok',
        'DATA-PROVIDER-ID',
        'sourceSystem01Name'
    );

INSERT INTO "test-project"."supporting__sphn_Interpretation" (
    "id", 
    "patient_id",
    "sphn_hasOutput__Allergy__id",
    "sphn_hasSourceSystem__id"
    )
    VALUES (
        'allergyInterpretation',
        'db_ok',
        'allergy01',
        'sourceSystem01'
    );

INSERT INTO "test-project"."supporting__sphn_Interpretation" (
    "id", 
    "patient_id",
    "sphn_hasOutput__Code__id",
    "sphn_hasOutput__Code__sourceConceptType",
    "sphn_hasOutput__Code__sourceConceptID",
    "sphn_hasSourceSystem__id"
    )
    VALUES (
        'allergyInterpretation',
        'db_ok',
        'TestInternal-test_pf4n3cke3',
        'sphn-Allergen',
        'allergen01',
        'sourceSystem01'
    );

INSERT INTO "test-project"."supporting__sphn_Interpretation" (
    "id", 
    "patient_id",
    "sphn_hasOutput__SourceSystem__id",
    "sphn_hasSourceSystem__id"
    )
    VALUES (
        'sourceSystemInterpretation',
        'db_ok',
        'sourceSystem01',
        'sourceSystem01'
    );
INSERT INTO "test-project"."supporting__sphn_SemanticMapping" (
    "id",
    "patient_id",
    "sphn_hasDataProvider__id",
    "sphn_hasOutputCode__id",
    "sphn_hasOutputCode__sourceConceptType",
    "sphn_hasOutputCode__sourceConceptID",
    "sphn_hasSourceSystem__id"
    )
    VALUES (
        'semanticMapping01',
        'db_ok',
        'DATA-PROVIDER-ID',
        'TestInternal-test_pf4n3cke3',
        'sphn-Allergen',
        'allergen01',
        'sourceSystem01'
    );

INSERT INTO "test-project"."supporting__sphn_SourceData" (
    "id",
    "patient_id",
    "sphn_hasSourceSystem__id"
    )
    VALUES (
        'sourceData01',
        'db_ok',
        'sourceSystem01'
    );