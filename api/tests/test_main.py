# 
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'test_main.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#
# 
# 
# Run all tests: 
#   docker exec -it api python -m pytest -vv
# Run single tests: 
#   docker exec -it api python api/tests/test_main.py TestApiEndpoints.test_api
#   docker exec -it api python api/tests/test_main.py TestApiEndpoints.test_project_configuration
#   docker exec -it api python api/tests/test_main.py TestApiEndpoints.test_data_ingestion_and_processing
#   docker exec -it api python api/tests/test_main.py TestApiEndpoints.test_users_management

import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'app'))
from enums import FileClass, FileType, UserType
import io
import requests
from datetime import datetime
import json
import unittest
import time
from passlib.context import CryptContext
from io import BytesIO
from zipfile import ZipFile
from fastapi.testclient import TestClient
from main import app
from config import Config
from database import Database
from airflow_lib import Airflow
from api import get_object_name
import zipfile
import warnings
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
urllib3.disable_warnings(urllib3.exceptions.DependencyWarning)


client = TestClient(app)
config = Config()
database = Database()
token_response = client.post("/token", data={"username": config.api_user, "password": config.api_password})
headers = {'Authorization': 'Bearer ' + token_response.json()['access_token']}
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
sphn_schema_content = requests.get('https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/raw/2025-1/rdf_schema/sphn_rdf_schema.ttl?ref_type=tags?inline=false', verify=False).content

def create_new_user(user: str, password: str, user_type: UserType = UserType.STANDARD):
    """
    Creates new API user
    :param user: API user to create
    :param password: API password
    """
    response = client.post("/Create_new_user", data={"user": user,"password": password}, params={"user_type": user_type.value}, headers=headers)
    return response

def create_project(project_name: str, testing: bool = True):
    """
    Initialize a project
    :param project_name: name of the project
    :param testing: testing setup
    """
    schema_files = {"sphn_schema": ("sphn_schema_2025.1.ttl", sphn_schema_content)}
    response = client.post("/Create_project", params={"project": project_name, "testing": testing, "generate_sparql": False}, files=schema_files, headers=headers)
    database.mark_project_as_initialized_in_db(project=project_name)
    return response

def project_exists(project_name: str) -> bool:
    """Check if project exists

    Args:
        project_name (str): name of the project

    Returns:
        bool: project exists
    """
    database.create_connection()
    sql = "SELECT COUNT(*) FROM configuration WHERE project_name=%s"
    database.cursor.execute(sql, (project_name, ))
    count = database.cursor.fetchone()[0]
    database.close_connection()
    return count != 0

def delete_project(project_name: str):
    """
    Delete a project
    :param project_name: name of the project
    """
    if project_exists(project_name=project_name):
        response = client.delete("/Delete_project", params={"project": project_name}, headers=headers)
        return response

def delete_user(user: str):
    """
    Deletes API user
    :param user: API user to delete
    """
    response = client.delete("/Delete_user", params={"user": user}, headers=headers)
    return response

def fill_project_tables(project_name: str):
    """
    Puts some dummy data related to the project into database tables
    :param project_name: name of the project
    """
    database.create_connection()
    timestmp = datetime.now()
    sql = "INSERT INTO release_zone (project_name, patient_id, data_provider_id, object_name, validated_ok, timestmp) VALUES (%s, %s, %s, %s, %s, %s)"
    database.cursor.execute(sql, (project_name, 'patient_id', 'DATA-PROVIDER-ID', 'object_name', True, timestmp, ))
    sql = "INSERT INTO logging (project_name, patient_id, data_provider_id, step ,status, message, timestmp) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    database.cursor.execute(sql, (project_name, 'patient_id', 'DATA-PROVIDER-ID', 'Validation', 'LOADING', 'validation_message', timestmp, )) 
    sql = "INSERT INTO graph_zone (project_name, patient_id, data_provider_id, object_name, processed, timestmp, size) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    database.cursor.execute(sql, (project_name, 'patient_id', 'DATA-PROVIDER-ID', 'object_name', True, timestmp, 1))
    sql = "INSERT INTO refined_zone (project_name, patient_id, data_provider_id, object_name, processed, timestmp, size) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    database.cursor.execute(sql, (project_name, 'patient_id', 'DATA-PROVIDER-ID', 'object_name', True, timestmp, 1))
    sql = "INSERT INTO pipeline_history (project_name, data_provider_id, pipeline_step, pipeline_status, patients_processed, patients_with_errors, execution_time, elapsed_time, run_uuid) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
    database.cursor.execute(sql, (project_name, 'DATA-PROVIDER-ID', 'Validation', 'SUCCESS', 1, 0, timestmp, 2, 'run_uuid'))
    database.close_connection()

def get_object_data(object_name: str) -> bytes:
    """
    Gets objects data from storage
    :param object_name: name of the object
    """
    try:
        bucket = database.config.s3_connector_bucket
        response = database.config.s3_client.get_object(Bucket=bucket, Key=object_name)["Body"]
        data = response.read()
        return data
    except Exception:
        assert False
    finally:
        try:
            response.close()
            response.release_conn()
        except Exception:
            pass

def get_users_list():
    """
    Returns the list of initialized API users
    """
    response = client.get("/Get_users_list", headers=headers)
    return response

def ingest_json_file(project_name: str, patient_id: str, files: dict):
    """
    Ingest JSON file into SPHN Connector
    :param project_name: name of the project
    :param patient_id: ID of the patient
    :param files: JSON files to ingest
    """
    response = client.post("/Ingest_json", params={"project": project_name, "patient_id": patient_id}, files=files, headers=headers)
    return response

def ingest_rdf_file(project_name: str, patient_id: str, files: dict):
    """
    Ingest RDF file into SPHN Connector
    :param project_name: name of the project
    :param patient_id: ID of the patient
    :param files: RDF files to ingest
    """
    response = client.post("/Ingest_rdf", params={"project": project_name, "patient_id": patient_id}, files=files, headers=headers)
    return response

def object_exists(object_name: str) -> bool:
    """
    Checks if object exists in S3 storage
    :param project_name: name of the project
    :param object_name: name of the object
    """
    head, _ = os.path.split(object_name)
    bucket = database.config.s3_connector_bucket
    object_names = database.list_objects_keys(bucket=bucket, prefix=head + "/")
    if object_names:
        return object_name in object_names
    else:
        return False

def wait_pipeline_execution(project: str, patient_id: str = None, dag_id: str = None):
    """
    Wait until the pipeline execution terminates
    :param patient_id: ID of the patient
    :param step: pipeline step
    """
    airflow_obj = Airflow()
    if dag_id is not None:
        dag_ids = [dag_id]
    else:
        dag_ids = ['run_whole_pipeline', 'run_pre_check_and_de_identification' 'run_integration', 'run_validation']
    for dag_id in dag_ids:
        latest_dag = airflow_obj.get_latest_dag(project=project, dag_id=dag_id, patient_id=patient_id)
        pipeline_status = latest_dag.get('state')
        while pipeline_status is not None and pipeline_status.upper() not in ["SUCCESS", "FAILED"]:
            time.sleep(2)
            latest_dag = airflow_obj.get_latest_dag(project=project, dag_id=dag_id, patient_id=patient_id)
            pipeline_status = latest_dag.get('state')

class ProjectConfigTest(object):
    """
    Class containing methods for checking project configuration functionalities and endpoints
    """
    @staticmethod
    def _check_pre_conditions(existing_project: str):
        """
        Test pre-conditions
        """
        patient_files = {"file": ('test.ttl', io.BytesIO())}
        non_existing_project = "non-existing-project"
        patient_id = "patient-test-id"

        # Check project init
        response = ingest_rdf_file(project_name=non_existing_project, patient_id=patient_id, files=patient_files)
        assert response.status_code == 404
        assert response.json() == {"detail": "Project \'{}\' has not been created yet. Please configure it at endpoint /Create_project".format(non_existing_project)}

        # Initialize project with too short name
        wrong_project_name = 'ab'
        response = create_project(project_name=wrong_project_name)
        assert response.status_code == 400
        assert response.json() == {"detail": "Invalid project name \'{}\'. Allowed number of characters: [3, 63]".format(wrong_project_name)}

        # Initialize project with not-allowed characters
        wrong_project_name = 'Ab_cD4'
        response = create_project(project_name=wrong_project_name)
        assert response.status_code == 400
        assert response.json() == {"detail": "Invalid project name \'{}\'. Allowed characters: letters, numbers, dots (.), and hyphens (-)".format(wrong_project_name)}

        # Initialize project with wrong first/last character
        wrong_project_name = "-abc."
        response = create_project(project_name=wrong_project_name)
        assert response.status_code == 400
        assert response.json() == {"detail": "Invalid project name \'{}\'. Project name must begin and end with a letter or number".format(wrong_project_name)}   

        # Initialize project with case insensive name equal to existing one
        upper_case_project = existing_project.upper()
        response = create_project(project_name=upper_case_project)
        assert response.status_code == 400
        assert response.json() == {"detail": "Invalid project name \'{}\'. Project \'{}\' already exists. Case insensitive equality between project name is not allowed".format(upper_case_project, existing_project)}    

        # Initialize already existing project
        response = create_project(project_name=existing_project)
        assert response.status_code == 409
        assert response.json() == {"detail": f"Project '{existing_project}' already exists. To update schema file, please delete and recreate the project"}    
        
        # Check input file size
        patient_files = {"file": ('test.ttl', io.BytesIO())}
        response = ingest_rdf_file(project_name=existing_project, patient_id=patient_id, files=patient_files)
        assert response.status_code == 422
        assert response.json() == {"detail": "File 'test.ttl' is empty and cannot be ingested. Please upload a file with data"}

        # Check file type in RDF ingestion
        filename = "test-file.json"
        patient_files = {"file": (filename, io.BytesIO(b'Sample patient JSON text'))}
        response = ingest_rdf_file(project_name=existing_project, patient_id=patient_id, files=patient_files)
        assert response.status_code == 422
        assert response.json() == {"detail": "File \'{}\' is not of type RDF. Expected extension: '.ttl'".format(filename)}

        # Check file type in JSON ingestion
        filename = "test-file.ttl"
        patient_files = {"file": (filename, io.BytesIO(b'@prefix sphn: <https://biomedit.ch/rdf/sphn-ontology/sphn#> .'))}
        response = ingest_json_file(project_name=existing_project, patient_id=patient_id, files=patient_files)
        assert response.status_code == 422
        assert response.json() == {"detail": "File \'{}\' is not of type JSON".format(filename)}
        
        # Check invalid JSON file
        filename = "test-file.json"
        patient_files = {"file": (filename, io.BytesIO(b'@prefix sphn: <https://biomedit.ch/rdf/sphn-ontology/sphn#> .'))}
        response = ingest_json_file(project_name=existing_project, patient_id=patient_id, files=patient_files)
        assert response.status_code == 422
        assert response.json() == {"detail": f"Invalid JSON file provided '{filename}'"}

        # Check extension of zip file for /Import_project
        filename = "project.txt"
        project_zip = {"project_zip": (filename, io.BytesIO(b'ZIP archive'))}
        response = client.post("/Import_project", params={"project": non_existing_project}, files=project_zip, headers=headers)
        assert response.status_code == 422
        assert response.json() == {"detail": f"File '{filename}' is not in compressed ZIP format"}

    @staticmethod
    def _create_project(project_name: str):
        """Test project creation

        Args:
            project_name (str): name of the project
        """
        response = create_project(project_name=project_name, testing=False)
        # Check post request response
        assert response.status_code == 200
        assert response.text == f"Project '{project_name}' successfully created"

        # Check Postgres updated content
        records = database.get_projects_configuration()
        assert len(records) == 1
        assert (records[0][0], records[0][1], records[0][2], records[0][4], records[0][6], records[0][8], records[0][10], records[0][12], records[0][13], records[0][14], records[0][15]) == (project_name, database.config.data_provider_id, database.config.api_user, database.config.s3_access_key, database.config.postgres_user, database.config.postgres_ui_email, database.config.airflow_user, database.config.airflow_firstname, database.config.airflow_lastname, database.config.airflow_email, True)
        assert database.pwd_context.verify(database.config.api_password, records[0][3])
        assert database.pwd_context.verify(database.config.s3_secret_key, records[0][5])
        assert database.pwd_context.verify(database.config.postgres_password, records[0][7])
        assert database.pwd_context.verify(database.config.postgres_ui_password, records[0][9])
        assert database.pwd_context.verify(database.config.airflow_password, records[0][11])
        
        # Check S3 object content
        object_name = get_object_name(project_name=project_name, data_provider_id=database.config.data_provider_id, file_class=FileClass.SCHEMA)
        assert object_exists(object_name=object_name)
        data = get_object_data(object_name=object_name)
        assert data == sphn_schema_content

        # Test initialization results
        json_schema = get_object_name(project_name=project_name, data_provider_id=database.config.data_provider_id, file_class=FileClass.RML, filename=f'{project_name}_json_schema.json')
        rml_mapping = get_object_name(project_name=project_name, data_provider_id=database.config.data_provider_id, file_class=FileClass.RML, filename=f'{project_name}_rml_mapping.ttl')
        shacl_file = get_object_name(project_name=project_name, data_provider_id=database.config.data_provider_id, file_class=FileClass.EXT_SHACL, filename=f"Shacler_{project_name}_shacl.ttl")
        formats_mapping = f"{project_name}/De-Identification/{project_name}_formats_mapping.json"
        assert object_exists(object_name=json_schema)
        assert object_exists(object_name=rml_mapping)
        assert object_exists(object_name=shacl_file)
        assert object_exists(object_name=formats_mapping)

        # Check that schema exists
        assert database.schema_exists(schema_name=project_name)

    @staticmethod
    def _get_init_logs(project_name: str):
        """Checks initialization logs

        Args:
            project_name (str): name of the project
        """
        response = client.get('/Get_init_logs',params={"project": project_name}, headers=headers)       
        zipfile = ZipFile(BytesIO(response.content))
        file_names = zipfile.namelist()
        zipfile.close()
        assert len(file_names) == 2
        assert file_names[0].startswith(f'{project_name}_shacler_output')
        assert file_names[1].startswith(f'{project_name}_json_rml')
    
    @staticmethod
    def _get_projects(project_one: str, project_two: str):
        """Checks results of /Get_projects

        Args:
            project_one (str): name of the first project
            project_two (str): name of the second project
        """
        try:
            # Check current projects
            response = client.get("/Get_projects", headers=headers)
            initialized_projects = database.get_created_projects()
            owl_imports = ['<http://purl.obolibrary.org/obo/eco/releases/2024-07-19/eco.owl>', 
                           '<http://purl.obolibrary.org/obo/genepio/releases/2023-08-19/genepio.owl>', 
                           '<http://purl.obolibrary.org/obo/geno/releases/2023-10-08/geno.owl>', 
                           '<http://purl.obolibrary.org/obo/obi/2024-10-25/obi.owl>', 
                           '<http://purl.obolibrary.org/obo/so/2021-11-22/so.owl>', 
                           '<http://snomed.info/sct/900000000000207008/version/20241201>', 
                           '<http://www.ebi.ac.uk/efo/releases/v3.72.0/efo.owl>', 
                           '<https://biomedit.ch/rdf/sphn-resource/atc/2025/1>', 
                           '<https://biomedit.ch/rdf/sphn-resource/chop/2025/4>', 
                           '<https://biomedit.ch/rdf/sphn-resource/edam/1.25>', 
                           '<https://biomedit.ch/rdf/sphn-resource/emdn/2021-09-29/1>', 
                           '<https://biomedit.ch/rdf/sphn-resource/hgnc/20241210>',
                            '<https://biomedit.ch/rdf/sphn-resource/icd-10-gm/2025/3>', 
                            '<https://biomedit.ch/rdf/sphn-resource/loinc/2.78/1>', 
                            '<https://biomedit.ch/rdf/sphn-resource/sphn/oncotree/2021-11-02>', 
                            '<https://biomedit.ch/rdf/sphn-resource/ucum/2025/1>', 
                            '<https://data.jrc.ec.europa.eu/collection/ICDO3_ontology_en/2.0>', 
                            '<https://www.orphadata.com/data/ontologies/ordo/last_version/ORDO_en_4.6.owl>'
                            ]
            assert response.status_code == 200
            assert response.json() == {"projects": [{"project-name": f"{project_one}", "schema-name": f"{project_one}", "data-provider-id": database.config.data_provider_id, "created-by": database.config.api_user,
                                        "is_initialized": True, "output_format": "Turtle", "share_with_einstein": False,
                                        "uploaded-files": {"project_specific_schema": [{"filename": project_one + "_schema.ttl", 'original_name': 'sphn_schema_2025.1.ttl', 'file_size': '602.62KB', 'versionIRI': 'https://biomedit.ch/rdf/sphn-schema/sphn/2025/1', "owl:imports": owl_imports}],
                                                        'ext_shacls': [{'filename': f'Shacler_{project_one}_shacl.ttl', 'file_size': '712.44KB'}]}}]}
            assert response.json() == initialized_projects
            # Create new project
            create_project(project_name=project_two)

            # Check new project
            response = client.get("/Get_projects", headers=headers)            
            assert response.status_code == 200
            initialized_projects = database.get_created_projects()['projects']
            expected_result = [{"project-name": f"{project_one}", "schema-name": f"{project_one}", "data-provider-id": database.config.data_provider_id, "created-by": database.config.api_user,
                                "is_initialized": True, "output_format": "Turtle", "share_with_einstein": False, "uploaded-files": {"project_specific_schema": [{"filename": project_one + "_schema.ttl", 'original_name': 'sphn_schema_2025.1.ttl', 'file_size': '602.62KB', 'versionIRI': 'https://biomedit.ch/rdf/sphn-schema/sphn/2025/1', "owl:imports": owl_imports}],
                                                                        'ext_shacls': [{'filename': f'Shacler_{project_one}_shacl.ttl', 'file_size': '712.44KB'}]}}, 
                                {"project-name": f"{project_two}", "schema-name": f"{project_two}", "data-provider-id": database.config.data_provider_id, "created-by": database.config.api_user, 
                                "is_initialized": True, "output_format": "Turtle", "share_with_einstein": False, "uploaded-files": {"project_specific_schema": [{"filename": project_two + "_schema.ttl", 'original_name': 'sphn_schema_2025.1.ttl', 'file_size': '602.62KB', 'versionIRI': 'https://biomedit.ch/rdf/sphn-schema/sphn/2025/1', "owl:imports": owl_imports}]}}]
            for project in response.json()['projects']:
                assert project in expected_result
                assert project in initialized_projects
        finally:
            delete_project(project_name=project_two)

    @staticmethod
    def _upload_external_files(project_name: str):
        """Tests upload of external files

        Args:
            project_name (str): name of the project
        """
        file_classes = [FileClass.EXT_TERMINOLOGY, FileClass.EXT_SHACL, FileClass.EXT_QUERIES, FileClass.PRE_CHECK_CONFIG, FileClass.DE_IDENTIFICATION_CONFIG]
        for file_class in file_classes:
            files_type = file_class.value
            if file_class == FileClass.EXT_QUERIES:
                external_file_one = "external-file-one.rq"
                external_file_two = "external-file-two.rq"
                external_file_one_data = "@prefix sphn: <https://biomedit.ch/rdf/sphn-ontology/sphn#> ."
                external_file_two_data = "@prefix sphn: <https://biomedit.ch/rdf/sphn-ontology/sphn#> ."
                external_files = [("files", (external_file_one, io.BytesIO(external_file_one_data.encode()))), ("files", (external_file_two, io.BytesIO(external_file_two_data.encode())))]
                response = client.post("/Upload_external_files", params={"project": project_name, "files_type": files_type}, files=external_files, headers=headers)
                assert response.status_code == 200
                assert response.text == f"Files '{external_file_one}', '{external_file_two}' successfully ingested as {files_type.lower()} files"
            elif file_class == FileClass.EXT_SHACL:
                external_file_one = "external-file-one.ttl"
                external_file_one_data = "@prefix sphn: <https://biomedit.ch/rdf/sphn-ontology/sphn#> ."
                external_files = [("files", (external_file_one, io.BytesIO(external_file_one_data.encode())))]
                response = client.post("/Upload_external_files", params={"project": project_name, "files_type": files_type, "purge_before_upload": True}, files=external_files, headers=headers)
                assert response.status_code == 200
                assert response.text == f"File '{external_file_one}' successfully ingested as {files_type.lower()} file"
            elif file_class == FileClass.PRE_CHECK_CONFIG:
                external_file_one = "external-file-one.json"
                external_file_one_data = '{"validationCheck": {"validationCheckName": {"level": "warn"}}}'
                external_files = [("files", (external_file_one, io.BytesIO(external_file_one_data.encode())))]
                response = client.post("/Upload_external_files", params={"project": project_name, "files_type": files_type, "purge_before_upload": True}, files=external_files, headers=headers)
                assert response.status_code == 200
                assert response.text == f"File '{external_file_one}' successfully ingested as {files_type.lower()} file"
            elif file_class == FileClass.DE_IDENTIFICATION_CONFIG:
                external_file_one = "external-file-one.json"
                external_file_one_data = '{"scrambleField": {"scrambleFieldRuleName": {"applies_to_fields": ["id"]}}}'
                external_files = [("files", (external_file_one, io.BytesIO(external_file_one_data.encode())))]
                response = client.post("/Upload_external_files", params={"project": project_name, "files_type": files_type, "purge_before_upload": True}, files=external_files, headers=headers)
                assert response.status_code == 200
                assert response.text == f"File '{external_file_one}' successfully ingested as {files_type.lower()} file"
            else:
                external_file_one = "external-file-one.ttl"
                external_file_two = "external-file-two.ttl"
                external_file_one_data = "@prefix sphn: <https://biomedit.ch/rdf/sphn-ontology/sphn#> ."
                external_file_two_data = "@prefix sphn: <https://biomedit.ch/rdf/sphn-ontology/sphn#> ."
                external_files = [("files", (external_file_one, io.BytesIO(external_file_one_data.encode()))), ("files", (external_file_two, io.BytesIO(external_file_two_data.encode())))]
                response = client.post("/Upload_external_files", params={"project": project_name, "files_type": files_type}, files=external_files, headers=headers)
                assert response.status_code == 200
                assert response.text == f"Files '{external_file_one}', '{external_file_two}' successfully ingested as {files_type.lower()} files"

            # Check S3 object content
            if file_class in [FileClass.EXT_SHACL, FileClass.PRE_CHECK_CONFIG, FileClass.DE_IDENTIFICATION_CONFIG]:
                for filename, msg in zip([external_file_one], [external_file_one_data]):
                    object_name = get_object_name(project_name=project_name, data_provider_id=database.config.data_provider_id, file_class=file_class, filename=filename)
                    assert object_exists(object_name=object_name)
                    data = get_object_data(object_name=object_name)
                    assert data == msg.encode()
            else:
                for filename, msg in zip([external_file_one, external_file_two], [external_file_one_data, external_file_two_data]):
                    object_name = get_object_name(project_name=project_name, data_provider_id=database.config.data_provider_id, file_class=file_class, filename=filename)
                    assert object_exists(object_name=object_name)
                    data = get_object_data(object_name=object_name)
                    assert data == msg.encode()

            # Check upload of invalid RDF file
            filename = 'wrong_rdf_file.ttl'
            external_files = [("files", (filename, io.BytesIO(b'Invalid RDF data')))]
            response = client.post("/Upload_external_files", params={"project": project_name, "files_type": FileClass.EXT_TERMINOLOGY.value}, files=external_files, headers=headers)
            assert response.status_code == 422
            assert response.json() == {"detail": f"Invalid RDF file provided '{filename}'"}

    @staticmethod
    def _get_sparqler_queries(project_name: str):
        """Test endpoint /Get_sparqler_queries

        Args:
            project_name (str): name of the project
        """
        response = client.get('/Get_sparqler_queries', params={"project": project_name}, headers=headers)        
        assert response.status_code == 404
        assert response.json() == {'detail': f"No SPAQRL queries found for project '{project_name}'. Make sure it has been successfully initialized"}

    @staticmethod
    def _reset_project(project_name: str):
        """Tests resetting of project

        Args:
            project_name (str): name of the project
        """
        patient_id = "test-patient-id"
        patient_files = {"file": ('test.ttl', io.BytesIO(b'@prefix sphn: <https://biomedit.ch/rdf/sphn-ontology/sphn#> .'))}
        
        # Ingest RDF files
        response = ingest_rdf_file(project_name=project_name, patient_id=patient_id, files=patient_files)

        # Fill project tables with some data
        fill_project_tables(project_name=project_name)

        # Reset project
        response = client.post("/Reset_project", params={"project": project_name}, headers=headers)
        assert response.status_code == 200
        assert response.text == f"Project '{project_name}' reset"

        # Check database tables
        database.create_connection()
        sqls = ["SELECT * from landing_zone", "SELECT * from release_zone", "SELECT * from refined_zone", "SELECT * from graph_zone", "SELECT * from logging", "SELECT * from pipeline_history"]
        for sql in sqls:
            database.cursor.execute(sql)
            records = database.cursor.fetchall()
            assert records == []
        sql = "SELECT * from configuration where project_name=%s"
        database.cursor.execute(sql, (project_name, ))
        records = database.cursor.fetchall()
        assert len(records) == 1
        database.close_connection()

        # Check object storage (testing landing zone, release zone not tested)
        patient_file = get_object_name(project_name=project_name, data_provider_id=database.config.data_provider_id, file_class=FileClass.PATIENT_DATA, patient_id=patient_id, file_type=FileType.RDF)
        assert not object_exists(object_name=patient_file)
        schema_file = get_object_name(project_name=project_name, data_provider_id=database.config.data_provider_id, file_class=FileClass.SCHEMA)
        assert object_exists(object_name=schema_file)

    @staticmethod
    def _delete_project(project_name: str):
        """Tests deletion of projects

        Args:
            project_name (str): name of the project
        """
        patient_files = {"file": ('test.ttl', io.BytesIO(b'@prefix sphn: <https://biomedit.ch/rdf/sphn-ontology/sphn#> .'))}
        
        # Ingest RDF files
        response = ingest_rdf_file(project_name=project_name, patient_id="test-patient-id", files=patient_files)

        # Fill project tables with some data
        fill_project_tables(project_name=project_name)

        # Delete project
        response = delete_project(project_name=project_name)
        assert response.status_code == 200
        assert response.text == f"Project '{project_name}' deleted"

        # Check database tables
        database.create_connection()
        sqls = ["SELECT * from configuration WHERE project_name=%s", 
                "SELECT * from landing_zone WHERE project_name=%s", 
                "SELECT * from release_zone WHERE project_name=%s", 
                "SELECT * from refined_zone WHERE project_name=%s", 
                "SELECT * from graph_zone WHERE project_name=%s", 
                "SELECT * from logging WHERE project_name=%s", 
                "SELECT * from shacler_logging WHERE project_name=%s", 
                "SELECT * from ddl_generation WHERE project_name=%s", 
                "SELECT * from rml_json_logs WHERE project_name=%s", 
                "SELECT * from pipeline_history WHERE project_name=%s", 
                "SELECT * FROM names_tracker WHERE project_name=%s"]
        for sql in sqls:
            database.cursor.execute(sql, (project_name, ))
            records = database.cursor.fetchall()
            assert records == []
        database.close_connection()

        # Check that schema has been dropped
        assert not database.schema_exists(schema_name=project_name) 

        try:
            # Test non-admin user can delete project only if they previously created it
            user_a = "user_a"
            user_b = "user_b"
            password = "password12345"
            test_project = 'test-project-2'
            create_new_user(user=user_a, password=password)
            create_new_user(user=user_b, password=password)
            token_response_a = client.post("/token", data={"username": user_a, "password": password})
            headers_a = {'Authorization': 'Bearer ' + token_response_a.json()['access_token']}
            token_response_b = client.post("/token", data={"username": user_b, "password": password})
            headers_b = {'Authorization': 'Bearer ' + token_response_b.json()['access_token']}

            schema_files = {"sphn_schema": ("sphn_schema_2023.2.ttl", sphn_schema_content)}
            client.post("/Create_project", params={"project": test_project, "testing": True, "generate_sparql": False}, files=schema_files, headers=headers_a)

            response = client.delete("/Delete_project", params={"project": test_project}, headers=headers_b)
            assert response.status_code == 401
            assert response.json() == {"detail": f"User '{user_b}' is not authorized to delete project '{test_project}'. Only the user who created the project and admin users can delete it"}

            response = client.delete("/Delete_project", params={"project": test_project}, headers=headers_a)
            assert response.status_code == 200
        finally:
            delete_project(project_name=test_project)
            delete_user(user=user_a)
            delete_user(user=user_b)


class UserManagementTest(object):
    """
    Class that checks functionalities and endpoints related to users management
    """

    @staticmethod
    def _create_new_user(user: str, password: str):
        """Checks users creation

        Args:
            user (str): user name
            password (str): user password
        """
        # Create user
        response = client.post("/Create_new_user", data={"user": user, "password": password}, params={"user_type": UserType.STANDARD.value}, headers=headers)
        assert response.status_code == 200
        assert response.text == f"API user '{user}' successfully configured"

        # Recreate user
        response = client.post("/Create_new_user", data={"user": user, "password": password}, params={"user_type": UserType.STANDARD.value}, headers=headers)
        assert response.status_code == 400
        assert response.json() == {"detail": f"API user '{user}' already exists. To reset the password for that user, please use the ad hoc method"}

        # Check on database
        database.create_connection()
        sql = "SELECT api_user from api_credentials"
        database.cursor.execute(sql)
        records = database.cursor.fetchall()
        assert len(records) == 1
        assert records[0] == (user, )
        database.close_connection()

    @staticmethod
    def _get_users_list(existing_std_user: str):
        """Tests returned list of users

        Args:
            existing_std_user (str): existing standard user
        """
        response = get_users_list()
        assert response.status_code == 200
        assert response.json() == {'admin_users': [], 'standard_users': [existing_std_user], 'ingestion_users': []}
        try:
            new_ingestion_user = "api_user_ingestion"
            new_user_password = "api_password"
            create_new_user(user=new_ingestion_user, password=new_user_password, user_type=UserType.INGESTION)
            response = get_users_list()
            assert response.status_code == 200
            assert response.json() == {'admin_users': [], 'standard_users': [existing_std_user], 'ingestion_users': [new_ingestion_user]}
        finally:
            delete_user(user=new_ingestion_user)

    @staticmethod
    def _change_password(existing_user: str, password: str):
        """Tests changing of password

        Args:
            existing_user (str): existing user
            password (str): user password
        """
        non_existing_user = "new_api_user"
        wrong_old_password = "wrong_api_password"
        old_password = "api_password"
        new_password = "new_api_password"

        # Try to change admin user password
        response = client.post("/Change_password", data={"user": config.api_user, "old_password": old_password, "new_password": new_password}, headers=headers)
        assert response.status_code == 405
        assert response.json() == {"detail": f"Admin user '{config.api_user}' cannot be modified. To change it, update configuration file, and restart the connector"}

        # Change password before user creation
        response = client.post("/Change_password", data={"user": non_existing_user, "old_password": old_password, "new_password": new_password}, headers=headers)
        assert response.status_code == 404
        assert response.json() == {"detail": f"API user '{non_existing_user}' not found"}

        # Create user and change the password with wrong old password
        response = client.post("/Change_password", data={"user": existing_user, "old_password": wrong_old_password, "new_password": new_password}, headers=headers)
        assert response.status_code == 400
        assert response.json() == {"detail": f"Provided old password for user '{existing_user}' differs from the existing one. No changes applied"}

        # Change password with new password equal to the old one
        response = client.post("/Change_password", data={"user": existing_user, "old_password": password, "new_password": password}, headers=headers)
        assert response.status_code == 400
        assert response.json() == {"detail": f"New password for user '{existing_user}' must differ from the old one"}

        # Create user and change the password 
        response = client.post("/Change_password", data={"user": existing_user, "old_password": password, "new_password": new_password}, headers=headers)
        assert response.status_code == 200
        assert response.text == f"Password for API user '{existing_user}' successfully updated"

        # Check on database
        database.create_connection()
        sql = "SELECT api_password from api_credentials"
        database.cursor.execute(sql)
        records = database.cursor.fetchall()
        assert len(records) == 1
        assert pwd_context.verify(new_password, records[0][0])
        database.close_connection()

    @staticmethod
    def _reset_password(existing_user: str):
        """Tests resetting of user's password

        Args:
            existing_user (str): existing user
        """
        non_existing_user = "new_api_user"
        new_password = "passwordabcdef"

        # Try to reset admin user
        response = client.post("/Reset_password", data={"user": config.api_user, "password": new_password}, headers=headers)
        assert response.status_code == 405
        assert response.json() == {"detail": f"Admin user '{config.api_user}' cannot be modified. To change it, update configuration file, and restart the connector"}

        # Reset before user creation
        response = client.post("/Reset_password", data={"user": non_existing_user, "password": new_password}, headers=headers)
        assert response.status_code == 404
        assert response.json() == {"detail": f"API user '{non_existing_user}' not found"}

        # Create new user and reset password
        response = client.post("/Reset_password", data={"user": existing_user, "password": new_password}, headers=headers)
        assert response.status_code == 200
        assert response.text == f"Password for API user '{existing_user}' successfully updated"

        # Check on database
        database.create_connection()
        sql = "SELECT api_password from api_credentials"
        database.cursor.execute(sql)
        records = database.cursor.fetchall()
        assert len(records) == 1
        assert pwd_context.verify(new_password, records[0][0])
        database.close_connection()

    @staticmethod
    def _delete_user(existing_user: str):
        """Tests deletion of a user

        Args:
            existing_user (str): existing user
        """
        non_existing_user = "new_api_user"
        # Try to delete admin user
        response = delete_user(user=config.api_user)
        assert response.status_code == 405
        assert response.json() == {"detail": f"Admin user '{config.api_user}' cannot be deleted. To change it, update configuration file, and restart the connector"}

        # Delete non-existing user
        response = delete_user(user=non_existing_user)
        assert response.status_code == 404
        assert response.json() == {"detail": f"API user '{non_existing_user}' not found"}

        # Create user and delete it
        response = delete_user(user=existing_user)
        assert response.status_code == 200
        assert response.text == f"API user '{existing_user}' successfully removed"

        # Check on database
        database.create_connection()
        sql = "SELECT api_user from api_credentials"
        database.cursor.execute(sql)
        records = database.cursor.fetchall()
        assert records == []
        database.close_connection()

class IngestProcessTest(object):
    """
    Class testing functionalities and endpoints related to data ingestion and processing
    """

    @staticmethod
    def _ingest_data(project_name: str):
        """Tests ingestion of several kinds of data

        Args:
            project_name (str): name of the project
        """
        # Ingest RDF data
        with open('/home/sphn/code/api/tests/test_data/patient-ok.ttl', 'rb') as patient_file:
            patient_1_data = patient_file.read()
        with open('/home/sphn/code/api/tests/test_data/patient-not-ok.ttl', 'rb') as patient_file:
            patient_2_data = patient_file.read()
        patient_1  = {"file": ('patient-ok.ttl', patient_1_data)}
        patient_2  = {"file": ('patient-not-ok.ttl', patient_2_data)}
        response = ingest_rdf_file(project_name=project_name, patient_id='rdf_ok', files=patient_1)
        assert response.status_code == 200
        assert response.text == "RDF file 'patient-ok.ttl' passed to SPHN connector"
        response = ingest_rdf_file(project_name=project_name, patient_id='rdf_not_ok', files=patient_2)
        assert response.status_code == 200
        assert response.text == "RDF file 'patient-not-ok.ttl' passed to SPHN connector"

        # Ingest JSON data
        response = client.get("/Get_json_schema", params={"project": project_name}, headers=headers)
        assert response.status_code == 200
        object_name = get_object_name(project_name=project_name, data_provider_id=database.config.data_provider_id, file_class=FileClass.RML, filename=f'{project_name}_json_schema.json')
        data = get_object_data(object_name=object_name)
        assert response.json() == json.loads(data)
        with open('/home/sphn/code/api/tests/test_data/patient-ok.json', 'rb') as patient_file:
            patient_1_data = patient_file.read()
        with open('/home/sphn/code/api/tests/test_data/patient-not-ok.json', 'rb') as patient_file:
            patient_2_data = patient_file.read()
        patient_1  = {"file": ('patient-ok.json', patient_1_data)}
        patient_2  = {"file": ('patient-not-ok.json', patient_2_data)}
        # Standard ingestion
        response = ingest_json_file(project_name=project_name, patient_id='json_ok', files=patient_1)
        assert response.status_code == 200
        assert response.text == "JSON file 'patient-ok.json' passed to SPHN connector"
        # Batch ingestion
        response = client.post("/Ingest_json", params={"project": project_name, "patient_id": "json_not_ok", "batch_ingestion": True}, files=patient_2, headers=headers)
        assert response.status_code == 200
        assert response.text == "JSON file 'patient-not-ok.json' passed to SPHN connector"
        response = client.post("/Trigger_batch_ingestion", params={"project": project_name}, headers=headers)
        assert response.status_code == 200
        assert response.text == f"Batch ingestion of data for project '{project_name}' triggered"
        wait_pipeline_execution(project=project_name, dag_id='run_batch_ingestion')
        
        # Ingest tabular data
        response = client.get('/Get_DDL_statements',params={"project": project_name}, headers=headers)        
        zipfile = ZipFile(BytesIO(response.content))
        file_names = zipfile.namelist()
        zipfile.close()
        assert len(file_names) == 2
        assert file_names[0] == f'{project_name}_tables_DDL.sql'
        assert file_names[1] == f'{project_name}_types_DDL.sql'
        with open('/home/sphn/code/api/tests/test_data/patient-ok.sql', 'rb') as patient_file:
            patient_1_sql = patient_file.read()
        with open('/home/sphn/code/api/tests/test_data/patient-not-ok.sql', 'rb') as patient_file:
            patient_2_sql = patient_file.read()
        database.create_connection(database_name='sphn_tables')
        database.cursor.execute(patient_1_sql)
        database.cursor.execute(patient_2_sql)
        database.conn.commit()
        database.close_connection()
        response = client.post("/Ingest_from_database", params={"project": project_name}, headers=headers)
        assert response.status_code == 200
        ingest_db_project = response.json()
        assert len(ingest_db_project) == 2
        assert set(ingest_db_project.keys()) == set(['run_uuid', "message"])
        assert ingest_db_project['message'] == f"Extraction of database data for project '{project_name}' triggered"
        wait_pipeline_execution(project=project_name, dag_id='run_database_extraction')

        # Ingest CSV data
        response = client.get("/Get_csv_templates", params={"project": project_name}, headers=headers)
        assert response.status_code == 200
        csv_files = ['sphn_DataProvider.csv', 'sphn_SubjectPseudoIdentifier.csv', 'sphn_DataRelease.csv', 'sphn_Allergy.csv', 
                     'sphn_SourceSystem.csv', 'supporting__sphn_Interpretation.csv', 'supporting__sphn_SemanticMapping.csv', 'supporting__sphn_SourceData.csv']
        for csv_file in csv_files:
            file_path = os.path.join('/home/sphn/code/api/tests/test_data', csv_file)
            with open(file_path, 'rb') as file:
                csv = {"file": (csv_file, file)}
                response = client.post("/Ingest_csv", params={"project": project_name}, files=csv, headers=headers)
                assert response.status_code == 200
                assert response.text == f"CSV file '{csv_file}' passed to SPHN Connector"
        response = client.post("/Trigger_batch_ingestion", params={"project": project_name}, headers=headers)
        assert response.status_code == 200
        assert response.text == f"Batch ingestion of data for project '{project_name}' triggered"
        wait_pipeline_execution(project=project_name, dag_id='run_batch_ingestion')
        response = client.post("/Ingest_from_database", params={"project": project_name}, headers=headers)
        assert response.status_code == 200
        ingest_db_project = response.json()
        assert len(ingest_db_project) == 2
        assert set(ingest_db_project.keys()) == set(['run_uuid', "message"])
        assert ingest_db_project['message'] == f"Extraction of database data for project '{project_name}' triggered"
        wait_pipeline_execution(project=project_name, dag_id='run_database_extraction')

        # Ingest Excel data
        response = client.get("/Get_excel_template", params={"project": project_name}, headers=headers)
        assert response.status_code == 200
        excel_template = 'ingestion_template.xlsx'
        file_path = os.path.join('/home/sphn/code/api/tests/test_data', excel_template)
        with open(file_path, 'rb') as file:
            excel = {"file": (excel_template, file)}
            response = client.post("/Ingest_excel", params={"project": project_name}, files=excel, headers=headers)
            assert response.status_code == 200
            assert response.text == f"Excel data ingested into SPHN Connector for project '{project_name}'"
        
        response = client.post("/Ingest_from_database", params={"project": project_name}, headers=headers)
        assert response.status_code == 200
        ingest_db_project = response.json()
        assert len(ingest_db_project) == 2
        assert set(ingest_db_project.keys()) == set(['run_uuid', "message"])
        assert ingest_db_project['message'] == f"Extraction of database data for project '{project_name}' triggered"
        wait_pipeline_execution(project=project_name, dag_id='run_database_extraction')

    @staticmethod
    def _check_unversioned_codes_replacement(project_name: str, patient_id: str):
        """Check that unversioned codes have been successfully replaced

        Args:
            patient_id (str): patient identifier
        """
        expected_iris = ['https://biomedit.ch/rdf/sphn-resource/icd-10-gm/2025/A00.0', 
                         'https://www.whocc.no/atc_ddd_index/?year=2025&code=A01', 
                         'https://biomedit.ch/rdf/sphn-resource/chop/2025/00.01',
                         'https://biomedit.ch/rdf/sphn-resource/icd-10-gm/2023/A00.0', 
                         'https://www.whocc.no/atc_ddd_index/?year=2023&code=A01', 
                         'https://biomedit.ch/rdf/sphn-resource/chop/2023/00.01']
        landing_object_name = get_object_name(project_name=project_name, data_provider_id=database.config.data_provider_id, file_class=FileClass.PATIENT_DATA, patient_id=patient_id)
        refined_object_name = landing_object_name.replace('/landing_zone/', '/refined_zone/')
        data = get_object_data(object_name=refined_object_name)
        json_data = json.loads(data)
        allergen_codes = json_data['content']['sphn:Allergy'][0]['sphn:hasAllergen']['sphn:hasCode']
        for code in allergen_codes:
            if 'iri' in code:
                assert code['iri'] in expected_iris

    @staticmethod
    def _pre_checks_and_de_identification(project_name: str):
        """Tests pre-checks and de-identification

        Args:
            project_name (str): name of the project
        """
        response = client.post("/Reset_project", params={"project": project_name}, headers=headers)
        assert response.status_code == 200
        with open('/home/sphn/code/api/tests/test_data/pre_checks_de_id/config/pre-checks-config.json', 'rb') as checks_config:
            checks_data = checks_config.read()
            config = {"files": ("pre-checks.json", checks_data)}
            response = client.post("/Upload_external_files", params={"project": project_name, "files_type": FileClass.PRE_CHECK_CONFIG.value, "purge_before_upload": True}, files=config, headers=headers)
            assert response.status_code == 200

        with open('/home/sphn/code/api/tests/test_data/pre_checks_de_id/config/de-id-config.json', 'rb') as de_id_config:
            de_id_data = de_id_config.read()
            config = {"files": ("de-id.json", de_id_data)}
            response = client.post("/Upload_external_files", params={"project": project_name, "files_type": FileClass.DE_IDENTIFICATION_CONFIG.value, "purge_before_upload": True}, files=config, headers=headers)
            assert response.status_code == 200

        with open('/home/sphn/code/api/tests/test_data/pre_checks_de_id/data/patient1.json', 'rb') as patient_file:
            patient_1_data = patient_file.read()
            patient_1  = {"file": ('patient1.json', patient_1_data)}
            response = ingest_json_file(project_name=project_name, patient_id="1", files=patient_1)
            assert response.status_code == 200

        with open('/home/sphn/code/api/tests/test_data/pre_checks_de_id/data/patient2.ttl', 'rb') as patient_file:
            patient_2_data = patient_file.read()
            patient_2  = {"file": ('patient2.ttl', patient_2_data)}
            response = ingest_rdf_file(project_name=project_name, patient_id="2", files=patient_2)
            assert response.status_code == 200
        
        # Trigger Pre-Check/De-ID step
        response = client.post("/Start_pipeline", params={"project": project_name, "step": "Pre-check/De-ID"}, headers=headers)
        assert response.status_code == 200
        wait_pipeline_execution(project=project_name, dag_id='run_pre_check_and_de_identification')
        object_name_1 = get_object_name(project_name=project_name, data_provider_id=database.config.data_provider_id, file_class=FileClass.PATIENT_DATA, patient_id="1", file_type=FileType.JSON).replace('landing_zone', 'refined_zone')
        data_1 = get_object_data(object_name=object_name_1)
        refined_patient_1 = json.loads(data_1)
        
        # Check patient logs
        database.create_connection()
        sql = "SELECT message FROM logging WHERE project_name=%s and data_provider_id=%s AND patient_id='1' AND step='Pre-check/De-ID' AND status='Pre-Check'"
        database.cursor.execute(sql, (project_name, database.config.data_provider_id))
        patient_1_pre_checks = [log.strip('\t').split('.')[0] for log in database.cursor.fetchone()[0].strip('\n').split('\n')]
        sql = "SELECT message FROM logging WHERE project_name=%s and data_provider_id=%s AND patient_id='2' AND step='Pre-check/De-ID' AND status='Pre-Check'"
        database.cursor.execute(sql, (project_name, database.config.data_provider_id))
        patient_2_pre_checks = [log.strip('\t').split('.')[0] for log in database.cursor.fetchone()[0].strip('\n').split('\n')]
        sql = "SELECT message FROM logging WHERE project_name=%s and data_provider_id=%s AND patient_id='1' AND step='Pre-check/De-ID' AND status='De-Identification'"
        database.cursor.execute(sql, (project_name, database.config.data_provider_id))
        patient_1_de_id = [log.strip('\t') for log in database.cursor.fetchone()[0].strip('\n').split('\n')]
        sql = "SELECT message FROM logging WHERE project_name=%s and data_provider_id=%s AND patient_id='2' AND step='Pre-check/De-ID' AND status='De-Identification'"
        database.cursor.execute(sql, (project_name, database.config.data_provider_id))
        patient_2_de_id = [log.strip('\t') for log in database.cursor.fetchone()[0].strip('\n').split('\n')]
        database.close_connection()
        assert patient_1_pre_checks == ["Pre-Check 'testReplaceRegexCheck' of type 'replaceRegexCheck' PASSED",
                                        "Pre-Check 'defaultIDReplaceCharsCheck' of type 'replaceCharsCheck' PASSED",
                                        "Pre-Check 'defaultTermIDReplaceCharsCheck' of type 'replaceCharsCheck' PASSED",
                                        "Pre-Check 'defaultValidationCheck' of type 'validationCheck' PASSED",
                                        "Pre-Check 'testRegexCheck' of type 'regexCheck' FAILED with severity level 'warn'", 
                                        "Pre-Check 'jsonPreProcessing' of type 'jsonPreProcessing' PASSED",
                                        "Pre-Check 'replaceUnversionedCodes' of type 'replaceUnversionedCodes' PASSED"]
        assert patient_2_pre_checks == ["Pre-Check 'testDataTypeCheck' of type 'dataTypeCheck' FAILED with severity level 'warn'", "Values not matching data type 'xsd:dateTime': ['2022-04-24']"]
        assert patient_1_de_id == ["De-Identification rule 'testScrambleField' of type 'scrambleField' EXECUTED", "De-Identification rule 'scrambleAllergenID' of type 'scrambleField' EXECUTED", 
                                   "De-Identification rule 'scrambleAllergenCode' of type 'scrambleField' EXECUTED", "De-Identification rule 'testDateShift' of type 'dateShift' EXECUTED", 
                                   "De-Identification rule 'testSubstituteFieldList' of type 'substituteFieldList' EXECUTED", "De-Identification rule 'testSubstituteFieldListSupporting' of type 'substituteFieldList' EXECUTED", 
                                   "De-Identification rule 'testSubstituteFieldRegex' of type 'substituteFieldList' EXECUTED"]
        assert patient_2_de_id == ["No De-Identification rules defined"]

        # Check replaced data
        database.create_connection()
        sql = "SELECT new_value FROM de_identification WHERE project_name=%s AND data_provider_id=%s AND patient_id='1' AND rule_name='testScrambleField'"
        database.cursor.execute(sql, (project_name, database.config.data_provider_id))
        expected_scrambled_value = database.cursor.fetchone()[0]
        sql = "SELECT new_value FROM de_identification WHERE project_name=%s AND data_provider_id=%s AND patient_id='1' AND rule_name='scrambleAllergenCode' AND input_field='content/sphn:Allergy/0/sphn:hasAllergen/sphn:hasCode/0/id'"
        database.cursor.execute(sql, (project_name, database.config.data_provider_id))
        allergen_code_scrambled = database.cursor.fetchone()[0]
        sql = "SELECT new_value FROM de_identification WHERE project_name=%s AND data_provider_id=%s AND patient_id='1' AND rule_name='scrambleAllergenID' and input_field='content/sphn:Allergy/0/sphn:hasAllergen/id'"
        database.cursor.execute(sql, (project_name, database.config.data_provider_id))
        allergen_id_scrambled = database.cursor.fetchone()[0]
        sql = "SELECT date_shift, new_value FROM de_identification WHERE project_name=%s AND data_provider_id=%s AND patient_id='1' AND rule_name='testDateShift'"
        database.cursor.execute(sql, (project_name, database.config.data_provider_id))
        results = database.cursor.fetchone()
        date_shift = results[0]
        expected_shifted_value = results[1]
        sql = "SELECT new_value FROM de_identification WHERE project_name=%s AND data_provider_id=%s AND patient_id='1' AND rule_name='testSubstituteFieldList'"
        database.cursor.execute(sql, (project_name, database.config.data_provider_id))
        substituted_value = database.cursor.fetchone()[0]
        sql = "SELECT new_value FROM de_identification WHERE project_name=%s AND data_provider_id=%s AND patient_id='1' AND rule_name='testSubstituteFieldListSupporting'"
        database.cursor.execute(sql, (project_name, database.config.data_provider_id))
        substituted_value_supporting = database.cursor.fetchone()[0]
        sql = "SELECT new_value FROM de_identification WHERE project_name=%s AND data_provider_id=%s AND patient_id='1' AND rule_name='testSubstituteFieldRegex'"
        database.cursor.execute(sql, (project_name, database.config.data_provider_id))
        regex_substituted_value = database.cursor.fetchone()[0]
        database.close_connection()
        assert refined_patient_1['content']['sphn:Allergy'][0]['id'] == "__aeAeueUeoeOessaaaAAAeeeEEEiiiIIIoooOOOuuuUUU"
        assert refined_patient_1['content']['sphn:Allergy'][0]['sphn:hasAllergen']['sphn:hasCode'][0]['id'] == allergen_code_scrambled
        assert refined_patient_1['content']['sphn:Allergy'][1]['sphn:hasAllergen']['sphn:hasCode'][0]['id'] == allergen_code_scrambled
        assert refined_patient_1['supporting_concepts']['sphn:SemanticMapping'][0]['sphn:hasOutputCode']['id'] == allergen_code_scrambled
        assert refined_patient_1['content']['sphn:Allergy'][0]['sphn:hasAllergen']['id'] == allergen_id_scrambled
        assert refined_patient_1['supporting_concepts']['sphn:SemanticMapping'][0]['sphn:hasOutputCode']['sourceConceptID'] == allergen_id_scrambled
        assert refined_patient_1['sphn:DataProvider']['sphn:hasInstitutionCode']['sphn:hasIdentifier'] == "DGG_346_346_233"
        assert refined_patient_1['sphn:DataProvider']['sphn:hasInstitutionCode']['sphn:hasCodingSystemAndVersion'] != 'UID' and refined_patient_1['sphn:DataProvider']['sphn:hasInstitutionCode']['sphn:hasCodingSystemAndVersion'] == expected_scrambled_value
        assert date_shift == 1
        assert refined_patient_1['sphn:DataRelease']['sphn:hasExtractionDateTime'] != '2022-04-24T11:55:43.304Z' and refined_patient_1['sphn:DataRelease']['sphn:hasExtractionDateTime'] == expected_shifted_value
        assert refined_patient_1['sphn:SubjectPseudoIdentifier']['sphn:hasIdentifier'] != 'patient_1' and refined_patient_1['sphn:SubjectPseudoIdentifier']['sphn:hasIdentifier'] == substituted_value
        assert refined_patient_1['sphn:DataRelease']['id'] != '1666216800' and refined_patient_1['sphn:DataRelease']['id'] == regex_substituted_value
        assert refined_patient_1['supporting_concepts']['sphn:SourceData'][0]['id'] == substituted_value_supporting

        # Check replaced unversioned codes
        IngestProcessTest._check_unversioned_codes_replacement(project_name=project_name, patient_id='1')

        # Check De-Identification report
        response = client.get("/Get_de_identification_report", params={"project": project_name}, headers=headers)
        assert response.status_code == 200

        with zipfile.ZipFile(io.BytesIO(response.content), 'r') as zip_ref:
            for zipinfo in zip_ref.infolist():
                if not zipinfo.filename.startswith('Restore/'):
                    with zip_ref.open(zipinfo) as extracted_file:
                        file_content = extracted_file.read().decode()
                        lines = file_content.split('\n')
                        column_names = lines[0].split(',')
                        data_dict = {name: set() for name in column_names}
                        for line in lines[1:]:
                            if line:
                                values = line.split(',')
                                for i, value in enumerate(values):
                                    data_dict[column_names[i]].add(value)
                        assert len(lines) == 25
                        assert data_dict['project_name'] == set([project_name])
                        assert data_dict['data_provider_id'] == set([database.config.data_provider_id])
                        assert data_dict['patient_id'] == set(['1'])
                        assert data_dict['input_field'] == set(['content/sphn:Allergy/1/sphn:hasAllergen/id', 'supporting_concepts/sphn:SemanticMapping/0/sphn:hasOutputCode/sourceConceptID', 
                                                                'content/sphn:Allergy/0/sphn:hasLastReactionDateTime', 'sphn:DataRelease/id', 'content/sphn:Allergy/1/sphn:hasFirstRecordDateTime', 
                                                                'content/sphn:Allergy/0/sphn:hasAllergen/sphn:hasCode/0/id', 'supporting_concepts/sphn:SourceData/0/id', 
                                                                'sphn:DataRelease/sphn:hasExtractionDateTime', 'content/sphn:Allergy/1/sphn:hasLastReactionDateTime', 
                                                                'sphn:SubjectPseudoIdentifier/sphn:hasIdentifier', 'content/sphn:Allergy/0/sphn:hasAllergen/sphn:hasCode/0/sourceConceptID', 
                                                                'content/sphn:Allergy/0/sphn:hasAllergen/sphn:hasCode/1/sourceConceptID', 'content/sphn:Allergy/0/sphn:hasAllergen/sphn:hasCode/2/sourceConceptID',
                                                                'content/sphn:Allergy/0/sphn:hasAllergen/sphn:hasCode/3/sourceConceptID', 'content/sphn:Allergy/0/sphn:hasAllergen/sphn:hasCode/4/sourceConceptID',
                                                                'content/sphn:Allergy/0/sphn:hasAllergen/sphn:hasCode/5/sourceConceptID', 'content/sphn:Allergy/0/sphn:hasAllergen/sphn:hasCode/6/sourceConceptID',
                                                                'supporting_concepts/sphn:SemanticMapping/0/sphn:hasOutputCode/id', 'content/sphn:Allergy/1/sphn:hasAllergen/sphn:hasCode/0/id', 
                                                                'content/sphn:Allergy/0/sphn:hasAllergen/id', 'content/sphn:Allergy/1/sphn:hasAllergen/sphn:hasCode/0/sourceConceptID', 
                                                                'content/sphn:Allergy/0/sphn:hasFirstRecordDateTime', 'sphn:DataProvider/sphn:hasInstitutionCode/sphn:hasCodingSystemAndVersion'])
                        assert data_dict['rule'] == set(['scrambleField', 'dateShift', 'substituteFieldList', 'substituteFieldList'])
                        assert data_dict['rule_name'] == set(['testSubstituteFieldRegex', 'testDateShift', 'testSubstituteFieldList', 'scrambleAllergenCode', 'testSubstituteFieldListSupporting', 'testScrambleField', 'scrambleAllergenID'])
                        assert data_dict['date_shift'] == set(['', '1'])
                        assert data_dict['substituted'] == set(['', 't'])
                        assert data_dict['old_value'] == set(['allergen_1', 'sourceData01', '1666216800', 'TestInternal-test_pf4n3cke3', '2022-04-24T11:55:43.304Z', 'patient_1', 'UID', 'allergen_two'])
                else:
                    assert zipinfo.filename in ['Restore/de_id_config.json', 'Restore/de_identification_scrambling_table_insert.sql', 'Restore/de_identification_shifts_table_insert.sql', 'Restore/de_identification_table_insert.sql']       

    @staticmethod
    def _processing(project_name: str):
        """Tests data processing

        Args:
            project_name (str): name of the project
        """
        # Get processed patients before pipeline execution
        response = client.get("/Get_processed_patients", params={"project": project_name}, headers=headers)
        assert response.status_code == 404
        assert response.json() == {"detail": f"No processed patients found for project '{project_name}'"}
        
        # Start processing of the data
        response = client.post("/Start_pipeline", params={"project": project_name}, headers=headers)
        assert response.json()['message'] == f"Processing of data for project \'{project_name}\' triggered"
        assert response.status_code == 200
        wait_pipeline_execution(project=project_name)

        # Get status for statistics step
        response = client.get("/Get_status", params={"project": project_name, "step": "Statistics"}, headers=headers)
        assert response.status_code == 404
        assert response.json() == {"detail": f"No pipelines found for project '{project_name}' and step 'Statistics'"}

        # Get full status report
        response = client.get("/Get_status", params={"project": project_name}, headers=headers)
        assert response.status_code == 200
        pipelines = response.json()
        assert len(pipelines) == 7
        assert set(pipelines[0].keys()) == set(['run_uuid', 'project_name', 'pipeline_step', 'pipeline_status', 'patients_processed', 'patients_with_warnings', 'patients_with_errors', 'execution_time', 'elapsed_time'])
        assert set(pipelines[1].keys()) == set(['run_uuid', 'project_name', 'pipeline_step', 'pipeline_status', 'patients_processed', 'patients_with_warnings', 'patients_with_errors', 'execution_time', 'elapsed_time'])
        assert set(pipelines[2].keys()) == set(['run_uuid', 'project_name', 'pipeline_step', 'pipeline_status', 'patients_processed', 'patients_with_warnings', 'patients_with_errors', 'execution_time', 'elapsed_time'])
        assert set(pipelines[3].keys()) == set(['run_uuid', 'project_name', 'pipeline_step', 'pipeline_status', 'patients_processed', 'patients_with_warnings', 'patients_with_errors', 'execution_time', 'elapsed_time'])
        assert set(pipelines[4].keys()) == set(['run_uuid', 'project_name', 'pipeline_step', 'pipeline_status', 'patients_processed', 'execution_time', 'elapsed_time'])
        assert pipelines[0]['project_name'] == project_name
        assert pipelines[0]['pipeline_step'] == 'Notify-Einstein'
        assert pipelines[0]['pipeline_status'] == 'SKIPPED'
        assert pipelines[0]['patients_processed'] == 0
        assert pipelines[0]['patients_with_warnings'] == 0
        assert pipelines[0]['patients_with_errors'] == 0
        assert pipelines[1]['project_name'] == project_name
        assert pipelines[1]['pipeline_step'] == 'Validation'
        assert pipelines[1]['pipeline_status'] == 'SUCCESS'
        assert pipelines[1]['patients_processed'] == 10
        assert pipelines[1]['patients_with_warnings'] == 10
        assert pipelines[1]['patients_with_errors'] == 5
        assert pipelines[2]['project_name'] == project_name
        assert pipelines[2]['pipeline_step'] == 'Integration'
        assert pipelines[2]['pipeline_status'] == 'SUCCESS'
        assert pipelines[2]['patients_processed'] == 10
        assert pipelines[2]['patients_with_warnings'] == 0
        assert pipelines[2]['patients_with_errors'] == 0
        assert pipelines[3]['project_name'] == project_name
        assert pipelines[3]['pipeline_step'] == 'Pre-check/De-ID'
        assert pipelines[3]['pipeline_status'] == 'SUCCESS'
        assert pipelines[3]['patients_processed'] == 10
        assert pipelines[3]['patients_with_warnings'] == 0
        assert pipelines[3]['patients_with_errors'] == 0
        assert pipelines[4]['project_name'] == project_name
        assert pipelines[4]['pipeline_step'] == 'Ingest from Database'
        assert pipelines[4]['pipeline_status'] == 'SUCCESS'

        # Get processed patients
        response = client.get("/Get_processed_patients", params={"project": project_name}, headers=headers)
        assert response.status_code == 200
        patients = response.json().get("patients")
        assert len(patients) == 10
        for patient in patients:
            patient_id = patient['id']
            if patient_id in ['rdf_ok', 'json_ok', 'db_ok', 'csv_ok', 'excel_ok']:
                assert patient['validated_ok']
            else:
                assert not patient['validated_ok']

        # Get patient report
        response = client.get("/Get_patients_report", params={"project": project_name}, headers=headers)
        assert response.status_code == 200
        assert response.json() == {
            "Pre-Release DB ingestion (Patient is ingested into DB but not released to sphn_DataRelease)":{
                "total":2,
                "released":2,
                "not-released":0
            },
            "Ingested into DB (Patient is ingested into DB but /Ingest_from_database not run)":{
                "total":2,
                "processed":6,
                "not-processed":0
            },
            "Pre-check & De-Identification (Pre-check and De-Identification not run or failed)":{
                "total":10,
                "processed":10,
                "not-processed":0
            },
            "Transformation (not transformed to RDF or failed)":{
                "total":10,
                "processed":10,
                "not-processed":0
            },
            "Validation (Validation failed or not run)":{
                "total":10,
                "processed":5,
                "not-processed":5
            },
            "Downloaded (not yet downloaded)":{
                "total":10,
                "downloaded":0,
                "not-downloaded":10
            }
            }
        
        # Check replaced unversioned codes
        IngestProcessTest._check_unversioned_codes_replacement(project_name=project_name, patient_id='db_not_ok')

    @staticmethod
    def _data_extraction(project_name: str):
        """Tests data extraction

        Args:
            project_name (str): name of the project
        """
        non_existing_patient = "non_existing"
        ok_patient_id = "rdf_ok"
        not_ok_patient_id = "rdf_not_ok"
        
        # Extract non-existing patient
        response = client.get("/Get_patient_file", params={"project": project_name, "patient_id": non_existing_patient}, headers=headers)
        assert response.status_code == 404
        assert response.json() == {"detail": f"No file found for project '{project_name}' and patient '{non_existing_patient}'"}

        # Extract existing patient
        response = client.get("/Get_patient_file", params={"project": project_name, "patient_id": ok_patient_id}, headers=headers)
        assert response.status_code == 200
        with open('/home/sphn/code/api/tests/test_data/patient-ok.ttl', 'r', encoding='utf-8') as patient_file:
            assert response.text == patient_file.read()
        
        # Download data for non-existing patient
        response = client.get("/Download_data", params={"project": project_name, "patient_id": non_existing_patient, "validation_status": "OK"}, headers=headers)
        assert response.status_code == 404
        assert response.json() == {"detail": f"No files to download for patient id '{non_existing_patient}' and project '{project_name}' and validation status 'OK'"}

        # Download OK patient
        response = client.get("/Download_data", params={"project": project_name, "patient_id": ok_patient_id, "validation_status": "OK"}, headers=headers)
        assert response.status_code == 200
        with zipfile.ZipFile(io.BytesIO(response.content), 'r') as zip_ref:
            file_list = zip_ref.namelist()
        assert len(file_list) == 1
        assert f'{project_name}_{ok_patient_id}_DATA-PROVIDER-ID.ttl' in file_list

        # Download NOT-OK patient
        response = client.get("/Download_data", params={"project": project_name, "patient_id": not_ok_patient_id, "validation_status": "NOT-OK"}, headers=headers)
        assert response.status_code == 200
        with zipfile.ZipFile(io.BytesIO(response.content), 'r') as zip_ref:
            file_list = zip_ref.namelist()
        assert len(file_list) == 2
        assert f'{project_name}_{not_ok_patient_id}_DATA-PROVIDER-ID.ttl' in file_list
        assert f'{project_name}_{not_ok_patient_id}_DATA-PROVIDER-ID.log' in file_list
        

    @staticmethod
    def _extract_logs(project_name: str):
        """Tests logs extraction

        Args:
            project_name (str): name of the project
        """
        # Extract global logs
        first_ingested_patient = 'rdf_ok'
        response = client.get("/Get_global_logs", params={"project": project_name, "patient_id": first_ingested_patient}, headers=headers)
        logging_messages = response.text.splitlines()
        assert response.status_code == 200
        assert ''.join(logging_messages[:3]) == f"{'='*100}Global logs for project '{project_name}'{'='*100}"
        assert ''.join(logging_messages[4:7]) == f"{'-'*100}Configuration files history report for project '{project_name}'{'-'*100}"
        assert ''.join(logging_messages[9:12]) == f"{'-'*100}Data ingestion report for project '{project_name}'{'-'*100}"
        assert ''.join(logging_messages[13:17]) == f"{'-'*100}Logs for patient '{first_ingested_patient}'{'-'*100}"

        # Extract logs
        patient_id = 'rdf_ok'
        filename = f'{project_name}_{patient_id}_DATA-PROVIDER-ID.ttl'
        response = client.get("/Get_logs", params={"project": project_name, "patient_id": patient_id}, headers=headers)
        logging_messages = response.text.splitlines()
        assert response.status_code == 200
        assert logging_messages[1].split(' -- ')[1] == "[PRE-CHECK] No Pre-Checks defined"
        assert logging_messages[2].split(' -- ')[1] == f"[COPY] Landing file '{filename}' copied to refined zone"
        assert logging_messages[4].split(' -- ')[1] == f"[LOADING] Patient '{patient_id}' for project '{project_name}' queued"
        assert logging_messages[5].split(' -- ')[1] == f"[RDF INTEGRATION] Refined RDF file '{filename}' copied to graph zone"
        assert logging_messages[6].split(' -- ')[1] == "[START TDB2 BUILD] Start building TDB2 model for QAF validation"
        assert logging_messages[7].split(' -- ')[1] == "[END TDB2 BUILD] TDB2 model built successfully"
        assert logging_messages[8].split(' -- ')[1] == "[WARNING IMPORTS] Missing terminologies:"
        assert logging_messages[29].split(' -- ')[1] == f"[BACKLOG] Patient '{patient_id}' for project '{project_name}' currently in the backlog"
        assert logging_messages[30].split(' -- ')[1] == f"[LOADING] Patient '{patient_id}' for project '{project_name}' queued"
        assert logging_messages[31].split(' -- ')[1].startswith(f"[VALIDATION RESULT] SUCCESSFUL SHACL validation of patient id '{patient_id}' and project '{project_name}'. Validation report:")

        # Extract Airflow logs
        response = client.get("/Get_Airflow_logs", params={"project": project_name}, headers=headers)
        assert response.status_code == 200
        dags_msgs = ["Logs for dag_id 'run_batch_ingestion'",
                     "Logs for dag_id 'run_database_backup'",
                     "Logs for dag_id 'run_database_extraction'",
                     "Logs for dag_id 'run_grafana_count'",
                     "Logs for dag_id 'run_integration'",
                     "Logs for dag_id 'run_s3_download'",
                     "Logs for dag_id 'run_notify_einstein'",
                     "Logs for dag_id 'run_pre_check_and_de_identification'",
                     "Logs for dag_id 'run_real_time_count'",
                     "Logs for dag_id 'run_shacler'",
                     "Logs for dag_id 'run_sparqler'",
                     "Logs for dag_id 'run_statistics'",
                     "Logs for dag_id 'run_validation'",
                     "Logs for dag_id 'run_whole_pipeline'"]
        for dag_msg in dags_msgs:
            assert dag_msg in response.text

class TestApiEndpoints(unittest.TestCase):
    """
    Class TestApiEndpoints to test the API endpoints
    """
    @classmethod
    def setUpClass(cls):
        """Test that the Airflow container is up and running before starting the tests
        """
        url = "http://connector:8080/airflow"
        max_retries = 10
        for _ in range(max_retries):
            try:
                response = requests.get(url)
                if response.status_code == 200:
                    return
            except Exception:
                pass
            time.sleep(5)
        print(f"Max retries reached. The server at {url} is still not reachable.")
        assert False

    def test_api(self):
        """
        Test basic functionalities of API
        """
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", category=DeprecationWarning)
            # Test entry page
            response = client.get("/")
            assert response.status_code == 200
            index_file = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'app', 'templates', 'index.html')
            release_tag = 'main' if not config.is_tag else config.version_name
            with open(index_file, 'r', encoding='utf-8') as index:
                assert response.text == index.read().replace('$RELEASE_TAG$', release_tag)

            # Test /api Swagger UI
            response = client.get("/api")
            assert response.status_code == 200

            # Test /docs
            response = client.get("/api/docs")
            assert response.status_code == 200

            # Check token response for wrong credenrtials
            token_response = client.post("/token", data={"username": "wrong_user", "password": "wrong_password"})
            assert token_response.json() == {"detail":"Incorrect username or password"}
            # Check authentication with wrong credentials
            response = client.get("/Get_users_list", headers={'Authorization': 'Bearer WRONG_TOKEN'})
            assert response.json() == {"detail":"Could not validate credentials"}
            # Check authentication without credentials
            response = client.get("/Get_users_list")
            assert response.json() == {"detail":"Not authenticated"}

    def test_project_configuration(self):
        """
        Test project creation and configuration functionalities
        """
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", category=DeprecationWarning)
            project_one = "test-project-one"
            project_two = "test-project-two"
            try:
                ProjectConfigTest._create_project(project_name=project_one)
                ProjectConfigTest._get_init_logs(project_name=project_one)
                ProjectConfigTest._check_pre_conditions(existing_project=project_one)
                ProjectConfigTest._get_projects(project_one=project_one, project_two=project_two)
                ProjectConfigTest._upload_external_files(project_name=project_one)
                ProjectConfigTest._get_sparqler_queries(project_name=project_one)
                ProjectConfigTest._reset_project(project_name=project_one)
                ProjectConfigTest._delete_project(project_name=project_one)
            finally:
                delete_project(project_name=project_one)
                delete_project(project_name=project_two)

    
    def test_data_ingestion_and_processing(self):
        """
        Test data ingestion/processing and results extraction
        """
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", category=DeprecationWarning)
            project_name = 'test-project'
            try:
                create_project(project_name=project_name, testing=False)
                IngestProcessTest._ingest_data(project_name=project_name)
                IngestProcessTest._processing(project_name=project_name)
                IngestProcessTest._data_extraction(project_name=project_name)
                IngestProcessTest._extract_logs(project_name=project_name)
                IngestProcessTest._pre_checks_and_de_identification(project_name=project_name)
            finally:
                delete_project(project_name=project_name)

    def test_users_management(self):
        """
        Test endpoints related to users management
        """
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", category=DeprecationWarning)
            user = "api_user"
            password = "api_password"
            try:
                UserManagementTest._create_new_user(user=user, password=password)
                UserManagementTest._get_users_list(existing_std_user=user)
                UserManagementTest._change_password(existing_user=user, password=password)
                UserManagementTest._reset_password(existing_user=user)
                UserManagementTest._delete_user(existing_user=user)
            finally:
                delete_user(user=user)

if __name__ == '__main__':
    unittest.main()