#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# This file: 'main.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

import io
import zipfile
from database_extractor import DatabaseExtractor
from fastapi import FastAPI, UploadFile, File, Depends, HTTPException, status, Query, Form
from fastapi.security import OAuth2PasswordRequestForm
from fastapi.openapi.docs import get_swagger_ui_html
from fastapi.openapi.utils import get_openapi
import uvicorn
import traceback
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
import time
from typing import List
from pydantic import BaseModel
from api import export_project, extract_sphn_iri_during_import, get_offline_redoc_html, import_project, ingest_terminology, load_csv_files, purge_internal_data, trigger_rml_generation, write_to_batch_folder, write_to_s3, download_schema, upload_schemas, initialize_project, extract_docker_logs, create_s3_bucket, project_schema_exists
from backup_restore import BackupRestore
from database import Database
from config import Config
from airflow_lib import Airflow
from enums import ActionType, AirflowDag, CompressionType, DeIdReportType, DockerService, Endpoint, ErrorLevel, FileClass, FileType, IngestCSVDataType, IngestionType, LogLevel, OutputFormat, PreCondition, QCSeverityLevel, StatisticsOutputType, StatisticsProfile, Steps, TabularDataType, UserType
from minio_policy import MinioPolicy
from rdf_parser import validate_rdf_file
from authentication import Authentication
from grafana_counts import add_grafana_initial_count
from responses import MultipartResponse, PlainResponse, ZipResponse, get_responses, remove_validation_response


def startup():
    """Execute before starting the API service (grant database permissions)
    """
    database = Database()
    try:
        print("Configuring API user")
        create_s3_bucket(bucket=database.config.s3_connector_bucket, config=database.config)
        database.grant_permissions(user=config.api_user, password=config.api_password, user_type=UserType.ADMIN)
    except Exception:
        time.sleep(5)
        try:
            database.grant_permissions(user=config.api_user, password=config.api_password, user_type=UserType.ADMIN)
        except Exception:
            error_message = f"Configuration of API user failed. It's possible that there is a mismatch between environment variables in the containers and in the .env file. Try to remove the volumes and restart the application. Exception raised:\n{traceback.format_exc(chain=False)}"
            print(error_message)
    create_s3_bucket(bucket=database.config.s3_einstein_bucket, config=database.config)

app = FastAPI(docs_url=None, redoc_url=None)
app.mount("/static", StaticFiles(directory="/home/sphn/code/api/app/static"), name="static")
config = Config()
airflow = Airflow()
api_database = Database()
minio_policy = MinioPolicy(database=api_database)
authentication = Authentication(database=api_database)


class Token(BaseModel):
    """Token class
    """
    access_token: str
    token_type: str


class User(BaseModel):
    """User class
    """
    username: str


@app.get("/", 
         include_in_schema=False)
async def root():
    """Root endpoint

    Returns:
        HTMLResponse: HTML contentn of Connector entry page
    """
    with open("/home/sphn/code/api/app/templates/index.html", encoding='utf-8') as f:
        html = f.read()
        release_tag = 'main' if not config.is_tag else config.version_name
    return HTMLResponse(content=html.replace('$RELEASE_TAG$', release_tag), status_code=200)


@app.post("/token", 
          response_model=Token, 
          include_in_schema=False)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    """Authenticate user

    Args:
        form_data (OAuth2PasswordRequestForm, optional): authentication data = Depends().

    Raises:
        HTTPException: incorrect credentials

    Returns:
        dict: map with token
    """
    user = authentication.authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token = authentication.create_access_token(data={"sub": user.username})
    return {"access_token": access_token, "token_type": "bearer"}


@app.post("/Create_project", 
          summary="Create and initialize project", 
          description="Creation of a new project. SPHN schema must be provided. Project specific schema is optional. Project is created and initialized", 
          tags=["Project configuration"], 
          response_class=PlainResponse, 
          responses=get_responses(Endpoint.CREATE_PROJECT))
async def create_project(project: str = Query(..., description="Name of the project to create"), 
                         sphn_schema: UploadFile = File(..., description="SPHN schema"),
                         project_specific_schema: UploadFile = File(None, description="Project specific schema"),
                         data_provider_id: str = Query(None, description="Data provider identifier. If not provided value from .env file will be used"),
                         exception_file: UploadFile = File(None, description="Exception file for SHACLer"), 
                         output_format: str = Query("Turtle", enum = ["Turtle", "NQuads", "Trig"], description="Output format of the generated RDF data"),
                         compression: str = Query(False, enum = [True, False], description="Compression on single patient files. If activated, patient file is compressed to GZIP format"),
                         share_with_einstein: bool = Query(False, enum = [True, False], description="Share project data (trig, n-quads) with Einstein tool"),
                         project_extended_tables: bool = Query(False, enum = [True, False], description="Separate SPHN only tables from project extended tables"),
                         project_extended_tables_prefix: str = Query(None, description="Prefix for project extended tables. Defaults to 'P_EXT__'"),
                         do_not_restrict_unit: bool = Query(False, description="Do not restrict UCUM values for sphn:Unit codes in the generated schemas"),
                         testing: bool = Query(False, enum = [True, False], description="Flag for testing purposes only. Do not activate", include_in_schema=False),
                         generate_sparql: bool = Query(True, enum = [True, False], description="Flag for testing purposes only which generates SPARQLer queries. Do not deactivate", include_in_schema=False), current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Create SPHN Connector project

    Args:
        project (str): name of the project
        sphn_schema (UploadFile): SPHN RDF schema file
        project_specific_schema (UploadFile, optional): project specific RDF schema file. Defaults to File(None, description="Project specific schema").
        data_provider_id (str, optional): custom data provider ID. Defaults to Query(None, description="Data provider identifier. If not provided value from .env file will be used").
        exception_file (UploadFile, optional): exception file for SHACLer. Defaults to File(None, description="Exception file for SHACLer").
        output_format (str, optional): patient data output format. Defaults to Query("Turtle", enum = ["Turtle", "NQuads", "Trig"], description="Output format of the generated RDF data").
        compression (str, optional): compression activated. Defaults to Query(False, enum = [True, False], description="Compression on single patient files. If activated, patient file is compressed to GZIP format").
        share_with_einstein (bool, optional): share patient data with Einstein tool. Defaults to Query(False, enum = [True, False], description="Share project data (trig, n-quads) with Einstein tool").
        project_extended_tables (bool, optional): extend project database tables. Defaults to Query(False, enum = [True, False], description="Separate SPHN only tables from project extended tables").
        project_extended_tables_prefix (str, optional): extended tables prefix. Defaults to Query(None, description="Prefix for project extended tables. Defaults to 'P_EXT__'").
        do_not_restrict_unit (bool, optional): do not restrict sphn:hasUnit values. Defaults to False.
        testing (bool, optional): flag used for testing. Defaults to Query(False, enum = [True, False], description="Flag for testing purposes only. Do not activate", include_in_schema=False).
        generate_sparql (bool, optional): flag for testing. Defaults to Query(True, enum = [True, False], description="Flag for testing purposes only which generates SPARQLer queries. Do not deactivate", include_in_schema=False).
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        str: completion message
    """
    airflow.is_dag_running(dag_id='run_database_backup')
    database = Database(data_provider_id=data_provider_id)
    database.project_exists(project=project)
    try:
        sphn_base_iri = upload_schemas(project=project, sphn_schema=sphn_schema, project_specific_schema=project_specific_schema, exception_file=exception_file, database=database,
                                       einstein=share_with_einstein, output_format=OutputFormat(output_format))
        database.create_project(project_name=project, current_user=current_user.username, sphn_base_uri=sphn_base_iri, output_format=OutputFormat(output_format), compression=compression,
                                share_with_einstein=share_with_einstein, project_extended_tables_prefix=project_extended_tables_prefix)
        add_grafana_initial_count(project_name=project)
    except HTTPException:
        try:
            database.delete_project(project_name=project, minio_policy=minio_policy, delete_policies=False)
        except Exception:
            pass
        raise
    except Exception:
        try:
            database.delete_project(project_name=project, minio_policy=minio_policy, delete_policies=False)
        except Exception:
            pass
        print(traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to create project '{}': {}".format(project, traceback.format_exc()))
    try:
        initialize_project(project=project, database=database, airflow=airflow, minio_policy=minio_policy, testing=testing,
                           project_extended_tables=project_extended_tables, do_not_restrict_unit=do_not_restrict_unit)
        if generate_sparql:
            airflow.trigger_dag(project=project, dag_id='run_sparqler', sphn_base_iri=sphn_base_iri)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to initialize project '{}'. Check init logs for more details: {}".format(project, traceback.format_exc()))
    return "Project '{}' successfully created".format(project)

@app.post("/Upload_external_files", 
          summary="Add project files to project", 
          description="External files needed for the data validation such as external terminology files, SHACL file, Queries files, SPARQL report query; and Pre-Check/De-Identification configuration file, can be uploaded via this endpoint.\
          **WARNING**: When flag *purge_before_upload* is activated, patient data and monitoring data related to all zone except the landing zone is cleaned up. For external Shacl, Pre-Checks config, and De-Identification config the flag must be set to True", 
          tags=["Project configuration"],
          response_class=PlainResponse, 
          responses=get_responses(Endpoint.UPLOAD_EXTERNAL_FILES))
async def upload_external_files(project: str = Query(..., description="Name of the project"),
                                files_type : str = Query(enum = ["External Terminology", "External SHACL", "External Queries", "Pre-Check Config", "De-Identification Config", "Report SPARQL Query"], description="Type of the files to upload"),
                                files: List[UploadFile] = File(...), 
                                purge_before_upload: bool = Query(False, enum = [True, False], description="Purge internal data of the same file type before uploading new files"), 
                                current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Upload external files into project

    Args:
        project (str): name of the project
        files_type (str): type of files being uploaded
        files (List[UploadFile]): files to upload
        purge_before_upload (bool, optional): purge existing data before uploading. Defaults to Query(False, enum = [True, False], description="Purge internal data of the same file type before uploading new files").
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        str: completion message
    """
    filenames = []
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database(project=project)
        file_class = FileClass(files_type)
        if file_class in [FileClass.PRE_CHECK_CONFIG, FileClass.DE_IDENTIFICATION_CONFIG, FileClass.EXT_SHACL] and len(files) > 1:
            raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=f"Uploading more than one file of type '{files_type}' is forbidden")
        purge_internal_data(project=project, file_class=file_class, database=database, purge_before_upload=purge_before_upload)
        for file in files:
            with file.file as input_file:
                filename = file.filename
                content = input_file.read()
                if file_class in [FileClass.PRE_CHECK_CONFIG, FileClass.DE_IDENTIFICATION_CONFIG]:
                    database.check_pre_conditions(project=project, content=content, filename=filename, type=PreCondition.INGESTION, file_type=FileType.JSON, file_class=file_class)
                else:
                    database.check_pre_conditions(project=project, content=content, filename=filename, type=PreCondition.INGESTION, file_type=FileType.RDF, file_class=file_class)
                    if file_class not in [FileClass.EXT_QUERIES, FileClass.REPORT_SPARQL_QUERY, FileClass.EXT_TERMINOLOGY]:
                        validate_rdf_file(content=content, filename=filename)
                airflow.is_pipeline_running(project=project)
                if file_class == FileClass.EXT_TERMINOLOGY:
                    if filename.endswith('.zip'):
                        with zipfile.ZipFile(io.BytesIO(content)) as zip_ref:
                            for zipinfo in zip_ref.infolist():
                                with zip_ref.open(zipinfo) as terminology_file:
                                    ingest_terminology(project=project, content=terminology_file.read(), filename=zipinfo.filename, database=database, filenames=filenames)
                    else:
                        ingest_terminology(project=project, content=content, filename=filename, database=database, filenames=filenames)
                else:
                    write_to_s3(content=content, project_name=project, file_class=file_class, database=database, filename=filename)
                    if file_class == FileClass.EXT_QUERIES:
                        database.cleanup_qc_statistics(project=project, filename=filename)
                    filenames.append(filename)
        if file_class == FileClass.EXT_TERMINOLOGY:
            database.update_tdb2_status_table(project=project, status=False)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to upload file '{}': {}".format(filename, traceback.format_exc()))
    if len(filenames) > 1:
        return "Files {} successfully ingested as {} files".format(", ".join(["'{}'".format(filename) for filename in filenames]), files_type.lower())
    else:
        return f"File '{filenames[0]}' successfully ingested as {files_type.lower()} file"

@app.get("/Get_projects", 
         summary="Get list of initialized projects", 
         description="Returns the list of projects configured, it can be useful to check which projects are already active", 
         tags=["Project configuration"],
         responses=get_responses(Endpoint.GET_PROJECTS))
async def get_projects(current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Get created projects

    Args:
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        dict: map with active projects and details
    """
    try:
        database = Database()
        projects = database.get_created_projects()
        return projects
    except Exception:
        print(traceback.format_exc())
        error_message = f"Extraction of initialized projects failed: {traceback.format_exc()}"
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.post("/Reset_project", 
          summary="Reset project", 
          description="Deletes all patient data and associated logs, but keeps configuration files and project initialization logs", 
          tags=["Project configuration"],
          response_class=PlainResponse, 
          responses=get_responses(Endpoint.RESET_PROJECT))
async def reset_project(project: str = Query(..., description="Name of the project to delete"), 
                        current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Reset project data

    Args:
        project (str): name of the project
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        str: completion message
    """
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        airflow.is_pipeline_running(project=project)
        airflow.is_dag_running(dag_id='run_s3_download', project=project)
        database.reset_project(project_name=project)
        return "Project '{}' reset".format(project)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Resetting of project '{}' failed: {}".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.delete("/Delete_project", 
            summary="Delete project", 
            description="Deletes an entire project and all the data associated to it. **WARNING**: De-Identification report will be dropped", 
            tags=["Project configuration"],
            response_class=PlainResponse, 
            responses=get_responses(Endpoint.DELETE_PROJECT))
async def delete_project(project: str = Query(..., description="Name of the project to delete"),
                         current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Delete project

    Args:
        project (str): name of the project
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        str: completion message
    """
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        database.check_delete_project_permissions(project=project, current_user=current_user.username)
        airflow.is_pipeline_running(project=project)
        airflow.is_dag_running(dag_id='run_s3_download', project=project)
        database.delete_project(project_name=project, minio_policy=minio_policy, delete_policies=True)
        return "Project '{}' deleted".format(project)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Deletion of project '{}' failed: {}".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.get("/Get_json_schema", 
         summary="Download JSON schema", 
         description="Download JSON schema generated based on schemas during project initialization", 
         tags=["JSON ingestion"],
         response_class=MultipartResponse, 
         responses=get_responses(Endpoint.GET_JSON_SCHEMA))
async def get_json_schema(project: str = Query(..., description="Name of the project"), 
                          current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Get project's generated JSON schema

    Args:
        project (str): name of the project
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        Response: JSON schema file
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        database.is_project_initialized(project=project)
        return download_schema(project=project, database=database)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = f"Failed to download JSON schema file for project '{project}': {traceback.format_exc()}"
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.get("/Get_DDL_statements", 
         summary="Download the DDL creation statements", 
         description="Downloads the creation statements for tables and enum types", 
         tags=["Database ingestion"],
         response_class=ZipResponse, 
         responses=get_responses(Endpoint.GET_DDL_STATEMENTS))
async def get_ddl_statements(project: str = Query(..., description="Name of the project"),
                             compression_type : str = Query(".zip", enum = [".zip", ".tar.gz"], description = "Compression type"), 
                             current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Get DDL creation statements

    Args:
        project (str): name of the project
        compression_type (str, optional): compression type. Defaults to Query(".zip", enum = [".zip", ".tar.gz"], description = "Compression type").
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        Response: DDL file
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        database.is_project_initialized(project=project)
        return database.download_ddl_statements(project=project, compression_type=CompressionType(compression_type))
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to extract DDL statements for project '{}': '{}'".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.get("/Get_sparqler_queries", 
         summary="Download SPARQLer queries", 
         description="Download SPARQL queries auto-generated with SPARQLer based on provided schemas", 
         tags=["Project configuration"],
         response_class=ZipResponse, 
         responses=get_responses(Endpoint.GET_SPARQL_QUERIES))
async def get_sparql_queries(project: str = Query(..., description="Name of the project"),
                             compression_type : str= Query(".zip", enum = [".zip", ".tar.gz"], description = "Compression type"), 
                             current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Get generated SPARQL queries

    Args:
        project (str): name of the project
        compression_type (str, optional): compression type. Defaults to Query(".zip", enum = [".zip", ".tar.gz"], description = "Compression type").
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        Response: SPARQL queries file
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        database.is_project_initialized(project=project)
        airflow.is_sparqler_running(project=project)
        return database.download_sparql_queries(project=project, compression_type=CompressionType(compression_type))
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to extract SPARQL queries for project '{}': '{}'".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.get("/Get_csv_templates", 
         summary="Get CSV templates reflecting database schema", 
         description="Get CSV files that have the same columns as the tables in the database. The user can import those files into Excel and fill in the tables. Then they can upload the files via the endpoint /Ingest_csv", 
         tags=["CSV ingestion"],
         response_class=ZipResponse, 
         responses=get_responses(Endpoint.GET_CSV_TEMPLATES))
async def get_csv_files_structure(project: str = Query(..., description="Name of the project"), 
                                  current_user: User = Depends(authentication.get_ingestion_level_permissions)):
    """Get CSV templates

    Args:
        project (str): name of the project
        current_user (User): user triggering the endpoint = Depends(authentication.get_ingestion_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        Response: CSV templates file
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        database.is_project_initialized(project=project)
        return database.get_csv_templates(project=project)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to extract CSV templates for project '{}': {}".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.get("/Get_excel_template", 
         summary="Get Excel template for data ingestion", 
         description="Get Excel template to fill with patient data", 
         tags=["Excel ingestion"],
          response_class=ZipResponse, 
          responses=get_responses(Endpoint.GET_EXCEL_TEMPLATE))
async def get_excel_template(project: str = Query(..., description="Name of the project"), 
                             current_user: User = Depends(authentication.get_ingestion_level_permissions)):
    """Get Excel template

    Args:
        project (str): name of the project
        current_user (User): user triggering the endpoint = Depends(authentication.get_ingestion_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        Response: Excel template file
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        database.is_project_initialized(project=project)
        return database.get_excel_template(project=project)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to extract Excel template for project '{}': {}".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)


@app.post("/Ingest_rdf", 
          summary="Ingest RDF files", 
          description="Send single patient data in RDF format to SPHN Connector. Before ingestion, all data related to the specified patient ID will be cleaned up", 
          tags=["RDF ingestion"],
          response_class=PlainResponse, 
          responses=get_responses(Endpoint.INGEST_RDF))
async def ingest_rdf(project: str = Query(..., description="Name of the project"), 
                     patient_id: str = Query(..., description="Unique patient identifier (should differ from _sphn:SubjectPseudoIdentifier/sphn:hasIdentifier_ value)"),
                     file: UploadFile = File(...), 
                     batch_ingestion: bool = Query(False, description="Enable batch ingestion of data only. Metadata update should be triggered afterwards with endpoint /Trigger_batch_ingestion"), 
                     current_user: User = Depends(authentication.get_ingestion_level_permissions)):
    """Ingest patient RDF file into Connector

    Args:
        project (str): name of the project
        patient_id (str): unique ID of the patient
        file (UploadFile): data file
        batch_ingestion (bool, optional): use batch ingestion. Defaults to Query(False, description="Enable batch ingestion of data only. Metadata update should be triggered afterwards with endpoint /Trigger_batch_ingestion").
        current_user (User): user triggering the endpoint = Depends(authentication.get_ingestion_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        str: completion message
    """
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database(project=project)
        with file.file as input_file:
            filename = file.filename
            content = input_file.read()
            database.check_pre_conditions(project=project, content=content, filename=filename, type=PreCondition.INGESTION, file_type=FileType.RDF, file_class=FileClass.PATIENT_DATA)
            database.is_project_initialized(project=project)
            if not batch_ingestion:
                airflow.is_pipeline_running(project=project)
                write_to_s3(content=content, project_name=project, file_class=FileClass.PATIENT_DATA, database=database, patient_id=patient_id, file_type=FileType.RDF, ingestion_type=IngestionType.RDF)
            else:
                write_to_batch_folder(content=content, project_name=project, filename=patient_id, file_type=FileType.RDF, database=database)
            return "RDF file '{}' passed to SPHN connector".format(filename)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to pass RDF file '{}' to SPHN connector: {}".format(filename, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.post("/Ingest_json", 
          summary="Ingest JSON files", 
          description="Send single patient data in JSON format to SPHN Connector. Before ingestion, all data related to the specified patient ID will be cleaned up", 
          tags=["JSON ingestion"],
          response_class=PlainResponse, 
          responses=get_responses(Endpoint.INGEST_JSON))
async def ingest_json(project: str = Query(..., description="Name of the project"), 
                      patient_id: str = Query(..., description="Unique patient identifier (should differ from _sphn:SubjectPseudoIdentifier/sphn:hasIdentifier_ value)"),
                      file: UploadFile = File(...), 
                      batch_ingestion: bool = Query(False, description="Enable batch ingestion of data only. Metadata update should be triggered afterwards with endpoint /Trigger_batch_ingestion"), 
                      current_user: User = Depends(authentication.get_ingestion_level_permissions)):
    """Ingest patient JSON file into Connector

    Args:
        project (str): name of the project
        patient_id (str): unique ID of the patient
        file (UploadFile): data file
        batch_ingestion (bool, optional): use batch ingestion. Defaults to Query(False, description="Enable batch ingestion of data only. Metadata update should be triggered afterwards with endpoint /Trigger_batch_ingestion").
        current_user (User): user triggering the endpoint = Depends(authentication.get_ingestion_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        str: completion message
    """
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database(project=project)
        with file.file as input_file:
            filename = file.filename
            content = input_file.read()
            database.check_pre_conditions(project=project, content=content, filename=filename, type=PreCondition.INGESTION, file_type=FileType.JSON, file_class=FileClass.PATIENT_DATA)
            database.is_project_initialized(project=project)
            if not batch_ingestion:
                airflow.is_pipeline_running(project=project)
                write_to_s3(content=content, project_name=project, file_class=FileClass.PATIENT_DATA, database=database, patient_id=patient_id, file_type=FileType.JSON, ingestion_type=IngestionType.JSON)
            else:
                write_to_batch_folder(content=content, project_name=project, filename=patient_id, file_type=FileType.JSON, database=database)
            return "JSON file '{}' passed to SPHN connector".format(filename)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to pass JSON file '{}' to SPHN connector: {}".format(filename, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.post("/Ingest_csv", 
          summary="Ingest CSV files with database tables content", 
          description="Store in S3 filled CSV template extracted from /Get_csv_templates. After successful ingestion of all CSV files, execute /Trigger_batch_ingestion endpoint first, and then execute /Ingest_from_database", 
          tags=["CSV ingestion"],
          response_class=PlainResponse, 
          responses=get_responses(Endpoint.INGEST_CSV))
async def ingest_csv(project: str = Query(..., description="Name of the project"), 
                     file: UploadFile = File(..., description="Filled CSV template"),
                     file_type : str= Query(".csv", enum = [".csv", ".zip"], description = "File type") ,
                     current_user: User = Depends(authentication.get_ingestion_level_permissions)):
    """Ingest CSV patient data files into Connector

    Args:
        project (str): name of the project
        file (UploadFile): data file
        file_type (str, optional): type of the file. Defaults to Query(".csv", enum = [".csv", ".zip"], description = "File type").
        current_user (User): user triggering the endpoint = Depends(authentication.get_ingestion_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        str: completion message
    """
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database(project=project)
        file_type = IngestCSVDataType(file_type)
        with file.file as input_file:
            database.is_project_initialized(project=project)
            airflow.is_pipeline_running(project=project)
            content = input_file.read()
            if file_type == IngestCSVDataType.CSV:
                database.check_pre_conditions(project=project, content=content, filename=file.filename, type=PreCondition.OTHER, tabular_type=TabularDataType.CSV, csv_file_type=file_type)
                write_to_batch_folder(content=content, project_name=project, filename=file.filename, file_type=FileType.CSV, database=database)
                return f"CSV file '{file.filename}' passed to SPHN Connector"
            else:
                database.check_pre_conditions(project=project, content=content, filename=file.filename, type=PreCondition.OTHER, tabular_type=TabularDataType.CSV, csv_file_type=file_type)
                load_csv_files(content=content, project=project, database=database)
                return f"CSV files in '{file.filename}' passed to SPHN Connector"
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = f"Failed to ingest CSV data for project '{project}': {traceback.format_exc()}"
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.post("/Ingest_excel", 
          summary="Ingest tabular data in Excel", 
          description="Populate database tables from filled Excel template extracted from /Get_excel_template. After successful ingestion of tabular data, trigger /Ingest_from_database endpoint", 
          tags=["Excel ingestion"],
          response_class=PlainResponse, 
          responses=get_responses(Endpoint.INGEST_EXCEL))
async def ingest_excel_data(project: str = Query(..., description="Name of the project"), 
                            file: UploadFile = File(..., description="Filled Excel template"), 
                            current_user: User = Depends(authentication.get_ingestion_level_permissions)):
    """Ingest Excel patient data file into Connector

    Args:
        project (str): name of the project
        file (UploadFile): data file
        current_user (User): user triggering the endpoint = Depends(authentication.get_ingestion_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        str: completion message
    """
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database(project=project)
        with file.file as input_file:
            content = input_file.read()
            database.check_pre_conditions(project=project, content=content, filename=file.filename, type=PreCondition.OTHER, tabular_type=TabularDataType.EXCEL)
            database.is_project_initialized(project=project)
            airflow.is_pipeline_running(project=project)
            database.truncate_tables(schema_name=project)
            return database.ingest_excel_data(project=project, content=content)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = f"Failed to ingest Excel data for project '{project}': {traceback.format_exc()}"
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.post("/Trigger_batch_ingestion", 
          summary="Ingest files in batches", 
          description="Patient data must be previously uploaded into S3 project bucket with prefix 'project/Input/batch_ingestion/', either by enabling the 'batch_ingestion' flag at the ingestion endpoints, or by uploading it manually into S3. **WARNING** for manual upload of RDF and JSON: file names must reflect the patient IDs. The user is responsible of uploading valid data and with correct naming", 
          tags=["RDF ingestion", "JSON ingestion", "CSV ingestion"],
          response_class=PlainResponse, 
          responses=get_responses(Endpoint.TRIGGER_BATCH_INGESTION))
async def trigger_batch_ingestion(project: str = Query(..., description="Name of the project"), 
                                  action_type: str = Query("Start", enum=["Start", "Check-status", "Stop"], description="Action type of the endpoint: start ingestion, check status of ingestion, stop ingestion"), 
                                  current_user: User = Depends(authentication.get_ingestion_level_permissions)):
    """Ingest data from 'project/Input/batch_ingestion/' prefix to landing_zone (or to database for CSV ingestion)

    Args:
        project (str): name of the project
        action_type (str, optional): type of action. Defaults to Query("Start", enum=["Start", "Check-status", "Stop"], description="Action type of the endpoint: start ingestion, check status of ingestion, stop ingestion").
        current_user (User): user triggering the endpoint = Depends(authentication.get_ingestion_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        dict: triggering message
    """
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database(project=project)
        action_type = ActionType(action_type)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        database.is_project_initialized(project=project)
        if action_type == ActionType.START:
            airflow.is_pipeline_running(project=project)
            database.check_for_batch_data(project=project)
            return airflow.trigger_dag(project=project, dag_id='run_batch_ingestion')
        elif action_type == ActionType.CHECK_STATUS:
            return airflow.check_dag_status(dag_id='run_batch_ingestion', project=project)
        else:
            return airflow.stop_dag(project=project, dag_identifier='run_batch_ingestion')
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        if action_type == ActionType.START:
            error_message = "Failed to trigger batch ingestion for project '{}': {}".format(project, traceback.format_exc())
        elif action_type == ActionType.CHECK_STATUS:
            error_message = f"Failed to check the status of batch ingestion for project '{project}': {traceback.format_exc()}"
        else:
            error_message = f"Failed to stop batch ingestion for project '{project}': {traceback.format_exc()}"
        
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.post("/Ingest_from_database", 
          summary="Extract database data", 
          description="Extract database data into JSON files. Before ingestion, all data related to the release patients will be cleaned up", 
          tags=["Database ingestion", "CSV ingestion", "Excel ingestion"],
          responses=get_responses(Endpoint.INGEST_FROM_DATABASE))
async def ingest_from_database(project: str = Query(..., description="Name of the project"), 
                               action_type: str = Query("Start", enum=["Start", "Check-status", "Stop"], description="Action type of the endpoint: start ingestion, check status of ingestion, stop ingestion"), 
                               current_user: User = Depends(authentication.get_ingestion_level_permissions)):
    """Extract data from database into JSON files

    Args:
        project (str): name of the project
        action_type (str, optional): type of action. Defaults to Query("Start", enum=["Start", "Check-status", "Stop"], description="Action type of the endpoint: start ingestion, check status of ingestion, stop ingestion").
        current_user (User): user triggering the endpoint = Depends(authentication.get_ingestion_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        dict: triggering message
    """
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database(project=project)
        action_type = ActionType(action_type)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        database.is_project_initialized(project=project)
        if action_type == ActionType.START:
            airflow.is_pipeline_running(project=project)
            database_extractor = DatabaseExtractor(project=project)
            database_extractor.check_patients_data(project_name=project)
            return airflow.trigger_dag(project=project, dag_id='run_database_extraction')
        elif action_type == ActionType.CHECK_STATUS:
            return airflow.check_dag_status(dag_id='run_database_extraction', project=project)
        else:
            return airflow.stop_dag(project=project, dag_identifier='run_database_extraction')
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        if action_type == ActionType.START:
            error_message = f"Failed to extract data from database for project '{project}': {traceback.format_exc()}"
        elif action_type == ActionType.CHECK_STATUS:
            error_message = f"Failed to check the status of database extraction for project '{project}': {traceback.format_exc()}"
        else:
            error_message = f"Failed to stop database extraction for project '{project}': {traceback.format_exc()}"
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.post("/Start_pipeline", 
          summary="Start the processing of ingested patient data", 
          description="Triggers the processing of the patient data", 
          tags=["Process patient data"],
          responses=get_responses(Endpoint.START_PIPELINE))
async def start_pipeline(project: str = Query(..., description="Name of the project"), 
                         patient_id: str = Query(None, description="Unique patient identifier"),
                         step : str= Query("All", enum = ["Pre-check/De-ID", "Integration", "Validation", "Notify-Einstein"]),
                         validation_log_level : str= Query("Compact", enum = ["Compact", "Verbose"],  description = "Log level of QAF validation step"),
                         severity_level: str = Query("INFO", enum=["INFO", "WARNING", "ERROR"], description="Severity level of SHACL validation. Info level checks all severities, Warning level checks only sh:Warning and sh:Violation, Error checks only severity sh:Violation"),
                         revalidate: bool = Query(False, enum=[True, False], description="Triggers the validation of all data in the graph zone, independently of their validation status (if already processed)"),
                         re_notify: bool = Query(False, enum=[True, False], description="Re-notify all validated patients to Einstein"),
                         current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Start pipeline

    Args:
        project (str): name of the project
        patient_id (str, optional): unique ID of the patient. Defaults to Query(None, description="Unique patient identifier").
        step (str, optional): pipeline step. Defaults to Query("All", enum = ["Pre-check/De-ID", "Integration", "Validation", "Notify-Einstein"]).
        validation_log_level (str, optional): log level of validation step. Defaults to Query("Compact", enum = ["Compact", "Verbose"],  description = "Log level of QAF validation step").
        severity_level (str, optional): severity level of validation step. Defaults to Query("INFO", enum=["INFO", "WARNING", "ERROR"], description="Severity level of SHACL validation. Info level checks all severities, Warning level checks only sh:Warning and sh:Violation, Error checks only severity sh:Violation").
        revalidate (bool, optional): revalidate all the data. Defaults to Query(False, enum=[True, False], description="Triggers the validation of all data in the graph zone, independently of their validation status (if already processed)").
        re_notify (bool, optional): re-notify all the data. Defaults to Query(False, enum=[True, False], description="Re-notify all validated patients to Einstein").
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        dict: triggering message
    """
    step = Steps(step)
    severity_level = QCSeverityLevel(severity_level)
    validation_log_level = LogLevel(validation_log_level)
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        database.is_project_initialized(project=project)
        airflow.is_pipeline_running(project=project)
        airflow.is_dag_running(dag_id='run_s3_download', project=project)
        if re_notify:
            database.reset_notification_status(project=project)
        database.is_patient_data_available(project=project, patient_id=patient_id, step=step, revalidate=revalidate)
        airflow.check_concurrent_projects(project=project)
        return airflow.trigger_dag(project=project, patient_id=patient_id, step=step, validation_log_level=validation_log_level,
                                   severity_level=severity_level, revalidate=revalidate)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to trigger processing of data for project '{}': {}".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)


@app.post("/Stop_pipeline", 
          summary="Stop the processing of ingested patient data", 
          description="Stop running processing of the patient data", 
          tags=["Process patient data"],
          response_class=PlainResponse, 
          responses=get_responses(Endpoint.STOP_PIPELINE))
async def stop_pipeline(project: str = Query(..., description="Name of the project"), 
                        current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Stop running pipeline

    Args:
        project (str): name of the project
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        str: completion message
    """
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        database.is_project_initialized(project=project)
        return airflow.stop_dag(project=project)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to stop the processing of data for project '{}': {}".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)


@app.post("/Start_statistics", 
          summary="Trigger patients statistics", 
          description="Trigger patients statistics on patient data in graph_zone", 
          tags=["Process patient data"],
          responses=get_responses(Endpoint.START_STATISTICS))
async def start_statistics(project: str = Query(..., description="Name of the project"), 
                           profile: str = Query("Fast", enum = ["Fast", "Extensive"], description="'Fast' profile does not collect attributes values to extract sample values and distinct count. The 'Extensive' profile is computationally more expensive. Keep in mind that the performance will be affected."), 
                           current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Trigger statistics

    Args:
        project (str): name of the project
        profile (str, optional): statistics profile. Defaults to Query("Fast", enum = ["Fast", "Extensive"], description="'Fast' profile does not collect attributes values to extract sample values and distinct count. The 'Extensive' profile is computationally more expensive. Keep in mind that the performance will be affected.").
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        dict: triggering message
    """
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        database.is_project_initialized(project=project)
        airflow.is_sparqler_running(project=project)
        airflow.is_pipeline_running(project=project)
        airflow.is_dag_running(dag_id='run_s3_download', project=project)
        database.is_stats_data_available(project=project)
        airflow.check_concurrent_projects(project=project)
        return airflow.trigger_dag(project=project, step=Steps.STATISTICS, profile=StatisticsProfile(profile))
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to trigger statistics for '{}': {}".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)
    
@app.get("/Get_status", 
         summary="Get status of the pipelines", 
         description="Returns information about pipelines", 
         tags=["Monitoring"],
         responses=get_responses(Endpoint.GET_STATUS))
async def get_status(project: str = Query(..., description="Name of the project"), 
                     step : str= Query("All", enum = ["Ingest from Database", "Pre-check/De-ID", "Integration", "Validation", "Notify-Einstein", "Statistics"], description = "Pipeline step to log"), 
                     current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Get status of steps

    Args:
        project (str): name of the project
        step (str, optional): pipeline step to monitor. Defaults to Query("All", enum = ["Ingest from Database", "Pre-check/De-ID", "Integration", "Validation", "Notify-Einstein", "Statistics"], description = "Pipeline step to log").
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        dict: map of pipelines status
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        airflow.wait_pipeline_execution(project=project, dag_id="run_grafana_count")    
        return airflow.get_status(project=project, step=Steps(step), database=database)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to extract pipelines status for project '{}': {}".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.get("/Get_patients_with_errors", 
         summary="Get list of patients with defined errors", 
         description="Returns a list of patient IDs having errors related to the specified input parameters", 
         tags=["Monitoring"],
         responses=get_responses(Endpoint.GET_PATIENTS_WITH_ERRORS))
async def get_patiets_with_errors(project: str = Query(..., description="Name of the project"), 
                                  step : str= Query(..., enum = ["Pre-check/De-ID", "Integration", "Validation", "Notify-Einstein", "Statistics"], description = "Pipeline step"), 
                                  error_level: str = Query(..., enum = ["WARNING", "ERROR"]), 
                                  current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Get patients with errors

    Args:
        project (str): name of the project
        step (str): pipeline step to monitor
        error_level (str): level of the error
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        dict: map of patients with errors
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        return database.get_patient_list_with_errors(project=project, step=Steps(step), error_level=ErrorLevel(error_level))
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to extract patient IDs with errors for project '{}': {}".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.get("/Get_logs", 
         summary="Get the logs of the data processing", 
         description="Returns the logs of the processing of the data", 
         tags=["Monitoring"],
         response_class=ZipResponse, 
         responses=get_responses(Endpoint.GET_LOGS))
async def get_logs(project: str = Query(..., description="Name of the project"), 
                   patient_id: str = Query(None, description="Unique patient identifier"),
                   step : str= Query("All", enum = ["Pre-check/De-ID", "Integration", "Validation", "Notify-Einstein", "Statistics"], description = "Pipeline step to log"),
                   compression_type : str= Query(".zip", enum = [".zip", ".tar.gz"], description = "Compression type"), 
                   current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Get logs

    Args:
        project (str): name of the project
        patient_id (str, optional): unique ID of the patient. Defaults to Query(None, description="Unique patient identifier").
        step (str, optional): pipeline step. Defaults to Query("All", enum = ["Pre-check/De-ID", "Integration", "Validation", "Notify-Einstein", "Statistics"], description = "Pipeline step to log").
        compression_type (str, optional): type of compression. Defaults to Query(".zip", enum = [".zip", ".tar.gz"], description = "Compression type").
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        Response: file with logs
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        return database.get_logs(project=project, patient_id=patient_id, step=step, compression_type=CompressionType(compression_type))
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to extract logs for project '{}': {}".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)


@app.get("/Get_global_logs", 
         summary="Get global logs", 
         description="Returns OK/NOT-OK data processing logs for a project", 
         tags=["Monitoring"],
         response_class=MultipartResponse, 
         responses=get_responses(Endpoint.GET_GLOBAL_LOGS))
async def get_global_logs(project: str = Query(..., description="Name of the project"), 
                          patient_id: str = Query(None, description="Unique patient identifier"),
                          validation_status : str= Query(None, enum = ["OK", "NOT-OK"], description = "Validation status"), 
                          current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Get global logs

    Args:
        project (str): name of the project
        patient_id (str, optional): unique ID of the patient. Defaults to Query(None, description="Unique patient identifier").
        validation_status (str, optional): status of the validation step. Defaults to Query(None, enum = ["OK", "NOT-OK"], description = "Validation status").
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        Response: file with global logs
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        return database.get_global_logs(project=project, patient_id=patient_id, validation_status=validation_status)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to extract global logs for project '{}': {}".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.get("/Get_qc_report", 
         summary="Get quality checks report", 
         description="Get quality checks results of the validation step", 
         tags=["Monitoring"],
         response_class=MultipartResponse, 
         responses=get_responses(Endpoint.GET_QC_REPORT))
async def get_qc_report(project: str = Query(..., description="Name of the project"), 
                        patient_id: str = Query(None, description="Unique patient identifier"),
                        validation_status : str= Query(None, enum = ["OK", "NOT-OK"], description = "Validation status"), 
                        current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Get quality checks report

    Args:
        project (str): name of the project
        patient_id (str, optional): unique ID of the patient. Defaults to Query(None, description="Unique patient identifier").
        validation_status (str, optional): status of the validation. Defaults to Query(None, enum = ["OK", "NOT-OK"], description = "Validation status").
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        Response: file with quality checks report
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        return database.get_quality_checks_report(project=project, patient_id=patient_id, validation_status=validation_status)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to extract quality checks report for project '{}': {}".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)


@app.get("/Get_init_logs", 
         summary="Get the logs of the init process", 
         description="Returns the logs of the processing of the Shacler, Json creation and RML mappings", 
         tags=["Monitoring"],
         response_class=ZipResponse, 
         responses=get_responses(Endpoint.GET_INIT_LOGS))
async def get_init_logs(project: str = Query(..., description="Name of the project"),
                        compression_type : str= Query(".zip", enum = [".zip", ".tar.gz"], description = "Compression type"), 
                        current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Get log files for the Shacler, Json and RML creation process

    Args:
        project (str): name of the project
        compression_type (str, optional): type of compression. Defaults to Query(".zip", enum = [".zip", ".tar.gz"], description = "Compression type").
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        Response: file with init logs
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        return database.download_init_files(project=project, compression_type=CompressionType(compression_type))
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to extract init logs project '{}' '{}'".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.get("/Get_Airflow_logs", 
         summary="Get latest logs from Airflow", description="In the event of an abrupt failure of an Airflow process, this endpoint returns the logs of the latest executed dags and can give some additional information about the failure ", 
         tags=["Monitoring"],
         response_class=MultipartResponse, 
         responses=get_responses(Endpoint.GET_AIRFLOW_LOGS))
async def get_airflow_logs(project: str = Query(..., description="Name of the project"), 
                           DAG: str = Query("all", enum = ['all', 'run_batch_ingestion', 'run_database_backup', 'run_database_extraction', 'run_grafana_count', 'run_integration', 'run_s3_download', 'run_notify_einstein', 'run_pre_check_and_de_identification', 'run_real_time_count', 'run_shacler', 'run_sparqler', 'run_statistics', 'run_validation', 'run_whole_pipeline'], description="Airflow DAG"), 
                           current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Get Airflow logs

    Args:
        project (str): name of the project
        DAG (str, optional): DAG to monitor. Defaults to Query("all", enum = ['all', 'run_batch_ingestion', 'run_database_backup', 'run_database_extraction', 'run_grafana_count', 'run_integration', 'run_s3_download', 'run_notify_einstein', 'run_pre_check_and_de_identification', 'run_real_time_count', 'run_shacler', 'run_sparqler', 'run_statistics', 'run_validation', 'run_whole_pipeline'], description="Airflow DAG").
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        Response: file with Airflow logs
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        return airflow.get_logs(project=project, dag=AirflowDag(DAG))
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to extract Airflow logs project '{}' '{}'".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.get("/Get_patient_file", 
         summary="Get RDF patient file from Graph Zone", 
         description="Allows to download RDF patient file for investigating potential issues encountered during QAF validation", 
         tags=["Monitoring"],
         response_class=MultipartResponse, 
         responses=get_responses(Endpoint.GET_PATIENT_FILE))
async def get_patient_file(project: str = Query(..., description="Name of the project"), 
                           patient_id: str = Query(..., description="Unique patient identifier"), 
                           current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Get patient file from graph zone

    Args:
        project (str): name of the project
        patient_id (str): unique ID of the patient
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        Response: graph zone patient file
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        return database.download_graph_zone_file(project=project, patient_id=patient_id)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = f"Failed download patient file for project '{project}' and patient '{patient_id}': {traceback.format_exc()}"
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.get("/Get_patients_report", 
         summary="Get patients status report", 
         description="Returns an overview of the patients status and where those patients are located. It can be useful to check if some patients are left behind and need to be taken care of", 
         tags=["Monitoring"],
         responses=get_responses(Endpoint.GET_PATIENTS_REPORT))
async def get_patients_report(project: str = Query(..., description="Name of the project"), 
                              extract_ids: bool = Query(False, enum=[True, False], description="Extract IDs of unprocessed patients. If activated, a download link for the file is provided"),
                              current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Get patients report

    Args:
        project (str): name of the project
        extract_ids (bool, optional): add patient IDs to the report. Defaults to Query(False, enum=[True, False], description="Extract IDs of unprocessed patients. If activated, a download link for the file is provided").
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        Response: file with patients report
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        return database.get_patients_report(project=project, extract_ids=extract_ids)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = f"Failed extract patients report for project '{project}': {traceback.format_exc()}"
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.get("/Get_docker_logs",
         summary="Get Docker logs",
         description="Extract logs of the Docker containers",
         tags=["Monitoring"],
         response_class=MultipartResponse,
         responses=get_responses(Endpoint.GET_DOCKER_LOGS))
async def get_docker_logs(service: str = Query("All", enum=["api", "data_handler", "grafana", "minio", "pgadmin", "postgres", "reverse-proxy", "connector"], description="Name of the service"),
                          current_user: User = Depends(authentication.get_admin_level_permissions)):
    """Get Docker containers logs

    Args:
        service (str, optional): Docker service. Defaults to Query("All", enum=["api", "data_handler", "grafana", "minio", "pgadmin", "postgres", "reverse-proxy", "connector"], description="Name of the service").
        current_user (User): user triggering the endpoint = Depends(authentication.get_admin_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        Response: file with Docker logs
    """
    try:
        return extract_docker_logs(service=DockerService(service))
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = f"Failed to extract Docker logs '{traceback.format_exc()}'"
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)


@app.get("/Get_processed_patients", 
         summary="Get list of proccessed patients", 
         description="Returns a list of patient ids that have been processed. The ids can be used to download the data with endpoint /Download_data", 
         tags=["Download patient data"],
         responses=get_responses(Endpoint.GET_PROCESSED_PATIENTS))
async def get_processed_patients(project: str = Query(..., description="Name of the project"), 
                                 current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Get processed patients details

    Args:
        project (str): name of the project
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        dict: map with patients processed
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        return database.get_patients_list(project_name=project)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to extract processed patients list for project '{}': {}".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)


@app.get("/Download_data", 
         summary="Download processed patient data", 
         description="When data has been successfully processed, this method allows to download it locally", 
         tags=["Download patient data"],
         response_class=ZipResponse, responses=get_responses(Endpoint.DOWNLOAD_DATA))
async def download_data(project: str = Query(..., description="Name of the project"), patient_id: str = Query(None, description="Unique patient identifier"),
                        validation_status : str= Query(None, enum = ["OK", "NOT-OK"], description = "Validation status"),
                        skip_downloaded: bool = Query(False, description="Only download files that haven't already been downloaded"),
                        patients_only: bool = Query(False, description="If activated, download only the patient data without statistics or logs"),
                        batch_size: int = Query(None, description="Size of patient batches. *Warning*: when activated, statistics report has to be extracted separately"),
                        compression_type : str= Query(".zip", enum = [".zip", ".tar.gz"], description = "Compression type"), 
                        current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Download processed data

    Args:
        project (str): name of the project
        patient_id (str, optional): unique ID of the patient. Defaults to Query(None, description="Unique patient identifier").
        validation_status (str, optional): status of the validation step. Defaults to Query(None, enum = ["OK", "NOT-OK"], description = "Validation status").
        skip_downloaded (bool, optional): skip downloaded files. Defaults to Query(False, description="Only download files that haven't already been downloaded").
        patients_only (bool, optional): download only patients data. Defaults to Query(False, description="If activated, download only the patient data without statistics or logs").
        batch_size (int, optional): batch size of the download. Defaults to Query(None, description="Size of patient batches. *Warning*: when activated, statistics report has to be extracted separately").
        compression_type (str, optional): type of the compression. Defaults to Query(".zip", enum = [".zip", ".tar.gz"], description = "Compression type").
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        Response: zipped file with patient data
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        return database.get_processed_files(project_name=project, patient_id=patient_id, validation_status=validation_status, skip_downloaded=skip_downloaded, compression_type=CompressionType(compression_type), batch_size=batch_size, patients_only=patients_only)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to download files for patient id '{}' project '{}' and validation status '{}': {}".format(patient_id, project, validation_status, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.get("/Download_in_S3", 
         summary="Generate download bundle and store it in S3", 
         description="Generate download bundle and store it into S3 object storage with /Download prefix. After generation the bundle can be downloaded manually from S3 (recommended if data size is big) or downloaded here by setting action_type to 'Download'. Results from download via endpoint of multiple validation status will be grouped into a single ZIP file",
         tags=["Download patient data"], 
         response_class=PlainResponse, 
         responses=get_responses(Endpoint.DOWNLOAD_IN_S3))
async def download_data_in_s3(project: str = Query(..., description="Name of the project"),
                              validation_status : str= Query(None, enum = ["OK", "NOT-OK"], description = "Validation status"),
                              skip_downloaded: bool = Query(False, description="Only download files that haven't already been downloaded"),
                              patients_only: bool = Query(False, description="If activated, download only the patient data without statistics or logs"),
                              compression_type : str= Query(".zip", enum = [".zip", ".tar.gz"], description = "Compression type"), 
                              action_type: str = Query("Start", enum=["Start", "Check-status", "Stop", "Download"], description="Action type of the endpoint: start download, check status of download, stop download, download generated file"), 
                              current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Download data into S3

    Args:
        project (str): name of the project
        validation_status (str, optional): status of the validation step. Defaults to Query(None, enum = ["OK", "NOT-OK"], description = "Validation status").
        skip_downloaded (bool, optional): skip downloaded files. Defaults to Query(False, description="Only download files that haven't already been downloaded").
        patients_only (bool, optional): download only patients data. Defaults to Query(False, description="If activated, download only the patient data without statistics or logs").
        compression_type (str, optional): type of the compression. Defaults to Query(".zip", enum = [".zip", ".tar.gz"], description = "Compression type").
        action_type (str, optional): type of action. Defaults to Query("Start", enum=["Start", "Check-status", "Stop", "Download"], description="Action type of the endpoint: start download, check status of download, stop download, download generated file").
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        Union[dict, Response]: triggering message, file to download
    """
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        dag_id = 'run_s3_download'
        action_type = ActionType(action_type)
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        database.check_data_to_download(project_name=project, patient_id=None, validation_status=validation_status)
        if action_type == ActionType.START:
            airflow.is_pipeline_running(project=project)
            airflow.is_dag_running(dag_id=dag_id, project=project)
            return airflow.trigger_dag(project=project, dag_id=dag_id, validation_status=validation_status, compression_type=CompressionType(compression_type), skip_downloaded=skip_downloaded,
                                       patients_only=patients_only)
        elif action_type == ActionType.CHECK_STATUS:
            return airflow.check_dag_status(dag_id=dag_id, project=project)
        elif action_type == ActionType.DOWNLOAD:
            return database.download_s3_file(dag_id='run_s3_download', project=project, validation_status=validation_status, compression_type=CompressionType(compression_type))
        else:
            return airflow.stop_dag(project=project, dag_identifier=dag_id)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to trigger data download in S3 for project '{}' and validation status '{}': {}".format(project, validation_status, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)


@app.post("/Reset_download_status", 
          summary="Make all patients available for download", 
          description="Makes all patient data available for download. Can be used to re-trigger batch download of data if data has been already extracted once", 
          tags=["Download patient data"],
          response_class=PlainResponse, 
          responses=get_responses(Endpoint.RESET_DOWNLOAD_STATUS))
async def reset_download_status(project: str = Query(..., description="Name of the project"), 
                                current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Rest download status

    Args:
        project (str): name of the project
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        str: completion message
    """
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        return database.reset_download_status(project=project)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to reset downloaded data status for project '{}': {}".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.get("/Get_de_identification_report", 
         summary="Get De-Identification report", 
         description="Returns a ZIP file containing the De-Identification report (CSV or Excel format) and files for De-Identification restore", 
         tags=["Download patient data"],
         response_class=MultipartResponse, 
         responses=get_responses(Endpoint.GET_DE_IDENTIFICATION_REPORT))
async def get_de_id_report(project: str = Query(..., description="Name of the project"),
                           report_file_type : str= Query(".csv", enum = [".csv", ".xlsx"], description="Format of De-Identification report"), 
                           current_user: User = Depends(authentication.get_admin_level_permissions)):
    """Get De-Identification report

    Args:
        project (str): name of the project
        report_file_type (str, optional): type of file. Defaults to Query(".csv", enum = [".csv", ".xlsx"], description="Format of De-Identification report").
        current_user (User): user triggering the endpoint = Depends(authentication.get_admin_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        Response: zipped file with de-identification report
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        return database.get_de_identification_report(project=project, output_file_type=DeIdReportType(report_file_type))
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to extract De-Identification report for project '{}': {}".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.get("/Get_statistics_reports", 
         summary="Get statistics report", 
         description="Get project statistics reports", 
         tags=["Download patient data"],
         response_class=ZipResponse, 
         responses=get_responses(Endpoint.GET_STATISTICS_REPORTS))
async def get_statistics(project: str = Query(..., description="Name of the project"), 
                         output_file_type : str= Query(".zip", enum = [".zip", ".tar.gz", ".xlsx"]), 
                         current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Get statistics report

    Args:
        project (str): name of the project
        output_file_type (str, optional): type of output file. Defaults to Query(".zip", enum = [".zip", ".tar.gz", ".xlsx"]).
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        Response: statistics report file
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        return database.extract_statistics(project=project, output_file_type=StatisticsOutputType(output_file_type))
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Failed to extract project '{}' statistics: {}".format(project, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.post("/Change_password", 
          summary="Change API user password", 
          description="Change the password for a specific API user", 
          tags=["User Management - User Role"],
          response_class=PlainResponse, 
          responses=get_responses(Endpoint.CHANGE_PASSWORD))
async def change_password(user: str = Form(...), 
                          old_password: str = Form(...), 
                          new_password: str = Form(...),  
                          current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Change user password

    Args:
        user (str): user to update
        old_password (str): old password
        new_password (str): new password
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        str: completion message
    """
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database()
        return database.change_api_user_pwd(user=user, old_password=old_password, new_password=new_password, current_user=current_user.username, minio_policy=minio_policy)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Changing of user '{}' password failed: {}".format(user, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.get("/Get_users_list", 
         summary="Get list of API users", 
         description="Returns the list of API users configured, it can be useful to check which users are already defined when one wants to make modification to the configured credentials", 
         tags=["User Management - User Role"],
         responses=get_responses(Endpoint.GET_USERS_LIST))
async def get_users_list(current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Get map of existing users

    Args:
        current_user (User): user triggering the endpoint = Depends(authentication.get_read_and_write_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        dict: map use users and types
    """
    try:
        database = Database()
        return database.get_users_map(current_user=current_user.username)
    except Exception:
        print(traceback.format_exc())
        error_message = f"Extraction of API users failed: {traceback.format_exc()}"
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)


@app.post("/Create_new_user", 
          summary="Create new API user", 
          description="Setup credentials for new API user", 
          tags=["User Management - Admin Role"],
          response_class=PlainResponse, 
          responses=get_responses(Endpoint.CREATE_NEW_USER))
async def create_new_user(user: str = Form(...), 
                          password: str = Form(...), 
                          user_type : str= Query(..., enum = ["Admin", "Standard", "Ingestion"],  description = "Type of the new user"), 
                          current_user: User = Depends(authentication.get_admin_level_permissions)):
    """Create new API user

    Args:
        user (str): user to create
        password (str): user's password
        user_type (str): user's type
        current_user (User): user triggering the endpoint = Depends(authentication.get_admin_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        str: completion message
    """
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database()
        return database.setup_api_user(user=user, password=password, user_type=UserType(user_type), current_user=current_user.username, minio_policy=minio_policy)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Configuration of API user '{}' failed: {}".format(user, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)


@app.delete("/Delete_user", 
            summary="Delete API user",
            description="Remove access for API user", 
            tags=["User Management - Admin Role"],
            response_class=PlainResponse, 
            responses=get_responses(Endpoint.DELETE_USER))
async def delete_user(user: str = Query(..., description="User to delete"), 
                      current_user: User = Depends(authentication.get_admin_level_permissions)):
    """Delete API user

    Args:
        user (str): user to delete
        current_user (User): user triggering the endpoint = Depends(authentication.get_admin_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        str: completion message
    """
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database()
        return database.delete_api_user(user=user, current_user=current_user.username, minio_policy=minio_policy)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Deletion of API user '{}' failed: {}".format(user, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)


@app.post("/Reset_password", 
          summary="Reset API user password", 
          description="Reset the password for a specific API user", 
          tags=["User Management - Admin Role"],
          response_class=PlainResponse, 
          responses=get_responses(Endpoint.RESET_PASSWORD))
async def reset_password(user: str = Form(...), 
                         password: str = Form(...), 
                         current_user: User = Depends(authentication.get_admin_level_permissions)):
    """Reset API user password

    Args:
        user (str): user to reset
        password (str): new password
        current_user (User): user triggering the endpoint = Depends(authentication.get_admin_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        str: completion message
    """
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database()
        return database.reset_api_user_pwd(user=user, new_password=password, current_user=current_user.username, minio_policy=minio_policy)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = "Resetting of user '{}' password failed: {}".format(user, traceback.format_exc())
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)


@app.get("/Export_project", 
         summary="Export project", 
         description="Export project configuration files in a zip archive folder", 
         tags=["Project export/import"],
         response_class=ZipResponse, 
         responses=get_responses(Endpoint.EXPORT_PROJECT))
async def export_project_endpoint(project: str = Query(..., description="Name of the project"), 
                                  current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Export project

    Args:
        project (str): name of the project
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        Response: file with project's export
    """
    try:
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER)
        database.is_project_initialized(project=project)
        return export_project(project=project, database=database)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = f"Failed to export project '{project}': {traceback.format_exc()}"
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)


@app.post("/Import_project", 
          summary="Import project", 
          description="Import and create project by uploading the exported project archive", 
          tags=["Project export/import"],
          response_class=PlainResponse, 
          responses=get_responses(Endpoint.IMPORT_PROJECT))
async def import_project_endpoint(project: str = Query(..., description="Name of the project"), project_zip: UploadFile = File(..., description="ZIP archive with project data"),
                                  data_provider_id: str = Query(None, description="Data provider identifier. If not provided value from .env file will be used"),
                                  output_format: str = Query("Turtle", enum = ["Turtle", "NQuads", "Trig"], description="Output format of the generated RDF data"),
                                  compression: str = Query(False, enum = [True, False], description="Compression on single patient files. If activated, patient file is compressed to GZIP format"),
                                  share_with_einstein: bool = Query(False, enum = [True, False], description="Share project data (trig, n-quads) with Einstein tool"),
                                  project_extended_tables: bool = Query(False, enum = [True, False], description="Separate SPHN only tables from project extended tables"),
                                  project_extended_tables_prefix: str = Query(None, description="Prefix for project extended tables. Defaults to 'P_EXT__'"),
                                  do_not_restrict_unit: bool = Query(False, description="Do not restrict UCUM values for sphn:Unit codes in the generated schemas"),
                                  current_user: User = Depends(authentication.get_standard_level_permissions)):
    """Import project

    Args:
        project (str): name of the project
        project_zip (UploadFile, optional): zipped file with project's export. Defaults to File(..., description="ZIP archive with project data").
        data_provider_id (str, optional): ID of data provider. Defaults to Query(None, description="Data provider identifier. If not provided value from .env file will be used").
        output_format (str, optional): patient data output format. Defaults to Query("Turtle", enum = ["Turtle", "NQuads", "Trig"], description="Output format of the generated RDF data").
        compression (str, optional): compression activated. Defaults to Query(False, enum = [True, False], description="Compression on single patient files. If activated, patient file is compressed to GZIP format").
        share_with_einstein (bool, optional): share patient data with Einstein tool. Defaults to Query(False, enum = [True, False], description="Share project data (trig, n-quads) with Einstein tool").
        project_extended_tables (bool, optional): extend project database tables. Defaults to Query(False, enum = [True, False], description="Separate SPHN only tables from project extended tables").
        project_extended_tables_prefix (str, optional): extended tables prefix. Defaults to Query(None, description="Prefix for project extended tables. Defaults to 'P_EXT__'").
        do_not_restrict_unit (bool, optional): do not restrict sphn:hasUnit values. Defaults to False.
        current_user (User): user triggering the endpoint = Depends(authentication.get_standard_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        str: completion message
    """
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database(data_provider_id=data_provider_id)
        database.project_exists(project=project)
        with project_zip.file as input_file:
            filename = project_zip.filename
            content = input_file.read()
            database.check_pre_conditions(project=project, content=content, filename=filename, type=PreCondition.PROJECT_CREATION, file_type=FileType.ZIP, einstein=share_with_einstein,
                                          output_format=OutputFormat(output_format))
            project_schema = project_schema_exists(content=content)
            database.check_pre_conditions(project=project, content=content, filename=filename, type=PreCondition.PROJECT_CREATION, file_type=FileType.ZIP, einstein=share_with_einstein,
                                          output_format=OutputFormat(output_format), project_specific=project_schema)
            import_project(project=project, content=content, database=database)
            sphn_base_uri = extract_sphn_iri_during_import(project=project, database=database)
            database.create_project(project_name=project, current_user=current_user.username, sphn_base_uri=sphn_base_uri, output_format=OutputFormat(output_format), compression=compression,
                                    share_with_einstein=share_with_einstein, project_extended_tables_prefix=project_extended_tables_prefix)
            trigger_rml_generation(project=project, database=database, project_extended_tables=project_extended_tables, do_not_restrict_unit=do_not_restrict_unit)
            database.ddl_generation(project_name=project)
            database.grant_permissions(project=project)
            minio_policy.create(project=project)
            database.update_tdb2_status_table(project=project, status=False)
            database.mark_project_as_initialized_in_db(project=project)
            add_grafana_initial_count(project_name=project)
            return f"Project '{project}' successfully imported"
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = f"Failed create project '{project}' from ZIP file '{filename}': {traceback.format_exc()}"
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.post("/Backup_database", 
          summary="Backup SPHN Connector Postgres database data", 
          description="Creates a backup file containing Postgres database data. It can be passed to endpoint /Restore_database as first step after a clean re-installation. **WARNING** -- S3 buckets data MUST be manually backed up and restored by the user", 
          tags=["Project export/import"],
          response_class=PlainResponse, 
          responses=get_responses(Endpoint.BACKUP_DATABASE))
async def backup_database(action_type: str = Query("Start", enum=["Start", "Check-status", "Stop", "Download"], description="Action type of the endpoint: start backup, check status of backup, stop backup, download generated backup file"),
                          current_user: User = Depends(authentication.get_admin_level_permissions)):
    """Backup SPHN Connector Postgres database

    Args:
        current_user (User): user triggering the endpoint = Depends(authentication.get_admin_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        dict: triggering message
    """
    try:
        action_type = ActionType(action_type)
        dag_id = 'run_database_backup'
        if action_type == ActionType.START:
            airflow.is_pipeline_running()
            airflow.is_dag_running(dag_id=dag_id)
            return airflow.trigger_dag(project=None, dag_id=dag_id, config=config)
        elif action_type == ActionType.CHECK_STATUS:
            return airflow.check_dag_status(dag_id=dag_id)
        elif action_type == ActionType.DOWNLOAD:
            airflow.is_dag_running(dag_id=dag_id)
            database = Database()
            return database.download_s3_file(dag_id=dag_id)
        else:
            return airflow.stop_dag(dag_identifier=dag_id)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = f"Failed to backup Connector database: {traceback.format_exc()}"
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)
    
@app.post("/Restore_database", 
          summary="Restore SPHN Connector Postgres database data", 
          description="Restores Postgres database data from backup file. **WARNING** -- S3 buckets data MUST be manually backed up and restored by the user", 
          tags=["Project export/import"],
          response_class=PlainResponse, 
          responses=get_responses(Endpoint.RESTORE_DATABASE))
async def restore_database(database_backup: UploadFile = File(..., description="SPHN Connector backup ZIP archive"),
                           current_user: User = Depends(authentication.get_admin_level_permissions)):
    """Restore SPHN Connector Postgres database

    Args:
        database_backup (UploadFile): database backup file
        current_user (User): user triggering the endpoint = Depends(authentication.get_admin_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        str: completion message
    """
    try:
        airflow.is_pipeline_running()
        with database_backup.file as input_file:
            content = input_file.read()
            restore = BackupRestore()
            return restore.restore_database(content=content)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = f"Failed to restore Connector database: {traceback.format_exc()}"
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)
    
@app.post("/Restore_de_identification", 
          summary="Restore De-Identification logic", 
          description="Restore De-Identification logic for a project based on the previously exported data via /Get_de_identification_report. **WARNING**: it should be used for a newly configured project. The available de-identification tracked results and processed patient data will be dropped",
          tags=["Project export/import"],  
          response_class=PlainResponse, 
          responses=get_responses(Endpoint.RESTORE_DE_IDENTIFICATION))
async def restore_de_id(project: str = Query(..., description="Name of the project"), 
                        de_id_data: UploadFile = File(..., description="De-Identification extracted ZIP"), 
                        current_user: User = Depends(authentication.get_admin_level_permissions)):
    """Restore de-identification from report

    Args:
        project (str): name of the project
        de_id_data (UploadFile): de-identification exported file
        current_user (User): user triggering the endpoint = Depends(authentication.get_admin_level_permissions)

    Raises:
        HTTPException: unexpected exception

    Returns:
        str: completion message
    """
    try:
        airflow.is_dag_running(dag_id='run_database_backup')
        database = Database(project=project)
        database.check_pre_conditions(project=project, type=PreCondition.OTHER, file_type=FileType.ZIP)
        with de_id_data.file as input_file:
            content = input_file.read()
            purge_internal_data(project=project, file_class=FileClass.DE_IDENTIFICATION_CONFIG, database=database, purge_before_upload=True)
            return database.restore_de_identification(project=project, de_id_data=content)
    except HTTPException:
        raise
    except Exception:
        print(traceback.format_exc())
        error_message = f"Failed to restore de-identification: {traceback.format_exc()}"
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=error_message)

@app.get("/api", 
         include_in_schema=False)
async def get_documentation():
    """Get SPHN Connector API page

    Returns:
        HTMLResponse: API page content
    """
    return get_swagger_ui_html(openapi_url="/openapi.json", title="SPHN Connector", swagger_favicon_url="/static/sphn--connector-logo-icon.png",
                               swagger_css_url="/static/swagger-ui.css", swagger_js_url="/static/swagger-ui-bundle.js")


@app.get("/api/docs", 
         include_in_schema=False)
async def get_redoc_documentation():
    """Get SPHN Connector documentation page

    Returns:
        HTMLResponse: documentation page content
    """
    return get_offline_redoc_html(openapi_url="/openapi.json", title="SPHN Connector Docs", redoc_favicon_url="/static/sphn--connector-logo-icon.png",
                                  redoc_js_url="/static/redoc.standalone.js")


def custom_openapi():
    """Get API schema

    Returns:
        dict: API schema
    """
    if app.openapi_schema:
        return app.openapi_schema
    version = config.version_name
    if not config.is_tag:
        version += f" ({config.commit_hash})"
        release_tag = 'main'
    else:
        release_tag = version
    description = f"""
Welcome to the **Swiss Personalized Health Network Connector** \n
The SPHN Connector is a dockerized software suite that enables the user to create, validate, and provide RDF files from patient data. Check out the [SPHN Connector user guide](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-connector/-/raw/{release_tag}/docs/SPHN-Connector_User-Guide.pdf) for a detailed description of the endpoints.\n
The API is divided into the following sections:\n
- **Project configuration**: management of projects \n
- **Ingest patient data**: RDF/JSON/Database/CSV/Excel ingestion of patient data\n
- **Process patient data**: process patient data\n
- **Monitoring**: monitors the processing of the data through process statuses and process logs\n
- **Download processed patient data**: allows to list the processed patient ids and use them to download the processed files locally\n
- **User Management - User Role**: API users management accessible by the API user\n
- **User Management - Admin Role**: API users management accessible by the API admin\n
- **Project export/import**: export/import project configuration for faster deployment
"""
    openapi_schema = get_openapi(
        title="Swiss Personalized Health Network Connector",
        version=version,
        openapi_version="3.0.0",
        description=description,
        contact={
            "name": "SPHN Connector Team",
            "email": "fair-data-team@sib.swiss"
            },
        routes=app.routes
    )
    openapi_schema["info"]["x-logo"] = {
        "url": "/static/sphn-connector-logo.png"
    }
    openapi_schema['tags'] = [{"name": "Project configuration", "description": "SPHN Connector project initialization/deletion"},
                              {"name": "RDF ingestion", "description": "Ingest RDF patient data into the Connector"},
                              {"name": "JSON ingestion", "description": "Ingest JSON patient data into the Connector"},
                              {"name": "Database ingestion", "description": "Ingest tabular patient data via database into the Connector"},
                              {"name": "CSV ingestion", "description": "Ingest CSV patient data into the Connector"},
                              {"name": "Excel ingestion", "description": "Ingest Excel patient data into the Connector"},
                              {"name": "Process patient data", "description": "Trigger processing of patient data"}, {"name": "Monitoring", "description": "Monitor data processing"},
                              {"name": "Download patient data", "description": "Download processed patient data"}, {"name": "User Management - User Role", "description": "API user management for API users"},
                              {"name": "User Management - Admin Role", "description": "API user management for admin user"},
                              {"name": "Project export/import", "description": "Export/Import project configuration for faster deployment"}]
    remove_validation_response(openapi_schema=openapi_schema)
    app.openapi_schema = openapi_schema
    return app.openapi_schema

app.openapi = custom_openapi


if __name__ == '__main__':
    startup()
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=False, workers=4)
