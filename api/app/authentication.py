#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'authentication.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from passlib.context import CryptContext
from datetime import datetime, timedelta, timezone
from typing import Union
import jwt
from pydantic import BaseModel
from enums import UserType
from database import Database

class TokenData(BaseModel):
    username: Union[str, None] = None

class User(BaseModel):
    username: str

class UserInDB(User):
    hashed_password: str

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

class Authentication(object):
    """
    Class defining logic for API authentication
    """
    def __init__(self, database: Database) -> None:
        """Class constructor

        Args:
            database (Database): database object
        """
        self.pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
        self.secret_key = database.config.api_secret_key
        self.algorithm = "HS256"
        self.database = database
        self.access_token_expire_hours = self.database.config.access_token_timeout

    def verify_password(self, plain_password: str, hashed_password: str) -> bool:
        """Verify password

        Args:
            plain_password (str): plain password to verify
            hashed_password (str): stored hashed password

        Returns:
            bool: is password valid
        """
        return self.pwd_context.verify(plain_password, hashed_password)

    def get_password_hash(self, password: str) -> str:
        """Get hashed value of the password

        Args:
            password (str): password to hash

        Returns:
            str: hashed password
        """
        return self.pwd_context.hash(password)

    def get_user(self, username: str, user_type: UserType) -> UserInDB:
        """Get user information

        Args:
            username (str): username
            user_type (UserType): type of the user

        Returns:
            UserInDB: user information
        """
        credentials = self.database.get_credentials(user_type=user_type)
        if username in credentials:
            user_dict = {"username": username, "hashed_password": credentials[username]}
            return UserInDB(**user_dict)

    def authenticate_user(self, username: str, password: str) -> UserInDB:
        """Authenticate credentials

        Args:
            username (str): username
            password (str): password for the user

        Returns:
            UserInDB: authenticated user
        """
        credentials = self.database.get_credentials(user_type=UserType.INGESTION)
        user = None
        if username in credentials:
            user_dict = {"username": username, "hashed_password": credentials[username]}
            user = UserInDB(**user_dict)
        if not user:
            return False
        if not self.verify_password(password, user.hashed_password):
            return False
        return user

    def create_access_token(self, data: dict) -> str:
        """Create encoded JWT access token

        Args:
            data (dict): user data

        Returns:
            str: encoded token
        """
        to_encode = data.copy()
        expire = datetime.now(timezone.utc) + timedelta(hours=self.access_token_expire_hours)
        to_encode.update({"exp": expire})
        encoded_jwt = jwt.encode(to_encode, self.secret_key, algorithm=self.algorithm)
        return encoded_jwt
    
    def check_permissions(self, token: str, user_type: UserType) -> UserInDB:
        """Check user's permissions

        Args:
            token (str): access token
            user_type (UserType): type of user

        Raises:
            HTTPException: unauthorized

        Returns:
            UserInDB: user information
        """
        credentials_exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
        permissions_exception = HTTPException(
            status_code= status.HTTP_403_FORBIDDEN,
            detail="Unauthorized: You do not have the necessary permissions to access this resource.",
            headers={"WWW-Authenticate": "Bearer"},
        )
        try:
            payload = jwt.decode(token, self.secret_key, algorithms=[self.algorithm])
            username: str = payload.get("sub")
            if username is None:
                raise credentials_exception
            token_data = TokenData(username=username)
        except jwt.PyJWTError as exc:
            raise credentials_exception from exc
        user = self.get_user(username=token_data.username, user_type=user_type)
        if user is None:
            raise permissions_exception
        return user


    async def get_admin_level_permissions(self, token: str = Depends(oauth2_scheme)) -> UserInDB:
        """Check if user has admin level permissions

        Args:
            token (str, optional): access token of user. Defaults to Depends(oauth2_scheme).

        Returns:
            UserInDB: user information
        """
        return self.check_permissions(token=token, user_type=UserType.ADMIN)

    async def get_standard_level_permissions(self, token: str = Depends(oauth2_scheme)):
        """Check if user has standard level permissions

        Args:
            token (str, optional): access token of user. Defaults to Depends(oauth2_scheme).

        Returns:
            UserInDB: user information
        """
        return self.check_permissions(token=token, user_type=UserType.STANDARD)

    async def get_ingestion_level_permissions(self, token: str = Depends(oauth2_scheme)):
        """Check if user has ingestion level permissions

        Args:
            token (str, optional): access token of user. Defaults to Depends(oauth2_scheme).

        Returns:
            UserInDB: user information
        """
        return self.check_permissions(token=token, user_type=UserType.INGESTION)