#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.
#
# This file: 'responses.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence.

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

from enums import Endpoint
from fastapi import Response


class PlainResponse(Response):
    media_type = "text/plain"

class MultipartResponse(Response):
    media_type = "multipart/form-data"

class ZipResponse(Response):
    media_type = "application/x-zip-compressed"


def get_responses(endpoint: Endpoint) -> dict:
    """Get responses definition based on endpoint type

    Args:
        endpoint (Endpoint): type of endpoint

    Returns:
        dict: responses
    """
    responses = {
        200: {
            "description": f"Return message for successful execution of endpoint {endpoint.value}",
            "content": {
                "text/plain": {
                    "examples": {
                        "Successful execution": {
                            "value": None
                        }
                    }
                }
            }
        },
        500: {
            "description": f"Error raised during execution of endpoint {endpoint.value}",
            "content": {
                "application/json": {
                    "examples":{
                        "Failed execution": {
                            "value": {
                                "detail": None
                            }
                        }
                    }
                }
            }

        },
        404: {
            "description": "Item not found",
            "content": {
                "application/json": {
                    "examples":{
                        "Project not found": {
                            "value": {
                                "detail": "Project 'test-project' has not been created yet. Please configure it at endpoint /Create_project"
                            }
                        }
                    }
                }
            }
        }
    }

    pre_conditions_examples = {
        "Empty file": {
            "value": {
                "detail": "File 'filename' is empty and cannot be ingested. Please upload a file with data"
            }
        },
        "Invalid JSON content": {
            "value": {
                "detail": "Invalid JSON file provided 'filename'"
            }
        },
        "Invalid JSON extension": {
            "value": {
                "detail": "File 'filename' is not of type JSON"
            }
        },
        "Invalid RDF content": {
            "value": {
                "detail": "Invalid RDF file provided 'filename'"
            }
        },
        "Invalid RDF extension": {
            "value": {
                "detail": "File 'filename' is not of type RDF. Expected extension: '.ttl'"
            }
        }
        }
    project_name_conditions = {
        "Invalid length": {
            "value": {
                "detail": "Invalid project name 'test-project'. Allowed number of characters: [3, 63]"
                }
        },
        "Invalid characters": {
            "value": {
                "detail": "Invalid project name 'test-project'. Allowed characters: letters, numbers, dots (.), and hyphens (-)"
            }
        },
        "Invalid name begin/end": {
            "value": {
                "detail": "Invalid project name 'test-project'. Project name must begin and end with a letter or number"
            }
        },
        "Case insensive equality": {
            "value": {
                "detail": "Invalid project name 'Test-Project'. Project 'test-project' already exists. Case insensitive equality between project name is not allowed"
            }
        },
        "Einstein sharing - deactivated": {
            "value": {
                "detail": "To activate data sharing with Einstein for project 'test-project', the EINSTEIN parameter must be set to 'True' in the .env file"
            }
        },
        "Einstein sharing - wrong format": {
            "value": {
                "detail": "To activate data sharing with Einstein for project 'test-project', the parameter 'output_format' must be set to either 'Trig' or 'NQuads'"
            }
        },
        "Einstein sharing - no project specific data allowed": {
            "value": {
                "detail": "Project specific data cannot be shared with Einstein. Either set share_with_einstein=False or remove the provided project specific schema"
            }
        }
    }
    if endpoint == Endpoint.CREATE_PROJECT:
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "Project 'test-project' successfully created"        
        responses[500]["content"]["application/json"]["examples"] = {
            "Failed creation": {
                "value": {
                    "detail": "Failed to create project 'test-project': ..."
                }
            },
            "Failed initialization": {
                "value": {
                    "detail": "Failed to initialize project 'test-project'. Check init logs for more details"
                }
            }
        }
        responses.pop(404)
        responses[400] = {
            "description": "Bad Request",
            "content": {
                "application/json": {
                    "examples": project_name_conditions
                    
                }
            }
        }
        responses[409] = {
            "description": "Conflict",
            "content": {
                "application/json": {
                    "examples": {
                        "Project exists": {
                            "value": {
                                "detail": "Project 'test-project' already exists. To update schema file, please delete and recreate the project"
                            }
                        },
                        "SHACLer failure": {
                            "value": "Execution of 'run_shacler' for project 'test-project' failed"
                        }
                    }
                }
            }
        }
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        **pre_conditions_examples,
                        "Missing SPHN import": {
                            "value": {
                                "detail": "SPHN schema IRI is not in the list of owl:imports of the project schema"
                            }
                        },
                        "Imports mismatch": {
                            "value": {
                                "detail": "Imported schema version 'sphn-iri' does not match SPHN schema owl:versionIRI"
                            }
                        }
                    }
                }
            }
        }
    elif endpoint == Endpoint.UPLOAD_EXTERNAL_FILES:
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "Files 'file1', 'file2' successfully ingested as external terminologies files"
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to upload external file 'file1': ..."
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        **pre_conditions_examples,
                        "Invalid query file": {
                            "value": {
                                "detail": "File 'filename' is not a SPARQL file. Expected extension: '.rq'"
                            }
                        },
                        "Invalid Pre-Check config": {
                            "value": {
                                "detail": "Check 'test-check' is misconfigured: 'regex' field is mandatory"
                            }
                        },
                        "Invalid De-Identification config": {
                            "value": {
                                "detail": "Rule 'test-rule' is misconfigured: 'applies_to_field' field is mandatory"
                            }
                        },
                        "Files in excess": {
                            "value": {
                                "detail": "Uploading more than one file of type 'Pre-Check Config' is forbidden"
                            }
                        },
                        "Wrong flag": {
                            "value": {
                                "detail": "To upload external files of type 'Pre-Check Config' the flag 'purge_before_upload' must be activated"
                            }
                        },
                        "Running pipeline": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has some running pipelines"
                            }
                        }
                        }
                }
            }
        }
    elif endpoint == Endpoint.GET_PROJECTS:
        responses[200] = {
            "description": "Map of existing projects",
            "content": {
                "application/json": {
                    "examples": {
                        "Projects list": {
                            "value": {
                                "projects": [
                                    {
                                        "project-name": "test-project",
                                        "schema-name": "test-project",
                                        "data-provider-id": "DATA-PROVIDER-ID",
                                        "created-by": "user",
                                        "is_initialized": True,
                                        "output_format": "Turtle",
                                        "share_with_einstein": False,
                                        "uploaded-files": {
                                            "project_specific_schema": [
                                                {
                                                    "filename": "test-project_schema.ttl",
                                                    "original_name": "sphn_schema_2024.2.ttl",
                                                    "file_size": "336.16KB",
                                                    "version_IRI": "https://biomedit.ch/rdf/sphn-ontology/sphn/2024/2"
                                                }
                                            ],
                                            "ext_shacls": [
                                                {
                                                    "filename": "Shacler_test-project_shacl.ttl",
                                                    "file_size": "118.74KB"
                                                }
                                            ],
                                            "ext_terminologies": [
                                                {
                                                    "filename": "sphn_ucum_2024-1.ttl",
                                                    "file_size": "115.1KB"
                                                }
                                            ]
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            }
        }
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Extraction of initialized projects failed: ..."
    elif endpoint == Endpoint.GET_JSON_SCHEMA:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to download JSON schema file for project 'test-project': ..."
        responses[200] = {
            "description": "File with project's JSON schema",
            "content": {
                "multipart/form-data": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                }
            }
        }
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Project not initialized": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has not been successfully initialized. Check init logs and recreate the project"
                            }
                        }
                    }   
                }
            }
        }
        responses[404]["content"]["application/json"]["examples"]["Missing JSON schema"] = {
            "value": {
                "detail": "JSON schema 'test-project_json_schema.json' not found on object storage. Make sure the project is successfully initialized"
            }
        }
    elif endpoint == Endpoint.GET_DDL_STATEMENTS:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to extract DDL statements for project 'test-project': ..."
        responses[200] = {
            "description": "Archive file with DDL statements",
            "content": {
                "application/x-zip-compressed": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                }
            }
        }
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Project not initialized": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has not been successfully initialized. Check init logs and recreate the project"
                            }
                        }
                    }  
                }
            }
        }
        responses[404]["content"]["application/json"]["examples"]["Missing DDL statements"] = {
            "value": {
                "detail": "No DDL creation statements found for project 'test-project'. Make sure it has been successfully initialized"
            }
        }
    elif endpoint == Endpoint.GET_CSV_TEMPLATES:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to extract CSV templates for project 'test-project'"
        responses[200] = {
            "description": "CSV files with tables structure",
            "content": {
                "application/x-zip-compressed": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                }
            }
        }
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Project not initialized": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has not been successfully initialized. Check init logs and recreate the project"
                            }
                        }
                    }  
                }
            }
        }
    elif endpoint == Endpoint.GET_EXCEL_TEMPLATE:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to Excel template for project 'test-project'"
        responses[200] = {
            "description": "Excel ingestion template",
            "content": {
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                }
            }
        }
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Project not initialized": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has not been successfully initialized. Check init logs and recreate the project"
                            }
                        }
                    }  
                }
            }
        }
    elif endpoint == Endpoint.GET_SPARQL_QUERIES:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to extract SPARQL queries for project 'test-project': ..."
        responses[200] = {
            "description": "Archive file with SPARQL queries",
            "content": {
                "application/x-zip-compressed": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                }
            }
        }
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Project not initialized": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has not been successfully initialized. Check init logs and recreate the project"
                            }
                        },
                        "SPARQLer running": {
                            "value": {
                                "detail": "SPARQLer is still running for project 'test-project'. Wait few minutes and retry..."
                            }
                        }
                    }  
                }
            }
        }
        responses[404]["content"]["application/json"]["examples"]["Missing SPARQL queries"] = {
            "value": {
                "detail": "No SPAQRL queries found for project 'test-project'. Make sure it has been successfully initialized"
            }
        }
    elif endpoint == Endpoint.RESET_PROJECT:
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "Project 'test-project' reset"
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Resetting of project 'test-project' failed: ..."
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Running pipeline": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has some running pipelines"
                                }
                        }
                    }
                }
            }
        }
    elif endpoint == Endpoint.DELETE_PROJECT:
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "Project 'test-project' deleted"
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Deletion of project 'test-project' failed: ..."
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Running pipeline": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has some running pipelines"
                                }
                        }
                    }
                }
            }
        }
        responses[401] = {
            "description": "Unauthorized user",
            "content": {
                "application/json": {
                    "examples": {
                        "User not authorized": {
                            "value": {
                                "detail": "User 'test-user' is not authorized to delete project 'test-project'. Only the user who created the project and admin users can delete it"
                                }
                        }
                    }
                }
            }
        }
    elif endpoint == Endpoint.INGEST_RDF:
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "RDF file 'filename' passed to SPHN connector"
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to pass RDF file 'filename' to SPHN connector: ..."
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Project not initialized": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has not been successfully initialized. Check init logs and recreate the project"
                            }
                        },
                        "Running pipeline": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has some running pipelines"
                                }
                        },
                        "Empty file": {
                            "value": {
                                "detail": "File 'filename' is empty and cannot be ingested. Please upload a file with data"
                            }
                        },
                        "Invalid RDF extension": {
                            "value": {
                                "detail": "File 'filename' is not of type RDF. Expected extension: '.ttl'"
                            }
                        }
                    }
                }
            }
        }
    elif endpoint == Endpoint.INGEST_JSON:
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "JSON file 'filename' passed to SPHN connector"
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to pass JSON file 'filename' to SPHN connector: ..."
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Project not initialized": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has not been successfully initialized. Check init logs and recreate the project"
                            }
                        },
                        "Running pipeline": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has some running pipelines"
                                }
                        },
                        "Empty file": {
                            "value": {
                                "detail": "File 'filename' is empty and cannot be ingested. Please upload a file with data"
                            }
                        },
                        "Invalid JSON content": {
                            "value": {
                                "detail": "Invalid JSON file provided 'filename'"
                            }
                        },
                        "Invalid JSON extension": {
                            "value": {
                                "detail": "File 'filename' is not of type JSON"
                            }
                        }
                    }
                }
            }
        }
    elif endpoint == Endpoint.INGEST_FROM_DATABASE:
        responses[200] = {
            "content": {
                "application/json": {
                    "examples": {
                        "Successful execution (project)": {
                            "value": {
                                "run_uuid": " 123e4567-e89b-12d3-a456-426614174000",
                                "message": "Extraction of database data for project 'test-project' triggered",
                            }
                        }
                    }
                },
                "text/plain": {
                    "examples": {
                        "No data to process": {
                            "value": {
                                "No data extraction running for project 'test-project'"
                            }
                        },
                    }
                },
            }
        }

        responses[500]["content"]["application/json"]["examples"] = {
            "Failed extraction": {
                "value": {
                    "detail": "Failed to extract data from database for project 'test-project': ..."
                }
            },
            "Failed status check": {
                "value": {
                    "detail": "Failed to check the status of database extraction for project 'test-project': ..."
                }
            },
            "Failed extraction stop": {
                "value": {
                    "detail": "Failed to stop database extraction for project 'test-project': ..."
                }
            },
            "Failed data extraction": {
                "value": {
                    "detail": "Data extraction for project '123' failed. Check Airflow logs"
                }
            }
        }
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Project not initialized": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has not been successfully initialized. Check init logs and recreate the project"
                            }
                        },
                        "Running pipeline": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has some running pipelines"
                                }
                        }
                    }
                }
            }
        }
        responses[404]["content"]["application/json"]["examples"]["Missing patient data"] = {
            "value": {
                "detail": "No new patients to process found on 'test-project.sphn_DataRelease' table for project 'test-project'"
            }
        }
        responses[404]["content"]["application/json"]["examples"]["No pipelines running"] = {
            "value": {
                "detail": "No pipelines found for project 'test-project'"
            }
        }
        responses[409] = {
            "description": "Conflict",
            "content": {
                "application/json": {
                    "examples": {
                        "Extraction still running": {
                            "value": {
                                "detail": "Data extraction for project 'test-project' is still running"
                            }
                        }
                    }
                }
            }
        }
    elif endpoint == Endpoint.INGEST_CSV:
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "CSV file 'filename.csv' passed to SPHN Connector"
        responses[200]["content"]["text/plain"]["examples"]["Successful execution (ZIP)"] = {"value": "CSV files in 'filename.zip' passed to SPHN Connector"}
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to ingest CSV data for project 'test-project': ..."
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Project not initialized": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has not been successfully initialized. Check init logs and recreate the project"
                            }
                        },
                        "Running pipeline": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has some running pipelines"
                                }
                        },
                        "Invalid CSV file": {
                            "value": {
                                "detail": "File 'filename.ext' is not a CSV file"
                            }
                        },
                        "Invalid ZIP file": {
                            "value": {
                                "detail": "File 'filename.ext' is not a ZIP file"
                            }
                        },
                        "Invalid CSV file in ZIP archive": {
                            "value": {
                                "detail": "File 'filename.ext' in zipped archive is not a CSV file"
                            }
                        }
                    }
                }
            }
        }
    elif endpoint == Endpoint.INGEST_EXCEL:
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "Excel data ingested into SPHN Connector for project 'test-project'"
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to ingest Excel data for project 'test-project': ..."
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Project not initialized": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has not been successfully initialized. Check init logs and recreate the project"
                            }
                        },
                        "Running pipeline": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has some running pipelines"
                                }
                        },
                        "Invalid file": {
                            "value": {
                                "detail": "File 'filename.ext' is not an Excel file"
                            }
                        },
                        "Failed ingestion": {
                            "value": {
                                "detail": "Update of table 'test-table' failed for project 'test-project'. Potential wrong data provided (invalid types, not-null constraint violations, ...). Fix the data, reset the project, and reingest the data. Exception: ..."
                            }
                        }
                    }
                }
            }
        }
    elif endpoint == Endpoint.TRIGGER_BATCH_INGESTION:
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "Batch ingestion of data for project 'test-project' triggered"
        responses[200]["content"]["text/plain"]["examples"]["Status check"] = {
            "value": "No batch data upload running for project 'test-project'"
            }   
        responses[500]["content"]["application/json"]["examples"] = {
            "Failed triggering": {
                "value": {
                    "detail": "Failed to trigger batch ingestion for project ''test-project': ..."
                }
            },
            "Failed status check": {
                "value": {
                    "detail": "Failed to check the status of batch ingestion for project 'test-project': ..."
                }
            },
            "Failed ingestion stop": {
                "value": {
                    "detail": "Failed to stop batch ingestion for project 'test-project': ..."
                }
            },
            "Failed ingestion": {
                "value": {
                    "detail": "Batch ingestion of data for project 'test-project' failed"
                }
            }
        }
        responses[404]["content"]["application/json"]["examples"]["Missing bucket"] = {
            "value": {
                "detail": "Bucket 'test-project' not found"
            }
        }
        responses[404]["content"]["application/json"]["examples"]["Missing data"] = {
            "value": {
                "detail": "No batch files found for project 'test-project' under prefix 'test-project/Input/batch_ingestion/'"
            }
        }
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Project not initialized": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has not been successfully initialized. Check init logs and recreate the project"
                            }
                        },
                        "Running pipeline": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has some running pipelines"
                                }
                        },
                        "Failed ingestion": {
                            "value": {
                                "detail": "Update of table 'test-table' failed for project 'test-project'. Potential wrong data provided (invalid types, not-null constraint violations, ...). Fix the data, reset the project, and reingest the data. Exception: ..."
                            }
                        }
                    }
                }
            }
        }
        responses[409] = {
            "description": "Conflict",
            "content": {
                "application/json": {
                    "examples": {
                        "Extraction still running": {
                            "value": {
                                "detail": "Batch ingestion of data for project 'test-project' is still running"
                            }
                        }
                    }
                }
            }
        }
    elif endpoint == Endpoint.START_PIPELINE:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to trigger processing of data for project 'test-project': ..."
        responses[200] = {
            "description": f"Return message for successful execution of endpoint {endpoint.value}",
            "content": {
                "application/json": {
                    "examples": {
                        "Successful execution (project)": {
                            "value": {
                                "run_uuid": " 123e4567-e89b-12d3-a456-426614174000",
                                "message": "Processing of data for project 'test-project' triggered"
                            }
                        },
                        "Successful execution (project, patient_id)": {
                            "value": {
                                "run_uuid": " 123e4567-e89b-12d3-a456-426614174000",
                                "message": "Processing of data for project 'test-project' and patient 'test-patient-id' triggered"
                            }
                        },
                        "Successful execution (project, step)": {
                            "value": {
                                "run_uuid": " 123e4567-e89b-12d3-a456-426614174000",
                                "message": "Processing of data for project 'test-project' and step 'Integration' triggered"
                            }
                        },
                        "Successful execution (project, patient_id, step)": {
                            "value": {
                                "run_uuid": " 123e4567-e89b-12d3-a456-426614174000",
                                "message": "Processing of data for project 'test-project', patient 'test-patient-id', and step 'Integration' triggered"
                            }
                        }
                    }
                }
            }
        }
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Project not initialized": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has not been successfully initialized. Check init logs and recreate the project"
                            }
                        },
                        "Running pipeline": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has some running pipelines"
                                }
                        },
                        "Maximum number of parallel projects": {
                            "value": {
                                "detail": "Project 'test-project' cannot be triggered becuase there are already 4 projects in running/queued state"
                            }
                        }
                    }
                }
            }
        }
        responses[404]["content"]["application/json"]["examples"]["Missing patient data"] = {
            "value": {
                "detail": "No data to process found for 'test-project'. Make sure necessary steps are executed"
            }
        }
    elif endpoint == Endpoint.STOP_PIPELINE:
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "Processing of data for project 'test-project' stopped"
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to stop the processing of data for project 'test-project': ..."
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Project not initialized": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has not been successfully initialized. Check init logs and recreate the project"
                            }
                        }
                    }
                }
            }
        }
        responses[404]["content"]["application/json"]["examples"]["No pipelines running"] = {
            "value": {
                "detail": "No pipelines found for project 'test-project'"
            }
        }
    elif endpoint == Endpoint.START_STATISTICS:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to trigger statistics for project 'test-project': ..."
        responses[200] = {
            "description": f"Return message for successful execution of endpoint {endpoint.value}",
            "content": {
                "application/json": {
                    "examples": {
                        "Successful execution": {
                            "value": {
                                "run_uuid": " 123e4567-e89b-12d3-a456-426614174000",
                                "message": "Statistics of project 'test-project' triggered"
                            }
                        }
                    }
                }
            }
        }
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Project not initialized": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has not been successfully initialized. Check init logs and recreate the project"
                            }
                        },
                        "Running pipeline": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has some running pipelines"
                                }
                        },
                        "Maximum number of parallel projects": {
                            "value": {
                                "detail": "Project 'test-project' cannot be triggered becuase there are already 4 projects in running/queued state"
                            }
                        }
                    }
                }
            }
        }
        responses[404]["content"]["application/json"]["examples"]["Missing patient data"] = {
            "value": {
                "detail": "No data to process found for 'test-project'. Make sure necessary steps are executed"
            }
        }
    elif endpoint == Endpoint.GET_STATUS:
        responses[200] = {
            "description": "Project status history",
            "content": {
                "application/json": {
                    "examples": {
                        "Successful execution": {
                            "value": [
                                {
                                    "run_uuid": "c9f5b0c1-c7ee-48f4-bca0-1850668f83d1",
                                    "project_name": "test-project",
                                    "pipeline_step": "Pre-check/De-ID",
                                    "pipeline_status": "SUCCESS",
                                    "patients_processed": 100,
                                    "patients_with_warnings": 0,
                                    "patients_with_errors": 0,
                                    "execution_time": "2022-12-08 18:17:10",
                                    "elapsed_time": "0:00:13"
                                }
                            ]
                        }
                    }
                    
                }
            }
        }
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to extract pipelines status for project 'test-project': ..."
        responses[404]["content"]["application/json"]["examples"]["No pipelines running (project)"] = {
            "value": {
                "detail": "No pipelines found for project 'test-project'"
            }
        }
        responses[404]["content"]["application/json"]["examples"]["No pipelines running (project, step)"] = {
            "value": {
                "detail": "No pipelines found for project 'test-project' and step 'Integration'"
            }
        }
    elif endpoint == Endpoint.GET_PATIENTS_WITH_ERRORS:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to extract patient IDs with errors for project 'test-project': ..."
        responses[200] = {
            "description": "Map of patients with errors",
            "content": {
                "application/json": {
                    "examples": {
                        "Successful execution": {
                            "value": {
                                "step": "Pre-check/De-ID",
                                "error_level": "WARNING",
                                "run_uuids": {
                                    "23a038e4-18f6-4f75-acbb-b94a8d0d8c19": {
                                    "patient_ids": [
                                        "1"
                                    ]
                                    }
                                }
                                }
                            }
                        }
                    }
            }
        }
        responses[404]["content"]["application/json"]["examples"]["No errors found"] = {
            "value": {
                "detail": "No errors found for project 'test-project', step 'Integration', and error level 'ERROR'"
            }
        }
    elif endpoint == Endpoint.GET_LOGS:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to extract logs for project 'test-project': ..."
        responses[200] = {
            "description": "Log file(s)",
            "content": {
                "application/x-zip-compressed": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                },
                "multipart/form-data": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                }
            }
        }
        responses[404]["content"]["application/json"]["examples"]["No logs found (project)"] = {
            "value": {
                "detail": "No logs found for project 'test-project'"
            }
        }
        responses[404]["content"]["application/json"]["examples"]["No logs found (project, patient_id)"] = {
            "value": {
                "detail": "No logs found for patient 'test-patient-id' and project 'test-project'"
            }
        }
        responses[404]["content"]["application/json"]["examples"]["No logs found (project, step)"] = {
            "value": {
                "detail": "No logs found for project 'test-project' and step 'Integration'"
            }
        }
        responses[404]["content"]["application/json"]["examples"]["No logs found (project, patient_id, step)"] = {
            "value": {
                "detail": "No logs found for patient 'test-patient-id', and project 'test-project', and step 'Integration'"
            }
        }
    elif endpoint == Endpoint.GET_GLOBAL_LOGS:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to extract global logs for project 'test-project': ..."
        responses[200] = {
            "description": "Global logs file",
            "content": {
                "multipart/form-data": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                }
            }
       }
        responses[404]["content"]["application/json"]["examples"]["No logs found"] = {
            "value": {
                "detail": "No global logs found for project 'test-project'"
            }
        }
    elif endpoint == Endpoint.GET_QC_REPORT:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to extract quality checks report for project 'test-project': ..."
        responses[200] = {
            "description": "Quality checks report",
            "content": {
                "multipart/form-data": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                }
            }
        }
        responses[404]["content"]["application/json"]["examples"]["No qc found"] = {
            "value": {
                "detail": "No quality checks found"
            }
        }
    elif endpoint == Endpoint.GET_INIT_LOGS:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to extract init logs project 'test-project': ..."
        responses[200] = {
            "description": "Init log files",
            "content": {
                "application/x-zip-compressed": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                }
            }
        }
        responses[404]["content"]["application/json"]["examples"]["No logs found"] = {
            "value": {
                "detail": "No init logs found for project 'test-project'"
            }
        }
    elif endpoint == Endpoint.GET_AIRFLOW_LOGS:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to extract Airflow logs project 'test-project': ..."
        responses[200] = {
            "description": "Airflow logs file",
            "content": {
                "multipart/form-data": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                }
            }
       }
    elif endpoint == Endpoint.GET_PATIENT_FILE:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed download patient file for project 'test-project' and patient 'test-patient-id': ..."
        responses[200] = {
            "description": "Patient file",
            "content": {
                "multipart/form-data": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                }
            }
        }
        responses[404]["content"]["application/json"]["examples"]["No file found"] = {
            "value": {
                "detail": "File 'filename' for project 'test-project' and patient 'test-patient-id' not found in object storage"
            }
        }
    elif endpoint == Endpoint.GET_PATIENTS_REPORT:
        responses[200] = {
            "description": "Patients report",
            "content": {
                "application/json": {
                    "examples": {
                        "Report": {
                            "value": {
                                "Pre-Release DB ingestion (Patient is ingested into DB but not released to sphn_DataRelease)": {
                                    "total": 0,
                                    "released": 0,
                                    "not-released": 0,
                                    "not-released-patients": []
                                },
                                "Ingested into DB (Patient is ingested into DB but /Ingest_from_database not run)": {
                                    "total": 0,
                                    "processed": 0,
                                    "not-processed": 0,
                                    "patients-not-processed": []
                                },
                                "Pre-check & De-Identification (Pre-check and De-Identification not run or failed)": {
                                    "total": 400,
                                    "processed": 400,
                                    "not-processed": 0,
                                    "not-processed-patients": []
                                },
                                "Transformation (not transformed to RDF or failed)": {
                                    "total": 400,
                                    "processed": 400,
                                    "not-processed": 0,
                                    "not-processed-patients": []
                                },
                                "Validation (Validation failed or not run)": {
                                    "total": 400,
                                    "processed": 399,
                                    "not-processed": 1,
                                    "not-processed-patients": [
                                        'patient-1'
                                    ]
                                },
                                "Notify-Einstein (Notify-Einstein failed or not run)": {
                                    "total": 400,
                                    "shared": 400,
                                    "not-shared": 0,
                                    "not-shared-patients": []
                                },
                                "Downloaded (not yet downloaded)": {
                                    "total": 400,
                                    "downloaded": 399,
                                    "not-downloaded": 1,
                                    "not-downloaded-patients": [
                                        'patient-1'
                                    ]
                                }
                            }
                        }
                    }
                }
            }
        }
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed extract patients report for project 'test-project': ..."
    elif endpoint == Endpoint.GET_DOCKER_LOGS:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"][
            "detail"] = "Failed to extract Docker logs: ..."
        responses[200] = {
            "description": "Docker logs file",
            "content": {
                "multipart/form-data": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                }
            }
        }
        responses.pop(404)
    elif endpoint == Endpoint.GET_PROCESSED_PATIENTS:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to extract processed patients list for project 'test-project': ..."
        responses[200] = {
            "description": "List of processed patients",
            "content": {
                "application/json": {
                    "examples": {
                        "Successful execution": {
                            "value": {
                                "patients": [
                                    {
                                        "id": "test-patient-id",
                                        "validated_ok": False,
                                        "timestamp": "12-12-2022 11:14:48"
                                    }
                                    ]
                                }
                            }
                        }
                    }
            }
        }
        responses[404]["content"]["application/json"]["examples"]["No patients found"] = {
            "value": {
                "detail": "No processed patients found for project 'test-project'"
            }
        }
    elif endpoint == Endpoint.DOWNLOAD_DATA:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to download files for patient id 'test-patient-id' project 'test-project' and validation status 'OK': ..."
        responses[200] = {
            "description": "Processed file(s)",
            "content": {
                "application/x-zip-compressed": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                },
                "multipart/form-data": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                }
            }
        }
        responses[404]["content"]["application/json"]["examples"]["No files found (project)"] = {
            "value": {
                "detail": "No files to download for project 'test-project' and validation status 'OK'"
            }
        }
        responses[404]["content"]["application/json"]["examples"]["No files found (project, patient_id)"] = {
            "value": {
                "detail": "No files to download for patient id 'test-patient-id' and project 'test-project' and validation status 'OK'"
            }
        }
    elif endpoint == Endpoint.DOWNLOAD_IN_S3:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to trigger data download in S3 for project 'test-project' and validation status 'OK': "
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "Download of data in S3 bucket 'test-project' triggered. Object will be stored with prefix 'test-project/Download/test-project.zip'"
        responses[200]["content"]["text/plain"]["examples"]["Status check"] = {
            "value": "No data download running for project 'test-project'"
            }  
        responses[200]["content"]["text/plain"]["examples"]["Stop download"] = {
            "value": "Processing of data for project 'test-project' stopped"
            }
        responses[404]["content"]["application/json"]["examples"]["No files found (project)"] = {
            "value": {
                "detail": "No files to download for project 'test-project'"
            }
        }
        responses[404]["content"]["application/json"]["examples"]["No files found (project, patient_id)"] = {
            "value": {
                "detail": "No files to download for patient id 'test-patient-id' and project 'test-project'"
            }
        }
        responses[404]["content"]["application/json"]["examples"]["No bundle"] = {
            "value": {
                "detail": "No download bundle found for project 'test-project'"
            }
        }
        responses[409] = {
            "description": "Conflict",
            "content": {
                "application/json": {
                    "examples": {
                        "Already running": {
                            "value": {
                                "detail": "S3 download for project 'test-project' is still running"
                            }
                        }
                    }
                }
            }
        }
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Running pipeline": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has some running pipelines"
                                }
                        }
                    }
                }
            }
        }
    elif endpoint == Endpoint.GET_DE_IDENTIFICATION_REPORT:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to extract De-Identification report for project 'test-project': ..."
        responses[200] = {
            "description": "De-Identification report file",
            "content": {
                "application/x-zip-compressed": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                }
            }
        }
        responses[404]["content"]["application/json"]["examples"]["No De-Identification report found"] = {
            "value": {
                "detail": "No De-Identification report found for project 'test-project'"
            }
        }
    elif endpoint == Endpoint.RESET_DOWNLOAD_STATUS:
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "Download status of project 'test-project' reset"
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to reset downloaded data status for project 'test-project': ..."
    elif endpoint == Endpoint.GET_STATISTICS_REPORTS:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to extract project 'test-project' statistics: ..."
        responses[200] = {
            "description": "Archive file with statistics data",
            "content": {
                "application/x-zip-compressed": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                },
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                }
            }
        }
        responses[404]["content"]["application/json"]["examples"]["No statistics data found"] = {
            "value": {
                "detail": "No statistics found for project 'test-project'"
            }
        }
    elif endpoint == Endpoint.CHANGE_PASSWORD:
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "Password for API user 'test-user' successfully updated"
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Changing of user 'test-user' password failed: ..."
        responses[404]["content"]["application/json"]["examples"]["API user not found"] = {
            "value": {
                "detail": "API user 'test-user' not found"
            }
        }
        responses[405] = {
            "description": "Method Not Allowed",
            "content": {
                "application/json": {
                    "examples": {
                        "Super-user change": {
                            "value": {
                                "detail": "Admin user 'test-user' cannot be modified. To change it, update configuration file, and restart the connector"
                            }
                        }
                    }
                }
            }
        }
        responses[400] = {
            "description": "Bad Request",
            "content": {
                "application/json": {
                    "examples": {
                        "Wrong old password": {
                            "value": {
                                "detail": "Provided old password for user 'test-user' differs from the existing one. No changes applied"
                            }
                        },
                        "Missing permissions": {
                            "value": {
                                "detail": "Current user does not have permissions to modify other users"
                            }
                        },
                        "Reused password": {
                            "value": {
                                "detail": "New password for user 'test-user' must differ from the old one"
                            }
                        }
                    }
                    
                }
            }
        }
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Invalid password": {
                            "value": {
                                "detail": "Password must be at least 12 characters long"
                            }
                        }
                    }
                }
            }
        }
    elif endpoint == Endpoint.GET_USERS_LIST:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Extraction of API users failed: ..."
        responses[200] = {
            "description": "List of existing API users",
            "content": {
                "application/json": {
                    "examples": {
                        "Successful execution": {
                            "value": {
                                "admin_users": ["test-user"],
                                "standard_users": [],
                                "ingestion_users": []
                            }
                        }
                    }
                }
            }
        }
    elif endpoint == Endpoint.CREATE_NEW_USER:
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "API user 'test-user' successfully configured"
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Configuration of API user 'test-user' failed: ..."
        responses.pop(404)
        responses[400] = {
            "description": "Bad Request",
            "content": {
                "application/json": {
                    "examples": {
                        "Existing user": {
                            "value": {
                                "detail": "API user 'test-user' already exists. To reset the password for that user, please use the ad hoc method"
                            }
                        },
                        "Missing permissions": {
                            "value": {
                                "detail": "Current user does not have permissions to create ADMIN type users"
                            }
                        }
                    }
                    
                }
            }
        }
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Invalid user name": {
                            "value": {
                                "detail": "Creation of API user 'test-user' failed. User name must match regex expression ^[a-zA-Z0-9\._-]*$"
                            }
                        },
                        "Invalid password": {
                            "value": {
                                "detail": "Password must be at least 12 characters long"
                            }
                        },
                        "Creation failed": {
                            "value": {
                                "detail": "Creation of API user 'test-user' failed: ..."
                            }
                        }
                    }
                }
            }
        }
    elif endpoint == Endpoint.DELETE_USER:
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "API user 'test-user' successfully removed"
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Deletion of API user 'test-user' failed: ..."
        responses[404]["content"]["application/json"]["examples"]= {
            "API user not found": {
                "value": {
                    "detail": "API user 'test-user' not found"
                }
                }
            }
        responses[405] = {
            "description": "Method Not Allowed",
            "content": {
                "application/json": {
                    "examples": {
                        "Super-user change": {
                            "value": {
                                "detail": "Admin user 'test-user' cannot be modified. To change it, update configuration file, and restart the connector"
                            }
                        }
                    }
                }
            }
        }
        responses[400] = {
            "description": "Bad Request",
            "content": {
                "application/json": {
                    "examples": {
                        "Missing permissions": {
                            "value": {
                                "detail": "Current user does not have permissions to delete ADMIN type users"
                            }
                        }
                    }
                }
            }
        }
    elif endpoint == Endpoint.RESET_PASSWORD:
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "Password for API user 'test-user' successfully updated"
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Resetting of user 'test-user' password failed: ..."
        responses[404]["content"]["application/json"]["examples"]= {
            "API user not found": {
                "value": {
                    "detail": "API user 'test-user' not found"
                }
                }
            }
        responses[405] = {
            "description": "Method Not Allowed",
            "content": {
                "application/json": {
                    "examples": {
                        "Super-user change": {
                            "value": {
                                "detail": "Admin user 'test-user' cannot be modified. To change it, update configuration file, and restart the connector"
                            }
                        }
                    }
                }
            }
        }
        responses[400] = {
            "description": "Bad Request",
            "content": {
                "application/json": {
                    "examples": {
                        "Missing permissions": {
                            "value": {
                                "detail": "Current user does not have permissions to reset other ADMIN type users"
                            }
                        },
                        "Not allowed": {
                            "value": {
                                "detail": "Only admin users can reset passwords"
                            }
                        }
                    }
                }
            }
        }
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Invalid password": {
                            "value": {
                                "detail": "Password must be at least 12 characters long"
                            }
                        }
                    }
                }
            }
        }
    elif endpoint == Endpoint.EXPORT_PROJECT:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to export project 'test-project': ..."
        responses[200] = {
            "description": "Archive file with project configuration files",
            "content": {
                "application/x-zip-compressed": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                }
            }
        }
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Project not initialized": {
                            "value": {
                                "detail": "Cannot trigger action because project 'test-project' has not been successfully initialized. Check init logs and recreate the project"
                            }
                        }
                    }
                }
            }
        }
        responses[404]["content"]["application/json"]["examples"]["Missing JSON schema"] = {
            "value": {
                "detail": "JSON schema 'test-project_json_schema.json' not found on object storage. Make sure the project is successfully initialized"
            }
        }
    elif endpoint == Endpoint.IMPORT_PROJECT:
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "Project 'test-project' successfully imported"
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed create project 'test-project' from ZIP file 'test-zip.zip': ..."
        responses.pop(404)
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Empty file": {
                            "value": {
                                "detail": "File 'filename' is empty and cannot be ingested. Please upload a file with data"
                            }
                        },
                        "Invalid ZIP extension": {
                            "value": {
                                "detail": "File 'test-zip.zip' is not in compressed ZIP format"
                            }
                        }
                    }
                }
            }
        }
        responses[400] = {
            "description": "Bad Request",
            "content": {
                "application/json": {
                    "examples": project_name_conditions 
                }
            }
        }
        responses[409] = {
            "description": "Conflict",
            "content": {
                "application/json": {
                    "examples": {
                        "Project exists": {
                            "value": {
                                "detail": "Project 'test-project' already exists. To update schema file, please delete and recreate the project"
                            }
                        }
                    }
                }
            }
        }
    elif endpoint == Endpoint.BACKUP_DATABASE:
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to backup Connector database: ..."
        responses[200] = {
            "description": "Archive file with SPHN Connector database data",
            "content": {
                "text/plain": {
                    "examples": {
                        "Start successful": {
                            "value": "Backup of SPHN Connector database triggered. Backup will be stored under 'sphn-connector-backup' prefix in Connector bucket in S3"
                        },
                        "Status check": {
                            "value": "No backup running"
                        },
                        "Stop backup": {
                            "value": "Backup of SPHN Connector database stopped"
                        }
                    }
                },
                "application/x-zip-compressed": {
                    "examples": {
                        "Download file": {
                            "value": "File"
                        }
                    }
                }
            }
        }
        responses[404]["content"]["application/json"]["examples"]= {
            "No backup running": {
                "value": {
                    "detail": "No backup of SPHN Connector database running"
                }
                },
            "No backup found": {
                "value": {
                    "detail": "No backup of SPHN Connector database found"
                }
                }
            }
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Running pipeline": {
                            "value": {
                                "detail": "Cannot trigger action because there are some running pipelines"
                                }
                        }
                    }
                }
            }
        }
    elif endpoint == Endpoint.RESTORE_DATABASE:
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "SPHN Connector database restored"
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to restore SPHN Connector database: ..."
        responses.pop(404)
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "SPHN Connector not empty": {
                            "value": {
                                "detail": "SPHN Connector database is not empty. Internal table 'test-table' contains data. SPHN Connector databasemust be completely empty in order to be restored"
                            }
                        },
                        "Older backup": {
                            "value": {
                                "detail": "SPHN Connector current commit 'commit_timestamp' is older than backup commit 'commit_datetime'. Restore not possible..."
                            }
                        }
                    }
                }
            }
        }
    elif endpoint == Endpoint.RESTORE_DE_IDENTIFICATION:
        responses[200]["content"]["text/plain"]["examples"]["Successful execution"]["value"] = "De-Identification for project 'test-project' restored"
        responses[500]["content"]["application/json"]["examples"]["Failed execution"]["value"]["detail"] = "Failed to restore de-identification: ..."
        responses[422] = {
            "description": "Unprocessable Entity",
            "content": {
                "application/json": {
                    "examples": {
                        "Invalid ZIP file": {
                            "value": {
                                "detail": "File 'filename.ext' is not a ZIP file"
                            }
                        }
                    }
                }
            }
        }
    return responses


def remove_validation_response(openapi_schema: dict):
    """Remove response 422 for specific endpoints

    Args:
        openapi_schema (dict): OpenAPI schema
    """
    get_endpoints = [Endpoint.GET_STATUS, Endpoint.GET_PATIENTS_REPORT, Endpoint.GET_PATIENTS_WITH_ERRORS, Endpoint.GET_LOGS, Endpoint.GET_GLOBAL_LOGS, Endpoint.GET_QC_REPORT, Endpoint.GET_INIT_LOGS, Endpoint.GET_AIRFLOW_LOGS, Endpoint.GET_PATIENT_FILE, Endpoint.GET_PROCESSED_PATIENTS, Endpoint.GET_DE_IDENTIFICATION_REPORT, Endpoint.DOWNLOAD_DATA, Endpoint.GET_DOCKER_LOGS]
    delete_endpoints = [Endpoint.DELETE_USER]
    post_endpoints = [Endpoint.RESET_DOWNLOAD_STATUS]
    for endpoint in get_endpoints:
        openapi_schema['paths'][endpoint.value]['get']['responses'].pop("422")
    for endpoint in delete_endpoints:
        openapi_schema['paths'][endpoint.value]['delete']['responses'].pop("422")
    for endpoint in post_endpoints:
        openapi_schema['paths'][endpoint.value]['post']['responses'].pop("422")
