#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'api.py' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

import io
import os
import shutil
from airflow_lib import Airflow
from minio_policy import MinioPolicy
from fastapi import Response, HTTPException, status, UploadFile
from enums import CompressionType, ConfigFileAction, DataPersistenceLayer, FileClass, FileType, RML_JSON_LOGS, IngestionType, OutputFormat, PreCondition, DockerService
from database import Database, bucket_exists
from config import Config
import rml_generator as rml
import logging
import zipfile
import re
from datetime import datetime
from fastapi.responses import StreamingResponse
from botocore.exceptions import ClientError
import traceback
from starlette.responses import HTMLResponse
from rdf_parser import get_schema_iri, check_imports_match, check_sphn_import, validate_external_terminology, validate_rdf_file
logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s', level=logging.getLevelName('INFO'), datefmt='%Y-%m-%d %H:%M:%S')


def get_object_name(project_name: str, 
                    data_provider_id: str, 
                    file_class: FileClass, 
                    patient_id: str = None, 
                    file_type: FileType = None, 
                    filename: str = None) -> str:
    """Gets the object name in S3

    Args:
        project_name (str): name of the project
        data_provider_id (str): id of data provide
        file_class (FileClass): class of the file
        patient_id (str, optional): unique patient identifier. Defaults to None.
        file_type (FileType, optional): type of the file either RDF or JSON. Defaults to None.
        filename (str, optional): name of the file (used for external resources only). Defaults to None.

    Raises:
        Exception: not supported file format

    Returns:
        str: object name in S3
    """
    if file_class == FileClass.PATIENT_DATA:
        extension = ".ttl" if file_type == FileType.RDF else ".json"
        filename = f"{project_name}_{patient_id}_{data_provider_id}{extension}"
        return f"{project_name}/Input/{DataPersistenceLayer.LANDING_ZONE.value}/{filename}"
    elif file_class == FileClass.SCHEMA:
        return f"{project_name}/Schema/" + project_name + "_schema.ttl"
    elif file_class == FileClass.EXT_SCHEMA:
        return f"{project_name}/QAF/Schemas/" + filename.replace(' ', '')
    elif file_class == FileClass.EXT_SHACL:
        return f"{project_name}/QAF/SHACL/" + filename
    elif file_class == FileClass.EXT_TERMINOLOGY:
        return f"{project_name}/QAF/Terminologies/" + filename
    elif file_class == FileClass.EXT_QUERIES:
        return f"{project_name}/QAF/Queries/" + filename
    elif file_class == FileClass.EXCEPTIONS:
        return f"{project_name}/QAF/Exceptions/" + filename
    elif file_class == FileClass.RML:
        return f"{project_name}/RML/" + filename
    elif file_class == FileClass.PRE_CHECK_CONFIG:
        return f"{project_name}/Pre-Check/{project_name}_checks_config.json"
    elif file_class == FileClass.DE_IDENTIFICATION_CONFIG:
        return f"{project_name}/De-Identification/{project_name}_de_identification_config.json"
    elif file_class == FileClass.REPORT_SPARQL_QUERY:
        return f"{project_name}/QAF/Report-Queries/report_sparql_query.rq"
    else:
        raise Exception("File format '{}' not supported".format(file_class))

def create_s3_bucket(bucket:str, config: Config):
    """Initializes the buckets in S3 storage

    Args:
        bucket (str): name of the bucket
        config (Config): configuration object
    """
    if not config.use_external_s3:
        try:
            # Check bucket existence
            config.s3_client.head_bucket(Bucket=bucket)
        except ClientError as e:
            if e.response['Error']['Code'] == '404':
                # Create bucket if it does not exist
                config.s3_client.create_bucket(Bucket=bucket)
            else:
                error_message = f"Connection to external object storage returned code '{e.response['Error']['Code']}'. Exception raised:\n{traceback.format_exc(chain=False)}"
                print(error_message)



def write_to_s3(content: bytes, 
                project_name: str, 
                file_class: FileClass, 
                database: Database, 
                patient_id: str = None, 
                file_type: FileType = None,
                filename: str = None, 
                ingestion_type: IngestionType = None):
    """Writes bytes content of the input data to file in S3

    Args:
        content (bytes): input data
        project_name (str): name of the project
        file_class (FileClass): class of the file
        database (Database): database object
        patient_id (str, optional): unique patient identifier. Defaults to None.
        file_type (FileType, optional): type of the file either RDF or JSON. Defaults to None.
        filename (str, optional): name of the file (used for external resources only). Defaults to None.
        ingestion_type (IngestionType, optional): type of ingestion. Defaults to None.
    """
    bucket = database.config.s3_connector_bucket
    create_s3_bucket(bucket = bucket, config=database.config)
    file_size = io.BytesIO(content).getbuffer().nbytes
    object_name = get_object_name(project_name=project_name, data_provider_id=database.config.data_provider_id, file_class=file_class, patient_id=patient_id, file_type=file_type, filename=filename)
    if patient_id is not None:
        database.cleanup_before_ingestion(project_name=project_name, patient_id=patient_id)
    database.config.s3_client.put_object(Bucket=bucket, Key=object_name, Body=content, ContentLength = file_size)
    if file_class in [FileClass.SCHEMA, FileClass.PRE_CHECK_CONFIG, FileClass.DE_IDENTIFICATION_CONFIG]:
        database.update_names_tracker(project_name=project_name, file_class=file_class, object_name=os.path.basename(object_name), original_name=filename)
    if file_class in [FileClass.SCHEMA, FileClass.PRE_CHECK_CONFIG, FileClass.DE_IDENTIFICATION_CONFIG, FileClass.EXT_SCHEMA, FileClass.EXCEPTIONS, file_class.EXT_QUERIES, file_class.EXT_SHACL, FileClass.EXT_TERMINOLOGY, FileClass.REPORT_SPARQL_QUERY]:
        database.update_config_files_history(project=project_name, filename=os.path.basename(object_name), action=ConfigFileAction.UPLOADED, file_class=file_class)
    if patient_id is not None:
        database.create_connection()
        sql = f"INSERT INTO {DataPersistenceLayer.LANDING_ZONE.value} (project_name, patient_id, data_provider_id, object_name, file_type, timestmp, ingestion_type, processed) VALUES (%s, %s, %s, %s, %s, %s, %s, False) \
                ON CONFLICT (project_name, patient_id, data_provider_id) DO UPDATE SET object_name=EXCLUDED.object_name, file_type=EXCLUDED.file_type, timestmp=EXCLUDED.timestmp, ingestion_type=EXCLUDED.ingestion_type, processed=EXCLUDED.processed"
        database.cursor.execute(sql, (project_name, patient_id, database.config.data_provider_id, object_name, file_type.value, datetime.now(), ingestion_type.value))
        database.close_connection()

def load_csv_files(content: bytes, project: str, database: Database):
    """Load CSV files in ZIP archive into SPHN Connector batching folder

    Args:
        content (bytes): ZIP content
        project (str): name of the project
        database (Database): database object
    """
    bytes_buffer = io.BytesIO(content)
    with zipfile.ZipFile(bytes_buffer) as zip_ref:
        for zipinfo in zip_ref.infolist():
            with zip_ref.open(zipinfo) as csv_file:
                file_content = csv_file.read()
                write_to_batch_folder(content=file_content, project_name=project, filename=os.path.basename(zipinfo.filename), file_type=FileType.CSV, database=database)

def write_to_batch_folder(content: bytes, project_name: str, filename: str, file_type: FileType, database: Database):
    """Write uploaded file to Ingestion/batch_ingestion/ prefix

    Args:
        content (bytes): content of the file
        project_name (str): name of the project
        filename (str): name of the file (e.g. patient_id, ingested filename)
        file_type (FileType): file type
        database (Database): database object
    """
    bucket = database.config.s3_connector_bucket
    file_size = io.BytesIO(content).getbuffer().nbytes
    if file_type == FileType.RDF:
        extension = ".ttl"
    elif file_type == FileType.JSON:
        extension = ".json"
    else:
        extension = ""
    object_name = f"{project_name}/Input/batch_ingestion/{filename}{extension}"
    database.config.s3_client.put_object(Bucket=bucket, Key=object_name, Body=content, ContentLength = file_size )


def get_schema(project: str, database: Database, rml_folder: str) -> str:
    """Downloads project schema locally and returns schema file path

    Args:
        project (str): name of the project
        database (Database): database object
        rml_folder (str): local folder with RML tmp data

    Returns:
        str: schema local file path
    """
    bucket = database.config.s3_connector_bucket
    schema_file = get_object_name(project_name=project, data_provider_id=database.config.data_provider_id, file_class=FileClass.SCHEMA)
    schema_path = os.path.join(rml_folder, os.path.basename(schema_file))
    os.makedirs(rml_folder, exist_ok=True)
    database.config.s3_client.download_file(Bucket=bucket, Key=schema_file, Filename=schema_path)
    return schema_path


def get_external_schemas(project: str, database: Database, rml_folder: str) -> list:
    """Downloads external schemas (if any) locally and returns list with local paths

    Args:
        project (str): name of the project
        database (Database): database object
        rml_folder (str): local folder with RML tmp data

    Returns:
        list: list of external schemas local paths
    """
    ext_schemas_folder = os.path.join(rml_folder, 'schemas')
    os.makedirs(ext_schemas_folder, exist_ok=True)
    bucket = database.config.s3_connector_bucket
    
    ext_schemas = database.list_objects_keys(bucket=bucket, prefix=f"{project}/QAF/Schemas/")

    ext_schemas_paths = []
    for ext_schema in ext_schemas:
        file_path = os.path.join(ext_schemas_folder, ext_schema.replace(f"{project}/QAF/Schemas/", ""))
        database.config.s3_client.download_file(Bucket=bucket, Key=ext_schema, Filename=file_path)
        ext_schemas_paths.append(file_path)
    
    return ext_schemas_paths


def trigger_rml_generation(project: str, database: Database, project_extended_tables: bool, do_not_restrict_unit: bool) -> str:
    """Triggers RML generation

    Args:
        project (str): name of the project
        database (Database): database object
        project_extended_tables (bool): create project extended tables
        do_not_restrict_unit (bool): do not restrict sphn:hasUnit values

    Returns:
        str: return message
    """
    bucket = database.config.s3_connector_bucket
    rml_folder = '/home/sphn/code/api/app/rml'
    rml_project_folder = os.path.join(rml_folder, project)
    json_schema_output_path = os.path.join(rml_project_folder, f'{project}_json_schema.json')
    SPHN_ONLY_json_schema_output_path = os.path.join(rml_project_folder, f'SPHN_ONLY_{project}_json_schema.json')
    rml_mapping_output_path = os.path.join(rml_project_folder, f'{project}_rml_mapping.ttl')
    SPHN_ONLY_rml_mapping_output_path = os.path.join(rml_project_folder, f'SPHN_ONLY_{project}_rml_mapping.ttl')
    try:
        os.makedirs(rml_folder, exist_ok=True)
        os.makedirs(rml_project_folder, exist_ok=True)
        msg = f"Getting external files for '{project}'"
        database.update_rml_json_logging(project_name = project,status=RML_JSON_LOGS.START, message = msg)
        schema_path = get_schema(project=project, database=database, rml_folder=rml_project_folder)
        ext_schemas = get_external_schemas(project=project, database=database, rml_folder=rml_project_folder)
        rml.main(input_data='patient_data_input.json', schema_file=schema_path, json_schema_output_path=json_schema_output_path, 
                 rml_mapping_output_path=rml_mapping_output_path, data_provider_id=database.config.data_provider_id, 
                 ext_schemas=ext_schemas, project_name=project, database=database, do_not_restrict_unit=do_not_restrict_unit)

        minio_json_schema = get_object_name(project_name=project, data_provider_id=database.config.data_provider_id, file_class=FileClass.RML, filename=os.path.basename(json_schema_output_path))
        database.config.s3_client.upload_file(Bucket=bucket, Key=minio_json_schema, Filename=json_schema_output_path)
        
        if ext_schemas and project_extended_tables:
            rml.main(input_data='patient_data_input.json', schema_file=ext_schemas[0], json_schema_output_path=SPHN_ONLY_json_schema_output_path, 
                     rml_mapping_output_path=SPHN_ONLY_rml_mapping_output_path, data_provider_id=database.config.data_provider_id, 
                     ext_schemas=[], project_name=project, do_not_restrict_unit=do_not_restrict_unit)
            SPHN_ONLY_minio_json_schema = get_object_name(project_name=project, data_provider_id=database.config.data_provider_id, file_class=FileClass.RML, filename=os.path.basename(SPHN_ONLY_json_schema_output_path))
            database.config.s3_client.upload_file(Bucket=bucket, Key=SPHN_ONLY_minio_json_schema, Filename=SPHN_ONLY_json_schema_output_path)

        minio_rml_mapping = get_object_name(project_name=project, data_provider_id=database.config.data_provider_id, file_class=FileClass.RML, filename=os.path.basename(rml_mapping_output_path))
        database.config.s3_client.upload_file(Bucket=bucket, Key=minio_rml_mapping, Filename=rml_mapping_output_path)

        msg = f"RML mapping and JSON schema generated for project '{project}'"
        database.update_rml_json_logging(project_name = project,status=RML_JSON_LOGS.ENDED, message = msg)
    except Exception as exc:
        logging.error("ERROR: %s", exc)
        database.update_rml_json_logging(project_name = project,status=RML_JSON_LOGS.FAILED, message = f"Exception raised when running RML generation: {exc}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=f"Exception raised when running RML generation: {exc}")
    finally:
        if os.path.exists(rml_project_folder):
            shutil.rmtree(path=rml_project_folder, ignore_errors=True)


def download_schema(project: str, database: Database) -> Response:
    """Download JSON schema file generated by RML generator

    Args:
        project (str): name of the project
        database (Database): database object

    Raises:
        HTTPException: Not found exception if no file is found in object storage

    Returns:
        Response: JSON schema file
    """
    bucket = database.config.s3_connector_bucket
    json_schema_name = get_object_name(project_name=project, data_provider_id=database.config.data_provider_id, file_class=FileClass.RML, filename=f'{project}_json_schema.json')
    try:
        response = database.config.s3_client.get_object(Bucket=bucket, Key=json_schema_name)['Body'].read()
    except Exception:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"JSON schema '{os.path.basename(json_schema_name)}' not found on object storage. Make sure the project is successfully initialized")
    headers = {'Content-Disposition': f'attachment; filename="{os.path.basename(json_schema_name)}"'}
    return Response(response, headers=headers)

def export_project(project: str, database: Database) -> Response:
    """Export project to ZIP archive

    Args:
        project (str): name of the project
        database (Database): database object

    Returns:
        Response: zipped data to download
    """
    bucket = database.config.s3_connector_bucket
    if bucket_exists(bucket = bucket, config=database.config):
        schemas = database.list_objects_keys(bucket=bucket, prefix=f"{project}/Schema/{project}_schema.ttl", file_path=True)
        external_objects = database.list_objects_keys(bucket=bucket, prefix=f"{project}/QAF/")
        pre_checks = database.list_objects_keys(bucket=bucket, prefix=f"{project}/Pre-Check/")
        de_identification = database.list_objects_keys(bucket=bucket, prefix=f"{project}/De-Identification/")
        sparqler_queries = database.list_objects_keys(bucket=bucket, prefix=f"{project}/Statistics/Queries/")
        file_list = schemas + external_objects + pre_checks + de_identification + sparqler_queries
        database.create_connection()
        insert_statements = []
        sql = 'SELECT terminology_name, version_iri FROM "public".external_terminologies WHERE project_name=%s and data_provider_id=%s'
        database.cursor.execute(sql, (project, database.config.data_provider_id))
        records = database.cursor.fetchall()
        if records:
            for record in records:
                name = record[0].replace("'", "''")
                name = f"'{name}'"
                version_iri = record[1].replace("'", "''")
                version_iri = f"'{version_iri}'"
                insert_statements.append(f'INSERT INTO "public".external_terminologies (project_name, data_provider_id, terminology_name, version_iri) VALUES ($PROJECT_NAME$, $DATA_PROVIDER_ID$, {name}, {version_iri});')
        database.close_connection()

        bucket = database.config.s3_connector_bucket
        zip_filename = f"{project}{CompressionType.ZIP.value}"
        bytes_buffer = io.BytesIO()
        with zipfile.ZipFile(bytes_buffer, mode='w', compression=zipfile.ZIP_DEFLATED) as zip_file:
            for fpath in file_list:
                try:
                    response = database.config.s3_client.get_object(Bucket=bucket, Key=fpath)["Body"].read()
                except Exception:
                    print("ERROR -- File '{}' for project '{}' not found in object storage even if it's supposed to be there".format(fpath, project))
                    continue
                zip_file.writestr(os.path.join('S3', fpath), response)
            if insert_statements:
                zip_file.writestr('PostgreSQL/internal_tables.sql', '\n'.join(insert_statements).encode())
            zip_file.close()

        return StreamingResponse(
            iter([bytes_buffer.getvalue()]),
            media_type="application/x-zip-compressed",
            headers = { "Content-Disposition": "attachment; filename={}".format(zip_filename)}
        )

def import_project(project: str, content: bytes, database: Database):
    """Import and create project from a zipped archive

    Args:
        project (str): name of the project
        content (bytes): ZIP archive content
        database (Database): database object
    """
    bytes_buffer = io.BytesIO(content)
    bucket = database.config.s3_connector_bucket
    create_s3_bucket(bucket = bucket, config=database.config)
    with zipfile.ZipFile(bytes_buffer) as zip_ref:
        for zipinfo in zip_ref.infolist():
            with zip_ref.open(zipinfo) as extracted_file:
                file_content = extracted_file.read()
                bytes_content= io.BytesIO(file_content)
                bytes_content.seek(0)
                if zipinfo.filename == 'PostgreSQL/internal_tables.sql':
                    file_content = file_content.decode().replace("$PROJECT_NAME$", f"'{project}'").replace("$DATA_PROVIDER_ID$", f"'{database.config.data_provider_id}'").encode()
                    database.create_connection()
                    database.cursor.execute(file_content)
                    database.close_connection()
                else:
                    filename = zipinfo.filename.replace('S3/', '')
                    if '/Schema/' in filename:
                        filename = f"{project}/Schema/{project}_schema.ttl"
                    elif '/Pre-Check/' in filename:
                        filename = f"{project}/Pre-Check/{project}_checks_config.json"
                    elif "/De-Identification/" in filename and filename.endswith('_de_identification_config.json'):
                        filename = f"{project}/De-Identification/{project}_de_identification_config.json"
                    elif "/De-Identification/" in filename and filename.endswith('_formats_mapping.json'):
                        filename = f"{project}/De-Identification/{project}_formats_mapping.json"
                    elif bool(re.match(r"^.*/QAF/SHACL/Shacler_.*_shacl.ttl$", filename)):
                        filename = f"{project}/QAF/SHACL/Shacler_{project}_shacl.ttl"
                    elif '/QAF/' in filename:
                        filename = f"{project}/QAF/{filename.split('/QAF/')[-1]}"
                    elif '/Statistics/' in filename:
                        filename = f"{project}/Statistics/{filename.split('/Statistics/')[-1]}"
                    database.config.s3_client.put_object(Bucket=bucket, Key=filename, Body=bytes_content)

def project_schema_exists(content: bytes) -> bool:
    """Check if project schema is present in ZIP import

    Args:
        content (bytes): ZIP project import

    Returns:
        bool: project schema exists
    """
    bytes_buffer = io.BytesIO(content)
    with zipfile.ZipFile(bytes_buffer) as zip_ref:
        for zipinfo in zip_ref.infolist():
            if '/QAF/Schemas/' in zipinfo.filename:
                return True
    return False

def extract_sphn_iri_during_import(project: str, database: Database) -> str:
    """Extract SPHN base IRI when importing a project

    Args:
        project (str): name of the imported project
        database (Database): database object

    Returns:
        str: SPHN base IRI
    """
    bucket = database.config.s3_connector_bucket
    
    ext_schemas = database.list_objects_keys(bucket=bucket, prefix=f"{project}/QAF/Schemas/")
    for ext_schema in ext_schemas:
        schema = database.config.s3_client.get_object(Bucket=bucket, Key=ext_schema)
        return get_schema_iri(content=schema["Body"].read())

    main_schemas = database.list_objects_keys(bucket=bucket, prefix=f"{project}/Schema/")
    for sphn_schema in main_schemas:
        schema = database.config.s3_client.get_object(Bucket=bucket, Key=sphn_schema)
        return get_schema_iri(content=schema["Body"].read())

def upload_schemas(project: str, 
                   sphn_schema: UploadFile, 
                   project_specific_schema: UploadFile, 
                   exception_file: UploadFile, 
                   database: Database,
                   einstein: bool, 
                   output_format: OutputFormat) -> str:
    """Upload schemas and exception file for project creation

    Args:
        project (str): name of the project
        sphn_schema (UploadFile): SPHN schema
        project_specific_schema (UploadFile): project specific schema
        exception_file (UploadFile): exception file
        database (Database): database object
        sphn_base_iri (str): SPHN base IRI
        einstein (bool): share data with Einstein tool
        output_format (OutputFormat): data output format
    """
    if project_specific_schema is None:
        content = sphn_schema.file.read()
        database.check_pre_conditions(project=project, content=content, filename=sphn_schema.filename, type=PreCondition.PROJECT_CREATION, file_type=FileType.RDF, file_class=FileClass.SCHEMA, einstein=einstein,
                                      output_format=output_format)
        validate_rdf_file(content=content, filename=sphn_schema.filename)
        sphn_base_iri = get_schema_iri(content=content)
        write_to_s3(content=content, project_name=project, file_class=FileClass.SCHEMA, database=database, filename=sphn_schema.filename)
    else:
        content = project_specific_schema.file.read()
        database.check_pre_conditions(project=project, content=content, filename=project_specific_schema.filename, type=PreCondition.PROJECT_CREATION, file_type=FileType.RDF, file_class=FileClass.SCHEMA, einstein=einstein, 
                                      output_format=output_format, project_specific=True)
        validate_rdf_file(content=content, filename=project_specific_schema.filename)
        sphn_imported_iri = check_sphn_import(content=content)
        write_to_s3(content=content, project_name=project, file_class=FileClass.SCHEMA, database=database, filename=project_specific_schema.filename)
        content = sphn_schema.file.read()
        database.check_pre_conditions(project=project, content=content, filename=sphn_schema.filename, type=PreCondition.EXT_SCHEMA_UPLOAD, file_type=FileType.RDF, file_class=FileClass.EXT_SCHEMA)
        validate_rdf_file(content=content, filename=sphn_schema.filename)
        check_imports_match(content=content, sphn_imported_iri=sphn_imported_iri)
        sphn_base_iri = get_schema_iri(content=content)
        write_to_s3(content=content, project_name=project, file_class=FileClass.EXT_SCHEMA, database=database, filename=sphn_schema.filename)
    if exception_file is not None:
        content = exception_file.file.read()
        database.check_pre_conditions(project=project, content=content, filename=exception_file.filename, type=PreCondition.PROJECT_CREATION, file_type=FileType.JSON, file_class=FileClass.EXCEPTIONS, 
                                      output_format=output_format)
        write_to_s3(content=content, project_name=project, file_class=FileClass.EXCEPTIONS, database=database, filename=exception_file.filename)
    return sphn_base_iri

def purge_internal_data(project: str, file_class: FileClass, database: Database, purge_before_upload: bool):
    """Purge S3 data for a specific class

    Args:
        project (str): name of the project
        file_class (FileClass): file class
        database (Database): database object
        purge_before_upload (bool): purge data before uploading new files
    """
    bucket = database.config.s3_connector_bucket
    if purge_before_upload:
        if bucket_exists(bucket = bucket, config=database.config):
            prefix = os.path.dirname(get_object_name(project_name=project, data_provider_id=database.config.data_provider_id, file_class=file_class, filename='filename')).lstrip('/') + "/"
            if file_class == FileClass.DE_IDENTIFICATION_CONFIG:
                external_objects = database.list_objects_keys(bucket=bucket, prefix=f"{project}/De-Identification/{project}_de_identification_config.json", file_path=True)
            else:
                external_objects = database.list_objects_keys(bucket=bucket, prefix=prefix)
            refined_objects=database.list_objects_keys(bucket=bucket, prefix=f"{project}/Input/refined_zone/")
            graph_objects=database.list_objects_keys(bucket=bucket, prefix=f"{project}/Input/graph_zone/")
            released_objects=database.list_objects_keys(bucket=bucket, prefix=f"{project}/Output/")
            objects_to_delete = external_objects + refined_objects + graph_objects + released_objects
            if objects_to_delete:
                for chunk in Database._split_list_into_chunks(object_names=objects_to_delete):
                    delete_keys = {'Objects' : []}
                    delete_keys['Objects'] = [{'Key' : k} for k in chunk]
                    if delete_keys['Objects']:
                        database.config.s3_client.delete_objects(Bucket=bucket, Delete=delete_keys)
            for file_object in external_objects:
                database.update_config_files_history(project=project, filename=os.path.basename(file_object), action=ConfigFileAction.DELETED, file_class=file_class)
        database.purge_tables(project=project, file_class=file_class)
    elif file_class in [FileClass.EXT_SHACL, FileClass.PRE_CHECK_CONFIG, FileClass.DE_IDENTIFICATION_CONFIG]:
        raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail=f"To upload external files of type '{file_class.value}' the flag 'purge_before_upload' must be activated")


def initialize_project(project: str, 
                       database: Database, 
                       airflow: Airflow, 
                       minio_policy: MinioPolicy, 
                       testing: bool,
                       project_extended_tables: bool,
                       do_not_restrict_unit: bool):
    """Initialize project

    Args:
        project (str): name of the project
        database (Database): database object
        airflow (Airflow): airflow object
        minio_policy (MinioPolicy): MinioPolicy object
        testing (bool): activate to skip initialization for testing purposes
        project_extended_tables (bool): create project extended tables
        do_not_restrict_unit (bool): do not restrict sphn:hasUnit values
    """
    if not testing:
        airflow.trigger_dag(project=project, dag_id='run_shacler') 
        trigger_rml_generation(project=project, database=database, project_extended_tables=project_extended_tables,
                               do_not_restrict_unit=do_not_restrict_unit)
        database.ddl_generation(project_name=project)
        database.extract_formats_mapping(project=project)
        database.grant_permissions(project=project)
        minio_policy.create(project=project)
        database.update_tdb2_status_table(project=project, status=False)
        database.mark_project_as_initialized_in_db(project=project)


def upload_batch_data(project: str):
    """Moves patient data uploaded in S3 to landing zone

    Args:
        project (str): name of the project
        database (Database): database object
    """
    database = Database(project=project)
    bucket = database.config.s3_connector_bucket
    prefix = f'{project}/Input/batch_ingestion/'
    batch_files = database.list_objects_keys(bucket=bucket, prefix=prefix)
    if batch_files:
        csv_files = []
        for batch_object in batch_files:
            extension = os.path.basename(batch_object).split('.')[-1]
            if extension in ['json', 'ttl']:
                file_type = FileType.JSON if extension == 'json' else FileType.RDF
                ingestion_type = IngestionType.JSON if file_type == FileType.JSON else IngestionType.RDF
                patient_id = os.path.basename(batch_object).rsplit('.', 1)[0]
                database.cleanup_before_ingestion(project_name=project, patient_id=patient_id)
                target_object_name = get_object_name(project_name=project, data_provider_id=database.config.data_provider_id, file_class=FileClass.PATIENT_DATA, patient_id=patient_id, file_type=file_type)
                copy_source = {"Bucket": bucket, "Key": batch_object}
                database.config.s3_client.copy(copy_source, bucket, target_object_name)
                if batch_object:
                    for chunk in Database._split_list_into_chunks(object_names=batch_object):
                        delete_keys = {'Objects' : []}
                        delete_keys['Objects'] = [{'Key' : chunk}]
                        if delete_keys['Objects']:
                            database.config.s3_client.delete_objects(Bucket=bucket, Delete=delete_keys)
                database.create_connection()
                sql = f"INSERT INTO {DataPersistenceLayer.LANDING_ZONE.value} (project_name, patient_id, data_provider_id, object_name, file_type, timestmp, ingestion_type, processed) VALUES (%s, %s, %s, %s, %s, %s, %s, False) \
                        ON CONFLICT (project_name, patient_id, data_provider_id) DO UPDATE SET object_name=EXCLUDED.object_name, file_type=EXCLUDED.file_type, timestmp=EXCLUDED.timestmp, ingestion_type=EXCLUDED.ingestion_type, processed=EXCLUDED.processed"
                database.cursor.execute(sql, (project, patient_id, database.config.data_provider_id, target_object_name, file_type.value, datetime.now(), ingestion_type.value))
                database.close_connection()
            elif extension == 'csv':
                csv_files.append(batch_object)
        if csv_files:
            database.truncate_tables(schema_name=project)
            for chunk in Database._split_list_into_chunks(object_names=csv_files):
                for csv_file in chunk:
                    database.ingest_csv_file(project=project, object_name=csv_file)
                    database.config.s3_client.delete_objects(Bucket=bucket, Delete={"Objects": [{"Key": csv_file}]})

def get_offline_redoc_html(openapi_url: str, title: str, redoc_js_url: str, redoc_favicon_url: str, with_google_fonts: bool = True) -> HTMLResponse:
    html = f"""
    <!DOCTYPE html>
    <html>
    <head>
    <title>{title}</title>
    <!-- needed for adaptive design -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    """
    if with_google_fonts:
        html += """
    <link href="/static/font.css" rel="stylesheet">
    """
    html += f"""
    <link rel="shortcut icon" href="{redoc_favicon_url}">
    <!--
    ReDoc doesn't change outer page styles
    -->
    <style>
      body {{
        margin: 0;
        padding: 0;
      }}
    </style>
    </head>
    <body>
    <noscript>
        ReDoc requires Javascript to function. Please enable it to browse the documentation.
    </noscript>
    <redoc spec-url="{openapi_url}"></redoc>
    <script src="{redoc_js_url}"> </script>
    </body>
    </html>
    """
    return HTMLResponse(html)

def ingest_terminology(project:str, content: bytes, filename: str, database: Database, filenames: list):
    """Ingest terminology file

    Args:
        project (str): name of the project
        content (bytes): terminology content
        filename (str): name of the file
        database (Database): database object
        filenames (list): list of ingested terminologies
    """
    database.check_pre_conditions(project=project, content=content, filename=filename, type=PreCondition.INGESTION, file_type=FileType.RDF, file_class=FileClass.EXT_TERMINOLOGY)
    version_iri = validate_external_terminology(content=content, filename=filename)
    write_to_s3(content=content, project_name=project, file_class=FileClass.EXT_TERMINOLOGY, database=database, filename=filename)
    database.update_external_terminologies_table(project=project, filename=filename, version_iri=version_iri)
    filenames.append(filename)

def get_service_logs(service: DockerService) -> str:
    """Return logs for service

    Args:
        service (DockerService): Docker service

    Returns:
        str: logs for the service
    """
    service_logs = "#"*100 + f"\nLogs for {service.value}\n" + "#"*100 + "\n\n"
    log_file_1 = f'/shared/logs/{service.value}1.log'
    log_file_2 = f'/shared/logs/{service.value}2.log'
    if os.path.exists(log_file_2):
        with open(log_file_2, 'r') as file:
            service_logs += "Previous run:\n"
            service_logs += file.read()
        service_logs += "\n\n"
    if os.path.exists(log_file_1):
        with open(log_file_1, 'r') as file:
            service_logs += "Current run:\n"
            service_logs += file.read()
        service_logs += "\n\n"
    else:
        service_logs += "No logs found for this service for the current run\n\n"
    return service_logs

def extract_docker_logs(service: DockerService) -> Response:
    """Extract Docker logs

    Args:
        service (DockerService): Docker service

    Returns:
        Response: logs file
    """
    if service == DockerService.ALL:
        logs = ""
        for docker_service in DockerService:
            if docker_service != DockerService.ALL:
                logs += get_service_logs(service=docker_service)
        headers = {'Content-Disposition': f'attachment; filename="Docker_services_{datetime.now().strftime("%Y-%m-%dT%H_%M_%S")}.log"'}
    else:

        logs = get_service_logs(service=service)
        headers = {'Content-Disposition': f'attachment; filename="Docker_{service.value}_{datetime.now().strftime("%Y-%m-%dT%H_%M_%S")}.log"'}

    return Response(logs, headers=headers)