#!/bin/sh

#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'create-standard-groups.sh' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
# 

# Syntax
# ------
#   ./create-standard-groups.sh bucket-name prefix/sub-prefix
#

MINIO_ALIAS=sib
BUCKET_NAME=$1
PATHPREFIX=$2
VERSION="2012-10-17" # version of the policy interpreter!

if [ "$#" -ne 2 ]; then
  echo "Illegal number of parameters"
  exit 1
fi

PREFIX=${PATHPREFIX//\//_} 
RO_POLICY=${BUCKET_NAME}_${PREFIX}_STANDARD_RO
RW_POLICY=${BUCKET_NAME}_${PREFIX}_STANDARD_RW
RWD_POLICY=${BUCKET_NAME}_${PREFIX}_STANDARD_RWD

echo "Creating policies and groups for prefix $PATHPREFIX..."
echo RO_POLICY: $RO_POLICY
echo RW_POLICY: $RW_POLICY
echo RWD_POLICY: $RWD_POLICY

mkdir -p /home/sphn/code/api/app/tmp/minio-policy
cat << EOF > /home/sphn/code/api/app/tmp/minio-policy/${RO_POLICY}.json
{
   "Version":"$VERSION",
   "Statement": [
    {
       "Sid": "AllowListBucketConsole",
       "Action": ["s3:ListAllMyBuckets","s3:GetBucketLocation"],
       "Effect": "Allow",
       "Resource": ["arn:aws:s3:::*"]
     },
    {
       "Sid": "AllowListofObjectsinRoot",
       "Action": ["s3:ListBucket"],
       "Effect": "Allow",
       "Resource": ["arn:aws:s3:::*"]
     },
    {
        "Sid": "GetObject",
        "Action": ["s3:GetObject"],
        "Effect": "Allow",
        "Resource": ["arn:aws:s3:::*"]
     }
   ]
}
EOF

cat << EOF > /home/sphn/code/api/app/tmp/minio-policy/${RW_POLICY}.json
{
   "Version":"$VERSION",
   "Statement": [
    {
       "Sid": "AllowListBucketConsole",
       "Action": ["s3:ListAllMyBuckets","s3:GetBucketLocation"],
       "Effect": "Allow",
       "Resource": ["arn:aws:s3:::*"]
     },
    {
       "Sid": "AllowListofObjectsinRoot",
       "Action": ["s3:ListBucket"],
       "Effect": "Allow",
       "Resource": ["arn:aws:s3:::*"]
     },
    {
       "Sid": "AllowListListBucketMultiUpload",
       "Action": ["s3:ListBucketMultipartUploads"],
       "Effect": "Allow",
       "Resource": ["arn:aws:s3:::*"]
     },
    {
        "Sid": "GetObject",
        "Action": ["s3:GetObject"],
        "Effect": "Allow",
        "Resource": ["arn:aws:s3:::*"]
     },
    {
        "Sid": "PutObject",
        "Action": ["s3:PutObject"],
        "Effect": "Allow",
        "Resource": ["arn:aws:s3:::$BUCKET_NAME/$PATHPREFIX/*"]
     }
   ]
}
EOF

cat << EOF > /home/sphn/code/api/app/tmp/minio-policy/${RWD_POLICY}.json
{
   "Version":"$VERSION",
   "Statement": [
    {
       "Sid": "AllowListBucketConsole",
       "Action": ["s3:ListAllMyBuckets","s3:GetBucketLocation"],
       "Effect": "Allow",
       "Resource": ["arn:aws:s3:::*"]
     },
    {
       "Sid": "AllowListofObjectsinRoot",
       "Action": ["s3:ListBucket"],
       "Effect": "Allow",
       "Resource": ["arn:aws:s3:::*"]
     },
    {
       "Sid": "AllowListBucketMultiUpload",
       "Action": ["s3:ListBucketMultipartUploads"],
       "Effect": "Allow",
       "Resource": ["arn:aws:s3:::*"]
     },
    {
        "Sid": "GetObject",
        "Action": ["s3:GetObject"],
        "Effect": "Allow",
        "Resource": ["arn:aws:s3:::*"]
     },
    {
        "Sid": "PutObject",
        "Action": ["s3:PutObject"],
        "Effect": "Allow",
        "Resource": ["arn:aws:s3:::$BUCKET_NAME/$PATHPREFIX/*"]
     },
    {
        "Sid": "Delete",
        "Action": ["s3:DeleteObject"],
        "Effect": "Allow",
        "Resource": ["arn:aws:s3:::$BUCKET_NAME/$PATHPREFIX/*"]
     }
   ]
}
EOF

/home/sphn/minio-binaries/mc admin user add ${MINIO_ALIAS} dummyuser dummypassword

/home/sphn/minio-binaries/mc admin group add ${MINIO_ALIAS} $RO_POLICY dummyuser
/home/sphn/minio-binaries/mc admin group add ${MINIO_ALIAS} $RW_POLICY dummyuser
/home/sphn/minio-binaries/mc admin group add ${MINIO_ALIAS} $RWD_POLICY dummyuser

/home/sphn/minio-binaries/mc admin policy create ${MINIO_ALIAS} $RO_POLICY /home/sphn/code/api/app/tmp/minio-policy/$RO_POLICY.json
/home/sphn/minio-binaries/mc admin policy create ${MINIO_ALIAS} $RW_POLICY /home/sphn/code/api/app/tmp/minio-policy/$RW_POLICY.json
/home/sphn/minio-binaries/mc admin policy create ${MINIO_ALIAS} $RWD_POLICY /home/sphn/code/api/app/tmp/minio-policy/$RWD_POLICY.json

/home/sphn/minio-binaries/mc admin policy attach ${MINIO_ALIAS} $RO_POLICY --group=$RO_POLICY
/home/sphn/minio-binaries/mc admin policy attach ${MINIO_ALIAS} $RW_POLICY --group=$RW_POLICY
/home/sphn/minio-binaries/mc admin policy attach ${MINIO_ALIAS} $RWD_POLICY --group=$RWD_POLICY

/home/sphn/minio-binaries/mc admin group remove ${MINIO_ALIAS} $RO_POLICY dummyuser
/home/sphn/minio-binaries/mc admin group remove ${MINIO_ALIAS} $RW_POLICY dummyuser
/home/sphn/minio-binaries/mc admin group remove ${MINIO_ALIAS} $RWD_POLICY dummyuser
/home/sphn/minio-binaries/mc admin user remove ${MINIO_ALIAS} dummyuser
