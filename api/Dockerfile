#
# Copyright (C) 2025 – SIB Swiss Institute of Bioinformatics.  
#
# This file: 'Dockerfile' is part of the SPHN Connector.
# This file is subject to  the terms and conditions defined in the file "License.txt"
# which is part of the source code package. See the "License.txt" file 
# distributed with this work for additional information regarding copyright ownership.
# You may not use this file except in compliance with the licence. 

# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

FROM registry.dcc.sib.swiss/sphn-semantic-framework/sphn-connector/python:3.10.15-alpine3.20

ENV CODE=/home/sphn/code
ENV HOME=/home/sphn
ENV API=/home/sphn/code/api/app/


#Create sphn user & Group with GID = 1000 and UID = 1000
RUN addgroup -g 1000 sphn && adduser -u 1000 -D -h /home/sphn sphn -G sphn

USER root

RUN apk update \
  && apk upgrade \
  && apk add --no-cache --virtual .build-deps git curl build-base bash gcc musl-dev linux-headers python3-dev libexpat=2.6.4-r0 \
  && apk add --no-cache curl \
  && curl https://dl.min.io/client/mc/release/linux-amd64/archive/mc.RELEASE.2025-01-17T23-25-50Z --insecure \
  --create-dirs \
  -o $HOME/minio-binaries/mc \
  && chmod +x $HOME/minio-binaries/mc \
  && chown sphn $HOME/minio-binaries/mc \
  && mkdir -p /shared/logs \
  && chmod 777 /shared/logs

# uncomment if you want add loadtesting to the container
# COPY ./loadtesting ${CODE}/loadtesting
# WORKDIR ${CODE}/loadtesting
# RUN bash loadtesting_lib/create-folders.sh \
# && curl 'https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/raw/2024-2/rdf_schema/sphn_rdf_schema.ttl?inline=false' -o project/sphn-schema/sphn-schema.ttl \
# && python3 -m pip install --no-cache-dir --upgrade -r requirements.txt

USER sphn

WORKDIR ${CODE}
ENV TZ=Europe/Zurich
COPY ./api/app/requirements.txt ${CODE}/requirements.txt

USER root
RUN python3 -m pip install --no-cache-dir --upgrade pip \
    && python3 -m pip install --no-cache-dir --upgrade -r ${CODE}/requirements.txt

USER sphn

COPY ./api/app/api.py ${API}/api.py
COPY ./api/app/main.py ${API}/main.py
COPY ./api/app/responses.py ${API}/responses.py
COPY ./api/app/authentication.py ${API}/authentication.py
COPY ./lib/database.py ${API}/database.py
COPY ./lib/config.py ${API}/config.py
COPY ./lib/airflow_lib.py ${API}/airflow_lib.py
COPY ./lib/enums.py ${API}/enums.py
COPY ./lib/rml_generator.py ${API}/rml_generator.py
COPY ./lib/database_extractor.py ${API}/database_extractor.py
COPY ./lib/pre_checks.py ${API}/pre_checks.py
COPY ./lib/de_identification.py ${API}/de_identification.py
COPY ./lib/grafana_counts.py ${API}/grafana_counts.py
COPY ./api/tests ${CODE}/api/tests
COPY ./utils/generate_config_file.py ${CODE}/utils/generate_config_file.py
COPY ./lib/rdf_parser.py ${API}/rdf_parser.py
COPY ./lib/backup_restore.py ${API}/backup_restore.py
COPY ./lib/minio_policy.py ${API}/minio_policy.py
COPY ./api/app/scripts/create-admin-groups.sh ${API}/scripts/create-admin-groups.sh 
COPY ./api/app/scripts/create-standard-groups.sh ${API}/scripts/create-standard-groups.sh 
COPY ./api/app/scripts/create-ingestion-groups.sh ${API}/scripts/create-ingestion-groups.sh
COPY ./lib/rml_generator_lib.py ${API}/rml_generator_lib.py
COPY ./api/app/templates ${API}/templates
COPY ./api/app/static ${API}/static

USER root
RUN chmod +x ${API}/scripts/create-admin-groups.sh \
&& chmod +x ${API}/scripts/create-standard-groups.sh \
&& chmod +x ${API}/scripts/create-ingestion-groups.sh

WORKDIR ${CODE}

COPY .git ${CODE}
RUN git config --global --add safe.directory /home/sphn/code && GIT_HASH=$(git rev-parse HEAD) && echo "GIT_HASH=${GIT_HASH}" >> version.txt
RUN IS_TAG=$(git describe --exact-match --tags HEAD >/dev/null 2>&1 && echo true || echo false) && echo "IS_TAG=${IS_TAG}" >> version.txt
RUN VERSION_NAME=$(git describe --exact-match --tags HEAD 2>/dev/null || git branch --show-current) && echo "VERSION_NAME=${VERSION_NAME:-main}" >> version.txt
RUN COMMIT_TIMESTAMP=$(git show -s --format=%ci HEAD) && echo "COMMIT_TIMESTAMP=${COMMIT_TIMESTAMP}" >> version.txt

RUN rm -rf ${CODE}.git \
    && apk del .build-deps openssh-client openssl \
    && chown -R sphn:sphn ${CODE} \
    && chmod -R 755 ${CODE}

USER sphn